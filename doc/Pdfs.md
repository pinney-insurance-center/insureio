# PDF's

## Interactive PDF's

To generate interactive PDF's we use LaTeX (because `wkhtmltopdf`'s support for interactive PDF's is broken).

Support for interactive PDF's is also broken on the version of texlive available on CentOS 7. Therefore, we must build it from source.

### Installation

Don't use the livetex packages on `yum`. They have some small bugs that interfere with interactive pdfs.

_cf._ https://www.tug.org/texlive/quickinstall.html

```bash
# Get source
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar xzf install-tl-unx.tar.gz
# Your release dir may differ
cd install-tl-20200515
sudo ./install-tl
```

### Generating interactive PDF's

Put all your assets (`.tex`, `.aux`, and whatever image files) into a single directory. Then:

```bash
pdflatex myfile.tex
```
