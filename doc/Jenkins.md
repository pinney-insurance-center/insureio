# Jenkins

## Command-line access

```bash
ssh qa
# Then in qa:
docker exec -it jenkins-docker sh
# Then in the jenkins container:
/var/insureio/launch.sh
```

The `launch.sh` script is just a helper, with one command: `docker run -v /var/jenkins_home/workspace/first-insureio:/Insureio -v /var/insureio:/var/insureio -u root --privileged -it reg.qa/insureio:2.wjre`
