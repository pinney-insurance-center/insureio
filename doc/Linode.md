# Linode

Our servers are currently running on **Linode**, and our patch script makes use of Linode's cli, so you should set up the cli on your system.

## CLI

### Installation & updates

You can install the cli as a python package: `pip install linode-cli`.

At times, the Insureio patch script will fail because the cli needs to be updated, so run `pip install --upgrade linode-cli`

### Access tokens

Having installed the cli, you need to get an **acceess token** from the linode website: https://cloud.linode.com/profile/tokens. In fact, these will expire every so often, so you'll need to get a new one periodically.

For the purposes of the _patch script_, make sure that you give your token RW access to NodeBalancers and R access to Linodes.

### Configuring

Each time you get a new access token, you need to re-configure your cli. Run `linode-cli configure`.

Supply your new access token. Skip the optional settings (unless you have a specific purpose). Do _not_ change the 'active user'.
