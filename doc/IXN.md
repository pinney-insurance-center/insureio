# IXN

IXN is a company in the same space as we are. We send quotes for NMB (National Mutual Benefit) through them.

## ALQ

The ALQ (Agency Life Quoter) is an embeddable widget. We use it in the Quote Path for NMB quotes. For an example of a snippet: https://github.com/ienetwork/code_examples/blob/master/agency_life_quoter/advanced.html

## Reference

- https://docs.thelifedx.com/ (LifeDX)
- https://developer.ixn.io/?version=latest (IXN)
- https://github.com/ienetwork/code_examples/blob/master/agency_life_quoter/advanced.html ALQ

## UI

- https://dashboard.ixn.tech/#/settings/agency/notification_triggers
