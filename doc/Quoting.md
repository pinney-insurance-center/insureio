# Quoting

## Steps

1. POST to `/quoting/get_quotes.json`
    1. Save `Consumer` and `Case` records
    1. Perform health screen with XRAE
    1. If XRAE screen fails, get quotes from XRAE
    1. If XRAE screen succeeds, get quotes from Compulife
    1. Return quotes to front end
