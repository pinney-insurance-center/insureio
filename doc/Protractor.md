# Protractor

Protractor is a framework for end-to-end front-end testing, which is compatible with AngularJS.

## Installing

```bash
npm install
export PATH="$PATH:$(pwd)/node_modules/protractor/bin/protractor"
webdriver-manager update 
webdriver-manager start &
protractor config/protractor.js 
```

We recorded protractor succeeding with the following component versions:

```bash
user@workstation:~/insureio$ node_modules/protractor/bin/webdriver-manager status
[11:56:12] I/status - selenium standalone version available: 3.141.59 [last]
[11:56:12] I/status - chromedriver version available: 80.0.3987.106 [last]
[11:56:12] I/status - geckodriver version available: v0.26.0 [last]
[11:56:12] I/status - android-sdk is not present
[11:56:12] I/status - appium is not present
```

### Errors

If you get an error along the lines of `This version of ChromeDriver only supports Chrome version...`, check your installed version of google chrome. Do you have a recent version? If not, download and install the latest version, then run the update again:

```bash
wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
sudo yum localinstall google-chrome-stable_current_x86_64.rpm
webdriver-manager update
# And then restart the webdriver-manager's server (if running)
```
