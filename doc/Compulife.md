# Compulife

Compulife is a third-pary quote engine which we run on one of our app servers. For most of our history, almost of our life insurance quote results have been come from Compulife.

## API

See `doc/Compulife-readme.html`, which is the doc provided by Compulife on 2020-08-10.

```bash
# Assuming complife.local is in your hosts file:
curl 'http://compulife.local/cgi-bin/cqsl.cgi?X=M&GoString=tr&NoGoString=false&DoNotKnowString=maybe&UserLocation=XML&ModeUsed=M&NewCategory=3&FaceAmount=200000&BirthMonth=10&BirthYear=1972&Birthday=30&DoHeightWeight=ON&DoSmokingTobacco=ON&DoCigarettes=ON&Smoker=N&NumCigarettes=1&PeriodCigarettes=-1&Health=PP&Feet=6&Inches=1&Sex=F&Weight=160&State=15'
```

## Files

Compulife is installed as a collection of data files, a cgi file, and a (Windows) desktop executable on the application servers at `/var/www/new-compulife` with an Apache config at `/etc/httpd/vhosts/compulife.conf`.

## Insureio interface

`lib/compulife.rb` sends HTTP GET requests to `/cgi-bin/cqsl.cgi` on the Compulife appliction. This implemention is largely based on the library of the same purpose from CLU (a legacy PIC web application).

## Updates

Every so often, the license file or the data files which supply quote information need to be updated on the Compulife installations. You can get these updates from Ryan or by running the Compulife desktop application on a Windows machine (see following subsection).

Because all of the application servers are running Compulife, they all must be updated.

When updating the server files, start by copying the current files to a new directory:

```bash
cp -p -r COMPULIFE COMPULIFE.old.20XX-XX-XX
```

Then move the updates into the COMPULIFE directory.

_NB:_ For the web application, you must rename `COMPLIFE/decomp.prm` to `COMPLIFE/DECOMP.PRM` (just caplitalize it). This file contains the license.

_NB:_ The web application requires files `COMPULIFE/USER/*` which hold user data. The desktop application has no `COMPULIFE/USER` directory.

### Windows desktop application

Download the installer from https://www.compulife.net/compulife/cqs/ and execute it on a Windows machine. Enter the 7-digit serial number (which Ryan has).

Running the installed `C:\COMPLIFE\GOWIN.EXE` will let you 'Install Next Update' (and may prompt you on launch to 'Install mid-month updates').

Transfer the contents of `C:\COMPLIFE` for the web server update.
