require File.join(Rails.root, 'config/initializers/audit_logger.rb')

module ActiveRecord
  class Base
    include ::AuditLogger::Helpers
  end
end