module Marginalia
  module Comment

    def self.user_id
      Thread.current[:current_user_id]||''
    end

    def self.true_user_id
      Thread.current[:true_current_user_id]||''
    end

    def self.auth
      Thread.current[:current_user_auth]||''
    end

    def self.short_line
      #Remove path to the relevant file, since filenames are already unique. 
      self.line.to_s.gsub(/(\/app\/.+\/)/,'')
    end

  end
end
Marginalia.application_name = "Insureio"
Marginalia::Comment.components = [:application, :controller, :action, :short_line, :user_id, :true_user_id, :auth]