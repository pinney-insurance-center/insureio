module EnumAssociations

  def self.included(base)
    base.extend ClassMethods
  end
  
  module ClassMethods
    def belongs_to_enum enum, options={}
      options.stringify_keys!
      klass_name = options['class_name'] || enum.to_s.camelize
      klass_name="Enum::#{klass_name}" if !klass_name.include?('Enum::')
      foreign_key = options['foreign_key'] || "#{enum}_id"
      enum_var  = "@#{enum}"
      attr_accessor enum
      # getter method
      define_method enum do
        ( send("#{foreign_key}_changed?") ? false : instance_variable_get(enum_var) ) ||
        instance_variable_set( enum_var, klass_name.constantize.find(self.send(foreign_key)) )
      end
      # override the foreign key setter method
      define_method "#{foreign_key}=" do |*args|
        super *args
        instance_variable_set enum_var, nil
      end
      # setter method
      define_method "#{enum}=" do |obj|
        instance_variable_set enum_var, obj
        write_attribute foreign_key, obj.try(:id)
      end
    end

    def has_and_belongs_to_many_enum enum, options={}
      options.stringify_keys!
      klass_name = options['class_name'] || enum.to_s.singularize.camelize
      join_table = options['join_table'] || join_table_name(enum)
      foreign_key = options['foreign_key'] || "#{self.name.split('::').last.underscore}_id"
      association_foreign_key = options['association_foreign_key'] || "#{enum.to_s.singularize}_id"
      enum_var  = "@#{enum}"
      attr_accessor enum
      # ids getter method
      define_method "#{enum}_ids" do
        return if id.nil?
        response = ActiveRecord::Base.connection.execute "select #{association_foreign_key} from #{join_table} where #{foreign_key} = #{self.id}"
        response.to_a.flatten.map &:to_i
      end
      # getter method
      define_method enum do
        instance_variable_get(enum_var) || begin
          ids = self.send "#{enum}_ids"
          items = klass_name.constantize.ewhere(id:ids)
          instance_variable_set enum_var, items
        end
      end
      # setter method
      define_method "#{enum}=" do |objects|
        if self.persisted?
          objects = Array(objects)
          instance_variable_set enum_var, objects
          insertion = "insert into #{join_table} (#{association_foreign_key}, #{foreign_key}) values " + objects.map{|o| "(#{o.id},#{self.id})" }.join(',')
          ActiveRecord::Base.connection.execute "delete from #{join_table} where #{foreign_key} = #{self.id}"
          ActiveRecord::Base.connection.execute insertion
        end
      end
    end

    def join_table_name other
      if self.table_name < other.table_name
        "#{self.table_name}_#{other.table_name}"
      else
        "#{other.table_name}_#{self.table_name}"
      end
    end
  end
end

module ActiveRecord
  class Base
    include EnumAssociations
  end
end
