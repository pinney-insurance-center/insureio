ExceptionNotifier.class_eval {
  class << self
    alias_method :orig_notify_exception, :notify_exception
    # Handle strings, as well as non-raised errors
    def notify_exception ex, opts={}
      ex = RuntimeError.new(ex) if ex.is_a? String
      ex.set_backtrace(caller) if ex.backtrace.nil?
      orig_notify_exception ex, opts
    end
  end
}
