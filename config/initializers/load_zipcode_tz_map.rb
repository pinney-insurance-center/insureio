=begin

This file sets a constant ZIP_TZ_MAP, which maps zip codes to time zones.
This is used for inferring a client's tz. This is used to display a tz difference
on the Crm::Case index pages.

=end

require 'csv'

TimezoneValue = Struct.new(:offset, :dst)

ZIP_TZ_MAP = {}
STATE_TZ_MAP = {}

CSV::foreach(File.join(Rails.root, 'config/data/zipcode_tz_map.csv'), headers:['zip' 'city' 'state' 'latitude' 'longitude' 'offset' 'dst']) do |row|
  zip = row['zip']
  next if zip.blank?#Skip blank rows, which are present in the file for better human readability.
  state = row['state']
  offset = row['offset'].to_i
  dst = row['dst'] == '1' ? true : false
  ZIP_TZ_MAP[zip] = TimezoneValue.new(offset, dst)
  # duplication in case person record provides state and no zip
  # if state has multiple timezones, default to earlier one
  STATE_TZ_MAP[state] = TimezoneValue.new(offset, dst) unless STATE_TZ_MAP.has_key?(state) && STATE_TZ_MAP[state].offset<offset
end
