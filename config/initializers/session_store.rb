# Be sure to restart your server when you modify this file.
Clu2::Application.config.session_store :cookie_store, key: '_clu2_session', httponly: false, domain: :all, tld_length: 2

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Clu2::Application.config.session_store :active_record_store
