class ActiveRecord::ConnectionAdapters::Mysql2Adapter
  #The following is needed because MySQL's new default is to not allow primary keys to default to NULL.
  #When we update rails to 4+, this can be removed.
  NATIVE_DATABASE_TYPES[:primary_key] = "int(11) auto_increment PRIMARY KEY"
end
