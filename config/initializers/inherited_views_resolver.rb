

class InheritedViewsResolver < ::ActionView::FileSystemResolver

  def initialize path
    super(path)
  end

  def find_templates(name, prefix, partial, details)

    klass = "#{prefix}_controller".camelize.constantize rescue nil

    return [] unless klass
    return [] unless klass.ancestors.include? ActionController::Base
    return [] if klass.ancestors.first == ActionController::Base

    #first check for the template in the default location
    Rails.logger.info "Resolving path:\n\tname: #{name}\n\tprefix: #{prefix}\n\tpartial: #{partial}\n\tdetails: #{details}"
    templates = super(name, prefix, partial, details)
    if templates.present?
      Rails.logger.info "Found the file.\n"
      return templates
    end
    Rails.logger.info "Default template is not present."
    if partial==false
      templates = super(name, prefix, true, details)

      if templates.present?
         Rails.logger.info "A partial is present. Using that instead.\n"
         #can't seem to get/set controller instance variables or the request object from here
        return templates
      end
    end
    return []

    #gsub makes the below code break:

    #then set up the recursive call
    ancestor = klass.ancestors.second
    ancestor_prefix = ancestor.name#.gsub(/Controller$/, '').underscore

    find_templates(name, ancestor_prefix, partial, details)
  end
end

ActiveSupport.on_load(:action_controller) do
    view_paths.each do |path|
        append_view_path(InheritedViewsResolver.new(path.to_s))
    end
end

