if Rails.env.test?
  require 'excon'
  Excon.defaults[:ssl_version] = :TLSv1
end