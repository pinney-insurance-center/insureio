require 'lograge/sql/extension'

Clu2::Application.configure do
  config.lograge.enabled = true

  config.lograge.keep_original_rails_log = true
  config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/#{Rails.env}_via_lograge.log"

  config.lograge.custom_options= lambda do |event|
    p=event.payload
    ignorable_params=['controller', 'action','format']
    params= p[:params].reject{|k| ignorable_params.include?(k) }

    {
      ip:                       p[:remote_ip],
      time:                     event.time,
      app_server_name:          p[:app_server_name],
      true_current_user_id:     p[:true_current_user_id],
      current_user_id:          p[:current_user_id],
      current_user_auth_method: p[:current_user_auth_method],
      tenant_id:                p[:tenant_id],
      current_user_acts_across_tenancies: p[:current_user_acts_across_tenancies],
      referrer:                 p[:referrer],
      headers:                  p[:headers],
      params:                   params,
      level:                    event.payload[:level],
    }
  end
  config.lograge.formatter = Lograge::Formatters::Json.new#basically, format as JSON.

  # Instead of extracting event as Strings, extract as Hash. You can also extract
  # additional fields to add to the formatter
  config.lograge_sql.extract_event = Proc.new do |event|
    regex_to_prettify_sql=[/(\s+)(FROM|SET|INNER JOIN|LEFT JOIN|WHERE|ORDER|LIMIT|\/\*)/, "\n\\2"]
    {
      name: event.payload[:name],
      duration: event.duration.to_f.round(2),
      sql: event.payload[:sql].gsub(*regex_to_prettify_sql)
    }
  end
  # Format the array of extracted events
  config.lograge_sql.formatter = Proc.new do |sql_queries|
    sql_queries
  end

end