class Array
  def search(&block)
    self.each do |item|
      return item if block.call(item)
    end
    nil
  end
end