Sidekiq.configure_server do |config|
      config.redis = { url: "redis://#{APP_CONFIG['socket']['redis_host']}:#{APP_CONFIG['socket']['redis_port']}/0",
        namespace: "#{APP_CONFIG['socket']['redis_channel_for_worker_processes']}_#{Rails.env}" }
      config.options[:job_logger] = Logger
end

Sidekiq.configure_client do |config|
      config.redis = { url: "redis://#{APP_CONFIG['socket']['redis_host']}:#{APP_CONFIG['socket']['redis_port']}/0",
        namespace: "#{APP_CONFIG['socket']['redis_channel_for_worker_processes']}_#{Rails.env}" }
end

Sidekiq::Logging.logger = Logger.new("#{Rails.root}/log/sidekiq.log", 2, 1024*100)
