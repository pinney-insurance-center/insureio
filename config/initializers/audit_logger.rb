=begin
  Usage:

    require 'audit_logger'

    class MyClass
      include AuditLogger::Helpers
      audit_logger_name 'my-class'

      # It works in instance methods
      def foo
        # Standard logging
        audit # => "#{self.class}[id:#{id}]##{method}:#{line}"
        
        # Log with a message
        audit 'this message' # => "#{self.class}[id:#{id}]##{method}:#{line}\nthis message"
        
        # If you want to log to a different logfile than the current class default:
        audit log:'mylogger.log'

        # There are several options you can pass in
        audit log:'mylogger.log', body:'this message', level:'error'

        #You can also pass in structured data, which will output as JSON.
        #This should help to keep data bitesized and digestible.
        #`vim` can apply syntax highlighting with command `:set syntax=json`,
        #but will not prettify with whitespace.
        #`jq` is a good cli JSON viewing tool with color and whitespace.
        audit level:'info', body:{ the_foo: self.serializable_foo, the_bar: self.bar, other_relevant_things: my_array_of_things }
      end

      # It works in class or static methods
      def self.foo
        audit # => "#{self}::#{method}:#{line}"
      end

    end

=end

class AuditLogger < Logger
  cattr_accessor :audit_loggers
  self.audit_loggers = {}
  
  at_exit { AuditLogger.close_all }

  # AuditLogger.[] is the preferred creation method
  def initialize(name, *args)
    # make sure to track any creations
    super
    self.class.audit_loggers[name] = self
  end

  def puts(*args); info args.join "\n\t"; end

  def tabs(*args); info args.join "\t"; end

  def self.file_name(name)
    Rails.root.join('log', "#{name.to_s.downcase}.log").to_s
  end

  def self.[](name)
    name = file_name(name)
    audit_loggers[name] || begin
      # log rotation: keep 25 old log files with 20MB
      AuditLogger.new(name, 'weekly')
    end
  end

  def self.close_all
    audit_loggers.values.each do |name, logger|
      if logger
        logger.close rescue nil # Ignore
      end
    end

    audit_loggers.clear
  end

  # This overrides a private method in `Logger`.
  # Somewhere between methods debug/info/warn/error and this one, message is forced to be a string.
  def format_message(severity, timestamp, progname, msg)
    #"#{timestamp.to_formatted_s(:db)} #{severity} #{msg}\n"
    msg_obj=JSON.parse(msg) rescue nil
    output_obj={level: severity, time: timestamp}
    if msg_obj && msg_obj.is_a?(Hash)
      output_obj=output_obj.merge(msg_obj)
    else
      output_obj[:message]=msg
    end
    "#{output_obj.to_json}\n"
  end

  module Helpers
    @audit_logger_name = 'default_audit'

    private

    def audit(*args)
      metadata={
        class_name:  self.class.name,
        id:          (self.respond_to?(:id) ? id : nil)
      }
      self.class._audit metadata, args
    end

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      def audit(*args)
        metadata={
          class_name:  self.name,
        }
        _audit metadata, args
      end

      def audit_logger_name name
        @audit_logger_name = name
      end

      # Collate metadata, stack data, and options extracted from args (a hash or array of alternating keys and values).
      # Allowed options: [:body, :log, :level]
      # Inferred options: [:body]
      def _audit metadata, args
        output_obj = metadata
        line_num, method_name = _stack_data 2
        method_prefix = metadata.has_key?(:id) ? '#' : '::'#indicates class or instance method
        output_obj[:method]=method_prefix+method_name
        output_obj[:line]=line_num

        thread_vars_to_capture=[:id, :current_user_id, :current_api_user_id, :tenant_id, :current_user_acts_across_tenancies]
        thread_vars_to_capture.each{|x| output_obj[x]=Thread.current[x] if !Thread.current[x].nil? }

        # set options
        options = args.extract_options!
        options[:body]  ||= options[:message] || options[:text] || args
        options[:log]   ||= @audit_logger_name        
        options[:level] ||= :info
        # handle case where options[:body] is an Exception
        if options[:body].respond_to? :backtrace
          app_backtrace=Rails.backtrace_cleaner.clean( options[:body].backtrace )
          options[:body] = { message: options[:body].message, backtrace: app_backtrace }
        end
        # build logger output
        output_obj[:body]=options[:body]||''
        if output_obj[:body].is_a?(Hash)
          output_obj=output_obj.merge output_obj.delete(:body)
        end
        AuditLogger[ options[:log] ].send options[:level], output_obj.to_json
      end

      # Returns method and line number from backtrace
      def _stack_data caller_steps=0
        match_data = caller[caller_steps].match /:(\d+):in `(.+)'/
        return [match_data[1], match_data[2]]
      end
    end

  end

end 
