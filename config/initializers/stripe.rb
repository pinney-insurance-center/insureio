Stripe.api_key = APP_CONFIG['stripe']['secret_key']
Rails.configuration.stripe = {
  :publishable_key => APP_CONFIG['stripe']['publishable_key'],
  :secret_key      => APP_CONFIG['stripe']['secret_key']
}
