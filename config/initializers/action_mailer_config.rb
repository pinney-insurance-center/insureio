Clu2::Application.configure do

  config.action_mailer.raise_delivery_errors = APP_CONFIG['action_mailer'].try(:fetch, 'raise_delivery_errors') || true

  delivery_method = APP_CONFIG['action_mailer'].try(:fetch, 'delivery_method')
  # Don't override the environment-specific setting if there is no config-specific setting
  config.action_mailer.delivery_method = delivery_method if delivery_method
  config.action_mailer.delivery_method ||= :smtp

  config.action_mailer.smtp_settings = APP_CONFIG['action_mailer_smtp'].try(:symbolize_keys)

end