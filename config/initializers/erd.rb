#We only load the gem in development.
if (Rails.env.development? || Rails.env.dev?) && defined?(RailsERD)
  #This patch is needed for the `rails-erd` gem to properly create it's diagrams.
  #See https://github.com/voormedia/rails-erd/issues/70#issuecomment-63645855
  module RailsERD
    class Domain
      class Relationship
        class << self
          private

          def association_identity(association)
            Set[association_owner(association), association_target(association)]
          end
        end
      end
    end
  end
end