if defined?(Rails::Console)
  #Only require for the console process, not the server.
  #In rails 4+ this should be done within a `console do` block, as described in
  #http://guides.rubyonrails.org/configuring.html#rails-general-configuration
  require 'awesome_print'
  AwesomePrint.defaults={
    # Use Ruby>=1.9 hash syntax (trailing colon, no hash-rocket) in output.
    ruby19_syntax:true,
    # Absolute value is the number of spaces for indentation.
    # Negative makes it left align versus centering keys.
    indent: -2,
  }
  AwesomePrint.irb!
end