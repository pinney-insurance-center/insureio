### Applicate wide constants are defined here
MALE = true
FEMALE = false


TIME_ZONES = ActiveSupport::TimeZone.us_zones() + ['American Samoa', 'Guam', 'Atlantic Time (Canada)' ].map { |e| ActiveSupport::TimeZone.new(e) }

PINNEYQUOTER_SITE = "https://quoter.pinneyapps.com"

BP_YEARS_OPTIONS = [
  ["6 Months or Less", "0.5"],
  ["1 Year or Less", "1"], 
  ["2 Years", "2"],
  ["3 Years", "3"],
  ["4 Years", "4"],
  ["5 Years", "5"],
  ["6 Years", "6"],
  ["7 Years", "7"],
  ["8 Years", "8"],
  ["9 Years", "9"],
  ["10 Years", "10"],
  ["11 Years", "11"],
  ["12 Years", "12"],
  ["13 Years", "13"],
  ["14 Years", "14"],
  ["15 Years or More", "15"]
]

CHOLESTEROL_YEARS_OPTIONS = BP_YEARS_OPTIONS

#Using larger steps for larger numbers produces a more managable list.
#Specifically, the below produces 50 options,
#whereas stepping through the entire range in increments of 25k would produce 360 options.
FACE_AMOUNT_OPTIONS=(
  (
    (25..175).step(25).to_a+
    (200..950).step(50).to_a+
    (1000..1900).step(100).to_a+
    (2000..4750).step(250).to_a+
    (5000..9000).step(1000).to_a
  ).map{|x| x=x*1000; ['$'+x.to_s(:delimited),x] }
)

#according to Top Level Domain Name Specification, March 2009
#https://tools.ietf.org/html/draft-liman-tld-names-00
VALID_TLD_PATTERN='[a-z]{1}([a-z0-9]|\-){0,61}[a-z0-9]{1}'
VALID_URL_PATTERN='\A(http|https):\/\/([^\./]+\.)+'+VALID_TLD_PATTERN+'(\/.*)?\z'
VALID_URL_PATTERN_JS='^(http|https):\/\/([^\./]+\.)+'+VALID_TLD_PATTERN+'(\/.*)?$'


#These values are used by browsers to ensure subresource integrity (SRI),
#generating a network error and refreshing the resource if the integrity check fails.
#`cat` command will follow symlinks, so filepath can be the path of a symlink to the actual resource.
def generate_sri_hash file_name
  compiled_assets_directory="#{Rails.root}/public/assets/"
  return '' unless Dir.exists?(compiled_assets_directory)
  file_path=compiled_assets_directory+file_name
  stdout, stderr, status = Open3.capture3 "cat #{file_path} | openssl dgst -sha256 -binary | openssl base64 -A"
  unless 0 == status && stderr.blank?#if cat fails, openssl will still digest the blank output, so also check stderr
    msg="openssl base64 exited with #{status} for asset file \"#{file_name}\".\n#{stderr}\n#{stdout}"
    if Rails.const_defined?('Console') || Rails.const_defined?('Server')
      raise RuntimeError.new msg
    else#don't require this to work for rake tasks
      Rails.logger.warn msg
    end
  end
  prefixed_hash="sha256-#{stdout}"
end
#In future, we may wish to include login.js, signup.js, rtt_quote_app.js in this set,
#but for now I'm excluding those, since the pages which reference them are static html.
#Those would either need to be re-generated each time sunresources change, or else left as is.
SRI_HASHES={
  widgets_js: generate_sri_hash('quoting_widget.js'),
  dep_js:     generate_sri_hash('dependencies.js'),
  app_js:     generate_sri_hash('application.js'),
  dep_css:    generate_sri_hash('dependencies.css'),
  app_css:    generate_sri_hash('application.css'),
}
