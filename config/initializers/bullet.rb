
if Rails.env!=:production
  filepath="config/data_sensitive_or_machine_specific/bullet.yml"
  if File::exist?(filepath)
    bullet_config_opts=YAML::load_file filepath
  end
  if defined?(bullet_config_opts) && bullet_config_opts.present?
    require 'bullet'
    bullet_config_opts.each do |opt,value|
      Bullet.send("#{opt}=",value)
    end
  end
end