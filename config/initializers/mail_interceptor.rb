require 'mail'
Mail.register_interceptor(QaMailInterceptor) unless Rails.env.development? || Rails.env.production?
