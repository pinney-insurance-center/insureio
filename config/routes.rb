Clu2::Application.routes.draw do

  match "/tos",    :controller => "users",         :action => "tos", via:[:get]
  match "/saml_metadata", :controller => "user_sessions", :action => "saml_metadata", via:[:get]
  match "/sso",    :controller => "user_sessions", :action => "sso", via:[:get,:post]
  match "/logout", :controller => "user_sessions", :action => "destroy", :via => [:get, :delete]
  match "/unload", controller: 'user_sessions', action:'unload', via:[:get]
  match "/marketech/updates", controller: 'processing/marketech', action:'update', :via => [:get, :post]#DEPRECATED. Will remove when they start sending to direct_update.
  match "/marketech/direct_update", controller: 'processing/marketech', action:'direct_update', :via => [:get, :post]

  match '/sso/p512_42', :controller => 'sso', :action => 'p512_42', via:[:get]

  #Temporary backward compatibility for API users:
  match "/crm/connections", controller: 'consumers', action: 'create', via: [:post]

  #Forward compatibity for when controllers and paths are eventually simplified.
  match "/quoting/get_quotes.json",   :controller => "quoting/quotes",         :action => "get_quotes", via:[:post]
  match "/get_quotes.json",           :controller => "quoting/quotes",         :action => "get_quotes", via:[:post]
  match "/marketing/messages/:id/log_and_redirect", :controller => "marketing/email/messages", :action => "log_and_redirect", via:[:get]
  match "/messages/:id/log_and_redirect",           :controller => "marketing/email/messages", :action => "log_and_redirect", via:[:get]
  match "/marketing/messages/:id/tracking_pixel",   :controller => "marketing/email/messages", :action => "tracking_pixel", via:[:get]
  match "/messages/:id/tracking_pixel",             :controller => "marketing/email/messages", :action => "tracking_pixel", via:[:get]
  match "/messages/:id",                            :controller => "marketing/messages",       :action => "show", via:[:get]

  match "*path", to: 'quoting/widgets#pre_flight_response', via:[:options]

  # TOP-LEVEL RESOURCES

  resources :carriers, only:[:index, :show]

  resources :dashboard do
    collection do
      get 'ezl_cases'
      get 'tasks'
      get 'opportunities'
      get 'unassigned'
      get 'background_jobs'
      get 'view_slow_queries'
      get 'kill_slow_queries'
      get 'view_recent_server_logs'
      get 'view_recent_request_stats'
      get 'view_recent_automated_job_stats'
    end
  end

  resources :ixn, only: [:create] do
    post :alq_save, on: :collection
    member do
      put :esp
      get :download
      get :pdf
      get :xml
      put :generate_pdf
    end
  end

  resources :resource_libraries, only:  [] do
    collection do
      get 'ask'
      get 'guides'
      get 'questionnaires'
      get 'table_shave'
      get 'xrae'
      get 'calculators'
      get 'contracting'
      get 'planning'
      get 'productivity'
      get 'quoters'
      get 'training'
      get 'marketplace'
    end
  end

  resources :help do
    collection do
      get 'faq'
      get 'tutorials'
      get 'about'
      get 'suggestions'
      post 'post_suggestion'
    end
  end

  resources :rtt, only:[:index] do
    collection do
      get  'recent_apps'
      get  'agent_summary'
      #All of these actions require a guid,
      #but for readability they keep it out of the path,
      #and instead read it from the query string.
      post 'save'
      post 'save_and_sign_for_consumer'
      get  'basic_info_wrapper'
      get  'health_questions_wrapper'
      get  'msg_signature_required_owner'
      get  'msg_signature_required_owner_payer'
      get  'msg_signature_required_payer'
      get  'msg_resume_app'
    end
    member do
      put  'verify_stakeholder'
      put  'stakeholder_sign'
      put  'stakeholder_decline'
      get  'poll_for_underwriting_decision'
      get  'app'
      get  'app_forms_only'
      get  'full_scor_underwriting_response'
      put  'force_send_for_underwriting'
      put  'force_transfer_xml'
      put  'deliver_resume_app_email'
    end
  end

  resources :contacts, only:[:index,:update] do
    collection do
      get 'unsubscribe_from_mktg_step_one'
      get 'unsubscribe_from_mktg_step_two'
      get 'unsubscribe_from_status_step_one'
      get 'unsubscribe_from_status_step_two'
      get 'search_by_phone'
      get 'crm_settings_for_3cx'
    end

    member do
      get 'marketing'
      get 'marketing_history_and_to_do'
      post 'send_sms'
    end
  end

  resources :addresses,       controller:'address',       only:[:create,:update,:destroy]
  resources :emails,          controller:'email_address', only:[:create,:update,:destroy]
  resources :email_addresses, controller:'email_address', only:[:create,:update,:destroy]
  resources :webs,            controller:'web',           only:[:create,:update,:destroy]
  resources :phones,          controller:'phone',         only:[:create,:update,:destroy]


  resources :attachments

  resources :smtp_servers do
    collection do
      post 'test_connection'
      put  'test_connection'
    end
  end

  resources :user_sessions, :only => [:new, :create, :destroy] do
  end

  resources :users, only: [:create, :update, :destroy, :index, :show] do
    collection do
      get 'show_my_api_key'
      post 'process_signup'
      get 'payment_info'
      get 'agents'
      get 'assign_agent'
      get 'common_tags'
      get 'end_impersonate'
      get 'set_security_questions'
      get 'answer_security_question'
      get 'descendant_names'
      get 'membership_levels'
      put 'put_tos'
      get 'names'
      get 'editable_user_ids'
      get 'hierarchy_mgmt'
      get 'hierarchy_branch'
      get 'child_and_descendant_counts'
      put 'update_aml_vendor'
      post 'add_security_question'
      post 'confirm_security_question'
      post 'mass_update'
      post 'member_brand_names'
    end

    member do
      get 'agent_for_quote_path'
      get 'brand_ids'
      get 'marketing'
      get 'impersonate'
      put "update_password"
      get 'lead_distribution_rules'
      get 'lead_types'
      get 'staff_assignment'
      post 'add_common_tags'
      post 'tag'
      get 'recruit_for_user'
      get 'hierarchy_branch'
      get 'hierarchy_breadcrumb'
    end
  end

  resources :consumers do
    collection do
      get 'financial'#deprecated
      get 'tracking'#deprecated
      get 'csv'
      post 'batch_import'
      get 'names'
    end
    member do
      post 'tag'
      post 'revert'
      get 'marketing'
      get 'financial'#deprecated
      get 'tracking'#deprecated
      get 'related'
      get 'suggested_relations'
      get 'existing_coverage'
      get 'primary_contact'
      get 'spouse'
      get 'tasks'#deprecated
      get 'log_contact_and_call'
      get 'policy_and_opportunity_ids'
    end
  end

  resources :brands do
    collection do
      post 'select'
      post 'change_owner'
      get  'assignments'
      get  'autocomplete'
      post 'update_assignments'
    end
    member do
      get  'agents'
      get  'lead_type_ids'
      get  'members'
      get  'member_ids'
    end
  end

  resources :notes

  resources :phone_recordings

  resources :tags

  resources :tasks do
    collection do
      get 'follow_up_names'
    end
    member do
      put 'inactive_task'
    end
  end

  # NAMESPACES

  namespace :lead_distribution do
    resources :user_rules, only: [:index, :update, :show] do
      collection do
        put 'update_multiple'
      end
    end
    resources :cursors, only: [:create, :show] do
      member do
        get 'reset_counts'
      end
      collection do
        get 'join_tables'
      end
    end
  end

  namespace :usage do

    resources :recruitment, :only => [:index] do
      collection { post 'user_invitation' }
    end

    resources :stripe, only: [] do
      collection do
        post 'create_event'
        post 'create_customer'
        post 'update_plan'
        post 'update_payment_source'
        get 'unsubscribe'
      end
    end

    resources :password_resets do
      collection do
        get 'successfully_sent'
        get 'link_expired'
      end
    end

    resources :contracts, except: [:new,:edit,:show]

    resources :licenses, except: [:new,:edit,:show]

    resources :staff_assignments, only: [:index, :show, :create, :update]

  end

  namespace :crm do

    resources :cases do
      collection do
        post 'create_details' # this needs to be refactored
        get 'index'
        get 'requirements' # this needs to be refactored out in favour of member
        get 'follow_up' # this needs to be refactored out in favour of member
        get 'exam' # this needs to be refactored out in favour of member
        post 'update_quote_param'
        get 'admin'
      end
      member do
        put 'update'
        put 'exam_update'
        get 'ezlink'
        get 'details'
        get 'case_nav'
        get 'beneficiaries'
        get 'notes'
        get 'requirements'
        get 'beneficiaries'
        get 'admin'
        get 'involved_parties'
      end
    end

    resources :consumer_relationships

    resources :financial_infos

    resources :lead_types


    resources :referrers

    resources :sources

    resources :statuses do
      get 'partial', on: :member
    end

    resources :status_types do
      collection do
        get 'status_type_names'
      end
      member do
        post 'adjust_sort_order'
      end
    end

    resources :requirements

    resources :task_builders
  end

  resources :processing, only:[] do
    #The member in question here is the Crm::Case.
    member do
      get  'exam_one_control_codes'
      post 'schedule_with_exam_one'
      post 'submit_to_exam_one'
      post 'submit_to_smm'
      post 'submit_to_agency_works'
      post 'submit_to_apps'
      post 'submit_to_apps_121'
      post 'submit_to_docusign'
      post 'submit_to_igo'
      post 'submit_to_marketech'
      post 'submit_to_app_assist'
    end
  end

  namespace :processing do
    namespace :docusign do
      resources :case_data
    end
    resources :marketech, only:[] do
      member do
        get  'email'
        post 'email'
      end
      collection do
        get  'export'
        get  'import'
      end
    end
    resources :apps, only:[] do
      collection do
        get  'status'
        post 'status'
      end
    end
  end

  namespace :marketing do
    get "links"

    resources :templates do
      member do
        post 'deliver'
        post 'gauge_size_of_delivery'
      end
    end

    resources :subscriptions, except:[:update] do
      collection do
        post 'update_order_for_queue'
      end
    end

    resources :campaigns do
      get 'task_builders', on: :member
      post 'subscribe_consumers', on: :member
    end

    resources :messages

    namespace :email do
      resources :messages do
        member do
          post 'send_message'
          get  'tracking_pixel'
          get  'log_and_redirect'
        end
      end
    end

    namespace :snail_mail do
      resources :messages
    end

    namespace :sms do
      resources :templates do
      end

      resources :messages do
        member do
          post 'send_sms'
        end
      end
    end

    resources :task_builders
  end

  namespace :quoting do

    resources :widgets do
      collection do
        get  :form_for_tli
        get  :form_for_ltc
        get  :form_for_rtt
        get  :form_for_cap
        get  :edit_modal
        get  :tunnel_for_local_storage
      end
      member do
        post :track_widget_view
      end
    end

    resources :quotes do
      member do
        put  'simple_update'
      end
      collection do
        post 'get_quotes'
        post 'email'
        post 'save_from_quote_path'
      end
    end

    resources :relatives_diseases
  end

  namespace :reporting do
    resources :searches, except:[:new, :edit] do
      collection do
         get 'results'
         post 'results'
         put 'results'
         get 'search_policy_create_date'
         get 'reporting_tabs'
         get 'fields_to_show_whitelist'
         get 'criteria_fields_whitelist'
      end
      member do
        get 'results'
        post 'results'
        put 'results'
        get 'get_consumers_for_subscription'
      end
    end
    resources :production_summaries
  end


  match "/cookies", :controller => "cookies", :action => "delete", via:[:get,:put,:delete]

  resources :sockets, only:[] do
    collection do
      post 'publish'
    end
  end

  get "/motorists" => redirect(path: "/rtt")

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'dashboard#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'

end
