set :rails_env, 'production'
set :deploy_to, "/var/www/dataraptor/"
set :branch, fetch(:branch, 'master')

server '66.228.51.251:4000', :app, :web, :db, :primary => true # dataraptor-app-1
server '45.33.17.105:4000', :app, :web # dataraptor-app-2
server '45.33.17.141:4000', :app, :web # dataraptor-app-3

# configure whenever
set :whenever_command, "bundle exec whenever"
set :whenever_environment, defer { stage }
set :whenever_identifier, defer { "#{application}_#{stage}" }
require "whenever/capistrano"

set :rvm_type, :system
set :rvm_ruby_string, 'ruby-2.3.0@insureio'
