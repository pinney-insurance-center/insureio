set :application, 'release'
set :deploy_to, "/var/www/release.insureio/"
set :rails_env, 'release'

set :branch, fetch(:branch, 'release')
set :user, 'deploy'

server '72.14.183.31:4000', :app, :web, :db, :primary => true

after 'deploy:migrate', "deploy:restart"

set :rvm_type, :system
set :rvm_ruby_string, 'ruby-2.3.0@release.insureio'

namespace :deploy do
  task :restart do
    run "cd #{current_release} && touch tmp/restart.txt"
  end
end
