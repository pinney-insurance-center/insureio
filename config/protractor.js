/*Reference
https://github.com/angular/protractor/blob/5.4.1/lib/config.ts
*/

const HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
  directConnect: true,
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 4,
    chromeOptions: {
      args: [ "--headless", "--disable-gpu", "--window-size=1920,1080"],
      binary: require('puppeteer').executablePath(),
    },
  },
  specs: [
    '../spec/javascripts/protractor/*-spec.js',
    '../spec/javascripts/protractor/**/*-spec.js',
  ],
  baseUrl: 'http://pinney.local.com:3000',
  params: {
    /* See rake task db:seed:protractor for these params. They should be an exact match. */
    login: { user: 'grace', password: 'qwer1234', brandId: 1, id: 1, quoter_key: 'thisismyquoterkey' },
    nmbUser: { user: 'nmb', password: 'qwer1234', brandId: 1, id: 10, quoter_key: 'thisismyquoterkey' },
  },
  getPageTimeout: 4*60*1000, // Increase default timeout
  allScriptsTimeout: 6*60*1000, // Increase default timeout
  onPrepare: function() {
    // Add a screenshot reporter and store screenshots
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'spec/screenshots',
      takeScreenShotsOnlyForFailedSpecs: true
    }).getJasmine2Reporter());
    /* Add globals */
    global.requireHelper = (pathSuffix) => ( require(__dirname + '/../spec/javascripts/helpers/' + pathSuffix) );
  },
}
