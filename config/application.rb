require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require( :default, Rails.env )
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

# initalize APP_CONFIG constant
require File.expand_path('../load_config.rb', __FILE__)
module Clu2
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.middleware.insert_before 'Rack::Runtime', 'CatchUnsupportedCharacters'
    config.middleware.insert_before 'Rack::Runtime', "CatchQueryParseErrors"
    config.middleware.insert_before 'ActionDispatch::ParamsParser', "CatchJsonRequestContentParseErrors"
    config.middleware.insert_before 'Rack::Runtime', 'RemoveUnsupportedIpHeader'
    config.middleware.insert_before 'Rack::Runtime', 'RemoveThreadAnnotations'
    config.middleware.insert_before 'Rack::Runtime', 'DefaultContentType'
    config.middleware.insert_before 'ActiveRecord::ConnectionAdapters::ConnectionManagement', 'LogRequestsForReplay'

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{config.root}/lib/workers)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    # Warning: Strings and symbols will be compared using "fuzzy" matching.
    # For stricter handling (strongly recommended), use regular expressions.
    config.filter_parameters += [/^ssn$/,/^encrypted_ssn$/,/^tin$/,/^encrypted_tin$/,/^dln$/,/^eft_/,:password, /_data_url$/]

    # Enable escaping HTML in JSON.
    config.active_support.escape_html_entities_in_json = true

    #Different places use different config variables to compose urls, so setting all 3 of them here for consistency.
    config.action_mailer.default_url_options = APP_CONFIG['default_url_options']
    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql

    # Enable the asset pipeline
    config.assets.enabled = true
    config.assets.paths << Rails.root.join('vendor', 'assets')

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    config.generators do |g|
      g.fixture_replacement :factory_bot
      g.stylesheets false
      g.test_framework false
      g.javascripts false
      g.helper false
    end

    config.cache = ActiveSupport::Cache::MemoryStore.new
  end
  class Error < StandardError; end
end
