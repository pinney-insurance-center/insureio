Clu2::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.perform_deliveries = true

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Do not compress assets
  #config.assets.js_compressor = :uglifier

  # Don't fallback to assets pipeline if a precompiled asset is missed
  # "true" allows assets to be compiled on the fly.
  config.assets.compile = true

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # So basically, don't precompile by default, but if someone manually precompiles, include this file.
  config.assets.precompile += %w( dependencies.js dependencies.css login.js signup.js quoting_widget.js rtt_quote_app.js)

  # Generate digests for assets URLs
  config.assets.digest = true

  # Expands the lines which load the assets
  config.assets.debug = true
  
  config.colorize_logging=false

  #ActiveResource::Base.logger = Logger.new(STDERR)

  config.eager_load=false

end
