Clu2::Application.configure do
  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false
  config.serve_static_files= false

  # Compress JavaScripts and CSS
  config.assets.compress = false

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = false

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # So basically, don't precompile by default, but if someone manually precompiles, include this file.
  config.assets.precompile += %w( dependencies.js dependencies.css login.js signup.js quoting_widget.js rtt_quote_app.js)

  # Generate digests for assets URLs
  config.assets.digest = true

  # See everything in the log (default is :info)
  config.log_level = :debug

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  #config.active_support.deprecation = :notify

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  config.logger = Logger.new("#{Rails.root}/log/#{Rails.env}.log", 25, (2097152*50))

  config.eager_load=false

  # ExceptionNofitier
  # email_config = YAML::load_file(File.join Rails.root, 'config.yml')['all']['email']
  config.middleware.use ExceptionNotification::Rack,
    :email => {
      :email_prefix => "[DataRaptor ExceptionNotifier] ",
      :sender_address => %{"DR #{Rails.env}" <#{APP_CONFIG['email']['sender']}>},
      :exception_recipients => [APP_CONFIG['email']['developers']]
    }

end
