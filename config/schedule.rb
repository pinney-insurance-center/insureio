# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# In RedHat/CentOS, the directory is located at: /var/spool/cron/

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
set :nice, 11
job_type :rake, "cd :path  && rvm use @insureio && RAILS_ENV=:environment nice -n :nice bundle exec rake :task --silent"
job_type :mono_rake, "ps x -o command | grep \"dataraptor.*rake :task\" | grep -q -v grep || cd :path && RAILS_ENV=:environment nice -n :nice bundle exec rake :task --silent"

every '0,15,30,45 5,8,11,12,13,14,15,16,17,19,21,22 * * *' do
  rake "agency_works:get_updates"
end

every 3.hours do
  rake "esp:update"
end

every 1.day, :at => '11:30 pm' do
  rake "lead_distribution_counts:reset"
end

every 15.minutes do
  rake "exam_one:get_statuses"
end

every 15.minutes do
  rake "rtt:transfer_xml_files"
end

every 15.minutes do
  rake "execute_past_due_tasks", nice:19
end

every 1.days do
  rake "agency_works:uat_test_updates"
end

every 3.hours do
  rake "sbli:update"
end
