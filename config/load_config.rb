# This file loads the data from <Rails.root>/config/*.yml
# It is called from `application.rb` so it can be run before regular initializers.
# Some other initializers make use of the data loaded here.

# The APP_CONFIG constant
APP_CONFIG = {}.with_indifferent_access

def get_universal_and_environmental_config(filepath)
  yaml = YAML::load(ERB.new(File.read filepath).result)
  return {} unless yaml.is_a?(Hash)
  if yaml.keys.any?{|k| %w[all test design_testing dev development release production].include?(k) }
    # get config for all enviroments
    universal = yaml['all'] || {}
    # determine which key to use for environment config values
    env = Rails.env
    env = 'development' if Rails.env == 'test' and !yaml.has_key?('test') # default to development
    env = 'production'  if !yaml.has_key?(env) && !universal.present?# default to production
    environmental = yaml[env] || {}
    # merge universal with environmental config
    HashWithIndifferentAccess.new universal.merge(environmental)
  else
    yaml
  end
end

files=(Dir[File::expand_path "../data/*.yml", __FILE__]+Dir[File::expand_path "../data_sensitive_or_machine_specific/*.yml", __FILE__])
files.reject{|f| f =~ /database.yml|phones_blacklist.yml/ }
.each{ |file|
  begin
    config = get_universal_and_environmental_config file
    slug = File::basename(file).sub(/\.yml$/,'')
    APP_CONFIG[slug] = config.with_indifferent_access
  rescue => ex
    puts "Exception for file #{file}"
    raise ex
  end
}

#Handle enums.json.
json_enums=JSON.parse( File.read(File::expand_path("config/data/enums.json")))
APP_CONFIG['enums']=json_enums

# Handle config.yml
APP_CONFIG.merge! APP_CONFIG['config']
APP_CONFIG.delete 'config'

# Handle passwd.yml.
APP_CONFIG.merge! APP_CONFIG['passwd']
APP_CONFIG.delete 'passwd'

# Add term life insurance face amounts to app config
#These values should eventually be used by quoting widgets and the quote path for a standardized list.
APP_CONFIG['face_amounts'] = (
    amounts = Array.new
    25_000.step(100_000,25_000){|x|amounts<<x}
    150_000.step(1_000_000,50_000){|x|amounts<<x}
    1_100_000.step(2_000_000,100_000){|x|amounts<<x}
    2_250_000.step(5_000_000,250_000){|x|amounts<<x}
    amounts
    )

#Storing min and max versus long arrays of values allows the `APP_CONFIG` list to be shorter,
#and hopefully easier to skim over in the console.
#These values should eventually be used by quoting widgets and the quote path for standardized lists.
APP_CONFIG['cholesterol_min']=100
APP_CONFIG['cholesterol_max']=299
APP_CONFIG['cholesterol_hdl_min']=2.5
APP_CONFIG['cholesterol_hdl_max']=9.9
APP_CONFIG['cholesterol_hdl_step']=0.1
APP_CONFIG['bp_systolic_min']=119
APP_CONFIG['bp_systolic_max']=249
APP_CONFIG['bp_diastolic_min']=79
APP_CONFIG['bp_diastolic_max']=179
APP_CONFIG['assumed_interest_rate_min']=100
APP_CONFIG['assumed_interest_rate_min_max']=299


APP_CONFIG['default_url_options']=APP_CONFIG['default_url_options'].symbolize_keys
#This key can be used to compose urls via string interpolation within models.
if url_opts=APP_CONFIG['default_url_options']
  APP_CONFIG['base_url']="http#{Rails.env=='production' ? 's' : ''}://#{url_opts[:host]}#{':'+url_opts[:port].to_s if url_opts[:port].present?}"
end

#Validate presence of essential config data
#Note: Some configs for dev environments are optional, including "bullet" and "chat".
expected_top_level_keys=%w[old_encryption_key encryption_key secret_token 
  redis_executable ca_cert_file base_url id_base_url date_format default_mail_sender dev_email_recipient
  face_amounts
  cholesterol_min cholesterol_max cholesterol_hdl_min cholesterol_hdl_max cholesterol_hdl_step
  bp_systolic_min bp_systolic_max bp_diastolic_min bp_diastolic_max
  assumed_interest_rate_min assumed_interest_rate_min_max]
missing_keys=expected_top_level_keys.select{|k| !APP_CONFIG.has_key?(k) }
expected_nested_keys={
  compulife:[:endpoint],
  sidekiq:[:queues],
  socket:[:nodejs_socket_app_port,:redis_channel_for_socket,:redis_channel_for_worker_processes,:redis_host,:redis_port,:log_level],
  smm:[:web_url,:entity_id],
  igo:[:xml_web_service_url,:post_url,:post_target],
  email:[:admin,:developers,:testers,:sender,:consumer_facing_sender,:notify],
  esp:[:host,:user,:pass,:dst_dir,:src_dir],
  ixn:[:id,:token],
  sbli:[:host,:port,:user,:pass],
  stripe:[:secret_key,:publishable_key],
  docusign:[:default_endpoint_url,:credential_endpoint_url,:server,:email,:user_name,:password,:integrator_key,:account_id,:debug],
  scor:[:test,:production,:pgp_key_id,:pgp_key_email,:ftp],
  exam_one:[:username,:password],
  marketech:[:url_base,:path_start,:upload_action,:retrieve_action,:start_action,:esign_action,:site_key,:legacy_site_key],
  agency_works:[:wsdl,:accounts],
  apps:[:username,:password,:url,:wsdl,:s4r_url,:s4r_wsdl,:s4r_username,:s4r_password,:s4r_account_num],
  default_url_options:[:host,:port],
  action_mailer_smtp:[:address,:domain,:port,:user_name,:password,:authentication,:enable_starttls_auto],
  aws:[:pwd,:s3_key_id,:s3_secret,:s3_bucket,:region],
  xrae:[:url,:api_key],
}
missing_nested_keys={}
expected_nested_keys.each do |group_key,nested_keys|
  group=APP_CONFIG[group_key]
  if group.blank? || ( !group.is_a?(Array) && !group.is_a?(Hash) )
    missing_nested_keys[group_key]=nested_keys
    next
  end
  nested_keys.each do |key|
    if group[key].blank?
      missing_nested_keys[group_key]||=[]
      missing_nested_keys[group_key].push(key)
    end
  end
end
missing_keys.push(missing_nested_keys) if missing_nested_keys.present?
if missing_keys.present?
  msg="Warning: Missing expected config data for #{missing_keys.to_json}.\n"+
      "This will prevent proper functioning of the app modules which rely on that data.\n\n"
  #Rails.logger.warn msg
  puts msg
end
