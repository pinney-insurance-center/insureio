module AuthenticationHelpers

  def current_user use_ostensible=true
    # Set true (authenticated) current user
    @current_user ||= begin
      # Authenticate user
      (key_auth || sess_auth || ltd_auth).tap do |user|
        Thread.current[:true_current_user_id] = user&.id
        Thread.current[:current_user_id] = user&.id
        Thread.current[:current_user_acts_across_tenancies] = !!user&.can_any?(*Usage::Permissions::CROSS_TENANCY_PERMISSIONS)
        use_user_time_zone user
      end
    end
    # Set ostensible (impersonated) user (if any)
    if @current_user && use_ostensible && session[:ostensible_user_id]
      @ostensible_user ||= catch(:abort) { ostensible_user_auth }
      if @ostensible_user.nil?
        # Every failure results in the same message so that we don't leak info across organizations
        custom_header(:error, "Impersonation died. Target User #{session[:ostensible_user_id]} is beyond your access")
        session.delete(:ostensible_user_id)
      end
    end
    # Return user
    use_ostensible ? (@ostensible_user || @current_user) : @current_user
  end

  def true_current_user
    current_user(false)
  end

  # Return an authenticated User (or nil) for this request
  def key_auth
    # Individual actions can set @additional_auth_keys
    key_types = [:api_key] + Array(@additional_auth_keys)
    # Individual actions can set @credentials
    # By default, all our actions which use params use keys [:agent_id, :key],
    # to avoid conflict with RESTful resource ids,
    # and all those that use basic auth header use keys [:id, :key], as specified by RFC.
    # At some point we may want to change to something like params[:auth].values_at(:id,:key).
    # But getting partners on board with API changes is slow.
    creds = @credentials
    creds = extract_basic_auth_credentials unless creds.present? && Array(creds).all?(&:present?)
    creds = params.values_at(:agent_id, :key) unless creds.present? && Array(creds).all?(&:present?)
    return unless creds.present? && Array(creds).all?(&:present?)
    id, key = creds
    #User API Keys are encrypted, so may not be compared at the database layer.
    user=User.unscoped.able_to_log_in.find_by(id: id)
    return if user.nil?
    if matched_key_type = key_types.find{ |which_key| user.send(which_key) == key }
      Thread.current[:current_user_auth]=matched_key_type
    else
      @controller_warnings ||= []
      @controller_warnings += ["Wrong agent id/key combo."]
    end
    matched_key_type && user
  end

  # When an agent sends an email to his/her client, certain links in that email
  # may lead to this application, and the ltd_access_code serves to
  # authenticate+authorize the client and a single other record. Any
  # resources (Consumer, Case, etc) which are accessed during this request are
  # intended to be associated with the accessible record.
  def ltd_auth
    return unless @ltd_auth_enabled # Controller filters can set @ltd_auth_enabled for individual actions
    access_code = params[:ltd_access_code] || session[:ltd_access_code]
    return unless access_code.present?
    user, accessible_record = User.from_access_code access_code
    if user # accessible_record is allowed to be nil
      session[:ltd_access_code] = access_code
      Thread.current[:current_user_auth] = :ltd_access_code
      Thread.current["sess_#{accessible_record.class.name.camelize.downcase}_id".to_sym] = accessible_record&.id
      user
    end
  end

  # Return an authenticated User (or nil) for this request
  def sess_auth
    user = User.unscoped.find_by(id: session[:user_credentials_id])
    user ||= UserSession.find&.record rescue nil
    Thread.current[:current_user_auth] = :session if user
    user
  end

  # Return an ostensible User (or throw :abort) for this request
  def ostensible_user_auth
    User.unscoped.find(session[:ostensible_user_id]).tap do |user|
      throw :abort unless user
      throw :abort unless user.enabled
      throw :abort unless user.editable? current_user(false)
      Thread.current[:current_user_id] = user.id
      use_user_time_zone user
    end
  rescue ActiveRecord::RecordNotFound
    throw :abort
  end

  #This method handles standard basic auth as defined in RFC 7617, for those users who prefer it.
  def extract_basic_auth_credentials
    header_value = request.headers["Authorization"]
    return if header_value.nil?
    regex_match = header_value.match(/\ABasic\s+(.*)/)
    return if regex_match.nil?
    _, id, key = Base64.decode64(regex_match[1]).match(/\A([^:]*):(.*)/)&.to_a
    [id, key]
  end
end
