# When working with 3rd-parties (e.g. RTT, IXN), we often have one-off fields
# which it behooves us to store. But these third-party fields affect such a
# small fraction of our total records, that they don't merit the inclusion of
# new columns in existing db tables. This mixin references a generic
# 'extension' class which can be attached to a Case, Consumer, Quote, or
# whatever.
module HasExtension
  extend ActiveSupport::Concern

  included do
    has_one :extension, as: :origin
  end

  def ext; (extension || build_extension)&.data; end

  def ext= data;
    (extension || build_extension).data = data&.with_indifferent_access
  end
end
