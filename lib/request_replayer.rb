# This module does not strictly 'replay' requests, but it does a near
# approximation to a replay, using logs created by the `LogRequestsForReplay`
# middleware.

class RequestReplayer
  def initialize attrs
    @attrs = attrs.with_indifferent_access
  end

  def run
    sess = ActionDispatch::Integration::Session.new Rails.application
    env = @attrs[:env]
    sess.host! env['SERVER_NAME']
    if @attrs.dig :sess, :user_credentials_id
      # Delete session cookie, since we're going to use our own session attrs, and the cookie could be expired
      env['HTTP_COOKIE'].gsub! /_clu2_session=[^;]+/, ''
      env[Rack::Session::Abstract::ENV_SESSION_KEY] = Session.new @attrs[:sess]
    end
    req_args = [env['REQUEST_URI'], @attrs[:body], env]
    case env['REQUEST_METHOD']
    when 'POST'
      sess.post *req_args
    when 'PUT'
      sess.put *req_args
    else
      raise ArgumentError.new "Unexpected HTTP request method (#{@method.inspect})"
    end
  end

  class Session < ActionDispatch::Request::Session
    def initialize kwargs
      @delegate = stringify_keys(kwargs)
      @loaded = true
    end
  end
end
