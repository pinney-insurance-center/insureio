# Provide functions to CRUD XML and PDF files for ESP, stored on AWS.
# cf. doc/ESP.md
class Esp::Aws
  def initialize kase
    @kase = kase
    @client = Aws::S3::Client.new(PARAMS)
  end

  # Return a signed url which the user can click to download the file
  def get_signed_url file_extension
    Aws::S3::Presigner.new(client: @client)
    .presigned_url :get_object, key: key(file_extension), bucket: BUCKET
  end

  def put file_extension, content
    @client.put_object(
      key: key(file_extension), bucket: BUCKET,
      server_side_encryption: "AES256",
      body: content,
    )
  end

  private

  def key file_extension; "#{@kase.id}.#{file_extension}"; end

  BUCKET = Rails.env.production? ? 'insureio-esp' : 'insureio-test'
  CONFIG = APP_CONFIG['aws']
  PARAMS = {
    access_key_id: CONFIG['s3_key_id'],
    secret_access_key: CONFIG['s3_secret'],
    region: APP_CONFIG.dig('aws', 'region') || 'us-west-1',
  }
end
