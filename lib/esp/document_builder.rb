class Esp::DocumentBuilder

  def initialize kase
    @kase = kase
    @cons = @kase.consumer
    @state = @cons.primary_address&.state
  end

  protected

  def coverage_in_force
    @coverage_in_force ||= [
      @cons.financial_info&.asset_life_insurance.to_i,
      @cons.coverage_in_force.to_i,
    ].max
  end

  def has_coverage?
    !!@cons.financial_info&.has_coverage || coverage_in_force > 0
  end

  # Should outbound PDF/XML include 'HIV' forms/orders?
  def include_hiv_forms?
    %w[AZ IA ND WI].include?(@state&.abbrev)
  end

  # Should outbound PDF/XML include 'Replacement' forms/orders?
  def include_replacement_forms? disregard_in_force_coverage=false
    return false unless %w[AZ CO IA NE SD WI].include?(@state&.abbrev)
    disregard_in_force_coverage || has_coverage?
  end
end
