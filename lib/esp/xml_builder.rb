class Esp::XmlBuilder < Esp::DocumentBuilder

  def initialize kase
    super
    @order_specs = ORDER_SPECIFICATIONS.clone
    @order_specs << [3203, 'Replacement Form(s)'] if include_replacement_forms?(true)
    @order_specs << [3244, 'HIV Consent Form'] if include_hiv_forms?
  end

  def to_s
    validate_xml
    doc.root.to_xml
  end

  private

  def address_1
    @cons.primary_address&.street.try :[], 0...50
  end

  def address_2
    @cons.primary_address&.street.try :[], 50..-1
  end

  # Free-form data which we can use for any purpose.
  # Our Esp::XmlReader requires that this be JSON.
  def client_data
    { case_id: @kase.id }.to_json
  end

  def doc
    @doc ||= Nokogiri::XML::Builder.new do |xml|
      xml.Root {
        xml.PI {
          xml.LastName_TX @cons.last_name
          xml.FirstName_TX @cons.first_name
          xml.MiddleName_TX @cons.middle_name
          xml.Address1_TX  address_1
          xml.Address2_TX address_2
          xml.City_TX @cons.primary_address&.city
          xml.State_TX @state&.abbrev
          xml.Zip_TX @cons.primary_address&.zip
          xml.HomePhone_TX phone(1)&.value || phone(nil)&.value
          xml.CellPhone_TX phone(3)&.value
          xml.BusinessPhone_TX phone(2)&.value
          xml.BusinessPhoneExt_TX phone(2)&.ext
          xml.SocSec_TX @cons.ssn&.gsub(/-/,'')
          xml.BirthDate_DT fmt_date @cons.birth_or_trust_date
          xml.Sex_TX @cons.gender_string[0]
          xml.Occupation_TX @cons.occupation
          xml.TimeZone_TX
          xml.BestTimetoCall_TX @cons.preferred_contact_time
          xml.PolicyNum_TX @kase.policy_number # nil for new ticket drops
          xml.OrderDate_DT fmt_date Time.current
          xml.CompanyCode_TX 'National Mutual Benefit'
          xml.Initials_TX 'IEO' # Represents the system sending the document
          xml.Requestor_TX @kase.agent&.full_name
          xml.Contact_TX @kase.agent_of_record&.phone&.value
          xml.Agent_TX @kase.agent_of_record&.full_name
          xml.AgentIdNumber_TX @kase.agent_of_record&.id
          xml.FaceAmount_MNY @kase.current_details&.face_amount
          xml.CoverageType_TX @kase.current_details&.product_type&.name
          xml.SpecialAttention_TX special_attn
          xml.TelNum_TX
          xml.MVRNum_TX @cons.dln
          xml.Client_Data client_data
          xml.Orders {
            @order_specs.each do |code, notes|
              xml.Order {
                xml.OrderCode_TX code
                xml.OrderNotes_TX notes
                xml.MDFirstName_TX
                xml.MDLastName_TX
                xml.MDFacilityName_TX
                xml.MDAddress_TX
                xml.MDCity_TX
                xml.MDState_TX
                xml.MDZip_TX
                xml.MDPhone_TX
              }
            end
          }
        }
      }
    end.doc
  end

  def fmt_date date
    date&.strftime '%m/%d/%Y'
  end

  # Does this XML meet ESP's requirements?
  def validate_xml
    @errors = []
    nil_errors = []
    blank_errors = []
    # Straightforward presence/absence fields
    blank_errors += %w[
      LastName_TX
      FirstName_TX
      Address1_TX
      City_TX
      State_TX
      Zip_TX
      SocSec_TX
      BirthDate_DT
      Sex_TX
      OrderDate_DT
      CompanyCode_TX
      Agent_TX
      AgentIdNumber_TX
      FaceAmount_MNY
    ].select { |css| doc.at_css(css)&.content.blank? }
    # Phones
    if %w[HomePhone_TX CellPhone_TX BusinessPhone_TX].all? do |field|
      doc.at_css(field)&.content.blank?
    end
      @errors << "At least one phone must be supplied."
    end
    # "Orders" dummy fields
    doc.css('Orders Order').each_with_index do |order, i|
      blank_errors += %w[
        OrderCode_TX
        OrderNotes_TX
      ].select { |css| order.at_css(css).blank? }
      .map { |css| "Order #{i} #{css}" }
      nil_errors += %w[
        MDFirstName_TX
        MDLastName_TX
        MDFacilityName_TX
        MDAddress_TX
        MDCity_TX
        MDState_TX
        MDZip_TX
        MDPhone_TX
      ].select { |css| order.at_css(css).nil? }
      .map { |css| "Order #{i} #{css}" }
    end
    if blank_errors.present?
      @errors << "The following fields require values: #{blank_errors.join(', ')}."
    end
    if nil_errors.present?
      @errors << "The following XML fields must exist: #{nil_errors.join(', ')}"
    end
    if @errors.present?
      raise ESPValidationError.new "Case is not valid for transmission. #{@errors.join(' ')}"
    end
  end

  def phone type
    @cons.phones.find { |p| p.phone_type_id == type }
  end

  def special_attn
    quote = @kase.current_details
    health_class = quote&.carrier_health_class.presence || quote&.health_class&.name || 'Best Class'
    "Health class: #{health_class}" +
    (@cons.notes.where(critical: true) + @kase.notes.where(critical: true))
    .map(&:text).join("\n")
  end

  ORDER_SPECIFICATIONS = [
    ['3100', 'Tele App Pt 1 Request'],
    ['3100P2', 'Tele App Pt 2 Request'],
    ['3105', 'Underwriting Request'],
  ]

  class ESPValidationError < StandardError; end
end
