# Pulls files from ESP's ftp server and updates our records
class Esp::Updater
  attr_accessor :ftp

  def open
    @ftp = Net::FTP.open(APP_CONFIG['esp']['host'])
    @ftp.login APP_CONFIG['esp']['user'], APP_CONFIG['esp']['pass']
    @ftp.chdir APP_CONFIG['esp']['src_dir']
  end

  def close; @ftp.close; end

  def run
    open
    fnames = @ftp.nlst('*.*')
    # Prepare a map of xml fnames to pdf fnames with canonical casing.
    # (Don't just perform a pattern substitution because ESP has
    # demonstrated inconsistent caps during the development process.)
    @fnames_map = fnames.map { |f| [f.downcase, f] }.to_h
    # Process files
    fnames.select { |f| f =~ /\.xml/i }.each do |xml_fname|
      update_one xml_fname
    end
    close
  end

  private

  def create_pdf_attachment pdf_fname
    return false if pdf_fname.nil?
    if pdf = ftp_get(pdf_fname)
      mockedfile = ActionDispatch::Http::UploadedFile.new(
        original_filename: pdf_fname,
        tempfile: StringIO.new(pdf),
        content_type: 'application/pdf',
      )
      Attachment.create!(
        file_for_upload: mockedfile,
        file_name: pdf_fname,
        person: @reader.kase.consumer,
        kase: @reader.kase,
      )
    end
  end

  def ftp_get fname
    data = @ftp.getbinaryfile(fname, nil)
    fname =~ /\.asc|\.pgp/i ? Esp.gpg_decrypt(data) : data
  end

  def update_one xml_fname
    @reader = Esp::XmlReader.new ftp_get(xml_fname)
    pdf_fname = @fnames_map[xml_fname.downcase.sub(/xml/, 'pdf')]
    if @reader.kase.present?
      @reader.update
      ftp.rename xml_fname, File.join('processed', xml_fname)
      if create_pdf_attachment pdf_fname
        ftp.rename pdf_fname, File.join('processed', pdf_fname)
      end
    else
      ExceptionNotifier.notify_exception ArgumentError.new("ESP XML specifies unknown Case record (#{xml_fname}) (#{@reader.client_data})")
    end
  rescue
    # Don't use the same class as the raised error b/c some errors (e.g. RecordInvalid) have different signatures for their initializers
    ex = RuntimeError.new "#{$!.message} (#{xml_fname}) (#{@reader&.client_data})"
    ex.set_backtrace $!.backtrace
    ExceptionNotifier.notify_exception ex
    ftp.rename(xml_fname, File.join('error', xml_fname))
    ftp.rename(pdf_fname, File.join('error', pdf_fname)) if pdf_fname
  end
end
