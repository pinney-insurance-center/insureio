# Given XML from ESP, update insureio records
class Esp::XmlReader
  attr_reader :kase, :client_data

  def initialize xml
    @xml = xml
    @doc = Nokogiri::XML.parse xml
    @client_data = JSON.parse(get('ClientData').presence || '{}')
    @kase = Crm::Case.find_by id: @client_data['case_id']
  end

  def update
    return false if kase.nil?
    set_consumer_attrs
    set_case_attrs
    kase.ext[:esp_app_end] = Time.now
    consumer.save! # `kase.save!` can return true even if associated consumer fails to save
    kase.tap &:save!
  end

  private

  # cf. doc/ESP/ESP_TO_INSUREIO_INDEX_DECISION_EXTRACT_06032020_V2.csv
  def apply_decision
    return unless decision = get('Decision')
    reg_dec = regularize decision
    if reg_dec =~ /^Decline/i
      @kase.status_type_id = 141 # "Withdrawn - Declined"
      create_case_note decision
      create_case_requirement decision
    elsif reg_dec =~ /^Withdraw|^Incomplete/i
      @kase.status_type_id = 682 # "Withdrawn - Incomplete"
      create_case_note decision
    elsif reg_dec =~ /Smoker$/i
      if decision == @kase.app_fulfillment_details&.carrier_health_class
        @kase.status_type_id = 129 # "Approved - as Applied"
      else
        @kase.status_type_id = 133 # "Approved - Other Than Applied"
      end
      @kase.app_fulfillment_details || @kase.build_app_fulfillment_details
      @kase.app_fulfillment_details.update! carrier_health_class: decision
    else
      raise ArgumentError.new "Unrecognized underwriting decision from ESP: #{decision}"
    end
  end

  # This function doesn't match the name of the XML node concerned, but it
  # does match the terminology that ESP uses and the semantics of that node.
  def apply_note
    return unless note = get('Status')
    if regularize('Applicant did not receive or was unable to open the documents received by agent') == regularize(note)
      @kase.status_type_id = 1001
    end
    create_case_note(note)
  end

  # cf. doc/ESP/ESP_TO_INSUREIO_STATUS_EXTRACT_06032020.csv
  def apply_product
    desc = get 'ProductDescription'
    if type = get('ProductType')
      case regularize type
      when regularize('APS/EDR')
      when regularize('Telephone Inspection')
        @kase.status_type_id = 448 # "Awaiting Requirements"
        case_req_name = [type, desc].join ' > '
        create_case_requirement type, get('Status')
      when regularize('Underwriting')
        @kase.status_type_id = 191 # "Submitted - Tentative Offer"
      else
        # It is not beneath ESP to change their codes. Contact them and get a
        # list of the latest codes if this error occurs. (And they refuse to
        # use numeric codes because their process is so broken that they can't
        # make the numbers correspond between their test and prod
        # environemnts.)
        raise ArgumentError.new "Unrecognized ProductType from ESP: #{type}"
      end
    elsif desc
      # This represents a change in schema from ESP. This is not outside the
      # realm of possibility (or experience) with them. Contact them and ask
      # for the latest schema.
      raise ArgumentError.new "ESP sent a ProductDescription with no ProductType:\n#{@xml}"
    end
  end

  def build_phones_from_xml
    %w[Home Business Cell].map.with_index do |key, i|
      val = get("PI #{key}Phone_TX")
      if val.present?
        type_id = i + 1 # See Enum::PhoneType
        ext = get("PI #{key}PhoneExt_TX")
        Phone.new value: val, ext: ext, contactable: consumer, phone_type_id: type_id
      end
    end.compact
  end

  def consumer
    kase.consumer || kase.build_consumer.tap do |new_consumer|
      new_consumer.tenant_id = 10 # `nmb.insureio.com`. See enums.yml
      new_consumer.agent_id = new_consumer.tenant.default_parent_id
      kase.agent_id ||= new_consumer.tenant.default_parent_id
      new_consumer.brand = new_consumer.agent&.default_brand
    end
  end

  def create_case_requirement name, note=nil
    @kase.custom_requirements.create!(
      name: name, note: note,
      requirement_type_id: 4, # Underwriting
      responsible_party_type_id: 5, # Vendor
      status_id: 1, # Outstanding
    )
  end

  def create_case_note text
    @kase.notes.create!(
      note_type_id: 10, # "api-triggered consumer update"
      created_at: get('StatusDateTime', 'CompletedDate'),
      text: text)
  end

  def get *selectors
    selectors.each do |selector|
      value = @doc.at_css(selector)&.content
      return value if value.present?
    end
    nil
  end

  def set_case_attrs
    get('PolicyNum_TX', 'PolNumber').tap { |x| x && kase.policy_number = x }
    kase.carrier_id = 123 # National Mutual Benefit b/c we do only NMB cases with ESP at this time
    get('FaceAmount_MNY', 'FaceAmount').tap { |x| x && kase.face_amount = x }
    apply_note
    apply_product
    apply_decision
  end

  # ESP can't be counted on to use consistent special characters (e.g. hypens
  # vs en-dashes), so we have agreed to require matching only on these
  # criteria.
  def regularize str; str.upcase.gsub(/\W/, ''); end

  def set_consumer_attrs
    %w[First Middle Last].map do |key|
      get("PI #{key}Name_TX", "App#{key[0]}Name")
    end.select(&:present?).join(' ')
    .tap { |x| x.present? && consumer.full_name = x }
    # Address attributes
    consumer.primary_address || consumer.addresses = [Address.new]
    [1, 2].map { |i| get("PI Address#{i}_TX") }
    .select(&:present?).join("\n")
    .tap { |x| x && consumer.primary_address.street = x }
    get('PI City_TX').tap { |x| x && consumer.primary_address.city = x }
    get('PI State_TX').tap do |state_abbrev|
      state = Enum::State.efind(abbrev: state_abbrev)
      consumer.primary_address.state = state unless state.nil?
    end
    get('PI Zip_TX').tap { |x| x && consumer.primary_address.zip = x }
    # Update existing phones, add new phones
    consumer.phones += build_phones_from_xml.reject do |phone|
      if extant_phone = consumer.phones.find { |p| p.value == phone.value }
        extant_phone.attributes = phone.attributes.slice('value', 'ext', 'contactable', 'phone_type_id')
      end
    end
    # Misc
    get('PI SocSec_TX', 'AppSSN').tap { |x| x && consumer.ssn = x }
    get('BirthDate_DT', 'DOB').tap { |x| x && consumer.birth = x }
    consumer.gender = MALE if get('Sex_TX', 'AppGender') == 'M'
    consumer.gender = FEMALE if get('Sex_TX', 'AppGender') == 'F'
    get('Occupation_TX').tap { |x| x && consumer.occupation = x }
    get('BestTimetoCall_TX').tap { |x| x && consumer.preferred_contact_time = x }
    @doc.css('Order').map do |order|
      "ESP Order #{order.content.gsub(/\s+/, ' ').strip}"
    end.unshift(get('SpecialAttention_TX')).tap do |x|
      x.present? && consumer.note = x.unshift(consumer.note.presence).compact.join("\n")
    end
  end
end