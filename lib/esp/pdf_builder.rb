class Esp::PdfBuilder < Esp::DocumentBuilder
  # This class uses many attr_readers because the ERB files hold code where a
  # type in an `@` reference could fail silently and be difficult to test.
  attr_reader :aor, :cons, :fin, :health, :kase, :quote
  attr_reader :bene_type
  attr_reader :crime_details, :has_felony, :has_pending_felony, :on_probation
  attr_reader :has_early_parent_death, :has_early_sibling_death
  attr_reader :has_dui, :has_dl_suspension
  attr_reader :purpose
  attr_reader :spouse_coverage_amt
  attr_reader :replacing

  def initialize kase
    super
    @aor = kase.agent_of_record || kase.agent&.agent_of_record
    @fin = cons.financial_info || cons.build_financial_info
    @health = cons.health_info || cons.build_health_info
    @quote = kase.current_details
    @ext = kase.ixn_ext
    @c_benes, @p_benes = kase.beneficiaries.partition { |b| b.contingent }
    @bene_type = kase.benes_type_id || 3 # Default: survivorship percentage
    @owner = @kase.owner || @kase.consumer
    @payer = @kase.payer || @kase.consumer
    #Force eager loading so failures/queries don't get the ambiguous stack trace
    #which would result from loading it while rendering the erb files.
    @replacing= @kase.replaced_cases.to_a
    @purpose = kase.purpose&.name || kase.purpose_type&.name
    @texfile = Tempfile.new ['esp-latex-', '.tex']
    @pdfpath = @texfile.path.sub /\.tex$/, '.pdf'
    @license = aor&.licenses&.find_by life: true, state_id: @cons.primary_address&.state_id, status_id: 2
    # Set IXN-specific values
    @has_early_parent_death = @ext&.parent_death_p60 || health.relatives_diseases.any? do |disease|
      next unless disease.relationship_type_id == 1
      next unless disease.death_age&.< 60
      is_cad_cancer_or_diabetes? disease
    end
    @has_early_sibling_death = @ext&.sibling_death_p60 || health.relatives_diseases.any? do |disease|
      next unless disease.relationship_type_id == 2
      next unless disease.death_age&.< 60
      is_cad_cancer_or_diabetes? disease
    end
    @has_dui = health.moving_violations.any? &:dui_dwi
    @has_dl_suspension = health.moving_violations.any? &:dl_suspension
    @crime_details = []
    health.crimes.each do |c|
      share_details = false
      if c.felony
        share_details = true
        if c.pending
          @has_pending_felony = true
        else
          @has_felony = true
        end
      end
      if c.probation_end&.> Time.now
        share_details = true
        @on_probation = true
      end
      if share_details && c.detail.present?
        @crime_details << c.detail
      end
    end
    # Apply overrides
    if whole_life?
      kase.autoloan_premium = true
      kase.dividend_type_id = 3 # Paid-up additions
    end
  end

  def to_bin
    build_pdf
    File.open(@pdfpath, 'rb') { |f| f.read }.tap { cleanup }
  end

  private

  def is_cad_cancer_or_diabetes? disease
    qualifying_type_names=[
      :coronary_artery_disease, # Abbreviated in this fn name as cad
      # NMB wants to know about early deaths from cancers _except_ basal cell carcinoma.
      :breast_cancer,
      :colon_cancer,
      :intestinal_cancer,
      :other_internal_cancer,
      :other_skin_cancer,
      :ovarian_cancer,
      :prostate_cancer,
      :diabetes_1,
      :diabetes_2,
    ]
    qualifying_type_names.any?{|type_name| disease.type_id==Enum::DiseaseType.id( type_name.to_s ) }
  end

  # Accidental death benefit (not accelerated death benefit)
  def adb_rider
    if %w[annual semi_annual quarterly monthly]
      .any? { |k| @ext&.data&.[]("adb_rider_#{k}").present? }
      currency [200_000, kase.face_amount.to_i].min, precision: 0
    end
  end

  def build_pdf
    write_tex_file
    fail_count = 0
    (0..1).each do # Compile 2x for the sake of hyperref & the aux file
      stdout, stderr, status = Open3.capture3 "pdflatex -interaction errorstopmode -halt-on-error -output-directory /tmp \"#{@texfile.path}\""
      unless 0 == status
        redo if (fail_count+=1) < 6 # Sometimes pdflatex fails but then succeeds on a later invocation
        cleanup
        error_trace = stdout.split("\n")[-7..-2]&.join("\"")
        raise RuntimeError.new "pdflatex exited with #{status} for Case \"#{@kase.id}\". #{error_trace}"
      end
    end
  end

  def carrier_name kase
    kase&.current_details&.carrier&.name
  end

  # Return LaTeX for a hyperref CheckBox
  def checkbox is_checked, opts={}
    attrs = stringify_field_attrs opts
    attrs += ',checked' if is_checked
    "\\CheckBox[#{attrs}]{}"
  end

  def child_rider
    units = @ext&.child_rider_units.to_i * 1000
    0 == units ? nil : currency(units, precision: 0)
  end

  def city_state_zip contact
    return unless addr = contact&.primary_address
    [addr.city, addr.state&.abbrev, addr.zip].join ' '
  end

  def cleanup
    pattern = @pdfpath.sub /\.tex$/, '.*'
    @texfile.close
    @texfile.unlink
    Dir.glob("#{pattern}.*").each { |f| File.delete f }
  end

  def currency value, opts={}
    return if value.nil?
    opts[:unit] ||= ''
    ActiveSupport::NumberHelper.number_to_currency value, opts
  end

  # Escape chars that are illegal for params in latex.
  def escape *input
    input.join.gsub(/\\/, '\\\\')
    .gsub(/(\~|\^)/, '\\string\1')
    .gsub(/(\&|\%|\$|\#|\_|\{|\})/, '\\\\\1')
    .gsub(/\n/, ',')
  end

  # Return LaTeX for a hyperref TextField
  # Omit overflow, store for overflow pages
  def textfield value, opts={}
    value = value.to_s
    maxlen = (opts[:maxlen] || opts[:width])&.to_i
    partition = (opts[:multiline] || maxlen == 0) ? -1 : maxlen
    overflow = value[partition..-1]
    if @collect_overflow && overflow.present?
      @overflow << "#{@current_erb_fname} > #{opts[:name]}: #{overflow}."
    end
    opts[:value] = escape value[0...partition]
    attrs = stringify_field_attrs opts.except(:maxlen)
    "\\TextField[#{attrs}]{}"
  end

  def height
    feet = @cons.health_info&.feet
    inches = @cons.health_info&.inches
    feet && "#{feet}'#{inches||0}\""
  end

  def hiv_forms
    return [] unless include_hiv_forms?
    case cons.primary_address&.state_id
    when 3 # Arizona
      ['F0401-AZ']
    when 16 # Iowa
      ['FORM-0417-1', 'FORM-0417-2']
    when 36 # North Dakota
      ['FORM-9033-ND']
    when 51 # Wisconsin
      ['FORM-9110-1']
    else
      []
    end
  end

  def whole_life?; @kase.current_details&.duration&.duration_category_id == 3; end

  def wop_rider?
    ['wop_rider_annual',
    'wop_rider_semi_annual',
    'wop_rider_quarterly',
    'wop_rider_monthly'].any? { |key| @ext&.data&.[](key).present? }
  end

  def ordered_page_names
    whole_life? ? ordered_page_names_for_whole_life : ordered_page_names_for_term
  end

  def ordered_page_names_for_term
    out = [
      'ICC18-FULL-01',
      'ICC18-FULL-02',
      'ICC18-FULL-03',
      'ICC18-FULL-04',
      'ICC18-FULL-05',
      'ICC18-FULL-06',
      'ICC18-FULL-07',
      'ICC18-FULL-08',
      'ICC18-FULL-09',
      'ICC18-FULL-10',
      'ICC18-FULL-11',
      'ICC18-FULL-12',
      'ICC18-FULL-13',
    ]
    out += policy_replacement_forms
    out += hiv_forms
    out
  end

  def ordered_page_names_for_whole_life
    out = [
      'ICC19-GIGBAPP-1',
      'ICC19-GIGBAPP-2',
      'ICC19-GIGBAPP-3',
      'ICC19-GIGBAPP-4',
      'ICC19-GIGBAPP-5',
    ]
    out += policy_replacement_forms
    out << 'FORM-8949' if cons.age >= 60
    out
  end

  def overflow_string; "OVERFLOW: #{@overflow.join ' '}"; end

  def policy_replacement_forms
    return [] unless include_replacement_forms?
    case cons.primary_address&.state_id
    when 14 # Illinois
      ['FORM-8521-IL', 'FORM-8522-IL']
    when 24 # Minnesota
      ['FORM-9615-1', 'FORM-9615-2']
    else
      ['F1900-1', 'F1900-2']
    end
  end

  # Return string value for relationship between PI and stakeholder
  def relationship stakeholder
    if stakeholder.nil?
      nil
    elsif stakeholder.id == @cons.id
      'self'
    elsif stakeholder.is_a? Crm::ConsumerRelationship
      stakeholder.relationship_type&.name
    elsif relation = @kase.stakeholder_relationships.find { |r| stakeholder.id == r.stakeholder_id }
      relation.relationship_type&.name
    end
  end

  def render_subtex fname
    texpath = "app/views/nmb/pdf/#{fname}/#{fname}.tex"
    erbpath = "#{texpath}.erb"
    bgpath = File.join RESOURCES_DIR, fname, 'bg'
    latex = if File.exists? erbpath
      @current_erb_fname = fname
      template = ERB.new(File.read erbpath)
      begin
        template.result binding
      rescue => ex
        raise ErbRenderError.new "#{ex.message}: (#{ex.backtrace[0]}) (#{fname})"
      end
    elsif File.exists? texpath
      File.read texpath
    else
      "\\thispagestyle{empty}\n\\mbox{}"
    end
    # Return latex
    <<~EOF
    % src: #{fname}
    \\backgroundsetup {
      scale=1, color=black, opacity=1, angle=0,
      contents={\\includegraphics[width=\\paperwidth,height=\\paperheight]{#{bgpath}}}
    }
    #{latex}

    \\newpage

    EOF
  end

  # Render an image of the AOR's signature
  def signature attrs={}
    @signature_png ||= begin
      return unless aor&.signature_file
      content_type = aor.signature_file.s3_file&.content_type
      return unless content_type =~ /^image\//
      bindata = aor.signature_file.binary_content
      img = MiniMagick::Image.read(bindata)
      # Convert to png b/c gif and tiff cannot be rendered in PDF
      img.format('png')
      # Return LaTeX code for graphic
      attrs = stringify_field_attrs attrs
      imgpath = img.tempfile.tap(&:close).path
      %Q{\\includegraphics[#{attrs}]{#{imgpath}}}
    end
  end

  def special_note
    @special_note ||= if @ext
      [ @ext&.agent_notes,
        @ext&.fam_w_apps,
        @ext&.prev_addr,
      ].compact.join(".\n").gsub(/\.{2,}/, ".\n")
    end
  end

  def stringify_field_attrs opts
    opts.map do |k,v|
      v = v.camelize if k == :name # Handle underscores (illegal in tex hyperref)
      "#{k}={#{v}}"
    end.join ','
  end

  def write_tex_file
    @texfile.write <<~EOF
      \\documentclass{article}
      \\usepackage{standalone}
      \\usepackage[top=2cm, bottom=2cm, outer=0cm, inner=0cm]{geometry}
      \\usepackage{graphicx}
      \\graphicspath{#{ RESOURCES_DIR }}
      \\usepackage{background}
      \\usepackage{hyperref}
      \\usepackage{import}

      \\usepackage{subfiles}

      \\def\\DefaultHeightofText{9px}
      \\pagenumbering{gobble}
      \\begin{document}
    EOF
    write_tex_subfiles
    @texfile.write <<~EOF
      \\end{document}
    EOF
    @texfile.flush
  end

  def write_tex_subfiles
    # Render once without writing to file in order to identify @overflow fields
    @overflow = []
    @collect_overflow = true
    ordered_page_names.each { |fname| render_subtex fname }
    # Render, writing to latex file
    @collect_overflow = false
    ordered_page_names.each { |fname| @texfile.write render_subtex fname }
  end

  RESOURCES_DIR = File.join Rails.root, 'app/views/nmb/pdf'

  class ErbRenderError < RuntimeError; end
end
