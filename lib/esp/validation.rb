class Esp::Validation
  attr_reader :errors

  def initialize kase
    @kase = kase
    @cons = kase.consumer
  end

  def valid?
    require_present @cons, :last_name
    require_present @cons, :first_name
    require_present @cons, :ssn
    require_present @cons, :birth_or_trust_date
    require_present @cons, :primary_address, :street
    require_present @cons, :primary_address, :city
    require_present @cons, :primary_address, :state_id
    require_present @cons, :primary_address, :zip
    require_present @kase, :agent_id
    require_present @kase, :current_details, :face_amount
    if @cons.gender.nil?
      add_error @cons, 'gender'
    end
    if @cons.phones.blank?
      add_error @cons, 'home, business, or mobile phone'
    end
    # Return
    @cons.errors.blank?
  end

  private

  def add_error obj, *args
    @cons.errors.add args.last, 'cannot be blank'
  end

  def get obj, *args
    args.reduce(obj) { |out, key| out&.send key }
  end

  def require_present *args
    add_error(*args) if get(*args).blank?
  end
end
