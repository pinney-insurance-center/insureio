class ContactSearch

  attr_reader :errors
  attr_reader :scopes

  def initialize current_user, params
    @params = params
    @errors = []
    @current_user = current_user
    # Identify which models should be searched. Set errors
    models = Array(params[:person_type]).presence || model_permissions.select {|_,v| v }.keys
    @models, blocked_models = models.partition { |m| model_permissions[m] }
    blocked_models.each { |m| errors << "You lack access to #{m} search" }
  end

  # Narrow scopes based on params
  def scope!
    @scopes = @models.map { |m| m.constantize.viewable_by @current_user }
    @scopes.map! { |scope|
      scope.where "#{scope.table_name}.full_name IS NOT NULL AND #{scope.table_name}.full_name != ''"
    }
    exclude_recruits! if @params[:exclude_recruits].present?
    scope_by_search_field!
    scope_by_name_start!
    @scopes
  end

  # Build the SELECT SQL
  def select! col_names
    scope!.map! do |scope|
      names_for_select = col_names.select { |col|
        scope.model.columns_hash.has_key?(col.to_s) && !col.match(/crypted_|_token|_key/)
      }.map { |col| "#{scope.table_name}.#{col}" }
      scope.order('full_name ASC').select names_for_select.presence
    end
  end

  private

  def exact?; @exact ||= @params[:exact].present?; end

  def exclude_recruits!
    @scopes.find { |s| s.model == User }&.where! is_recruit: false
  end

  def model_permissions
    @model_permissions ||= {
      'Consumer' => @current_user&.can?(:contacts_consumers),
      'User' => @current_user&.can?(:contacts_users),
      'Brand' => @current_user&.can?(:contacts_brands),
    }
  end

  def scope_by_name_start!
    pattern = @params[:name_starting_with]
    return @scopes if pattern.blank?
    return @scopes if pattern == 'Any'
    # If pattern is a range of letters, expand it
    if pattern =~ /^[A-Z]-[A-Z]$/
      a, z = pattern.upcase.split('-')
      pattern = Range.new(a.ord, z.ord).to_a.map { |d| "#{d.chr}%" }
    end
    likes = Array(pattern).map { |c| "full_name LIKE ?" }.join(' OR ')
    @scopes.map! do |scope|
      scope.where likes, *pattern
    end
  end

  def scope_by_search_field!
    term = @params[:search_term]
    field = @params[:search_by]
    return unless term.present? && field.present?
    @scopes.map! do |s|
      case field
      when 'id'
        s.where id: [term.to_i, term.to_i(36)]
      when 'name', 'full_name'
        s.where exact? ? {full_name: term} : ['full_name LIKE ?', like_term]
      when 'name_or_id'
        s.name_or_id_like term
      when 'phone'
        s.joins!(:phones)
        s.where exact? ? {'phones.value': term} : ['phones.value LIKE ?', like_term]
      when 'email'
        s.joins!(:emails)
        s.where exact? ? {'email_addresses.value': term} : ['email_addresses.value LIKE ?', like_term]
      else
        raise ArgumentError.new "Unsupported search field: #{field}"
      end
    end
  end

  # For use with `WHERE ... LIKE ...` stmt
  # Remove non-latin and angle-bracket chars; place wildcards
  def like_term
    @like_term ||= @params[:search_term].gsub(/[\u0250-\ue007<>]/,'').gsub(/\s+|^|$/, '%')
  end

  class ArgumentError < ::ArgumentError; end
end
