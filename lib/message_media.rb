# E.g. send message using class method:
#
#   MessageMedia.send_message('9169608709', 'If you see kay, tell him from me, see you in tea, tell him from me')
#
# E.g. check replies, using instance method:
#
#   mm = MessageMedia.new
#   mm.check_replies(message:{...}, attributes:MessageMedia::SOAP_ATTRIBUTES)

class MessageMedia
  extend Savon::Model
  include AuditLogger::Helpers
  audit_logger_name 'message_media'

  # Constants
  SOAP_ATTRIBUTES = {xmlns:"http://xml.m4u.com.au/2009"}.freeze
  USER = APP_CONFIG['message_media']['user_id']
  PASS = APP_CONFIG['message_media']['password']
  
  # Savon::Model class function calls
  client wsdl:'http://soap.m4u.com.au/?wsdl'
  operations :check_user, :send_messages, :check_replies

  def self.authentication_hash; {authentication:{userId:USER, password:PASS}}; end

  def self.check_replies; super(message:authentication_hash.merge(requestBody:{maximumReplies:100}), attributes:SOAP_ATTRIBUTES); end
  
  # Convenience override. For diagnostics.
  def self.check_user; super(message:authentication_hash, attributes:SOAP_ATTRIBUTES); end
  
  # Barebones method for sending a single SMS text to a single recipient
  # Available formats are 'SMS' and 'voice'
  def self.send_message(phone_num, content, opts={})
    opts[:format] ||= 'SMS'
    # Build message for SOAP operation
    message = Nokogiri::XML::Builder.new do |xml|
      xml.sendMessages {|sm|
        xml.authentication {|auth|
          auth.userId USER
          auth.password PASS
        }
        xml.requestBody {|rb|
          rb.messages {|ms|
            ms.message(format:opts[:format], sequenceNumber:1) {|message|
              message.recipients{|rs|
                rs.recipient phone_num, uid:1
              }
              message.content content
            }
          }
        }
      }
    end.doc.root.children.map(&:to_xml).join
    # Call SOAP operation sendMessages
    output = send_messages(attributes:SOAP_ATTRIBUTES, message:message)
    audit
    output
  end
end
