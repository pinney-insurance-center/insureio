class Ixn::IxnToPic
  attr_reader :consumer
  attr_reader :params

  def initialize params
    @params = params.with_indifferent_access
    if params[:insureio_quote_id].present?
      @quote = Quoting::Quote.find params[:insureio_quote_id]
      @case = @quote.crm_case
      set_consumer_from_case
    elsif params[:insureio_case_id].present?
      @quote = Quoting::Quote.find_by case_id: params[:insureio_case_id], sales_stage_id: 1
      @case = Crm::Case.find params[:insureio_case_id]
      set_consumer_from_case
    elsif params[:insureio_consumer_id].present?
      @consumer = Consumer.find params[:insureio_consumer_id]
    end
    @consumer ||= Consumer.new
  end

  def set_consumer_from_case
    # We are getting Consumers created with the right brand but unrestricted
    # tenant. `Consumer.default_scope` interferes.
    if @case
      @consumer = @case.consumer = Consumer.unscoped.where(brand_id: Brand::NMB_ID).find(@case.consumer_id)
    end
  end

  def health_class_id
    case cat = params.dig(:quote, :health_category, :ien_health_category)
    when 'Preferred Plus'; 1
    when 'Preferred'; 2
    when 'Standard Plus'
      tobacco? ? 5 : 3
    when 'Standard'
      tobacco? ? 6 : 4
    when 'Table A', 'Table B', 'Table C', 'Table D', 'Table E', 'Table F', 'Table G', 'Table H', 'Table I', 'Table J', 'Table K', 'Table L'
      i = cat[-1].ord - 'A'.ord + 7
      tobacco? ? i : 1+i
    when 'Table M', 'Table N', 'Table O', 'Table P'
      i = 'L'.ord - 'A'.ord + 7
      tobacco? ? i : 1+i
    when nil; nil
    else
      raise ArgumentError.new "Unrecognized ien_health_category #{cat.inspect} from guid #{params.dig :quote, :guid}"
    end
  end

  def run current_user
    @agent = @case&.agent || @consumer&.agent || current_user
    status_type = Crm::StatusType.find Crm::StatusType::PRESETS[:nmb_app_out].id
    quoting_details_attributes = [{
      id: @quote&.id,
      carrier_name: params.dig(:quote, :carrier_name),
      plan_name: params.dig(:quote, :product_name),
      product_type_name: params.dig(:quote, :product_type),
      carrier_health_class: params.dig(:quote, :health_category, :carrier_health_category),
      face_amount: params.dig(:quote, :face_amount),
      health_class_id: health_class_id,
      premium_mode_id: premium_mode_id,
      planned_modal_premium: params[:quote]["premium_#{params[:billing_frequency]}"],
      sales_stage_id: status_type.sales_stage_id,
      ext: @quote&.ext || {},
    }]
    quoting_details_attributes[0][:ext][:ixn_guid] = params[:quote][:guid] # Denormalize for easy frontend display
    cases_attributes = [{
      id: @case&.id,
      ixn_ext_attributes: build_ixn_ext_attrs,
      stakeholder_relationships_attributes: stakeholder_relationships_attributes,
      quoting_details_attributes: quoting_details_attributes,
      sales_stage_id: status_type.sales_stage_id,
      status_type_id: status_type.id,
      benes_type_id: benes_type_id,
      bill_type_id: bill_type_id,
      autoloan_premium: bool(params[:automatic_premium_loan]),
      dividend_type_id: Enum::DividendType.find { |dt| codes_match? dt.name, params[:dividend_option] }&.id,
      ext: (@case&.ext || {}).merge(ixn_guid: params[:quote][:guid]) # Denormalize for easy frontend display
    }]
    bankruptcy = case params[:recent_bankruptcy]
    when "yes"; true
    when "no"; false
    else; params[:recent_bankruptcy_details].present?
    end
    financial_info_attributes = {
      id: @consumer.financial_info_id,
      annual_income: params[:employment_annual_income],
      bankruptcy: bankruptcy,
    }
    health_info_attributes = {
      id: @consumer.health_info_id,
      feet: params[:height_ft],
      inches: params[:height_in],
      tobacco: tobacco?,
      weight: params[:weight],
    }
    @consumer.attributes = {
      actively_employed: bool(:employment_employed),
      addresses_attributes: addresses_attributes,
      agent: @consumer.agent || @agent, # the Consumer agent may differ from the stakeholders' agent
      birth_or_trust_date: birth,
      brand_id: Brand::NMB_ID,
      cases_attributes: cases_attributes,
      dln: params[:citizenship_dl_number],
      dl_state: Enum::State.find_by_abbrev(params[:citizenship_dl_state]),
      ssn: params[:ssn],
      emails_attributes: emails_attributes,
      financial_info_attributes: financial_info_attributes,
      full_name: name,
      gender: gender,
      health_info_attributes: health_info_attributes,
      last_applied_for_life_ins_outcome_id: bool(:declined_life_insurance) && 4,
      marital_status: Enum::MaritalStatus.efind { |s| codes_match? s.name, params[:marital_status] },
      occupation: params[:employment_occupation],
      phones_attributes: phones_attributes,
      referrer: params[:referrer],
      years_of_agent_relationship: params[:agent_years_knowing_pi].to_f,
      relationship_to_agent: params[:agent_pi_relationship],
      years_at_address: params[:time_at_address].to_f,
    }
    set_citizenship
    set_crimes
    set_foreign_travel
    set_hazards
    set_moving_violations
    # Return
    @consumer
  end

  private

  def addresses_attributes
    [{
      id: @consumer.primary_address&.id,
      street: params[:address],
      city: params[:city],
      zip: params[:zip],
      state_id: params[:state_id],
    }]
  end

  # Return a Hash of beneficiary attributes
  def bene level, i
    prefix = "#{level}_beneficiary_#{i}"
    relationship_code = normalize_code params[[prefix, 'relationship'].join('_')]
    relationship = Enum::RelationshipType.find(3) if relationship_code =~ /^spouse$|^domesticpartner$/
    relationship ||= Enum::RelationshipType.efind { |r| codes_match? r.name, relationship_code }
    relationship ||= Enum::RelationshipType.find(4) # default to 'other'
    {
      relationship_type_id: relationship&.id,
      is_beneficiary: true,
      percentage: params[[prefix, 'percentage'].join('_')].to_i,
      contingent: level == 'contingent',
      stakeholder_attributes: {
        agent: @agent,
        birth_or_trust_date: birth(prefix),
        brand_id: Brand::NMB_ID,
        entity_type_id: 1,
        full_name: name(prefix),
        gender: gender(prefix),
      }
    }
  end

  def benes_type_id
    distribution = params[:primary_beneficiary_1_distribution]
    if id = ['survivors_equally', 'stripes_equally', 'survivors_percentage', 'stripes_percentage'].index(distribution)
      1 + id
    end
  end

  def bill_type_id
    case params[:billing_method]
    when 'eft'
      1
    when 'direct_billing'
      2
    end
  end

  def birth prefix=nil
    date = params[[prefix, 'date_of_birth'].compact.join('_')]
    # `ContactableStereotype#birth_or_trust_date=` will complain if it has to parse the Date, so it is done here.
    date.present? && Date.parse(date)
  rescue ArgumentError => ex
    # `ArgumentError` is too general where the errors should be caught, so a custom error is used.
    raise DateError.new "Invalid date: #{date}"
  end

  def bool key
    return true if 'yes' == params[key]
    return false if 'no' == params[key]
  end

  def build_ixn_ext_attrs
    # The extension uses some short names to save space (because its data are serialized)
    data = {
      agent_notes: params[:agent_notes],
      bankruptcy_details: params[:recent_bankruptcy_details],
      declined_why: params[:declined_life_insurance_details],
      fam_w_apps: params[:family_member_names_with_applications],
      ixn_id: params[:id], # E-ticket id
      heart_stroke_cancer: bool(:diagnosed_heart_condition_stroke_cancer),
      heart_stroke_cancer_details: params[:diagnosed_heart_condition_stroke_cancer_details],
      parent_death_p60: bool(:prior_to_60_parent_death),
      prev_addr: params[:previous_address],
      quote_guid: params.dig(:quote, :guid),
      sibling_death_p60: bool(:prior_to_60_sibling_death),
      spouse_coverage_amt: params[:insurance_on_pi_spouse],
      src_guid: params.dig(:quote, :source_guid),
      unemployed_why: params[:employment_unemployment_reason],
      us_entry: params[:citizenship_date_of_entry],
    }
    %i[
      adb_rider
      adb_rider_annual
      adb_rider_monthly
      adb_rider_quarterly
      adb_rider_semi_annual
      child_rider_annual
      child_rider_monthly
      child_rider_quarterly
      child_rider_semi_annual
      child_rider_units
      child_wop_rider_annual
      child_wop_rider_monthly
      child_wop_rider_quarterly
      child_wop_rider_semi_annual
      wop_rider_annual
      wop_rider_monthly
      wop_rider_quarterly
      wop_rider_semi_annual
    ].each do |ext_key|
      data[ext_key] = params.dig(:quote, ext_key)
    end
    data.select { |k,v| v == false || v.present? }
  end

  def codes_match? a, b
    c, d = [a, b].map { |x| normalize_code x }
    c == d
  end

  def emails_attributes
    params[:email].present? ? [{ value: params[:email], id: @consumer.emails.first&.id }] : []
  end

  def gender prefix=nil
    if value = params[[prefix, 'gender'].compact.join('_')]
      return MALE if value == 'male'
      return FEMALE if value == 'female'
    end
  end

  def name prefix=nil
    %w[first_name middle_initial last_name].map do |key|
      params[[prefix, key].compact.join('_')]
    end.join(' ')
  end

  def normalize_code code; code&.downcase&.gsub /[^a-z]/i, ''; end

  def phones_attributes
    %w[primary secondary].map do |suffix|
      params["phone_#{suffix}"]
    end.select(&:present?).compact.map.with_index do |value, i|
      { value: value, id: @consumer.phones[i]&.id }
    end
  end

  def premium_mode_id
    case params[:billing_frequency]
    when 'monthly'
      return 4
    when 'quarterly'
      return 3
    when 'semi_annual'
      return 2
    when 'annual'
      return 1
    end
  end

  def set_citizenship
    if params[:citizenship_permanent_resident]
      @consumer.citizenship_id = 2
      @consumer.citizenship_noncitizen_registration_number = params[:citizenship_id_number]
    elsif params[:citizenship_visa_number].present?
      @consumer.citizenship_id = 3
      @consumer.citizenship_noncitizen_registration_number = params[:citizenship_visa_number]
    end
  end

  def set_crimes
    crime = Crm::HealthInfo::Crime.new
    crime.felony = bool :convicted_of_felony
    crime.detail = params[:convicted_of_felony_details].presence
    crime.pending = bool :pending_felony_change
    crime.probation_end = 1.year.from_now if bool(params[:on_probation]) # Best estimate
    @consumer.health_info.crimes << crime if crime.truthy?
  end

  def set_foreign_travel
    @consumer.health_info.foreign_travel = bool :international_travel
    @consumer.health_info.foreign_travel_details.detail = params[:international_travel_details].presence
    @consumer.health_info.foreign_travel_details.country_detail = params[:international_travel_details].presence
  end

  def set_hazards
    details = @consumer.health_info.hazardous_avocation_details
    details.flying = bool :flying_participation
    details.military = bool :armed_forces_member
    @consumer.health_info.hazardous_avocation ||= bool :dangerous_sport_participation
  end

  def set_moving_violations
    dui_detail = params[:operating_under_influence_details].presence
    if bool(:operating_under_influence) || dui_detail
      @consumer.health_info.moving_violations << Crm::HealthInfo::MovingViolation.new(dui_dwi: true, detail: dui_detail)
    end
    sus_detail = params[:moving_violation_suspended_revoked_license_last_3_years_details].presence
    if bool(:moving_violation_suspended_revoked_license_last_3_years) || sus_detail
      @consumer.health_info.moving_violations << Crm::HealthInfo::MovingViolation.new(dl_suspension: true, detail: sus_detail)
    end
  end

  def stakeholder_relationships_attributes
    %w[primary contingent].product([1, 2])
    .map { |level, i| bene level, i }
    .select { |b| b[:stakeholder_attributes][:full_name].present? }
  end

  def tobacco?
    params[:tobacco_use_frequency].present? || params[:tobacco_last_used].present? && Date.parse(params[:tobacco_last_used]) > 7.years.ago
  end

  class DateError < ArgumentError; end
end
