module Ixn::Public
  DEFAULT_TTL = 12.hours

  # Request a list of carriers from the IXN API
  def carriers
    get_json 'carriers', :ixn_carriers, DEFAULT_TTL
  end

  # Request a list of product groups for a given carrier from the IXN API
  def product_groups carrier_id
    get_json "product_groups?carrier_id=#{carrier_id}", "ixn_prod_gps-#{carrier_id}", DEFAULT_TTL
  end

  # Request a map of product categories => types
  def product_types
    get_json 'product_types', :ixn_prod_types, DEFAULT_TTL
  end

  # Request a list of products for a given carrier from the IXN API
  def products carrier_id
    get_json "products?carrier_id=#{carrier_id}", "ixn_prod-#{carrier_id}", DEFAULT_TTL
  end
end
