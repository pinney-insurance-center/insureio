class Ixn::AlqToPic
  attr_reader :params

  def initialize params
    @params = params
    @quote = Quoting::Quote.find_or_initialize_by(id: params[:insureio_quote_id]) do |quote|
      quote.sales_stage_id = 1 # Opportunity
      quote.build_crm_case quoting_details: [quote]
    end
    @cons = @quote.consumer ||= Consumer.find_or_initialize_by(id: params[:insureio_consumer_id])
    @quote.consumer = @cons
    @agent = @cons.agent || @cons.build_agent
    @health = @cons.health_info || @cons.build_health_info
  end

  # Set attributes on local records, return quote
  def run current_user
    raise PermissionDeniedException.new unless @cons.new_record? || @cons.editable_by?(current_user)
    state = Enum::State.efind abbrev: params[:state]
    @quote.attributes = {
      annual_premium: params[:premium_annual],
      carrier_health_class: params[:carrier_health_category],
      carrier: Carrier.find_by(ixn_id: params[:carrier_id]),
      duration: duration,
      ext: @quote.ext.merge(
        adb_rider: params[:adb_rider],
        child_rider_units: params[:child_rider_units].to_i,
        ixn_guid: params[:guid],
        flat_extra: params[:flat_extra],
      ).select { |k,v| v.present? },
      face_amount: params[:face_amount],
      health_class_id: params[:insureio_health_class_id],
      monthly_premium: params[:premium_monthly],
      plan_name: params[:product_name],
      planned_modal_premium: nil,
      premium_mode_id: nil,
      product_type: product_type,
      quarterly_premium: params[:premium_quarterly],
      sales_stage_id: 1, # Opportunity
      semiannual_premium: params[:premium_semi_annual],
      status_type_category_id: product_type&.product_cat_id,
      user_id: current_user.id,
    }
    @health.tobacco = params[:tobacco]
    @cons.attributes = {
      gender: params[:gender] ? (params[:gender] == MALE) : nil,
      birth_or_trust_date: Date.parse(params[:date_of_birth]),
      full_name: [:first_name, :last_name].map { |key| params[key] }.join(' ').strip,
    }
    @cons.agent_id ||= current_user.id
    @cons.brand_id ||= current_user.default_brand_id
    if @agent.new_record?
      @agent.attributes = {
        full_name: params[:agent_name],
        phones_attributes: [{value: params[:agent_phone]}],
        emails_attributes: [{value: params[:agent_email]}],
        licenses_attributes: [{
          number: params[:agent_insurance_license_number],
          state: state,
          life: true,
        }],
      }
    end
    return @quote
  end

  private

  def duration
    @duration ||= Enum::TliDurationOption.efind(ixn_code: params[:product_type])
  end

  def product_type
    @product_type ||= duration && Enum::ProductType.find(duration.duration_category_id)
  end

  class PermissionDeniedException < StandardError; end
end
