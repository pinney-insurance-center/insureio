# This translation class is kept distinct from IxnToPic because the latter has
# an unstable history (due to IXN's changes, not ours). Moreover, once IXN
# rolls out its LifeDX platform, this library is to be replaced with an
# interface to LifeDX.
class Ixn::Quotes
  include ActiveModel::Validations
  include ActiveModel::Validations::HelperMethods

  attr_reader :consumer
  attr_reader :kase
  attr_reader :product_ids
  attr_reader :product_types
  attr_reader :request
  attr_reader :response

  validates :age, presence: true
  validates :face_amount, presence: true
  validates :gender, presence: true
  validates :product_types, presence: true
  validates :state, presence: true
  validates :tobacco, presence: true
  validate :validate_health_xor_tables

  delegate :age, to: :@consumer
  delegate :child_rider_units, to: :@ext, allow_nil: true
  delegate :face_amount, to: :@qq
  delegate :has_rider_ad_and_d, to: :@kase, allow_nil: true
  delegate :has_rider_waiver_of_premium, to: :@kase, allow_nil: true

  def initialize quote, opts={}
    @carrier_ids = opts.fetch :carrier_ids, Ixn.carriers.map { |carrier| carrier['id'] }
    @qq = quote
    @product_types = opts[:types] || [@qq.duration&.ixn_code].compact.presence || Ixn.product_types['TERM']
    @product_ids = opts[:ids]
    @consumer = opts[:consumer] || @qq.consumer
    @kase = opts[:kase] || @qq.crm_case
    @ext = @kase&.ixn_ext
    @quote_guid = @qq.ext[:ixn_guid]
    @agent = opts[:agent] || @kase&.agent || @consumer&.agent
    @health_class = @qq.health_class
  end

  # Make a request against the IXN quotes API
  def get
    raise ValidationError.new errors.full_messages.join("\n") unless valid?
    endpoint = 'quotes'
    endpoint += "/#{@quote_guid}/re_quote" if @quote_guid
    @response = Ixn.request(endpoint) do |uri|
      @request = Net::HTTP::Post.new(uri)
      @request['APP-ID'] = APP_CONFIG['ixn']['id']
      @request['APP-TOKEN'] = APP_CONFIG['ixn']['token']
      @request.body = build_request_body.to_json
      @request
    end
    if response.code == '200'
      JSON.parse(response.body) # IXN returns an array of objects
    else
      raise ResponseError.new response.body.presence || response.code.to_i
    end
  end

  # Copy relevant details from IXN quote response to (unsaved) Quoting::Quote instances
  def to_quote_objects
    get if response.nil? # Each instance of this class is intended to make only one API request
    @quote_objects ||= JSON.parse(response.body).map do |h|
      quote_to_quote_object(h)
    end
  end

  # Copy relevant details from an IXN quote to an instance of Quoting::Quote.
  # @arg from_ixn is a hash containing a single quote from an IXN response.
  def quote_to_quote_object from_ixn, quote=nil
    # modal_fetch = -> (prefix) { from_ixn["#{prefix}_#{}"] }
    quote ||= Quoting::Quote.new
    # Copy fields that are from Insureio, not IXN
    cons = quote.consumer = @consumer || quote.build_consumer
    kase = quote.crm_case = @kase || quote.build_crm_case
    ext = quote.crm_case.ixn_ext || quote.crm_case.build_ixn_ext
    quote.premium_mode_id = @qq.premium_mode_id
    # Copy fields from IXN response
    cons.gender = from_ixn.fetch('gender') == "Female" ? FEMALE : MALE
    IxnExt::SERIALIZED_FIELDS.each do |field|
      ext.send "#{field}=", from_ixn[field]
    end
    ext.agent_notes = from_ixn.fetch 'agent_message'
    ixn_carrier_id = from_ixn.fetch('carrier_id')
    quote.carrier = Carrier.find_by ixn_id: ixn_carrier_id
    ExceptionNotifier.notify_exception Carrier::UnknownCarrier.new("#{from_ixn.fetch('carrier_name')} / IXN id #{ixn_carrier_id}")
    quote.duration = Enum::TliDurationOption.efind(ixn_code: from_ixn.fetch('product_type'))
    quote.ext[:ixn_guid] = from_ixn.fetch 'guid'
    quote.created_at ||= from_ixn.fetch 'created_at'
    quote.face_amount = from_ixn.fetch 'face_amount'
    quote.carrier_health_class = from_ixn.fetch "carrier_health_category"
    quote.plan_name = from_ixn.fetch 'product_name'
    if from_ixn['table_rate_letter']
      quote.health_class = Enum::TliHealthClassOption.parse from_ixn.fetch('table_rate_letter'), from_ixn.fetch('tobacco')
      quote.annual_premium = from_ixn.fetch "table_rate_annual"
      quote.semiannual_premium = from_ixn.fetch "table_rate_semi_annual"
      quote.quarterly_premium = from_ixn.fetch "table_rate_quarterly"
      quote.monthly_premium = from_ixn.fetch "table_rate_monthly"
    else
      quote.health_class = Enum::TliHealthClassOption.parse from_ixn.fetch('ixn_health_category'), from_ixn.fetch('tobacco')
      quote.annual_premium = from_ixn.fetch "premium_annual"
      quote.semiannual_premium = from_ixn.fetch "premium_semi_annual"
      quote.quarterly_premium = from_ixn.fetch "premium_quarterly"
      quote.monthly_premium = from_ixn.fetch "premium_monthly"
    end
    # Return
    quote
  end

private

  def build_request_body
    { #####################
      # Required params
      #####################
      carrier_ids: @carrier_ids, # Array (Required) The IDs of the Carriers to be quoted. A list of available Carriers is available from the /v1/carriers endpoint.
      current_age: age, # Integer (Required) The current age of the person being quoted for.
      face_amount: face_amount, # Integer (Required) A whole dollar amount that the policy's beneficiaries will receive upon the death of the insured.
      gender: gender, # String (Required) Accepts the following values: "Male", "Female". These values are case-sensitive.
      nearest_age: age(true), # Integer (Required) The nearest age of the person being quoted for. This should the same or one number higher than the current_age. For example, if the current month is March, the person is 32 years old and has a birthday in February...then the value of nearest_age should be 32.
      product_types: product_types, # Array (Required) Accepts any combination of the values returned by /v1/product_types.
      state: state, # String (Required) A two letter abbrevation for any of the 50 U.S. states.
      tobacco: tobacco, # Boolean (Required) Valid values: "false", "true"
      #####################
      # Conditionally-required params
      #####################
      health_categories: health_categories, # Array (Required) Only required if table_rates isn't passed. Accepts any combination of the following values: "Preferred Plus", "Preferred", "Standard Plus", "Standard".
      table_rates: table_rates, # Array (Required) Only required if health_categories isn't passed. Accepts any combination of the following values: "Table A", "Table B", "Table C", "Table D", "Table E", "Table F", "Table G", "Table H", "Table I", "Table J", "Table K".
      #####################
      # Optional params
      #####################
      product_ids: product_ids, # Array (Optional) The IDS of the Products to be quoted. If none are provided, then all Products for specified Carriers will be quoted. A list of available Products is available from the /v1/products endpoint.
      adb_rider: has_rider_ad_and_d.presence.to_s, # Boolean (Optional) If "true", then only products with an Accidental Death Benefit rider will be returned.
      wop_rider: has_rider_waiver_of_premium.presence.to_s, # Boolean (Optional) If "true", then only products with a Waiver of Premium rider will be returned.
      child_rider_units: child_rider_units.to_i, # Integer (Optional) If greater than "0", then only products with a Child Rider that allow the specified number of units will be returned.
      flat_extra: @qq.flat_amt.to_i, # Integer (Optional) A dollar amount per thousand of the face_amount that will be added to the premium. This is calculated for the life of the policy, it is NOT a temporary flat extra.
      'agent.name': @agent&.name, # String (Optional) The name of the Agent running the quote.
      'agent.email': @agent&.primary_email&.to_s, # String (Optional) The email of the Agent running the quote.
      'agent.phone_number': @agent&.primary_phone&.to_s, # String (Optional) The phone number of the Agent running the quote.
    }.delete_if { |_k,v| v.blank? }
  end

  def gender
    consumer.gender_string.capitalize
  end

  def health_categories
    unless @health_class.nil? or @health_class.name =~ /^Table / 
      health_category = @health_class.name.sub(/ Tobacco/, '')
      [@health_class.id == 1 ? "Preferred Plus" : health_category]
    end
  end

  def state
    consumer.primary_address&.state&.abbrev
  end

  def table_rates
    if @health_class and @health_class.name =~ /^Table /
      label = @health_class.name.sub(/ - Tobacco/, '').sub(/\d+\//, '')
      [label] unless label =~ /L$/ # Only A - K are used by IXN
    end
  end

  def tobacco
    @health_class.nil? ? nil : (@health_class.name =~ /Tobacco$/).present?.to_s
  end

  def validate_health_xor_tables
    if health_categories.present? == table_rates.present?
      errors.add(:base, "Specify health_categories or table_rates, not both")
    end
  end

  class ValidationError < StandardError; end
  class ResponseError < StandardError; end
end
