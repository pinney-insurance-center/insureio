# Create default_scope on all ActiveRecord::Base descendants which have direct
# associations to Consumer. If `Thread.current[:sess_consumer_id]` is
# set, only records related to the designated consumer_id shall be in scope
# for these classes.

# This narrowing of scope protects Consumer, Case, Quote, etc. It does NOT
# mean that every record and field that can be retrieved should be accessible
# for every request.

# The paradigm of names here may seem odd. (1) I define a module matching the
# name of the file because it is Rails convention. (2) I don't use a `class
# ActiveRecord::Base` declaration in order to avoid an execution of any sort
# thinking that this file contains the definition of that class.

module ActiveRecordLimitedAccessScoper
  def inherited child_klass
    super
    return if child_klass == ApplicationRecord # Can't check `abstract_class` or table yet
    if child_klass == Consumer
      ActiveRecordLimitedAccessScoper.set_consumer_default_scope
    else
      ActiveRecordLimitedAccessScoper.set_consumer_assoc_default_scope child_klass
    end
  end

  # Is the current session intended to use limited records access?
  def ltd_access_sess?
    :ltd_access_code == Thread.current[:current_user_auth]
  end

  # Return the id to which this session+class is limited (0 if limited but unspecified)
  def ltd_access_id
    Thread.current["sess_#{name.camelize.downcase}_id".to_sym].to_i if ltd_access_sess?
  end

  def self.set_consumer_default_scope
    Consumer.default_scope -> {
      if cons_id = Consumer.ltd_access_id
        Consumer.where id: cons_id
      end
    }
  end

  # Examine all associations for `klass`. Any that relate directly to
  # `Consumer` will narrow the default scope whenever
  # `Thread.current[:sess_consumer_id]` is set.
  def self.set_consumer_assoc_default_scope klass
    t = klass.arel_table
    belongs_assocs = []
    has_assocs = []
    # Build lambdas which return arel groupings for each Consumer-related associations
    klass.reflect_on_all_associations.each do |a|
      if a.through_reflection.present?
        next # Revisit this type of assoc if the functionality becomes necessary
      elsif a.polymorphic?
        belongs_assocs << -> (cons_id) { t[a.foreign_type].not_eq('Consumer').or(t[a.foreign_key].eq(cons_id)) }
      elsif a.klass == Consumer
        if a.belongs_to?
          belongs_assocs << -> (cons_id) { t[a.foreign_key].eq(cons_id) }
        elsif a.has_and_belongs_to_many?
          next # Revisit this type of assoc if the functionality becomes necessary
        else # has_one or has_many (there is no helper function to check this)
          has_assocs << -> (cons_id) { t[:id].eq Consumer.find_by(id: cons_id).try(a.foreign_key).to_i }
        end
      end
    end
    return if belongs_assocs.blank? && has_assocs.blank?
    # Set default scope on non-Consumer class if a direct assoc to Consumer exists
    klass.default_scope -> {
      if cons_id = Consumer.ltd_access_id
        # Make a single OR-chain of belongs_to-conditions
        belongs_grouping = belongs_assocs.reduce(t.create_true) { |acc, l| acc.or(l.call(cons_id)) }
        # Make a single AND-chain of all conditions
        arel_grouping = t.create_and [belongs_grouping] + has_assocs.map { |l| l.call(cons_id) }
        # Return a scope
        klass.where(arel_grouping)
      end
    }
  end
end

ActiveRecord::Base.class_eval { extend ActiveRecordLimitedAccessScoper }
