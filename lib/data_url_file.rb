class DataUrlFile < ActionDispatch::Http::UploadedFile
  #This class wraps data urls, exposing the methods we devs expect from the parent class.

  attr_accessor :content_type, :extension

  def initialize data_url
    #Note the nested parentheses, which will capture substrings like "image/png" and "png" respectively.
    match_obj=data_url.match(/^data:(\w+\/(\w+))\;base64,(.*)/)
    raise "Improper format for a data url." unless match_obj
    entire_data_url, @content_type, @extension, encoded_data = match_obj.to_a
    @decoded_data = Base64.decode64(encoded_data)
    self
  end

  def read
    @decoded_data
  end

end