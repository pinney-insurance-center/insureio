# See doc/ESP.md

require 'net/ftp'

module Esp
  # Drop a PDF and XML file onto ESP's server
  def self.push_ticket kase
    fname = file_name_prefix kase
    xml_data = XmlBuilder.new(kase).to_s
    pdf_data = PdfBuilder.new(kase).to_bin
    # Send files to ESP's FTP server
    Net::FTP.open(APP_CONFIG['esp']['host']) do |ftp|
      ftp.login APP_CONFIG['esp']['user'], APP_CONFIG['esp']['pass']
      ftp.chdir APP_CONFIG['esp']['dst_dir']
      push_file ftp, "#{fname}.XML.asc", gpg_encrypt(xml_data)
      push_file ftp, "#{fname}.PDF.asc", gpg_encrypt(pdf_data)
    end
    # Send files to AWS S3
    Esp::Aws.new(kase).tap do |aws|
      aws.put 'xml', xml_data
      aws.put 'pdf', pdf_data
    end
    # Update case
    kase.ext[:nmb_pdf_saved] = kase.ext[:esp_push] = Time.now
    kase.sales_stage_id = 3 unless kase.sales_stage_id&.>= 3 # App Fulfillment
    kase.save!
  end

  def self.generate_pdf_only kase
    fname = file_name_prefix kase
    pdf_data = PdfBuilder.new(kase).to_bin
    Esp::Aws.new(kase).tap do |aws|
      aws.put 'pdf', pdf_data
    end
    kase.ext[:nmb_pdf_saved] = Time.now
    kase.save!
  end

  private

  def self.file_name_prefix kase
    raise "Case must be saved before preparing documents" unless kase&.persisted?
    cons = kase.consumer
    raise "Consumer must be saved before preparing documents" unless cons&.persisted?
    [
      cons.last_name,
      cons.first_name,
      cons.ssn.try(:[], -4..-1) || '',
      kase.updated_at&.strftime('%Y%m%d'),
      kase.updated_at&.strftime('%H%M%S'),
    ].join('_')
  end

  def self.gpg_decrypt bindata
    # NB: This relies on the secret key being in your keyring
    crypto = GPGME::Crypto.new
    crypto.decrypt(bindata).to_s
  end

  def self.gpg_encrypt data
    # NB: This relies on ESP's public key being in your keyring
    crypto = GPGME::Crypto.new
    crypto.encrypt(data,
      always_trust: true,
      armor: true,
      recipients: ENV['ESP_GPG_PUBLIC_KEY']).to_s
  end

  # FTP file to ESP's server
  # Unlike `gettextfile`, `puttextfile` requires that a local file exist
  def self.push_file ftp, fname, data
    file = Tempfile.new 'ESP-Payload'
    file.write data
    file.close
    ftp.puttextfile file.path, fname
    File.delete file
  end
end
