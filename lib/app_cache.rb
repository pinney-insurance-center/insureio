class AppCache
  @@cache = ActiveSupport::Cache::MemoryStore.new
  @@cache_blocks = {}

  def self.cached_method(cache_name, &block)
    @@cache_blocks[cache_name] = block
    self.instance_eval <<-EVAL
      def #{cache_name}
        @@cache.fetch(:#{cache_name}, &@@cache_blocks[:#{cache_name}])
      end
    EVAL
  end

  def self.reload!
    @@cache.clear
  end

end
