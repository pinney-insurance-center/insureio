require 'nokogiri'
require 'nokogiri_helpers.rb'
require 'net/http'

module Ebix

	EBIX_URI = URI::parse(::EBIX_URL)

	def self.process quote
		request_body = build_request_body quote
		response = send_xml_to_ebix request_body
		results = parse_response response.body
		if results.empty?
			errors = parse_errors(response.body)
			results = {errors:errors, response:response} if errors.present?
		else
			# Retrieve all carriers in one query for speed
			carriers = Carrier.for_naic_codes(results.map { |r| r[:naic_code]})
			results = results.map do |res|
				Quoting::Widget::Quote::EbixOffer.new({
						carrier: carriers.detect { |c| c.naic_code.to_s == res[:naic_code].to_s },
						has_joint: quote.joint?
					}.merge(res.except(:naic_code)))
			end
		end
		results
	end

private
	def self.parse_errors xml
		errors = []
		doc = Nokogiri::XML::Document.parse xml
		doc.xpath('.//xmlns:Error').each do |node|
			if node.fetch('Type') == 'Error'
				errors << node.fetch('Description')
			end
		end
		errors
	end

	def self.parse_response xml
		results = []
		doc = Nokogiri::XML::Document.parse xml
		doc.xpath('.//xmlns:VLTCQuote').each do |node|
			result = {
				:company_name => node.fetch('Company'),
				:naic_code => node.fetch('CompanyNAIC'),
				:product_name => node.fetch('ProductName'),
				:product_code => node.fetch('Code'),
				:premium => node.fetch('Premium'),
				:applicant_premium => node.fetch('Quote', 'IndivPremium'),
				:spouse_premium => node.fetch('JointQuote', 'IndivPremium'),
				:jwop => node.fetch_option('JWOP'), # joint waiver of premium
				:swop => node.fetch_option('SWOP'),
				:wop => node.fetch_option('WOP'), # waiver of premium
				:sb => node.fetch_option('SB'),
				:flex => node.fetch_option('FLEX'),
				:acl => node.fetch_option('ACL'),
				:hwep => node.fetch_option('HWEP'),
				:nf => node.fetch_option('NF'),
				:rop => node.fetch_option('ROP'), # return of premium
				:rob => node.fetch_option('ROB'), # restoration of benefits
				:part => node.fetch_option('PART') # partnership approved
			}
			results << result
		end
		results
	end

	def self.send_xml_to_ebix xml
		headers = {'content-type' => 'text/plain'}
		request = Net::HTTP::Post.new EBIX_URI.path, headers
		request.set_form_data 'inXML' => xml
		http = Net::HTTP::new EBIX_URI.host, EBIX_URI.port
		return http.request request
	end

	def self.build_request_body quote
		builder = Nokogiri::XML::Builder.new do |xml|
			xml.LifeLink('xmlns' => "urn:lifelink-schema", 'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation' => "urn:lifelink-schema llXML.xsd") {
				xml.LL {
					xml.GroupName EBIX_GROUP_NAME
					xml.GroupPassword EBIX_GROUP_PASSWORD
					xml.UserName EBIX_USER_NAME ### todo: verify w/ Ryan
					xml.InterfaceType 'NoGUI'
					xml.OutputType 'XML'
					xml.Logout false
					xml.Tool {
						xml.Name 'VitalLTC'
					}
				}
				xml.Client {
					# Client (applicant) info
					xml.Applicant {
						xml.Age quote.age
						xml.Sex quote.female? ? 'Female' : 'Male'
						xml.MaritalStatus quote.married? ? 'Married' : 'Single'
					}
					# Spouse info
					if quote.joint.present?
						xml.Spouse {
							xml.Age quote.joint.age
							xml.Sex quote.joint.female? ? 'Female' : 'Male'
						}
					end
					# State
					xml.StateOfIssue quote.state.abbrev
				}
				xml.VitalLTC {
					xml.ProductOptions {
						xml.BenAmt {
							xml.Value quote.face_amount
							xml.PayoutTimePeriod quote.payout_period || 'D' ### Should this default value be here?
						}
						xml.PM quote.premium_mode || 'MONTH' ### Should this default value be here?
						# xml.add :CL, quote.coverage_level
						xml.add :BP, quote.benefit_period
						xml.add :IP, quote.inflation_protection
						xml.add :EP, quote.elimination_period
						xml.add :RC, quote.health_class
						xml.add :JP, marital_status(quote)
						# xml.add :JRC, quote.joint.health_class unless quote.joint.nil?
						if quote.joint?
							xml.add :WOP, quote.joint.waiver_of_premium?
							xml.add :SHAREDBEN, 'YES' if quote.joint.shared_benefit?
						end
						xml.ProductList 'ALL'
						xml.IncludeFiltersWithQuotes false
						xml.IncludeRemovedProducts true
						xml.EnableRidersNoFiltering true
					}
				}
			}
		end
		builder.to_xml
	end

	def self.marital_status(quoter)
		if quoter.married?
			quoter.joint.present? ? 'W/SP' : 'MARR'
		else
			quoter.joint.present? ? 'W/OTH' : 'SING'
		end
	end

end
