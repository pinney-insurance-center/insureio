# NB: SBLI has whitelisted our app server IPs. Only they have access to the
# SBLI SFTP server.

class Sbli
  UPDATES_REMOTE_DIR = 'fromSBLI'
  UPDATES_PROCESSED_DIR = File.join UPDATES_REMOTE_DIR, 'processed'

  # Pull ACORD documents from SBLI's SFTP server and update our records.
  def self.update_local_records
    get_update_files do |xml, fname|
      Acord::Document.all_tx_life_requests(xml) do |tx_life_request, _|
        begin
          update = Acord::Update.new(tx_life_request, fname).tap(&:apply)
          if update.missing_case_ids.present?
            Rails.logger.warn "SBLI update #{fname} included statuses for cases which are missing from the database:\n#{update.missing_case_ids.map(&:to_s).join(", ")}"
          end
        rescue => ex
          ExceptionNotifier.notify_exception ex
        end
      end
    end
  end

private

  # Read remote ACORD files. Yield their contents to the calling function. Move
  # them into a 'processed' folder when complete.
  def self.get_update_files
    host = APP_CONFIG['sbli']['host']
    port = APP_CONFIG['sbli']['port'] || 22 # Passing nil is deprecated for Net::SFTP
    user = APP_CONFIG['sbli']['user']
    pass = APP_CONFIG['sbli']['pass']
    Net::SFTP.start(host, user, port: port, password: pass) do |sftp|
      begin
        sftp.mkdir! UPDATES_PROCESSED_DIR
      rescue Net::SFTP::StatusException => ex
        raise unless [3, 11].include?(ex.code) # Dir may exist
      end
      sftp.dir.foreach UPDATES_REMOTE_DIR do |entry|
        if entry.file?
          next unless entry.name =~ /\.xml$/
          src = File.join(UPDATES_REMOTE_DIR, entry.name)
          handle = sftp.open! src
          length = entry.attributes.size
          buffer = self.read sftp, handle, length
          sftp.close! handle
          yield buffer, entry.name
          sftp.rename! src, File.join(UPDATES_PROCESSED_DIR, entry.name)
        end
      end
      sftp.channel.eof!
    end
  end

  # Larger files won't be completely read in a single `read!` request, so this
  # function loops and accumulates the transmitted contents until EOF.
  def self.read sftp, handle, length
    offset = 0
    buffer = ''
    loop do
      data = sftp.read! handle, buffer.length, length - buffer.length
      break if data.nil?
      buffer << data
    end
    buffer
  end
end
