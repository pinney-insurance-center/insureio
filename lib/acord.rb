module Acord

  # This struct `PaymentMode` is used in the `PAYMENT_MODES` constant below.
  # Its purpose is to map ACORD's premium mode tc to Insureio's
  # Enum::PremiumModeOption ids and to ACORD's own field names
  # (`premium_field`).
  PaymentMode = Struct.new(:premium_field, :insureio_id)
  # Reference https://modelviewers.pilotfishtechnology.com/modelviewers/ACORD/model/typecodes/OLI_LU_PAYMODE.html
  PAYMENT_MODES = {
    1 => PaymentMode.new(:annual_premium, 1),
    2 => PaymentMode.new(:semiannual_premium, 2),
    3 => PaymentMode.new(:quarterly_premium, 3),
    4 => PaymentMode.new(:monthly_premium, 4),
    9 => PaymentMode.new(:single_premium, 5),
  }.freeze
end
