require 'nokogiri'
require 'net/http'

class Compulife
  def initialize consumer, quoting_details
    @consumer=consumer
    @quote=quoting_details
    @q={}#the query data
    @errors=''
  end

  def quote; @quote; end
  def consumer; @consumer; end
  def hi; @consumer.health_info; end#Health info fields can be accessed through the consumer record.

  #Should return an object with either key "results", or key "errors":
  def get_quotes
    quote_url = build_quote_url
    response = Net::HTTP.get( quote_url )
    # Handle case where Compulife has no products for criteria
    results = if response =~ /No Products are available/
      []
    # Handle case of form errors
    elsif response =~ /Error in form/
      ({
        error:"There was a problem communicating with the Quote Engine. #{response}"
      })
    # Handle successful quote
    else
      cl_resp_hashes = parse_response(response)
      cl_codes = cl_resp_hashes.map { |i| i[:compulife_code] }
      relevant_carriers = Carrier.for_compulife_codes(cl_codes).map { |c| [c.compulife_code, c] }.to_h
      ok_results, results_wout_known_carrier = cl_resp_hashes.partition { |r| relevant_carriers.has_key? r[:compulife_code] }

      # Send a single message alerting recipient to any quotes whose carriers
      # are not in the carriers table of the database.
      if results_wout_known_carrier.present?
        message = <<~EOF
        Compulife delivered quotes to the quoter widget which cannot be
        rendered by Insureio because the carrier(s) in question are not found
        in the 'carriers' table of the Insureio database. Would you like to
        add them?

        #{ results_wout_known_carrier.map { |r| r[:compulife_code] }.join("\n") }

        If you need help, please notify the developers. Tell them that they
        would do well to search the binary file N:\\COMPLIFE\\COMPINF.DAT for
        the Compulife code aforementioned.
        EOF
        SystemMailer.warning backtrace: caller, message: message
      end

      # Compile an Array of results to return
      ok_results.map do |cl_resp|
        carrier = relevant_carriers[cl_resp[:compulife_code]]
        Quoting::Widget::Quote::CompulifeOffer.new({
          carrier: carrier,
          period: quote.duration,
          face_amount: quote.face_amount
        }.merge(cl_resp))
      end.compact
    end
    return results
  rescue URI::InvalidURIError, Errno::ECONNREFUSED => ex
    Rails.logger.error ex.message
    Rails.logger.error ex.backtrace.join("\n")
  end

  private

  # Parses the body of an HTTP response to return an Array of Hashes which contain quote data from Compulife
  def parse_response response
    results = []
    doc = Nokogiri::XML response
    doc.xpath('/CompanyQuote/quote').each do |quote|
      if quote['display'] == 'true' or (quote['display'] == 'maybe' and quote.at_xpath('healthcat')['class'] == 'Rg')
        result = parse_quote(quote)
        results << result unless result.nil?
      end
    end
    return results
  end

  # Returns a Hash, representing the essential info from a Nokogiri::XML::Element representing a single Compulife quote
  def parse_quote quote
    output = {}
    output[:compulife_code] = quote.at_xpath('company')['code']
    output[:company_name]   = quote.at_xpath('company').text
    output[:product_code]   = quote.at_xpath('product')['code']
    output[:product_name]   = quote.at_xpath('product').text
    output[:health_code]    = quote.at_xpath('healthcat')['class']
    output[:health_category]= quote.at_xpath('healthcat').text
    output[:monthly_premium]= quote.at_xpath('premium').text.gsub(/,/,'').to_f
    output[:annual_premium] = quote.at_xpath('annualPremium').text.gsub(/,/,'').to_f
    output[:policy_fee]     = quote.at_xpath('policyfee').text
    return output
  rescue => ex
    raise "to do - email and log error message"
  end

  def build_quote_url
    url = APP_CONFIG['compulife']['endpoint']
    url+= '?'+build_query
    URI.parse(url)
  end

  def build_query
    @q={
      #boilerplate keys:
      'X'               => 'M',
      'DoHeightWeight'  => 'ON',
      'GoString'        => true,
      'NoGoString'      => false,
      'DoNotKnowString' => 'maybe',
      'ModeUsed'        => 'M',
      'UserLocation'    => 'XML',
      #basic data
      'State'           => consumer.primary_address.try(:state).try(:compulife_code),
      'NewCategory'     => quote.duration.compulife_code,
      'FaceAmount'      => quote.face_amount,
      'Health'          => (quote.health_class||Enum::TliHealthClassOption['Best Class']).value,
      'Feet'            => consumer.feet,
      'Inches'          => consumer.inches,
      'Weight'          => consumer.weight,
      'BirthMonth'      => consumer.birth_or_trust_date&.month,
      'BirthYear'       => consumer.birth_or_trust_date&.year,
      'Birthday'        => consumer.birth_or_trust_date&.day,
      'Sex'             => consumer.gender_string.match(/f/i) ? 'F' : 'M',
    }
    handle_bp                if hi.bp?
    handle_cholesterol       if hi.cholesterol?
    handle_family_diseases   if hi.family_diseases.present?
    handle_moving_violations if hi.moving_violation?
    handle_tobacco           if hi.tobacco?
    #Unlike the other "DoX" sort of fields, this one is required.
    #Provides tobacco results without requiring any tobacco use details.
    @q["Smoker"] = hi.tobacco? ? 'Y' : 'N'

    blank_keys=[]
    @q.each do |k,v|
      blank_keys << k.to_s.humanize.downcase if v.blank?
    end

    @q.delete_if{ |k, v| v.blank? }
    @errors+="Missing required field(s): #{blank_keys.join(', ')}." if blank_keys.present?

    Rails.logger.info "\nQuery params for Compulife: #{@q}\n" if Rails.env!='production'
    @q.to_query
  end

  def count_moving_violations_since date
    hi.moving_violations.count { |v| v.date && v.date >= date }
  end

  def handle_bp
   @q["DoBloodPressure"]         = "ON"
   @q["BloodPressureMedication"] = "Y"
   @q["Systolic"]                = hi.bp_systolic
   @q["Dystolic"]                = hi.bp_diastolic
   @q["PeriodBloodPressure"]     = period(hi.bp_last_treatment)
   @q["PeriodBloodPressureControlDuration"] = period(hi.bp_control_start)
  end

  def handle_cholesterol
   @q["DoCholesterol"]         = "ON"
   @q["CholesterolLevel"]      = hi.cholesterol_level
   @q["CholesterolMedication"] = "Y"
   @q["HDLRatio"]              = hi.cholesterol_hdl
   @q["PeriodCholesterol"]     = period(hi.cholesterol_last_treatment)
   @q["PeriodCholesterolControlDuration"] = period(hi.cholesterol_control_start)
  end

  def handle_family_diseases
    @q["DoFamily"]      = "ON"
    #Compulife needs two distinct lists:
    #family members who've died of a disease,
    #and family members who've only contracted a disease.
    @q["NumContracted"] = hi.family_diseases.length-hi.family_disease_death_count
    @q["NumDeaths"]     = hi.family_disease_death_count
    fd_map=[#[Compulife key, model key]
      ["AgeDied",            :death_age],
      ["AgeContracted",      :onset_age],
      ["IsParent",           :parent],
      ["CVD",                :cardiovascular_disease],
      ["CAD",                :coronary_artery_disease],
      ["CVI",                :cardiovascular_impairments],
      ["CVA",                :cerebrovascular_disease],
      ["Diabetes",           :diabetes],
      ["KidneyDisease",      :kidney_disease],
      ["ColonCancer",        :colon_cancer],
      ["IntestinalCancer",   :intestinal_cancer],
      ["BreastCancer",       :breast_cancer],
      ["ProstateCancer",     :prostate_cancer],
      ["OvarianCancer",      :ovarian_cancer],
      ["OtherInternalCancer",:other_internal_cancer],
      ["BasalCellCarcinoma", :basal_cell_carcinoma],
    ]
    idx_died=0
    idx_contracted_not_died=0
    hi.family_diseases.each do |fd|
      which_fd_list= fd.death_age ? '0' : '1'
      idx_in_list= fd.death_age ? idx_died : idx_contracted_not_died
      fd_map.each do |compulife_key,model_key|
        v=fd.send(model_key)
        if !!v==v#convert boolean to Y/N.
          v= v ? 'Y' : 'N'
        end
        @q[compulife_key+which_fd_list.to_s+idx_in_list.to_s]=v unless v.nil?
      end
      fd.death_age ? idx_died+=1 : idx_contracted_not_died+=1
    end
  end

  def handle_moving_violations
    @q["DoDriving"] = "ON"
    @q["HadDriversLicense"] = "Y"
    #Insureio has some fields for moving violations in health info model,
    #some in a moving violation history model.
    #Eventually these should be moved into the health info table.

    mv_map = [
      ["DwiConviction",       last_dui_dwi],
      ["MoreThanOneAccident", penultimate_car_accident],
      ["RecklessConviction",  last_reckless_driving],
      ["SuspendedConviction", last_dl_suspension],
    ]
    mv_map.each do |compulife_key, date|
      if date
        @q[compulife_key]='Y'
        @q["Period#{compulife_key}"] = period date
      else
        @q[compulife_key]='N'
      end
    end

    count_moving_violations_since(6.months.ago).tap { |n| @q["MovingViolations0"] = n if n > 0 }
    count_moving_violations_since(1.years.ago).tap { |n| @q["MovingViolations1"] = n if n > 0 }
    count_moving_violations_since(2.years.ago).tap { |n| @q["MovingViolations2"] = n if n > 0 }
    count_moving_violations_since(3.years.ago).tap { |n| @q["MovingViolations3"] = n if n > 0 }
    count_moving_violations_since(5.years.ago).tap { |n| @q["MovingViolations4"] = n if n > 0 }
  end

  def handle_tobacco
    @q["DoSmokingTobacco"] = "ON"
    t_map=[#[Compulife key, model key, frequency unit]
      ['Cigarettes',          'cigarette',            'day'],
      ['Cigars',              'cigar',                'month'],
      ['Pipe',                'pipe',                 'year'],
      #We don't collect quantity/frequency for these types of use.
      ['ChewingTobacco',      'chewed',               ''],
      ['NicotinePatchesOrGum','nicotine_patch_or_gum','']
    ]
    t_map.each do |compulife_key, model_key, frequency_unit|
      last_use_date = hi.send('tobacco_'+model_key+'_last')
      current_user  = hi.send('tobacco_'+model_key+( ['cigarette','cigar'].include?(model_key) ? 's' : '')+'_current')
      next unless last_use_date || current_user
      @q['Do'+compulife_key] = "ON"
      period_code=period( last_use_date, current_user )
      @q['Period'+compulife_key]=period_code
      next if frequency_unit.blank?
      #Number of 1 or greater is required if current or < a year ago, but not for older use.
      minimum_number = period_code<2 ? 1 : 0
      saved_number   = hi.send('tobacco_'+model_key+'s_per_'+frequency_unit)
      number_to_use  = [minimum_number, saved_number.to_i].max
      @q['Num'+compulife_key]=number_to_use
    end
  end

  def last_dl_suspension
    hi.moving_violations.select(&:dl_suspension).sort_by(&:date).last&.date
  end

  def last_dui_dwi
    hi.moving_violations.select(&:dui_dwi).max(&:date)&.date
  end

  def last_reckless_driving
    hi.moving_violations.select(&:reckless_driving).sort_by(&:date).last&.date
  end

  def penultimate_car_accident
    hi.moving_violations.select(&:accident).sort_by(&:date)[-2]&.date
  end

  def period date_str_or_obj, current=false
    return 0 if current
    return -1 if date_str_or_obj.nil?
    date= date_str_or_obj.is_a?(Date) ? date_str_or_obj : Date.parse(date_str_or_obj)

    if 6.months.ago.to_date <= date
      0
    elsif 1.years.ago.to_date <= date#Number of 1 or greater is required if current or < a year ago.
      1
    elsif 2.years.ago.to_date <= date
      2
    elsif 3.years.ago.to_date <= date
      3
    elsif 4.years.ago.to_date <= date
      4
    elsif 5.years.ago.to_date <= date#Our GUI asks about past 5 years, but who knows what will come in via API.
      5
    elsif 7.years.ago.to_date <= date
      6
    elsif 10.years.ago.to_date <= date
      7
    elsif 15.years.ago.to_date <= date
      8
    else
      9
    end
  end

end

=begin
Sample URL:

  http://compulife.pinneyapps.com/cgi-bin/cqsl.cgi
  ?X=M
  &GoString=true
  &NoGoString=false
  &DoNotKnowString=maybe
  &UserLocation=XML
  &ModeUsed=M
  &NewCategory=3
  &FaceAmount=200000
  &BirthMonth=10
  &BirthYear=1972
  &Birthday=30
  &DoHeightWeight=ON
  &DoSmokingTobacco=ON
  &DoCigarettes=ON
  &Smoker=N
  &NumCigarettes=1
  &PeriodCigarettes=-1
  &Health=PP
  &Feet=6
  &Inches=1
  &Sex=F
  &Weight=160
  &State=15
=end
