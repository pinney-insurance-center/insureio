# With multiple app servers, all could attempt to start a single scheduled job
# at the same time, though the job should only run on one server at a time.
# This class helps the servers allocate scheduled jobs.

class SyncedRunner
  # Set TTL on our state in Redis in order to ignore the current rake if the
  # most recent rake was started recently. Multiple app servers are
  # contending for this job and this rake could be a repeat.
  LOCK = 'lock'
  RUNNING = 'running'
  FINISHED = 'finished'

  attr_reader :name, :host, :ttl_run, :lock_key

  def initialize name, opts={}, &block
    puts "#{name} name"
    # Set attrs
    @name = name # Used as a key in Redis
    @ttl_run = opts.fetch :ttl_run, 4.hours # Hold lock at RUNNING for this long. Suggested 1.5x the scheduling interval
    ttl_done = opts.fetch :ttl_done, 15.minutes # Hold lock at FINISHED for this long
    @host = `hostname`.strip
    @lock_key = "#{name} #{LOCK}"
    # Get lock from redis
    claim_lock
    # run job
    if @state.nil?
      redis.set "#{name} begun", Time.now.rfc2822, ex: 1.week
      yield
      redis.set lock_key, "#{FINISHED} #{host}", ex: ttl_done
      redis.set "#{name} done", Time.now.rfc2822, ex: 1.week
    elsif @state == RUNNING && redis.ttl(REDIS_KEY) < (ttl_run / 2)
      # Send notification that a task is running much longer than expected
      # (`redis.ttl` gives the _remaining_ time that a key has before the cache
      # expires it.)
      msg = "Expected previous task #{name} to have terminated cleanly. Check whether task \"#{@state}\" is still running."
      $stderr.puts msg
      ExceptionNotifier.notify_exception RuntimeError.new(msg) if Rails.env.production?
    end
  end

  private

  def claim_lock
    # Claim a lock. (This runs atomically so that check-and-set is supported.)
    redis.watch(lock_key) do
      @state = redis.get(lock_key)
      if @state.nil? # Lock is available
        redis.multi do |multi|
          multi.set lock_key, "#{RUNNING} #{host}", ex: ttl_run
        end
      else
        redis.unwatch
      end
    end
  end

  def redis
    @redis ||= Redis.new host: APP_CONFIG['socket']['redis_host'], port: APP_CONFIG['socket']['redis_port']
  end
end
