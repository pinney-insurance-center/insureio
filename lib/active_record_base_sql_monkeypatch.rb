# These methods are for developer-use. When performing something dangerous
# with data, this is a quick way to save a local copy of the data which can be
# swiftly restored into the db.
module ActiveRecordBaseSqlMonkeyPatch
  # Generate SQL to delete any record with this record's id and to insert this exact record (including id and timestamps).
  def to_sql
    # Build `DELETE` SQL
    table = self.class.arel_table
    id_col = table.primary_key.name
    sql_delete = "DELETE FROM #{table.name} WHERE #{id_col} = #{self[id_col]}"
    # Build `INSERT` SQL
    arel_attributes = arel_attributes_with_values_for_create attribute_names
    arel_attributes[Arel::Attributes::Attribute.new self.class.arel_table, 'id'] = id
    sql_insert = self.class.arel_table.create_insert.tap{ |im| im.insert(arel_attributes) }.to_sql
    # Output
    <<~EOF
      #{sql_delete};
      #{sql_insert};
    EOF
  end

  # Write a SQL file to delete any record with this record's id and to insert this exact record (including id and timestamps).
  def to_sql_file outdir=nil, fname=nil
    outdir ||= Dir.home
    fname ||= begin
      klass_name = self.class.name.underscore.gsub(/\//,'-')
      timestamp = Time.now.strftime '%Y-%m-%d'
      "restore-#{timestamp}-#{klass_name}-#{id}.sql"
    end
    File.write File.join(outdir, fname), to_sql
  end
end

# Ensure that under no circumstances is any reference to `ActiveRecord::Base`
# in this file treated as the primary load of ActiveRecord::Base
ActiveRecord::Base.class_eval { include ActiveRecordBaseSqlMonkeyPatch }
