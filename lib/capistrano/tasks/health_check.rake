# Usage
# bundle exec cap [production|release|...] health

def health_check hostname
  Net::HTTP.start hostname, use_ssl: production?, verify_mode: OpenSSL::SSL::VERIFY_NONE do |http|
    res = http.get("/")
    raise "Expected 303 redirect from server #{hostname}. Got #{res.code}" unless res.code == "303"
    redirect_regex = production? ? /^https:\/\/[^\/]+\/login[^\/]*$/ : /^http:\/\/[^\/]+\/login[^\/]*$/
    raise "Expected redirect to http login page. Got #{res['location']}" unless res['location'] =~ redirect_regex
    res = http.get("/assets/application.js")
    raise "Expected 200 response for /assets/application.js. It could be that assets are not successfully compiled (yet)." unless res.code == "200"
  end
  puts "\033[32mHealth checks passed\033[0m"
rescue => ex
  $stderr.write "\033[31mError #{ex}\033[0m\n"
end

def production?
  stage == :production
end

desc "Perform a rudimentary up/down health check on each of the servers for this stage"
task :health do
  puts
  find_servers.each do |server|
    $stdout.write "%15s... " % server.host
    health_check server.host
  end
end
