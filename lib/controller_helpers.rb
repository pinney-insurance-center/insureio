module ControllerHelpers
  def require_case editable=false
    # Require login
    require_login
    return if performed?
    # Require case existence
    id = params[:case_id] || params[:id]
    @kase = @crm_case = Crm::Case.find_by id: id
    if @kase.nil?
      canned_fail_response status: 404
    # Require user permission on case
    elsif editable && !@kase.editable?(current_user)
      permission_denied object: @kase
    elsif !@kase.viewable? current_user
      permission_denied object: @kase
    end
  end

  def require_editable_case; require_case true; end

  def require_viewable_case; require_case false; end
end
