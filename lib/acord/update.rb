# This class performs Status updates to Cases and creates Notes.

# cf. request_single_update, apply_update in
# Processing::AgencyWorks::UpdateService

class Acord::Update
  include AuditLogger::Helpers
  audit_logger_name 'ACORD'

  attr_reader :missing_case_ids

  def initialize request, xml_fname
    @request = request
    @fname = xml_fname
    n_olife_elements = @request.xpath('OLifE').length
    raise ArgumentError.new "Expected 1 OLifE element in ACORD document. Got #{n_olife_elements}" unless 1 == n_olife_elements
    @guid = @request.at_xpath('TransRefGUID')
    @olife = @request.at_xpath('OLifE')
    @policies = @olife.xpath('Holding/Policy')
  end

  def apply
    # Get kases
    kases = Crm::Case.where policy_number: policy_numbers
    kases_map = Hash[kases.map { |k| [k.policy_number, k] }]
    @missing_case_ids = []
    # Perform updates
    @policies
    .sort { |a, b| max_policy_date(a) <=> max_policy_date(b) }
    .each do |policy|
      kase = kases_map[policy.at_xpath('PolNumber').content]
      if kase.nil?
        missing_case_ids << policy.at_xpath('PolNumber').content
        next
      end
      apply_one kase, policy
    end
  end

private

  def apply_one kase, policy
    # Status update (xguid is the guid for the xaction, not for the
    # Policy/Holding)
    acord_status = policy.at_xpath('PolicyStatus')['tc'].to_i
    old_status_type_id = kase.status&.status_type_id
    audit level: 'info', body: { crm_case_id: kase.id, old_status: old_status_type_id, acord_status: acord_status, xguid: @guid }
    new_status_type_id = Acord::Status::MAP[acord_status]
    # Update status, quoting details
    kase.sales_stage_id = Crm::StatusType.find(new_status_type_id).sales_stage_id
    kase.status_type_id = new_status_type_id
    kase.create_details_for_sales_stage
    details = kase.current_details
    # Update effective date
    effective_date = policy.at_xpath('EffDate')&.content
    kase.effective_date = effective_date unless effective_date.nil?
    # Update plan name
    plan_name = policy.at_xpath('PlanName')&.content
    details.plan_name = plan_name unless plan_name.nil?
    # Update premium
    payment_amt = policy.at_xpath('PaymentAmt')&.content
    details.planned_modal_premium = payment_amt.to_i unless payment_amt.nil?
    payment_mode_id = policy.at_xpath('PaymentMode').try(:[], 'tc')
    unless payment_mode_id.nil?
      payment_mode = Acord::PAYMENT_MODES[payment_mode_id.to_i]
      details.premium_mode_id = payment_mode.insureio_id
    end
    # Create Notes
    # cf. https://modelviewers.pilotfishtechnology.com/modelviewers/ACORD/model/AttachmentType.html
    policy.parent.xpath('Attachment').each do |attachment|
      if attachment.at_xpath('AttachmentType').try(:[], 'tc') == "2" # Comment/Remark
        note_text = %w[AttachmentSource AttachmentData].map { |k| attachment.at_xpath(k).content.strip }.join(": ")
        note_date = Date.parse attachment.at_xpath('LastUpdate').content
        Note.find_or_create_by text: note_text, created_at: note_date, notable: kase
      end
    end
    # Save
    kase.save!
    details.save!
  rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotFound => ex
    ExceptionNotifier.notify_exception ex, data: { message: "kase: #{kase.id}. File: #{@fname}. ACORD guid: #{@guid}.\n#{kase.errors.full_messages.join(" \n")}" }
  end

  def max_policy_date policy
    %w[
      CarrierInputDate
      EffDate
      IssueDate
      LastUnderwritingActivityDate
      PlacementEndDate
      ReceivedDate
      RequestedDate
    ].map do |name|
      policy.xpath(name).compact.map(&:content).map { |c| Date.parse(c) }
    end.flatten.max
  end

  def policy_numbers
    @policies.map { |p| p.at_xpath('PolNumber') }.map(&:content)
  end
end
