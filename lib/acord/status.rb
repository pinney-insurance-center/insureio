module Acord::Status
  begin
    name_map = {
      0 => nil, # Unknown OLI_UNKNOWN 0
      1 => 'Issued - Inforce', # Active (inforce)  OLI_POLSTAT_ACTIVE  1 A Policy Status of 'Active' only indicates that at least one, but possibly more, coverages or components of this contract are active. Each individual coverage must be evaluated to determine the status of each specific component. If any coverage is active, the contract is active.
      2 => nil, # Inactive  OLI_POLSTAT_INACTIVE  2
      3 => 'Issued - Inforce', # Paid-Up OLI_POLSTAT_PAIDUP  3 The life insurance contract has reached the end of its contractually defined premium-paying period. No further premiums are due, but the contract and its benefits are still in effect. For example => a 20-Pay Whole Life contract -- premiums are paid-up over a 20-year period and at the end of that period, the cash value continues to accumulate and the death benefit remains in effect until contract maturity or a claim is incurred.
      4 => nil, # Lapsed  OLI_POLSTAT_LAPSED  4
      5 => nil, # Lapse Pending.  OLI_POLSTAT_LAPSEPEND 5
      6 => nil, # Surrendered OLI_POLSTAT_SURR  6
      7 => 'Withdrawn - NTO', # Not Taken.  OLI_POLSTAT_NOTAKE  7
      8 => 'Submitted - Pending Decision', # Pending OLI_POLSTAT_PENDING 8 Request accepted for processing and under review by carrier.
      9 => nil, # Waiver  OLI_POLSTAT_WAIVER  9
      10 => nil, # Death Claim Pending.  OLI_POLSTAT_DTHPEND 10
      11 => nil, # Death Claim Paid. OLI_POLSTAT_DEATH 11
      12 => nil, # Proposed  OLI_POLSTAT_PROPOSED  12  Not yet submitted to carrier for consideration.
      13 => nil, # Canceled - Home Office Cancellation OLI_POLSTAT_HOCANCEL  13
      14 => nil, # Terminated  OLI_POLSTAT_TERMINATE 14
      15 => nil, # Under Disability  OLI_POLSTAT_DI  15
      16 => nil, # Cash Value Paid.  OLI_POLSTAT_CVPAID  16
      17 => nil, # Expired OLI_POLSTAT_EXPIRED 17
      18 => nil, # Extended Term OLI_POLSTAT_EXTTERM 18
      19 => nil, # Reissue OLI_POLSTAT_REISSUE 19
      20 => nil, # Reduced Paid Up OLI_POLSTAT_REDUCEDPAIDUP 20
      21 => 'Submitted - App Submitted', # Applied For OLI_POLSTAT_APPLIEDFOR  21  Transmission of a formal app is successful. This is only used for a formal application and only represents status from the perspective of the sender; i.e., the transmission of an application from a broker/vendor to the carrier. It does not reflect a carrier status.
      22 => nil, # Pending Transmission  OLI_POLSTAT_PENDINGTRANS  22  The application is completed and ready to be sent but transmission has not taken place.The transferring of a property title from one individual to another. OR The legal transfer and receipt of ownership rights.
      23 => 'Withdrawn - Incomplete', # Incomplete (application was incomplete).  OLI_POLSTAT_INCOMPLETE  23  Either all of the application fields were not completed or the applicant did not provide complete information.
      24 => 'Approved - as Applied', # Approved  OLI_POLSTAT_APPROVED  24  Approved for issue (may or may not have outstanding requirements).
      25 => 'Issued - Holding', # Issued  OLI_POLSTAT_ISSUED  25  All requirements were satisfied. Can be used with StatusReason to reflect it is not paid. Could also indicate all requirements satisfied but effective date not yet reached.
      26 => 'Approved - Other Than Applied', # Counter offer - made by HO  OLI_POLSTAT_COUNTEROFFER  26  Issued other than applied for. Must include at least one material change(s), may additionally include nonmaterial change(s)
      27 => 'Withdrawn - Declined', # Carrier decline to issue  OLI_POLSTAT_DECISSUE  27  The insurer refuses to issue a policy for the individual or company depending on the insurance company's terms of issuance of a policy.
      28 => nil, # Carrier decline to reinstate  OLI_POLSTAT_DECREINSTATE  28
      29 => 'Withdrawn - Postponed', # Carrier deferred (e.g. carrier is deferring coverage until certain conditions are met). OLI_POLSTAT_DEFERRED  29
      30 => nil, # Conversion with a new number  OLI_POLSTAT_CONVERTED 30  Conversion implies that a previous policy exists. While considered old business, a conversion usually has a new effective date. This status is intended for use on the new policy/system not the original.
      31 => nil, # Contractual change pending. OLI_POLSTAT_CTRCTCHANGE 31
      32 => nil, # Back Billed OLI_POLSTAT_BACKBILL  32  Not paid up to where you've been billed to.
      33 => nil, # Illustration declined OLI_POLSTAT_ILLUSDEC  33  Illustration System wants to return coverage to indicate it was declined.
      34 => 'Submitted - Pending Decision', # Eligible, Issue Pending OLI_POLSTAT_ELIGISSPEND 34
      35 => 'Withdrawn - Declined', # Declined Not Eligible OLI_POLSTAT_DECNOTELIG  35
      36 => nil, # Annuitized  OLI_POLSTAT_ANNUITIZED  36
      37 => nil, # Reinstated  OLI_POLSTAT_REINSTATED  37  Subtle short period between pending and active- awaiting policy exhibit
      38 => nil, # Pending Reinstatement OLI_POLSTAT_PENDREINST  38
      39 => nil, # Canceled - Customer canceled new business app OLI_POLSTAT_CANCELLED 39  Customer is withdrawing new business application
      40 => 'Issued - Inforce', # Matured OLI_POLSTAT_MATURED 40
      41 => nil, # Conversion Pending  OLI_POLSTAT_CONVPEND  41  This status is intended for use on the new policy/system not the original.
      42 => nil, # Grace Period  OLI_POLSTAT_GRACEPD 42  The policy has lapsed due to non payment of premiums and is in its contractual grace period prior to final termination
      43 => 'Approved - Other Than Applied', # Policy was issued with changes  OLI_POLSTAT_CHANGEISS 43  Policy was issued with non-material change(s).
      44 => 'Issued - Del Reqs.', # Issued with Requirements  OLI_POLSTAT_ISSUEDWREQUIREMENTS 44  Issued with outstanding requirements.
      45 => nil, # Invalid OLI_POLSTAT_INVALID 45
      46 => nil, # Pending Replacement OLI_POLSTAT_PENDREPLACEMENT 46
      47 => 'Issued - Inforce', # Fully Paid-Up OLI_POLSTAT_FULLPAIDUP  47  Fully Paid-Up insurance (FPU) is in effect following the election by the policy owner of the conversion option that allows the policy owner to stop paying premiums prior to the end of the normal premium-paying period, and receive a paid up policy for the full current face amount. FPU is a combination of the reduced paid-up and premium reduction non-forfeiture options.Fully Paid Up -- Fully Paid-Up insurance (FPU) is a type of non-forfeiture option available on life insurance contracts (class code 1) where the policy owner may optionally elect to cease paying premiums prior to the end of the normal premium paying period as defined in the contract, and use the total cash value of the contract as a Net Single Premium to purchase a fully paid-up policy for the full face amount of the original contract. The total cost for conversion to a fully paid-up contract is based on the net single premium for the full original face amount plus the cost to retain certain supplemental benefit riders. The total cash value of the contract needs to be sufficient to cover the NSP cost -- Total CV includes the guaranteed cash value, the cash surrender value of any PUA's and OYT Additions and any dividends on deposit. If the total cash value required to cover the NSP is less than the cost to convert to fully paid-up, the policy owner may pay the amount to cover the shortage of the NSP; if the total cash value exceeds the cost, any unneeded amount over the NSP will be refunded in cash.
      48 => nil, # Active - Preliminary Term OLI_POLSTAT_PRETERM 48  Both the Preliminary Term coverage and the basic policy have been approved and the premium for the Preliminary Term period has been paid, but the basic policy effective date has not been reached (Preliminary Term period still is in effect) nor has billing occurred for the initial premium, which has not been paid.
      50 => nil, # Canceled - Free Look  OLI_POLSTAT_FREELOOK  50  Policy owner exercised the free look provision to terminate this policy. In the United Kingdom and Europe, "Canceled - Free Look" is called "Cooling Off Cancellation" as the "Free Look Period" is called the "Cooling Off Period".
      51 => nil, # Policy was not placed OLI_POLSTAT_NOTPLACED 51  Carrier did not place the policy.
      52 => nil, # Coverage is "active" but one (or more) of insureds have died  OLI_POLSTAT_SOMEDEAD  52
      53 => nil, # Conditional Approval  OLI_POLSTAT_APPROVCOND  53
      54 => 'Submitted - Pending Decision', # Submitted to Underwriting OLI_POLSTAT_SUBMITTEDTOUW 54  The proposed policy is a completed application and is submitted to Underwriting. All initial information/requirements required to put the policy in Underwriting have been accepted.
      55 => nil, # Suspend OLI_POLSTAT_SUSPEND 55  Suspend indicates whether financial activity is suspended on a Policy or not. Some reasons for freezing a Policy could be because of the death of the insured, an internal processing problem, etc.
      56 => nil, # Re-entry pending  OLI_POLSTAT_REENTRYPENDING  56  Policy owner has elected to re-qualify for a premium rate comparable to a new issue premium rate for someone of the same age.
      57 => 'Saved Quote', # Quoted  OLI_POLSTAT_QUOTED  57  A request for a rate quote or an informal application (AKA trial application) was received by the carrier. If the carrier was able to satisfy the request the status is quoted.
      58 => 'Submitted - Tentative Offer', # Approved Tentative Offer  OLI_POLSTAT_APPROVEDTENTOFFER 58  Has been approved with tentative offer – as opposed to final offer. This is only used for a formal application.
      59 => nil, # HO Withdrew OLI_POLSTAT_WITHDRAWNHO 59  Status of coverage for an inquiry or formal application that the HO withdrew
      60 => nil, # Producer Withdrew OLI_POLSTAT_WITHDRAWNFLD  60  Status of coverage for an inquiry or formal application that the Producer (Agent) withdrew (changed mind, etc.)
      61 => nil, # Lapsing on a daily cost basis OLI_POLSTAT_LAPSINGDAILY  61
      62 => 'Withdrawn - Postponed', # Postponed OLI_POLSTAT_POSTPONED 62  Used when a case needs to be re-opened from an issued status. Deprecated in favor of Policy Status "OLI_POLSTAT_DEFERRED".
      63 => nil, # Block Conversion  OLI_POLSTAT_CONVERTEDBLOCK  63  Used when the issue system is used as a conversion vehicle for moving multiple policies from another administration system. This status is intended for use on the new policy/system not the original.
      64 => nil, # Conversion with the same number OLI_POLSTAT_CONVERTEDSAME 64  Conversion implies that a previous policy exists. The inforce policy with the same number must be terminated and carry exit code conversion out.
      65 => 'Withdrawn - NTO', # Not Taken Out OLI_POLSTAT_NOTTAKENOUT 65  The Not Taken Out status indicates that the contract was declined before issue. The party declining the contract may be the contract owner or insurance company itself.
      89 => nil, # Terminated - Reserves Released  OLI_POLSTAT_TERMRESRELEASD  89
      90 => nil, # Active Rider Terminated - Policy Converted to NFO (non forfeiture options)  OLI_POLSTAT_RDRTERMNFO  90  Original rider coverage is modified to reflect new non-forfeiture paid-up conditions
      91 => nil, # Paid-Up Rider Terminated - Policy Converted to NFO (non forfeiture options) OLI_POLSTAT_PDUPRDRTERMNFO  91  Original paid-up rider coverage is modified to reflect NFO Conditions
      92 => nil, # Status of 'member left' OLI_POLSTAT_MEMBERLEAVES  92  A member has left employment
      93 => nil, # Status of 'Departure Extension' OLI_POLSTAT_DEPARTUREEXTENSION  93  A member is on departure extension
      94 => nil, # Inforce- Attained Age Conversion  OLI_POLSTAT_INFAGECONF  94  Attained age of insured being reached resulted in a conversion, but policy remains inforce
      95 => nil, # Disability claim pending without policy termination OLI_POLSTAT_DISCLAIM  95  A disability claim against the policy, which does not lead to the termination of the policy, is pendingThis is a disability claim on a Life coverage.
      97 => nil, # Reconsider and Approve  OLI_POLSTATUS_RECONSIDER  97  Final action procedure done on a case which has gotten Further Information Unobtainable (FIU) rejected and subsequently received the Money and/or pre-issue forms within the 45 day grace period.
      98 => 'Withdrawn - Declined', # FIU New Business Rejection  OLI_POLSTATUS_FIUREJECT 98  A case may be rejected for FIU (Further Information Unobtainable) due to insufficient money or outstanding pre issue forms. A case pending in new business has a grace period (such as 45 days) for money and the pre issue forms. If these details are unobtainable for any reason, the underwriter FIU rejects the case.
      99 => nil, # Exercised OLI_POLSTAT_EXERCISED 99  A status which indicates that the associated Option or Rider has been exercised.
      100 => nil, # Claim pending without policy termination  OLI_POLSTAT_PENDCLAIMNOTERM 100 A claim against the policy, which does not lead to the termination of the policy, is pending
      103 => 'Issued - Holding', # Approve - Issue Hold  OLI_POLSTAT_ISSUEHOLD 103 To have the case status 'Case is approved but issue process is on hold'. Case will not be issued until this hold is removed
      106 => nil, # Underwriting complete, policy paid  OLI_POLSTAT_NOTPAID 106 This accounts for the fact that some companies will not issue the policy until something has been paid towards it.
      107 => nil, # Underwriting incomplete; policy paid  OLI_POLSTAT_PAIDUNDINC  107
      108 => nil, # Under Retrenchment (Unemployment) OLI_POLSTAT_RETRENCH  108 Under Retrenchment (Unemployment) status indicates that the insured is receiving a retrenchment benefit from this policy.
      109 => nil, # Request Refused OLI_POLSTAT_REQREFUSE 109 The submission of a request for a quote, trial/informal or formal application cannot be processed
      110 => 'Withdrawn - Declined', # Risk Not Acceptable OLI_POLSTAT_RISKNOTACCEPT 110 Has gone through an underwriting process for an informal application and determined that the risk was not acceptable to the carrier
      111 => nil, # Rescinded OLI_POLSTAT_RESCINDED 111 Type of policy cancellation by the carrier during the contestable period. The rescission is usually triggered by a death claim where misrepresentation has been discovered in the information provided by the applicant.
      113 => 'Issued - Holding', # Dormant OLI_POLSTAT_DORMANT 113 This status applies to features that are not yet providing coverage, but for which some maintenance still needs to be performed. These features become active when triggered by some action on the policy such as the expiration of another feature.
      114 => nil, # Canceled - Customer canceled reinstatement request  OLI_POLSTAT_CANCELREINST  114 Customer withdrew their application for reinstatement.
      115 => nil, # FIU Reinstatement Rejection OLI_POLSTAT_FIUREJECTREINST 115 A reinstatement request may be Further Information Unobtainable (FIU) rejected for insufficient money or outstanding information.
      116 => nil, # Candidate OLI_POLSTAT_CANDIDATE 116 Candidate Policy - Not yet proposed.
      117 => nil, # Vested  OLI_POLSTAT_VESTED  117 A person's right to the full amount of some type of benefit, most commonly employee benefits such as stock options, profit sharing or retirement benefits. Fully vested benefits often accrue to employees each year, but they only become the employee's property according to a vesting schedule. Vesting may occur on a gradual schedule, such as 25% per year, or on a "cliff" schedule where 100% of benefits vest at a set time, such as four years after the award date.
      122 => nil, # Canceled - no additional information specified  OLI_POLSTAT_GENERICCANC 112 To be used in situations where a policy that was active at some point is canceled, but no additional information is known. For example, it is unknown which entity (carrier or customer) initiated the cancellation.
      2147483647 => nil, # Other OLI_OTHER 2147483647
    }
    status_types = Hash[Crm::StatusType.all.pluck(:name, :id)]
    self::MAP = Hash[name_map.map { |acord_id, status_type_name|
      if status_type_name
        # Raise error if we expect to find match but fail
        [acord_id, status_types.fetch(status_type_name)]
      else
        [acord_id, nil]
      end
    }]
  end
end
