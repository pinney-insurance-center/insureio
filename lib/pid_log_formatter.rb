class PidLogFormatter < ActiveSupport::Logger::SimpleFormatter

  def call(severity, time, progname, msg)
    modified_msg = "<%d> %s" % [Process.pid, msg.to_s]
    super(severity, time, progname, modified_msg)
  end

end
