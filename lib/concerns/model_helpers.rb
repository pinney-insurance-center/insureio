module Concerns
  module ModelHelpers
    module ClassMethods
      def has_col? col_name
        table_exists? && columns_hash.include?(col_name.to_s)
      end

      def table_exists?
        connection.table_exists?(table_name)
      end
    end
  end
end
