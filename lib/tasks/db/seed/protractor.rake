namespace :db do
  namespace :seed do
    desc "Seed records for running protractor tests"
    task :protractor => :environment do
      ####################
      # Create a brand
      ####################
      b = Brand.find_or_initialize_by(id: 1)
      b.full_name = 'First Brand'
      b.tenant_id = 0
      b.ownership_id = 1
      b.save!(validate: false)
      ####################
      # Create Users
      ####################
      user_attrs = { tos: true, membership_id: -1, brand_id: 1, tenant_id: 0, active_or_enabled: true }
      u = User.find_or_initialize_by(id: 1)
      u.attributes = user_attrs.merge login: 'grace'
      u.crypted_password = Authlogic::CryptoProviders::Sha512.encrypt 'qwer1234', u.password_salt
      u.can! :super_edit
      u.can! :lead_distribution
      u.save! validate: false
      u.update_columns quoter_key: 'thisismyquoterkey' # `quoter_key` caused a lot of problems. I'm updating it especially here
      b.members << u
      # Create an NMB user
      NMB_TENANT_ID = 10
      u = User.find_or_initialize_by(id: 10)
      u.attributes = user_attrs.merge login: 'nmb',tenant_id: NMB_TENANT_ID
      u.crypted_password = Authlogic::CryptoProviders::Sha512.encrypt 'qwer1234', u.password_salt
      u.can! :super_edit
      u.can! :lead_distribution
      u.save! validate: false
      u.update_columns quoter_key: 'thisismyquoterkey' # `quoter_key` caused a lot of problems. I'm updating it especially here
    end
  end
end
