SYSTEM_TASK_TYPES = ['email', 'email agent']
MAX_TASK_ATTEMPTS = 2

desc "Execute all system-owned email tasks which belong to active models"
task :execute_past_due_tasks => :environment do
  log = AuditLogger['rake-crm-tasks']
  log.info "== STARTING RAKE =="

  local_rake_processes=`ps aux | grep 'execute_past_due_tasks'`
  # The above generates an instance of `sh`, and an instance of `grep`.
  # Rake itself generates an instance of `bash` and an instance of `ruby` when running this script.
  # All 4 of those commands contain the search term.
  # Therefor, we are testing for a scenario with more than 4 matching commands.
  local_count=local_rake_processes.split("\n").length-4
  if local_count>0
    log.info "== STOPING RAKE, since #{local_count} prior instance#{local_count>1 ? 's are' : ' is'} still running=="
    log.info "Output from `ps aux`:\n#{local_rake_processes}"
    return
  end

  while 1
    # Get next task to execute. It is selected alone and not en masse because
    # we care about the value of is_executing.
    #
    # Autodial (phone) tasks are not sought here because they are not controlled by a cronjob.
    # They should be requested by the client's browser when the client is logged in.
    task_to_execute = Task.handled_by_system
      .due
      .active
      .where('TRUE
        /* This query originated in `tasks.rake`.\
           It is a fallback, intended to immediately execute any past due tasks.
           It does not interact with the Sidekiq queue.
        */')
      .where('suspended IS NOT TRUE')
      .where("task_type_id IN (?)", Enum::TaskType.id(*SYSTEM_TASK_TYPES))#ignores sms and marketech tasks.
      .where("failed_attempts < ?", MAX_TASK_ATTEMPTS)
      .where(is_executing:false).first
    # Break if there are no more tasks to execute
    if task_to_execute.nil?
      log.info "== OUT OF TASKS =="
      break
    end
    # Execute. Handle contingency of StaleObjectError (optimistic locking exclusion).
    begin
      logf 'start', task_to_execute
      # Get a lock on this object by setting :is_executing. This should raise
      # a StaleObjectError if another process has already locked this for
      # execution. Otherwise, this will prevent other processes from executing
      # this task
      task_to_execute.execute_from_bg
      logf 'finish', task_to_execute
    rescue ActiveRecord::StaleObjectError
      logf 'stale', task_to_execute
      next
    rescue => ex
      task_to_execute.increment!(:failed_attempts, 1) if task_to_execute
      logf 'error', task_to_execute
      log.error ex.message
      ExceptionNotifier.notify_exception(ex, data: "System task execution failed.\nTask to execute: #{task_to_execute.inspect}")
    end
  end
end

desc "Set 'active' field, inferring from task.active? for all Task"
task :set_active_for_tasks do
  Task.all.each do |task|
    task.update_attribute :active, task.active?
  end
end

def logf action, task
  AuditLogger['rake-crm-tasks'].info "%8s |task %7d| |pid %6d|" %[action, task.id, Process.pid]
end
