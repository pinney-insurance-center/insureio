namespace :agency_works do
  desc "Get status updates from AgencyWorks"
  task :get_updates => :environment do
    #This function will spawn one Sidekiq job per status type we check for,
    #which is necessary because each status type must make a separate request to the AW API,
    #and their API is very slow to respond.
    Processing::AgencyWorks::UpdateService.request_all_updates
  end

  desc "Get status updates from User Acceptance Testing (UAT) environment at AgencyWorks"
  task :uat_test_updates => :environment do
    # Assign vars for UAT enviroment
    wsdl = 'http://downloads.ipipeline.com/ai/AcordService-UAT.wsdl'
    APP_CONFIG['agency_works']['accounts']={
      'uat'=>{
        'username'=> 'PinneyUAT',
        'password'=>'U78dr29h'
      }
    }
    @errors = [] # Email these to developers if non-empty
    # Iterate through status types and send a SOAP request for each
    Processing::AgencyWorks::UpdateService::STATUS_TYPE_CODES.each do |status_type_code|
      begin
        request_body = Processing::AgencyWorks::XmlBuilder.new.build_request_for_updates(status_type_code,'uat')
        client = Savon.client(
          wsdl:wsdl,
          ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
          ssl_verify_mode: :none,
          ssl_version: :TLSv1,
          convert_request_keys_to: :none,
          headers: {'SOAPAction' => ''}, # AW no longer supports this header, but rather chokes on it <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>The given SOAPAction tXLifeOperation does not match an operation.</faultstring></soap:Fault></soap:Body></soap:Envelope>
          log_level: :debug,
          log: true,
          open_timeout: Processing::AgencyWorks::SOAP_SEND_TIMEOUT,
          read_timeout: Processing::AgencyWorks::SOAP_RECEIVE_TIMEOUT
        )
        op = client.operation(:t_x_life_operation)
        response = op.call message:{ 'content' => request_body }
        content = response.body[:t_x_life_operation_response][:return][:message]
        raise "No content from AgencyWorks SOAP response for #{status_type_code}" unless content.present?
      rescue Exception => ex
        @errors.push(ex)
      end
    end
    ExceptionNotifier.notify_exception("AgencyWorks UAT rake failed", data: @errors.inspect) if @errors.present?
  end
end
