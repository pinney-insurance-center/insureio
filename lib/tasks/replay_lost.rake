desc "Replay requests which were not processed by the application but stored at log/lost-requests.log*"
task :replay_lost => :environment do
  FileUtils.mkdir_p 'log/in-process'
  FileUtils.mkdir_p 'log/processed'
  log_paths = Dir['log/lost-requests.log*']
  log_paths.sort_by { |f| File.mtime(f) }.each do |f|
    basename = File.basename f
    in_process_path = File.join 'log/in-process', basename
    processed_path = File.join 'log/processed', basename
    `mv -n #{f.inspect} #{in_process_path.inspect}`
    File.new(in_process_path).each do |line|
      next if line =~ /^#/
      RequestReplayer.new(JSON.load line).run
    end
    `mv --backup=numbered #{in_process_path.inspect} #{processed_path.inspect}`
  end
end
