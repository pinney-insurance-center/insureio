module Rake::Doc
  def self.start_html_file identifier, html_name = nil
    `mkdir --parents doc/html`
    path = "doc/html/#{html_name || identifier}.html"
    puts "Starting HTML file #{path}"
    f = File::open path, "w"
    f.puts %Q{
    <!DOCTYPE html>
    <html>
    <head>
        <title>#{identifier}</title>
    </head>
    <body>
        <h1>#{identifier}</h1>
    }
    f
  end

  def self.create_doc_for_klass title, klass
    f = start_html_file title, "#{title}-fields"
    f.puts '<table>'
    klass.fields.each do |field|
      comment = field[2]
      if field[0] =~ /_id$/
        type = :DiseaseType if field[0].to_s == 'type_id'
        type ||= field[0].to_s.sub(/_id$/, '').camelize.to_sym
        if Enum.constants.include?(type)
          comment = [comment, %Q{See <a href="/doc/Enums.html\##{type}">#{type}</a> enum for legal values.}].compact.join(' ')
        end
      end
      f.puts %Q{<tr><td><tt>#{field[0]}</tt></td><td><tt>#{field[1]}</tt> #{comment}</td></tr>}
    end
    f.puts '</table></html>'
    f.close
  end
end

task :doc => ['doc:html', 'doc:sync']
namespace :doc do

  task :html => ['html:health_info', 'html:enums']
  namespace :html do
    desc 'Create Enums.html'
    task :enums => :environment do
      puts 'in enum task'
      f = Rake::Doc.start_html_file('Enums')
      Enum.constants.each do |c|
        konst = "Enum::#{c}".constantize
        next unless konst < Enum::EnumStereotype && konst.first&.respond_to?(:name)
        f.puts %Q{<h2 id="#{c}">#{c}</h2>}
        f.puts '<table>'
        f.puts %Q{<tr><th>ID</th><th>NAME</th></tr>}
        konst.all.each do |item|
          f.puts %Q{<tr><td><tt>#{item.id}</tt></td><td>#{item.name}</td></tr>}
        end
        f.puts '</table>'
      end
      f.close
    end

    task :health_info => [:diseases] do
      Rake::Doc.create_doc_for_klass 'Crime', Crm::HealthInfo::Crime
      Rake::Doc.create_doc_for_klass 'MovingViolation', Crm::HealthInfo::MovingViolation
    end

    task :diseases => :environment do
      puts "in html task"
      # Start disease-type file
      g = Rake::Doc.start_html_file "Disease types", "disease-types"
      g.puts '<table>'
      g.puts '<tr><th>NAME (unused in API)</th><th>TYPE</th></tr>'
      # Get set of classes for disease types
      klasses = Set.new Crm::HealthInfo::DISEASES_MAP.values
      # Iterate through disease types
      Enum::DiseaseType.each do |type|
        # Get title for this type
        label = type.name
        title = type.name.camelize
        # Get class for this type
        if type.klass
          klass = "Crm::HealthInfo::#{type.klass}".constantize
        else
          klass = "Crm::HealthInfo::#{title}".constantize
          raise "Class missing from Crm::HealthInfo: #{title}" unless klasses.delete?(klass)
        end
        # Add to disease-type file
        g.puts %Q{<tr><td>#{title}</td><td><a href="/doc/#{klass.name.demodulize}-fields.html">#{klass.name.demodulize}</a></td></tr>}
        # Start doc file for this type
        Rake::Doc.create_doc_for_klass title, klass        
      end
      Rake::Doc.create_doc_for_klass 'OtherCancer', Crm::HealthInfo::OtherCancer
      Rake::Doc.create_doc_for_klass 'OtherDisease', Crm::HealthInfo::OtherDisease
      g.puts '</table></html>'
      g.close
    end
  end

  task :sync do
    puts "in sync task"
    %x{rsync -rat -e 'ssh -p 4000' doc/html/ deploy@cms.pinneyinsurance.com:doc/}
  end
end
