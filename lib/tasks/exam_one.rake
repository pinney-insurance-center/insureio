namespace :exam_one do
  desc "Seed db with control codes (Paramed codes) for ExamOne"
  task :seed => :environment do
    require File.join Rails.root, 'db/seeding/exam_one'
  end

  desc "Get status updates from ExamOne"
  task :get_statuses => :environment do
    Processing::ExamOne.get_statuses
  end
end
