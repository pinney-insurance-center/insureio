require 'active_record_base_sample.rb'
begin
  require 'forgery' if Rails.env!='production'
rescue LoadError
end

# Override/augment db:test:prepare so as not to lose our enum data in our views
namespace :db do
  task :backup => :environment do
    config = ActiveRecord::Base.connection_config
    options = []
    options << "-u #{config[:username]}"
    options << "-p#{config[:password]}"
    options << "-P #{config[:port]}" if config[:port]
    options << "-h #{config[:host]}" if config[:host]
    %x(mysqldump #{options.join(' ')} --single-transaction --quick #{config[:database]} | gzip | gpg --encrypt -r 'Ryan Pinney' --output ~/dr-dump.sql.gz.gpg --batch --yes)
  end

  task :fix => :environment do
    (0..4).each do |i|
      ActiveRecord::Base.connection.execute "ALTER TABLE users MODIFY permissions_#{i} INT UNSIGNED;"
    end
  end

	namespace :test do
		task :prepare do
      Rake::Task['db:schema_supplement'].invoke
			Rake::Task['db:seed'].invoke
		end
	end

	task :truncate => :environment do
		tables = ActiveRecord::Base.connection.execute('SHOW FULL TABLES')
		tables.each{ |tab|
      next if tab[0] =~ /schema_migrations/
      if tab[1] =~ /BASE TABLE/i
				ActiveRecord::Base.connection.execute "TRUNCATE #{tab[0]}"
  			ActiveRecord::Base.connection.execute "ALTER TABLE #{tab[0]} AUTO_INCREMENT = 1"
      end
		}
	end

  task :clean => ["db:drop", "db:create", "db:schema:load", "db:migrate", "db:fix", "db:seed"]
  task :reload => ["db:clean", "db:seed:dummy"]
  task :seed => ['db:seed:carriers']

	namespace :seed do

    task :carriers => :environment do
      Carrier # Without this reference, Rails will not load the model files and raise 'undefined class/module'
      load 'db/seeding/carriers.rb'
    end

    task :dummy => ["dummy:usage", "dummy:crm", "dummy:marketing"]

	  namespace :dummy do

      task :usage => [:user, :users, :brands]

      task :user => :environment do
        require 'factory_bot_rails'
        FactoryBot.create :usage_security_answer, user_id:developer.id
      end

      task :users => :environment do
        require 'factory_bot_rails'
        @agents = [developer]
        agents.concat FactoryBot.create_list :user_w_assoc, 2, parent:developer
        # create, assign children for @users
        agents.each{ |user|
          user.can! :lead_distribution # Grant agent permission
          FactoryBot.create_list :user_w_assoc, rand(2), parent:user
        }
      end

      task :brands => :environment do
        require 'factory_bot_rails'
        # create brands for users
        FactoryBot.create_list :brand_w_assoc, 2, owner_id:developer.id
        agents.each do |user|
          FactoryBot.create_list :brand_w_assoc, rand(4), owner_id:user.id
        end
      end

      task :crm => :environment do
        require 'factory_bot_rails'
        # create consumers for users
        @connections = []
        agents.each{ |agent|
          @connections += FactoryBot.create_list :consumer_w_assoc, rand(3), agent:agent
        }
        @connections += FactoryBot.create_list(:consumer_w_assoc, 3, agent:developer)

        # status types
        Rake::Task['db:seed:dummy:status_types'].invoke

        # create Cases and lead types
        tag_values = [nil,'arf','qrf','foo','bar','baz','qux','daisy chain']
        @cases = []
        @connections.each{ |conn|
          # create, assign cases for connections
          @cases += FactoryBot.create_list :crm_case_w_assoc, rand(4), consumer:conn
          # create, assign tags for connections
          rand(5).times{ conn.tags << Tag.new(value: tag_values.sample) }
          # create lead types, assign to connections
          lead_type_name = tag_values.sample
          conn.lead_type = lead_type_name if lead_type_name.present?
          conn.save
        }
        # create Cases for @user.connections
        developer.consumers.where(entity_type_id: Enum::EntityType.id('person', 'trust')).each { |conn|
          conn.brand ||= FactoryBot.create :brand, owner_id:developer.id
          FactoryBot.create_list :crm_case_w_assoc, 2+rand(2), consumer:conn
          # create, assign tags for connections
          (2+rand(2)).times{ conn.tags << Tag.new(value: tag_values.sample) }
          # create lead types, assign to connections
          lead_type_name = tag_values.sample
          conn.lead_type = lead_type_name if lead_type_name.present?
          conn.save
        }

        # create Tasks for cases
        Rake::Task['db:seed:dummy:tasks'].invoke
      end

      task :status_types => :environment do
        require 'factory_bot_rails'
        task_type_n = Enum::TaskType.count
        status_types = []
        # create system-wide StatusTypes
        status_types += FactoryBot.create_list :crm_status_type, 3
        # create user-owned StatusTypes
        status_types += FactoryBot.create_list :crm_status_type, 2, owner_id:developer.id
        # create a template
        template = FactoryBot.create :marketing_email_template
        # create task builders
        status_types.each{|st|
          task_type_id = 1 + rand(task_type_n)
          FactoryBot.create_list :crm_task_builder, rand(3), status_type_id:st.id, task_type_id:task_type_id, role_id:Enum::UsageRole.sample.id, template:template
        }
      end

      desc "build Tasks of various types, pertaining to developer"
      task :tasks => :environment do
        kases = Crm::Case.where(agent_id:developer.id).to_a
        # email
        # email agent
        # autodial
        2.times {
          kases.sample.status.tasks.create({
            task_type_id:Enum::TaskType.id('phone dial'),
            label:Forgery(:name).company_name,
            due_at:(Time.now + rand(999999) - rand(999999)),
            assigned_to_id:developer.id
          })
        }
      end

      desc "build Marketing templates for email, snail mail, sms"
      task :marketing => :environment do
        require 'factory_bot_rails'
        # smtp settings
        if ActionMailer::Base.smtp_settings and ActionMailer::Base.smtp_settings[:user_name]
          settings = ActionMailer::Base.smtp_settings.clone.tap { |s| s[:tls] = s.delete(:enable_starttls_auto) }
          settings[:password_confirmation] = settings[:password]
          Marketing::Email::SmtpServer.find_or_create_by_owner_id(developer.id, settings)
        end
        # unowned templates
        FactoryBot.create :marketing_email_template
        FactoryBot.create :marketing_message_media_template
        FactoryBot.create :marketing_snail_mail_template
        # templates for developer
        FactoryBot.create :marketing_email_template, owner_id:developer.id
        FactoryBot.create :marketing_message_media_template, owner_id:developer.id
        FactoryBot.create :marketing_snail_mail_template, owner_id:developer.id
        # campaigns for developer
        FactoryBot.create :marketing_campaign, owner_id:developer.id
      end

      desc "create a Consumer, Case, Status, TaskBuilder, Brand, CaseManager where agent is developer"
      task :holistic => :environment do
        require 'factory_bot_rails'
        # ensure case manager exists for developer
        if developer.staff_assignment.try(:case_manager).nil?
          developer.create_staff_assignment if developer.staff_assignment.nil?
          case_manager = User.can(:super_edit) || FactoryBot.create(:user_w_assoc).tap { |u| u.can! :super_edit }
          developer.staff_assignment.update_attributes case_manager_id:case_manager.id
        end
        status_type = FactoryBot.create(:crm_status_type)
        developer.update_attributes initial_status_type_id:status_type.id
        template = Marketing::Email::Template.create! name:'test', subject:"test subj", body: %q[
          branded email: {{ brand_specific_fallback_primary_email_for_agent }}
          agent address: {{ agent.address }}
          case mgr first name: {{ case_manager.first_name }}
          quoted details face amount: {{ case.quoted_details.face_amount | currency}}
          3 days from now: {{ 3 | days_from_now }}
          client birth: {{ client.birth }}
          brand name: {{ brand.name }}
        ]
        task_builder = Crm::TaskBuilder.create status_type_id:status_type.id, template_id:template.id, delay:1, task_type_id:Enum::TaskType.id('email consumer')
        client = FactoryBot.create(:consumer_w_assoc, agent:developer)
        kase = FactoryBot.create(:crm_case_w_assoc, consumer:client)
      end
  	end
  end

  desc "Changes schema in ways which Rails schema file does not store but in ways on which this app depends."
  task :schema_supplement => [:environment] do
    ActiveRecord::Base.connection.execute('ALTER TABLE users MODIFY permissions_0 INTEGER ZEROFILL UNSIGNED')
    ActiveRecord::Base.connection.execute('ALTER TABLE users MODIFY permissions_1 INTEGER ZEROFILL UNSIGNED')
    ActiveRecord::Base.connection.execute('ALTER TABLE users MODIFY permissions_2 INTEGER ZEROFILL UNSIGNED')
    ActiveRecord::Base.connection.execute('ALTER TABLE users MODIFY permissions_3 INTEGER ZEROFILL UNSIGNED')
  end
end

private

# Cache main user (developer) for quicker user
def developer
  @developer ||= (
    attributes = {login:'username', password:'password', password_confirmation:'password'}
    user = User.find_by_login('username') || FactoryBot.build(:user_w_assoc)
    user.attributes = attributes
    user.super!
    user.enabled = true
    user.save!
    user
  )
end

# Cache agents for quicker use
def agents
  @agents ||= User.agents.to_a << developer
end
