
namespace :rtt do
  desc "Transfer XML files"
  task :transfer_xml_files => :environment do
    log = AuditLogger['rake-rtt-transfer_xml_files']
    log.info "== STARTING RAKE =="
    hostname=`echo $HOSTNAME`
    port  =APP_CONFIG['scor']['ftp']['port']
    user  =APP_CONFIG['scor']['ftp']['username']
    server=APP_CONFIG['scor']['ftp']['server']
    path  =APP_CONFIG['scor']['ftp']['path']
    gpg_files=`ls -1 tmp/*.xml.gpg`.split("\n")
    log.info "found #{gpg_files.length} files."
    gpg_files.each do |filename|
      log.info "Attempting to transfer file #{filename}"
      scp_cmd_output=`scp -P#{port} #{Rails.root}/tmp/#{filename} #{user}@#{server}:#{path} && echo 'success'`
      if scp_cmd_output!="success\n"
        log.error "Failed to transfer file #{filename}"
        SystemMailer.warning("Failed to transfer file #{filename} from server #{hostname} to the RTT SFTP endpoint.\n"+
          "This message is generated from the cleanup task, which means this file failed to transfer at least once before.")
      else#delete the files from tmp folder
        `rm #{filename}`
        `rm #{filename.sub(/.gpg$/,'')}`
      end
    end
    log.info "== END RAKE =="
  end
end