namespace :sbli do
  desc "Pull ACORD documents from SBLI SFTP server and update our records"
  task :update => :environment do
    SyncedRunner.new('SBLI_UPDATE') do
      Sbli.update_local_records
    end
  end
end
