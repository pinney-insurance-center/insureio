
desc "Re-attempt upload of any app page snapshot files which were generated but could not be uploaded to AWS prior."
task :cleanup_failed_snapshot_uploads => :environment do
  log = AuditLogger['rake_cleanup_failed_snapshot_uploads']
  log.info "== STARTING RAKE =="
  files_to_upload=`ls -1 tmp/ | grep -E 'app_page_snapshot_[0-9\-]+_[0-9_]+_[0-9]+\.pdf'`.split("\n")
  files_to_upload.each do |f_name|
    policy_id=f_name.match(/app_page_snapshot_[\d\-]+_[\d_]+_(\d+)\.pdf/)[1] rescue nil
    policy_id=policy_id.to_i
    unless policy_id>0
      log.error "Could not extract a policy id from file name '#{f_name}'."
      next
    end
    consumer_id=Crm::Case.where(id:policy_id).pluck(:consumer_id).first
    unless consumer_id
      log.error "Could not find policy for file '#{f_name}'."
      next
    end
    f_path =Rails.root.join('tmp',f_name)
    attachment=Attachment.create person_id:consumer_id, person_type:'Consumer', case_id:policy_id, uploader:File.open(f_path)
    if attachment.persisted?
      log.info "Saved '#{f_name}', with id #{attachment.id}."
      File.delete(f_path)
      log.info "Removed temp file."
    else
      log.error "Failed to upload and save '#{f_path}'.\n#{attachment.errors.full_messages.join('. ')}"
    end
  end
end