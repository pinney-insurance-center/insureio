# This is a one-shot job. This file can be removed from source control in the
# future if desired.

# This job's task is to migrate Tag data in the database according to the
# migration 20200204014001_merge_tag_key_and_value.rb

%q{
  To test this job:

  1. dump the prod db
  2. import the dump to a new db
    nohup mysql clone_dr_prod < dr-dump-2020-02-04.sql &
  3. open SSH tunnel to db server
    ssh -N -L $LOCALPORT:127.0.0.1:$REMOTEPORT drdb
  4. rake migration
    tic; RAILS_ENV=second bundle exec rake db:migrate tmp:tags:migrate; toc
}

TAGS_TABLE_FPATH = '/tmp/tags.txt'
TICTOC_STORE = {}

def tictoc title, opts={}, &block
  if block.nil?
    stats = TICTOC_STORE.delete title
  else
    stats = Benchmark.measure(&block)
  end
  if opts[:save]
    TICTOC_STORE[title] = stats + TICTOC_STORE.fetch(title, 0)
  else
    puts "%16s %s" % [title, stats]
  end
end

class TagUpdater
  DEPRECATED_TAG_NAMES = Set.new(%w[lead_type referrer source]).freeze
  SELECT_LIMIT = 10000

  def initialize model, *additional_fields
    @row_id = 0
    @model = model
    @table = model.table_name
    @selects = %w[id tenant_id tags] + [model::OWNER_ID_COLUMN] + DEPRECATED_TAG_NAMES.to_a + additional_fields
  end

  def run
    while true
      tictoc 'select' do
        @rows = connection.execute %Q{SELECT #{@selects.join(',')} FROM #{@table}
                WHERE id > #{@row_id}
                AND tags IS NOT NULL
                AND tags != '--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n'
                AND tags != '--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess {}\n'
                ORDER BY id LIMIT #{SELECT_LIMIT}}
      end
      break if @rows.size == 0
      tictoc 'iteration' do
        tictoc('start xact') { connection.begin_transaction }
        tictoc('rows') { @rows.each { |row| process_row row } }
        tictoc 'update person'
        tictoc 'insert tags'
        tictoc('commit xact') { connection.commit_transaction }
      end
      puts '-' * 64
    end
  end

  private

  def connection
    ActiveRecord::Base.connection
  end

  def process_row row
    @row_id, tenant_id, tags_yaml, owner_id, *_ = row
    row_hash = Hash[@selects.zip row]
    tags_set = TagSet.new YAML.load(tags_yaml)
    tags_set_start_size = tags_set.size

    setters = [] # Set column-value pairs on person record for SQL command

    # If any tags are deprecated, remove them from this set. If their
    # corresponding tracking field is blank on the person record, set the
    # tracking field.
    tags_set.delete_if do |text|
      key_val_match = /^([^-=]*)([-=](.+))?/.match text
      key, _unused, val = key_val_match.captures
      if key.blank?
        val.blank? # Delete tag if both key and value are blank
      elsif DEPRECATED_TAG_NAMES.include?(key)
        # Copy value to a tracking column (e.g. lead_type, source, referrer)
        setters << "`#{key}`=#{escape(val)}" if row_hash[key].blank? and val.present?
        true # Delete this tag
      elsif key == 'source_user' and ip_address_for_update?(val, row_hash)
        # Copy ip address from `source_user` tag to `ip_address`
        setters << "`ip_address`='#{val}'"
        false # Don't delete this tag
      end
    end

    # Prepare to update `tags` field if any tags have been removed
    if tags_set.empty?
      setters << "`tags`=NULL"
    elsif tags_set.size != tags_set_start_size
      setters << "`tags`=#{escape YAML.dump(tags_set.to_a)}"
    end

    # Update person record
    tictoc 'update person', save: true do
      if setters.present?
        connection.execute %Q{UPDATE #{@table} SET #{setters.join(',')} WHERE id = #{@row_id}}
      end
    end

    # Write tags insert statements to data file for active tags
    tictoc 'write tags', save: true do
      tags_set.each do |value|
        field_values = [value, @model.name, @row_id, tenant_id, owner_id]
        $tags_file.write "%s\n" % field_values.map { |v| v.presence || 'NULL' }.join("\t")
      end
    end
  end

  def escape input
    input.blank? ? 'NULL' : "'#{connection.raw_connection.escape(input)}'"
  end

  def ip_address_for_update? value, row_hash
    if row_hash.include?('ip_address') and row_hash['ip_address'].blank?
      IPAddr.new(value).present?
    end
  rescue IPAddr::InvalidAddressError, IPAddr::AddressFamilyError
    # Swallow this error. Just checking whether value is a valid IP address
  end
end

namespace :tmp do
  namespace :tags do
    desc "Convert `key-value` tags to `value` tags. Store tag information in the `tags` table in addition to on the person records User and Consumer."
    task :migrate => :environment do
      # If the tags table is not empty, then the db migration for rebuilding the
      # tags table was not immediately precedent
      if Tag.count == 0
        # Process person records
        $tags_file = File.open TAGS_TABLE_FPATH, 'w'
        tictoc('Update Consumers') { TagUpdater.new(Consumer, 'ip_address').run }
        tictoc('Updated Users') { TagUpdater.new(User).run }
        $tags_file.close

        # Insert tag data
        tictoc 'Insert into tags table' do
          ActiveRecord::Base.connection.execute %Q{LOAD DATA LOCAL
            INFILE '#{TAGS_TABLE_FPATH}'
            INTO TABLE tags
            (value, person_type, person_id, tenant_id, owner_id)}
          File.delete TAGS_TABLE_FPATH
        end
      end
    end
  end
end