# This is a one-shot job. This file can be removed from source control in the
# future if desired.

# This job's task is to migrate User API key data in the database according to the
# migration 20200514232032_encrypt_user_api_keys.rb

namespace :tmp do
  namespace :api_keys do
    desc "Use `attr_encrypted` gem to populate `encrypted_api_key` and to clear `old_plaintext_api_key` for all enabled users."
    task :encrypt => :environment do
      cryptor=UserApiKeyEncryptor.new true
    end
    desc "The above, but in reverse."
    task :decrypt => :environment do
      cryptor=UserApiKeyEncryptor.new false
    end
  end
end

class UserApiKeyEncryptor
  load 'config/initializers/audit_logger.rb'
  include ActiveModel
  include AuditLogger::Helpers

  def initialize encrypting=true
    @encrypting=encrypting
    @source_column= @encrypting ? 'old_plaintext_api_key' : 'encrypted_api_key'
    @dest_column  = @encrypting ? 'encrypted_api_key' : 'old_plaintext_api_key'
    @operation    = @encrypting ? :encrypt : :decrypt
    self.class.audit_logger_name "user_api_key_#{@operation.to_s}"
    @count=relevant_users.count
    if @count==0
      msg= "It is now safe to remove column old_plaintext_api_key from the users table.\nThere is nothing left for this script to do."
      puts msg
      audit msg
      return
    end
    @ids=relevant_users.pluck(:id)
    start
  end

  def start
    if @count==0
      msg= "Column `#{@source_column}` is empty for all users. Nothing to #{@operation.to_s}."
      puts msg
      audit msg
      return
    end
    @ids.in_groups_of(100) do |g|
      g.compact!
      users=User.where(id:g).select([:id, :old_plaintext_api_key, :encrypted_api_key, :encrypted_api_key_iv])
      users.each do |u|
        begin
          source_value=u.send :api_key#tmp accessor method on the User model will return decrypted or plaintext api key.
          u.assign_attributes old_plaintext_api_key:nil, encrypted_api_key:nil
          if @operation==:encrypt
            u.api_key= source_value#this invokes `attr_encrypted` gem's encryption logic
          end
          connection.execute(%Q(
            UPDATE users
            SET    #{@source_column}=null,
                   #{@dest_column}='#{u.encrypted_api_key}',
                   encrypted_api_key_iv='#{u.encrypted_api_key_iv}'
            WHERE  id=#{u.id}
          ))
        rescue =>ex
          audit level:'error', body:{id:u&.id,error_class:ex.class.name,msg:ex.message,backtrace:ex.backtrace}
        end
      end
    end
  end

  def connection
    ActiveRecord::Base.connection
  end

  def relevant_users
    User.where(active_or_enabled:true,is_recruit:false).
         where("#{@dest_column} IS NULL AND #{@source_column} IS NOT NULL")
  end

end
