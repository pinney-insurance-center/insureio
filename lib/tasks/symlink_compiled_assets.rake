
namespace :assets do
  desc "Create symlinks so that compiled assets can be accessed with or without the digests."
  task :symlink_compiled_assets => ['environment'] do
    name_and_extension_list=[
      ['login',         'js'],
      ['signup',        'js'],
      ['quoting_widget','js'],
      ['rtt_quote_app', 'js'],
      ['dependencies',  'js'],
      ['application',   'js'],
      ['dependencies',  'css'],
      ['application',   'css'],
    ]

    shared_path||="#{Rails.root}/public"
    assets_dir = File.join shared_path, Rails.application.config.assets.prefix

    name_and_extension_list.each do |asset_name, extension|
      fname_no_digest = "#{asset_name}.#{extension}"
      fpath_no_digest = File.join assets_dir, fname_no_digest
      fname_w_digest = Rails.application.assets.find_asset(fname_no_digest).digest_path
      fpath_w_digest = File.join assets_dir, fname_w_digest
      if File.exists? fpath_w_digest
        puts "Found file #{fname_w_digest.ljust(55,' ')}. Symlinking as #{fname_no_digest}."
        FileUtils.ln_s fpath_w_digest, fpath_no_digest, force: true
        if File.readlink(fpath_no_digest) != fpath_w_digest
          raise RuntimeError.new("Failed to make symlink for #{fname_w_digest}")
        end
      else
        $stderr.puts "Could not find compiled file '#{fname_w_digest}'.\nNo symlink created."
      end
    end
  end

  task :precompile_and_symlink => ['assets:precompile', 'assets:symlink_compiled_assets']
end
