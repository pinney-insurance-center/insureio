# This is a one-shot job. This file can be removed from source control in the
# future if desired.

# This job's task is to populate tenant_id data in the database according to the
# migration 20200629163926_add_tenant_id_to_tasks.rb

namespace :tmp do
  desc "Populate tenant_id column for existing, active tasks which lack one currently."
  task :task_tenant_ids => :environment do
    TaskTenantPopulator.new
  end
end

class TaskTenantPopulator
  load 'config/initializers/audit_logger.rb'
  include ActiveModel
  include AuditLogger::Helpers

  def initialize encrypting=true
    u_ts_to_update=Task.where(active:true,tenant_id:nil,person_type:'User'    ).select([:id,:person_id]).as_json
    c_ts_to_update=Task.where(active:true,tenant_id:nil,person_type:'Consumer').select([:id,:person_id]).as_json
    c_ts_to_update.in_groups_of(50) do |g|
      g.compact!
      p_ids=g.map{|t| t.person_id }
      #t_ids=g.map{|t| t.id }
      cs=Consumer.where(id: p_ids ).select([:id,:tenant_id]).map{|c| [c.id, c.tenant_id] }
      g.each do |t|
        tenant_id=cs.find{|c| c[0]==t[:id] }[1]
        Task.find(t[:id]).update_column(:tenant_id, tenant_id )
      end
    end
    u_ts_to_update.in_groups_of(50) do |g|
      g.compact!
      us=User.where(id: g.map{|t| t.person_id } ).select([:id,:tenant_id]).map{|u| [u.id,u.tenant_id] }
      g.each do |t|
        tenant_id=us.find{|u| u[0]==t[:id] }[1]
        Task.find(t[:id]).update_column(:tenant_id, tenant_id )
      end
    end
  end
end