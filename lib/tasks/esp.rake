namespace :esp do
  desc "Pull XML documents from SBLI FTP server and update our records"
  task :update => :environment do
    SyncedRunner.new('ESP_UPDATE') do
      begin
        Esp::Updater.new.run
      rescue => ex
        ExceptionNotifier.notify_exception ex
      end
    end
  end
end
