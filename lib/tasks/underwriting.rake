task :underwriting => [ 'underwriting:html', 'underwriting:spec' ]

namespace :underwriting do
  def str_to_title str
    str.to_s.underscore.humanize.split.map(&:capitalize).join(' ')
  end

  def klass_to_title klass, label
    if KLASS_TO_TITLE_MAP.has_key? label
      KLASS_TO_TITLE_MAP[label]
    elsif klass <= Crm::HealthInfo::OtherDisease
      str_to_title label
    else
      str_to_title klass.name.demodulize
    end
  end
end

KLASS_TO_TITLE_MAP = {
  copd: "COPD",
  crohns: "Crohn's Disease",
  diabetes_1: "Type I Diabetes (Juvenile)",
  diabetes_2: "Type II Diabetes",
}.with_indifferent_access.freeze
