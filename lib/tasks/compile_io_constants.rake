namespace :spec do
  namespace :js do
    desc "Build code to support protractor tests of global front end constants"
    task :compile_io_constants => :environment do
      constants_js_erb = File.read(File.join(__dir__, '../../app/assets/javascripts/application/constants.js.erb'))
      @ignore_deprecation_warning = true
      constants_js = ERB.new(constants_js_erb, nil, '-').result binding
      File.write('spec/javascripts/helpers/io_constants.js', constants_js)
    end
  end
end