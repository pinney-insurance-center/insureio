desc "Use this environment's data to update certain pages on academy.insureio.com"
task :academy => ['academy:carriers', 'academy:enums']

# Write a local tempfile containing the new post body, send it to the
# Wordpress server, run a command on the server to update the db.
# @arg `post_id`: the post in the Wordpress db
def update_wp_post post_id, html
  success = false
  content_file = Tempfile.create '', nil
  content_file.write html
  content_file.close
  # Send tempfile to Wordpress server
  remote_path = "/tmp/#{File.basename content_file.path}"
  if system "scp -C #{content_file.path} deploy@academy:#{remote_path}"
    # Use Wordpress app to update db
    IO.popen(['ssh', '-o BatchMode=yes', '-T', 'academy'], 'w') do |ssh|
      ssh.puts <<~EOF
        cd /var/www/wordpress/io_academy
        cat <<-EOSHELL | php -a
          require_once('wp-config.php');
          require_once('wp-includes/load.php');
          \\$post_content = file_get_contents('#{remote_path}');
          \\$wpdb->query( \\$wpdb->prepare("UPDATE \\$wpdb->posts SET post_content = %s WHERE ID = %d", \\$post_content, #{post_id}) );
        EOSHELL
        rm #{remote_path}
      EOF
      ssh.close
      success = $?.success?
    end
  end
  File.unlink content_file
  puts success ? 'OK' : 'FAILURE'
end

namespace :academy do
  task :carriers => :environment do
    carriers = Carrier.order(:id)
    erb = File.read(File.join(__dir__, 'academy/carriers.html.erb'))
    html = ERB.new(erb, nil, '-').result binding
    update_wp_post 423, html
  end

  task :enums => :environment do
    CARRIERS_HTML = <<~EOF
      <p>Please see the table on the <a href="/carrier-names">Carriers Page</a></p>
    EOF
    TOBACCO_HTML = <<~EOF
      <table>
        <thead><tr><th>ID</th><th>Name</th></tr></thead>
        <tbody>
          <tr><td>true</td><td>smoker</td></tr>
          <tr><td style="padding:0 10px 0 0;">false</td><td>non-smoker</td></tr>
        </tbody>
      </table>
      <p style="text-align: center;"><i>Note: "Smoker" will also work in place of "Tobacco," but is deprecated as of 9/10/15.</i></p>
    EOF
    DEFAULT_FIELDS = ['ID', 'Name']
    ENUMS = [
      [Enum::AddressType, 'Address Type',],
      [Enum::EntityType, 'Entity/Genre Type',],
      [Enum::RelationshipType, 'Relationship Type',],
      [Enum::Citizenship, 'Citizenship',],
      [Enum::Country, 'Birth Country & Foreign Travel ID',],
      [Enum::ContactMethod, 'Contact Method',],
      [Enum::NoteType, 'Note Type', note: 'Note: This is for reference only. Users should not attempt to set note type.'],
      [Enum::StatusTypeCategory, 'Policy Openness',],
      [Enum::ProductType, 'Policy Type', fields: [*DEFAULT_FIELDS, :marketech_code, :sort_order, 'Active']],
      [Enum::Purpose, 'Purpose', fields: [*DEFAULT_FIELDS, :type_id]],
      [Enum::SalesStage, 'Sales Stage',],
      [Enum::TaskType, 'Task Type',],
      [Enum::MaritalStatus, 'Marital Status',],
      [Enum::Ownership, 'Ownership',],
      [Enum::PhoneType, 'Phone Type',],
      [Enum::PlanningCat, 'Planning Category',],
      [Enum::ProductTypeCat, 'Product Category',],
      [Enum::LtcBenefitPeriodOption, 'Quoting: LTC Benefit Period Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [Enum::LtcHealthClassOption, 'Quoting: LTC Health Class Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [Enum::LtcInflationProtectionOption, 'Quoting: LTC Inflation Protection Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [Enum::PremiumModeOption, 'Quoting: Premium Mode Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [Enum::SpiaIncomeOption, 'Quoting: SPIA Income Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [Enum::TliDurationOption, 'Quoting: Term Life Insurance (TLI) Duration Options', fields: [*DEFAULT_FIELDS, :compulife_code, :name_for_tli_widget]],
      [Enum::TliHealthClassOption, 'Quoting: Term Life Insurance (TLI) Health Class Option', fields: [*DEFAULT_FIELDS, 'Value']],
      [nil, 'Tobacco', html: TOBACCO_HTML],
      [Enum::State, 'State', fields: %w[ID Abbrev compulife_code Name]],
      [Enum::TagType, 'Tag Type',],
      [Enum::CommissionLevel, 'Commission Level',],
      [Enum::ContractStatus, 'Contract Status',],
      [Enum::LicenseStatus, 'License Status',],
      [Enum::UsageRole, 'Role',],
      [nil, 'Carrier Names', html: CARRIERS_HTML],
      [Enum::DiseaseType, 'Disease Type',],
    ]
    build_anchor_name = lambda { |label| label.gsub(/\W/, '') }
    erb = File.read(File.join(__dir__, 'academy/enums.html.erb'))
    html = ERB.new(erb, nil, '-').result binding
    update_wp_post 331, html
  end
end
