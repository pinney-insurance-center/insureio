# Stripe is a credit card payment service.

require 'stripe'

namespace :stripe do

  # To interface with Stripe, our application needs to refer to payment plans
  # that have been created on the Stripe application. This task creates those
  # plans.

  desc "Create 4 payment plans on the Stripe system"
  task :create_plans => [:environment] do
    Stripe::Plan.create(
      :amount                => 599,
      :interval              => 'month',
      :name                  => 'Dataraptor BASIC',
      :currency              => 'usd',
      :id                    => 'DR_BASIC',
      :statement_description => "Dataraptor 1"
    )
    Stripe::Plan.create(
      :amount                => 4999,
      :interval              => 'month',
      :name                  => 'Dataraptor PROFESSIONAL',
      :currency              => 'usd',
      :id                    => 'DR_PROFESSIONAL',
      :statement_description => "Dataraptor 2"
    )
    Stripe::Plan.create(
      :amount                => 7999,
      :interval              => 'month',
      :name                  => 'Dataraptor WEB PRO',
      :currency              => 'usd',
      :id                    => 'DR_WEB_PRO',
      :statement_description => "Dataraptor 3"
    )
    Stripe::Plan.create(
      :amount                => 12999,
      :interval              => 'month',
      :name                  => 'Dataraptor WEB PRO PLUS',
      :currency              => 'usd',
      :id                    => 'DR_WEB_PRO_PLUS',
      :statement_description => "Dataraptor 4"
    )
  end

end
