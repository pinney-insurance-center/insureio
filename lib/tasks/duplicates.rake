namespace :duplicates do
	namespace :lead_distribution do
		desc "Remove duplicate LeadDistribution UserRules"
		task :remove => :environment do
			ids_to_destroy = [].to_set
			puts "Finding Duplicates...(this may take a while)"
			user_rules = LeadDistribution::UserRule.find(:all, :order => "user_id ASC")
			user_rules.each do |rule|
				dupes = LeadDistribution::UserRule.where(:user_id => rule.user_id, :tag_value_id => rule.tag_value_id).order(:id)
				dupes.pop
				dupes.each { |dupe| ids_to_destroy.add(dupe.id) }
			end
			puts "Destroying ids: #{ids_to_destroy.to_a}"
			LeadDistribution::UserRule.delete_all(:id => ids_to_destroy.to_a) if ids_to_destroy.count > 0
			puts "Done"
		end
	end
end