namespace :lead_distribution_counts do
  desc "Reset all counts to zero"
  task :reset => :environment do
    LeadDistribution::UserRule.update_all(count:0)
  end
end
