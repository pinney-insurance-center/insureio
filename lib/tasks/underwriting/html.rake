module Rake::Underwriting
  module Html
    class DiseaseWrapper
      attr_accessor :is_relatives
      attr_reader :klass, :label, :title, :ng_show_model, :id, :type_id
      def initialize(klass, label)
        @klass = klass
        @label = label
        @title = klass_to_title(klass, label)
        @ng_show_model = "health.#{label}_true"
        @ng_show_type_id = "disease.type_id == #{Enum::DiseaseType.id(label)}"
        @id = "health-{{$id}}-#{label}" # append `-div` or `-checkbox`
        @type_id = Enum::DiseaseType.id(label)
      end

      # Return a tuple for all fields on this disease, used for rendering ERB
      def fields
        klass.fields.select { |field|
          # Skip fields that should only be filled for relation_type or should never be filled manually
          not [:true, :relationship_type_id, :onset_age, :death_age].include?(field[0])
        }
        .map { |field|
          fname, ftype, _comment = field
          field_title = fname == :date ? 'Date of Diagnosis' : str_to_title(fname)
          ng_model = is_relatives ? "disease.#{fname}" : "health.#{label}_#{fname}"
          fid = "#{label}-{{$id}}-#{fname}"
          [fname, ftype, field_title, fid, ng_model]
        }
      end

      # Return whether XRAE considers this disease as part of Personal History
      def xrae?
        Quoting::Xrae::PERSONAL_DISEASE_IDS.include? @type_id
      end

      def ng_show
        is_relatives ? @ng_show_type_id : "#{@ng_show_model} || #{@ng_show_type_id}"
      end
    end

    CATEGORIES = {
      'Diabetes': [ :diabetes_1, :diabetes_2 ],
      'Mental / Nervous Disorders': [
        :anxiety,
        :depression,
        :epilepsy,
        :multiple_sclerosis,
        :parkinsons,
      ],
      'Respiratory': [ :asthma, :copd, :emphysema, :sleep_apnea ],
      'Gastro-Intestinal': [ :crohns, :ulcerative_colitis_iletis ],
      'Cancer': [
        :breast_cancer,
        :basal_cell_carcinoma,
        :colon_cancer,
        :intestinal_cancer,
        :malignant_melanoma,
        :other_internal_cancer,
        :other_skin_cancer,
        :ovarian_cancer,
        :prostate_cancer,
      ],
      'Cardiovascular / Cerebral Vascular': [
        :atrial_fibrillation,
        :cardiovascular_disease,
        :cardiovascular_impairments,
        :cerebrovascular_disease,
        :coronary_artery_disease,
        :elevated_liver_function,
        :heart_attack,
        :heart_murmur,
        :irregular_heartbeat,
        :other_heart_condition,
        :stroke,
        :vascular_disease,
      ],
      'Other Medical Condtions': [
        :arthritis,
        :alcohol_abuse,
        :drug_abuse,
        :hepatitis_c,
        :kidney_disease,
        :mental_illness,
        :weight_reduction,
      ],
    }
    ORDERED_CATEGORIES = [ 'Cancer', 'Cardiovascular / Cerebral Vascular', 'Diabetes', 'Gastro-Intestinal', 'Respiratory', 'Mental / Nervous Disorders', 'Other Medical Condtions'].map(&:to_sym)
  end
end

namespace :underwriting do

  desc "Build html for forms for underwriting (sub)page"
  task :html => ['html:diseases']

  namespace :html do
    desc "Build html for disease-related fields"
    task :diseases => :environment do
      # Prepare output dirs
      FileUtils.mkdir_p 'public/quoter'
      FileUtils.mkdir_p 'public/health'

      # Prepare field values to set and check
      diseases = Crm::HealthInfo::DISEASES_MAP.map do |label, klass|
        wrapper = Rake::Underwriting::Html::DiseaseWrapper.new(klass, label)
        [klass, label, wrapper]
      end

      # Prepare file for Disease details
      erb = File.read(File.join(__dir__, 'erb/disease_fields.html.erb'))
      html = ERB.new(erb, nil, '-').result binding
      File.write('public/quoter/disease_fields.html', html)

      # Prepare file for RelativesDiseases details
      diseases.each { |_k, _l, wrapper| wrapper.is_relatives = true }
      erb = File.read(File.join(__dir__, 'erb/disease_fields.html.erb'))
      html = ERB.new(erb, nil, '-').result binding
      File.write('public/quoter/disease_fields_merged.html', html)

      # Prepare file for Disease checkboxes
      diseases_map = diseases.map { |klass, label, wrapper| [label, wrapper] }.to_h
      erb = File.read(File.join(__dir__, 'erb/disease_checkboxes.html.erb'))
      html = ERB.new(erb, nil, '-').result binding
      File.write('public/health/disease_checkboxes.html', html)
    end
  end
end
