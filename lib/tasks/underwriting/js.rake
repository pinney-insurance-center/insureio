module Rake::Underwriting
  module Js
    # This model wraps a health condition for the purpose of rendering ERB
    # into protractor specs.
    class ConditionWrapper
      # Generate an instance of `klass`, setting values for all fields. This
      # instance will be used to generate protractor code to set and verify
      # values on inputs in html pages.
      def self.rand_instance klass
        inst = klass.new
        klass.fields.each do |field|
          fname, ftype, _comment = field
          next if [:true, :type_id, :relationship_type_id].include?(fname)
          inst.send "#{fname}=",
          case ftype
          when :boolean
             true
          when :date
            Date.new(1971,1,1) + rand(1000)
          when :integer
            rand 10000
          when :float
            rand * 100
          when :string
            SecureRandom.base64[0...8]
          else
            raise ArgumentError.new "Unexpected field type #{ftype} for #{klass.name}.#{fname}"
          end
        end
        inst
      end

      attr_reader :klass, :prefix

      def initialize(klass, prefix)
        @klass = klass
        @prefix = prefix # whatever appears before the period (.) in ng-model
        @instance = self.class.rand_instance(klass)
        @fields_map = Hash[klass.fields.map { |item| item.slice(0,2) }]
      end

      # Use the stored value of field `fname` to generate protractor code for
      # an expectation on the value of a field.
      def check container, fname, opts={}
        selector = selector container, fname, opts
        ftype = @fields_map[fname]
        if [:type_id, :relationship_type_id].include? fname
          %Q{expect(#{selector}.getAttribute('value')).toEqual('number:#{value(fname)}');}
        elsif :boolean == ftype
          %Q{expect(#{selector}.getAttribute('checked')).toBeTruthy();}
        else
          %Q{expect(#{selector}.getAttribute('value')).toEqual('#{value(fname)}');}
        end
      end

      # Use the stored value of field `fname` to generate protractor code to
      # set an input in an html page.
      def set container, fname, opts={}
        value = self.value(fname)
        selector = selector container, fname, opts
        selector = %Q{scroll(#{selector})}
        ftype = @fields_map[fname]
        if [:type_id, :relationship_type_id].include? fname
          %Q{#{selector}.$('[value="number:#{value}"]').click();}
        elsif :boolean == ftype
          %Q{#{selector}.click();}
        else
          %Q{#{selector}.sendKeys('#{value}');}
        end
      end

      # Get the value of a field `fname` from this disease
      def value fname
        @instance.send(fname)
      end

      protected

      # Generate protractor code for a selector for field `fname`
      def selector container, fname, opts
        ftype = @fields_map[fname]
        suffix = opts.fetch :suffix, fname
        ng_model = "#{prefix}.#{suffix}"
        case ftype
        when :boolean
          %Q{#{container}.element(by.css('[id$="-#{fname}-1"]'))}
        when :date
          %Q{#{container}.element(by.css('[id$="-#{suffix}"]'))}
        when :integer
          %Q{#{container}.element(by.model('#{ng_model}'))}
        when :float
          %Q{#{container}.element(by.model('#{ng_model}'))}
        when :string
          %Q{#{container}.element(by.model('#{ng_model}'))}
        else
          raise ArgumentError.new "Unexpected field type #{ftype} for #{klass.name}.#{fname}"
        end
      end
    end

    # This model wraps a health condition for the purpose of rendering ERB
    # into protractor specs.
    class DiseaseWrapper < ConditionWrapper
      attr_reader :label, :title

      def initialize(klass, label)
        super klass, 'disease'
        @label = label
        @title = klass_to_title(klass, label).sub(/'/, '\\\\\'')
        @instance.type_id = Enum::DiseaseType.id(label)
        @instance.relationship_type_id = Enum::RelationshipType.all.sample.id
      end

      # Return whether XRAE considers this disease as part of Personal History
      def xrae?
        Quoting::Xrae::PERSONAL_DISEASE_IDS.include? @instance.type_id
      end

      protected

      def selector container, fname, opts
        if opts[:relatives]
          opts[:suffix] = fname
          @prefix = :disease
        else
          opts[:suffix] = "#{label}_#{fname}"
          @prefix = :health
        end
        super container, fname, opts
      end
    end
  end
end

namespace :underwriting do

  desc "Build js code to support tests of Underwriting (sub)page"
  task :spec => ['spec:protractor']

  namespace :spec do
    desc "Build code to support protractor tests of Underwriting (sub)page"
    task :protractor => :environment do
      # Prepare field values to set and check
      diseases = Crm::HealthInfo::DISEASES_MAP.map do |label, klass|
        [klass, label, Rake::Underwriting::Js::DiseaseWrapper.new(klass, label)]
      end

      # Choose a few diseases to test as RelativesDiseases
      relatives_diseases = Quoting::Xrae::RELATIVES_DISEASE_IDS.to_a.sample(3).map do |type_id|
        disease_type = Enum::DiseaseType.find(type_id)
        klass = disease_type.klass ? "Crm::HealthInfo::#{disease_type.klass}".constantize : Health::Disease
        label = disease_type.name
        [klass, label, Rake::Underwriting::Js::DiseaseWrapper.new(klass, label)]
      end
      # Add a disease of the same type and values as a disease already in the
      # Array. Duplicates should be accepted.
      relatives_diseases << relatives_diseases.first

      # Prepare some MovingViolations
      moving_violations = (0..2).map { Rake::Underwriting::Js::ConditionWrapper.new(Crm::HealthInfo::MovingViolation, 'violation') }

      # Prepare some Crimes
      crimes = (0..2).map { Rake::Underwriting::Js::ConditionWrapper.new(Crm::HealthInfo::Crime, 'crime') }

      # Prepare file for filling underwriting on quote path
      erb = File.read(File.join(__dir__, 'erb/health_info.js.erb'))
      js = ERB.new(erb, nil, '-').result binding
      File.write('spec/javascripts/helpers/health-info.js', js)
    end
  end
end
