class ActiveRecordLogger < Logger
  FORMATTER = proc do |severity, time, progname, msg|
    timestr = time.strftime("%Y-%m-%dT%H:%M:%S.") << "%06d " % time.usec
    "\n#{timestr} #{msg}"
  end

  def initialize *args
    super
    @formatter = FORMATTER
  end

private
  def format_message(severity, datetime, progname, msg)
    if msg =~ /INSERT|UPDATE|ALTER|DROP/
      @formatter.call(severity, datetime, progname, msg)
    end
  end
end
