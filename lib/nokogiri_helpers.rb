# Add a convenience method to Nokogiri::XML::Builder
class Nokogiri::XML::Builder
	# Only adds a value to xml if value not nil
	def add sym, value
		self.send(sym, value) unless value.nil?
	end
end

# Add convenience method to Nokogiri::XML::Node
class Nokogiri::XML::Node
	# Grab value of first child node of given name
	def fetch(*nodes)
		plain_fetch(".//#{nodes.map{|node| "xmlns:#{node}" }.join('/')}")
	end
	# Grab value of <Selection> sibling of <Option> of given @Type
	def fetch_option(option_type)
		value = plain_fetch(".//xmlns:Option[@Type=\"#{option_type}\"]/xmlns:Selection/text()")
		['No', 'None'].include?(value) ? nil : value
	end
	# Grab inner text of node at path unless path returns no node
	def plain_fetch(xpath)
		node = at_xpath(xpath)
		node.inner_text unless node.nil?
	end
end