module Health
  class Detail
    include ActiveModel::Conversion
    include ActiveModel::Validations
    include ActiveModel::Dirty
    extend  ActiveModel::Naming

    validate :field_values_are_correct_types

    attr_accessor :changed_attributes

    def initialize json=nil
      if json.present?
        json = JSON.parse(json) if json.is_a?(String)
        json = json.to_h.with_indifferent_access
        # Apply JSON data to self
        field_names = Set.new(self.class.fields.map(&:first).map(&:to_s))
        json.select do |k, v|
          next true if field_names.include?(k)
          next false unless respond_to?("#{k}=")
          meth = self.class.instance_method("#{k}=")
          orig = meth.original_name.to_s.sub(/=$/, '')
          orig != k && field_names.include?(orig) # k is aliased to orig
        end.each do |k, v|
          self.send("#{k}=", v, true) if v.present? || v == false
        end
      end
      self.changed_attributes||={}
    end

    def self.load(json)
      self.new(json)
    end

    def self.dump(obj)
      # Make sure the type is right.
      unless obj.blank? || obj.is_a?(self)
        raise ::TypeError, "Expected #{self}, got #{obj.class}"
        return nil
      end
      allowed_field_names=self.fields.map(&:first).map(&:to_s)
      hash=obj.as_json.slice(*allowed_field_names).reject{|k,v| v.nil? }
      some_keys_indicate_condition=hash.any? do |k,v|
        (v.is_a?(Fixnum) && v>0) ||
        (v.is_a?(Date) && (!self.name.match(/Tobacco/) || v>5.years.ago.to_date) ) ||#only tobacco drops off after 5 years
        (v.is_a?(String) && v.present?) ||
        (v==true)
      end
      if hash.present? && some_keys_indicate_condition
        hash.to_json
      else
        nil
      end
    end

    def as_json opts={}
      exceptions = ['changed_attributes', 'errors', 'validation_context']
      exceptions.push('true') if truthy?(true)
      # The `super` function is the not the usual `as_json`. It ignores
      # `opts`, so handle the exceptions manually here.
      super.except(exceptions).reject { |k,v| v.blank? && v != false }
    end

    def truthy? ignore_true_field = false
      self.class.fields.any? do |field|
        field_indicates_truthy?(field.first, ignore_true_field)
      end
    end

    #This accessor method is meant to be overridden by inheriting classes.
    #We define a blacklist instead of a whitelist because our field lists match theirs almost 100%.
    def xrae_blacklisted_keys
      []
    end

    def self.attr field_name, field_type, *other_args
      @fields ||= Set.new
      @fields.add [field_name, field_type, *other_args]
      attr_reader field_name
      define_attribute_methods field_name
      #This is a partial implementation of `ActiveModel::Dirty` methods.
      #The point is to set `changed` and `changes` so that changes get persisted. No effort is made to reset these after save.
      define_method "#{field_name}=" do |new_value, bypass_recording_changes=false|
        old_value=self.instance_variable_get("@#{field_name}")
        cast_new_value=verify_or_cast_value_to_correct_type new_value, field_name, field_type
        unless bypass_recording_changes || cast_new_value==old_value
          begin
            self.send "#{field_name}_will_change!"
          rescue =>ex
            msg="Unable to set key `#{field_name}_will_change!`.\n"
            msg+="This error does not directly prevent save of the health info record, but may indicate invalid data or flawed back end logic. Please investigate."
            ExceptionNotifier.notify_exception ex, data: msg
          end
        end
        self.instance_variable_set "@#{field_name}", cast_new_value
      end
    end

    def self.inherited base
      klass = base.superclass
      while klass <= self
        klass.fields.each { |array| base.attr *array }
        klass = klass.superclass
      end
    end

    def self.fields; @fields; end

    attr :detail, :string

    private

    def field_indicates_truthy? field, ignore_true_field
      # Many Detail subclasses have a field :true which is used to indicate
      # that a particular health condition exists but that no detail is
      # available.
      return false if ignore_true_field && field.to_sym == :true
      value = send field
      return true if value.is_a?(Fixnum) && value > 0
      return true if value.is_a?(Date) && (self.class != Crm::HealthInfo::Tobacco || value > 5.years.ago.to_date) # only tobacco drops off after 5 years
      return true if value.is_a?(String) && value.present?
      return value == true
    end

    def field_values_are_correct_types
      self.class.fields.each do |field_name, type|
        value=self.send(field_name)
        next if value.blank? && value!=false
        verify_or_cast_value_to_correct_type value, field_name, type
      end
    end

    def verify_or_cast_value_to_correct_type value, name, type
      if type==:date
        if value.is_a?(Date) || (value.to_date rescue nil)
          value=value.to_date
        else
          errors.add name, "must be a date"
        end
      elsif type==:boolean
        if(
            value.is_a?(TrueClass) ||
            value=='true' ||
            value=='1' ||
            value==1
          )
          value=true
        elsif(
            value.is_a?(FalseClass) ||
            value=='false' ||
            value=='0' ||
            value==0
          )
          value=false
        else
          errors.add name, "must be a boolean (true or false)"
        end
      elsif type==:string
        if value.is_a?(String)
          value
        else
          errors.add name, "must be a string (text)"
        end
      elsif type==:integer
        if(
            value.is_a?(Integer) ||
            ( value.is_a?(String) && value.match(/^(\d|,)+$/) && (value.to_i rescue nil) )
          )
          value=value.to_i
        else
          errors.add name, "must be an integer"
        end
      elsif type==:float
        if(
            value.is_a?(Float) || value.is_a?(Integer) ||
            ( value.is_a?(BigDecimal) && value.to_f ) ||
            ( value.is_a?(String) && value.match(/^(\d|,|.)+$/) && (value.to_f rescue nil) )
          )
          value=value.to_f
        else
          errors.add name, "must be a number"
        end
      end
      value
    end
  end
end
