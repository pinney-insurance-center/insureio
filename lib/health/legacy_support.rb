# As we get test coverage for the front-end, we can start deleting thes,
# which exist only to support legacy formats.
module Health
  module LegacySupport
    extend ActiveSupport::Concern

    included do
      #DEPRECATED
      # Override so that when Angular front end supplies empty Array and Rails munges it into nil, it won't create a problem. (Thanks, Rails)
      alias_method :set_family_diseases_attributes, :family_diseases_attributes=
      alias_attribute :disease_details, :diseases
      alias_attribute :relatives_disease_details, :relatives_diseases
      alias_attribute :hazardous_avocation_further_detail, :hazardous_avocation_detail

      #DEPRECATED
      #Going forward this should only be done on the front end.
      pseudo_date_accessor :bp_control_start,                            prev:'years_of_bp_control'
      pseudo_date_accessor :bp_last_treatment,                           prev:'years_since_bp_treatment'
      pseudo_date_accessor :cholesterol_control_start,                   prev:'years_of_cholesterol_control'
      pseudo_date_accessor :cholesterol_last_treatment,                  prev:'years_since_cholesterol_treatment'
      pseudo_date_accessor :tobacco_cigar_last,                          prev:'years_since_tobacco_cigar_last'
      pseudo_date_accessor :tobacco_cigarette_last,                      prev:'years_since_tobacco_cigarette_last'
      pseudo_date_accessor :tobacco_nicotine_patch_or_gum_last,          prev:'years_since_tobacco_nicotine_patch_or_gum_last'
      pseudo_date_accessor :tobacco_pipe_last,                           prev:'years_since_tobacco_pipe_last'
      pseudo_date_accessor :tobacco_chewed_last,                         prev:'years_since_tobacco_chewed_last'

      before_validation :add_dummy_moving_violations

      NON_DISEASE_HEALTH_CONDITIONS_MAP
      .reject { |label, klass| Crm::HealthInfo.serialized_attributes.include? label.pluralize }
      .each do |condition_name, klass|
        health_info_field = "#{condition_name}_details"
        serialize health_info_field, klass
      end

      # Metaprogramming to create boolean accessor for all SerializableDetail
      # types. This is done in the `included` block because it uses the
      # `after_initialize` function.
      HEALTH_CONDITIONS_MAP.each do |condition_name, klass|
        if klass < ArrayDetail
          define_method "#{condition_name}?" do
            self.send(condition_name.pluralize).any? &:truthy?
          end
        elsif klass < Disease
          define_method "#{condition_name}?" do
            diseases[condition_name].truthy?
          end
        elsif klass < Detail
          if columns_hash[condition_name.to_s]&.type == :boolean
            define_method "#{condition_name}?" do
              self[condition_name] ||= send("#{condition_name}_details").truthy?
            end
            # Set boolean column value if indicated (in case one col is out of sync with the other)
            after_initialize -> { self[condition_name] = true }, if: "#{condition_name}?"
          else
            define_method "#{condition_name}?" do
              self[condition_name]&.truthy
            end
          end
        else
          raise KeyError.new("#{klass.name} not found among Crm::HealthInfo::Detail subclasses")
        end
      end

      # Metaprogramming to support Disease aliases
      begin
        {
          cancer_breast: :breast_cancer,
          cancer_prostate: :prostate_cancer,
          cancer_skin: :other_skin_cancer,
          cancer_internal: :other_internal_cancer,
        }.each do |nick, real|
          public_instance_methods.each do |method_name|
            next unless method_name.to_s.starts_with? real.to_s
            method_nick = method_name.to_s.sub(real.to_s, nick.to_s)
            alias_method method_nick, method_name
          end
        end
      end
    end

    # Build HEALTH_CONDITIONS constant
    begin
      DISEASES_MAP = Hash[Enum::DiseaseType.map do |type|
        klass_name = type.klass || type.name.to_s.camelize
        klass = "Crm::HealthInfo::#{klass_name}".constantize
        [type.name, klass]
      end]
      NON_DISEASE_HEALTH_CONDITIONS_MAP = Hash[Crm::HealthInfo.constants.map do |symbol|
        klass = "Crm::HealthInfo::#{symbol}".constantize
        label = symbol.to_s.underscore.gsub(/(\D)(\d+)/, '\1_\2').gsub(/(\d+)(\D)/, '\1_\2')
        [label, klass]
      end.select do |label, klass|
        next false unless klass.is_a? Class
        next false unless klass < Detail
        next false if klass == Disease
        next false if klass < Disease
        true
      end]
      HEALTH_CONDITIONS_MAP = {}.merge(NON_DISEASE_HEALTH_CONDITIONS_MAP).merge(DISEASES_MAP)
      HEALTH_CONDITIONS = HEALTH_CONDITIONS_MAP.keys.map(&:to_sym)
      HEALTH_CONDITIONS.sort!.freeze
    end

    # Metaprogramming to create accessors for contents of
    # Diseases as if they were fields on Crm::HealthInfo
    DISEASES_MAP.each do |condition_name, _klass|
      # Define getter
      define_method condition_name do
        diseases[condition_name] || Crm::HealthInfo::Disease.for_name(condition_name)
      end
      alias_method "#{condition_name}_details", condition_name

      # Define setter
      define_method "#{condition_name}=" do |value|
        will_change 'diseases' do
          if value === true
            diseases[condition_name].true = true
          elsif !value
            diseases[condition_name] = nil
          else
            diseases[condition_name] = value
          end
        end
      end
      alias_method "#{condition_name}_details=", "#{condition_name}="
    end

    # Override setters so that Hash may be used as value and immediately cast to
    # the chosen serialized type
    NON_DISEASE_HEALTH_CONDITIONS_MAP
    .reject { |label, klass| Crm::HealthInfo.serialized_attributes.include? label.pluralize }
    .each do |label, klass|
      define_method "#{label}=" do |value|
        self["#{label}_details"] = klass.new unless value
        super value
      end

      define_method "#{label}_details=" do |value|
        self["#{label}_details".to_sym] = klass.new(value)
      end
    end


    DELEGATED_KEYS = [] # populated by the loop below.

    # Metaprogramming to create accessors for _fields_ on Detail
    # instances
    HEALTH_CONDITIONS_MAP.each do |condition_name, klass|
      next if klass < ArrayDetail

      klass.fields.map(&:first).each do |sub_field|
        if klass < Disease
          health_info_field = :diseases
        else
          health_info_field = "#{condition_name}_details"
        end
        prefixed_name = "#{condition_name}_#{sub_field}"
        DELEGATED_KEYS << prefixed_name

        # Define getter
        define_method prefixed_name do
          send("#{condition_name}_details").send sub_field
        end

        # Define setter
        changed_field = klass < Health::Disease ? :diseases : "#{condition_name}_details"
        define_method "#{prefixed_name}=" do |value|
          unless value == self[prefixed_name]
            will_change changed_field do
              send("#{condition_name}_details").send("#{sub_field}=", value)
            end
          end
        end

        # Add aliased accessors for disparate legacy names
        case sub_field
        when :date
          alias_method "#{condition_name}_diagnosed_date=", "#{condition_name}_date="
          klass.send :alias_method, :diagnosed_date=, :date=
        when :currently_treating
          alias_method "#{condition_name}_currently_treated=", "#{condition_name}_currently_treating="
          klass.send :alias_method, :currently_treated=, :currently_treating=
        when :last_treatment
          alias_method "#{condition_name}_last_treatment_date=", "#{condition_name}_last_treatment="
          klass.send :alias_method, :last_treatment_date=, :last_treatment=
          alias_method "#{condition_name}_treatment_end_date=", "#{condition_name}_last_treatment="
          klass.send :alias_method, :treatment_end_date=, :last_treatment=
        end
      end
    end

    ########################
    # Handle one-off legacy field names
    ########################
    alias_method :stroke_first_stroke_age=, :stroke_onset_age=
    Crm::HealthInfo::Stroke.send :alias_method, :first_stroke_age=, :onset_age=

    # This freeze is to take place after the metaprogramming code blocks.
    DELEGATED_KEYS.freeze

    #These aliased names are DEPRECATED.
    #For any new code, use the new names, which are uniformly prefixed by the condition name.
    #When we are sure all uses of them are gone (including by users of our lead import API), they should be removed.
    deprecated_aliases_map=[
      [:last_bp_treatment,             :bp_last_treatment],
      [:last_cholesterol_treatment,    :cholesterol_last_treatment],
      [:travel_when,                   :foreign_travel_when],
      [:travel_duration,               :foreign_travel_duration],
      [:travel_country_id,             :foreign_travel_country_id],
      [:travel_country_detail,         :foreign_travel_country_detail],
      [:travel_where,                  :foreign_travel_country_detail],
      [:last_dui_dwi,                  :moving_violation_last_dui_dwi],
      [:last_dl_suspension,            :moving_violation_last_dl_suspension],
      [:last_reckless_driving,         :moving_violation_last_reckless_driving],
      [:penultimate_car_accident,      :moving_violation_penultimate_car_accident],
      [:cigarettes_current,            :tobacco_cigarettes_current],
      [:last_cigarette,                :tobacco_cigarette_last],
      [:cigarettes_per_day,            :tobacco_cigarettes_per_day],
      [:cigars_current,                :tobacco_cigars_current],
      [:last_cigar,                    :tobacco_cigar_last],
      [:cigars_per_month,              :tobacco_cigars_per_month],
      [:nicotine_patch_or_gum_current, :tobacco_nicotine_patch_or_gum_current],
      [:last_nicotine_patch_or_gum,    :tobacco_nicotine_patch_or_gum_last],
      [:pipe_current,                  :tobacco_pipe_current],
      [:last_pipe,                     :tobacco_pipe_last],
      [:pipes_per_year,                :tobacco_pipes_per_year],
      [:last_tobacco_chewed,           :tobacco_chewed_last],
    ].freeze
    deprecated_aliases_map.each do |deprecated_name,preferred_name|
      define_method deprecated_name do
        self.send preferred_name
      end
      define_method "#{deprecated_name}=" do |value|
        SystemMailer.deprecated_method_warning(self) rescue nil
        self.send "#{preferred_name}=", value
      end
    end

    def clear_prior_family_diseases= value
      SystemMailer.deprecated_method_warning(self) rescue nil
    end

    def criminal= value
      will_change 'crimes' do
        if value.is_a?(String)
          value = JSON.load(value) rescue value
        end
        if value == true || value == 1
          # Only add a crime if there are no crimes present. Else, the current
          # function call contributes no additional data.
          crimes.push(Crm::HealthInfo::Crime.new(true: true)) if crimes.none?(&:truthy?)
        elsif not value
          crimes.clear
        else
          raise ArgumentError.new "Unexpected type for #{__method__}: #{value.class.name}"
        end
      end
    end

    def criminal_further_detail= detail
      crime = crimes.find do |c|
        # Look for an existing crime which has no field set but :true. If
        # there is one, there's no harm in assigning this detail to it.
        c.truthy? and not c.truthy?(true)
      end
      if crime
        crime.detail = detail
      else
        crimes.push(Crm::HealthInfo::Crime.new(detail: detail))
      end
    end

    #Going forward this should only be done on the front end.
    def family_diseases_attributes
      relatives_diseases
    end
    alias_method :family_diseases, :family_diseases_attributes

    #Going forward this should only be done on the front end.
    def family_diseases_attributes= fd_attr_array
      unless fd_attr_array.nil?
        data = []
        fd_attr_array.each do |relative|
          relative = relative.with_indifferent_access
          diseases, details = relative.partition { |k,v| v == true && k != 'parent' }.map(&:to_h)
          details['onset_age'] ||= details.delete 'age_of_contraction'
          details['death_age'] ||= details.delete 'age_of_death'
          details['date'] ||= details.delete 'diagnosed_date'
          is_parent = ActiveRecord::ConnectionAdapters::Column.new(nil, nil, :boolean).type_cast details.delete('parent')
          relation_id = Enum::RelationshipType.id(is_parent ? 'parent' : 'other')
          diseases.keys.each do |disease_name|
            disease_name = :diabetes_1 if disease_name.to_s == 'diabetes'
            type = Enum::DiseaseType.efind(name: disease_name.to_s)
            item = { type_id: type.id, relationship_type_id: relation_id }
            data << item.merge(details)
          end
        end
        self.relatives_diseases = data
      end
    end

    def family_disease_death_count
      family_diseases.select{|fd| fd.death_age.present? }.length
    end

    def moving_violation= value
      will_change 'moving_violations' do
        if value == true || value == 1
          # Only add a violation if there are none present. Else, the current
          # function call contributes no additional data.
          moving_violations.push(Crm::HealthInfo::MovingViolation.new(date: 5.years.ago)) if moving_violations.none?(&:truthy?)
        elsif not value
          moving_violations.clear
        else
          raise ArgumentError.new "Unexpected type for #{__method__}: #{value.class.name}"
        end
      end
    end

    def moving_violation_more_than_one?
      moving_violation_total > 1
    end

    def moving_violation_history_attributes= value
      will_change 'moving_violations' do
        self.attributes = value.map do |k, v|
          ["moving_violation_#{k}", v]
        end.to_h
      end
    end

    # This attribute will be used in the `before_validation` hook
    # `add_dummy_moving_violations`
    attr_accessor :moving_violation_last_6_mo
    attr_accessor :moving_violation_last_1_yr
    attr_accessor :moving_violation_last_2_yr
    attr_accessor :moving_violation_last_3_yr
    attr_accessor :moving_violation_last_5_yr

    def moving_violation_last_dl_suspension= date
      will_change 'moving_violations' do
        set_attrs_on_moving_violation_for_date date, dl_suspension: true
      end
    end

    def moving_violation_last_dui_dwi= date
      will_change 'moving_violations' do
        set_attrs_on_moving_violation_for_date date, dui_dwi: true
      end
    end

    def moving_violation_last_reckless_driving= date
      will_change 'moving_violations' do
        set_attrs_on_moving_violation_for_date date, reckless_driving: true
      end
    end

    def moving_violation_penultimate_car_accident= date
      will_change 'moving_violations' do
        set_attrs_on_moving_violation_for_date date, accident: true
      end
    end

    # Returns int total
    def moving_violation_total
      moving_violations.length
    end

    def disease_factors_incompatible_w_compulife
      ok_for_compulife=[
        :bp,
        :cholesterol,
        :foreign_travel,
        :moving_violation,
        :tobacco,
        :hazardous_avocation,
      ]
      factors=HEALTH_CONDITIONS-ok_for_compulife # todo
      list=[]
      factors.each do |hc|
        if self.send "#{hc}?"
          list << hc
        end
      end
      list
    end

    #DEPRECATED
    #This method is here only to accommodate API users who are sending values in [0,1] for 'smoker'.
    #Documentation should note that the preferred key to pass in going forward will be 'tobacco'.
    def smoker= value
      SystemMailer.deprecated_method_warning(self) rescue nil
      if value.is_a?(String)
        if value.match(/\A\d\z/)
          value=value.to_i
        elsif value.match(/\Atrue\z/)
          value=true
        else
          value=false
        end
      end
      value=value>0 if value.is_a?(Integer)

      self.tobacco=value
    end

    #DEPRECATED
    #Going forward this should only be done on the front end.
    def height
      SystemMailer.deprecated_method_warning(self) rescue nil
      str = ''
      str += "#{feet}'" if feet.to_i > 0
      str += "#{inches}\"" if inches.to_i > 0
      str
    end

    #DEPRECATED
    #Going forward this should only be done on the front end.
    def height=(val)
      SystemMailer.deprecated_method_warning(self) rescue nil
      feet_regx = /^[0-9]+\'/
      if feet_regx.match(val)
        feet_will_change!
        @feet = feet_regx.match(val).to_s.gsub(/\'/, '')
      end
      inches_regx = /[0-9]+\"$/
      if inches_regx.match(val)
        inches_will_change!
        @inches = inches_regx.match(val).to_s.gsub(/\"/, '')
      end
    end

    def foreign_travel_country
      Enum::Country.find self.foreign_travel_country_id
    end

    def as_json options={}
      output = super
      output.delete_if do |k,v|
        next true if self.class.serialized_attributes.has_key?(k) && k =~ /details?$/
      end
      DELEGATED_KEYS.each do |k|
        next if self.send(k).nil?
        next if k =~ /_type_id/
        output[k] = self.send k
      end
      output
    end

    def health_condition_detail_fields_are_valid
      NON_DISEASE_HEALTH_CONDITIONS_MAP.each do |condition_name, klass|
        next if klass < ArrayDetail
        details=self.send("#{condition_name}_details")
        inherit_errors condition_name, details
      end
      diseases.each do |disease_name, disease|
        inherit_errors disease_name, disease
      end
      crimes.each do |crime|
        inherit_errors 'criminal_history', crime
      end
      moving_violations.each do |violation|
        inherit_errors 'moving_violation', violation
      end
    end

    private

    def will_change field
      send "#{field}_will_change!"
      yield
      before, after = changes[field]
      changed_attributes.delete(field) if before == after
    end

    # Create zero or more MovingViolation objects without any of the necessary
    # information, knowing only that it occured within a certain time period.
    # If according to one of the examined attributes, the number of moving
    # violations should actually be decreased, we do not honour that
    # attribute.
    def add_dummy_moving_violations
      [
        [moving_violation_last_6_mo, 6.months.ago.to_date],
        [moving_violation_last_1_yr, 1.year.ago.to_date],
        [moving_violation_last_2_yr, 2.years.ago.to_date],
        [moving_violation_last_3_yr, 3.years.ago.to_date],
        [moving_violation_last_5_yr, 5.years.ago.to_date],
      ].each do |qty, since|
        next if qty.nil?
        n_existing = moving_violations.count { |v| v.date >= since }
        n_needed = qty - n_existing
        (1..n_needed).each do |i|
          moving_violations << Crm::HealthInfo::MovingViolation.new(date: since)
        end
      end
    end

    def inherit_errors obj_name, object
      unless object.valid?
        object.errors.each do |key, error_msg|
          errors.add "#{obj_name}_#{key}", error_msg
        end
      end
    end

    def set_attrs_on_moving_violation_for_date date, attrs
      violation = moving_violations.find { |v| v.date == date }
      if violation.nil?
        violation = Crm::HealthInfo::MovingViolation.new date: date
        moving_violations << violation
      end
      attrs.each do |k,v|
        violation.send "#{k}=", v
      end
    end
  end
end
