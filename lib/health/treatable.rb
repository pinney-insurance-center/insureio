module Health::Treatable
  extend ActiveSupport::Concern

  included do
    attr :currently_treating, :boolean
    attr :last_treatment, :date, 'The end of treatment (may be in future)'
  end
end
