module Health
  class Disease < Detail
    # The Disease class is used for both a personal Health History and a
    # Family History (`relatives_diseases`)
    include Treatable

    attr :true, :boolean, "Optional. If no other fields are set on this Object, then this flag can be set to indicate that the present health concern exists even though no further detail has been provided."
    attr :date, :date, "Date of diagnosis"
    attr :onset_age, :integer, 'The age at which this individual developed an disease'
    attr :death_age, :integer, 'Include this only if this disease was fatal (and pertains to a family member instead of a proposed insured).'
    attr :type_id, :integer
    attr :relationship_type_id, :integer, %Q{Omit this field if this object pertains to the <a href="/doc/consumer-fields.html">Consumer</a> in which it is enveloped (instead of to a family member).}

    validate :type_id, presence: true
    validate :validate_type_id
    validate :validate_relationship_type_id

    def self.for_name type_name, attrs_hash={}
      type_id = Enum::DiseaseType.id(type_name.to_s)
      raise Enum::NotFound.new("Unrecognized Disease name #{type_name}") if 0 == type_id
      Coder.build_disease attrs_hash.merge( type_id: type_id )
    end

    def disease_type
      Enum::DiseaseType.find(type_id)
    end

    def relationship_type
      Enum::RelationshipType.find(relationship_type_id)
    end

    def truthy? ignore_true_field = false
      return false if self.true === false # This indicates falseness even if we're ignoring `true`
      self.class.fields.any? do |field|
        next if field.first.to_sym == :type_id # This field does not indicate truthiness
        field_indicates_truthy?(field.first, ignore_true_field)
      end
    end

    class DiseaseArray < ::Array
      include ArrayDetail

      def initialize input_array=nil
        diseases = Array(input_array).map do |data|
          if data.is_a? Disease
            data
          elsif data.is_a? Hash
            Coder.build_disease data
          else
            raise ArgumentError.new "Unrecognized type in DiseaseArray: #{data.class.name}"
          end
        end
        super diseases
      end
    end

    class DiseaseSet < ::HashWithIndifferentAccess
      def initialize input_array=nil
        input_array = [input_array] if input_array.is_a?(Hash)
        Array(input_array).each do |disease|
          self[Enum::DiseaseType.find(disease.type_id).name] = disease
        end
        self.default_proc = proc do |this, key|
          this[key] = Disease.for_name(key)
        end
      end

      def []= type_name, value
        type_id = Enum::DiseaseType.id(type_name.to_s)
        raise "Unrecognized DiseaseType name #{type_name.to_s}" if type_id == 0
        if value.is_a? Disease
          value.type_id = type_id
        elsif value.is_a? Hash
          value['type_id'] = type_id
          value = Coder.build_disease(value)
        else
          value = Coder.build_disease({'type_id' => type_id, 'true' => !!value})
        end
        super type_name, value
      end

      def valid?
        return false unless all? &:valid?
        keys.all? { |k| Enum::DiseaseType.id(key.to_s) > 0 }
      end

      def self.from_hash input_hash
        output = new
        input_hash.each do |type_name, attrs|
          output[type_name] = attrs
        end
        output
      end

      Enum::DiseaseType.each do |type|
        define_method type.name do
          self[type.name]
        end
        define_method "#{type.name}=" do |value|
          self[type.name] = value
        end
      end

      # Support deprecated labels
      alias_method :cancer_breast, :breast_cancer
      alias_method :cancer_breast=, :breast_cancer=
      alias_method :cancer_prostate, :prostate_cancer
      alias_method :cancer_prostate=, :prostate_cancer=
    end

    class Coder
      def initialize uniq=false
        @uniq = uniq
      end

      def self.build_disease data_hash
        type_id = data_hash.with_indifferent_access['type_id']
        raise Enum::NotFound.new("attempted to create a Disease with no type_id #{data_hash.inspect}") if type_id.nil?
        type = Enum::DiseaseType.find(type_id)
        raise Enum::NotFound.new("unrecognized Disease type_id #{type_id} #{data_hash.inspect}") if type.nil?
        klass_name = type.klass || type.name.camelize
        klass = "Crm::HealthInfo::#{klass_name}".constantize
        klass.new(data_hash)
      end

      def dump diseases
        diseases = diseases.values if diseases.is_a?(Hash)
        diseases.select! &:truthy? # Non-truthy diseases hold no information which cannot be reconstructed on the fly
        Array(diseases).presence&.to_json # Use `try` to avoid putting 'null' in db
      end

      def load json
        diseases = Array(JSON.load(json)).map { |d| Coder.build_disease(d) }
        @uniq ? DiseaseSet.new(diseases) : DiseaseArray.new(diseases)
      end
    end

    protected

    def validate_type_id
      unless type_id && disease_type
        errors.add :type_id, "does not indicate a known DiseaseType"
      end
    end

    def validate_relationship_type_id
      unless relationship_type_id.nil? || relationship_type
        errors.add :relationship_type_id, "does not indicate a known RelationshipType"
      end
    end
  end
end
