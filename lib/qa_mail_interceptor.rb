class QaMailInterceptor
  def self.delivering_email(message)
    #if 'to' email address is in the whitelist or sent to the developers email,
    #then skip the rerouting
    unless ([APP_CONFIG['email']['developers']] + APP_CONFIG['email']['testers']).include?(message.to.first)
      message.subject = "[#{message.to}] #{message.subject}"
      message.to = message.from.first
    end
  end
end
