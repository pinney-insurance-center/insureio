module Ixn
  REST_URL_BASE = 'https://api.ixn.tech/v1'

  # Create a number of API-calling functions
  extend Ixn::Public

  # Send existing Consumer, Kase, Quote details to and then save any changes
  # to the original quote. @arg is a Quoting::Quote. If `quoting_details`
  # already has an `ixn_guid`, signifying that this is a "requote," then this
  # guid will be sent to IXN in order to update their records.
  def self.quote quoting_details
    iq = Ixn::Quotes.new quoting_details
    iq.to_quote_objects
  end

private

  # Make a GET request against the IXN API
  def self.get path
    request path do |uri|
      Net::HTTP::Get.new(uri)
    end
  end

  # Make and parse a JSON GET request agains the IXN API. Cache if indicated
  def self.get_json endpoint, cache_key=nil, ttl=nil
    if cache_key.nil? != ttl.nil?
      raise ArgumentError "cache_key & ttl must both be present or nil: #{cache_key.inspect} vs #{ttl.inspect}"
    elsif cache_key.nil?
      JSON.parse Ixn.get(endpoint).body
    else # Recurse on a cache miss
      Rails.cache.fetch(cache_key, expires_in: ttl) { get_json endpoint }
    end
  end

  # Make a request against the IXN API
  def self.request path, &block
    uri = URI([REST_URL_BASE, path].join '/')
    Net::HTTP.start(uri.host, use_ssl: true) do |http|
      req = yield uri # Block must return a Net::HTTP::Request
      req['Accept'] = 'application/json'
      req['Content-Type'] = 'application/json'
      http.request req
    end
  end
end
