class EspPushWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: 2

  def perform kase_id
    kase = Crm::Case.find kase_id
    Esp.push_ticket(kase) if !kase.opportunity?
  rescue => ex
    AuditLogger['esp_push_worker'].error "kase id:#{kase_id}: #{ex.message}\n#{ex.backtrace.join "\n"}"
    ExceptionNotifier.notify_exception ex, data: { message: "EspPushWorker error. (#{kase_id}) #{ex.message}", kase_id: kase_id }
  end
end
