class TaskWorker
  include Sidekiq::Worker
  sidekiq_options queue: :io_tasks, retry: false

  def perform task_id
    AuditLogger['task_worker'].info "task id:#{task_id}"
    task = Task.find(task_id)
    task.execute_from_bg
  end
end