# Delivers Marketing::Email::Message for Sidekiq
class EmailMessageWorker
  include Sidekiq::Worker
  sidekiq_options queue: :email_retries, retry: false

  def perform message_id
    AuditLogger['email_message_worker'].info "message id:#{message_id}"
    msg = Marketing::Email::Message.find(message_id)
    msg.deliver unless msg.failed_attempts.to_i >= Marketing::Email::Message::MAX_FAILED_ATTEMPTS
  end
end