class NewUserWorker
  include Sidekiq::Worker
  sidekiq_options queue: :new_user_emails, retry: false

  def perform user_id
    AuditLogger['sidekiq'].info "performing NewUserWorker on id #{user_id}"
    user = User.find(user_id)
    SystemMailer.notify_admin_of_new_user(user).deliver
  end
end