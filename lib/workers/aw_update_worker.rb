# Delivers Marketing::Email::Message for Sidekiq
class AwUpdateWorker
  include Sidekiq::Worker
  sidekiq_options queue: :aw_updates, retry: false

  def perform status_type_code, aw_account_key
    #Redis keys are shared by all app servers,
    #so this ensures only one host is running this update at once.
    lock_key="aw_update_in_progress_for_status_#{status_type_code}_account_#{aw_account_key}"
    if $redis[lock_key].present?
      time_of_lock=$redis[lock_key].match(/started (\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} -0(7|8)00)$/)[0] rescue nil
      if time_of_lock && Time.parse(time_of_lock)<1.hour.ago
        logger.warn "Detected old lock. Overwriting, since that process should have completed by now."+
          "Time of old lock: #{time_of_lock}"
      else
        logger.error "Locked out. Status: #{status_type_code}."+
          "Worker jid: #{self.jid}."+
          "Worker options: #{self.class.sidekiq_options_hash}."
        return
      end
    end
  
    logger.info "Setting lock. Status: #{status_type_code}."+
      "Worker jid: #{self.jid}."+
      "Worker options: #{self.class.sidekiq_options_hash}."

    lock_code="#{`echo $HOSTNAME`} started #{Time.now}"

    begin
      $redis[lock_key]=lock_code
      us=Processing::AgencyWorks::UpdateService.new
      us.request_single_update status_type_code, aw_account_key
    rescue => ex
      ExceptionNotifier.notify_exception ex
    ensure
      $redis.del lock_key
      logger.info "Released lock. Status: #{status_type_code}. Exiting."
    end
  end
end