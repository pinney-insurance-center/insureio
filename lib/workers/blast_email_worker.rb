# Delivers Marketing::Email::Message for Sidekiq
class BlastEmailWorker
  include Sidekiq::Worker
  sidekiq_options queue: :email_blasts, retry: false

  def perform blast_id, batch_num, blast_initiated, template_id, sender_id, targets, retry_count=0
    AuditLogger['blast_email_worker'].info "#{retry_count==0 ? 'starting' : 'retrying'} batch #{batch_num} of email blast #{blast_id} from user #{sender_id}, initiated at #{blast_initiated}, using template #{template_id}."
    sender  =User.find(sender_id)
    template=Marketing::Email::Template.find(template_id)

    targets.each do |t|
      #For each recipient, either an email string or an array with class name and id are passed in.
      if t.is_a?(Array)
        t=t[0].constantize.send(:find, t[1])
        person=t.is_a?(Crm::Case) ? t.consuemr : t
        target_string="#{person.class.name} #{person.id}#{' regarding case '+t.id.to_s if t.is_a?(Crm::Case)}"
        recipient=t.primary_email.to_s
        target_opt={target:t}
      else
        target_string=t
        recipient=t
        target_opt={}
      end
      begin
        AuditLogger['blast_email_worker'].info "delivering msg from batch #{batch_num} of blast #{blast_id} to recipient #{target_string}"
        template.deliver({sender:sender, recipient:recipient, blast_id:blast_id}.merge(target_opt) )
      rescue =>ex
        AuditLogger['blast_email_worker'].error "delivering msg from batch #{batch_num} of blast #{blast_id}: #{ex} #{ex.backtrace}"
      ensure#so that one error doesn't cause the entire batch to fail.
        next
      end
    end
  rescue ActiveRecord::ConnectionTimeoutError =>ex
    retry_in=rand(30..120)
    will_retry=retry_count<3
    AuditLogger['blast_email_worker'].error "delivering batch #{batch_num} of blast #{blast_id}: Timed Out.#{' Will retry.' if will_retry}"
    if will_retry
      BlastEmailWorker.perform_at(retry_in.seconds.from_now, blast_id, batch_num, blast_initiated, template_id, sender_id, targets, retry_count+1)
    end
  rescue =>ex
    AuditLogger['blast_email_worker'].error "delivering batch #{batch_num} of blast #{blast_id}: #{ex}"
  end
end