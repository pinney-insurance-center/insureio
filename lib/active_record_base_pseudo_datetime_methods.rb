=begin
	
The purpose of this patch is to make pseudo-datetime accessor and mutator methods.

So if you have a date field :red_letter on a class MyClass,
calling MyClass::pseudo_date_accessor(:red_letter) can generate useful methods
  :red_letter_human
  :red_letter_human=

=end

module ActiveRecord
	class Base

		PSEUDO_DATETIME_FORMAT = '%m/%d/%Y %I:%M %p'

		def self.pseudo_datetime_accessor attr_name, options={}

			define_method("#{attr_name}_human") {
				self.read_attribute(attr_name).try(:strftime, PSEUDO_DATETIME_FORMAT)
			}

			define_method("#{attr_name}_human=") { |val|
				val = val.blank? ? nil : Time.zone.parse(val)
				self.send :write_attribute, attr_name, val
			}

		end

	end
end