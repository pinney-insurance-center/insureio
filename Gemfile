source 'http://rubygems.org'

gem 'rails', '~> 4.0.1'
ruby '2.3.0'

gem 'mysql2', '~> 0.3.21'
gem 'sass-rails', '~> 4'
gem 'coffee-rails'
#   gem 'uglifier', '>= 1.0.3'

group :development do
  gem 'better_errors'                 # displays errors with variable states, stack trace, code view, in browser
  gem 'binding_of_caller'             # supports better errors by providing IDE in browser when error occurs
  gem 'letter_opener' # mail interceptor which opens emails in new a browser tab
  gem 'meta_request'      # for use with chrome rails-panel extension - allows showing dev log as structured data within chrome
  gem 'rack-mini-profiler', require:false
  gem "rails-erd", require:false # generates entity relationship diagrams, important for documentation. typical usage: `bundle exec erd --cluster=true`
  gem 'thin'              # Sane development server
  #### DON'T JUST ADD GEMS TO THE END OF THIS LIST! ALPHABETIZE! ###
end

group :test do
  gem 'capybara',    require: false
  gem 'database_cleaner', require: false  # for use with capybara tests
  gem 'excon'          # used by vcr gem.
  gem 'factory_bot', '~>4.8'          # for generating test models
  gem 'factory_bot_rails', '~>4.8'#, require:false
  gem 'forgery'                         # for generating test data
  gem 'launchy',     require: false
  gem 'rspec-rails'
  gem "selenium-webdriver", "~> 2.40.0" # for AJAX testing
  gem 'shoulda',     require: false
  gem 'simplecov',   require: false    # Test coverage
  gem 'spork-rails', platform: [:mswin, :mingw] # Fast rpsec testing on Windows
  gem 'timecop'
  gem 'vcr', '~> 2.9.3'#'~> 2.5.0'
  gem "webmock", '~> 1.10.2'
  #### DON'T JUST ADD GEMS TO THE END OF THIS LIST! ALPHABETIZE! ###
end

group :test, :design_testing, :dev, :release, :development do
  gem 'bullet', '5.4.3', require:false# for identifying areas to speed up database queries
  gem 'pry-nav'
  gem 'pry-rails'
  gem 'test-unit', '~> 3.0'
  gem 'oj', require:false
end

gem 'activeuuid', '>= 0.5.0'
#gem 'ahoy_matey', '1.1.0'      #for parsing and tracking location, browser, OS. our use of it was so heavily customized, we'd be better off just using its dependencies directly.
gem 'american_date'             # allows mm/dd/yyy to be parsed. appears to effect `to_s` output, but not `to_json` output.
gem 'attr_encrypted', '~> 3.1'  # encrypt activerecord attributes in db.
gem 'authlogic', '~> 3.2'       # user login
gem 'awesome_print', '~>1.8', require: false #produces more readable objects in console. @see config/initializers/awesome_print.rb.
gem 'aws-sdk-s3', '~> 1'        # the service-specific gem for sdk v3.
gem 'capistrano', '~> 2.15.7'   # deployment of application
gem 'capistrano-ext', require: false
gem 'counter_culture'           # handles updating counter cache columns
gem 'curb'                      # a binding for libcurl4. allows tighter control of requests than `Net::HTTP`.
#gem 'daemons'                   # used for running delayed_job jobs
gem 'dejunk', '~> 0.2'          # filter obvious junk from user/consumer-entered text
gem 'exception_notification', '~> 4.4.0'
gem 'faraday', '~>0.8.11'       # REST client for interfacing with external APIs
gem 'faraday_middleware'
gem 'gpgme', '~> 2.0.20' # For GPG/PGP (which we do for ESP)
gem 'holidays', '~> 2.1.0'
gem 'httpi', '~> 2.4'
gem 'httparty'                  #exception_notification uses it to post to chat webhook
gem 'jquery-ui-rails', '~> 4.0.5'#Required by bootstrap. REMOVE once all plain bootstrap has been replaced with ui-bootstrap.
gem 'json', '~> 1.8.1'          # Specified so as to 'safely' conduct an update for the sake of stripe gem (2014-05-21)
gem 'lograge', '~> 0.4'         # cleaner log formatting.
gem 'lograge-sql', '~> 1.2.0'   # cleaner query log formatting
gem 'marginalia', '~> 1.5.0'    # adds helpful comments to sql queries, useful for identifying origins of slow queries
gem 'mini_magick'               # photo resizing
gem 'multi_xml'
gem 'net-ssh', '~> 4.0'         #required by capistrano. specifying here so it does not try to download the newer versions, which require ruby 2.2.6.
gem 'niceql', require:false     # prettyfies sql output **with color** for easier reading. only used when you explicitly call `.niceql` on a scope.
gem 'pp_sql'                    # prettyfies sql output for easier reading. replaces the default formatter in console and logs.
gem 'pry', require:false
gem 'nested_form'
gem 'redis'                     # for publishing
gem 'rocketchat'
gem "ruby_dig"                  # backfills a feature of ruby 2.3 until we can update
gem 'ruby-saml', '~> 1.11.0'
gem 'rvm-capistrano', require: false # for deploying to servers where multiple rubies are available
gem 'safe_attributes'           # takes care of attribute name collisions w/ ActiveRecord (we use 'delay')
gem 'savon', '~> 2.11'
gem 'sidekiq', '~> 2.17.0'      # handles background/delayed jobs
gem 'spreadsheet', require: false
gem "stripe", '~> 1.11.0'
gem 'transaction_retry'         # Catch mysql deadlock and try again
gem 'tzip', '~> 0.0.4'          # Find timezone by zip code
gem 'whenever', require: false  # handles cron scheduled tasks
gem 'wicked_pdf'                # creates pdfs from html (is a wrapper for wkhtmltopdf)
gem 'will_paginate', '~> 3.0'   # paginates index pages for activerecord
gem 'wkhtmltopdf-binary','~>0.12'# the backend linux tool that creates pdfs from html using QT Webkit rendering engine
#### DON'T JUST ADD GEMS TO THE END OF THIS LIST! ALPHABETIZE! ###
