#################
# This is a Dockerfile for setting up the Insureio rails app.
# This Dockerfile's intended use is for running Jenkins pipelines,
# but it can be used for development or just as a specification
# to show devs how to build the application.
# Cheers! - Markham (a.k.a. entrity)
#################

FROM scratch
LABEL version=0.0.1
CMD /bin/bash -l
ADD centos-7.7-x86_64-docker.tar.xz /
ADD ssh /root/.ssh

#################
# Set up ordinary dependencies
#################
RUN yum updateinfo
RUN yum -y install \
  curl.x84_64 \
  git \
  graphviz.x86_64 \
  mariadb.x86_64 \
  mariadb-devel.x86_64 \
  mariadb-server.x86_64 \
  openssl.x86_64 \
  which
# Install dependencies for karma
RUN yum -y install libcurl-devel.x86_64
# Install dependencies for Chrome (for karma)
RUN yum -y install \
  alsa-lib.x86_64 \
  at-spi2-atk.x86_64 \
  libXtst.x86_64 \
  libXScrnSaver.x86_64 \
  gtk3.x86_64
# Install dependencies for Protractor (E2E testing)
RUN yum -y install java-11-openjdk.i686
# Install latex
RUN yum -y install wget perl-Digest-MD5
RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
RUN tar xzf install-tl-unx.tar.gz
RUN cd install-tl-* && touch profile && ./install-tl -profile profile
RUN echo export PATH="/usr/local/texlive/2020/bin/x86_64-linux/:\$PATH" >> /etc/profile.d/custom.sh

#################
# Set up MySQL
#################
RUN /usr/libexec/mariadb-prepare-db-dir
RUN /usr/libexec/mysqld --user=root &

#################
# Set up RVM
#################
RUN yes | gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN \curl -sSL https://get.rvm.io | bash -s stable
SHELL ["/bin/bash", "-c", "-l"]
RUN rvm install 2.3.0

#################
# Set up rails app
#################
RUN rvm @global do gem uninstall -x bundler
RUN gem install bundler --version=1.7

#################
# Set up Node.js
#################
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash -
RUN yum -y install nodejs
RUN npm install
