#!/bin/sh

if ! docker ps | grep Up; then
  docker run -it \
    -v /var/jenkins_home/workspace/first-insureio:/Insureio \
    -v /var/insureio:/var/insureio \
    -u root --privileged reg.qa/insureio:latest /usr/local/rvm/bin/rvm-shell
else
	docker exec -it $(docker ps | tail -n1 | awk '{print $1}') /usr/local/rvm/bin/rvm-shell
fi
