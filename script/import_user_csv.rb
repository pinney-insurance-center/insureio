class User

  def csr_ids_attributes= array_of_hashes
    ids=array_of_hashes.map{|x| x[:value]}
    self.tags= self.tags.merge({'EZLife Account Ids'=>ids.to_s})
  end

  def image_file_names= fn
    self.tags= self.tags.merge({'EZLife Image File Name'=>fn})
  end

  def app_type_pref= string
    if string=='Application'
      self.submit_application=true
    elsif string=='Referral'
      self.submit_referral=true
    end
  end

  def tenant_name= tname
    self.tenant_id= Tenant.id(tname.downcase)||0
  end
  alias_method :subdomain=, :tenant_name=

  def agent_of_record_full_name= string
    self.tags= self.tags.merge({'agent of record full name'=>string})
  end

  def agent_of_record_phone= string
    self.tags= self.tags.merge({'agent of record phone'=>string})
  end

  def agent_of_record_email= string
    self.tags= self.tags.merge({'agent of record email'=>string})
  end

end

@hashes=[]
@successes=[]
@failures=[]
def process_csv_file data
  #This will remove non-ascii characters.
  data=data.encode(Encoding.find('ASCII'),{invalid: :replace, undef: :replace, replace:'', universal_newline:true})
  @rows=CSV.parse(data, headers:true)
  @ct=@rows.length

  convert_rows_to_nested_hashes
  create_users
end

def convert_rows_to_nested_hashes
  @rows.each_with_index do |row,i|
    hash=row.to_h
    nested_hash={}
    hash.each do |k,v|
      #The bizarre-looking `gsub` is needed because Exce or other spreadsheet apps sometimes add
      #nonstandard space characters, which `strip` cannot remove.
      method_chain=k.to_s.downcase.strip.gsub(/\A\p{Space}*|\p{Space}*\z/, '').split('.')
      method_chain=method_chain.map{|m|  matches=m.match(/(\D+)(\d+)/);  matches ? [matches[1],matches[2].to_i] : m }.flatten
      convert_method_chain_to_nested_hash nested_hash, method_chain, v
    end
    nested_hash=deep_delete_blanks nested_hash
    @hashes << nested_hash
  end
end

def create_users
  @hashes.each_with_index do |h,i|
    user=User.new(h)
    user.login||=user.emails.first.try(:value)||user.name.gsub(/\s+/,'')
    user.role_id=12#recruit
    user.save
    if user.persisted? && user.errors.empty?
      @successes << {row_number:i, id:user.id, created_at:user.created_at, updated_at:user.updated_at}
    else
      @failures << {row_number:i, errors:user.errors.full_messages}
    end
  end
end

def convert_method_chain_to_nested_hash obj, method_chain, v
  if method_chain.length>1
    if method_chain[0].is_a?(Integer)#key is an index
      next_obj=obj[ method_chain[0] ]||={}
    else#key is a key
      if method_chain[1].is_a?(Integer)#value is an array
        next_obj=obj[ "#{method_chain[0].pluralize}_attributes" ]||=[]
      else#value is an object
        next_obj=obj[ "#{method_chain[0]}_attributes" ]||={}
      end
    end
    method_chain.shift
    convert_method_chain_to_nested_hash next_obj, method_chain, v
  else#key is an attribute
    obj[ method_chain[0] ]=v
  end
end

def deep_delete_blanks hash_or_array
  if hash_or_array.is_a?(Hash)
    new_structure={}
    hash_or_array.each do |k,v|
      if( hash_or_array[k].is_a?(Hash) || hash_or_array[k].is_a?(Array) )
        v2=deep_delete_blanks hash_or_array[k]
        new_structure[k]=v2 unless v2.blank?
      else
        new_structure[k]=v unless v.blank?
      end
    end
  elsif hash_or_array.is_a?(Array)
    new_structure=[]
    hash_or_array.each_with_index do |v,i|
      if( hash_or_array[i].is_a?(Hash) || hash_or_array[i].is_a?(Array) )
        v2=deep_delete_blanks hash_or_array[i]
        new_structure << v2 unless v2.blank?
      else
        new_structure << v unless v.blank?
      end
    end
  end
  new_structure
end