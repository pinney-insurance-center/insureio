require_relative 'linode_helper'
require 'net/http'
require 'openssl'
require_relative 'post_msg_to_chat'

class Patcher
  include ScriptUtil

  def initialize servers, remote_dir, opts={}
    @servers = Array(servers) # user@host:port
    @remote_dir = remote_dir # e.g. /var/www/insureio/current
    @env_name = opts.fetch :env_name, :production # RAILS_ENV
    @default_branch = opts.fetch :default_branch, :master
    @branch = ARGV[0] || current_branch_name
    if @branch.to_s != @default_branch.to_s
      exit unless prompt "Selected branch #{@branch} and default branch #{@default_branch} differ. Continue?", false
    end
    @linode = LinodeNodeBalancer.new(true)
  end

  # Make requests against the server to ensure assets are built and Passenger is ready
  def check_server_health hostname, allow_fail
    if hostname =~ /@/
      puts "hostname #{hostname}"
      _user, hostname, _port = parse_server hostname
    end
    info "Running health checks on #{hostname}..."
    Net::HTTP.start hostname, use_ssl: production?, verify_mode: OpenSSL::SSL::VERIFY_NONE do |http|
      res = http.get('/', 'Accept' => 'html')
      raise "Expected 303 redirect from server #{hostname}. Got #{res.code}" unless res.code == "303"
      redirect_regex = production? ? /^https:\/\/[^\/]+\/login[^\/]*$/ : /^http:\/\/[^\/]+\/login[^\/]*$/
      raise "Expected redirect to http login page. Got #{res['location']}" unless res['location'] =~ redirect_regex
      res = http.get("/assets/application.js")
      raise "Expected 200 response for /assets/application.js. It could be that assets are not successfully compiled (yet)." unless res.code == "200"
    end
    green "Health checks passed"
  rescue => ex
    if allow_fail
      red "Rescued exception #{ex.message}"
      exit 11 unless prompt("Do you wish to continue?", false)
    else
      throw ex
    end
  end

  def run
    @cap_tasks = ['deploy:update_code', 'deploy:create_symlink', 'deploy:restart', 'sidekiq:restart', 'nodejs:start']
    check_for_pending_db_migrations
    chat_notifier = RocketChat::Notifier.new @env_name, verb: '_is starting_'
    chat_notifier.notify
    info "Preparing update to #{@env_name} from branch #{@branch}..."
    @servers.each_with_index do |server, idx|
      user, host, port = parse_server(server)
      info "\nFor server ##{idx+1} (#{host})..."
      check_server_health host, true
      server_down!(idx, host) if production?
      blue "Running capistrano tasks on app server #{idx+1} (#{host})."
      run_capistrano [host, port].compact.join(':')
      @after_code_update&.call(idx)
      check_server_health host, false
      server_up!(idx, host) if production?
    end
    chat_notifier.notify interactive: false, verb: '_completed_'
  end

  # Take server off Nodebalancer
  def server_down! idx, host
    if @linode.count_reject.any? { |scheme| scheme != 0 }
      red "At least one node is already REJECTing connections."
      exit 12 unless prompt("Do you wish to continue?", false)
    end
    blue "Taking app server #{idx+1} (#{host}) off of the Node balancer."
    @linode.down 1+idx, true
  end

  # Put server back onto Nodebalancer
  def server_up! idx, host
    blue "Putting app server #{idx+1} (#{host}) back on the Node balancer."
    @linode.up 1+idx
  end

  def status
    info "Loading node statuses..."
    @nodesets = @linode.nodes(true)
    @nodesets.each do |nodes|
      nodes.nodes.each_with_index do |n, i|
        puts "%d %-5s %20s %4s %s" % [1+i, nodes.protocol, n.address, n.status, n.mode]
      end
    end
    @nodesets
  end

protected

  def check_for_pending_db_migrations
    unless system 'script/pending-db-migrations.sh'
      blue 'What would you like to do?'
      info <<~EOF
          [a]bort (DEFAULT)
          [r]un migrations during patch
          [p]ause after 1st server's capistrano tasks
          pause after [e]ach server's capistrano tasks
          patch [w]ithout migrations
      EOF
      bell(n: 3)
      case getch.downcase
      when 'e'
        green "Will pause for input after each server's capistrano_tasks, allowing you to perform whatever tasks you wish..."
        @after_code_update = -> (idx) { pause("Press enter to continue...") }
      when 'p'
        green "Will pause for input after first server's capistrano_tasks, allowing you to perform whatever tasks you wish..."
        @after_code_update = -> (idx) { pause("Press enter to continue...") if idx == 0 }
      when 'r'
        green "Will run migrations during patch..."
        idx = @cap_tasks.index 'deploy:update_code'
        if idx
          @cap_tasks[idx] = 'deploy:migrations'
        else
          @cap_tasks << 'deploy:migrations'
        end
      when 'w'
        red "Will NOT run migrations...", true
      else
        red "Aborting...", true
        exit 1
      end
    end
  end

  def current_branch_name
    branch_name = `git rev-parse --abbrev-ref HEAD`
    raise "Git failed with #{$?}" if 0 != $?
    branch_name.strip
  end

  def parse_server server
    m = /^([^@\s]+)@([^:\s]+)(:(\d+))?$/.match server
    raise "Bad server identifier parse \"#{server}\"" if m.nil?
    user, host, _, port = m.captures
    return user, host, port
  end

  def pause msg
    bell(n: 3)
    blue msg
    $stdout.write '... '
    gets
  end

  def production?
    @env_name&.to_sym == :production
  end

  def run_capistrano host_and_port
    puts `HOSTS=#{host_and_port} bundle exec cap -s branch=#{@branch} #{@env_name} #{@cap_tasks.join(' ')}`
    raise "Capistrano failed with #{$?}" if 0 != $?
  end

  def blue msg; puts "\033[36m#{msg}\033[0m"; end

  def green msg; puts "\033[32m#{msg}\033[0m"; end

  def info msg; puts "\033[33m#{msg}\033[0m"; end

  def red msg, no_bell=false
    puts "\033[31m#{msg}\033[0m"
    bell(n: 4) unless no_bell
  end
end
