#!/usr/bin/env node

//ES5 (aka ECMAScript 2009) is the latest that IE11 can reliably support, and that's mostly supported by IE9.
//Our insurance consumer facing (not to be confused with client facing, as in front end) quote processes
//that we license to users (not the Insureio quote path) need to be backward compatible to IE11.


const fs       = require('fs');//node's filesystem reader
const JSHINT   = require('../node_modules/jshint/dist/jshint.js').JSHINT;
const execSync = require('child_process').execSync;

const es5Opts={
  esversion: 5,
  multistr:  true,//allow escaping of EOL
  asi:       true,//allow Automatic Semicolon Insertion. iow, don't complain about missing ones
  sub:       true,//allow bracket notation when dot notation would also work
  indent:    2,
  maxerr:    75,
  "ignoreDelimiters": [
    //Expects an Array of Objects like `{"start":String,"end":String}`
    //where the String values are used to ignore certain chunks from code.
    //ie. When `ignoreDelimiters[i].start=="<%" && ignoreDelimiters[i].end=="%>"`,
    //it sees `foobar <% baz qux %> lorem ipsum` as `foobar  lorem ipsum`.
    //See: https://github.com/jshint/jshint/blob/b7dd7d8f93244abb874fdb616c7a5725209e36b0/src/jshint.js#L6435
    { "start": "<%", "end": "%>" }//excludes embedded ruby.
  ],
};
const consumerFacingJSFiles=[
  'public/assets/rtt_quote_app.js',//a compiled js file
  'public/assets/quoting_widget.js',//a compiled js file
  'app/views/quoting/widgets/show.js.erb',
];
const fileCount  =consumerFacingJSFiles.length;
const cliSeparator='=================';

let lintSuccesses=[],
    lintErrors   =[],
    lintWarnings =[],
    passCount    =0,
    failCount    =0;


function lintFileForES5(path){
  var src=fs.readFileSync(path, 'utf8'),
      msgs,
      errs=[],
      warns=[],
      summaryForFile='',
      forOfLoopMatcher=/for\s*\(\s*var\s+\w+\s+of\s+/,
      lines=src.split("\n");
  JSHINT(src,es5Opts);
  msgs=JSHINT.errors;
  for(let m in msgs){//needed because not all "errors" are actually errors.
    msgs[m].file=path+':'+msgs[m].line+':'+msgs[m].character;
    delete msgs[m].id;
    delete msgs[m].line;
    delete msgs[m].character;
    if( !msgs[m].a ){ delete msgs[m].a; }
    if( !msgs[m].b ){ delete msgs[m].b; }
    if( !msgs[m].c ){ delete msgs[m].c; }
    if( !msgs[m].d ){ delete msgs[m].d; }

    if( msgs[m].code.match(/^W/) ){
      warns.push(msgs[m]);
    }else{
      errs.push(msgs[m]);
    }
  }

  for(let lNum in lines){
    let forOfMatchedLine=lines[lNum].match(forOfLoopMatcher);
    if( forOfMatchedLine ){
      errs.push({
        code:'Custom',
        evidence: lines[lNum],
        reason:'IE does not support for..of loops. Use for..in instead.',
        file:path+':'+lNum
      });
    }
  }

  if(errs.length>0){
    summaryForFile+='Fx'+errs.length+' ';
    lintErrors=lintErrors.concat(errs);
    failCount++;
  }else{
    summaryForFile+='P - ';
    lintSuccesses.push(path);
    passCount++;
  }
  if(warns.length>0){
    summaryForFile+='Wx'+warns.length+' ';
    lintWarnings=lintWarnings.concat(warns);
  }
  summaryForFile+=path;
  console.log(summaryForFile);
}

//Generate complete script files like those which will be served in production.
execSync(
  "bundle exec rake assets:precompile_and_symlink",
  (error, stdout, stderr) => {
    if(error){
      console.log('Error trying to run rake: '+error.message);
    }else if(stderr){
      console.log('Error output from rake: '+stderr);
    }else{
      console.log('stdout: '+stdout);
    }
});

for(let i in consumerFacingJSFiles){
  lintFileForES5( consumerFacingJSFiles[i] );
}

if(lintErrors.length){
  console.error(cliSeparator);
  console.error('Errors:');
  console.error(cliSeparator);
  console.error(lintErrors);
}
if(lintWarnings.length){
  console.warn(cliSeparator);
  console.warn('Warnings:');
  console.warn(cliSeparator);
  console.warn(lintWarnings);
}

console.log(cliSeparator);
console.log('Total:  '+fileCount);
console.log('Passed: '+passCount);
console.log('Failed: '+failCount);

/* Clean up. This script ran `assets:precompile`. */
execSync("bundle exec rake assets:clobber");

return passCount==fileCount;