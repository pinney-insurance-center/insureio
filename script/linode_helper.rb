#!/usr/bin/env ruby

require 'json'
require 'ipaddr'
require_relative 'script_util'

class LinodeNodeBalancer
  include ScriptUtil

  N_NODES_ON_NODEBALANCER = 3
  WRAP_LIMIT = 120

  def fatal_install_linode_cli
    puts %q{
  Please install the linode-cli
      e.g. pip install linode-cli --upgrade

  Then make sure the executable is on your PATH. If you installed this as a normal user, it might be located at ~/.local/bin
  }
    exit 2
  end

  def fatal_linode_access_token
    puts %q{
  You have linode-cli installed, but it appears that you have not set up a Personal Access Token for it.

  Please go to https://cloud.linode.com/profile/tokens and click "Add a Personal Access Token," then use it to set up your linode-cli but running
      linode-cli configure
  }
    exit 2
  end

  def initialize postpone_load=false
    %x{>/dev/null which linode-cli}
    fatal_install_linode_cli unless $?.to_i == 0
    %x{</dev/null 2>/dev/null linode-cli show-users}
    fatal_linode_access_token unless $?.to_i == 0
    load unless postpone_load
  end

  def down idx, skip_prompt = false
    @http && @http || load
    [@http, @https].each do |config|
      puts %x{linode-cli nodebalancers nodes-list #{@id} #{config.id}}
    end
    if skip_prompt || prompt("\nAre you certain that you want to take down the nodes at index #{idx}?", false)
      [@http, @https].each do |config|
        node = config.nodes.find { |n| n.number == idx }
        raise "No matching node for idx #{idx}" if node.nil?
        puts %Q{linode-cli nodebalancers node-update --mode reject #{@id} #{config.id} #{node.id}}
        puts %x{linode-cli nodebalancers node-update --mode reject #{@id} #{config.id} #{node.id}}
      end
    else
      exit 11
    end
  end

  def up idx
    @http && @http || load
    [@http, @https].each do |config|
      node = config.nodes.find { |n| n.number == idx }
      raise "No matching node for idx #{idx}" if node.nil?
      puts %x{linode-cli nodebalancers node-update --mode accept #{@id} #{config.id} #{node.id}}
    end
  end

  def nodes reload=false
    load if reload
    [@http, @https]
  end

  def count_reject
    nodes(true) if nodes.all?(&:nil?)
    nodes.map do |scheme|
      scheme.nodes.count { |n| n.mode != 'accept' }
    end
  end

private

  def load
    # Load nodebalancer
    json = %x{linode-cli --json nodebalancers list}
    nodebalancers = JSON.parse(json)
    raise "Only one nodebalancer expected. Found #{nodebalancers.length}" unless nodebalancers.length == 1
    @data = OpenStruct.new(nodebalancers.first)
    @id = @data.id
    # Load configs
    json = %x{linode-cli --json nodebalancers configs-list #{@id}}
    configs = JSON.parse(json)
    # Load nodes
    @http = load_nodes configs, 'http'
    @https = load_nodes configs, 'https'
  end

  def load_nodes configs, protocol
    config = OpenStruct.new(configs.find { |c| c['protocol'] == protocol })
    raise "No #{protocol} config found" if config.nil?
    json = %x{linode-cli --json nodebalancers nodes-list #{@id} #{config.id}}
    config.nodes = JSON.parse(json).map { |obj| Node.new obj }
    raise "Expected #{N_NODES_ON_NODEBALANCER} nodes but got #{config.nodes.length}" unless config.nodes.length == N_NODES_ON_NODEBALANCER
    config
  end
  
  def view_node config_id, node_id
    json = %x{linode-cli --json nodebalancers node-view #{@id} #{config_id} #{node_id}}
    Node.new JSON.parse(json).first
  end
end

# E.g. <OpenStruct status="UP", weight=100, label="app-1", mode="accept", address="192.168.132.150:80", id=881336>
class Node < OpenStruct
  # Get the server's index (1,2,3...) based on the label assigned it on the node balancer
  def number
    txt = label.scan(/\d+$/).first
    raise "Bad label for node: #{label}. It needs to end in a numeric identifier which is unique (for its Configuration). Fix this using the Linode web interface" if txt.nil?
    txt.to_i
  end
end
