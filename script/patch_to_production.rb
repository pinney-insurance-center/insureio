#!/usr/bin/env ruby

require_relative 'patch_helper'
include ScriptUtil

SSH_USR_SERVERS = ['deploy@insureio.one:4000','deploy@insureio.two:4000','deploy@insureio.three:4000']
REMOTE_PATH = '/var/www/dataraptor/current/'


INTERACTIVE = ARGV.index('-i')
ARGV.delete_at(INTERACTIVE) if INTERACTIVE

patcher = Patcher.new(SSH_USR_SERVERS, REMOTE_PATH)

def select_server
  $stdout.write 'Select server 1,2,3: '
  getch.to_i - 1
end

if INTERACTIVE
  while true
    puts "\nu[p] / d[own] / h[ealth] / s[tat] / q[uit]"
    $stdout.write 'Cmd (e.g. `dn 1`): '
    cmd = getch
    if cmd == 'q'
      break
    elsif cmd == 'h'
      patcher.check_server_health SSH_USR_SERVERS[select_server], true
    elsif cmd == 's'
      patcher.status
    elsif cmd == 'u'
      idx = select_server
      patcher.server_up! idx, SSH_USR_SERVERS[idx]
    elsif cmd == 'd'
      idx = select_server
      patcher.server_down! idx, SSH_USR_SERVERS[idx]
    end
  end
else
  patcher.run
end
