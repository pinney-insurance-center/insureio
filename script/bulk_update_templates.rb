#This script assumes html files named according to the ids of existing templates,
#like "status_emails/1234.html".
Dir.glob("#{Rails.root}/status_emails/*.html").each do |fname|
  t_id=fname.match(/(\d+).html/)[1].to_i
  new_body=File.read(fname)
  template=Marketing::Email::Template.find(t_id)
  template.update_attributes(body:new_body)
end
