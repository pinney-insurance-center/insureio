@echo off

for /f "delims== tokens=2" %%a in ('findstr keyfile= admin.config') do set keyfile=%%a
for /f "delims== tokens=2" %%a in ('findstr username= admin.config') do set username=%%a
for /f "delims== tokens=2" %%a in ('findstr plink= admin.config') do set plink=%%a
echo Using keyfile %keyfile%
echo Using username %username%
echo Using plink at %plink%
