@echo off

call load_config.bat
set /p id="Enter comma-separated user ids to set user(s) free: "
call run_ssh.bat "User.where(id:[%id%]).update_all(membership_id: -2)"
pause
