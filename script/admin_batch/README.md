## Setup ##

You must have all of the following in a single directory (or at least have links to all of the following in a single directory):

- admin.config
- load_config.bat
- run_ssh.bat
- (optional admin .bat files)

You must modify your admin.config file so that it contains the correct values for your account and workstation. 

| Variable | Description |
| ---- | ---- |
| keyfile | The path to your PuTTY private key file. If your private key is in the same directory as your admin scripts, the filename is a sufficient path. |
| username | The username under which you SSH to the application server. |
| plink | The path to your plink executable file. If your executable is in the same directory as your admin scripts, the filename is a sufficient path. |

## Usage ##

Double-click whichever script you wish to run and follow the instructions that appear.

NB: You should not run load_config.bat or run_ssh.bat. These files are called by other batch scripts.

This will execute plink to SSH to the Insureio application server with a Ruby instruction wrapped in `rails runner`.
