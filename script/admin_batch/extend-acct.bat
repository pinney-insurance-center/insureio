@echo off

call load_config.bat
set /p id="Enter comma-separated user ids to set user(s) trials to end 30 days from now: "
call run_ssh.bat "User.where(id:[%id%]).update_all(trial_membership_expires:Date.today+30.days)"
pause
