#!/usr/bin/env ruby

# Usage
# bundle exec cap production nb:up
# bundle exec cap production nb:dn

require_relative 'linode_helper'

class Interaction
  def initialize
    @nb = LinodeNodeBalancer.new(true)
  end

  def loop
    int = Interaction.new
    while true
      puts
      puts %w[Quit Up Down Status].each_with_index.map { |v, i| "#{i}. #{v}" }.join("\n")
      ret = prompt("(default 0):").to_i
      case ret
      when 0; exit
      when 1; @nb.up choose_node
      when 2; @nb.down choose_node
      when 3; print_status
      end
    end
  end

private

  def prompt msg
    $stdout.write "#{msg} "
    gets
  end

  def print_status
    puts "Loading nodes' status..."
    @http, @https = @nb.nodes(true)
    puts "HTTP config"
    @http.nodes.each do |node|
      puts "%1d %20s %7s %4s" % [node.number, node.label, node.mode, node.status]
    end
    puts "HTTPS config"
    @https.nodes.each do |node|
      puts "%1d %20s %7s %4s" % [node.number, node.label, node.mode, node.status]
    end
  end

  def choose_node
    print_status
    prompt("Which node? ").to_i
  end
end

def prompt_yes msg
  ret = prompt "#{msg} [Y/n]"
end

Interaction.new.loop
