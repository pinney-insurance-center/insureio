us=User.excluding_recruits.preload(:emails).select([:id,:primary_email_id,:full_name,:membership_id,:payment_due]);
File.open("users_#{Date.today.iso8601}.csv",'w') do |f|
  f.puts "id,full_name,membership_id,payment_due,primary_email"
  us.each{|u| f.puts "#{u.id},#{u.full_name},#{u[:membership_id]},#{u[:payment_due]},#{u.primary_email.try(:value) }" }
end
