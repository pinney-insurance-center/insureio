#!/usr/bin/env ruby

require_relative 'patch_helper'

SSH_USR_SERVER = 'deploy@insureio.qarelease'
REMOTE_PATH ='/var/www/release.insureio/current/'
GEMSET = 'ruby-2.3.0@release.insureio'

Patcher.new(SSH_USR_SERVER, REMOTE_PATH, env_name: :release, default_branch: :release, gemset: GEMSET).run
