#!/bin/bash

USER=${1:-deploy}
HOST=${2:-insureio.one}
PORT=${3:-4000}
BRANCH=${4:-master}
TMPFILE=$(mktemp)

# Read and throw away characters (which should be printable text if present at
# all) until the start of the header for gzipped content. This is useful
# because the server may begin the session by printing "There were ... failed
# login attempts since the last successful login" (which can be a useful
# message at other times).
skip_non_gzipped_content () {
	local HEADER=$'\x1f\x8b'
	ID1=$'\x0'
	while : ; do
		read -N 1 ID2
		if [[ "$ID1$ID2" == $HEADER ]]; then
			break
		else
			ID1=$ID2
		fi
	done
	printf $HEADER
	cat
}

cat <<-EOF | ssh -q -T -p "$PORT" "$USER@$HOST" | skip_non_gzipped_content | gunzip > "$TMPFILE"
	CONF=/var/www/dataraptor/current/config/database.yml
	extract () {
		sed '/^production:/p;/^[[:space:]]*$/q' "\$CONF" | grep "\$1" | awk '{printf \$2}' | sed -e 's/^"//' -e 's/"$//'
	}
	mysql -P "\$(extract port)" -h "\$(extract host)" -u "\$(extract username)" "-p\$(extract password)" "\$(extract database)" -N -B -e 'select * from schema_migrations' | gzip
EOF

migrations_on_branch () {
	git ls-tree -r --name-only "$BRANCH" -- db/migrate
}

pending_migration_timestamps () {
	migrations_on_branch | xargs basename --multiple | cut -d_ -f1 | diff --new-line-format="" --unchanged-line-format="" -w -B - "$TMPFILE"
}

N_PENDING_MIGRATIONS=$(pending_migration_timestamps | wc -l)
if (( $N_PENDING_MIGRATIONS )); then
	echo "Pending migrations:"
	pending_migration_timestamps | while read stamp; do
		echo stamp $stamp
		find db/migrate -name ${stamp}\* #| xargs basename
	done
	exit 1
fi

rm "$TMPFILE"
