#!/usr/bin/env ruby
require 'yaml'#for reading config
require 'json'#for posting to webhook
require_relative 'script_util'


CHAT_CONFIG     =YAML::load_file 'config/data_sensitive_or_machine_specific/chat.yml'
CHAT_URL        =CHAT_CONFIG['deploy_or_patch_webhook_url']


JSON_HEADER     ='-H "Content-Type: application/json"'
TMP_FILE = '/tmp/rocket.chat.patch.msg.tmp'

module RocketChat
  class Notifier
    extend ScriptUtil

    def initialize env_name, opts = {}
      @env_name = env_name
      @verb = opts.fetch :verb, '_is starting_'
      @patch_user = `whoami`.strip.capitalize
      @msg = opts.fetch :msg, ["#{@patch_user} #{@verb} a new **patch** to **#{env_name}** environment.",
        `git log -1 --pretty='format:%B'`].join("\n")
    end

    def notify opts = {}
      interactive = opts.fetch :interactive, true
      if interactive
        return unless prompt_message
      end
      return if @msg.strip.length == 0
      @msg.sub!(@verb, opts[:verb]) if opts.has_key? :verb
      puts
      puts 'Posting notification to #develop and #insureio chatrooms.'
      File.write(TMP_FILE, { text: @msg }.to_json)
      puts
      puts %Q{curl #{JSON_HEADER} -X POST -d "@#{TMP_FILE}" #{CHAT_URL}}
      webhook_curl_output=`curl #{JSON_HEADER} -X POST -d "@#{TMP_FILE}" #{CHAT_URL}`
      puts webhook_curl_output  
    end

    private

    def prompt_message
      puts wrap_text "\033[34mDefault message for chat is:\033[0m"
      puts wrap_text @msg, hanging_indent: '  '
      $stdout.write "\033[33mUse default message? (Y/n/q) \033[0m"
      choice = getch; puts
      if choice == 'q'
        exit
      elsif choice == 's'
        @msg = ''
      elsif choice =~ /n/i
        @msg = edit_message @msg
      else
        @msg = @msg
      end
    end
  end
end

if File.basename(__FILE__) == File.basename($0)
  msg = $stdin.read
  notifier = RocketChat::Notifier.new 'production', msg: msg
  notifier.notify interactive: false
end
