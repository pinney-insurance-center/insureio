module ScriptUtil

  def bell opts = {}
    n = opts.fetch :n, 1
    delay = opts.fetch :sec, 0.5
    raw_shell { system 'tput bel' }
    (2..n).each do
      sleep delay
      raw_shell { system 'tput bel '}
    end
  end

  # Open a text editor, populate it with the default message, let the user
  # edit and save the message
  def edit_message msg
    tmp_file = open TMP_FILE, 'w'
    tmp_file.puts wrap_text '# Please provide a message. Lines that begin with "#" are treated as comments'
    tmp_file.puts msg
    tmp_file.close
    system "</dev/tty #{RocketChat.get_editor} #{TMP_FILE}"
    File.read(TMP_FILE).sub(/^#.*(\n|$)/, '').strip
  end

  def get_editor
    git_editor = `git config core.editor`.strip
    git_editor.empty? ? 'vim' : git_editor
  end

  def getch
    got = raw_shell { STDIN.getc }
    puts
    got
  end
    
  def prompt msg, default_yes=false
    if default_yes
      $stdout.write wrap_text "#{msg} [Y/n] "
      not (getch =~ /n|no/i)
    else
      $stdout.write wrap_text "#{msg} [y/N] "
      getch =~ /y|yes/i
    end
  end

  # todo handle non-visible chars (ansi codes)
  # @arg text is a single line of text
  def wrap_text text, opts={}
    hard_limit = opts.fetch :hard_limit, 120
    limit = [hard_limit, `tput cols`.to_i].min
    limit = hard_limit if limit == 0
    lines = []
    while text
      indent = if lines.length == 0
        opts[:first_indent] || opts.fetch(:indent, '')
      else
        opts[:hanging_indent] || opts.fetch(:indent, '')
      end
      raise "Illegal indent length #{indent.length} for limit #{limit}" unless limit > indent.length
      current_limit = limit - indent.length
      if text.length <= current_limit
        break lines << indent + text
      else
        i = text[0..current_limit].rindex(/\s/)
        i = current_limit if i.nil?
        lines << (indent + text[0..i])
        text = text[i+1..-1]
      end
    end
    lines.join("\n")
  end

  private

  def raw_shell
    begin
      system("stty raw -echo")
      yield
    ensure
      system("stty -raw echo")
    end
  end
end
