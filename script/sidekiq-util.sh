#!/bin/bash

DIR=$(dirname $(dirname `readlink -f $BASH_SOURCE`))

# This logic for determining TAG is similar to the logic used by Sidekiq to determine the default tag
IFS=/ read -a DIRS <<< "$DIR"
if [[ ${DIRS[-1]} =~ ^[0-9] ]] && [[ ${DIRS[-2]} == releases ]]; then
	TAG=${DIRS[-3]}
fi
[[ -z $TAG ]] && TAG=${DIRS[-1]}
[[ -z $TAG ]] && TAG=`hostname`

# List current sidekiq processes
function ls_ps () {
	ps x --sort start_time --format pid,lstart,cmd | grep -P 'sidekiq [.0-9]+ '"$TAG"' \[\d+ of \d+ busy\]$'
}

# Kill all but the most recent sidekiq process
function keep_newest_ps () {
	# Process all _but_ the last line
	ls_ps | head -n -1 | awk '{print $1}' | while read pid; do
		echo $pid > tmp/old-sidekiq-pid
		bundle exec sidekiqctl stop tmp/old-sidekiq-pid
	done
}

function kill () {
	ls_ps | awk '{print $1}' | while read pid; do
		echo $pid > tmp/old-sidekiq-pid
		bundle exec sidekiqctl stop tmp/old-sidekiq-pid
	done
}

# Unique process index on this machine
function machine_index () {
	hostname | grep -oP '\d+$' || echo 127 # fallback to a dummy value
}

function start () {
	rails_env=${1:-production}
	i=${2:-$(machine_index)}
	cd "$DIR"
	bundle exec sidekiq \
		--environment $rails_env \
		--tag "$TAG" \
		--daemon \
		--logfile log/sidekiq.log \
		--config config/data/sidekiq.yml \
		--pidfile tmp/pids/sidekiq.pid \
		--index $i \
		--concurrency 15 \
		--verbose
}

case $1 in
	ls) ls_ps;;
	kill) kill;;
	start) start "${@:2}";;
	restart) kill && start "${@:2}";;
	clean) keep_newest_ps;;
esac
