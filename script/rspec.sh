#!/bin/bash

# e.g.
# ./script/rspec.sh --seed 21055

# This allows for easy setting of `seed` or other rspec command-line options
extra_args () {
	cat # stdin
	echo "${@}"
}

# Run only the Rspec tests specified in Jenkinsfile
cat Jenkins/Jenkinsfile \
| sed -n '/bundle exec rspec/,/"/p' \
| sed -e "s/sh \"PATH=.* \$rvms/\"/" -e "s/'\|\"//g" \
| extra_args "${@}" \
| bash
