# Setup

## Install dependencies
```bash
# Install system packages
sudo apt install -y libcurl4-gnutls-dev # Or some package that includes libcurl
sudo apt install -y libmysqlclient-dev mysql-client
sudo apt install -y graphviz
sudo apt install -y nodejs npm

# Install Ruby (using rbenv, but you can use rvm or whatever you want, really)
rbenv install 2.3.0 # Check Gemfile to ensure version choice
rbenv global 2.3.0 # This could be local or be set with a .ruby-version file or a RUBY_VERSION env variable or other ways

# Install gems
gem install bundler --version 1.9 # Check Gemfile.lock to ensure version choice
bundle
```

## Mysql (MariaDB) server
```bash
sudo apt install mariadb-server
sudo mysql --defaults-file= <<< "create user 'dev'@'localhost' identified by 'password';"
sudo mysql --defaults-file= <<< "grant all on *.* to 'dev'@'localhost';"
```

## Config files
Several config files populate values from environment variables. In future, most or all of the contents of directory "config/data_sensitive_or_machine_specific config" may be moved to environment variables. On the production app servers, those are set in "/home/deploy/.bash_profile". When adding or removing essential config data, please update the validation in "load_config.rb".
```bash
scp -r app.insureio.qarelease:/var/www/release.insureio/current/config/data_sensitive_or_machine_specific config/
cp config/database.yml.template config/database.yml
sed -i 's/root/dev/' config/database.yml
```

## Database
```bash
bundle exec rake db:create db:schema:load db:migrate db:seed
# Here's a kludge to make certain fields unsigned integers because the db:schema:load task doesn't appear to honour unsigned options.
# (This assumes you've set up a $HOME/.my.cnf file so that you don't have to enter your username, password, and database every time.)
bundle exec rake db:schema:load db:migrate db:fix db:seed
RAILS_ENV=test bundle exec rake db:schema:load db:migrate db:fix db:seed
    mysql dr_test <<< "ALTER TABLE users MODIFY permissions_$i INT UNSIGNED;"
```

## Assets
```bash
npm install
alias bower=node_modules/bower/bin/bower
bower install
# Here's a kludge for jquery, which appears to be unpacked differently on the servers
JQUERY_PATH=vendor/assets/bower_components/jquery/dist
if ! [[ -e $JQUERY_PATH ]]; then
    mkdir -p $JQUERY_PATH
    ln vendor/assets/bower_components/jquery/jquery.js $JQUERY_PATH
    ln vendor/assets/bower_components/jquery/jquery.min.js $JQUERY_PATH
fi
```

## Hosts management
Either update your `/etc/hosts` file or setup `puma-dev`.
The former is easier, but the latter will handle port forwarding and https for you.

You want subdomains whose bottom-level domain matches the tenants in `public/enums.json`'s `Tenant` structure.
Do so for your local dev machine and for the QA server.
Here's an example amendment to the `/etc/hosts` file:

```
127.0.1.1     app.insureio.local pinney.insureio.local amg.insureio.local bsb.insureio.local ezlife.insureio.local termteam.insureio.local ebi.insureio.local m2.insureio.local orgcorp.insureio.local
127.0.1.1     foo_hamilton.insurancedivision.local ryan.insurancedivision.local maurice.insurancedivision.local pinney_brokerage.insurancedivision.local null.insurancedivision.local insurancedivision.local
```

## Start
```bash
bundle exec rails server
```

# Running tests

## Testing Angular.js
```bash
npm test
```

Or for more control:
```bash
alias karma=node_modules/karma/bin/karma
karma start config/karma.conf.js --single-run
```

Or to run specific tests, start the karma server and execute `run` with a `--grep`:
```bash
alias karma=node_modules/karma/bin/karma
karma start --no-auto-watch &
karma run -- --grep 'when the mime type is bad'
```

# Branching model
We adhere for the most part to the strategy described here: https://nvie.com/posts/a-successful-git-branching-model/

| Issue Type | Branch Name Pattern | Branch Parent | Merged Into | Notes |
| --- | --- | --- | --- | --- |
| documentation | doc/short-issue-desc | master | all | |
| hot fix | fix/issue-num-short-issue-desc | master | all | patched in when ready |
| cold fix | fix/issue-num-short-issue-desc | master | all | deployed with biweekly scheduled deploy |
| feature | feature/issue-num-short-issue-desc | release | develop, then release once OKed by project head |
| refactor | refactor/issue-num-short-issue-desc | release | develop, then release once OKed by project head |
| set of features, refactors | release | master | periodically merged into master |

| Major Branch Name | Environment File | Domain(s) | Notes |
| --- | --- | --- | --- |
| master | production | \*.insureio.com | User-facing. Fixes go in on a regular basis. Hot fixes are patched into release env if appropriate for quick smoke testing there, then patched into production. Cold fixes are deployed with the next biweekly scheduled deploy. |
| release | release | \*.insureio.qarelease | Intended for 2nd-round in-house user testing of features, refactors. Mostly stable. If a merged-in feature needs major reworking, or we want to move some features, refactors from this branch into production and leave a less finished feature behind, this branch may be rebuilt from master and the finished feature, refactor branches merged back into it. |
| develop | development | \*.insureio.qadev | Intended for 1st-round in-house user testing of features, refactors. The major branch with highest churn. May need to be rebuilt off of master from time to time. |
| design_testing | design_testing | \*.insureio.qadesign | Intended for isolated user testing of design-exclusive changes. |

Our qa environments are currently both hosted on the same cloud based machine, with no public DNS mapping both in order to avoid the expense and for whatever small security advantage its obscurity might add.

# Patch procedure

## Configuration
1. Ensure that your local `config/data_sensitive_or_machine_specific/chat.yml` has key `deploy_or_patch_webhook_url` set.
2. Ensure that your machine's ssh key is copied to `/home/deploy/.ssh/authorized_keys` on all app servers.
3. Ensure that your machine's hosts file or `puma-dev` has mappings for `insureio.one`,`insureio.two`,`insureio.three` (refer to Linode dashboard for IPs).

## Steps
When a hotfix needs patching to production (after testing locally and preferably also in release):
1. Switch to the fix branch
2. Squash the fix into a single commit if it comprises multiple commits.
3. Run `.script/patch_to_production.rb`.
    - a. It will prompt you to confirm/change the name of the patch.
      By default it will create it will create a patch file from the commit named for the fix branch.
    - b. It will send and apply patches with feedback.
    - c. It will prompt you to enter any commands that need to be run on each server prior to restart.
      By default it will remove \*.orig and \*.rej files.
    - d. It will prompt you once per server to confirm before issuing commands and restarting.
      This is meant to allow you to complete the following steps, in order to avoid any service interruption for users.
        1. Take the specific app server offline via the node balancer dashboard gui.
        2. Trigger the restart.
        3. Confirm functionality of the specific server from your browser.
        4. Bring the specific server back online.
      This part of the procedure can be automated in future **now that Linode has an API for configuring node balancers**. They didn't have that feature when this script was written.
    - e. It opens `post_msg_to_chat.rb`, and prompts you to accept/modify the message before posting to developer and insureio chat rooms.
      By default, the message is composed from the latest commit message on your currently checked out branch.
4. If the current patch affects background job workers, tasks, or messages:
    - a. Run `cap production sidekiq:restart`
    - b. Navigate to `https://app.insureio.com/dashboard/background_jobs` and monitor for a few minutes to confirm that jobs continue to run as expected.

To patch a fix into release, follow the same steps, but with `.script/patch_to_release.rb`.

# Style guide

## Directory/File naming
Our file/directory naming conventions is to stick to snake case regardless of file type.
There are a few exceptions (camelCases, hyphenated, etc.), mostly created before this convention was established. When touching materials that violate this convention, please rename them.

For template files, the outermost template for a main section is generally named "container.html" versus the typical "index.html".

This convention was chosen so that we could do all 3 of the following:
- keep the "public" folder's structure roughly equivalent to the rails "app/controller" folder structure
- bypass the rails app to serve the template faster
- not bypass standard rails actions like `UsersController#index` which need to serve dynamic JSON data

For example, a file named "index.html" in "public/users/" would bypass the rails action `UsersController#index`. This could probably be solved with different Apache/Passenger/Rack config, but the currently employed file naming convention works just as well.

Templates intended as modals typically begin with "modal_for_" so they'll appear together in alpha-sorted views of the file structure.

## Code Formatting
For the sake of simplicity, we use 2-space indentation across ruby, javascript, and markup files.

For the sake of readability without horizontal scrolling across various IDEs and diff viewers, we try to cap lines around 120 characters in length where possible, including in ruby, javascript, markup, and commit messages.

To this end, markup is generally written with max one tag per line, often one attribute per line.
Aligning similar data (key/value pairs in a hash, multiple association/validation/callback definitions, etc.) is generally appreciated as it increases legibility.

For AngularJS modules, the convention going forward is PascalCase with an initial capital letter for module and controller names, with module names ending in "App" and controller names ending in "Ctrl".

A commonly used abbreviation in module and controller names is "mgmt" for management.

## HTML

### Record IDs

We tend to display 'short' ids in the front-end and include a tooltip to show the decimal id:

```html
<div class="col-md-7" uib-tooltip="ID: {{person.id}}">
  <span class="label label-default">{{person.id | drShortId}}</span>
</div>
```

## JS
Most of our AngularJS controllers start with a function called `$scope.init`, and end with a line appending that function as a callback to `$rootScope.getCurrentUser().$promise`.

That assures us that both the current user and all controller functions are available before the controller is initialized.

Most AngularJS modules in Insureio have module-level state data to keep track of. We had been creating objects like `$scope.brandCtrlVars` in the init function to hold this, but it seems preferable going forward to define and use a factory, as is done in `leadDistStateObj` for module `drLeadDistribution`, to set the state object apart enough that it is visible at a glance.

For requests that modify data, our pattern is to add a bulletin stating what is being modified, then replace it with either a success or failure bulletin after the request completes.

For requests that do not modify data, our pattern is to add a bulletin describing what is being loaded, then remove it upon success or replace it with a failure bulletin upon failure.
