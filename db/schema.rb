# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200805152901) do

  create_table "addresses", force: true do |t|
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "address_type_id"
    t.string   "street"
    t.string   "city"
    t.string   "zip"
    t.integer  "state_id"
    t.string   "county_name"
    t.boolean  "is_within_city_limits"
    t.string   "contactable_type",      limit: 20, null: false
    t.integer  "contactable_id",                   null: false
  end

  add_index "addresses", ["contactable_id", "contactable_type"], name: "index_addresses_on_contactable_id_and_contactable_type", using: :btree
  add_index "addresses", ["contactable_id"], name: "addresses_by_brand_cid", using: :btree
  add_index "addresses", ["contactable_id"], name: "addresses_by_consumer_cid", using: :btree
  add_index "addresses", ["contactable_id"], name: "addresses_by_user_cid", using: :btree
  add_index "addresses", ["street"], name: "index_addresses_on_street", using: :btree

  create_table "agency_works", force: true do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "agency_works_reqs", force: true do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ahoy_events", force: true do |t|
    t.uuid     "visit_id"
    t.integer  "user_id"
    t.string   "user_type"
    t.integer  "owner_id"
    t.integer  "brand_id"
    t.integer  "message_id"
    t.string   "name"
    t.text     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["time"], name: "index_ahoy_events_on_time", using: :btree
  add_index "ahoy_events", ["user_id"], name: "index_ahoy_events_on_user_id", using: :btree
  add_index "ahoy_events", ["visit_id"], name: "index_ahoy_events_on_visit_id", using: :btree

  create_table "attachments", force: true do |t|
    t.string   "file_name"
    t.integer  "person_id"
    t.string   "person_type"
    t.integer  "case_id"
    t.datetime "created_at",    null: false
    t.integer  "created_by_id"
    t.datetime "updated_at",    null: false
    t.integer  "updated_by_id"
    t.string   "description"
  end

  add_index "attachments", ["person_id", "person_type"], name: "index_attachments_on_person_id_and_person_type", using: :btree
  add_index "attachments", ["person_id"], name: "attachments_by_brand_pid", using: :btree
  add_index "attachments", ["person_id"], name: "attachments_by_consumer_pid", using: :btree
  add_index "attachments", ["person_id"], name: "attachments_by_user_pid", using: :btree

  create_table "brands", force: true do |t|
    t.integer  "owner_id"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.integer  "ownership_id"
    t.integer  "initial_status_type_id"
    t.boolean  "enabled",                                          default: true
    t.string   "full_name"
    t.string   "title"
    t.string   "company"
    t.integer  "preferred_contact_method_id"
    t.string   "preferred_contact_time"
    t.integer  "primary_address_id"
    t.integer  "primary_phone_id"
    t.integer  "primary_email_id"
    t.date     "birth_or_trust_date"
    t.boolean  "active_or_enabled",                                default: true
    t.integer  "entity_type_id",                         limit: 1, default: 1
    t.integer  "tenant_id",                                        default: 0
    t.string   "preferred_insurance_division_subdomain"
    t.boolean  "preferred_subdomain_set_already",                  default: false
  end

  add_index "brands", ["full_name"], name: "index_brands_on_full_name", using: :btree
  add_index "brands", ["id", "full_name"], name: "index_brands_on_id_and_full_name", using: :btree
  add_index "brands", ["owner_id", "full_name"], name: "index_brands_on_owner_id_and_full_name", using: :btree

  create_table "brands_tagging_tag_values", force: true do |t|
    t.integer "brand_id"
    t.integer "tag_value_id"
  end

  create_table "brands_users", force: true do |t|
    t.integer "brand_id"
    t.integer "user_id"
  end

  add_index "brands_users", ["user_id", "brand_id"], name: "index_usage_profiles_usage_users_on_user_id_and_profile_id", unique: true, using: :btree

  create_table "carriers", force: true do |t|
    t.integer  "naic_code"
    t.string   "compulife_code"
    t.string   "name"
    t.text     "description"
    t.integer  "pinney_rating"
    t.string   "am_best"
    t.string   "standard_poors"
    t.string   "moodys"
    t.string   "fitch"
    t.integer  "sprite_offset"
    t.datetime "updated_at"
    t.boolean  "quoting_on",     default: true
    t.boolean  "existing_on",    default: true
    t.boolean  "replacing_on",   default: true
    t.integer  "ixn_id"
    t.boolean  "deleted",        default: false
  end

  add_index "carriers", ["id", "name"], name: "index_carriers_on_id_and_name", using: :btree

  create_table "consumers", force: true do |t|
    t.integer  "agent_id"
    t.integer  "birth_state_id"
    t.string   "birth_country_custom_name"
    t.integer  "citizenship_id"
    t.string   "citizenship_noncitizen_registration_number"
    t.date     "dl_expiration"
    t.integer  "dl_state_id"
    t.string   "encrypted_dln"
    t.boolean  "email_send_failed"
    t.string   "ip_address"
    t.string   "quoter_url"
    t.integer  "marital_status_id"
    t.text     "note"
    t.boolean  "actively_employed"
    t.string   "occupation"
    t.string   "occupation_description"
    t.integer  "brand_id"
    t.integer  "product_type_id"
    t.string   "suffix"
    t.float    "years_at_address"
    t.datetime "created_at",                                                                                   null: false
    t.datetime "updated_at",                                                                                   null: false
    t.integer  "health_info_id"
    t.integer  "financial_info_id"
    t.date     "anniversary"
    t.string   "relationship_to_agent"
    t.date     "relationship_to_agent_start"
    t.boolean  "completed_discovery"
    t.boolean  "completed_financial"
    t.boolean  "completed_underwriting"
    t.boolean  "health_completed",                                                             default: false
    t.boolean  "personal_completed",                                                           default: false
    t.boolean  "marketing_unsubscribe",                                                        default: false
    t.boolean  "completed_basic"
    t.boolean  "completed_compare"
    t.boolean  "completed_application"
    t.string   "case_manager_name"
    t.text     "tags"
    t.string   "lead_type"
    t.string   "referrer"
    t.string   "source"
    t.string   "physician_name"
    t.boolean  "test_lead",                                                                    default: false
    t.integer  "product_cat_id"
    t.integer  "primary_contact_relationship_id"
    t.boolean  "status_unsubscribe"
    t.boolean  "recently_applied_for_life_ins"
    t.decimal  "total_life_coverage_in_force",                         precision: 8, scale: 2
    t.integer  "last_applied_for_life_ins_outcome_id"
    t.datetime "last_contacted"
    t.date     "last_applied_for_life_ins_date"
    t.string   "encrypted_eft_account_num"
    t.string   "encrypted_eft_account_num_iv"
    t.string   "encrypted_eft_routing_num"
    t.string   "encrypted_eft_routing_num_iv"
    t.string   "encrypted_eft_bank_name"
    t.string   "encrypted_eft_bank_name_iv"
    t.string   "full_name"
    t.string   "title"
    t.string   "company"
    t.integer  "preferred_contact_method_id"
    t.string   "preferred_contact_time"
    t.integer  "primary_address_id"
    t.integer  "primary_phone_id"
    t.integer  "primary_email_id"
    t.date     "birth_or_trust_date"
    t.boolean  "active_or_enabled",                                                            default: true
    t.integer  "entity_type_id",                             limit: 1,                         default: 1
    t.integer  "tenant_id",                                                                    default: 0
    t.boolean  "gender"
    t.string   "trustee_name"
    t.string   "encrypted_ssn"
    t.integer  "unscoped_attachments_count"
    t.integer  "attachments_count"
    t.integer  "recordings_count"
    t.integer  "opportunities_count"
    t.integer  "pending_policies_count"
    t.integer  "placed_policies_count"
    t.integer  "old_tasks_count"
    t.integer  "birth_country_id"
    t.integer  "relationships_w_self_as_primary_count",                                        default: 0,     null: false
    t.integer  "relationships_w_self_as_related_count",                                        default: 0,     null: false
    t.integer  "primary_contact_id"
  end

  add_index "consumers", ["agent_id"], name: "index_crm_connections_on_agent_id", using: :btree
  add_index "consumers", ["brand_id", "agent_id", "full_name"], name: "index_consumers_on_brand_id_and_agent_id_and_full_name", using: :btree
  add_index "consumers", ["full_name"], name: "index_consumers_on_full_name", using: :btree
  add_index "consumers", ["id", "full_name"], name: "index_consumers_on_id_and_full_name", using: :btree

  create_table "crm_case_requirements", force: true do |t|
    t.string   "name"
    t.string   "note"
    t.integer  "case_id"
    t.integer  "requirement_type_id"
    t.integer  "responsible_party_type_id"
    t.integer  "created_by_user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.date     "ordered_at"
    t.date     "completed_at"
    t.integer  "status_id",                 default: 1
  end

  create_table "crm_cases", force: true do |t|
    t.boolean  "active",                                                                  default: true
    t.integer  "agent_id"
    t.float    "approved_premium_due"
    t.boolean  "bind"
    t.integer  "consumer_id"
    t.boolean  "cross_sell"
    t.boolean  "equal_share_contingent_bens"
    t.boolean  "equal_share_primary_bens"
    t.string   "exam_company"
    t.string   "exam_num"
    t.string   "exam_status"
    t.text     "exam_status_detail"
    t.datetime "exam_time"
    t.boolean  "is_a_preexisting_policy"
    t.boolean  "ipo"
    t.string   "policy_number"
    t.date     "policy_period_expiration"
    t.integer  "submitted_qualified"
    t.boolean  "underwriter_assist"
    t.integer  "up_sell"
    t.datetime "created_at",                                                                              null: false
    t.datetime "updated_at",                                                                              null: false
    t.date     "effective_date"
    t.date     "termination_date"
    t.string   "reason"
    t.boolean  "esign",                                                                   default: true
    t.integer  "replaced_by_id"
    t.boolean  "financing"
    t.integer  "product_type_id"
    t.boolean  "packet"
    t.integer  "face_amount"
    t.datetime "quoted_at"
    t.boolean  "replacing"
    t.string   "xrae_case_id"
    t.date     "sent_to_igo"
    t.integer  "agency_works_id"
    t.date     "sent_to_agency_works_at"
    t.integer  "apps_ag_id_offset"
    t.string   "apps_order_id"
    t.date     "sent_103_to_apps_at"
    t.date     "sent_121_to_apps_at"
    t.string   "scor_guid"
    t.datetime "date_signed_by_consumer"
    t.datetime "date_signed_by_owner"
    t.datetime "date_signed_by_payer"
    t.datetime "date_processing_started_with_scor"
    t.integer  "stage",                                                                   default: 0
    t.integer  "ezl_id"
    t.integer  "agent_of_record_id"
    t.integer  "purpose_id"
    t.integer  "purpose_type_id"
    t.boolean  "collateral_assignment",                                                   default: false
    t.boolean  "business_insurance",                                                      default: false
    t.integer  "sales_stage_id",                                                          default: 1,     null: false
    t.integer  "has_rider_child",                       limit: 1,                         default: 0
    t.boolean  "has_rider_waiver_of_premium",                                             default: false
    t.boolean  "has_rider_disability_income",                                             default: false
    t.boolean  "has_rider_critical_or_chronic_illness",                                   default: false
    t.boolean  "has_rider_ltc",                                                           default: false
    t.boolean  "has_rider_ad_and_d",                                                      default: false
    t.integer  "status_type_category_id",               limit: 1
    t.datetime "entered_stage_contacted"
    t.datetime "entered_stage_app_fulfillment"
    t.datetime "entered_stage_submitted_to_carrier"
    t.boolean  "is_test",                                                                 default: false
    t.boolean  "is_duplicate",                                                            default: false
    t.string   "collateral_assignment_note"
    t.integer  "rider_disability_income_amount"
    t.integer  "rider_ltc_amount"
    t.integer  "rider_ad_and_d_amount"
    t.datetime "entered_stage_placed"
    t.decimal  "conditional_binding_receipt_amount",              precision: 2, scale: 0
    t.text     "ext"
    t.boolean  "autoloan_premium"
    t.integer  "benes_type_id",                         limit: 1
    t.integer  "bill_type_id",                          limit: 1
    t.integer  "dividend_type_id",                      limit: 1
  end

  add_index "crm_cases", ["agency_works_id"], name: "index_crm_cases_on_agency_works_id", using: :btree
  add_index "crm_cases", ["agent_id"], name: "index_crm_cases_on_agent_id", using: :btree
  add_index "crm_cases", ["apps_ag_id_offset"], name: "index_crm_cases_on_apps_id_offset", using: :btree
  add_index "crm_cases", ["consumer_id"], name: "index_crm_cases_on_consumer_id", using: :btree
  add_index "crm_cases", ["policy_number"], name: "index_crm_cases_on_policy_number", using: :btree

  create_table "crm_connection_relationships", force: true do |t|
    t.integer "connection_a_id"
    t.integer "connection_b_id"
    t.integer "b_is_to_a_id"
    t.string  "note"
  end

  add_index "crm_connection_relationships", ["connection_a_id"], name: "index_crm_connection_relationships_on_connection_a_id", using: :btree
  add_index "crm_connection_relationships", ["connection_b_id"], name: "index_crm_connection_relationships_on_connection_b_id", using: :btree

  create_table "crm_consumer_relationships", force: true do |t|
    t.integer  "primary_insured_id",                             null: false
    t.integer  "stakeholder_id",                                 null: false
    t.integer  "relationship_type_id",                           null: false
    t.integer  "crm_case_id"
    t.boolean  "is_owner",                       default: false
    t.boolean  "is_payer",                       default: false
    t.boolean  "is_beneficiary",                 default: false
    t.integer  "percentage",           limit: 1
    t.boolean  "contingent",                     default: false
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "crm_consumer_relationships", ["crm_case_id"], name: "consumer_rels_on_case", using: :btree
  add_index "crm_consumer_relationships", ["primary_insured_id"], name: "consumer_rels_on_insured", using: :btree
  add_index "crm_consumer_relationships", ["stakeholder_id", "crm_case_id"], name: "consumer_rels_on_stakeholder_and_case", unique: true, using: :btree
  add_index "crm_consumer_relationships", ["stakeholder_id"], name: "consumer_rels_on_stakeholder", using: :btree

  create_table "crm_financial_infos", force: true do |t|
    t.integer  "asset_home_equity"
    t.integer  "asset_investments"
    t.integer  "asset_pension"
    t.integer  "asset_real_estate"
    t.integer  "asset_savings"
    t.integer  "liability_auto"
    t.integer  "liability_credit"
    t.integer  "liability_education"
    t.integer  "liability_other"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.boolean  "bankruptcy"
    t.date     "bankruptcy_declared"
    t.date     "bankruptcy_discharged"
    t.integer  "asset_checking"
    t.integer  "asset_earned_income"
    t.integer  "asset_life_insurance"
    t.integer  "asset_retirement"
    t.integer  "asset_unearned_income"
    t.float    "assumed_interest_rate"
    t.integer  "liability_final_expense"
    t.integer  "liability_heloc"
    t.integer  "liability_mortgage_1"
    t.integer  "liability_mortgage_2"
    t.integer  "liability_personal_loans"
    t.integer  "liability_student_loans"
    t.integer  "liability_years_income_needed"
    t.integer  "spouse_income"
    t.date     "spouse_retirement"
    t.float    "spouse_marginal_tax_rate"
    t.float    "assumed_inflation_rate"
    t.integer  "net_worth_specified"
    t.boolean  "net_worth_use_specified",                                default: true
    t.decimal  "annual_income",                 precision: 10, scale: 2
    t.decimal  "assets",                        precision: 10, scale: 2
    t.decimal  "liabilities",                   precision: 10, scale: 2
    t.boolean  "has_coverage"
  end

  create_table "crm_health_infos", force: true do |t|
    t.integer  "feet"
    t.integer  "inches"
    t.integer  "weight"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "bp"
    t.text     "bp_details"
    t.boolean  "cholesterol"
    t.text     "cholesterol_details"
    t.boolean  "criminal"
    t.text     "criminal_details"
    t.boolean  "hazardous_avocation"
    t.text     "hazardous_avocation_details"
    t.boolean  "moving_violation"
    t.text     "moving_violation_details"
    t.boolean  "foreign_travel"
    t.text     "foreign_travel_details"
    t.boolean  "tobacco"
    t.text     "tobacco_details"
    t.boolean  "alcohol_abuse"
    t.text     "alcohol_abuse_details"
    t.boolean  "anxiety"
    t.text     "anxiety_details"
    t.boolean  "arthritis"
    t.text     "arthritis_details"
    t.boolean  "asthma"
    t.text     "asthma_details"
    t.boolean  "atrial_fibrillation"
    t.text     "atrial_fibrillation_details"
    t.boolean  "cancer_breast"
    t.text     "cancer_breast_details"
    t.boolean  "cancer_prostate"
    t.text     "cancer_prostate_details"
    t.boolean  "copd"
    t.text     "copd_details"
    t.boolean  "crohns"
    t.text     "crohns_details"
    t.boolean  "depression"
    t.text     "depression_details"
    t.boolean  "diabetes_1"
    t.text     "diabetes_1_details"
    t.boolean  "diabetes_2"
    t.text     "diabetes_2_details"
    t.boolean  "drug_abuse"
    t.text     "drug_abuse_details"
    t.boolean  "epilepsy"
    t.text     "epilepsy_details"
    t.boolean  "heart_murmur"
    t.text     "heart_murmur_details"
    t.boolean  "hepatitis_c"
    t.text     "hepatitis_c_details"
    t.boolean  "irregular_heartbeat"
    t.text     "irregular_heartbeat_details"
    t.boolean  "elevated_liver_function"
    t.text     "elevated_liver_function_details"
    t.boolean  "multiple_sclerosis"
    t.text     "multiple_sclerosis_details"
    t.boolean  "parkinsons"
    t.text     "parkinsons_details"
    t.boolean  "sleep_apnea"
    t.text     "sleep_apnea_details"
    t.boolean  "stroke"
    t.text     "stroke_details"
    t.boolean  "weight_reduction"
    t.text     "weight_reduction_details"
    t.boolean  "mental_illness"
    t.text     "mental_illness_details"
    t.boolean  "emphysema"
    t.text     "emphysema_details"
    t.boolean  "ulcerative_colitis_iletis"
    t.text     "ulcerative_colitis_iletis_details"
    t.boolean  "cancer_skin"
    t.text     "cancer_skin_details"
    t.boolean  "cancer_internal"
    t.text     "cancer_internal_details"
    t.boolean  "heart_attack"
    t.text     "heart_attack_details"
    t.boolean  "vascular_disease"
    t.text     "vascular_disease_details"
    t.text     "diseases"
    t.text     "relatives_diseases"
    t.text     "moving_violations"
    t.text     "crimes"
  end

  create_table "crm_lead_types", force: true do |t|
    t.string "text"
  end

  create_table "crm_lead_types_brands", force: true do |t|
    t.integer "lead_type_id"
    t.integer "brand_id"
  end

  add_index "crm_lead_types_brands", ["lead_type_id", "brand_id"], name: "uniqueness_index_crm_lead_types_usage_profiles", unique: true, using: :btree

  create_table "crm_physician_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "crm_physicians", force: true do |t|
    t.string   "address"
    t.string   "findings"
    t.date     "last_seen"
    t.string   "name"
    t.string   "phone"
    t.string   "reason"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "physician_type_id"
    t.integer  "years_of_service"
    t.integer  "consumer_id"
  end

  create_table "crm_referrers", force: true do |t|
    t.string "text"
  end

  create_table "crm_referrers_brands", force: true do |t|
    t.integer "referrer_id"
    t.integer "brand_id"
  end

  add_index "crm_referrers_brands", ["referrer_id", "brand_id"], name: "index_crm_referrers_usage_profiles_on_referrer_id_and_profile_id", unique: true, using: :btree

  create_table "crm_sources", force: true do |t|
    t.string "text"
  end

  create_table "crm_sources_users", force: true do |t|
    t.integer "source_id"
    t.integer "user_id"
  end

  add_index "crm_sources_users", ["source_id", "user_id"], name: "index_crm_sources_usage_users_on_source_id_and_user_id", unique: true, using: :btree

  create_table "crm_stakeholder_relationships", force: true do |t|
    t.integer "stakeholder_id"
    t.integer "crm_case_id"
    t.integer "primary_insured_id"
    t.integer "relationship_type_id"
    t.boolean "is_owner"
    t.boolean "is_payer"
    t.boolean "is_beneficiary"
    t.integer "percentage",           limit: 1
    t.boolean "contingent"
  end

  add_index "crm_stakeholder_relationships", ["crm_case_id"], name: "index_crm_stakeholder_details_on_crm_case_id", using: :btree
  add_index "crm_stakeholder_relationships", ["primary_insured_id"], name: "index_crm_stakeholder_details_on_primary_insured_id", using: :btree
  add_index "crm_stakeholder_relationships", ["stakeholder_id", "crm_case_id"], name: "index_crm_stakeholder_details_on_stakeholder_id_and_crm_case_id", unique: true, using: :btree
  add_index "crm_stakeholder_relationships", ["stakeholder_id"], name: "index_crm_stakeholder_details_on_participant_id", using: :btree

  create_table "crm_status_types", force: true do |t|
    t.string   "color"
    t.string   "name"
    t.integer  "sort_order",              default: 0,     null: false
    t.integer  "owner_id"
    t.integer  "ownership_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "status_type_category_id"
    t.integer  "sales_stage_id",          default: 1
    t.boolean  "indicates_closed",        default: false
    t.boolean  "is_for_policies",         default: true
    t.boolean  "indicates_test",          default: false
    t.boolean  "indicates_duplicate",     default: false
  end

  create_table "crm_statuses", force: true do |t|
    t.boolean  "active",                  default: true
    t.integer  "statusable_id"
    t.integer  "created_by_id"
    t.boolean  "current"
    t.integer  "status_type_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "status_type_name"
    t.string   "statusable_type"
    t.integer  "sales_stage_id",          default: 1
    t.boolean  "indicates_closed",        default: false
    t.integer  "status_type_category_id"
  end

  add_index "crm_statuses", ["active"], name: "active", using: :btree
  add_index "crm_statuses", ["created_at", "active"], name: "created_at_active", using: :btree
  add_index "crm_statuses", ["statusable_id", "created_at"], name: "statusable_id_created_at", using: :btree

  create_table "crm_task_builders", force: true do |t|
    t.integer  "role_id"
    t.integer  "task_type_id"
    t.integer  "template_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.float    "delay"
    t.string   "label"
    t.integer  "owner_id"
    t.integer  "assigned_to_id"
    t.boolean  "evergreen",                  default: false, null: false
    t.string   "template_type"
    t.string   "owner_type"
    t.integer  "brand_id"
    t.boolean  "skip_holidays_and_weekends", default: false
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0
    t.integer  "attempts",   default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "email_addresses", force: true do |t|
    t.string   "value"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "contactable_type", limit: 20, null: false
    t.integer  "contactable_id",              null: false
  end

  add_index "email_addresses", ["contactable_id", "contactable_type"], name: "index_email_addresses_on_contactable_id_and_contactable_type", using: :btree
  add_index "email_addresses", ["contactable_id"], name: "emails_by_brand_cid", using: :btree
  add_index "email_addresses", ["contactable_id"], name: "emails_by_consumer_cid", using: :btree
  add_index "email_addresses", ["contactable_id"], name: "emails_by_user_cid", using: :btree
  add_index "email_addresses", ["value"], name: "index_email_addresses_on_contact_id_and_value", using: :btree

  create_table "excluded_carriers", force: true do |t|
    t.integer "carrier_id"
    t.integer "widget_id"
  end

  create_table "extensions", force: true do |t|
    t.integer "origin_id",   null: false
    t.string  "origin_type", null: false
    t.text    "data"
  end

  add_index "extensions", ["origin_id"], name: "extensions_origin", unique: true, using: :btree

  create_table "helps", force: true do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ixn_exts", force: true do |t|
    t.integer  "case_id"
    t.string   "ixn_id",     limit: 36
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ixn_exts", ["case_id"], name: "index_ixn_exts_on_case_id", using: :btree

  create_table "lead_distribution_cursors", force: true do |t|
    t.integer "lead_type_id"
    t.integer "brand_id",                 null: false
    t.integer "rule_id",      default: 0
  end

  create_table "lead_distribution_user_rules", force: true do |t|
    t.integer "count",         default: 0
    t.string  "tag_blacklist"
    t.integer "user_id",                       null: false
    t.boolean "off",           default: false
    t.integer "sunday",        default: 1
    t.integer "monday",        default: 1
    t.integer "tuesday",       default: 1
    t.integer "wednesday",     default: 1
    t.integer "thursday",      default: 1
    t.integer "friday",        default: 1
    t.integer "saturday",      default: 1
    t.integer "cursor_id",                     null: false
  end

  create_table "marketech_updates", force: true do |t|
    t.string   "case_id"
    t.string   "code"
    t.integer  "event"
    t.string   "party"
    t.datetime "created_at"
  end

  create_table "marketing_campaigns", force: true do |t|
    t.integer  "owner_id"
    t.integer  "ownership_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "name"
    t.integer  "planning_category_id"
    t.integer  "product_category_id"
  end

  create_table "marketing_email_attachments", force: true do |t|
    t.integer  "message_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "marketing_email_blasts", force: true do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.integer  "search_id"
    t.integer  "template_id"
    t.text     "recipients"
    t.string   "subject"
    t.text     "body"
  end

  create_table "marketing_email_smtp_servers", force: true do |t|
    t.integer "owner_id"
    t.integer "ownership_id",               default: 2
    t.string  "host"
    t.integer "port",                       default: 587
    t.string  "username"
    t.string  "address"
    t.text    "crypted_password"
    t.integer "membership_id"
    t.string  "authentication",             default: "plain"
    t.integer "tls",              limit: 1, default: 0
    t.string  "from_addr"
  end

  add_index "marketing_email_smtp_servers", ["owner_id"], name: "index_marketing_email_smtp_servers_on_owner_id", using: :btree

  create_table "marketing_messages", force: true do |t|
    t.string   "subject"
    t.string   "topic"
    t.text     "body"
    t.string   "recipient"
    t.datetime "sent"
    t.string   "sent_by_host"
    t.integer  "failed_attempts"
    t.string   "failure_msgs"
    t.integer  "template_id"
    t.integer  "brand_id"
    t.integer  "sender_id"
    t.integer  "user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "type"
    t.integer  "target_id"
    t.boolean  "marketing",       default: false
    t.string   "target_type"
    t.string   "blast_id"
    t.integer  "viewed",          default: 0
  end

  create_table "marketing_organizations", force: true do |t|
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marketing_subscriptions", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "person_id"
    t.string   "person_type"
    t.integer  "queue_num"
    t.integer  "order_in_queue"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by_id"
    t.integer  "created_via_report_id"
  end

  create_table "marketing_templates", force: true do |t|
    t.text     "body",                        limit: 16777215
    t.integer  "owner_id"
    t.integer  "ownership_id",                                 default: 3
    t.string   "subject"
    t.string   "name"
    t.string   "description"
    t.boolean  "enabled",                                      default: true
    t.string   "type"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "product_category_id"
    t.integer  "planning_category_id"
    t.integer  "quoter_url_type",             limit: 1,        default: 0
    t.string   "custom_quoter_url"
    t.string   "insurance_division_url_page"
    t.integer  "template_purpose_id"
  end

  create_table "marketing_vici_client_data", force: true do |t|
    t.integer  "client_id"
    t.integer  "vici_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: true do |t|
    t.boolean  "confidential"
    t.integer  "creator_id"
    t.integer  "note_type_id"
    t.text     "text"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "notable_id"
    t.string   "notable_type"
    t.boolean  "critical",     default: false
    t.boolean  "personal",     default: false
  end

  add_index "notes", ["notable_id", "notable_type"], name: "index_notes_on_notable_id_and_notable_type", using: :btree

  create_table "phone_recordings", force: true do |t|
    t.integer  "person_id"
    t.string   "person_type"
    t.string   "s3_filepath"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "phone_recordings", ["person_id", "person_type"], name: "index_phone_recordings_on_person_id_and_person_type", using: :btree

  create_table "phones", force: true do |t|
    t.string   "ext"
    t.integer  "phone_type_id"
    t.string   "value",            limit: 10
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "carrier_id"
    t.string   "contactable_type", limit: 20, null: false
    t.integer  "contactable_id",              null: false
  end

  add_index "phones", ["contactable_id", "contactable_type"], name: "index_phones_on_contactable_id_and_contactable_type", using: :btree
  add_index "phones", ["contactable_id"], name: "phones_by_brand_cid", using: :btree
  add_index "phones", ["contactable_id"], name: "phones_by_consumer_cid", using: :btree
  add_index "phones", ["contactable_id"], name: "phones_by_user_cid", using: :btree
  add_index "phones", ["value"], name: "index_phones_on_contact_id_and_value", using: :btree

  create_table "pma__bookmark", force: true do |t|
    t.string "dbase", default: "", null: false
    t.string "user",  default: "", null: false
    t.string "label", default: "", null: false
    t.text   "query",              null: false
  end

  create_table "pma__central_columns", id: false, force: true do |t|
    t.string  "db_name",       limit: 64,              null: false
    t.string  "col_name",      limit: 64,              null: false
    t.string  "col_type",      limit: 64,              null: false
    t.text    "col_length"
    t.string  "col_collation", limit: 64,              null: false
    t.boolean "col_isNull",                            null: false
    t.string  "col_extra",                default: ""
    t.text    "col_default"
  end

  create_table "pma__column_info", force: true do |t|
    t.string "db_name",                      limit: 64, default: "", null: false
    t.string "table_name",                   limit: 64, default: "", null: false
    t.string "column_name",                  limit: 64, default: "", null: false
    t.string "comment",                                 default: "", null: false
    t.string "mimetype",                                default: "", null: false
    t.string "transformation",                          default: "", null: false
    t.string "transformation_options",                  default: "", null: false
    t.string "input_transformation",                    default: "", null: false
    t.string "input_transformation_options",            default: "", null: false
  end

  add_index "pma__column_info", ["db_name", "table_name", "column_name"], name: "db_name", unique: true, using: :btree

  create_table "pma__designer_settings", primary_key: "username", force: true do |t|
    t.text "settings_data", null: false
  end

  create_table "pma__export_templates", force: true do |t|
    t.string "username",      limit: 64, null: false
    t.string "export_type",   limit: 10, null: false
    t.string "template_name", limit: 64, null: false
    t.text   "template_data",            null: false
  end

  add_index "pma__export_templates", ["username", "export_type", "template_name"], name: "u_user_type_template", unique: true, using: :btree

  create_table "pma__favorite", primary_key: "username", force: true do |t|
    t.text "tables", null: false
  end

  create_table "pma__history", force: true do |t|
    t.string   "username",  limit: 64, default: "", null: false
    t.string   "db",        limit: 64, default: "", null: false
    t.string   "table",     limit: 64, default: "", null: false
    t.datetime "timevalue",                         null: false
    t.text     "sqlquery",                          null: false
  end

  add_index "pma__history", ["username", "db", "table", "timevalue"], name: "username", using: :btree

  create_table "pma__navigationhiding", id: false, force: true do |t|
    t.string "username",   limit: 64, null: false
    t.string "item_name",  limit: 64, null: false
    t.string "item_type",  limit: 64, null: false
    t.string "db_name",    limit: 64, null: false
    t.string "table_name", limit: 64, null: false
  end

  create_table "pma__pdf_pages", primary_key: "page_nr", force: true do |t|
    t.string "db_name",    limit: 64, default: "", null: false
    t.string "page_descr", limit: 50, default: "", null: false
  end

  add_index "pma__pdf_pages", ["db_name"], name: "db_name", using: :btree

  create_table "pma__recent", primary_key: "username", force: true do |t|
    t.text "tables", null: false
  end

  create_table "pma__relation", id: false, force: true do |t|
    t.string "master_db",     limit: 64, default: "", null: false
    t.string "master_table",  limit: 64, default: "", null: false
    t.string "master_field",  limit: 64, default: "", null: false
    t.string "foreign_db",    limit: 64, default: "", null: false
    t.string "foreign_table", limit: 64, default: "", null: false
    t.string "foreign_field", limit: 64, default: "", null: false
  end

  add_index "pma__relation", ["foreign_db", "foreign_table"], name: "foreign_field", using: :btree

  create_table "pma__savedsearches", force: true do |t|
    t.string "username",    limit: 64, default: "", null: false
    t.string "db_name",     limit: 64, default: "", null: false
    t.string "search_name", limit: 64, default: "", null: false
    t.text   "search_data",                         null: false
  end

  add_index "pma__savedsearches", ["username", "db_name", "search_name"], name: "u_savedsearches_username_dbname", unique: true, using: :btree

  create_table "pma__table_coords", id: false, force: true do |t|
    t.string  "db_name",         limit: 64, default: "",  null: false
    t.string  "table_name",      limit: 64, default: "",  null: false
    t.integer "pdf_page_number",            default: 0,   null: false
    t.float   "x",                          default: 0.0, null: false
    t.float   "y",                          default: 0.0, null: false
  end

  create_table "pma__table_info", id: false, force: true do |t|
    t.string "db_name",       limit: 64, default: "", null: false
    t.string "table_name",    limit: 64, default: "", null: false
    t.string "display_field", limit: 64, default: "", null: false
  end

  create_table "pma__table_uiprefs", id: false, force: true do |t|
    t.string   "username",    limit: 64, null: false
    t.string   "db_name",     limit: 64, null: false
    t.string   "table_name",  limit: 64, null: false
    t.text     "prefs",                  null: false
    t.datetime "last_update",            null: false
  end

  create_table "pma__tracking", id: false, force: true do |t|
    t.string   "db_name",         limit: 64,                     null: false
    t.string   "table_name",      limit: 64,                     null: false
    t.integer  "version",                                        null: false
    t.datetime "date_created",                                   null: false
    t.datetime "date_updated",                                   null: false
    t.text     "schema_snapshot",                                null: false
    t.text     "schema_sql"
    t.text     "data_sql",        limit: 2147483647
    t.string   "tracking",        limit: 0
    t.integer  "tracking_active",                    default: 1, null: false
  end

  create_table "pma__userconfig", primary_key: "username", force: true do |t|
    t.datetime "timevalue",   null: false
    t.text     "config_data", null: false
  end

  create_table "pma__usergroups", id: false, force: true do |t|
    t.string "usergroup", limit: 64,               null: false
    t.string "tab",       limit: 64,               null: false
    t.string "allowed",   limit: 1,  default: "N", null: false
  end

  create_table "pma__users", id: false, force: true do |t|
    t.string "username",  limit: 64, null: false
    t.string "usergroup", limit: 64, null: false
  end

  create_table "processing_apps_case_data", force: true do |t|
    t.datetime "uploaded_at"
    t.integer  "case_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "processing_docusign_case_data", force: true do |t|
    t.integer  "case_id"
    t.string   "envolope_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "processing_exam_one_case_data", force: true do |t|
    t.integer  "case_id"
    t.boolean  "info_sent"
    t.string   "or01_code"
    t.string   "schedule_now_code"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "control_code_id"
  end

  create_table "processing_exam_one_control_codes", force: true do |t|
    t.string  "name"
    t.integer "paramed"
    t.string  "subsidiary"
    t.boolean "packet"
    t.boolean "disability"
    t.integer "state_id"
    t.integer "carrier_id"
  end

  create_table "processing_exam_one_statuses", force: true do |t|
    t.integer  "case_id"
    t.string   "description"
    t.datetime "completed_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "consumer_id"
    t.integer  "exam_one_status_id"
  end

  create_table "processing_igo_carrier_products", force: true do |t|
    t.integer "carrier_id"
    t.string  "carrier_name"
    t.integer "product_id"
    t.string  "product_name"
    t.integer "product_type_id"
    t.string  "product_type_name"
    t.integer "dr_carrier_id"
  end

  create_table "processing_marketech_carriers", force: true do |t|
    t.string  "code"
    t.integer "carrier_id"
    t.boolean "esign"
    t.integer "state_id_mask",        limit: 8
    t.integer "face_amount"
    t.string  "face_amount_operator"
  end

  create_table "processing_marketech_datasets", force: true do |t|
    t.integer  "case_id"
    t.integer  "status_id"
    t.string   "xml_id"
    t.integer  "update_id"
    t.string   "esign_code"
    t.datetime "updated_at"
    t.boolean  "esign_complete"
    t.datetime "created_at"
  end

  create_table "processing_marketech_updates", force: true do |t|
    t.string   "case_id"
    t.string   "code"
    t.integer  "event"
    t.string   "party"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "processing_smm_carriers", force: true do |t|
    t.integer "carrier_id"
    t.integer "smm_code"
  end

  create_table "processing_smm_statuses", force: true do |t|
    t.integer  "case_id"
    t.integer  "client_id"
    t.string   "desc"
    t.integer  "order_id"
    t.date     "scheduled"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_categories", force: true do |t|
    t.string "name"
  end

  create_table "quoting_details", force: true do |t|
    t.decimal  "annual_premium",          precision: 10, scale: 2
    t.integer  "carrier_id"
    t.string   "carrier_health_class"
    t.string   "plan_name"
    t.decimal  "planned_modal_premium",   precision: 10, scale: 2
    t.integer  "product_type_id"
    t.integer  "user_id"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.decimal  "monthly_premium",         precision: 10, scale: 2
    t.integer  "consumer_id"
    t.integer  "duration_id"
    t.integer  "health_class_id"
    t.integer  "premium_mode_id"
    t.integer  "face_amount"
    t.integer  "case_id"
    t.decimal  "quarterly_premium",       precision: 10, scale: 2
    t.decimal  "semiannual_premium",      precision: 10, scale: 2
    t.decimal  "single_premium",          precision: 10, scale: 2
    t.integer  "sales_stage_id",                                   default: 1,     null: false
    t.boolean  "include_in_forecasts",                             default: false
    t.boolean  "closed",                                           default: false
    t.datetime "entered_stage_contacted"
    t.boolean  "is_test",                                          default: false
    t.boolean  "is_duplicate",                                     default: false
    t.integer  "status_type_category_id"
    t.decimal  "x1035_amt",               precision: 10, scale: 2
    t.decimal  "lump_sum",                precision: 10, scale: 2
    t.decimal  "flat_amt",                precision: 10, scale: 2
    t.integer  "flat_yrs"
  end

  add_index "quoting_details", ["case_id", "sales_stage_id"], name: "index_quoting_details_on_case_id_and_sales_stage_id", unique: true, using: :btree
  add_index "quoting_details", ["consumer_id"], name: "index_quoting_details_on_connection_id", using: :btree

  create_table "quoting_ltc_extra_field_sets", force: true do |t|
    t.integer  "benefit_period_id"
    t.integer  "elimination_period"
    t.integer  "health_id"
    t.integer  "inflation_protection_id"
    t.integer  "quoter_id"
    t.boolean  "shared_benefit"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "quoting_neli_extra_field_sets", force: true do |t|
    t.boolean  "hiv"
    t.boolean  "in_ltc_facility"
    t.boolean  "terminal"
    t.boolean  "tobacco"
    t.integer  "quoter_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "quoting_quoter_types_widgets", id: false, force: true do |t|
    t.integer "quoter_type_id"
    t.integer "widget_id"
  end

  create_table "quoting_quoters", force: true do |t|
    t.integer  "client_id"
    t.integer  "coverage_amount"
    t.string   "income_option"
    t.boolean  "joint"
    t.date     "joint_birth"
    t.string   "joint_gender"
    t.string   "joint_health"
    t.integer  "joint_state_id"
    t.integer  "premium_mode_id"
    t.integer  "quoter_type_id"
    t.integer  "state_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.boolean  "married"
    t.integer  "health_info_id"
  end

  create_table "quoting_relatives_diseases", force: true do |t|
    t.boolean  "basal_cell_carcinoma"
    t.boolean  "breast_cancer"
    t.boolean  "cardiovascular_disease"
    t.boolean  "cardiovascular_impairments"
    t.boolean  "cerebrovascular_disease"
    t.boolean  "colon_cancer"
    t.boolean  "coronary_artery_disease"
    t.boolean  "diabetes"
    t.boolean  "intestinal_cancer"
    t.boolean  "kidney_disease"
    t.boolean  "malignant_melanoma"
    t.boolean  "other_internal_cancer"
    t.boolean  "ovarian_cancer"
    t.boolean  "prostate_cancer"
    t.integer  "age_of_contraction"
    t.integer  "age_of_death"
    t.boolean  "parent"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "health_info_id"
  end

  add_index "quoting_relatives_diseases", ["health_info_id"], name: "index_quoting_relatives_diseases_on_health_info_id", using: :btree

  create_table "quoting_spia_extra_field_sets", force: true do |t|
    t.string   "income_option"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "quoting_widget_quoters", force: true do |t|
    t.integer  "widget_id"
    t.string   "tier_1_action"
    t.string   "tier_2_action"
    t.string   "type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "quoting_widget_widgets", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "hostname"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "background_color"
    t.string   "border_color"
    t.string   "text_color"
    t.string   "header_text_color"
    t.string   "button_color"
    t.string   "header_text"
    t.string   "button_text"
    t.string   "button2_text"
    t.string   "button3_text"
    t.string   "default_tier_1_action"
    t.string   "default_tier_2_action"
    t.integer  "widget_type_id"
    t.integer  "quoter_style"
    t.integer  "brand_id"
    t.boolean  "show_name_field"
    t.boolean  "show_phone_field"
    t.boolean  "show_email_field"
    t.string   "lead_type"
    t.string   "source"
    t.string   "referrer"
    t.integer  "product_cat_id"
    t.string   "tags"
    t.string   "agent_split"
  end

  create_table "reporting_searches", force: true do |t|
    t.integer  "owner_id"
    t.integer  "ownership_id"
    t.string   "name"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.text     "criteria"
    t.boolean  "client_visible",         default: true
    t.boolean  "case_visible",           default: true
    t.boolean  "status_visible",         default: true
    t.boolean  "fields_visible",         default: true
    t.text     "fields_to_show"
    t.boolean  "marketing_visible"
    t.boolean  "owner_visible"
    t.integer  "record_types_option_id", default: 0
  end

  create_table "tagging_tag_keys", force: true do |t|
    t.string "name"
  end

  create_table "tagging_tag_values", force: true do |t|
    t.string "value"
  end

  create_table "tags", force: true do |t|
    t.string  "value"
    t.string  "person_type"
    t.integer "person_id"
    t.integer "tenant_id"
    t.integer "owner_id"
  end

  add_index "tags", ["value"], name: "index_tags_on_value", using: :btree

  create_table "tasks", force: true do |t|
    t.integer  "assigned_to_id"
    t.integer  "created_by_id"
    t.string   "recipient"
    t.integer  "task_type_id"
    t.integer  "template_id"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.datetime "completed_at"
    t.integer  "person_id"
    t.datetime "due_at"
    t.string   "label"
    t.integer  "origin_id"
    t.string   "template_type"
    t.integer  "lock_version",                              default: 0,     null: false
    t.boolean  "is_executing",                              default: false
    t.integer  "failed_attempts",                           default: 0
    t.integer  "completed_by_id"
    t.string   "assigned_to_name"
    t.string   "completed_by_name"
    t.string   "status_type_name"
    t.decimal  "tz_offset",         precision: 4, scale: 2
    t.string   "origin_type"
    t.boolean  "evergreen",                                 default: false, null: false
    t.integer  "message_id"
    t.string   "person_type"
    t.integer  "role_id"
    t.boolean  "active",                                    default: true
    t.boolean  "suspended",                                 default: false
    t.string   "person_name"
    t.integer  "sequenceable_id"
    t.string   "sequenceable_type"
    t.integer  "state_id"
    t.boolean  "scheduled",                                 default: false
    t.integer  "tenant_id"
  end

  add_index "tasks", ["assigned_to_id"], name: "tasks_by_assigned_to_id", using: :btree
  add_index "tasks", ["due_at"], name: "due_at", using: :btree
  add_index "tasks", ["message_id"], name: "tasks_by_message_id", using: :btree
  add_index "tasks", ["origin_id"], name: "tasks_by_status_oid", using: :btree
  add_index "tasks", ["origin_id"], name: "tasks_by_sub_oid", using: :btree
  add_index "tasks", ["person_id"], name: "tasks_by_consumer_pid", using: :btree
  add_index "tasks", ["person_id"], name: "tasks_by_user_pid", using: :btree

  create_table "usage_aml_vendors", force: true do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usage_ascendants_descendants", id: false, force: true do |t|
    t.integer "ascendant_id"
    t.integer "descendant_id"
  end

  add_index "usage_ascendants_descendants", ["ascendant_id", "descendant_id"], name: "index_ascendancy", using: :btree

  create_table "usage_contracts", force: true do |t|
    t.integer  "carrier_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "carrier_contract_id"
    t.boolean  "corporate"
    t.integer  "status_id"
    t.date     "effective_date"
    t.date     "expiration"
    t.integer  "user_id"
  end

  add_index "usage_contracts", ["user_id"], name: "index_usage_contracts_on_user_id", using: :btree

  create_table "usage_contracts_states", id: false, force: true do |t|
    t.integer "contract_id"
    t.integer "state_id"
  end

  add_index "usage_contracts_states", ["contract_id"], name: "index_usage_contracts_states_on_contract_id", using: :btree

  create_table "usage_docusign_creds", force: true do |t|
    t.integer "user_id"
    t.string  "account_id"
    t.string  "email"
    t.string  "password"
  end

  create_table "usage_licenses", force: true do |t|
    t.date     "expiration"
    t.boolean  "expiration_warning_sent"
    t.string   "number"
    t.integer  "status_id"
    t.integer  "state_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "corporate"
    t.date     "effective_date"
    t.boolean  "health"
    t.boolean  "life"
    t.boolean  "p_and_c"
    t.boolean  "variable"
    t.integer  "user_id"
  end

  add_index "usage_licenses", ["user_id"], name: "index_usage_licenses_on_user_id", using: :btree

  create_table "usage_profiles_users", force: true do |t|
    t.integer "profile_id"
    t.integer "user_id"
  end

  create_table "usage_sales_support_field_sets", force: true do |t|
    t.string   "docusign_email"
    t.string   "docusign_account_id"
    t.string   "docusign_password"
    t.string   "metlife_agent_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "usage_security_answers", force: true do |t|
    t.integer  "question_1_id"
    t.string   "question_1_value"
    t.integer  "question_2_id"
    t.string   "question_2_value"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "usage_staff_assignments", force: true do |t|
    t.integer  "administrative_assistant_id"
    t.integer  "case_manager_id"
    t.integer  "manager_id"
    t.integer  "policy_specialist_id"
    t.integer  "application_specialist_id"
    t.integer  "underwriter_id"
    t.integer  "insurance_coordinator_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "agent_id"
    t.integer  "owner_id"
    t.string   "owner_type"
  end

  add_index "usage_staff_assignments", ["administrative_assistant_id"], name: "index_usage_staff_assignments_on_administrative_assistant_id", using: :btree
  add_index "usage_staff_assignments", ["application_specialist_id"], name: "index_usage_staff_assignments_on_application_specialist_id", using: :btree
  add_index "usage_staff_assignments", ["case_manager_id"], name: "index_usage_staff_assignments_on_case_manager_id", using: :btree
  add_index "usage_staff_assignments", ["insurance_coordinator_id"], name: "index_usage_staff_assignments_on_insurance_coordinator_id", using: :btree
  add_index "usage_staff_assignments", ["manager_id"], name: "index_usage_staff_assignments_on_manager_id", using: :btree
  add_index "usage_staff_assignments", ["owner_type", "owner_id"], name: "index_usage_staff_assignments_on_owner_type_and_owner_id", using: :btree
  add_index "usage_staff_assignments", ["policy_specialist_id"], name: "index_usage_staff_assignments_on_policy_specialist_id", using: :btree
  add_index "usage_staff_assignments", ["underwriter_id"], name: "index_usage_staff_assignments_on_underwriter_id", using: :btree

  create_table "usage_stripe_events", force: true do |t|
    t.string   "evt_id"
    t.string   "evt_type"
    t.text     "body"
    t.datetime "created_at"
  end

  create_table "usage_user_group_rules_tag_values", id: false, force: true do |t|
    t.integer "group_id"
    t.integer "tag_value_id"
  end

  create_table "users", force: true do |t|
    t.string   "login"
    t.integer  "parent_id"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.datetime "current_login_at"
    t.string   "current_login_ip"
    t.integer  "failed_login_count",                     default: 0,     null: false
    t.datetime "last_login_at"
    t.string   "last_login_ip"
    t.datetime "last_request_at"
    t.integer  "login_count",                            default: 0,     null: false
    t.string   "perishable_token"
    t.integer  "sales_support_field_set_id"
    t.text     "note"
    t.integer  "manager_id"
    t.integer  "commission_level_id"
    t.string   "nickname"
    t.date     "anniversary"
    t.integer  "legacy_search_id"
    t.text     "encrypted_tin"
    t.string   "assistant_name"
    t.string   "assistant_email"
    t.string   "assistant_phone"
    t.integer  "relationship_manager_id"
    t.date     "commission_level_updated_at"
    t.integer  "primogen_id"
    t.integer  "support_group_id"
    t.boolean  "user_processes_own_business"
    t.boolean  "security_questions_answered",            default: false
    t.integer  "initial_status_type_id"
    t.string   "time_zone"
    t.text     "tags"
    t.text     "common_tags"
    t.text     "custom_field_one"
    t.text     "custom_field_two"
    t.text     "custom_field_three"
    t.string   "vici_id"
    t.string   "docusign_id"
    t.integer  "default_brand_id"
    t.boolean  "marketing_unsubscribe",                  default: false
    t.integer  "status_id"
    t.integer  "subscription",                           default: 1
    t.integer  "recruitment_brand_id"
    t.integer  "membership_id",                          default: 1
    t.string   "stripe_email"
    t.string   "stripe_customer_id"
    t.string   "stripe_subscription_id"
    t.string   "stripe_plan_id"
    t.datetime "payment_due"
    t.boolean  "tos"
    t.integer  "user_account_id"
    t.boolean  "pinney_record"
    t.integer  "broker_dealer_id"
    t.integer  "permissions_0"
    t.integer  "permissions_1"
    t.integer  "permissions_2"
    t.integer  "permissions_3"
    t.integer  "permissions_4"
    t.string   "old_plaintext_api_key"
    t.datetime "last_contacted"
    t.string   "quoter_key",                  limit: 32
    t.string   "encrypted_api_key"
    t.string   "encrypted_api_key_iv"
    t.integer  "tenant_id",                              default: 0,     null: false
    t.string   "motorist_id"
    t.boolean  "status_unsubscribe"
    t.boolean  "exclude_global_status_types",            default: false
    t.date     "aml_completed_at"
    t.integer  "aml_vendor_id"
    t.string   "full_name"
    t.string   "title"
    t.string   "company"
    t.integer  "preferred_contact_method_id"
    t.string   "preferred_contact_time"
    t.integer  "primary_address_id"
    t.integer  "primary_phone_id"
    t.integer  "primary_email_id"
    t.date     "birth_or_trust_date"
    t.boolean  "active_or_enabled",                      default: true
    t.integer  "entity_type_id",              limit: 1,  default: 1
    t.string   "encrypted_ssn"
    t.boolean  "is_recruit",                             default: false, null: false
    t.boolean  "temporary_suspension",                   default: false
    t.integer  "premium_limit"
    t.integer  "tz_max"
    t.integer  "tz_min"
    t.integer  "agent_of_record_id"
    t.boolean  "submit_application",                     default: false
    t.boolean  "submit_referral",                        default: false
    t.string   "marketech_signature"
    t.string   "lead_type"
    t.string   "referrer"
    t.string   "source"
  end

  add_index "users", ["encrypted_api_key_iv"], name: "index_users_on_encrypted_api_key_iv", unique: true, using: :btree
  add_index "users", ["full_name"], name: "index_users_on_full_name", using: :btree
  add_index "users", ["id", "full_name"], name: "index_users_on_id_and_full_name", using: :btree
  add_index "users", ["motorist_id"], name: "index_usage_users_on_motorist_id", unique: true, using: :btree
  add_index "users", ["old_plaintext_api_key"], name: "index_usage_users_on_api_key", using: :btree
  add_index "users", ["parent_id", "full_name"], name: "index_users_on_parent_id_and_full_name", using: :btree
  add_index "users", ["quoter_key"], name: "index_usage_users_on_quoter_key", using: :btree

  create_table "users_common_tags", id: false, force: true do |t|
    t.integer "user_id", null: false
    t.integer "tag_id",  null: false
  end

  add_index "users_common_tags", ["user_id", "tag_id"], name: "index_usage_users_common_tags_on_user_id_and_tag_id", unique: true, using: :btree

  create_table "visits", force: true do |t|
    t.uuid     "visitor_id"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "user_type"
    t.integer  "owner_id"
    t.integer  "brand_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree

  create_table "webs", force: true do |t|
    t.integer  "web_type_id"
    t.string   "value"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "contactable_type", limit: 20, null: false
    t.integer  "contactable_id",              null: false
  end

  add_index "webs", ["contactable_id", "contactable_type"], name: "index_webs_on_contactable_id_and_contactable_type", using: :btree

end
