class RestructureRemainingHealthConditions < ActiveRecord::Migration
=begin
This migration seeks to move all the health condition details into serialized objects for the sake of uniformity.
Those formerly stored through health history are already moved via arlier migration 20180719183814.
=end

  def up
    condition_names=[
      :bp,
      :cholesterol,
      :foreign_travel,
      :moving_violation,
      :tobacco,
      :hazardous_avocation,
      :criminal
    ]

    hi_records=execute(%Q(
      SELECT * FROM crm_health_infos WHERE id>3912;
    )).to_a
    hi_column_names=execute(%Q(
      DESCRIBE crm_health_infos;
    )).to_a.map(&:first)
    mvh_records=execute(%Q(
      SELECT * FROM crm_moving_violation_histories;
    )).to_a
    mvh_column_names=execute(%Q(
      DESCRIBE crm_moving_violation_histories;
    )).to_a.map(&:first)

    #Copy individual condition fields into condition details fields within the same database row.
    #This will be quite slow, as it is 1 update query per record.
    hi_records.in_groups_of(50) do |group|
      group.compact!
      group.each do |hi|
        attrs=Hash[hi_column_names.zip hi]
        hi_id=attrs['id']
        nested_attrs={}

        condition_names.each do |condition_name|
          if condition_name==:foreign_travel
            old_cn_prefix= :travel
          else
            old_cn_prefix=condition_name
          end

          is_prefixed_with_cn=Regexp.new("^#{old_cn_prefix.to_s}_")
          is_a_serialized_field=/_details$/
          attrs_to_copy=attrs.select do |k,v|
            k.match( is_prefixed_with_cn ) && !k.match( is_a_serialized_field ) && !v.nil? && !(v.is_a?(String) && v.blank?)
          end
          nested_attrs["#{condition_name}_details"]={}
          attrs_to_copy.each{|k,v| nested_attrs["#{condition_name}_details"][ k.to_s.sub("#{condition_name}_",'') ]=v }
        end

        if attrs['moving_violation_history_id']
          mvh_id=attrs['moving_violation_history_id']
          mvh=mvh_records.select{|r| r[0]==mvh_id }.first
          mvh_attrs=Hash[mvh_column_names.zip mvh]
          nested_attrs["moving_violation_details"].merge!(mvh_attrs).except!('id','history_id','created_at','updated_at')
        end

        update_str=''
        nested_attrs.each do |k,v|
          next unless v.present?
          json_str=v.to_json.gsub("'","")
          update_str+="#{k}='#{json_str}',\n"
        end

        update_str=update_str[0..(update_str.length-3)]

        if update_str.present?
          execute(%Q(
            UPDATE crm_health_infos
            SET #{ update_str }
            WHERE id=#{hi_id};
          ))
        end
      end
      Rails.env.production? ? sleep(4) : nil
    end
  end

  def down
    #Nothing to do here.
    #Earlier migration 20180719183813 added new columns.
    #Later migration 20180719183816 will remove the old columns and tables.
  end

end
