class AddColumnsForRttProcess < ActiveRecord::Migration
  def change
    add_column :crm_connections, :occupation_description,                                :string, after: :occupation
    add_column :crm_cases,       :conditional_binding_receipt_amount,                    :decimal, precision:2
    add_column :crm_health_infos,:hazardous_avocation_aeronautics,                       :boolean
    add_column :crm_health_infos,:hazardous_avocation_aeronautics_hang_gliding,          :boolean
    add_column :crm_health_infos,:hazardous_avocation_aeronautics_sky_diving,            :boolean
    add_column :crm_health_infos,:hazardous_avocation_aeronautics_parachuting,           :boolean
    add_column :crm_health_infos,:hazardous_avocation_aeronautics_ballooning,            :boolean
    add_column :crm_health_infos,:hazardous_avocation_aeronautics_other,                 :boolean
    add_column :crm_health_infos,:hazardous_avocation_racing,                            :boolean
    add_column :crm_health_infos,:hazardous_avocation_racing_car,                        :boolean
    add_column :crm_health_infos,:hazardous_avocation_racing_motorcycle,                 :boolean
    add_column :crm_health_infos,:hazardous_avocation_racing_boat,                       :boolean
    add_column :crm_health_infos,:hazardous_avocation_racing_other,                      :boolean
    add_column :crm_health_infos,:hazardous_avocation_scuba_skin_diving,                 :boolean
    add_column :crm_health_infos,:hazardous_avocation_scuba_skin_diving_lte_75_ft,       :boolean
    add_column :crm_health_infos,:hazardous_avocation_scuba_skin_diving_gt_75_ft,        :boolean
    add_column :crm_health_infos,:hazardous_avocation_climbing_hiking,                   :boolean
    add_column :crm_health_infos,:hazardous_avocation_climbing_hiking_trail,             :boolean
    add_column :crm_health_infos,:hazardous_avocation_climbing_hiking_mountain,          :boolean
    add_column :crm_health_infos,:hazardous_avocation_climbing_hiking_rock,              :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying,                            :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_non_pilot_crew,             :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_student_pilot,              :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_private_pilot,              :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_private_pilot_lt_50_h,      :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_private_pilot_50_to_250,    :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_private_pilot_gt_250_h,     :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_private_pilot_lt_100_solo_h,:boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_commercial_pilot,           :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_corporate_pilot,            :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_military_pilot,             :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_test_pilot,                 :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_flight_instructor,          :boolean
    add_column :crm_health_infos,:hazardous_avocation_flying_other,                      :boolean
    add_column :crm_health_infos,:travel_country_id,                                     :integer
    add_column :crm_health_infos,:moving_violation_last_dl_suspension_reason,            :string
    add_column :crm_health_infos,:moving_violation_dl_suspended_current,                 :boolean

  end
end
