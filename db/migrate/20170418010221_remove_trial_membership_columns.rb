class RemoveTrialMembershipColumns < ActiveRecord::Migration
  def up
    remove_column :usage_users, :trial_membership_id
    remove_column :usage_users, :trial_membership_expires
    remove_column :usage_users, :trial_expiration_processed
  end

  def down
    add_column    :usage_users, :trial_membership_id,        :integer
    add_column    :usage_users, :trial_membership_expires,   :date
    add_column    :usage_users, :trial_expiration_processed, :boolean
  end
end
