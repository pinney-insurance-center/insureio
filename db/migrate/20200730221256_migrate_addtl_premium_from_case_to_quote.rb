class MigrateAddtlPremiumFromCaseToQuote < ActiveRecord::Migration
  def up
    # _All_ the records in the db have `false` for `needs_1035`
    # 2 records have non-null amount_for_1035
    # 412 records have non-null, non-zero  flat_extra
    # 324 records have non-null, non-zero  flat_extra_years
    query = <<~EOF
      (amount_for_1035 IS NOT NULL AND  amount_for_1035 != 0)
      OR (flat_extra IS NOT NULL AND flat_extra != 0)
      OR (flat_extra_years IS NOT NULL AND flat_extra_years != 0)
    EOF
    # There are 464 of these records
    Crm::Case.where(query).includes(:current_details).each do |kase|
      kase.current_details&.update_columns(
        flat_amt: kase.flat_extra,
        flat_yrs: kase.flat_extra_years,
        x1035_amt: kase.amount_for_1035,
      )
    end
  end

  def down
    query = <<~EOF
      (x1035_amt IS NOT NULL AND x1035_amt != 0)
      OR (flat_amt IS NOT NULL AND flat_amt != 0)
      OR (flat_yrs IS NOT NULL AND flat_yrs != 0)
    EOF
    Quoting::Quote.where(query).includes(:crm_case).each do |quote|
      quote.crm_case&.update_columns(
        flat_extra: quote.flat_amt,
        flat_extra_years: quote.flat_yrs,
        amount_for_1035: quote.x1035_amt,
      )
    end
  end
end
