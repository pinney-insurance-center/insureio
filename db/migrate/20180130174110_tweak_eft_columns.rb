class TweakEftColumns < ActiveRecord::Migration
  def up
    remove_column :crm_connections, :eft_account_type_id

    add_column    :crm_connections, :encrypted_eft_bank_name,     :string
    add_column    :crm_connections, :encrypted_eft_bank_name_iv,  :string
  end

  def down
    add_column    :crm_connections, :eft_account_type_id,          :integer

    remove_column :crm_connections, :encrypted_eft_bank_name
    remove_column :crm_connections, :encrypted_eft_bank_name_iv
  end
end
