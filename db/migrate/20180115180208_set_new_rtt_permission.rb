class SetNewRttPermission < ActiveRecord::Migration
  TENANT_ID_FOR_BSB=6

  #This function is redefined here because the copy in the user model refers to the new tablename,
  #which differs from the expected table name for this older migration.
  def bulk_set permission, user_ids, value_to_set=true
    col, bit_position = Usage::Permissions::column_and_bit(permission)
    value_of_bit_position = 2**bit_position
    #In SQL, the single pipe character means OR (used for turning a bit on),
    #and the single ampersand character means AND (used for turning a bit off).
    bitwise_operator = value_to_set ? '| ' : '& ~'

    #handle large user sets in batches to prevent timeouts or lock issues
    user_ids.in_groups_of(100) do |uid_group|
      uid_group.compact!
      query_string="
        UPDATE usage_users
        SET #{col}=#{col} #{bitwise_operator}#{value_of_bit_position}
        WHERE id IN (#{ uid_group.join(',') })
      "
      ActiveRecord::Base.connection.execute(query_string)
    end
  end

  def up
    non_bsb_user_ids=execute(%Q(
      SELECT id
      FROM   usage_users
      WHERE  tenant_id!=#{TENANT_ID_FOR_BSB}
    )).to_a.flatten

    bsb_user_ids=execute(%Q(
      SELECT id
      FROM   usage_users
      WHERE  tenant_id=#{TENANT_ID_FOR_BSB}
    )).to_a.flatten
    #Clear permission for those who do not need it.
    #The handy `bulk_set` method already takes care of processing in batches to prevent timeouts.
    bulk_set :motorists_rtt_process, non_bsb_user_ids, false
    #Set permission for those who do need it.
    bulk_set :motorists_rtt_process, bsb_user_ids, true
  end

  def down
  end
end
