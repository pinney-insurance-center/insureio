class DropUnusedTablesAndColumns < ActiveRecord::Migration
  def up

    tables_and_columns={
      tag_tags:[],
      tag_tag_keys:[],
      tag_tag_values:[],
      usage_paypal_transactions:[],
      crm_activities:[],
      crm_ezl_joins:[],
      crm_statuses:[:system_task_id],
      marketing_templates:[
        :thumbnail_file_name,
        :thumbnail_content_type,
        :thumbnail_file_size,
        :thumbnail_updated_at
      ],
      quoting_details:[
        :illustration_file_name,
        :illustration_content_type,
        :illustration_file_size,
        :illustration_updated_at
      ],
      brands:[
        :logo_file_name,
        :logo_content_type,
        :logo_file_size,
        :logo_updated_at,
        :header_image_file_name,
        :header_image_content_type,
        :header_image_file_size,
        :header_image_updated_at
      ],
      users:[
        :role_id,
        :signature_file_name,
        :signature_content_type,
        :signature_file_size,
        :signature_updated_at,
        :photo_file_name,
        :photo_content_type,
        :photo_file_size,
        :photo_updated_at,
        :selected_brand_id
      ],
    }

    tables_and_columns.each do |table_name,col_list|
      if col_list.empty? && table_exists?(table_name)
        drop_table table_name
      else
        col_list.each do |col|
          remove_column table_name, col if column_exists?(table_name,col)
        end
      end
    end

  end

  def down
  end
end
