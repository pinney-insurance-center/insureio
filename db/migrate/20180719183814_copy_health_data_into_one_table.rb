class CopyHealthDataIntoOneTable < ActiveRecord::Migration
=begin
Background info:
These 22 tables currently contain 307 non-timestamp, non-id fields.
74 date columns, 156 boolean columns, 60 integer columns, 23 string columns
There are currently 537,253 health info records, and 32,374 health history records.
So roughly 6% of consumers with health info have health history.
Even fewer will have any particular health condition.

Down sides:
  -health info table will gain columns that will rarely be filled
  -health condition detail fields will take up more space for the same data, since they will store serialized data, which will include both keys and values.
   (This matters less since they will so rarely store any data at all, and they will only store keys that have accompanying values.)

Up sides:
  -fewer queries will be made
  -fewer indices will be kept
  -fewer id, foreign key, and timestamp fields will be kept
  -health condition detail fields, as varchar fields, will be kept separately from the actual db rows, so they won't effect query speed unless they themselves are being searched
  -the database schema will be cleaner
  -storage of various health conditions will be more consistent, leading to a simpler API
=end

  def up
    #Add history records to their corresponding health info records.
    hhs=ActiveRecord::Base.connection.execute(%Q(
      SELECT * FROM crm_health_histories;
    )).to_a
    hh_column_names=ActiveRecord::Base.connection.execute(%Q(
      DESCRIBE crm_health_histories;
    )).to_a.map(&:first)
    hhs.in_groups_of(50) do |group|
      group.compact!
      group.each do |hh|
        attrs=Hash[hh_column_names.zip hh]
        hh_id=attrs['id']
        #Not copying `diabetes_neuropathy`,
        #as we have no front end field for it, and zero records with it marked true.
        #It also seems to overlap with the other two diabetes types.
        attrs=attrs.except('id','created_at','updated_at','diabetes_neuropathy').delete_if{|k,v| v.nil? || v==0 }
        next if attrs.blank?
        q_str=''
        attrs.each do |k,v|
          next if v.blank? && !v.is_a?(FalseClass)
          if v.is_a?(Date) || v.is_a?(String)
            q_str+=" #{k}=\"#{v}\", "
          else#false or an integer
            q_str+=" #{k}=#{v}, "
          end
        end
        q_str=q_str[0..(q_str.length-3)]
        execute(%Q(
          UPDATE crm_health_infos
          SET #{ q_str}
          WHERE health_history_id=#{hh_id};
        ))
      end
      Rails.env.production? ? sleep(4) : nil
    end

    hh_condition_names=[
      :alcohol_abuse,
      :anxiety,
      :arthritis,
      :asthma,
      :atrial_fibrillation,
      :cancer_breast,
      :cancer_prostate,
      :copd,
      :crohns,
      :depression,
      :diabetes_1,
      :diabetes_2,
      :drug_abuse,
      :epilepsy,
      :heart_murmur,
      :hepatitis_c,
      :irregular_heartbeat,
      :elevated_liver_function,
      :multiple_sclerosis,
      :parkinsons,
      :sleep_apnea,
      :stroke,
      :weight_reduction,
    ]

=begin
    For reference only. Will still move these boolean fields to health info.
    Previous migration still creates details fields for them as well.
    hh_fields_lacking_profile_table=[
      :mental_illness
      :emphysema
      :ulcerative_colitis_iletis
      :cancer_skin
      :cancer_internal
      :heart_attack
      :vascular_disease
    ]
=end

    #Add condition history records to their corresponding health info records.
    hh_condition_names.each do |condition_name|
      where_str=''
      diabetes_type=condition_name.match(/^diabetes_(.+)/)
      if diabetes_type
        if diabetes_type[1]=='neuropathy'
          next
        elsif diabetes_type[1]=='1'
          where_str="WHERE type=\"Crm::HealthHistory::DiabetesTypeOneProfile\""
        else
          where_str="WHERE type=\"Crm::HealthHistory::DiabetesTypeTwoProfile\""
        end
        condition_table="crm_health_history_diabetes_profiles"
      elsif condition_name==:elevated_liver_function
        condition_table="crm_health_history_liver_function_profiles"
      else
        condition_table="crm_health_history_#{condition_name.to_s}_profiles"
      end

      detail_records=execute(%Q(
        SELECT * FROM #{condition_table} #{where_str};
      )).to_a
      dr_column_names=execute(%Q(
        DESCRIBE #{condition_table};
      )).to_a.map(&:first)
      #Add health history records to their corresponding health info records.
      detail_records.in_groups_of(50) do |group|
        group.compact!
        group.each do |dr|
          attrs=Hash[dr_column_names.zip dr]
          hh_id=attrs['crm_health_history_id']
          next if hh_id.nil?#won't attempt to fix orphaned records.
          attrs=attrs.except('id','crm_health_history_id','created_at','updated_at','type').delete_if{|k,v| v.nil? || (v.is_a?(String) && v.blank?) }
          next if attrs.blank?
          attrs_json_str=attrs.to_json.gsub('"', '\"' )
          execute(%Q(
            UPDATE crm_health_infos
            SET #{ condition_name }_details= "#{ attrs_json_str }"
            WHERE health_history_id=#{hh_id};
          ))
        end
        sleep(4)
      end
    end

  end

  def down
    #Nothing to do here.
    #Earlier migration 20180719183813 added new columns.
    #Later migration 20180719183816 will remove the old columns and tables.
  end

end
