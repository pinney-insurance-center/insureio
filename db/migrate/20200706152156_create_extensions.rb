class CreateExtensions < ActiveRecord::Migration
  def change
    create_table :extensions do |t|
      t.integer :origin_id, null: false
      t.string :origin_type, null: false
      t.text :data
    end

    add_index :extensions, ["origin_id"], name: "extensions_origin", unique: true
  end
end
