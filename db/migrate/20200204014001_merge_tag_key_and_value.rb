%q{
  To test this migration:
  1. dump the prod db
  2. import the dump to a new db
    nohup mysql clone_dr_prod < dr-dump-2020-02-04.sql &
  3. open SSH tunnel to db server
    ssh -N -L $LOCALPORT:127.0.0.1:$REMOTEPORT drdb
  4. rake migration
    tic; RAILS_ENV=second bundle exec rake db:migrate; toc
}

class MergeTagKeyAndValue < ActiveRecord::Migration
  def down
    drop_table :tags
    create_table "tags", force: true do |t|
      t.integer "count", default: 0
      t.string  "key"
      t.string  "value"
    end

    remove_column :users, :lead_type
    remove_column :users, :referrer
    remove_column :users, :source

    create_table "tagging_tag_keys", force: true do |t|
      t.string "name"
    end

    create_table "tagging_tag_values", force: true do |t|
      t.string "value"
    end

    create_table "tagging_tags", force: true do |t|
      t.integer "tag_key_id"
      t.integer "tag_value_id"
      t.integer "tag_type_id"
      t.integer "user_id"
      t.integer "consumer_id"
    end
  end

  def up
    # Remove obsolete table
    drop_table :tagging_tags

    # Make new tags table
    drop_table :tags
    create_table "tags", force: true do |t|
      t.string :value
      t.string :person_type
      t.integer :person_id
      t.integer :tenant_id
      t.integer :owner_id
    end
    add_index :tags, [:value]

    # Update users table with tracking columns
    add_column :users, :lead_type, :string
    add_column :users, :referrer, :string
    add_column :users, :source, :string
  end
end
