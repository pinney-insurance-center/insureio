class ReplaceTaskPolymorphicIndexesWithPartialIndexes < ActiveRecord::Migration
  def up
    #To cover most queries for active tasks by person.
    add_index :tasks, "person_id", where: "(active= TRUE AND person_type = 'Consumer')", name: "tasks_by_consumer_pid"
    add_index :tasks, "person_id", where: "(active= TRUE AND person_type = 'User')",     name: "tasks_by_user_pid"

    #To cover most queries for active tasks by origin.
    add_index :tasks, "origin_id", where: "(active= TRUE AND origin_type = 'Crm::Status')",             name: "tasks_by_status_oid"
    add_index :tasks, "origin_id", where: "(active= TRUE AND origin_type = 'Marketing::Subscription')", name: "tasks_by_sub_oid"

    to_do_task_type_id=1

    #To cover most queries for active tasks by assigned user.
    add_index :tasks, ["assigned_to_id"], where: "(active= TRUE AND task_type_id= #{ to_do_task_type_id } )", name: "tasks_by_assigned_to_id"

    #To cover most queries for completed tasks by message.
    add_index :tasks, ["message_id"], where: "message_id IS NOT NULL", name: "tasks_by_message_id"

    remove_index :tasks, name: "index_tasks_on_person_id_and_person_type_and_active"
    remove_index :tasks, name: "index_tasks_on_origin_id_and_origin_type_and_evergreen"
    remove_index :tasks, name: "index_crm_tasks_on_assigned_to_id"
    remove_index :tasks, name: "index_crm_tasks_on_message_id"
  end

  def down
  end
end
