class AddDeletedToCarriers < ActiveRecord::Migration
  def change
    add_column :carriers, :deleted, :boolean, default: false
  end
end
