class MigrateHealthData < ActiveRecord::Migration
  def up
    # Map db cols (legacy names) to disease names
    col_names_to_disease_names = {
      cancer_breast: :breast_cancer,
      cancer_prostate: :prostate_cancer,
      cancer_skin: :other_skin_cancer,
      cancer_internal: :other_internal_cancer,
    }
    disease_name_set = Set.new col_names_to_disease_names.values
    Enum::DiseaseType.each do |disease_type|
      key = disease_type.name.to_sym
      next if col_names_to_disease_names.has_key? key
      next if disease_name_set.include? key
      disease_name_set << key
      next unless Crm::HealthInfo.columns_hash.has_key? key.to_s
      col_names_to_disease_names[key] = key
    end
    col_names_to_disease_names.freeze
    col_names = Crm::HealthInfo.column_names

    # Build SQL query for rows where diseases are present.
    # There are fewer than 6000 of these records.
    where_diseases_are_present = col_names_to_disease_names.map { |col_name, _disease_name|
      "#{col_name} = TRUE OR #{col_name}_details IS NOT NULL"
    }.join(' OR ')
    query = "select #{col_names.join(',')} from crm_health_infos where #{where_diseases_are_present}"
    raw_data = Crm::HealthInfo.connection.execute(query).map { |a| [a.first, a] }.to_h
    orm_data = Crm::HealthInfo.where(where_diseases_are_present)

    Crm::HealthInfo.transaction do
      orm_data.each do |health_info|
        raw_row = raw_data[health_info.id]
        raw_hash = col_names.zip(raw_row).to_h.with_indifferent_access
        col_names_to_disease_names.each do |col_name, disease_name|
          if details = raw_hash["#{col_name}_details"]
            begin
              details = JSON.parse(details).presence
            rescue TypeError => ex # Someone provided an illegal value, but it got saved to the db anyway
              health_info.diseases[disease_name] = raw_hash[col_name].presence
              next
            end
            health_info.diseases[disease_name] = details
          elsif bool_value = raw_hash[col_name]
            health_info.diseases[disease_name] = bool_value
          end
        end
        health_info.save!
      end
    end
  end

  # No `down` function is needed. This is a non-destructive data migration.
  # We can delete the now-unused columns after confirming no loss of data
  # following this migration.
end
