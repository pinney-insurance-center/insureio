class AddColumnsToHealthTable < ActiveRecord::Migration
=begin
This table is simply preparation for the next migration,
which copies data from the health history table and health history profiles tables.
The reasoning for this set of changes is layed out in the next migration file.
=end

  def up
    hh_conditions_needing_renaming=[
      [:elft,                        :elevated_liver_function],
      [:atrial_fibrillations,        :atrial_fibrillation],
      [:rheumatoid_arthritis,        :arthritis],
      [:weight_loss_surgery,         :weight_reduction],
      [:skin_cancer,                 :cancer_skin],
      [:internal_cancer,             :cancer_internal],
      [:breast_cancer,               :cancer_breast],
      [:prostate_cancer,             :cancer_prostate],
      [:heart_murmur_valve_disorder, :heart_murmur],
      [:irregular_heart_beat,        :irregular_heartbeat],
    ]
    #Rename boolean column names  to match condition profile names.
    hh_conditions_needing_renaming.each do |old_name, new_name|
      rename_column :crm_health_histories, old_name, new_name
    end

    rename_column :crm_health_infos, :criminal_detail, :criminal_further_detail

    hh_condition_names=[
      :alcohol_abuse,
      :anxiety,
      :arthritis,
      :asthma,
      :atrial_fibrillation,
      :cancer_breast,
      :cancer_prostate,
      :copd,
      :crohns,
      :depression,
      :diabetes_1,
      :diabetes_2,
      :drug_abuse,
      :epilepsy,
      :heart_murmur,
      :hepatitis_c,
      :irregular_heartbeat,
      :elevated_liver_function,
      :multiple_sclerosis,
      :parkinsons,
      :sleep_apnea,
      :stroke,
      :weight_reduction,
      #The following conditions lack profile tables, but will still get a details field for consistency.
      :mental_illness,
      :emphysema,
      :ulcerative_colitis_iletis,
      :cancer_skin,
      :cancer_internal,
      :heart_attack,
      :vascular_disease,
    ]
    #Create the columns.
    hh_condition_names.each do |condition_name|
      add_column :crm_health_infos, condition_name, :boolean
      add_column :crm_health_infos, "#{condition_name.to_s}_details", :text, limit:2_000
    end

    hi_condition_names=[
      :bp,
      :cholesterol,
      :foreign_travel,
      :moving_violation,
      :tobacco,
      :hazardous_avocation,
      :criminal
    ]
    hi_condition_names.each do |condition_name|
      add_column :crm_health_infos, "#{condition_name.to_s}_details", :text, limit:2_000, after: condition_name
    end
  end

  def down
    hh_condition_names=[
      :alcohol_abuse,
      :anxiety,
      :arthritis,
      :asthma,
      :atrial_fibrillation,
      :cancer_breast,
      :cancer_prostate,
      :copd,
      :crohns,
      :depression,
      :diabetes_1,
      :diabetes_2,
      :drug_abuse,
      :epilepsy,
      :heart_murmur,
      :hepatitis_c,
      :irregular_heartbeat,
      :elevated_liver_function,
      :multiple_sclerosis,
      :parkinsons,
      :sleep_apnea,
      :stroke,
      :weight_reduction,
      #The following conditions lack profile tables, but will still get a details field for consistency.
      :mental_illness,
      :emphysema,
      :ulcerative_colitis_iletis,
      :cancer_skin,
      :cancer_internal,
      :heart_attack,
      :vascular_disease,
    ]

    hh_condition_names.each do |condition_name|
      remove_column :crm_health_infos, condition_name
      remove_column :crm_health_infos, "#{condition_name.to_s}_details"
    end

    rename_column :crm_health_infos, :criminal_further_detail, :criminal_detail

    hi_condition_names=[
      :bp,
      :cholesterol,
      :foreign_travel,
      :moving_violation,
      :tobacco,
      :hazardous_avocation,
      :criminal
    ]
    hi_condition_names.each do |condition_name|
      remove_column :crm_health_infos, "#{condition_name.to_s}_details"
    end
  end

end
