class TextSubstitutionsInTemplatesAndReports < ActiveRecord::Migration

#This overridden class allows use of some handy `ActiveRecord` functionality without loading the entire class.
class Reporting::Search < ActiveRecord::Base
  class Data < HashWithIndifferentAccess
    def initialize hash={}
      merge! hash
      each{|k,v| self[k] = self.class.new(v) if v.is_a?(Hash) and not v.is_a?(self.class)}
    end

    def compact!
      delete_if do |k,v|
        v.compact! if v.is_a?(Data) && k!='tags'#need to exclude tags from the compaction, to allow tags with key and no value.
        v.blank? || v=='0' || v==0
      end
    end

    def to_s
      empty? ? "" : super
    end

    def zip
      self[:zip]
    end

  end

  serialize :criteria, Data
  serialize :fields_to_show, Data


  def criteria= hash
    self[:criteria] = Data.new hash
  end

  def fields_to_show= hash
    super Data.new(hash)
  end

end

  def update_templates reverse=false
    old_values=[
      '{{client.',
      '{{ client.',
      '{{profile.',
      '{{ profile.',
      '{{ user.license_info',
      '{{email.unsubscribe',
      '{{ email.unsubscribe',
    ]
    new_values=[
      '{{ recipient.',
      '{{ recipient.',
      '{{ brand.',
      '{{ brand.',
      '{{ license_info',
      '{{ unsubscribe_link',
      '{{ unsubscribe_link',
    ]

    unless reverse
      from_values=old_values
      to_values  =new_values
    else
      from_values=new_values
      to_values  =old_values
    end

    #Performs 2 update queries per value to be replaced.
    from_values.each_with_index do |from_value,i|
      to_value=to_values[i]
      execute %Q(
        UPDATE marketing_templates
        SET    body=REPLACE( body, '#{from_value}', '#{to_value}' )
        WHERE  body LIKE '%#{from_value}%'
      )
      execute %Q(
        UPDATE marketing_templates
        SET    subject=REPLACE( subject, '#{from_value}', '#{to_value}' )
        WHERE  subject LIKE '%#{from_value}%'
      )
    end
  end

  def update_reports reverse=false
    old_keys=[
      'client',
      'contact',
      'annual_premium',
      'agent_contact',
      ['consumer','profile_id'],
    ]
    new_keys=[
      'consumer',
      'consumer',
      'annualized_premium',
      'agent',
      ['consumer','brand_id'],
    ]

    unless reverse
      from_keys=old_keys
      to_keys  =new_keys
    else
      from_keys=new_keys
      to_keys  =old_keys
    end

    #It is safe to assume they all contain these keys.
    #There are also not enough reports to justify loading them in batches.
    reports=Reporting::Search.all
    #This loop manipulates the objects in memory and makes one update request per report record.
    #At the time of the writing of this migration, there are 393 report records in production.
    reports.each do |r|
      from_keys.each_with_index do |from_key,i|
        to_key=to_keys[i]
        [r.criteria,r.fields_to_show].each do |obj|
          if from_key.is_a?(String) && obj.has_key?(from_key)
            obj[to_key]||=Reporting::Search::Data.new {}
            obj[to_key]=obj[to_key].merge( obj[from_key] )
            obj.delete from_key
          elsif from_key.is_a?(Array) && obj[from_key[0]] && obj[from_key[0]].has_key?(from_key[1])
            obj[to_key[0]][to_key[1]]=obj[from_key[0]][from_key[1]]
            obj[from_key[0]].delete from_key[1]
          end
        end
      end
      r.save
    end

  end


  def up
    update_templates
    rename_column :reporting_searches, :json, :criteria
    update_reports
  end

  def down
    update_templates true
    rename_column :reporting_searches, :criteria, :json
    update_reports true
  end

end
