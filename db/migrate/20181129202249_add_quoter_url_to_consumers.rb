class AddQuoterUrlToConsumers < ActiveRecord::Migration
  def up
    #Must check name, since the refactor is being worked in parallel
    #and may enter a given environment before or after this migration.
    table_name= table_exists?(:consumers) ? :consumers : :crm_connections
    add_column table_name, :quoter_url, :string, after: :ip_address
  end

  def down
    table_name= table_exists?(:consumers) ? :consumers : :crm_connections
    remove_column table_name, :quoter_url
  end
end
