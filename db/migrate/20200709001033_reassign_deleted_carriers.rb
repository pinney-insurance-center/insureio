class ReassignDeletedCarriers < ActiveRecord::Migration
  REASSIGNMENTS = { 115 => 50, 48 => 4, 61 => 1 }

  def reassign klass
    klass.where(carrier_id: REASSIGNMENTS.keys).each do |record|
      record.to_sql_file # Save SQL for re-creating the original record
      record.carrier_id = REASSIGNMENTS[record.carrier_id]
      record.save!
    end
  end

  def up
    # There are only 55 of these
    reassign Quoting::Quote
    # There are only 13 of these
    reassign ExcludedCarrier
    # There are only 0 of these
    reassign Processing::ExamOne::ControlCode
    # There are only 0 of these
    reassign Processing::Marketech::Carrier
    # There are only 0 of these
    reassign Processing::Smm::Carrier
    # There are only 0 of these
    reassign Usage::Contract
    # There are only 0 of these
    Processing::Igo::CarrierProduct.where(dr_carrier_id: REASSIGNMENTS.keys).each do |carrier_product|
      carrier_product.to_sql_file # Save SQL for re-creating the original record
      carrier_product.dr_carrier_id = REASSIGNMENTS[carrier_product.dr_carrier_id]
      carrier_product.save!
    end
  end
end
