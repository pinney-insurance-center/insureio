class ReplaceMarketechLinkLiquidField < ActiveRecord::Migration
  def up
    update_data 'body'
  end

  def down
    update_data 'body',    true
  end

  def update_data field, reverse=false
    old_values=[
      '{{ marketech_email_link }}',
      '{{marketech_email_link}}'
    ]
    new_values=[
      '{{ marketech_esign_url }}',
      '{{ marketech_esign_url }}'
    ]

    unless reverse
      from_values=old_values
      to_values  =new_values
    else
      from_values=new_values
      to_values  =old_values
    end

    from_values.each_with_index do |from_value,i|
      to_value=to_values[i]
      execute %Q(
        UPDATE marketing_templates
        SET    #{field}=REPLACE( #{field}, '#{from_value}', '#{to_value}' )
        WHERE  #{field} LIKE '%#{from_value}%'
      )
    end
  end
end
