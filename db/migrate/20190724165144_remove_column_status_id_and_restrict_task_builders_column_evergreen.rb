class RemoveColumnStatusIdAndRestrictTaskBuildersColumnEvergreen < ActiveRecord::Migration
  def up
    remove_column :crm_cases,       :status_id
    remove_column :quoting_details, :status_id

    execute(%Q(
      UPDATE crm_task_builders
      SET    evergreen=0
      WHERE  evergreen IS NULL
    ))
    change_column :crm_task_builders, :evergreen, :boolean, default: false, null: false

    execute(%Q(
      UPDATE tasks
      SET    evergreen=0
      WHERE  evergreen IS NULL
    ))
    change_column :tasks,             :evergreen, :boolean, default: false, null: false
  end

  def down
    add_column :crm_cases,       :status_id, :integer
    add_column :quoting_details, :status_id, :integer

    #Repopulating these ids would be an enormously long running process.
    #If we were to go back to that, it would be better simply to restore the database from a backup.

    change_column :crm_task_builders, :evergreen, :boolean, default: nil,   null: true
    change_column :tasks,             :evergreen, :boolean, default: false, null: true
  end
end
