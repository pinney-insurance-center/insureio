class ReassociateContactMethods < ActiveRecord::Migration
  #This class is being stubbed here
  #so we can make use of `ActiveRecord` functionality
  #without invoking any app code.
  class Contact < ActiveRecord::Base
  end

  def up
    contact_method_table_names=[:addresses,:email_addresses,:phones,:webs]

    contact_method_table_names.each do |t|
      add_column t, :contactable_type, :string, limit:20, null:false unless column_exists?(t, :contactable_type)
      add_column t, :contactable_id,   :integer, null:false          unless column_exists?(t, :contactable_id)
      add_index  t, [:contactable_id, :contactable_type]
    end

    #Performs about 4N/200 or N/50 update queries for N contact rows.
    #There are currently about 770k contact records in production, so about 15k update queries.
    #Does not instantiate any records in Rails.
    ['Crm::Connection','Usage::User','Usage::Profile'].each do |person_type|
      cids=Contact.where(person_type:person_type).pluck(:id)
      cids.in_groups_of(200) do |g|
        g.compact!
        contact_method_table_names.each do |t|
          execute(%Q(
            UPDATE #{t}
            SET contactable_type="#{person_type}"
            WHERE contact_id IN (#{g.join(',')})
          ))
        end
      end
    end

    #Performs 4N/100 or N/25 update queries for N contact rows.
    #Is expected to be very slow.
    Contact.select([:id,:person_id]).in_groups_of(100) do |g|
      g.compact!
      query_fragment=''
      c_ids=g.map{|c| c.id.to_i }
      g.each do |c|
        query_fragment+="WHEN contact_id=#{c.id} THEN #{c.person_id}\n" if c.person_id.present?
      end
      next unless query_fragment.present?
      contact_method_table_names.each do |t|
        execute(%Q(
          UPDATE #{t}
          SET contactable_id=CASE
          #{query_fragment}
          END
          WHERE contact_id IN (#{c_ids.join(',')})
        ))
      end
      sleep(4) if Rails.env.production?#So it allows other queries through while performing the updates.
    end

  end

  def down

     [:addresses,:email_addresses,:phones,:webs].each do |t|
       remove_column t, :contactable_type
       remove_column t, :contactable_id
     end

  end
end
