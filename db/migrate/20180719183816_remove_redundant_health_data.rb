class RemoveRedundantHealthData < ActiveRecord::Migration
=begin
Background info:
These 22 tables currently contain 307 non-timestamp, non-id fields.
diagnosed_date x20, condition_degree x15, 74 date columns, 156 boolean columns, 60 integer columns, 23 string columns
There are currently 537,253 health info records, and 32,374 health history records.
So roughly 6% of consumers with health info have health history.
Even fewer will have any particular health condition.

Down sides:
  -health info table will gain columns that will rarely be filled
  -health condition detail fields will take up more space for the same data, since they will store serialized data, which will include both keys and values.
   (This matters less since they will so rarely store any data at all, and they will only store keys that have accompanying values.)

Up sides:
  -fewer queries will be made
  -fewer indices will be kept
  -fewer id, foreign key, and timestamp fields will be kept
  -health condition detail fields, as varchar fields, will be kept separately from the actual db rows, so they won't effect query speed unless they themselves are being searched
  -the database schema will be cleaner
  -storage of various health conditions will be more consistent, leading to a simpler API
=end

  def up
    hh_condition_names=[
      :alcohol_abuse,
      :anxiety,
      :arthritis,
      :asthma,
      :atrial_fibrillation,
      :cancer_breast,
      :cancer_prostate,
      :copd,
      :crohns,
      :depression,
      :diabetes,#diabetes_1 and diabetes_2 don't have their own tables in existing schema.
      :drug_abuse,
      :epilepsy,
      :heart_murmur,
      :hepatitis_c,
      :irregular_heartbeat,
      :liver_function,
      :multiple_sclerosis,
      :parkinsons,
      :sleep_apnea,
      :stroke,
      :weight_reduction,
    ]

    remove_column :crm_health_infos, :health_history_id if column_exists?(:crm_health_infos, :health_history_id)
    remove_column :crm_health_infos, :moving_violation_history_id if column_exists?(:crm_health_infos, :moving_violation_history_id)
    remove_column :crm_health_infos, :hazardous_avocation_detail  if column_exists?(:crm_health_infos, :hazardous_avocation_detail)
    drop_table :crm_health_histories if table_exists? :crm_health_histories
    drop_table :crm_moving_violation_histories if table_exists? :crm_moving_violation_histories

    hh_condition_names.each do |condition_name|
      condition_table="crm_health_history_#{condition_name.to_s}_profiles"
      drop_table condition_table if table_exists? condition_table
    end

    hi_condition_names=[
      :bp,
      :cholesterol,
      :foreign_travel,
      :moving_violation,
      :tobacco,
      :hazardous_avocation,
      :criminal
    ]
    hi_condition_names.each do |condition_name|
      hc_class="Crm::HealthConditionDetail::#{condition_name.to_s.camelize}".constantize
      columns_to_remove=hc_class::FIELDS.map(&:first)

      columns_to_remove.each do |col|
        column_name="#{condition_name}_#{col}".to_s.sub('foreign_travel_','travel_')
        remove_column :crm_health_infos, column_name if column_exists?(:crm_health_infos, column_name)
      end
    end

  end

  def down

    create_table "crm_moving_violation_histories", :force => true do |t|
      t.integer  "last_6_mo"
      t.integer  "last_1_yr"
      t.integer  "last_2_yr"
      t.integer  "last_3_yr"
      t.integer  "last_5_yr"
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false
    end

    create_table "crm_health_histories", :force => true do |t|
      t.boolean  "diabetes_1"
      t.boolean  "diabetes_2"
      t.boolean  "diabetes_neuropathy"
      t.boolean  "anxiety"
      t.boolean  "depression"
      t.boolean  "epilepsy"
      t.boolean  "parkinsons"
      t.boolean  "mental_illness"
      t.boolean  "alcohol_abuse"
      t.boolean  "drug_abuse"
      t.boolean  "elevated_liver_function"
      t.boolean  "hepatitis_c"
      t.boolean  "arthritis"
      t.boolean  "asthma"
      t.boolean  "copd"
      t.boolean  "emphysema"
      t.boolean  "sleep_apnea"
      t.boolean  "crohns"
      t.boolean  "ulcerative_colitis_iletis"
      t.boolean  "weight_reduction"
      t.boolean  "cancer_breast"
      t.boolean  "cancer_prostate"
      t.boolean  "cancer_skin"
      t.boolean  "cancer_internal"
      t.boolean  "atrial_fibrillation"
      t.boolean  "heart_murmur"
      t.boolean  "irregular_heartbeat"
      t.boolean  "heart_attack"
      t.boolean  "stroke"
      t.boolean  "vascular_disease"
      t.datetime "created_at",                :null => false
      t.datetime "updated_at",                :null => false
      t.boolean  "multiple_sclerosis"
    end

    create_table "crm_health_history_alcohol_abuse_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.boolean  "currently_consuming"
      t.date     "last_consumed_date"
      t.boolean  "ever_treated"
      t.boolean  "currently_treated"
      t.date     "treatment_end_date"
      t.boolean  "ever_relapse"
      t.date     "last_relapse_date"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_anxiety_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_hospitalized"
      t.date     "last_hospitalized_date"
      t.integer  "episodes_year"
      t.boolean  "currently_treated"
      t.integer  "medication_count"
      t.boolean  "outpatient_care"
      t.boolean  "inpatient_care"
      t.boolean  "work_absence"
      t.integer  "absence_count"
      t.boolean  "ever_suicide_attempt"
      t.date     "last_suicide_attempt_date"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                :null => false
      t.datetime "updated_at",                :null => false
    end

    create_table "crm_health_history_arthritis_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_suppressants"
      t.boolean  "current_suppressants"
      t.boolean  "ever_prednisone"
      t.boolean  "current_prednisone"
      t.boolean  "ever_immunosuppressants"
      t.boolean  "currently_immunosuppressants"
      t.boolean  "good_treatment_response"
      t.boolean  "complications"
      t.date     "ever_surgery"
      t.boolean  "disabled"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                   :null => false
      t.datetime "updated_at",                   :null => false
    end

    create_table "crm_health_history_asthma_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_treated"
      t.boolean  "currently_treated"
      t.boolean  "inhaled_bronchodilators"
      t.boolean  "inhaled_corticosteroids"
      t.boolean  "oral_medication_no_steroids"
      t.boolean  "oral_medication_steroids"
      t.boolean  "rescue_inhaler"
      t.boolean  "ever_hospitalized"
      t.integer  "hospitalized_count_past_year"
      t.integer  "hospitalized_count"
      t.date     "last_hospitalized_date"
      t.integer  "episodes_year"
      t.boolean  "work_absence"
      t.integer  "absence_count"
      t.float    "fev1"
      t.boolean  "ever_life_threatening"
      t.date     "last_life_threatening_date"
      t.boolean  "is_treatment_compliant"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                   :null => false
      t.datetime "updated_at",                   :null => false
    end

    create_table "crm_health_history_atrial_fibrillation_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.date     "last_episode_date"
      t.integer  "episode_length"
      t.boolean  "self_resolve"
      t.boolean  "ever_cardiac_eval"
      t.date     "cardiac_eval_date"
      t.boolean  "cardiac_eval_result"
      t.boolean  "heart_disease"
      t.boolean  "shortness_of_breath"
      t.boolean  "current_medications"
      t.boolean  "ablation_procedure"
      t.date     "ablation_date"
      t.boolean  "ablation_result"
      t.boolean  "is_controlled"
      t.integer  "crm_health_history_id"
      t.string   "fibrillation_type"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_cancer_breast_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.integer  "cancer_stage"
      t.text     "condition_degree"
      t.integer  "node_count"
      t.boolean  "metastatis"
      t.integer  "tumorsize"
      t.date     "last_treatment_date"
      t.boolean  "reoccurrence"
      t.date     "last_mammogram_date"
      t.boolean  "currently_treating"
      t.boolean  "ductal_carcinoma"
      t.boolean  "lobular_carcinoma"
      t.boolean  "diagnosed_dcis"
      t.boolean  "dcis_removed"
      t.integer  "number_of_lesions"
      t.integer  "lesion_size"
      t.boolean  "comedonecrosis"
      t.boolean  "diagnosed_lcis"
      t.boolean  "lumpectomy"
      t.boolean  "mastectomy"
      t.boolean  "single_mastectomy"
      t.boolean  "double_mastectomy"
      t.boolean  "negative_sentinel_lymph_exam"
      t.boolean  "radiation_treatment"
      t.boolean  "endocrine_therapy"
      t.boolean  "endocrine_treated"
      t.integer  "cancer_grade"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                   :null => false
      t.datetime "updated_at",                   :null => false
    end

    create_table "crm_health_history_cancer_prostate_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.integer  "cancer_stage"
      t.boolean  "prostatectomy"
      t.date     "prostatectomy_date"
      t.boolean  "radiation"
      t.boolean  "radiation_currently"
      t.boolean  "watchful_waiting"
      t.integer  "gleason_score"
      t.float    "pre_psa"
      t.float    "post_psa"
      t.boolean  "metastasis"
      t.date     "last_treatment_date"
      t.boolean  "reoccurrence"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_copd_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.integer  "fev1"
      t.boolean  "has_symptoms"
      t.string   "copd_severity"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_crohns_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.date     "stabilization_date"
      t.date     "last_attack_date"
      t.boolean  "ever_steroid"
      t.boolean  "currently_steroids"
      t.date     "steroid_stop_date"
      t.boolean  "ever_immuno_suppressants"
      t.boolean  "current_immuno_suppressants"
      t.date     "immunosuppressants_stop_date"
      t.boolean  "limited_to_colon"
      t.boolean  "complications"
      t.boolean  "surgery"
      t.date     "surgery_date"
      t.boolean  "weight_stable"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                   :null => false
      t.datetime "updated_at",                   :null => false
    end

    create_table "crm_health_history_depression_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_hospitalized"
      t.date     "last_hospitalized"
      t.boolean  "currently_treated"
      t.integer  "medication_count"
      t.boolean  "in_psychotherapy"
      t.boolean  "responding_well"
      t.date     "last_treatment_date"
      t.boolean  "treatment_completed"
      t.boolean  "work_absence"
      t.integer  "absence_count"
      t.boolean  "ever_suicide_attempt"
      t.date     "last_suicide_attempt_date"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                :null => false
      t.datetime "updated_at",                :null => false
    end

    create_table "crm_health_history_diabetes_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.float    "last_a1_c"
      t.float    "average_a1_c"
      t.boolean  "complications"
      t.boolean  "currently_treated"
      t.date     "last_treatment_date"
      t.boolean  "is_gestational"
      t.integer  "insulin_units"
      t.string   "type"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_drug_abuse_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.boolean  "currently_using"
      t.date     "last_used_date"
      t.boolean  "ever_treated"
      t.boolean  "currently_treated"
      t.date     "treatment_end_date"
      t.boolean  "ever_relapse"
      t.date     "last_relapse_date"
      t.boolean  "ever_convicted"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_epilepsy_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_treated"
      t.boolean  "ever_surgery"
      t.date     "surgery_date"
      t.boolean  "controlled_seizures"
      t.boolean  "neurological_evaluation"
      t.boolean  "neurological_normal"
      t.boolean  "caused_by_other"
      t.date     "last_seizure_date"
      t.integer  "seizures_per_year"
      t.integer  "thirty_minute_plus"
      t.boolean  "ever_medication"
      t.string   "seizure_type"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",              :null => false
      t.datetime "updated_at",              :null => false
    end

    create_table "crm_health_history_heart_murmur_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_echocardiogram"
      t.boolean  "valve_structures_normal"
      t.boolean  "heart_enlargement"
      t.boolean  "symptomatic"
      t.boolean  "progression"
      t.boolean  "valve_surgery"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",              :null => false
      t.datetime "updated_at",              :null => false
    end

    create_table "crm_health_history_hepatitis_c_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.date     "contraction_date"
      t.string   "condition_degree"
      t.boolean  "normal_viral_loads"
      t.date     "normal_viral_date"
      t.date     "normal_liver_date"
      t.boolean  "normal_liver_functions"
      t.boolean  "ever_treated"
      t.boolean  "currently_treated"
      t.date     "treatment_start_date"
      t.date     "treatment_end_date"
      t.boolean  "liver_cirrhosis"
      t.boolean  "liver_biopsy"
      t.boolean  "complications"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",             :null => false
      t.datetime "updated_at",             :null => false
    end

    create_table "crm_health_history_irregular_heartbeat_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.boolean  "underlying_heart_disease"
      t.boolean  "current_symptoms"
      t.boolean  "current_medications"
      t.integer  "medication_count"
      t.boolean  "atrioventricular_block"
      t.boolean  "second_degree_av_block"
      t.boolean  "third_degree_av_block_atr_disassociation"
      t.boolean  "born_with_third_degree_av_block"
      t.boolean  "pacemaker_implanted"
      t.date     "pacemaker_implant_date"
      t.boolean  "attrioventricular_junctional_rhythm"
      t.boolean  "paroxysmal_super_tachycardia"
      t.boolean  "cardiac_evaluations"
      t.boolean  "cardiac_eval_result_normal"
      t.date     "last_experience_symptoms"
      t.integer  "symptoms_per_year"
      t.boolean  "premature_atrial_complexes"
      t.boolean  "history_of_cardiovascular_disease"
      t.boolean  "premature_ventricular_contraction"
      t.boolean  "simple_pvc"
      t.boolean  "complex_pvc"
      t.boolean  "require_treatment_for_pvc"
      t.boolean  "sick_sinus_syndrome"
      t.boolean  "pacemaker_for_sick_sinus_syndrome"
      t.date     "pacemaker_for_sick_sinus_syndrome_implant_date"
      t.boolean  "history_of_fainting"
      t.boolean  "sinus_bradycardia"
      t.integer  "pulse_rate"
      t.boolean  "sinus_bradycardia_caused_by_another_condition"
      t.boolean  "sinus_bradycardia_caused_by_medication"
      t.boolean  "sinus_bradycardia_cause_unknown"
      t.boolean  "wandering_pacemaker"
      t.boolean  "cardiac_eval_for_wandering_pacemaker"
      t.boolean  "idoventricular_rhythm"
      t.integer  "mobitz_type_1_block"
      t.integer  "mobitz_type_2_block"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                                     :null => false
      t.datetime "updated_at",                                     :null => false
    end

    create_table "crm_health_history_liver_function_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.integer  "alt"
      t.integer  "ast"
      t.integer  "ggtp"
      t.boolean  "is_stable"
      t.boolean  "is_hepatitis_negative"
      t.boolean  "is_cdt_negative"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_multiple_sclerosis_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.integer  "attacks_per_year"
      t.date     "last_attack_date"
      t.string   "condition_type"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_parkinsons_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.integer  "age_at_diagnosis"
      t.string   "condition_degree"
      t.boolean  "live_independently"
      t.boolean  "condition_stable"
      t.boolean  "currently_disabled"
      t.string   "disabled_severity"
      t.boolean  "currently_receive_treatment"
      t.boolean  "rigidity"
      t.string   "rigidity_severity"
      t.date     "stable_date"
      t.boolean  "walking_impairment"
      t.string   "walking_impairment_severity"
      t.boolean  "mental_deterioration"
      t.boolean  "affect_fingers_only"
      t.boolean  "affect_hands_only"
      t.boolean  "affect_multiple_areas"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",                  :null => false
      t.datetime "updated_at",                  :null => false
    end

    create_table "crm_health_history_sleep_apnea_profiles", :force => true do |t|
      t.date     "diagnosed_date"
      t.string   "condition_degree"
      t.boolean  "ever_treated"
      t.date     "treatment_start_date"
      t.boolean  "use_cpap"
      t.integer  "rd_index"
      t.integer  "apnea_index"
      t.integer  "ah_index"
      t.integer  "o2_saturation"
      t.boolean  "cpap_machine_complaint"
      t.boolean  "sleep_study"
      t.boolean  "sleep_study_followup"
      t.boolean  "on_oxygen"
      t.boolean  "currently_sleep_apnea"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",             :null => false
      t.datetime "updated_at",             :null => false
    end

    create_table "crm_health_history_stroke_profiles", :force => true do |t|
      t.string   "condition_degree"
      t.date     "last_stroke_date"
      t.boolean  "multiple_strokes"
      t.integer  "first_stroke_age"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    create_table "crm_health_history_weight_reduction_profiles", :force => true do |t|
      t.date     "procedure_date"
      t.integer  "prior_weight"
      t.date     "current_weight_date"
      t.boolean  "any_complications"
      t.string   "procedure_type"
      t.integer  "crm_health_history_id"
      t.datetime "created_at",            :null => false
      t.datetime "updated_at",            :null => false
    end

    hh_condition_names=[
      :alcohol_abuse,
      :anxiety,
      :arthritis,
      :asthma,
      :atrial_fibrillation,
      :cancer_breast,
      :cancer_prostate,
      :copd,
      :crohns,
      :depression,
      :diabetes,
      :drug_abuse,
      :epilepsy,
      :heart_murmur,
      :hepatitis_c,
      :irregular_heartbeat,
      :elevated_liver_function,
      :multiple_sclerosis,
      :parkinsons,
      :sleep_apnea,
      :stroke,
      :weight_reduction,
    ]
    his=execute(%Q(
      SELECT * FROM crm_health_infos;
    )).to_a
    his.in_groups_of(50) do |group|
      group.compact!
      group.each do |hi|
        attrs=hi.attributes.except('id','created_at','updated_at')
        q_str=''
        attrs.each do |k,v|
          next if v.blank?
          if v.is_a?(Date) or v.is_a?(String)
            q_str+=" #{k}=\"#{v}\" "
          else
            q_str+=" #{k}=#{v} "
          end
        end
        execute(%Q(
          INSERT INTO crm_health_histories
          SET #{ q_str};
        ))
      end
      Rails.env.production? ? sleep(4) : nil
    end
  end

end
