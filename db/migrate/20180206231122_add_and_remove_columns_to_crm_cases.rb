class AddAndRemoveColumnsToCrmCases < ActiveRecord::Migration
  def up
    add_column    :crm_cases, :date_signed_by_consumer, :datetime, after: :scor_guid
    add_column    :crm_cases, :date_signed_by_owner,    :datetime, after: :date_signed_by_consumer
    add_column    :crm_cases, :date_signed_by_payer,    :datetime, after: :date_signed_by_owner

    remove_column :crm_cases, :owner_id
    remove_column :crm_cases, :premium_payer_id
    remove_column :crm_cases, :insured_is_owner
    remove_column :crm_cases, :insured_is_premium_payer
    remove_column :crm_cases, :owner_is_premium_payer
    remove_column :crm_cases, :owner_is_beneficiary
  end

  def down
    add_column    :crm_cases, :owner_id,                :integer
    add_column    :crm_cases, :premium_payer_id,        :integer
    add_column    :crm_cases, :insured_is_owner,        :boolean, default:true
    add_column    :crm_cases, :insured_is_premium_payer,:boolean, default:true
    add_column    :crm_cases, :owner_is_premium_payer,  :boolean
    add_column    :crm_cases, :owner_is_beneficiary,    :boolean

    remove_column :crm_cases, :date_signed_by_consumer, :datetime, after: :scor_guid
    remove_column :crm_cases, :date_signed_by_owner,    :datetime, after: :date_signed_by_consumer
    remove_column :crm_cases, :date_signed_by_payer,    :datetime, after: :date_signed_by_owner
  end
end
