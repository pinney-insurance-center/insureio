class RenameColumnsForClarity < ActiveRecord::Migration
  def change
    rename_column :contacts,         :genre_id,                      :entity_type_id
    rename_column :quoting_details,  :category_id,                   :duration_id
    rename_column :crm_health_infos, :last_bp_treatment,             :bp_last_treatment
    rename_column :crm_health_infos, :last_cholesterol_treatment,    :cholesterol_last_treatment
    rename_column :crm_health_infos, :travel_where,                  :travel_country_detail
    rename_column :crm_health_infos, :last_dui_dwi,                  :moving_violation_last_dui_dwi
    rename_column :crm_health_infos, :last_dl_suspension,            :moving_violation_last_dl_suspension
    rename_column :crm_health_infos, :last_reckless_driving,         :moving_violation_last_reckless_driving
    rename_column :crm_health_infos, :penultimate_car_accident,      :moving_violation_penultimate_car_accident
    rename_column :crm_health_infos, :cigarettes_current,            :tobacco_cigarettes_current
    rename_column :crm_health_infos, :last_cigarette,                :tobacco_cigarette_last
    rename_column :crm_health_infos, :cigarettes_per_day,            :tobacco_cigarettes_per_day
    rename_column :crm_health_infos, :cigars_current,                :tobacco_cigars_current
    rename_column :crm_health_infos, :last_cigar,                    :tobacco_cigar_last
    rename_column :crm_health_infos, :cigars_per_month,              :tobacco_cigars_per_month
    rename_column :crm_health_infos, :nicotine_patch_or_gum_current, :tobacco_nicotine_patch_or_gum_current
    rename_column :crm_health_infos, :last_nicotine_patch_or_gum,    :tobacco_nicotine_patch_or_gum_last
    rename_column :crm_health_infos, :pipe_current,                  :tobacco_pipe_current
    rename_column :crm_health_infos, :last_pipe,                     :tobacco_pipe_last
    rename_column :crm_health_infos, :pipes_per_year,                :tobacco_pipes_per_year
    rename_column :crm_health_infos, :last_tobacco_chewed,           :tobacco_chewed_last
  end
end
