class FixReportsWithInvalidFieldsToShow < ActiveRecord::Migration
  def up
    update_data 'fields_to_show'
  end

  def down
    update_data 'fields_to_show',    true
  end

  def update_data field, reverse=false
    old_values=[
      'profile_',
      'phone_string',
      'address_string',
    ]
    new_values=[
      'brand_',
      'phone',
      'address',
    ]

    unless reverse
      from_values=old_values
      to_values  =new_values
    else
      from_values=new_values
      to_values  =old_values
    end

    from_values.each_with_index do |from_value,i|
      to_value=to_values[i]
      execute %Q(
        UPDATE reporting_searches
        SET    #{field}=REPLACE( #{field}, '#{from_value}', '#{to_value}' )
        WHERE  #{field} LIKE '%#{from_value}%'
      )
    end
  end

end
