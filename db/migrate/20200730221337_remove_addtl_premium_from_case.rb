class RemoveAddtlPremiumFromCase < ActiveRecord::Migration
  def up
    remove_column :crm_cases, :flat_extra
    remove_column :crm_cases, :flat_extra_years
    remove_column :crm_cases, :amount_for_1035
    remove_column :crm_cases, :needs_1035
  end

  def down
    add_column :crm_cases, :needs_1035, :boolean
    add_column :crm_cases, :amount_for_1035, :decimal, precision: 10, scale: 2
    add_column :crm_cases, :flat_extra, :decimal, precision: 10, scale: 2
    add_column :crm_cases, :flat_extra_years, :decimal, precision: 10, scale: 2
  end
end
