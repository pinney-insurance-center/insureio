class EnsureQuotesAndCasesNeverHaveNullSalesStage < ActiveRecord::Migration
  def up
  	execute(%Q(
  		UPDATE crm_cases
  		SET    sales_stage_id=1
  		WHERE  sales_stage_id IS NULL
  	))
  	execute(%Q(
  		UPDATE quoting_details
  		SET    sales_stage_id=1
  		WHERE  sales_stage_id IS NULL
  	))
  	change_column :crm_cases,      :sales_stage_id, :integer, default: 1, null:false
  	change_column :quoting_details, :sales_stage_id, :integer, default: 1, null:false
  end

  def down
  	change_column :crm_cases,      :sales_stage_id, :integer, default: 1
  	change_column :quoting_details, :sales_stage_id, :integer, default: 1
  end
end
