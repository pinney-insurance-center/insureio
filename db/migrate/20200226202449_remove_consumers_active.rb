class RemoveConsumersActive < ActiveRecord::Migration
  def up
    remove_column :consumers, :active
  end
  def down
    add_column :consumers, :active, :boolean, default:true
    execute(%Q(
      UPDATE consumers
      SET    active=FALSE
      WHERE  consumers.active_or_enabled IS FALSE
    ))
  end
end
