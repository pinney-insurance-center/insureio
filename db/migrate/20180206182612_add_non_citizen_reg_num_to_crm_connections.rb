class AddNonCitizenRegNumToCrmConnections < ActiveRecord::Migration
  def change
    add_column :crm_connections, :citizenship_noncitizen_registration_number, :string, after: :citizenship_id
  end
end
