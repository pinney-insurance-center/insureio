class RemoveDuplicatePhoneRecordings < ActiveRecord::Migration
  def up
    # This gets rid of duplicate items in the db (inserted at different times,
    # probably by a periodic job which no longer exists but examined files on
    # the 3CX server). But it does not get rid of spurious entries (which are
    # probably the result of a test), such as one phone call for Brad, which
    # supposedly had hundreds of participants.
    ActiveRecord::Base.connection.execute 'CREATE TABLE tmp_phone_recs LIKE phone_recordings'
    remove_column :tmp_phone_recs, :description
    remove_column :tmp_phone_recs, :created_by_id
    remove_column :tmp_phone_recs, :updated_by_id
    # This should take 2-3 minutes...
    ActiveRecord::Base.connection.execute 'INSERT INTO tmp_phone_recs (person_id, person_type, s3_filepath, created_at, updated_at) SELECT person_id, person_type, s3_filepath, created_at, min(updated_at) FROM phone_recordings GROUP BY CONCAT(person_id, person_type, s3_filepath, created_at)'
    ActiveRecord::Base.connection.execute 'DROP TABLE phone_recordings'
    ActiveRecord::Base.connection.execute 'RENAME TABLE tmp_phone_recs TO phone_recordings'
  end
end
