class AddColumnSentByHostToMarketingMessages < ActiveRecord::Migration
  def change
    add_column :marketing_messages, :sent_by_host, :string, after: :sent
    add_column :marketing_messages, :failure_msgs, :string, after: :failed_attempts
  end
end
