class AddCreateUpdateCreditAndDescriptionToAttachmentsAndRecordings < ActiveRecord::Migration
  def up
    rename_column :attachments, :attachment, :file_name

    add_column :attachments, :created_by_id, :integer, after: :created_at
    add_column :attachments, :updated_by_id, :integer, after: :updated_at
    add_column :attachments, :description,   :string

    rename_table :communication_tcx_recordings, :phone_recordings

    add_column :phone_recordings, :created_by_id, :integer, after: :created_at
    add_column :phone_recordings, :updated_by_id, :integer, after: :updated_at
    add_column :phone_recordings, :description,   :string
  end

  def down
    rename_column :attachments, :file_name, :attachment

    remove_column :attachments, :created_by_id
    remove_column :attachments, :updated_by_id
    remove_column :attachments, :description

    remove_column :phone_recordings, :created_by_id
    remove_column :phone_recordings, :updated_by_id
    remove_column :phone_recordings, :description

    rename_table :phone_recordings, :communication_tcx_recordings
  end
end
