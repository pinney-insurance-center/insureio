class NoDbRestrictionOnOpportunities < ActiveRecord::Migration
  def up
    remove_index "quoting_details", ["case_id", "sales_stage_id"] # The previous index on these columns had a uniqueness contstraint
    add_index "quoting_details", ["case_id", "sales_stage_id"]
  end

  def down
    remove_index "quoting_details", ["case_id", "sales_stage_id"] # The previous index on these columns had _no_ uniqueness contstraint
    add_index "quoting_details", ["case_id", "sales_stage_id"], unique: true
  end
end
