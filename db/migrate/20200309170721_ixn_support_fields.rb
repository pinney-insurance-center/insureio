class IxnSupportFields < ActiveRecord::Migration
  def up
    remove_column :quoting_details, :quote_param_dyn_id
    add_column :quoting_details, :ext, :text
  end

  def down
    remove_column :quoting_details, :ext
    add_column :quoting_details, :quote_param_dyn_id, :integer
  end
end
