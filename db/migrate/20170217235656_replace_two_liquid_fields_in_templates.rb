class ReplaceTwoLiquidFieldsInTemplates < ActiveRecord::Migration
  def up
    update_data 'body'
  end

  def down
    update_data 'body',    true
  end

  def update_data field, reverse=false
    old_values=[
      '{% profiled_email agent %}',
      '{%profiled_email agent%}',
      '{% profiled_email case_manager %}',
      '{%profiled_email case_manager%}',
      '{{agent.phone}}',
      '{{ agent.phone }}'
    ]
    new_values=[
      '{{ brand_specific_fallback_primary_email_for_agent }}',
      '{{ brand_specific_fallback_primary_email_for_agent }}',
      '{{ brand_specific_fallback_primary_email_for_case_manager }}',
      '{{ brand_specific_fallback_primary_email_for_case_manager }}',
      '{{ brand_specific_fallback_primary_phone_for_agent }}',
      '{{ brand_specific_fallback_primary_phone_for_agent }}',
    ]

    unless reverse
      from_values=old_values
      to_values  =new_values
    else
      from_values=new_values
      to_values  =old_values
    end

    from_values.each_with_index do |from_value,i|
      to_value=to_values[i]
      execute %Q(
        UPDATE marketing_templates
        SET    #{field}=REPLACE( #{field}, '#{from_value}', '#{to_value}' )
        WHERE  #{field} LIKE '%#{from_value}%'
      )
    end
  end
end
