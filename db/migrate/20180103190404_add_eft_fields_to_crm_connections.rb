class AddEftFieldsToCrmConnections < ActiveRecord::Migration
  def change
    add_column :crm_connections, :encrypted_eft_account_num,    :string
    add_column :crm_connections, :encrypted_eft_account_num_iv, :string
    add_column :crm_connections, :encrypted_eft_routing_num,    :string
    add_column :crm_connections, :encrypted_eft_routing_num_iv, :string
    add_column :crm_connections, :eft_account_type_id,          :integer
  end
end
