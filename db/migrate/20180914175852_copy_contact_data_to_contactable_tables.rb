class CopyContactDataToContactableTables < ActiveRecord::Migration
  #These classes are being stubbed here
  #so we can make use of `ActiveRecord` functionality
  #without invoking any app code.
  class Contact < ActiveRecord::Base
  end
  class Crm::Connection < ActiveRecord::Base
  end
  class Usage::User < ActiveRecord::Base
  end
  class Usage::Profile < ActiveRecord::Base
  end

  TENANTS={
    pinney:   0,
    amg:      1,
    ebi:      2,
    ezlife:   3,
    pjp:      4,
    taber:    5,
    bsb:      6,
    termteam: 7,
    m2:       8,
  }.with_indifferent_access.freeze

  def up
    common_contactable_fields=[
      "full_name",
      "title",
      "company",
      "preferred_contact_method_id",
      "preferred_contact_time",
      "primary_address_id",
      "primary_phone_id",
      "primary_email_id",
      "birth_or_trust_date",
      "active_or_enabled",
      "entity_type_id",
    ]

    contact_ids=Contact.pluck(:id).select{|id| id>6137 }

    #Each batch of 50 includes one select from `contacts`
    #and 50 updates to one of the contactable tables.
    contact_ids.in_groups_of(50) do |g_ids|
      g_ids.compact!
      g=Contact.where(id:g_ids)
      g.each do |c|
        begin
          next unless c.person_type.in?(['Crm::Connection','Usage::Profile','Usage::User'])
          table_to_update=c.person_type.constantize.table_name
          update_string=''

          tenant_id=TENANTS[c.tenant_name]
          tenant_id=0 unless tenant_id.present?
          if table_to_update=='crm_connections'
            update_string+="tenant_id=#{tenant_id}, "
            update_string+="trustee_name='#{ c.trustee.gsub(/'/,'') }', " if c.trustee.present?
            update_string+="gender=#{c.gender}, " if !c.gender.nil?
            update_string+="encrypted_ssn='#{c.encrypted_ssn}', " if c.encrypted_ssn.present?
          elsif table_to_update=='usage_users'
            update_string+="encrypted_ssn='#{c.encrypted_ssn}', " if c.encrypted_ssn.present?
          elsif table_to_update=='usage_profiles'
            update_string+="tenant_id=#{tenant_id}, "
          end

          common_contactable_fields.each do |f|
            next unless c[f].present? || c[f]==false
            use_quotes=!f.match(/_id/) && f!="active_or_enabled"
            if !use_quotes
              update_string+="#{f}=#{c[f]}, "
            elsif c[f].is_a?(Date)
              string_value=c[f].to_s(:db)
              update_string+=%Q(#{f}="#{string_value}", )
            else
              escaped_value=c[f].gsub(/"/,'')#remove any double quotes
              update_string+=%Q(#{f}="#{escaped_value}", )
            end
          end
          update_string=update_string[0..(update_string.length-3)]#remove last comma and space
          execute(%Q(
            UPDATE #{table_to_update}
            SET #{update_string}
            WHERE id=#{c.person_id}
          ))
        rescue => ex
          puts ex.message
          puts "The attempt to update contact #{ JSON.pretty_generate(c) } failed."
          puts "Failed while attempting to compose an update clause for field #{f}." if defined?(f)
          raise ex
        end
      end

      #In production, there is always a small chance of users interacting while a long-running migration takes place.
      #Hopefully, this will be able to be run at night during a weekend with ample notice to users.
      Rails.env.production? ? sleep(4) : nil
    end

  end

  def down
  end

end
