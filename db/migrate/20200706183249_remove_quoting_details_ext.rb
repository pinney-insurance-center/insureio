class RemoveQuotingDetailsExt < ActiveRecord::Migration
  def up
    remove_column :quoting_details, :ext
  end

  def down
    add_column :quoting_details, :ext, :text
  end
end
