# Moving details from individual crimes and moving violations into fields on
# the HealthInfo object itself

class MigrateDetailsForCrimeAndDriving < ActiveRecord::Migration
  def up
    # Clear legacy serialized field (formerly criminal_details)
    ActiveRecord::Base.connection.execute <<-EOF
    UPDATE crm_health_infos SET criminal_details = NULL WHERE criminal_details IS NOT NULL;
    EOF
    # There are fewer than 500 records in this query
    Crm::HealthInfo.where('crimes is not null').each do |health|
      details = health.crimes.map(&:detail).join("\n").strip
      health.update_columns(criminal_details: details) if details.present?
    end
    # There are fewer than 5000 records in this query
    Crm::HealthInfo.where('moving_violations is not null').each do |health|
      details = health.moving_violations.map(&:detail).join("\n").strip
      health.update_columns(moving_violation_details: details) if details.present?
    end
  end
end
