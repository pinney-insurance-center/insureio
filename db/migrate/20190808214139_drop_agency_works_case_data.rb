class DropAgencyWorksCaseData < ActiveRecord::Migration
  def up
    drop_table :processing_agency_work_case_data
  end

  def down
    #This is a time intensive task.
    #Just don't run this migration until you're sure the last migration worked without issue.
    create_table "processing_agency_work_case_data", :force => true do |t|
      t.integer  "case_id"
      t.integer  "agency_works_id"
      t.datetime "created_at",              :null => false
      t.datetime "updated_at",              :null => false
    end
    cases_with_aw_id=execute(%Q(
      SELECT id, agency_works_id, sent_to_agency_works_at
      FROM crm_cases
      WHERE agency_works_id IS NOT NULL
    )).to_a

    cases_with_aw_id.in_groups_of(50) do |g|
      g.compact!
      next unless g.present?
      row_strings=[]
      g.each do |c_id,aw_id,aw_date|
        row_strings<< "(#{c_id},#{aw_id},\"#{sent_to_agency_works_at}\",\"#{sent_to_agency_works_at}\")"
      end
      execute(%Q(
        INSERT INTO processing_agency_work_case_data(case_id,agency_works_id,created_at,updated_at)
        VALUES
        #{row_strings.join(",\n")};
      ))
    end

  end
end