class AddColumnFinancingToCrmCases < ActiveRecord::Migration
  def change
    add_column :crm_cases, :financing, :boolean, after: :replaced_by_id
  end
end
