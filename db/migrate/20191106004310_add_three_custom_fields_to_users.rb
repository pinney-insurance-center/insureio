class AddThreeCustomFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :custom_field_one,   :text, after: :common_tags
    add_column :users, :custom_field_two,   :text, after: :custom_field_one
    add_column :users, :custom_field_three, :text, after: :custom_field_two
  end
end
