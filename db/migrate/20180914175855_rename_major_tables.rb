class RenameMajorTables < ActiveRecord::Migration
  def up
    tables_to_rename=[
      [:crm_connections,                  :consumers],
      [:usage_profiles,                   :brands],
      [:usage_users,                      :users],
      [:crm_tasks,                        :tasks],
      [:usage_profiles_usage_users,       :brands_users],
      [:crm_lead_types_usage_profiles,    :crm_lead_types_brands],
      [:crm_referrers_usage_profiles,     :crm_referrers_brands],
      [:usage_profiles_tagging_tag_values,:brands_tagging_tag_values],
      [:crm_sources_usage_users,          :crm_sources_users],
      [:usage_users_common_tags,          :users_common_tags],
    ]
    tables_to_rename.each do |from_value,to_value|
      rename_table from_value, to_value if table_exists?(from_value)
    end

    columns_to_change=[
      [:marketing_messages,      :target_type],
      [:marketing_subscriptions, :person_type],
      [:addresses,               :contactable_type],
      [:email_addresses,         :contactable_type],
      [:phones,                  :contactable_type],
      [:webs,                    :contactable_type],
      [:usage_staff_assignments, :owner_type],
      [:ahoy_events,             :user_type],
      [:attachments,             :person_type],
      [:tasks,                   :person_type],
      [:crm_notes,               :notable_type]
    ]
    model_name_changes=[
      ['Crm::Connection', 'Consumer'],
      ['Usage::Profile',  'Brand'],
      ['Usage::User',     'User']
    ]
    columns_to_change.each do |table_name,column_name|
      model_name_changes.each do |from_value,to_value|
        execute(%Q(
          UPDATE #{table_name}
          SET #{column_name}=REPLACE( #{column_name}, '#{from_value}', '#{to_value}' )
          WHERE #{column_name} LIKE '%#{from_value}%'
        ))
      end
    end

    foreign_key_columns_to_rename=[
      [:crm_cases,                         :connection_id, :consumer_id],
      [:crm_physicians,                    :connection_id, :consumer_id],
      [:processing_exam_one_statuses,      :connection_id, :consumer_id],
      [:quoting_details,                   :connection_id, :consumer_id],
      [:tagging_tags,                      :connection_id, :consumer_id],
      [:brands_tagging_tag_values,         :profile_id, :brand_id],
      [:users,                             :selected_profile_id, :selected_brand_id],
      [:users,                             :default_profile_id,  :default_brand_id],
      [:visits,                            :profile_id, :brand_id],
      [:ahoy_events,                       :profile_id, :brand_id],
      [:brands_users,                      :profile_id, :brand_id],
      [:consumers,                         :profile_id, :brand_id],
      [:crm_lead_types_brands,             :profile_id, :brand_id],
      [:crm_referrers_brands,              :profile_id, :brand_id],
      [:crm_task_builders,                 :profile_id, :brand_id],
      [:lead_distribution_cursors,         :profile_id, :brand_id],
      [:marketing_messages,                :profile_id, :brand_id],
      [:quoting_widget_widgets,            :profile_id, :brand_id],
    ]

    foreign_key_columns_to_rename.each do |table_name, old_col_name, new_col_name|
      rename_column table_name, old_col_name, new_col_name if column_exists?(table_name, old_col_name)
    end

  end

  def down

    columns_to_change=[
      [:marketing_messages,      :target_type],
      [:marketing_subscriptions, :person_type],
      [:addresses,               :contactable_type],
      [:email_addresses,         :contactable_type],
      [:phones,                  :contactable_type],
      [:webs,                    :contactable_type],
      [:usage_staff_assignments, :owner_type],
      [:ahoy_events,             :user_type],
      [:attachments,             :person_type],
      [:tasks,                   :person_type],
      [:crm_notes,               :notable_type]
    ]
    model_name_changes=[
      ['Crm::Connection', 'Consumer'],
      ['Usage::Profile',  'Brand'],
      ['Usage::User',     'User']
    ]
    columns_to_change.each do |table_name,column_name|
      model_name_changes.each do |to_value,from_value|
        execute(%Q(
          UPDATE #{table_name}
          SET #{column_name}=REPLACE( #{column_name}, '#{from_value}', '#{to_value}' )
          WHERE #{column_name} LIKE '%#{from_value}%'
        ))
      end
    end

    tables_to_rename=[
      [:crm_connections,                  :consumers],
      [:usage_profiles,                   :brands],
      [:usage_users,                      :users],
      [:crm_tasks,                        :tasks],
      [:usage_profiles_usage_users,       :brands_users],
      [:crm_lead_types_usage_profiles,    :crm_lead_types_brands],
      [:crm_referrers_usage_profiles,     :crm_referrers_brands],
      [:usage_profiles_tagging_tag_values,:brands_tagging_tag_values],
      [:crm_sources_usage_users,          :crm_sources_users],
      [:usage_users_common_tags,          :users_common_tags],
    ]
    tables_to_rename.each do |to_value,from_value|
      rename_table from_value, to_value
    end

    foreign_key_columns_to_rename=[
      [:crm_cases,                         :connection_id, :consumer_id],
      [:crm_physicians,                    :connection_id, :consumer_id],
      [:processing_exam_one_statuses,      :connection_id, :consumer_id],
      [:quoting_details,                   :connection_id, :consumer_id],
      [:tagging_tags,                      :connection_id, :consumer_id],
      [:usage_profiles_tagging_tag_values, :profile_id, :brand_id],
      [:users,                             :selected_profile_id, :selected_brand_id],
      [:users,                             :default_profile_id,  :default_brand_id],
      [:visits,                            :profile_id, :brand_id],
      [:ahoy_events,                       :profile_id, :brand_id],
      [:brands_users,                      :profile_id, :brand_id],
      [:consumers,                         :profile_id, :brand_id],
      [:crm_lead_types_usage_profiles,     :profile_id, :brand_id],
      [:crm_referrers_usage_profiles,      :profile_id, :brand_id],
      [:crm_task_builders,                 :profile_id, :brand_id],
      [:lead_distribution_cursors,         :profile_id, :brand_id],
      [:marketing_messages,                :profile_id, :brand_id],
      [:quoting_widget_widgets,            :profile_id, :brand_id],
      [:usage_profiles_users,              :profile_id, :brand_id],
    ]

    foreign_key_columns_to_rename.each do |table_name, new_col_name, old_col_name|
      rename_column table_name, old_col_name, new_col_name
    end

  end

end
