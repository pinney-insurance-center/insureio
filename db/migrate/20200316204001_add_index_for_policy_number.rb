class AddIndexForPolicyNumber < ActiveRecord::Migration
  def change
    add_index :crm_cases, :policy_number
  end
end
