class AddTimestampsAndUserAttributionToMarketingSubscriptions < ActiveRecord::Migration
  def change
    add_column :marketing_subscriptions, :created_at,            :datetime
    add_column :marketing_subscriptions, :updated_at,            :datetime
    add_column :marketing_subscriptions, :created_by_id,         :integer
    add_column :marketing_subscriptions, :created_via_report_id, :integer
  end
end
