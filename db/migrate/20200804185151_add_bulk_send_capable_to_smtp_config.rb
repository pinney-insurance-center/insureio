class AddBulkSendCapableToSmtpConfig < ActiveRecord::Migration
  def change
    add_column :marketing_email_smtp_servers, :bulk_send_capable, :boolean, null:false, default: false
  end
end
