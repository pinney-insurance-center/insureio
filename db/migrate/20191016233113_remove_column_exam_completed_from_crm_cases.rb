class RemoveColumnExamCompletedFromCrmCases < ActiveRecord::Migration
  def up
    remove_column :crm_cases, :exam_completed
  end

  def down
    add_column    :crm_cases, :exam_completed, :boolean, default: false
    #There is no sense re-populating the data. It was both redundant and never used.
  end
end
