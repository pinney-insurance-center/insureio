class CreateIxnExts < ActiveRecord::Migration
  def change
    create_table :ixn_exts do |t|
      t.references :case, foreign_key: true
      t.index :case_id
      t.string :ixn_id, limit: 36
      t.text :data # serialized
      t.timestamps
    end
  end
end
