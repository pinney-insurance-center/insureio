class UpdateTaskPersonName < ActiveRecord::Migration

  UPDATE_CONTACT_TRIGGER = "if NEW.primary_email_id <> OLD.primary_email_id then
    update email_addresses set contact_id = NEW.id where email_addresses.id = NEW.primary_email_id;
  end if;
  IF NEW.primary_address_id <> OLD.primary_address_id THEN
    UPDATE addresses SET contact_id = NEW.id where addresses.id = NEW.primary_address_id;
  END IF;
  IF NEW.primary_phone_id <> OLD.primary_phone_id THEN
    UPDATE phones SET contact_id = NEW.id WHERE phones.id = NEW.primary_phone_id;
  END IF;"

  def up
    # May need to also drop trigger contact_inserted
    execute "drop trigger contact_updated" rescue nil


    execute "create trigger contact_updated
    after update on contacts for each row
    begin
      IF NEW.full_name <> OLD.full_name THEN
        update crm_tasks t
          set t.person_name = NEW.full_name
          where t.person_id = NEW.person_id AND t.person_type = NEW.person_type;
     #{UPDATE_CONTACT_TRIGGER}
      END IF;
    END"
  end

  def down
    execute "drop trigger contact_updated"
  end
end
