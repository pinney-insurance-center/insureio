class RemoveRedundantContactData < ActiveRecord::Migration
  def up
    drop_table :contacts
    ['addresses','email_addresses','phones','webs'].each do |t|
      remove_column t, :contact_id
    end
    remove_column :usage_users, :role_id
  end

  def down
    #If we need to undo such a huge database change,
    #it would be easier to simply restore the entire database server from a backup.
    #Therefor, no down method will be written.
  end

end
