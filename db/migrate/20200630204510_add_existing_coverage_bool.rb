class AddExistingCoverageBool < ActiveRecord::Migration
  def change
    add_column :crm_financial_infos, :has_coverage, :boolean
  end
end
