class ChangeBlastRecipientsToText < ActiveRecord::Migration
  def up
    change_column :marketing_email_blasts, :recipients, :text
  end
  def down
    change_column :marketing_email_blasts, :recipients, :string
  end
end
