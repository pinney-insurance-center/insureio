class CopyAgencyWorksIdToCrmCase < ActiveRecord::Migration
  def up
    add_column :crm_cases, :agency_works_id,         :integer, after: :sent_to_igo
    add_column :crm_cases, :sent_to_agency_works_at, :date,    after: :agency_works_id

    add_index :crm_cases, [:agency_works_id]

    aw_data=execute(%Q(
      SELECT case_id, agency_works_id, updated_at
      FROM processing_agency_work_case_data
      WHERE agency_works_id IS NOT NULL
    )).to_a

    aw_data.in_groups_of(50) do |g|
      g.compact!
      next unless g.present?
      g.each do |c_id, aw_id, aw_date|
        execute(%Q(
          UPDATE crm_cases
          SET agency_works_id=#{aw_id}, sent_to_agency_works_at="#{aw_date.to_s(:db)}"
          WHERE id=#{c_id}
        ))
      end
    end
  end

  def down
    remove_index  :crm_cases, [:agency_works_id]
    remove_column :crm_cases, :agency_works_id
    remove_column :crm_cases, :sent_to_agency_works_at
  end
end
