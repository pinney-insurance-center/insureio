class AddInsDivFieldsToBrands < ActiveRecord::Migration
  def up
    add_column    :brands, :preferred_insurance_division_subdomain, :string if !column_exists?(:brands, :preferred_insurance_division_subdomain)
    add_column    :brands, :preferred_subdomain_set_already,        :boolean, default: false if !column_exists?(:brands, :preferred_subdomain_set_already)

    #Populate these for existing brands.
    brands=execute("SELECT id, full_name, company FROM brands").to_a
    brands.in_groups_of(50) do |g|
      g.compact!
      next unless g.present?
      query_string="UPDATE brands SET preferred_insurance_division_subdomain=CASE\n"
      b_ids=[]
      g.each do |id,name,company|
        inferred_value=(name||company)
        next unless inferred_value.present?
        inferred_value=inferred_value.gsub(/\sinc\./i,'').gsub(/(\W+)/,' ').strip.gsub(/\s+/,'_').downcase
        query_string+="WHEN id=#{id} THEN '#{inferred_value}'\n"
        b_ids<< id
      end
      next unless b_ids.present?
      query_string+="END WHERE id IN (#{b_ids.join(',')})"
      execute(query_string)
    end

  end

  def down
    remove_column :brands, :preferred_insurance_division_subdomain
    remove_column :brands, :preferred_subdomain_set_already
  end
end
