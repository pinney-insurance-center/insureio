class RemoveStateFromConsumer < ActiveRecord::Migration
  def up
    remove_column :consumers, :state_id
  end

  def down
    #This removes data which cannot be repopulated from the addresses table in a timely manner.
    #In the unlikely event that it needed to be rolled back, that would likely require a restore of the database from a backup.
    add_column :consumers, :state_id, :integer
  end
end
