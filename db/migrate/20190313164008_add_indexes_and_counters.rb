class AddIndexesAndCounters < ActiveRecord::Migration
  def change
    #Add Indexes
    add_index :attachments, [:person_id,:person_type]

    add_index :brands, [:owner_id,:full_name]
    add_index :brands, [:id,:full_name]
    add_index :brands, :full_name

    add_index :consumers, [:brand_id, :agent_id,:full_name]
    add_index :consumers, [:id,:full_name]
    add_index :consumers, :full_name

    add_index :crm_cases, :consumer_id

    add_index :crm_connection_relationships, :connection_a_id
    add_index :crm_connection_relationships, :connection_b_id

    add_index :marketing_email_smtp_servers, :owner_id

    add_index :notes, [:notable_id,:notable_type]

    add_index :phone_recordings, [:person_id,:person_type]

    add_index :quoting_relatives_diseases, :health_info_id

    add_index    :tasks, [:person_id,:person_type,:active]
    add_index    :tasks, [:origin_id,:origin_type,:evergreen]
    remove_index :tasks, name: :index_crm_tasks_on_origin_id
    remove_index :tasks, name: :origin_id
    remove_index :tasks, name: :index_crm_tasks_on_person_id

    add_index :usage_contracts, :user_id
    add_index :usage_contracts_states, :contract_id

    add_index :usage_licenses, :user_id

    add_index :users, [:parent_id,:full_name]
    add_index :users, [:id,:full_name]
    add_index :users, :full_name

    #Add Count Columns
    #Regular `has_many` associations will work out of the box.
    #But scoped or polymorphic associations will need `after_save` and `after_destroy` callbacks to maintain counts.
    #Populating these counters would be a long-running task,
    #so rather than do so in this migration, a script will be run in a console immediately after migration.
    add_column :consumers, :unscoped_attachments_count,          :integer
    add_column :consumers, :attachments_count,                   :integer
    add_column :consumers, :recordings_count,                    :integer
    add_column :consumers, :opportunities_count,                 :integer
    add_column :consumers, :pending_policies_count,              :integer
    add_column :consumers, :placed_policies_count,               :integer
    add_column :consumers, :related_count,                       :integer
    add_column :consumers, :old_tasks_count,                     :integer


    drop_table :marketing_organization if table_exists?(:marketing_organization)
  end

end
