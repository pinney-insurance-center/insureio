class AddContactFieldsToContactableTables < ActiveRecord::Migration
  def up
    common_contactable_fields=[
      ["full_name",                  :string],
      ["title",                      :string],
      ["company",                    :string],
      ["preferred_contact_method_id",:integer],
      ["preferred_contact_time",     :string],
      ["primary_address_id",         :integer],
      ["primary_phone_id",           :integer],
      ["primary_email_id",           :integer],
      ["birth_or_trust_date",        :date],
    ]
    contactable_model_tables=[
      'crm_connections',
      'usage_users',
      'usage_profiles'
    ]

    contactable_model_tables.each do |table_name|
      common_contactable_fields.each do |field_name, field_type|
        add_column table_name, field_name, field_type unless column_exists?(table_name, field_name)
      end
      add_column table_name, :active_or_enabled, :boolean, default: true unless column_exists?(table_name, :active_or_enabled)
      add_column table_name, :entity_type_id, :integer, limit: 1, default: 1 unless column_exists?(table_name, :entity_type_id)
    end

    add_column :crm_connections, :tenant_id, :integer, default: 0
    add_column :usage_profiles,  :tenant_id, :integer, default: 0

    add_column :crm_connections, :gender, :boolean
    add_column :crm_connections, :trustee_name, :string
    add_column :crm_connections, :state_id,     :integer

    add_column :usage_users,     :encrypted_ssn, :string
    add_column :crm_connections, :encrypted_ssn, :string, before: :encrypted_tin

    add_column :usage_users, :is_recruit, :boolean, default:false, null:false, before: :user_account_id
    execute(%Q(
      UPDATE usage_users
      SET is_recruit=TRUE
      WHERE role_id=12
    ))
  end

  def down
  end

end
