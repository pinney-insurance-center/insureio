class AddFinancialFieldsToCase < ActiveRecord::Migration
  def change
    add_column :crm_cases, :autoloan_premium, :boolean # Automatic Premium Loan
    add_column :crm_cases, :benes_type_id, :integer, limit: 1 # Benefit distribution type: per stirpes or survivorship
    add_column :crm_cases, :bill_type_id, :integer, limit: 1 # Billing method: eft, cod (direct_billing), cc
    add_column :crm_cases, :dividend_type_id, :integer, limit: 1 # Dividend: accumulation, cash, paid-up additions, pay premium
  end
end
