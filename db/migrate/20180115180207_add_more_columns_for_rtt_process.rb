class AddMoreColumnsForRttProcess < ActiveRecord::Migration
  def change
    add_column :addresses,       :county_name,                       :string
    add_column :addresses,       :is_within_city_limits,             :boolean
    add_column :crm_connections, :actively_employed,                 :boolean,  after: :note
    add_column :crm_cases,       :scor_guid,                         :string,   after: :sent_to_igo
    add_column :crm_cases,       :date_processing_started_with_scor, :datetime, after: :scor_guid
  end
end
