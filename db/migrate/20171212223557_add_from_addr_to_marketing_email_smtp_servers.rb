class AddFromAddrToMarketingEmailSmtpServers < ActiveRecord::Migration
  def change
    add_column :marketing_email_smtp_servers, :from_addr, :string
  end
end
