class AddSinglePremiumAndAddtlPremium < ActiveRecord::Migration
  def change
    add_column :quoting_details, :single_premium, :decimal, precision: 10, scale: 2, after: :semiannual_premium
    add_column :quoting_details, :x1035_amt, :decimal, precision: 10, scale: 2
    add_column :quoting_details, :lump_sum, :decimal, precision: 10, scale: 2
    add_column :quoting_details, :flat_amt, :decimal, precision: 10, scale: 2
    add_column :quoting_details, :flat_yrs, :integer
  end
end
