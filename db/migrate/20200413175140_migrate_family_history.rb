class MigrateFamilyHistory < ActiveRecord::Migration
  def up
    # There are fewer than 4000 of these records
    relatives_map = Crm::RelativesDisease.includes(:health_info).all.group_by(&:health_info_id)
    health_info_objects = Crm::HealthInfo.where(id: relatives_map.keys)
    .each do |health_info|
      health_info.update! family_diseases_attributes: Array(relatives_map[health_info.id].as_json)
    end
  end

  # No `down` function is needed. This is a non-destructive data migration.
  # We can delete the now-unused table after confirming no loss of data
  # following this migration.
end
