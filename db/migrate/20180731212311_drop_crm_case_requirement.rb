class DropCrmCaseRequirement < ActiveRecord::Migration
  def up
    drop_table :crm_case_requirements
  end

  def down
    create_table "crm_case_requirements", :force => true do |t|
      t.string   "name"
      t.string   "required_of"
      t.date     "ordered"
      t.date     "recieved"
      t.string   "requirement_type"
      t.string   "status"
      t.datetime "created_at",       :null => false
      t.datetime "updated_at",       :null => false
    end
  end
end
