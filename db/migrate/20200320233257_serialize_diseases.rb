class SerializeDiseases < ActiveRecord::Migration
  def change
    add_column :crm_health_infos, :diseases, :text
    add_column :crm_health_infos, :relatives_diseases, :text
    add_column :crm_health_infos, :moving_violations, :text
    add_column :crm_health_infos, :crimes, :text
    # After a month or so, it is okay to delete the deprecated disease-related
    # fields on crm_health_infos, the table quoting_relatives_diseases, and
    # the entire table for model Crm::RelativesDisease.
  end
end
