class MigrateMovingViolations < ActiveRecord::Migration
  def up
    # There are fewer than 7000 of these records
    query = <<~EOF
    select id, moving_violation, moving_violation_details from crm_health_infos
    where moving_violation = true OR moving_violation_details IS NOT NULL
    EOF
    raws = Crm::HealthInfo.connection.execute(query).map { |a| [a.first, a[1..-1]] }.to_h
    orms = Crm::HealthInfo.where('moving_violation = true OR moving_violation_details IS NOT NULL')

    Crm::HealthInfo.transaction do
      orms.each do |health_info|
        bool_field, details = raws[health_info.id]
        if details && details = JSON.parse(details).presence
          strings, details = details.partition { |k,v| v.is_a? String }.map(&:to_h)
          years, details = details.partition { |k,v| v && v > 1900 }.map(&:to_h)
          if details.present?
            health_info.moving_violation_history_attributes = details
          end
          strings.each do |k,v|
            # Some user entered a date instead of a quantity
            health_info.moving_violations << Crm::HealthInfo::MovingViolation.new(date: Date.parse(v))
          end
          years.each do |k,v|
            # Some user entered a year instead of a quantity
            health_info.moving_violations << Crm::HealthInfo::MovingViolation.new(date: Date.new(v))
          end
        else
          health_info.moving_violation = bool_field
        end
          health_info.save!
      end
    end
  end

  # No `down` function is needed. This is a non-destructive data migration.
  # We can delete the now-unused columns after confirming no loss of data
  # following this migration.
end
