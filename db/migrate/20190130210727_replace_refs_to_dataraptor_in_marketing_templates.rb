class ReplaceRefsToDataraptorInMarketingTemplates < ActiveRecord::Migration
  #This correction is being introduced here as a migration versus a script because
  #it should be run in all environments, and a migration file facilitates that well.
  def up
    execute(%Q(
      UPDATE marketing_templates
      SET body=REPLACE(body, '.dataraptor.com', '.insureio.com')
      WHERE body LIKE "%.dataraptor.com%"
    ))
  end

  def down
    #No down action needed.
  end
end
