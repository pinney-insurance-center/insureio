class RemoveConfirmedFromUsers < ActiveRecord::Migration
  def up
  	remove_column(:usage_users, :confirmed)
  end

  def down
  	add_column(:usage_users, :confirmed, :boolean, default: false)
  end
end
