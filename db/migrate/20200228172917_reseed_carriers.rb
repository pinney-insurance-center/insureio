class ReseedCarriers < ActiveRecord::Migration
  def up
    # Reference the Carrier class so that Rails will load the model. Without
    # this reference, Rails would raise 'undefined class/module'.
    Carrier
    load 'db/seeding/carriers.rb'
  end
end
