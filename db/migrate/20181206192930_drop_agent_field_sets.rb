class DropAgentFieldSets < ActiveRecord::Migration
  def up
    drop_table :usage_agent_field_sets
    remove_column :users, :agent_field_set_id
  end

  def down
    #Just don't run this migration until the one immediately prior is verified to have succeeded.
  end
end
