class EncryptUserApiKeys < ActiveRecord::Migration
  def up
    remove_column :users, :crypted_api_secret if column_exists?(:users, :crypted_api_secret)#this column was never used

    rename_column :users, :api_key, :old_plaintext_api_key if column_exists?(:users, :api_key)
    add_column    :users, :encrypted_api_key,    :string, after: :quoter_key if !column_exists?(:users, :encrypted_api_key)
    add_column    :users, :encrypted_api_key_iv, :string, after: :encrypted_api_key if !column_exists?(:users, :encrypted_api_key_iv)

    add_index     :users, :encrypted_api_key_iv, unique: true
  end

  def down
    add_column    :users, :crypted_api_secret, :string#this column was never used

    rename_column :users, :old_plaintext_api_key, :api_key

    #Before removing the encrypted data, be sure to re-populate the plaintext data.
    remove_index  :users, :encrypted_api_key_iv, unique: true
    remove_column :users, :encrypted_api_key
    remove_column :users, :encrypted_api_key_iv
  end
end
