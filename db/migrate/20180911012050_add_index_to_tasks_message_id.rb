class AddIndexToTasksMessageId < ActiveRecord::Migration
  def change
    add_index :crm_tasks, :message_id
  end
end
