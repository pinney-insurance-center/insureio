class CopyAppsIdToCrmCase < ActiveRecord::Migration
  def up
    add_column :crm_cases, :apps_id_offset,      :integer, after: :sent_to_agency_works_at unless column_exists?(:crm_cases, :apps_id_offset)
    add_column :crm_cases, :sent_103_to_apps_at, :date,    after: :apps_id_offset          unless column_exists?(:crm_cases, :sent_103_to_apps_at)
    add_column :crm_cases, :sent_121_to_apps_at, :date,    after: :sent_103_to_apps_at     unless column_exists?(:crm_cases, :sent_121_to_apps_at)

    add_index :crm_cases, [:apps_id_offset], unique:true

    aw_data=execute(%Q(
      SELECT case_id, id, uploaded_at
      FROM processing_apps_case_data
      WHERE uploaded_at IS NOT NULL
    )).to_a

    aw_data.in_groups_of(50) do |g|
      g.compact!
      next unless g.present?
      g.each do |c_id, apps_id_offset, apps_date|
        execute(%Q(
          UPDATE crm_cases
          SET apps_id_offset=#{apps_id_offset}, sent_103_to_apps_at="#{apps_date.to_s(:db)}"
          WHERE id=#{c_id}
        ))
      end
    end
  end

  def down
    remove_index  :crm_cases, [:apps_id_offset]
    remove_column :crm_cases, :apps_id_offset
    remove_column :crm_cases, :sent_103_to_apps_at
    remove_column :crm_cases, :sent_121_to_apps_at
  end
end
