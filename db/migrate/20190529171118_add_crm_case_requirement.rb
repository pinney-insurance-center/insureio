class AddCrmCaseRequirement < ActiveRecord::Migration

  def change
    create_table "crm_case_requirements", :force => true do |t|
      t.string   "name"
      t.string   "note"
      t.integer  "case_id"
      t.integer  "requirement_type_id"
      t.integer  "responsible_party_type_id"
      t.integer  "created_by_user_id"
      t.datetime "created_at",       :null => false
      t.datetime "updated_at",       :null => false
      t.date     "ordered_at"
      t.date     "completed_at"
      t.integer  "status_id", default: 1
    end
  end

end
