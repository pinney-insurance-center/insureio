class MergeAgentFieldSetsIntoUsers < ActiveRecord::Migration
  def up
    agent_field_set_columns=[
      [:boolean,  "temporary_suspension",    {default:false}],
      [:integer,  "premium_limit"],
      [:integer,  "tz_max"],
      [:integer,  "tz_min"],
      [:integer,  "agent_of_record_id"],
      [:boolean,  "submit_application",      {default:false}],
      [:boolean,  "submit_referral",         {default:false}],
      [:string,   "marketech_signature"],
    ]

    execute(%Q(
      UPDATE usage_agent_field_sets
      SET agent_of_record_id=NULL
      WHERE agent_of_record_is_self IS TRUE AND agent_of_record_id IS NOT NULL
    ))

    column_names=[
      "temporary_suspension",
      "premium_limit",
      "tz_max",
      "tz_min",
      "agent_of_record_id",
      "submit_application",
      "submit_referral",
      "marketech_signature",
    ]
    agent_field_set_columns.each do |data_type, col_name, opts|
      add_column :users, col_name, data_type, opts||{} unless column_exists?(:users, col_name)
    end

    #I'm intentionally not using rails methods `find_in_batches` and `eager_load` here,
    #because they generate longer, slower queries.
    column_names_w_prefix=column_names.map{|c| "afs.#{c}" }
    user_count=execute('SELECT COUNT(*) FROM users WHERE is_recruit IS FALSE').to_a.flatten.first
    max_batch_index=[ ((user_count/200)-1), 0 ].max

    #Makes N/200 SELECT queries and N/200 INSERT/UPDATE queries.
    #That's N/100 round trips from app to db server.
    (0..max_batch_index).each do |idx|
      users=execute(
        "SELECT users.id, #{column_names_w_prefix.join(', ')} "+
        "FROM users "+
        "INNER JOIN usage_agent_field_sets AS afs ON afs.id=users.agent_field_set_id "+
        "WHERE users.is_recruit IS FALSE "+
        "LIMIT 200 OFFSET #{idx*200}"
      ).to_a
      next unless users.present?
      user_ids=users.map(&:first)
      update_fragment=[]
      users.each do |u|
        afs_attrs=u[1..9].map{|v| v.to_s.present? ? v.to_s : 'NULL' }
        update_fragment<<"(#{u[0]}, '#{Time.now.to_s(:db)}', '#{Time.now.to_s(:db)}', #{afs_attrs.join(',')})"
      end
      update_fragment=update_fragment.join(",\n")
      on_dup_key_fragment=column_names.map{|c| "#{c}=VALUES(#{c})" }.join(', ')
      #This is a bit of a hack to update multiple columns on multiple rows with a single query,
      #in order to cut down the number of round trips between servers.
      #It is likely still faster than one update query per column per batch,
      #and definitely much faster than one update query per row.
      #Timestamp columns need to be included, even though they won't actually be modified,
      #because the query starts under the assumption that it's inserting new rows.
      #Those columns disallow null, but provide no default.
      execute(
        "INSERT INTO users (id, created_at, updated_at, #{column_names.join(', ')} )\n"+
        "VALUES "+update_fragment+"\n"+
        "ON DUPLICATE KEY UPDATE id=VALUES(id), #{on_dup_key_fragment}"
      )
    end

  end

  def down
    column_names=[
      "temporary_suspension",
      "premium_limit",
      "tz_max",
      "tz_min",
      "agent_of_record_id",
      "submit_application",
      "submit_referral",
      "marketech_signature",
    ]
    columns.each do |col_name|
      remove_column users, col_name if column_exists?(:users, col_name)
    end
  end
end
