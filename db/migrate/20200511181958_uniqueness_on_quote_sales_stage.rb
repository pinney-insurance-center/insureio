class UniquenessOnQuoteSalesStage < ActiveRecord::Migration
  def up
    # Select Quoting::Quote records that violate uniqueness on sales_stage_id + case_id
    results = ActiveRecord::Base.connection.execute <<~EOF
      SELECT
        case_id,
        sales_stage_id,
        COUNT(case_id) AS ct,
        GROUP_CONCAT(id SEPARATOR ',')
      FROM quoting_details
      GROUP BY case_id, sales_stage_id HAVING ct > 1;
    EOF
    # Identify which Quoting::Quote records to delete
    quote_ids_to_delete = []
    results.each do |case_id, sales_stage_id, ct, quote_ids|
      quote_ids = quote_ids.split ','
      quote_ids.pop
      quote_ids_to_delete += quote_ids
    end
    # Log Quoting::Quote records before deletion
    ActiveRecord::Base.connection.execute <<~EOF
      SELECT * FROM quoting_details
      WHERE id IN (#{quote_ids_to_delete.join(',')})
      INTO OUTFILE 'tmp #{Time.now.xmlschema} delete-quoting_details.log'
    EOF
    # Delete Quoting::Quote records that violate uniqueness on sales_stage_id + case_id
    Quoting::Quote.delete_all id: quote_ids_to_delete
    # Combine indices
    add_index "quoting_details", ["case_id", "sales_stage_id"], unique: true
    remove_index "quoting_details", ["sales_stage_id"]
    remove_index "quoting_details", ["case_id"]
  end

  def down
    add_index "quoting_details", ["sales_stage_id"], name: "index_quoting_details_on_sales_stage_id"
    remove_index "quoting_details", ["case_id", "sales_stage_id"]
    add_index "quoting_details", ["case_id"], name: "index_quoting_details_on_case_id"
  end
end
