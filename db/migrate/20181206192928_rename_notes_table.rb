class RenameNotesTable < ActiveRecord::Migration
  def change
    rename_table :crm_notes, :notes
  end
end