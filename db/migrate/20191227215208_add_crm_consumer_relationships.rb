class AddCrmConsumerRelationships < ActiveRecord::Migration
require 'pry'

  class Crm::ConnectionRelationship < ActiveRecord::Base
    #
  end
  class Crm::StakeholderRelationship < ActiveRecord::Base
    #
  end

  DEFAULT_RELATIONSHIP_TYPE_ID=4

  def up
    create_table "crm_consumer_relationships", :force => true do |t|
      #The existing two tables do not have timestamp columns. Neither will this one.
      t.integer "primary_insured_id",  null:false
      t.integer "stakeholder_id",      null:false
      t.integer "relationship_type_id",null:false
      t.integer "crm_case_id",         default:nil
      t.boolean "is_owner",            default:false
      t.boolean "is_payer",            default:false
      t.boolean "is_beneficiary",      default:false
      t.integer "percentage",          limit: 1
      t.boolean "contingent",          default:false
      t.string  "note"
    end

    add_index "crm_consumer_relationships", ["crm_case_id"],                   name: "consumer_rels_on_case"
    add_index "crm_consumer_relationships", ["primary_insured_id"],            name: "consumer_rels_on_insured"
    add_index "crm_consumer_relationships", ["stakeholder_id", "crm_case_id"], name: "consumer_rels_on_stakeholder_and_case", unique: true
    add_index "crm_consumer_relationships", ["stakeholder_id"],                name: "consumer_rels_on_stakeholder"

    add_column :consumers, :relationships_w_self_as_primary_count, :integer, default:0, null:false, before: :old_tasks_count
    add_column :consumers, :relationships_w_self_as_related_count, :integer, default:0, null:false, before: :old_tasks_count
    remove_column :consumers, :related_count

    add_column :consumers, :primary_contact_id, :integer, before: :status_unsubscribe

    #At time of migration, there are about 5k rows in this table in production.
    conn_r_ids=execute(%Q(
      SELECT id
      FROM   crm_connection_relationships
      WHERE  TRUE
    )).to_a.flatten

    #At time of migration, there are about 51k rows in this table in production.
    stake_r_ids=execute(%Q(
      SELECT id
      FROM   crm_stakeholder_relationships
      WHERE  TRUE
    )).to_a.flatten

    conn_r_ids.in_groups_of(200) do |g|
      g.compact!
      Crm::ConnectionRelationship.where(id:g).each do |cr|
        attrs=cr.attributes.except!('id')
        attrs[:primary_insured_id]  =attrs.delete 'connection_a_id'
        attrs[:stakeholder_id]      =attrs.delete 'connection_b_id'
        attrs[:relationship_type_id]=attrs.delete 'b_is_to_a_id'
        attrs[:relationship_type_id]||=DEFAULT_RELATIONSHIP_TYPE_ID
        Crm::ConsumerRelationship.create attrs
      end
    end

    stake_r_ids.in_groups_of(200) do |g|
      g.compact!
      Crm::StakeholderRelationship.where(id:g).each do |cr|
        attrs=cr.attributes.except!('id')
        attrs['relationship_type_id']||=DEFAULT_RELATIONSHIP_TYPE_ID
        Crm::ConsumerRelationship.create attrs
      end
    end

    #At time of migration, there are about 350 rows in production with a value for column `primary_contact_relationship_id`.
    cs_with_primary_contact=execute(%Q(
      SELECT consumers.id,
             crm_connection_relationships.connection_a_id,
             crm_connection_relationships.connection_b_id
      FROM   consumers
      INNER JOIN crm_connection_relationships
              ON crm_connection_relationships.id=consumers.primary_contact_relationship_id
      WHERE TRUE
    )).to_a
    cs_with_primary_contact.each do |c_id, a_id, b_id|
      other_person_id= c_id==a_id ? b_id : a_id
      execute("UPDATE consumers SET primary_contact_id=#{other_person_id} WHERE id=#{c_id}")
    end

  end

  def down
    drop_table :crm_consumer_relationships
    remove_column :consumers, :primary_contact_id
    remove_column :consumers, :relationships_w_self_as_primary_count
    remove_column :consumers, :relationships_w_self_as_related_count
    add_column :consumers, :related_count, :integer, default:0
  end
end
