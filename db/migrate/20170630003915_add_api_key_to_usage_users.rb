class AddApiKeyToUsageUsers < ActiveRecord::Migration
  #The API key is like the quoter Key, except more secure because it is only passed between users' machines and Insurieo.
  #The quoter key is used in the urls for user signatures and user photos within marketing emails, as well as in quote links sent to customers.
  def up
    unless column_exists? :usage_users, :api_key
      add_column :usage_users, :api_key, :string, limit:32
    end

    #Populate for existing users.
    Usage::User.excluding_recruits.in_groups_of(50) do |g|
      g.compact!
      g.each do |u|
        u.update_column :api_key, SecureRandom.hex(16)
      end
    end
  end

  def down
    remove_column :usage_users, :api_key
  end
end
