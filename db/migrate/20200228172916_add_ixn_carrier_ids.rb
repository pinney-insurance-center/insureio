class AddIxnCarrierIds < ActiveRecord::Migration
  def change
    add_column :carriers, :ixn_id, :integer
  end
end
