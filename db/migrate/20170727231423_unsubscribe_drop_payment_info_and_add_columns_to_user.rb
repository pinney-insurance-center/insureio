class UnsubscribeDropPaymentInfoAndAddColumnsToUser < ActiveRecord::Migration

  FREE_SUBSCRIPTION_LEVEL_ID=-1
  BASIC_LEVEL_PERMISSIONS_VALUES=[#The values for all 5 permissions columns
    1073618945,
    2861020127,
    4290773010,
    4278427116,
    1239
  ]

  def up

    add_column    :usage_users, :stripe_email,           :string, after: :membership_id
    add_column    :usage_users, :stripe_customer_id,     :string, after: :stripe_email
    add_column    :usage_users, :stripe_subscription_id, :string, after: :stripe_customer_id
    add_column    :usage_users, :stripe_plan_id,         :string, after: :stripe_subscription_id

    #Gather customer ids and populate new columns.
    #One update query per column per 50 users strikes a balance between
    #too many slow round trips to the db server and overly large queries that would just time out.
    users_with_stripe_customer_id=execute(%Q(
      SELECT usage_users.id, usage_payment_infos.stripe_customer_id,usage_payment_infos.email
      FROM usage_users
      INNER JOIN usage_payment_infos ON usage_payment_infos.id=usage_users.payment_info_id
      WHERE stripe_subscribed IS TRUE
    )).to_a
    users_with_stripe_customer_id.in_groups_of(50) do |group|
      group.compact!#needed because method `in_groups_of` will pad the last group with nils
      cid_query_string="UPDATE usage_users\nSET stripe_customer_id = CASE\n"
      email_query_string="UPDATE usage_users\nSET stripe_email = CASE\n"
      ids=[]
      group.each do |x|
        u_id,stripe_customer_id,stripe_email=x
        ids<< u_id
        cid_query_string+="WHEN id=#{u_id} THEN '#{stripe_customer_id}'\n"
        email_query_string+="WHEN id=#{u_id} THEN '#{stripe_email}'\n"
      end
      cid_query_string+="END\nWHERE id IN ( #{ids.join(',')} )"
      email_query_string+="END\nWHERE id IN (#{ids.join(',')} )"
      execute(cid_query_string)
      execute(email_query_string)
    end

    #Beginning the destructive part:

    #Cancel all Stripe subscriptions.
    payment_infos=execute(%Q(
      SELECT stripe_customer_id, subscription_id
      FROM usage_payment_infos
      WHERE stripe_subscribed IS TRUE
    )).to_a
    payment_infos.in_groups_of(50) do |group|
      group.compact!
      group.each do |pp|
        stripe_customer_id,subscription_id=pp
        begin
          customer = Stripe::Customer.retrieve(stripe_customer_id)
          subscription = customer.subscriptions.retrieve(subscription_id)
          subscription.delete
        rescue => e
          SystemMailer.error e
        end
      end
      sleep(1)
    end

    drop_table    :usage_payment_infos
    remove_column :usage_users, :payment_info_id

    paying_u_ids=execute(%Q(#find currently paying users
      SELECT id
      FROM usage_users
      WHERE membership_id>0 AND motorist_id IS NULL AND (role_id IS NULL OR role_id!=12)
    )).to_a.flatten
    paying_u_ids.in_groups_of(100) do |group|
      group.compact!
      execute(%Q(#set default payment due dates, membership id, and permissions for currently paying or disabled users
        UPDATE usage_users
        SET payment_due=NULL, membership_id=1,
          permissions_0=#{BASIC_LEVEL_PERMISSIONS_VALUES[0]},
          permissions_1=#{BASIC_LEVEL_PERMISSIONS_VALUES[1]},
          permissions_2=#{BASIC_LEVEL_PERMISSIONS_VALUES[2]},
          permissions_3=#{BASIC_LEVEL_PERMISSIONS_VALUES[3]},
          permissions_4=#{BASIC_LEVEL_PERMISSIONS_VALUES[4]}
        WHERE id IN( #{group.join(',')} )
      ))
    end

    motorist_u_ids=execute(%Q(#find motorist users
      SELECT id
      FROM usage_users
      WHERE membership_id>0 AND motorist_id IS NOT NULL AND (role_id IS NULL OR role_id!=12)
    )).to_a.flatten
    motorist_u_ids.in_groups_of(100) do |group|
      group.compact!
      execute(%Q(#set free membership and basic permissions for motorist users
        UPDATE usage_users
        SET membership_id=#{FREE_SUBSCRIPTION_LEVEL_ID},
          permissions_0=#{BASIC_LEVEL_PERMISSIONS_VALUES[0]},
          permissions_1=#{BASIC_LEVEL_PERMISSIONS_VALUES[1]},
          permissions_2=#{BASIC_LEVEL_PERMISSIONS_VALUES[2]},
          permissions_3=#{BASIC_LEVEL_PERMISSIONS_VALUES[3]},
          permissions_4=#{BASIC_LEVEL_PERMISSIONS_VALUES[4]}
        WHERE id IN( #{group.join(',')} )
      ))
    end

  end

  def down
    #There is no reversing the removed/overwritten data, but the tables can be put back to their previous structures.
    create_table :usage_payment_infos do |t|
      t.string  :email
      t.string  :plan_id
      t.string  :stripe_customer_id
      t.boolean :stripe_subscribed,     :default => false
      t.string  :subscription_id
      t.string  :email
      t.string  :stripe_customer_token
      t.string  :paypal_payer_id
      t.string  :paypal_profile_id
      t.string  :paypal_token
    end

    add_column    :usage_users, :payment_info_id, :integer

    remove_column :usage_users, :stripe_email,           :string
    remove_column :usage_users, :stripe_customer_id,     :string
    remove_column :usage_users, :stripe_subscription_id, :string
    remove_column :usage_users, :stripe_plan_id,         :string
  end
end
