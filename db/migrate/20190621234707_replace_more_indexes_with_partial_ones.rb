class ReplaceMoreIndexesWithPartialOnes < ActiveRecord::Migration
  def up
    add_index :addresses, :contactable_id, where: "contactable_type='Consumer'", name: "addresses_by_consumer_cid"
    add_index :addresses, :contactable_id, where: "contactable_type='User'",     name: "addresses_by_user_cid"
    add_index :addresses, :contactable_id, where: "contactable_type='Brand'",    name: "addresses_by_brand_cid"
    
    add_index :email_addresses, :contactable_id, where: "contactable_type='Consumer'", name: "emails_by_consumer_cid"
    add_index :email_addresses, :contactable_id, where: "contactable_type='User'",     name: "emails_by_user_cid"
    add_index :email_addresses, :contactable_id, where: "contactable_type='Brand'",    name: "emails_by_brand_cid"

    add_index :phones, :contactable_id, where: "contactable_type='Consumer'", name: "phones_by_consumer_cid"
    add_index :phones, :contactable_id, where: "contactable_type='User'",     name: "phones_by_user_cid"
    add_index :phones, :contactable_id, where: "contactable_type='Brand'",    name: "phones_by_brand_cid"

    add_index :attachments, :person_id, where: "person_type='Consumer'", name: "attachments_by_consumer_pid"
    add_index :attachments, :person_id, where: "person_type='User'",     name: "attachments_by_user_pid"
    add_index :attachments, :person_id, where: "person_type='Brand'",    name: "attachments_by_brand_pid"


    (0..8).each do |i|
      add_index :brands,    :full_name,          where: "active_or_enabled=TRUE AND tenant_id=#{i}", name: "brands_by_full_name_where_tid#{i}"

      add_index :consumers, :agent_id,           where: "active_or_enabled=TRUE AND tenant_id=#{i}", name: "consumers_by_agent_id_where_tid#{i}"
      add_index :consumers, :brand_id,           where: "active_or_enabled=TRUE AND tenant_id=#{i}", name: "consumers_by_brand_where_tid#{i}"
      add_index :consumers, :full_name,          where: "active_or_enabled=TRUE AND tenant_id=#{i}", name: "consumers_by_full_name_where_tid#{i}"

      add_index :users,     :full_name,          where: "active_or_enabled=TRUE AND tenant_id=#{i}", name: "users_by_full_name_where_tid#{i}"
    end


    add_index :crm_statuses, :statusable_id, where:"statusable_type='Crm::Case'", name: "statuses_by_cid"
    add_index :crm_statuses, :statusable_id, where:"statusable_type='Quoting::Quote'", name:"statuses_by_qid"


    add_index :tasks, :sequenceable_id, where:"sequenceable_type='Crm::Case'",      name: "tasks_by_case_id"
    add_index :tasks, :sequenceable_id, where:"sequenceable_type='Quoting::Quote'", name:"tasks_by_quote_id"
    add_index :tasks, :sequenceable_id, where:"sequenceable_type='User'",           name:"tasks_by_sequenceable_user_id"

    indexes_to_remove=[
        [:addresses,      'index_addresses_on_contact_id'],
        [:addresses,      'index_addresses_on_contactable_id_and_contactable_type'],
        [:email_addresses,'index_email_addresses_on_contact_id'],
        [:email_addresses,'index_email_addresses_on_contactable_id_and_contactable_type'],
        [:phones,         'index_phones_on_contact_id'],
        [:phones,         'index_phones_on_contactable_id_and_contactable_type'],
        [:attachments,    'index_attachments_on_person_id_and_person_type'],
        [:brands,         'index_brands_on_full_name'],
        [:brands,         'index_brands_on_id_and_full_name'],
        [:consumers,      'index_crm_connections_on_agent_id'],
        [:consumers,      'index_consumers_on_brand_id_and_agent_id_and_full_name'],
        [:consumers,      'index_consumers_on_full_name'],
        [:consumers,      'index_consumers_on_id_and_full_name'],
        [:users,          'index_usage_users_on_api_key'],
        [:users,          'index_users_on_full_name'],
        [:users,          'index_users_on_id_and_full_name'],
        [:users,          'index_users_on_parent_id_and_full_name'],
        [:tasks,          'index_tasks_on_person_id_and_evergreen'],
    ]
    indexes_to_remove.each do |table_name,index_name|
        remove_index table_name, name: index_name if index_exists?(table_name, name: index_name)
    end

  end

  def down
  end
end
