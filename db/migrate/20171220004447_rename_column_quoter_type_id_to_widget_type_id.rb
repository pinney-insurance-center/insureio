class RenameColumnQuoterTypeIdToWidgetTypeId < ActiveRecord::Migration
  def up
    rename_column :quoting_widget_widgets, :quoter_type_id, :widget_type_id
    execute(%Q(
      UPDATE quoting_widget_widgets
      SET widget_type_id=CASE widget_type_id
        WHEN 0 THEN 3/* Lead Capture*/
        WHEN 1 THEN 1/* Term Life */
        WHEN 4 THEN 2/* Long Term Care */
        WHEN 5 THEN 4/* Motorists RTT */
        END
      WHERE TRUE;
    ))
  end

  def down
    execute(%Q(
      UPDATE quoting_widget_widgets
      SET widget_type_id=CASE widget_type_id
        WHEN 3 THEN 0/* Lead Capture*/
        WHEN 1 THEN 1/* Term Life */
        WHEN 2 THEN 4/* Long Term Care */
        WHEN 4 THEN 5/* Motorists RTT */
        END
      WHERE TRUE;
    ))
    rename_column :quoting_widget_widgets, :widget_type_id, :quoter_type_id
  end
end
