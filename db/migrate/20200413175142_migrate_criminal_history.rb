class MigrateCriminalHistory < ActiveRecord::Migration
  def up
    # There are fewer than 200 of these records
    query = <<~EOF
    select id, criminal, criminal_details from crm_health_infos
    where criminal = true OR criminal_details IS NOT NULL
    EOF
    raw_data = Crm::HealthInfo.connection.execute(query).map { |a| [a.first, a[1..-1]] }.to_h
    orm_data = Crm::HealthInfo.where('criminal = true OR moving_violation_details IS NOT NULL')

    Crm::HealthInfo.transaction do
      orm_data.each do |health_info|
        bool_field, details = raw_data[health_info.id]
        # Escape-and-parse isn't an option because some strings in the DB are
        # completely illegal JSON. Therefore, after a review of the data, I
        # elected to manually adjust the values.
        fixed_details = details && details.sub(/^\{"further_detail":/, '').sub(/\}$/, '').gsub(/^\"|\"$/, '')
        if fixed_details.present?
          health_info.crimes = Crm::HealthInfo::Crime.new detail: fixed_details
        else
          health_info.criminal = bool_field
        end
        health_info.save!
      end
    end
  end

  # No `down` function is needed. This is a non-destructive data migration.
  # We can delete the now-unused columns after confirming no loss of data
  # following this migration.
end
