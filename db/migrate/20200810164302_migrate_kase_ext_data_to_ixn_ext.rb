class MigrateKaseExtDataToIxnExt < ActiveRecord::Migration
  def up
    # There are 95 of these records
    Crm::Case.where("ext REGEXP 'nmb|esp'").each do |kase|
      next if kase.ixn_ext.present?
      kase.create_ixn_ext!(
        adb_rider: kase.ext['adb_rider'],
        adb_rider_annual: kase.ext['adb_rider_annual'],
        adb_rider_monthly: kase.ext['adb_rider_monthly'],
        adb_rider_quarterly: kase.ext['adb_rider_quarterly'],
        adb_rider_semi_annual: kase.ext['adb_rider_semi_annual'],
        bankruptcy_details: kase.ext['nmb_bankruptcy_details'],
        child_rider_annual: kase.ext['child_rider_annual'],
        child_rider_monthly: kase.ext['child_rider_monthly'],
        child_rider_quarterly: kase.ext['child_rider_quarterly'],
        child_rider_semi_annual: kase.ext['child_rider_semi_annual'],
        child_rider_units: kase.ext['child_rider_units'],
        child_wop_rider_annual: kase.ext['child_wop_rider_annual'],
        child_wop_rider_monthly: kase.ext['child_wop_rider_monthly'],
        child_wop_rider_quarterly: kase.ext['child_wop_rider_quarterly'],
        child_wop_rider_semi_annual: kase.ext['child_wop_rider_semi_annual'],
        heart_stroke_cancer: kase.ext['nmb_heart_stroke_cancer'],
        heart_stroke_cancer_details: kase.ext['nmb_heart_stroke_cancer_details'],
        parent_death_p60: kase.ext['nmb_parent_death_p60'],
        prev_addr: kase.ext['prev_address'],
        sibling_death_p60: kase.ext['nmb_sibling_death_p60'],
        unemployed_why: kase.ext['unemployment_reason'],
        wop_rider_annual: kase.ext['wop_rider_annual'],
        wop_rider_monthly: kase.ext['wop_rider_monthly'],
        wop_rider_quarterly: kase.ext['wop_rider_quarterly'],
        wop_rider_semi_annual: kase.ext['wop_rider_semi_annual'],
      )
      kase.ext.delete 'adb_rider'
      kase.ext.delete 'adb_rider_annual'
      kase.ext.delete 'adb_rider_monthly'
      kase.ext.delete 'adb_rider_quarterly'
      kase.ext.delete 'adb_rider_semi_annual'
      kase.ext.delete 'nmb_bankruptcy_details'
      kase.ext.delete 'child_rider_annual'
      kase.ext.delete 'child_rider_monthly'
      kase.ext.delete 'child_rider_quarterly'
      kase.ext.delete 'child_rider_semi_annual'
      kase.ext.delete 'child_rider_units'
      kase.ext.delete 'child_wop_rider_annual'
      kase.ext.delete 'child_wop_rider_monthly'
      kase.ext.delete 'child_wop_rider_quarterly'
      kase.ext.delete 'child_wop_rider_semi_annual'
      kase.ext.delete 'nmb_heart_stroke_cancer'
      kase.ext.delete 'nmb_heart_stroke_cancer_details'
      kase.ext.delete 'nmb_parent_death_p60'
      kase.ext.delete 'prev_address'
      kase.ext.delete 'nmb_sibling_death_p60'
      kase.ext.delete 'unemployment_reason'
      kase.ext.delete 'wop_rider_annual'
      kase.ext.delete 'wop_rider_monthly'
      kase.ext.delete 'wop_rider_quarterly'
      kase.ext.delete 'wop_rider_semi_annual'
      kase.save!
    end
  end
end
