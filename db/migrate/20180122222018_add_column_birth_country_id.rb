class AddColumnBirthCountryId < ActiveRecord::Migration
  def up

  end

  def down
    consumers_w_country_ids=execute(%Q(
      SELECT id, birth_country_id
      FROM   crm_connections
      WHERE birth_country_custom_name IS NOT NULL
    )).to_a

    consumers_w_country_ids.in_groups_of(50) do |group|
      group.compact!
      group.each do |c|
        c_name=Country.find(c[1]).name
        Crm::Connection.find(c[0]).update_attributes(birth_country_custom_name:c_name)
      end
      sleep(1)
    end

    remove_column :crm_connections, :birth_country_id, :integer
    rename_column :crm_connections, :birth_country_custom_name, :birth_country
  end
end
