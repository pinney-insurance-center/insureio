class RemoveUnusedTableMovingViolation < ActiveRecord::Migration
  def up
    #This migration is irreversible, but safe because there is no code referencing it
    #and not a single record in the table in any of our environments.
    if ActiveRecord::Base.connection.table_exists? :quoting_moving_violations
      drop_table :quoting_moving_violations
    end
  end

  def down
    #No going back!
  end
end
