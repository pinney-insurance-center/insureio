class AddButtonTextFieldsToQuotingWidgets < ActiveRecord::Migration
  def change
    add_column :quoting_widget_widgets, :button2_text, :string, after: :button_text
    add_column :quoting_widget_widgets, :button3_text, :string, after: :button2_text
    execute(%Q(
      UPDATE quoting_widget_widgets
      SET    button2_text="Select",
             button3_text="Apply"
      WHERE  TRUE;
    ))
  end
end
