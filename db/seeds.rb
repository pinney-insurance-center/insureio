# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Add Carriers
require File::expand_path '../seeding/carriers', __FILE__

# Seed Igo data
require File::expand_path '../seeding/igo', __FILE__

# Seed ExamOne data
require File::expand_path '../seeding/exam_one', __FILE__

# Seed Marketech data
require File::expand_path '../seeding/marketech', __FILE__

# Seed the Marketing::Email::Template for Marketech esignature
require File::expand_path '../seeding/templates', __FILE__

require File::expand_path '../seeding/status_types', __FILE__

require File::expand_path '../seeding/brands', __FILE__

require File::expand_path '../seeding/tracking_tables', __FILE__