ActiveRecord::Base.logger = Logger.new STDOUT
carriers =  YAML.load_file File::expand_path '../carriers.yml', __FILE__
Carrier.transaction do
  Carrier.unscoped.delete_all
  carriers.each do |c|
    Carrier.create! c
  end
end
