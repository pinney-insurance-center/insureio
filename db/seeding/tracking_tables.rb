
lead_types=[
  "IO Quote Path Full Application",
  "IO Quote Path Referral"
]
lead_types.each do |lt|
  Crm::LeadType.create text: lt
end

referrers=[
  "Purchased Lead",
  "Affiliate Link"
]
referrers.each do |r|
  Crm::Referrer.create text: r
end

sources=[
  "IO Quote Path, started via Manual Entry By User",
  "IO Quote Path, started via Referral Link",
  "IO Quote Path Referral, started via Manual Entry By User",
  "IO Quote Path Referral, started via Referral Link",
]
sources.each do |s|
  Crm::Source.create text: s
end
