Processing::ExamOne::ControlCode.destroy_all

require 'spreadsheet'

# Create a ControlCode for each row in the worksheet
def digest_worksheet sheet, packet
  sheet.each(1) do |row|
    break if row[0].nil?
    # Create ControlCode
    options = {name:row[0], subsidiary:row[1], paramed:row[2], packet:packet, disability:row[4]}
    cc = Processing::ExamOne::ControlCode.create options
  end
end

ESIGN = nil
PICK_UP_PACKET = true
TAKE_OUT_PACKET = false

# Open workbook
book = Spreadsheet.open File.join Rails.root, 'db/seeding/exam_one_control_codes.xls'
no_packet_sheet = book.worksheet "PIN2 No Packet"
raise "Sought worksheet not found" if no_packet_sheet.nil?
take_out_sheet = book.worksheet "PIN2 Take Out Ptk"
raise "Sought worksheet not found" if take_out_sheet.nil?

# Digest worksheets
digest_worksheet no_packet_sheet, ESIGN
digest_worksheet take_out_sheet, TAKE_OUT_PACKET
