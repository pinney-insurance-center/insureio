# Marketech::Carrier
# data taken from pages 27-28 of "eDirect User Documentation.pdf"
# and from CLU db

Processing::Marketech::Carrier.destroy_all

yaml = YAML::load_file File::expand_path '../marketech_carriers.yml', __FILE__
yaml.each do |params|
  params.delete 'states'
  params.delete 'rules'
  carrier = Processing::Marketech::Carrier.new params
  carrier.id = params['id']
  carrier.save
end