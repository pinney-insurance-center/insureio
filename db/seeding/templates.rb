
#so far only this one template needs to be seeded into the database, since it is used by the Marketech#email action
Marketing::Email::Template.find_or_create_by(name: 'App Sent - eSign') do |user|
  user.description = 'Your application is ready for e-signature.',
  user.subject =     '{{client.first_name}}, your application is ready for e-signature',
  user.enabled =     true,
  user.template_purpose_id =  Enum::MarketingTemplatePurpose.id("Status"),
  user.body = 
'<p></p>
<table border="0" width="555" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr><th scope="col" bgcolor="#BCBEC0" width="556">
<table border="0" width="100%" cellspacing="5" cellpadding="5" align="left">
<tbody>
<tr><th scope="col">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>
<td valign="top" bgcolor="#FFFFFF">
<table class="Body" style="text-transform: none; color: #000; font-style: normal; font-variant: normal; font-weight: normal; font-size: 9pt; line-height: normal; font-family: Verdana, Geneva, sans-serif; border: none;" border="0" width="96%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td colspan="12" valign="top">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr><th scope="col" width="53%">
<div align="left">{{header}}</div>
</th></tr>
</tbody>
</table>
<br class="Header1" style="text-transform: none; color: #4679a8; font-style: normal; font-variant: normal; font-weight: bolder; font-size: 14pt; line-height: normal; font-family: ''Arial Black'', Gadget, sans-serif;" />
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr><th scope="col" valign="top" width="59%" height="61">
<div align="left">
<table border="0" width="100%" cellspacing="5" cellpadding="5">
<tbody>
<tr>
<td>
<div class="BodyTable_SpacedOut" style="text-transform: none; color: #58595b; text-align: left; font-style: normal; font-variant: normal; font-weight: lighter; font-size: 10pt; line-height: 18px; font-family: Verdana, Geneva, sans-serif; border: none;" align="left">
<p class="Header_Red_18" style="text-transform: none; color: #c90304; margin-bottom: 20px !important; margin-top: 10px !important; font-style: normal; font-variant: normal; font-weight: bold; font-size: 18pt; line-height: normal; font-family: Verdana, Geneva, sans-serif;">Your Application Is Ready <br /> for e-Signature</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;"><span class="Header_Gray" style="text-transform: none; color: #414042; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14pt; line-height: normal; font-family: Verdana, Geneva, sans-serif;"><img src="http://pinney.insureio.com/emails/images/Image_Signature.jpg" alt="" width="200" height="173" align="right" hspace="10" vspace="10" /></span>Hello {{client.first_name}},</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">Your {{case.submitted_details.carrier_name}} life insurance application is ready for e-signature!</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">Please click the link below to review, edit, and sign your application. It will only take a few minutes.</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">{{ marketech_esign_url }}</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">This e-signature process will shorten the processing time of your application by as much as two weeks.&nbsp;</p>
If you have any questions or concerns, please call me at {{agent.phone}}.
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">Best regards,</p>
<p style="font-family: Verdana, Geneva, sans-serif !important; margin-bottom: 20px !important; margin-top: 10px !important;">{{agent.first_name}} {{agent.last_name}}<br /> {{agent.title}}<br /> {{ brand_specific_fallback_primary_email_for_agent }} <br /> {{agent.phone}}<br /> {{agent.fax_number}}<br /> {{profile.address}} </p>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</th></tr>
<tr><th scope="col" valign="top"><span class="Subhead" style="text-transform: none; color: #4679a8; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12pt; line-height: normal; font-family: ''Arial Black'', Gadget, sans-serif;">&nbsp;</span></th></tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td colspan="12" valign="top" bgcolor="#005CAB"><img src="http://pinney.insureio.com/emails/images/Footer_Curve.jpg" alt="" width="536" height="50" />
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr><th scope="col" bgcolor="#005CAB" width="4%">
<div align="left">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</th><th scope="col" bgcolor="#005CAB" width="50%">
<div align="left"><br /><br /></div>
</th><th scope="col" bgcolor="#005CAB" width="45%">
<div class="WhiteText_SpacedOutMore" style="text-transform: none; color: #fff; font-style: normal; font-variant: normal; font-weight: bold; font-size: 11pt; line-height: 20px; font-family: Verdana, Geneva, sans-serif; border: none;" align="right">Call for Questions<br /> {{agent.phone}}</div>
</th><th scope="col" bgcolor="#005CAB" width="1%">
<div align="right">&nbsp;&nbsp;&nbsp;&nbsp;</div>
</th></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</th></tr>
</tbody>
</table>
</th></tr>
<tr><th scope="col" valign="top" bgcolor="#FFFFFF">
<div class="Footer" style="font-family: Verdana, Geneva, sans-serif; font-size: 8pt; color: #4679a8; line-height: 15px;" align="left">
<div class="FooterGray_LessSpace-Dark" style="font-family: Verdana, Geneva, sans-serif; font-size: 7pt; color: #999; line-height: 14px;" align="left">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr><th scope="col" valign="top"><br />
<div align="left">{{footer}}</div>
</th></tr>
</tbody>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr><th scope="col" valign="top" height="42">
<p><br /> {{email.unsubscribe}}</p>
</th></tr>
</tbody>
</table>
</div>
</div>
</th></tr>
</tbody>
</table>'
end