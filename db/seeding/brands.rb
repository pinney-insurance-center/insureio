brands=[
  {
    id: 107,
    ownership_id: 2,
    initial_status_type_id: 545,
    tenant_id: 0,
    full_name:'Life Insurance Division',
    company:'Life Insurance Division',
    preferred_insurance_division_subdomain: "life_insurance_division",
  },
  {
    id:21264,
    ownership_id: 3,
    initial_status_type_id: 125,
    tenant_id: 10,
    full_name:'National Mutual Benefit',
    company:'National Mutual Benefit',
    preferred_insurance_division_subdomain: "todd_ewing",
  },
]

brands.each do |b_attrs|
  b=Brand.find_or_initialize_by(id: b_attrs[:id])
  b.assign_attributes b_attrs
  b.save!(validate: false)
end
