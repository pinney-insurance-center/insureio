require 'spec_helper'
require 'redis_spec_helper'

describe SocketsController do

  before do
    authentication_stubs
    current_user.can!(:socket_comm, true)
  end

  #This is the most basic test.
  #It ensures that the app is referencing the correct server,
  #that the redis server process is running and listening on the correct port,
  #and that the server iptables and redis process are setup to accept remote connections.
  context 'when a ping is sent to redis server' do
    it 'returns "PONG"' do
      expected_response="PONG\n"
      command="redis-cli -h #{APP_CONFIG['socket']['redis_host']} ping"
      puts "\nRunning Shell Command:#{command}\n"
      actual_response=`#{command}`
      assert_equal expected_response, actual_response
    end
  end

  describe '#publish' do
    before do
      controller.stub(:redis)#This method is tested separately below.
    end
    context 'when user lacks socket permission' do
      before do
        #current_user.can!(:socket_comm, false)
        current_user.stub(:can?).and_return(true)
        current_user.stub(:can?).with(:socket_comm).and_return(false)
      end
      it 'returns permission denied' do
        post :publish, {mode:'alert', format: :json}
        response.code.should == "401"
      end
    end
    context "when specifying alert mode" do
      it 'returns 200' do
        post :publish, {mode:'alert'}
        response.code.should == "200"
      end
    end
    context "when specifying bulletin mode" do
      it 'returns 200' do
        post :publish, {mode:'bulletin'}
        response.code.should == "200"
      end
    end
    context "when specifying js mode" do
      it 'returns 200' do
        post :publish, {mode:'js'}
        response.code.should == "200"
      end
    end
  end
  describe 'private method `#recipients`' do
    context 'when it recieves a recipient id list which includes non-viewable users' do
      it 'scrubs non-viewable users from the list'
    end
  end
  describe 'private method `#redis`' do
    #This test adapted from the demo at:
    #http://blog.bigbinary.com/2015/05/09/verifying-pubsub-services-from-rails-redis.html
    #This implementation is much less extensive.
    #In future we should probably test pub/sub via unit tests and via tests within the NodeJS app.
    it 'publishes to redis' do
      controller.stub(:recipients).and_return( [16] )
      $redis.stub(:publish){|arg1,arg2| RedisWriterService.new(arg1,arg2).process }

      mode='bulletin'
      data={text:'Text', icon:'fa fa-bullhorn', id: SecureRandom.uuid}

      expected_return_val={type: "bulletin", recipients:[16], data: data}.to_json

      controller.send :redis, mode, data

      assert_equal expected_return_val, RedisReaderService.new(APP_CONFIG['socket']['redis_channel_for_socket']).process
    end
  end

end