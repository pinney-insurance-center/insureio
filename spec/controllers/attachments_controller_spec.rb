require 'spec_helper'

describe AttachmentsController do
  before { authentication_stubs }

  describe '#create' do
    it 'gives helpful error messages when params[:attachment] is missing' do
      post :create
      expect(response.status).to eq 422
      expect(response.body).to eq 'param is missing or the value is empty: attachment'
    end

    it '200s', :vcr do
      file = ActionDispatch::Http::UploadedFile.new tempfile: File.new('public/images/join-2.png'), content_type: 'image/png', original_filename: 'join-2.png'
      attachment = {
        file_for_upload: file,
        person_id: authenticated_user.id,
        person_type: 'User',
      }
      expect {
        post :create, attachment: attachment
        expect(response.status).to eq 200
      }.to change(Attachment, :count).by 1
    end
  end
end
