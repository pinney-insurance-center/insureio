require 'spec_helper'

describe UsersController do
  render_views

  before do
    authentication_stubs
    current_user.can!(:super_view, true)
  end

  describe '#hierarchy_breadcrumb' do
    context 'with a user who has several ancestors, where id order does not correspond to order of ascendancy in the hierarchy' do
      before(:each) do
        @ascendants=[
          FactoryBot.build_stubbed(:user, id: 25, parent_id:50),
          FactoryBot.build_stubbed(:user, id: 50, parent_id:nil),
          FactoryBot.build_stubbed(:user, id: 75, parent_id:25),
        ]
        @ascendant_ids_in_order=[50,25,75]
        @specified_user=FactoryBot.build_stubbed(:user, id: 30, parent_id:75)
        User.stub_chain(:where,:select,:preload,:first).and_return(@specified_user)
        User.stub_chain(:ascendants,:preload,:select).and_return(@ascendants)
        get :hierarchy_breadcrumb, {id:@specified_user.id, format:'json'}
      end
      it 'returns 200 status' do
        expect(response.code).to eql "200"
      end
      it 'returns the specified user with the expected attributes' do
        #expect(response.body).to include {id:some_id,parent_id:some_id}
      end
      it 'returns all ancestors of the specified user' do
        @ascendants.each do |u|
          expect(response.body).to include("\"id\":#{u.id}")
        end
      end
      it 'returns user and ancestors in the proper order' do
        response_obj=JSON.parse(response.body)
        ids_in_order=(@ascendant_ids_in_order+[@specified_user.id])
        ids_in_order.each_with_index do |uid,idx|
          expect(response_obj[idx]['id']).to eql(uid)
        end
      end
    end
  end

  #This member action doesn't check id. Only modifies current_user.
  describe '#common_tags' do
    it 'returns 200' do
      get :common_tags, id:1, format:'js'
      expect(response.code).to eql "200"
    end
  end

  #This member action doesn't check id. Only modifies current_user.
  describe '#add_common_tags' do
    context 'with params[:delete_tag]' do
      it 'returns 200' do
        post :add_common_tags, id:1, format:'js', delete_tag:'tag_key'
        expect(response.code).to eql "200"
      end
    end
    context 'with params[:tag]=~"k=v"' do
      it 'returns 200' do
        post :add_common_tags, id:1, format:'js', tag:'tag_key=tag_value'
        expect(response.code).to eql "200"
      end
    end
    context 'with params[:tag]=~"k"' do
      it 'returns 200' do
        post :add_common_tags, id:1, format:'js', tag:'tag_key'
        expect(response.code).to eql "200"
      end
    end
    context 'with params[:tag] & params[:delete_tag]' do
      it 'returns 200' do
        post :add_common_tags, id:1, format:'js', tag:'tag_key=tag_value', delete_tag:'tag_key'
        expect(response.code).to eql "200"
      end
    end
  end

  describe '#membership_levels' do
    let(:user){build :user }
    before do
      current_user.stub(:can?).and_return(true)
      current_user.stub(:pinney?).and_return(true)
    end
    context 'when requesting the html page' do
      it 'returns 200' do
        post :membership_levels, format:'html'
        expect(response.code).to eql "200"
      end
    end
    context 'when requesting the json for a given user\'s descendants' do
      it 'returns 200' do
        User.stub(:find).and_return(user)
        User.stub_chain(:descendants,:select,:group,:count).and_return({})
        post :membership_levels, id:1, format:'js'
        expect(response.code).to eql "200"
      end
    end
  end

  describe '#permissions' do
    let(:user){build :user }
    it 'returns 200' do
      User.stub(:find_by_id).and_return(user)
      post :permissions, id:1, format:'js'
      expect(response.code).to eql "200"
    end
  end

  describe 'true member action' do
    let(:user){build :user}
    before do
      user.id=User.maximum(:id).to_i.next
      User.stub(:find_by_id).with(user.id.to_s).and_return(user)
      user.stub(:persisted?).and_return(true)
      user.stub(:save).and_return(true)
    end

    describe '#l_and_c' do
      context 'for a non-recruit user' do
        before(:each) do
          user.stub(:recruit?){ false }
        end
        it 'returns 200' do
          get :l_and_c, id:user.id, format:'js'
          expect(response.code).to eql "200"
        end
      end
      context 'for a recruit user' do
        before(:each) do
          user.stub(:recruit?){ true }
          user.stub(:user_account){ User.last }
        end
        it 'returns 200' do
          get :l_and_c, id:user.id, format:'js'
          expect(response.code).to eql "200"
        end
      end
    end
  end

  describe '#update' do
    before do
      authentication_stubs build_stubbed(:user)
    end
    subject{ put :update, {format: :json, id: current_user.id}.merge(params) }
    context 'when misc permissions change' do
      let(:new_super_edit_value){true}
      before do
        current_user.password = 'true password'
        allow(User).to receive(:find).with(current_user.id.to_s).and_return(current_user)
        allow(current_user).to receive(:can_edit_user?).with(current_user,params[:user]).and_return(true)
      end
      context 'when params[:password] is nil' do
        let(:params){{user:{can_strict_super_edit:new_super_edit_value}}}
        it 'returns 401 with message' do
          expect(current_user).to_not receive(:crypted_password)
          expect(current_user).to_not receive(:save)
          subject
          expect(response.status).to be 401
          expect(JSON.parse(response.body)['error']).to be_present
        end
      end
      context 'when params[:password] is incorrect' do
        let(:params){{user:{can_strict_super_edit:new_super_edit_value}, password:'wrong password'}}
        it 'returns 401 with message' do
          expect(current_user).to receive(:crypted_password).and_call_original
          expect(current_user).to_not receive(:save)
          subject
          expect(response.status).to be 401
          expect(JSON.parse(response.body)['error']).to be_present
        end
      end
      context 'when params[:password] is correct' do
        let(:params){{user:{can_strict_super_edit:new_super_edit_value}, password:'true password'}}
        it 'returns 200' do
          expect(current_user).to receive(:crypted_password).and_call_original
          expect(current_user).to receive(:save).and_return(true)
          subject
          expect(response.status).to be 200
        end
      end
    end
    context 'when misc permissions do not change' do
      let(:params){{user:{can_strict_lead_distribution:true}}}
        before do
          allow(User).to receive(:find).with(current_user.id.to_s).and_return(current_user)
          allow(current_user).to receive(:can_edit_user?).with(current_user,params[:user]).and_return(true)
          allow(current_user).to receive(:parent_id_changed?).and_return(false)
          allow(current_user).to receive(:permissions_0_changed?).and_return(false)
          allow(current_user).to receive(:permissions_1_changed?).and_return(false)
          allow(current_user).to receive(:permissions_2_changed?).and_return(false)
          allow(current_user).to receive(:permissions_3_changed?).and_return(false)
          allow(current_user).to receive(:permissions_4_changed?).and_return(false)
        end
        it 'returns 200' do
          expect(current_user).to_not receive(:crypted_password)
          expect(current_user).to receive(:save).and_return(true)
          subject
          expect(response.status).to be(200), response.body
        end
    end
  end

end