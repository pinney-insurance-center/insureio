require 'spec_helper'

describe Crm::CasesController do
  render_views

  before do
    authentication_stubs
  end

  describe '#involved_parties' do
    let(:cons) { create :consumer }
    let(:kase) { create :crm_case, consumer: cons }

    it '200s with ltd_auth' do
      authentication_unstub
      access_code = cons.agent.access_code cons
      get :involved_parties, format: :json, id: kase.id, ltd_access_code: access_code
      expect(response.status).to eq 200
      expect(response.body).to match /\{.+\}/
    end
  end

  describe '#update' do
    subject{ put :update, params }
    let(:cons) { create :consumer }
    let(:kase) { create :crm_case, consumer: cons }
    let(:params){{id:kase.id, crm_case:{}}}
    before { authentication_stubs cons.agent }

    it 'creates a note when agent_id is in params' do
      params[:crm_case][:agent_id] = 33
      expect(kase).to receive(:agent).and_return build_stubbed :user
      expect(kase).to receive(:agent).and_return build_stubbed :user
      expect{subject}.to change(Note, :count).by(1)
    end

    it 'creates a note when agent_of_record_id is in params' do
      params[:crm_case][:agent_of_record_id] = 33
      expect(kase).to receive(:agent_of_record).and_return build_stubbed :user
      expect(kase).to receive(:agent_of_record).and_return build_stubbed :user
      expect{subject}.to change(Note, :count).by(1)
    end

    it 'creates a note when status_type_id is in params' do
      @status_call_counter = 0
      @first_status = build_stubbed :crm_status
      @second_status = build_stubbed :crm_status
      params[:crm_case][:status_type_id] = 33
      expect(kase).to receive(:status).at_least(3).times do
        @status_call_counter += 1
        if @status_call_counter <= 2
          @first_status
        else
          @second_status
        end
      end
      expect{subject}.to change(Note, :count).by(1)
    end

    it '200s for params[:name] + params[:value]' do
      put :update, id: kase.id, name: 'exam_company', value: 'ExamOne'
      expect(response.status).to eq(200), response.body
    end

    it '422s when no update attrs are given' do
      put :update, id: kase.id
      expect(response.status).to eq(422)
    end
  end
end
