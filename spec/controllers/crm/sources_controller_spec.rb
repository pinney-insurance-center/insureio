require 'spec_helper'

#Minimal data for these actions to render is set in "/db/seeding/tracking_tables.rb"
#These should still pass if other lead_types/referrers/sources are added to the test db.

describe Crm::SourcesController do
  render_views
  let(:model){ Crm::Source }

  before do
    authentication_stubs
    allow(current_user).to receive(:lead_sources).and_return(model.all)
  end

  describe '#index' do
    subject{ get :index, params }
    context 'with no params specified' do
      let(:params){ {format:'json'} }
      it 'does not raise error' do
        expect{ subject }.to_not raise_error
        expect(response.status).to eq 200
      end
      it 'renders the entire list' do
        subject
        expect( response.body ).to eq( model.all.to_json )
      end
    end
    context 'with a search string parameter provided' do
      let(:params){ {format:'json', q:'by user'} }
      it 'does not raise error' do
        expect{ subject }.to_not raise_error
        expect(response.status).to eq 200
      end
      it 'renders the filtered list' do
        subject
        expect( response.body.length ).to be < model.all.to_json.length
        partial_json_list_string="[{\"id\":1,\"text\":\"IO Quote Path, started via Manual Entry By User\"},{\"id\":3,\"text\":\"IO Quote Path Referral, started via Manual Entry By User\"}"
        expect( response.body ).to include(partial_json_list_string)
      end
    end
  end
end