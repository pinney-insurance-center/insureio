require 'spec_helper'

describe UserSessionsController do
  it 'redirects to the user\'s tenant', type: :request do
    user = create :user, tenant_id: 3
    host! 'badsubdomain.insureio.com'
    post '/user_sessions', usage_user_session: { login: user.login, password: user.password }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body)['redirect']).to eq('//ezlife.insureio.com/')
  end
end
