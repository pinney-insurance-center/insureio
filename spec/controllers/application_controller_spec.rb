require 'spec_helper'

describe ApplicationController, type: :request do
  include AuthenticationHelper

  let(:user) { create :user, tenant_id: 1 }
  let(:other_user) { create :user, tenant_id: 2 }

  describe '#ltd_auth' do
    let(:cons) { create :consumer, agent: user }
    let(:other_cons) { create :consumer, agent: user }

    it 'works with either params or session' do
      access_code = user.access_code cons
      query = { ltd_access_code: access_code }.to_query
      # No authentication
      get "/consumers/#{cons.id}"
      expect(response.body).to match /You need to login first/
      expect(response.status).to eq(303)
      # Authenticate with message params
      get "/consumers/#{cons.id}?#{query}"
      expect(response.status).to eq(200)
      # Authenticate with session
      get "/consumers/#{cons.id}"
      expect(response.status).to eq(200)
      # Authenticate with session (for another Consumer)
      get "/consumers/#{other_cons.id}"
      expect(response.status).to eq(403)
      # Authenticate with message params (for another Consumer)
      get "/consumers/#{other_cons.id}?#{query}"
      expect(response.status).to eq(403)
      # Try to access an endpoint which does not allow `ltd_auth`
      get "/dashboard?#{query}"
      expect(response.body).to match /You need to login first/
      expect(response.status).to eq(303)
    end
  end

  describe '#require_login' do
    context 'when not authenticated' do
      it 'redirects for html request' do
        get '/dashboard'
        expect(response.body).to match /You need to login first/
        expect(response.status).to eq(303)
      end

      it 'gives error for json request' do
        get '/dashboard.json'
        expect(response.body).to match /You need to login first/
        expect(response.status).to eq(401)
      end
    end

    context 'when user is not enabled' do
      before do
        create_login_session
        authenticated_user.update_columns active_or_enabled: false
      end

      it 'redirects for html request' do
        get '/dashboard'
        expect(response.body).to match /Your account must be enabled/
        expect(response.status).to eq(303)
      end

      it 'gives error for json request' do
        get '/dashboard.json'
        expect(response.body).to match /Your account must be enabled/
        expect(response.status).to eq(401)
      end
    end

    context 'when user payment has lapsed' do
      before do
        create_login_session
        authenticated_user.update_columns payment_due: 1.week.ago, membership_id: 1
        expect(Stripe::Customer).to receive(:retrieve) do
          double( subscriptions: double( retrieve: double( current_period_end: 1.week.ago ) ))
        end
      end

      it 'redirects for html request' do
        get '/dashboard'
        expect(response.location).to match /\/payment_info.html/
        expect(response.status).to eq(303)
      end

      it 'gives error for json request' do
        get '/dashboard.json'
        expect(response.body).to match /Your subscription has expired/
        expect(response.status).to eq(403)
      end
    end

    context 'when tos is not complete' do
      before do
        create_login_session
        authenticated_user.update_columns tos: false
      end

      it 'redirects for html request' do
        get '/dashboard'
        expect(response.location).to match /\/tos/
        expect(response.status).to eq(303)
      end

      it 'gives error for json request' do
        get '/dashboard.json'
        expect(response.body).to match /You must accept the TOS/
        expect(response.status).to eq(403)
      end
    end

    context 'when the tenant doesn\'t match the request domain' do
      before do
        domain = "ezlife.insureio.com"
        authenticated_user.update_columns tenant_id: 3 # ezlife
        host! domain
        create_login_session domain
        authenticated_user.update_columns tenant_id: 0 # pinney
      end

      it 'redirects for html request' do
        get '/dashboard'
        expect(response.status).to eq(303)
        expect(response.location).to match /:\/\/pinney\.insureio\.com\/dashboard/
      end

      it 'gives error for json request' do
        get '/dashboard.json', format: :json
        expect(response.body).to match /:\/\/pinney\.insureio\.com\/dashboard/
        expect(response.status).to eq(303)
      end
    end

    context 'when all checks pass' do
      it 'gives a 200 response' do
        create_login_session
        get '/dashboard'
        expect(response.status).to eq(200)
      end
    end
  end

  describe '#require_login_or_quoter_key' do
    context 'when correct auth params are provided in params, and login session is also present' do
      it 'correctly sets thread variables to reflect params' do
        session_user = authenticated_user
        api_user = other_user
        host! "#{api_user.tenant.name}.insureio.com"
        create_login_session "#{api_user.tenant.name}.insureio.com"
        api_user.can!(:motorists_rtt_process_details) # Grants cross-tenancy-permission and rtt/agent_details permission
        api_user.save!
        get "/rtt/agent_summary?agent_id=#{api_user.id}&key=#{api_user.quoter_key}"
        expect(response.status).to eq(200)
        expect(Thread.current[:current_user_auth]).to eq(:quoter_key)
        expect(Thread.current[:current_user_id]).to eq(api_user.id)
        expect(Thread.current[:current_user_acts_across_tenancies]).to eq(true)
        expect(Thread.current[:tenant_id]).to eq(api_user.tenant_id)
      end
    end

    context 'when incorrect auth params are provided, but login session is also present' do
      it 'correctly sets thread variables to reflect login session' do
        session_user = authenticated_user # `authenticated_user` comes from `login` above
        api_user = other_user
        host! "#{session_user.tenant.name}.insureio.com"
        create_login_session "#{session_user.tenant.name}.insureio.com"
        Usage::Permissions::CROSS_TENANCY_PERMISSIONS.each do |perm|
          session_user.can!(perm, false)
        end
        allow_any_instance_of(User).to receive(:ok_for_rtt_process?).and_return(true)
        session_user.save!
        get "/rtt/agent_summary?agent_id=#{other_user.id}&key=an_obvious_wrong_quoter_key"
        expect(response.status).to eq(200)
        expect(Thread.current[:current_user_auth]).to eq(:session)
        expect(Thread.current[:current_user_id]).to eq(session_user.id)
        expect(Thread.current[:current_user_acts_across_tenancies]).to eq(false)
        expect(Thread.current[:tenant_id]).to eq(session_user.tenant_id)
        expect(assigns(:controller_warnings)).to include("Wrong agent id/key combo.")
      end
    end
  end

  describe '#require_tenant_match' do
    it 'redirects users whose request doesn\'t match their tenant' do
      create_login_session "pinney.insureio.com"
      expect(authenticated_user.tenant.name).to eq('pinney')
      #This action was chosen because it requires login.
      #Actions that do not require any authentication do not need to check credentials.
      host! "ezlife.insureio.com"
      get '/dashboard'
      expect(response.status).to eq 303
      expect(response.location).to eq 'http://pinney.insureio.com/dashboard'
    end
  end

  describe '#require_widget_and_user' do
    let(:widget) { Quoting::Widget.create! user: user, hostname: 'http://foo.bar' }
    let(:params) {{ widget_id: widget.id, key: user.quoter_key, agent_id: user.id, consumer: { full_name: 'cobb', agent_id: user.id, brand_id: user.default_brand_id }}}

    it '200s if a widget and user are provided' do
      post "/consumers.json", params
      expect(response.status).to eq(200)
    end

    it '401s when user is not present' do
      post "/consumers.json", params.except(:agent_id)
      expect(response.status).to eq(401)
    end

    it '403s with message when user lacks :quoter_widget permission' do
      user.can! :super_edit, false; user.can! :quoter_widget, false; user.save!
      post "/consumers.json", params
      expect(response.status).to eq(403)
      expect(response.body).to match 'Your account does not currently have access to quoter widgets.'
    end

    it '404s with message when widget is absent' do
      post "/consumers.json", params.merge(widget_id: 7777) # non-existant id
      expect(response.status).to eq(404)
      expect(response.body).to match 'No widget for id.'
    end
  end
end

describe ApplicationController, type: :controller do
  let(:user      ){ create :user, tenant_id: 2 }
  let(:other_user){ create :user, tenant_id: 2 }

  before do
    # Initialize users
    user
    other_user
    # Active Authlogic::Session::Base.controller s.t. it can look for User
    Authlogic::Session::Base.controller = Authlogic::ControllerAdapters::RailsAdapter.new(controller)
    # Remove existing logged-in session
    controller.session.destroy
  end

  describe '#current_user' do
    before do
      allow(controller).to receive(:response) { @response ||= ActionDispatch::Response.new }
    end

    it 'returns nil by default' do
      expect(controller.send(:current_user)).to be_nil
      expect(Thread.current[:current_user_auth]).to be_nil
    end

    shared_examples_for 'with params[:key]' do |key_field|
      context "holding #{key_field}" do
        it 'returns a user for match' do
          controller.params = { key: other_user.send(key_field), agent_id: user.id }
          expect(controller.send(:current_user)).to be_nil
          controller.params = { key: user.send(key_field), agent_id: other_user.id }
          expect(controller.send(:current_user)).to be_nil
          controller.params = { key: user.send(key_field), agent_id: user.id }
          expect(controller.send(:current_user)&.id).to eq(user.id)
        end

        it 'sets current_user_auth' do
          controller.params = { key: user.send(key_field), agent_id: user.id }
          expect(controller.send(:current_user)).to be_present
          debugging_info= ->{ JSON.pretty_generate({
            current_user_id:          controller.send(:current_user).id,
            params:                   controller.params,
            true_current_user_id:     Thread.current[:true_current_user_id],
            current_user_id:          Thread.current[:current_user_id],
            current_user_auth_method: Thread.current[:current_user_auth],
            tenant_id:                Thread.current[:tenant_id],
            current_user_acts_across_tenancies: Thread.current[:current_user_acts_across_tenancies],
          }) }
          expect(Thread.current[:current_user_auth]).to eq(key_field), debugging_info
        end
      end
    end
    it_behaves_like 'with params[:key]', :api_key

    context "with quoter_key authentication allowed" do
      before do
        controller.instance_variable_set :@additional_auth_keys, :quoter_key
      end

      it_behaves_like 'with params[:key]', :quoter_key
    end

    context 'with basic auth' do
      def set_auth_header id, key
        b64 = Base64.encode64 [id, key].join(':')
        controller.request.headers["Authorization"] = "Basic #{b64}"
      end

      it 'does not honour quoter_key matches' do
        set_auth_header user.id, user.send(:quoter_key)
        expect(controller.send(:current_user)).to be_nil
        expect(Thread.current[:current_user_auth]).to be_nil
      end

      it 'returns a user for an api_key match' do
        set_auth_header user.id, other_user.send(:api_key)
        expect(controller.send(:current_user)).to be_nil
        set_auth_header other_user.id, user.send(:api_key)
        expect(controller.send(:current_user)).to be_nil
        set_auth_header user.id, user.send(:api_key)
        expect(controller.send(:current_user)&.id).to eq(user.id)
      end

      it 'sets current_user_auth' do
        controller.params = { key: user.send(:api_key), agent_id: user.id }
        controller.send(:current_user)
        expect(Thread.current[:current_user_auth]).to eq(:api_key)
      end
    end

    context 'when impersonating' do
      before do
        controller.params = { key: user.api_key, agent_id: user.id }
        controller.session[:ostensible_user_id] = other_user.id
        allow_any_instance_of(User).to receive(:editable?) { |arg_user, this_user|
          this_user.id == user.id && arg_user.id == other_user.id
        }
      end

      it 'returns the ostensible (impersonated) user' do
        expect(controller.send(:current_user)&.id).to eq(other_user.id)
        expect(Thread.current[:current_user_id]).to eq(other_user.id)
        expect(Thread.current[:true_current_user_id]).to eq(user.id)
      end

      it 'returns the authenticated user if param `false` is supplied to `current_user`' do
        expect(controller.send(:current_user, false)&.id).to eq(user.id)
        expect(Thread.current[:current_user_id]).to eq(user.id)
        expect(Thread.current[:true_current_user_id]).to eq(user.id)
      end
    end
  end
end
