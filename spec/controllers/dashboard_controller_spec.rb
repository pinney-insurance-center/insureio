require 'spec_helper'
require 'pry'
describe DashboardController do
  before do
    authentication_stubs
    authenticated_user.can! :view_descendants_resources
  end

  describe '#ezl_cases' do
    context 'when team is true, and ordering is by premium' do
      let(:order_column){ 'premium' }
      let(:team        ){ 1 }
      context 'and user has team members' do
        before do
          allow(authenticated_user).to receive(:descendant_ids).and_return([authenticated_user.id.to_i+1])
        end
        it '200s' do # this was 500-ing for at least 2.5 years
          get :ezl_cases, format: :json, order_column: order_column, team: team, due: 'All', order_direction: 'DESC'
          expect(response.status).to eq 200
          expect(JSON.parse(response.body)['cases']).to match_array []
        end
      end
      context 'and user lacks team members' do
        it '422s with useful error message' do
          get :ezl_cases, format: :json, order_column: order_column, team: team, due: 'All', order_direction: 'DESC'
          expect(response.status).to eq 422
          expect(JSON.parse(response.body)['errors']).to match(/You do not appear to have any team members yet/)
        end
      end
    end
  end
end
