require 'spec_helper'
require 'rspec-rails'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end

describe Quoting::WidgetsController do
  let(:user){      build :user}
  let(:widget){    Quoting::Widget.new user_id:user_id, brand_id:brand_id, hostname:'http://www.example.com' }
  let(:user_id){   User.maximum(:id).to_i.next}
  let(:widget_id){ Quoting::Widget.maximum(:id).to_i.next}
  let(:brand){   build :brand}
  let(:brand_id){Brand.maximum(:id).to_i.next}
  before do
    user.id=user_id
    User.stub(:find).with(user_id).and_return(user)
    User.stub(:find).with(brand_id).and_return(brand)
    Quoting::Widget.stub(:find).with(widget_id.to_s).and_return(widget)
    Quoting::Widget.stub(:find_by_id).with(widget_id.to_s).and_return(widget)
    Quoting::Widget.any_instance.stub(:user).and_return(user)
    Consumer.any_instance.stub(:brand).and_return(brand)
  end

  describe '#show' do
    it 'returns 200' do
      get :show, id:widget_id, format: :js
      response.code.should == "200"
    end
    it 'returns 200' do
      get :show, id:widget_id, format: :html
      response.code.should == "200"
    end
  end

end
