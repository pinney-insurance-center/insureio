require 'spec_helper'
#VCR.use_cassette
describe Quoting::QuotesController do
  render_views

  def produces_valid_consumer_and_case
    saved_consumer_instance= assigns(:consumer)
    saved_case_instance= saved_consumer_instance.cases.last
    expect( saved_consumer_instance ).to_not be_blank
    expect( saved_consumer_instance.valid? ).to eq(true), saved_consumer_instance.errors.full_messages.join(' ') # If neither proc nor string, the extra argument gets ignored
    expect( saved_case_instance ).to_not be_blank
    expect( saved_case_instance.valid? ).to eq(true), saved_case_instance.errors.full_messages.join(' ') # If neither proc nor string, the extra argument gets ignored
  end

  let(:brand) do
    create(:brand, tenant_id: 0).tap do |brand|
      params['consumer']['brand_id'] = brand.id.to_s
      (params.dig('consumer', 'cases_attributes') || []).each do |kase|
        (kase['stakeholder_relationships_attributes'] || []).each do |bene|
          bene.dig('stakeholder_attributes').try(:[]=, 'brand_id', brand.id)
        end
      end
    end
  end
  let(:agent) do
    create(:user, tenant_id: 0).tap do |user|
      params['consumer']['agent_id'] = user.id.to_s
      (params.dig('consumer', 'cases_attributes') || []).each do |kase|
        (kase['stakeholder_relationships_attributes'] || []).each do |bene|
          bene.dig('stakeholder_attributes').try(:[]=, 'agent_id', user.id)
        end
      end
      user.brands = [brand]
    end
  end
  let(:params) do
    JSON.parse(File.read(params_file)).tap do |data|
      data[:format] = :json
    end
  end

  describe '#save_from_quote_path, with a logged-in user,', :vcr do
    before :each do
      authentication_stubs agent
    end

    let(:params_file) { "#{Rails.root}/spec/support/quote_form.json" }

    context 'for new record' do
      it 'should return 200' do
        post :save_from_quote_path, params
        produces_valid_consumer_and_case
        expect( response.code ).to eql "200"
      end

      context 'with non-compulife-able health issues' do
        let(:params_file){ "#{Rails.root}/spec/support/quote_form_health_problems.json" }
        it 'should return 200' do
          post :save_from_quote_path, params
          produces_valid_consumer_and_case
          expect( response.code ).to eql "200"
        end
      end

      context 'given legacy health info fields' do
        # This check is calculated to only check whether the controller
        # receives and passes data effectively. Checking the values is tested
        # in the model specs.
        it 'should return 200' do
          params = {"consumer":{"emails_attributes":[{"value":"foo@bar.baz"}],"addresses_attributes":[{"state_id":1}],"brand_id":brand.id,"agent_id":agent.id,"health_info_attributes":{"clear_prior_family_diseases":true,"family_diseases_attributes":[{"parent":"1","age_of_contraction":4,"age_of_death":5,"coronary_artery_disease":true,"breast_cancer":true,"malignant_melanoma":true,"ovarian_cancer":false}],"tobacco_cigarettes_per_day":5,"feet":5,"inches":6,"weight":167,"tobacco":true,"tobacco_cigarettes_current":"1","tobacco_cigars_current":"1","cigars_per_month":6,"tobacco_pipe_current":false,"pipes_per_year":3,"tobacco_pipe_last":"03/04/1967","bp":true,"bp_systolic":130,"bp_diastolic":82,"bp_last_treatment":"04/05/2000","bp_control_start":"2019/09/26","cholesterol":true,"cholesterol_level":134,"cholesterol_hdl":3.7,"cholesterol_last_treatment":"02/06/1987","diabetes_1":true,"copd":true,"diabetes_2":true,"asthma":true,"sleep_apnea":true,"crohns":true,"breast_cancer":true,"hepatitis_c":true,"drug_abuse":true,"alcohol_abuse":true,"stroke":true,"irregular_heartbeat":true,"heart_murmur":true,"atrial_fibrillation":true,"multiple_sclerosis":true,"parkinsons":true,"epilepsy":true,"depression":true,"anxiety":true,"diabetes_1_diagnosed_date":"11/11/1988","diabetes_1_last_a1_c":"5","diabetes_1_complications":"","diabetes_1_currently_treated":"1","diabetes_1_insulin_units":5,"diabetes_1_average_a1_c":"7.8","diabetes_2_diagnosed_date":"11/11/1998","asthma_condition_degree":"Moderate","asthma_episodes_year":7,"copd_diagnosed_date":"11/11/1998","diabetes_2_last_a1_c":"7.8","diabetes_2_average_a1_c":"9","diabetes_2_complications":"","diabetes_2_currently_treated":"","diabetes_2_last_treatment_date":"11/11/1998","diabetes_2_insulin_units":67,"asthma_diagnosed_date":"11/11/1988","asthma_ever_treated":"1","asthma_currently_treated":"1","asthma_inhaled_bronchodilators":"1","asthma_inhaled_corticosteroids":"1","asthma_oral_medication_no_steroids":"1","asthma_rescue_inhaler":"1","asthma_oral_medication_steroids":"1","asthma_ever_hospitalized":"1","asthma_hospitalized_count_past_year":8,"asthma_last_hospitalized_date":"11/11/1998","asthma_work_absence":"1","asthma_fev1":5,"asthma_ever_life_threatening":"1","asthma_last_life_threatening_date":"11/11/2000","asthma_is_treatment_compliant":"1","copd_condition_degree":"Mild","copd_fev1":-26,"copd_has_symptoms":"1","copd_copd_severity":"Severe","sleep_apnea_diagnosed_date":"11/11/1978","sleep_apnea_condition_degree":"Mild","sleep_apnea_ever_treated":"1","sleep_apnea_treatment_start_date":"11/11/1999","sleep_apnea_use_cpap":"1","sleep_apnea_rd_index":7,"sleep_apnea_apnea_index":6,"sleep_apnea_ah_index":5,"sleep_apnea_o2_saturation":4,"sleep_apnea_cpap_machine_complaint":"1","sleep_apnea_sleep_study":"1","sleep_apnea_sleep_study_followup":"1","sleep_apnea_on_oxygen":"1","sleep_apnea_currently_sleep_apnea":"1","cancer_breast_diagnosed_date":"11/11/1999","cancer_breast_cancer_stage":7,"cancer_breast_metastatis":"1","cancer_breast_tumorsize":4,"cancer_breast_last_treatment_date":"11/11/1999","cancer_breast_reoccurrence":"1","cancer_breast_last_mammogram_date":"11/11/1999","cancer_breast_currently_treating":"1","cancer_breast_ductal_carcinoma":"1","cancer_breast_diagnosed_dcis":"1","cancer_breast_dcis_removed":"1","cancer_breast_lobular_carcinoma":"1","cancer_breast_diagnosed_lcis":"1","cancer_breast_number_of_lesions":8,"cancer_breast_lesion_size":7,"cancer_breast_comedonecrosis":"1","cancer_breast_lumpectomy":"1","cancer_breast_mastectomy":"1","cancer_breast_single_mastectomy":"1","cancer_breast_double_mastectomy":"1","cancer_breast_negative_sentinel_lymph_exam":"1","cancer_breast_radiation_treatment":"1","cancer_breast_endocrine_therapy":"1","cancer_breast_endocrine_treated":"1","cancer_breast_cancer_grade":4,"anxiety_diagnosed_date":"11/11/1999","anxiety_condition_degree":"Mild","anxiety_ever_hospitalized":"1","anxiety_last_hospitalized_date":"11/11/1999","anxiety_episodes_year":6,"anxiety_currently_treated":"1","anxiety_outpatient_care":"1","anxiety_inpatient_care":"1","anxiety_work_absence":"1","anxiety_ever_suicide_attempt":"1","anxiety_last_suicide_attempt_date":"11/11/1999","depression_diagnosed_date":"11/11/1999","depression_condition_degree":"Mild","depression_ever_hospitalized":"1","depression_last_hospitalized":"11/11/1999","depression_currently_treated":"1","depression_in_psychotherapy":"1","depression_responding_well":"1","depression_work_absence":"1","depression_ever_suicide_attempt":"1","depression_last_suicide_attempt_date":"11/11/1999","epilepsy_diagnosed_date":"11/11/1999","epilepsy_condition_degree":"Mild","epilepsy_ever_treated":"1","epilepsy_ever_surgery":"1","epilepsy_surgery_date":"11/11/1999","epilepsy_seizure_type":"poiu","epilepsy_controlled_seizures":"1","epilepsy_neurological_evaluation":"1","epilepsy_neurological_normal":"1","epilepsy_caused_by_other":"1","epilepsy_last_seizure_date":"11/19/1999","epilepsy_seizures_per_year":99,"epilepsy_thirty_minute_plus":2,"epilepsy_ever_medication":"1","parkinsons_diagnosed_date":"11/11/1999","parkinsons_age_at_diagnosis":8,"parkinsons_condition_degree":"Mild","parkinsons_live_independently":"1","parkinsons_condition_stable":"1","parkinsons_currently_disabled":"1","parkinsons_disabled_severity":"dfg","parkinsons_currently_receive_treatment":"1","parkinsons_rigidity":"1","parkinsons_rigidity_severity":"wert","parkinsons_stable_date":"11/11/1999","parkinsons_walking_impairment":"1","parkinsons_walking_impairment_severity":"ijh","parkinsons_mental_deterioration":"1","parkinsons_affect_fingers_only":"1","parkinsons_affect_hands_only":"1","parkinsons_affect_multiple_areas":"1","multiple_sclerosis_diagnosed_date":"11/11/1999","multiple_sclerosis_condition_degree":"Mild","multiple_sclerosis_attacks_per_year":3,"multiple_sclerosis_last_attack_date":"11/11/1999","multiple_sclerosis_condition_type":"lkjh","crohns_diagnosed_date":"11/11/1999","crohns_condition_degree":"Mild","crohns_stabilization_date":"11/11/1999","crohns_last_attack_date":"11/11/1999","crohns_ever_steroid":"1","crohns_currently_steroids":"1","crohns_steroid_stop_date":"11/11/1999","crohns_ever_immuno_suppressants":"1","crohns_current_immuno_suppressants":"1","crohns_immunosuppressants_stop_date":"11/11/1999","crohns_limited_to_colon":"1","crohns_complications":"1","crohns_surgery":"1","crohns_surgery_date":"11/11/1999","crohns_weight_stable":"1","atrial_fibrillation_diagnosed_date":"11/11/1999","atrial_fibrillation_condition_degree":"Mild","atrial_fibrillation_fibrillation_type":"cvbn","atrial_fibrillation_last_episode_date":"11/11/1999","atrial_fibrillation_episode_length":7,"atrial_fibrillation_self_resolve":"1","atrial_fibrillation_ever_cardiac_eval":"1","atrial_fibrillation_cardiac_eval_date":"11/11/1999","atrial_fibrillation_cardiac_eval_result":"1","atrial_fibrillation_heart_disease":"1","atrial_fibrillation_shortness_of_breath":"1","atrial_fibrillation_current_medications":"1","atrial_fibrillation_ablation_procedure":"1","atrial_fibrillation_ablation_date":"11/11/1999","atrial_fibrillation_ablation_result":"1","atrial_fibrillation_is_controlled":"1","heart_murmur_diagnosed_date":"11/11/1999","heart_murmur_condition_degree":"Mild","heart_murmur_ever_echocardiogram":"1","heart_murmur_valve_structures_normal":"1","heart_murmur_valve_surgery":"1","heart_murmur_heart_enlargement":"1","heart_murmur_symptomatic":"1","heart_murmur_progression":"1","irregular_heartbeat_diagnosed_date":"11/11/1999","irregular_heartbeat_underlying_heart_disease":"1","irregular_heartbeat_current_symptoms":"1","irregular_heartbeat_current_medications":"1","irregular_heartbeat_atrioventricular_block":"1","irregular_heartbeat_second_degree_av_block":"1","irregular_heartbeat_third_degree_av_block_atr_disassociation":"1","irregular_heartbeat_born_with_third_degree_av_block":"1","irregular_heartbeat_pacemaker_implanted":"1","irregular_heartbeat_pacemaker_implant_date":"11/11/1999","irregular_heartbeat_attrioventricular_junctional_rhythm":"1","irregular_heartbeat_paroxysmal_super_tachycardia":"1","irregular_heartbeat_cardiac_evaluations":"1","irregular_heartbeat_cardiac_eval_result_normal":"1","irregular_heartbeat_last_experience_symptoms":"11/11/1999","irregular_heartbeat_symptoms_per_year":6,"irregular_heartbeat_premature_atrial_complexes":"1","irregular_heartbeat_history_of_cardiovascular_disease":"1","irregular_heartbeat_premature_ventricular_contraction":"1","irregular_heartbeat_simple_pvc":"1","irregular_heartbeat_complex_pvc":"1","irregular_heartbeat_require_treatment_for_pvc":"1","irregular_heartbeat_sick_sinus_syndrome":"1","irregular_heartbeat_pacemaker_for_sick_sinus_syndrome":"1","irregular_heartbeat_pacemaker_for_sick_sinus_syndrome_implant_date":"11/11/1999","irregular_heartbeat_history_of_fainting":"1","irregular_heartbeat_sinus_bradycardia":"1","irregular_heartbeat_sinus_bradycardia_caused_by_another_condition":"1","irregular_heartbeat_sinus_bradycardia_caused_by_medication":"1","irregular_heartbeat_sinus_bradycardia_cause_unknown":"1","irregular_heartbeat_wandering_pacemaker":"1","irregular_heartbeat_pulse_rate":9,"irregular_heartbeat_cardiac_eval_for_wandering_pacemaker":"1","irregular_heartbeat_idoventricular_rhythm":"1","irregular_heartbeat_mobitz_type_1_block":12,"irregular_heartbeat_mobitz_type_2_block":6,"stroke_condition_degree":"Mild","stroke_last_stroke_date":"11/11/1999","stroke_multiple_strokes":"1","stroke_first_stroke_age":8,"alcohol_abuse_diagnosed_date":"11/11/1999","alcohol_abuse_currently_consuming":"1","alcohol_abuse_last_consumed_date":"11/11/1999","alcohol_abuse_ever_treated":"1","alcohol_abuse_currently_treated":"1","alcohol_abuse_treatment_end_date":"11/11/1999","alcohol_abuse_ever_relapse":"1","alcohol_abuse_last_relapse_date":"11/11/1999","drug_abuse_diagnosed_date":"11/11/1999","drug_abuse_currently_using":"1","drug_abuse_last_used_date":"11/11/1999","drug_abuse_ever_treated":"1","drug_abuse_currently_treated":"1","drug_abuse_treatment_end_date":"11/11/1999","drug_abuse_ever_relapse":"1","drug_abuse_ever_convicted":"1","drug_abuse_last_relapse_date":"11/11/1999","hepatitis_c_diagnosed_date":"11/11/1999","hepatitis_c_contraction_date":"11/11/1999","hepatitis_c_condition_degree":"Mild","hepatitis_c_normal_viral_loads":"1","hepatitis_c_normal_viral_date":"11/11/1999","hepatitis_c_normal_liver_functions":"1","hepatitis_c_normal_liver_date":"11/11/1999","hepatitis_c_ever_treated":"1","hepatitis_c_currently_treated":"1","hepatitis_c_treatment_start_date":"11/11/1999","hepatitis_c_treatment_end_date":"11/11/1999","hepatitis_c_liver_cirrhosis":"1","hepatitis_c_liver_biopsy":"1","hepatitis_c_complications":"1","hazardous_avocation":true,"hazardous_avocation_further_detail":"fooooooooooooooooo","criminal":true,"criminal_further_detail":"wertyu","foreign_travel":true,"travel_country_id":1,"travel_when":"11/11/1999","travel_duration":"8 yrs"},"financial_info_attributes":{"bankruptcy":true,"bankruptcy_declared":"11/11/1999","bankruptcy_discharged":"11/11/1999"},"full_name":"A Rivera","gender":"1","birth_or_trust_date":"11/12/1987","marital_status_id":3,"recently_applied_for_life_ins":false,"citizenship_id":"3","source":"IO Quote Path, started via Manual Entry By User","lead_type":"IO Quote Path Full Application"},"request_type":"APPLICATION","agent_id":agent.id}
          expect {
            post :save_from_quote_path, format: :json, **params
            expect(response.status).to eq(200), [response.status, response.body]
          }.to change(Consumer, :count).by(1)
          .and change(Crm::HealthInfo, :count).by(1)
        end
      end
    end

    context 'given an existing consumer and case' do
      let(:existing_consumer) do
        create :consumer_w_assoc, params['consumer']
        .merge(cases_attributes: [build(:crm_case_w_beneficiary_and_owner).attributes])
        .merge(agent_id: agent.id, brand_id: brand.id)
      end
      let(:case_record) { existing_consumer.cases.first }
      let(:case_params) { params['consumer']['cases_attributes'][0] }
      let(:quote_params) { case_params['quoting_details_attributes'][0] }

      #When user entered the quote path via the "requote" button.
      before do
        #add existing record ids to params
        params['consumer']['id'] = existing_consumer.id
        case_params['id'] = case_record.id
        quote_params['id'] = case_record.quoting_details.first.id
      end

      context 'when params contain modified quote details' do
        #When user has changed quote details and clicked "save".
        before do
          case_params['quoting_details_attributes'][0]['face_amount'] = 444_444
        end

        it 'modifies the quote record as expected' do
          quote = case_record.quoting_details.first
          expect {
            post :save_from_quote_path, params
          }.to change { case_record.reload.face_amount }.from(nil).to(444_444)
          .and change { quote.reload.face_amount }.from(nil).to(444_444)
          .and change(Crm::Case, :count).by(0)
          expect( response.code ).to eql("200"), response.body
        end

        it 'does NOT create a new status' do
          expect {
            post :save_from_quote_path, params
          }.to_not change { case_record.reload.status.id }
          expect( response.code ).to eql("200"), response.body
        end
      end

      context 'when params contain modified quote details and status type id which would change the sales stage' do
        before do
          quote_params['face_amount'] = 444_444
          case_params['status_type_id'] = Crm::StatusType::PRESETS[:submit_to_app_team][:id]
          @opportunity_ss_id=Enum::SalesStage.id('Opportunity')
          @app_fulfillment_ss_id=Enum::SalesStage.id('App Fulfillment')
        end

        it 'modifies the quote record as expected' do
          quote = case_record.quoting_details.first
          expect {
            post :save_from_quote_path, params
          }.to change { case_record.reload.face_amount }.from(nil).to(444_444)
          .and change { quote.reload.face_amount }.from(nil).to(444_444)
          expect( response.code ).to eql("200"), response.body
        end

        it 'creates the intended status' do
          expect {
            post :save_from_quote_path, params
          }.to change { case_record.reload.status.id }
          .and change { case_record.reload.sales_stage_id }.from(@opportunity_ss_id).to(@app_fulfillment_ss_id)
          expect( response.code ).to eql("200"), response.body
        end

        it 'duplicates the modified quote record for the new sales stage' do
          expect {
            post :save_from_quote_path, params
            expect(response.status).to eq(200), response.body
          }.to change { case_record.reload.quoting_details.count }.by(1)
          expect(case_record.current_details.face_amount).to eq(444_444)
          expect(case_record.current_details.sales_stage_id).to eq( @app_fulfillment_ss_id )
        end
      end
    end
  end

  describe '#email' do
    it 'provides a valid access code' do
      authentication_stubs
      cons = create :consumer, agent: authenticated_user
      build(:marketing_email_smtp_server, owner: authenticated_user).save validate: false
      params = {
        brand_id: 13,
        user_id: authenticated_user.id,
        consumer_id: cons.id,
        to_address: 'marty@mc.fly',
        quotes: [],
        request_type: 'APPLICATION',
        product_line: '',
        premium_mode_id: 1,
        health_class_id: 1,
        face_amount: 777_000,
        duration_id: 23
      }
      get :email, params
      uri = URI.parse assigns :quote_url
      access_code = URI::decode_www_form(uri.query).to_h['ltd_access_code']
      user, contact = User.from_access_code access_code
      expect(user).to be_a User
      expect(user.id).to eq authenticated_user.id
      expect(contact).to be_a Consumer
      expect(contact.id).to eq cons.id
    end
  end

  describe '#get_quotes, with a logged-in user,', :vcr do
    before :each do
      authentication_stubs
      brand
    end
    let(:user  ){ build :user}
    let(:params_file){ "#{Rails.root}/spec/support/quote_form.json" }
    context 'for new record' do
      before do
        # stub agent/brand for validation
        allow_any_instance_of(Consumer).to receive(:agent).and_return(user)
        allow_any_instance_of(Consumer).to receive(:brand).and_return(user.default_brand)
        params['consumer']['agent_id']=user.id
        params['consumer']['brand_id']=user.default_brand_id
        params[:format] = 'js'
      end

      it 'should return 200' do
        post :get_quotes, params
        produces_valid_consumer_and_case
        expect( response.code ).to eql "200"
        expect( JSON.parse(response.body)["results"] ).to be_a Array
      end
      context 'with non-compulife-able health issues' do
        let(:params_file){ "#{Rails.root}/spec/support/quote_form_health_problems.json" }
        it 'should return 200' do
          pending('XRAE service integration needs fixing/replacement')
          post :get_quotes, params
          produces_valid_consumer_and_case
          expect( response.code ).to eql "200"
          expect( JSON.parse(response.body)["results"] ).to be_a Array
        end
      end
    end
  end

  describe '#get_quotes, when called from a quote widget', :vcr do
    let(:user       ){ build_stubbed :user}
    let(:widget     ){ Quoting::Widget.new name:'X', user_id:user_id, brand_id:brand_id, hostname:'http://www.example.com' }
    let(:user_id    ){ user.id }
    let(:widget_id  ){ Quoting::Widget.maximum(:id).to_i.next}
    let(:brand      ){ build_stubbed :brand}
    let(:brand_id   ){ brand.id }
    let(:staff      ){ build_stubbed :usage_staff_assignment_w_assoc }
    let(:params     ){
      {
        "consumer"=>{
          "product_cat_id"=>"1",
          "emails_attributes"=>[{"value"=>"foo@bar.com"}],
          "addresses_attributes"=>[{"state_id"=>"1"}],
          "phones_attributes"=>[{"value"=>"999-999-9999"}],
          "opportunities_attributes"=>[{"face_amount"=>"2750000", "health"=>"PP","duration_id":"1"}],
          "notes_attributes"=>[{text:"This consumer record was submitted from stage 1 of widget id 4 from IP address 10.71.16.84."}],
          "lead_type"=>"",
          "source"=>"",
          "referrer"=>"",
          "tags_attributes"=>[
            "quoter url - http://localhost/quoting/widget/widgets/#{widget_id}",
            "agent split - Jack gets nil. Jill gets 100%.",
            "foo - bar",
            "abc 123 - the basics",
            "lalala",
          ],
          "full_name"=>"Beebop Pop",
          "gender"=>"m", "weight"=>"120",  "feet"=>"6", "inches"=>"0",
          "birth_or_trust_date"=>"01/01/1965",
          "brand_id"=>brand_id,
          "agent_id"=>user_id,
        },
        "brand_id"=>brand_id,
        "agent_id"=>user_id,
        "key"=>user.quoter_key,
        "widget_id"=>widget_id
      }
    }

    before do
      #These allow us to persist consumer for real without saving auxilliary records:
      allow_any_instance_of(Consumer).to receive(:staff_assignment).and_return(staff)
      allow_any_instance_of(Consumer).to receive(:agent).and_return(user)
      allow_any_instance_of(Consumer).to receive(:brand).and_return(brand)
      #This takes care of the quoting credential check in filter method `require_login_or_id_and_quoter_key`.
      User.stub_chain(:unscoped,:able_to_log_in,:find_by).and_return(user)
    end

    it 'returns 200' do
      post :get_quotes, params
      expect( response.code ).to eql "200"
    end

    context 'called with a NEW consumer' do
      it 'creates a new consumer and opportunity' do
        expect {
          params["consumer"]["full_name"] = Forgery::Name.full_name
          post :get_quotes, params
        }.to change(Consumer, :count).by(1)
        .and change { Quoting::Quote.where(sales_stage_id: 1).count }.by(1)
        expect( assigns(:consumer).persisted? ).to be true
      end
    end

    context 'called subsequent times with different quoted details, without leaving the page' do
      it 'does NOT create any new consumers, but adds a new opportunity' do
        params["consumer"]["full_name"] = Forgery::Name.full_name
        post :get_quotes, params # first call
        expect {
          params["consumer"]["id"] = Consumer.last.id.to_s
          params["consumer"]["opportunities_attributes"][0]["id"] = Consumer.last.opportunities.last.id.to_s
          params["consumer"]["opportunities_attributes"][0]["face_amount"] = "200000"
          post :get_quotes, params # subsequent call
        }.to change(Consumer, :count).by(0)
        .and change { Quoting::Quote.where(sales_stage_id: 1).count }.by(0)
        Quoting::Quote.where(sales_stage_id:1).last.face_amount.should == 200000
      end
    end
  end

end
