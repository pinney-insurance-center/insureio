require 'spec_helper'
require 'support/authentication_helper'
require 'authlogic/test_case'

include Authlogic::TestCase

describe RttController do
  setup :activate_authlogic
  render_views
  let(:user) { authenticated_user }
  let(:kase) { create :crm_case, consumer: consumer, scor_guid: 'scor-foobar' }
  let(:consumer) { create :consumer }

  before do
    authentication_stubs
    user.can! :motorists_rtt_process_details
    user.save!
  end

  describe '#agent_summary' do
    it 'authenticates with :quoter_key authentication' do
      authentication_unstub
      get :agent_summary, format: :json, agent_id: user.id
      expect(response.status).to eq(401)
      get :agent_summary, format: :json, agent_id: user.id, key: user.quoter_key
      expect(response.status).to eq(200)
    end
  end

  describe '#force_send_for_underwriting' do
    it 'does not authenticate with :quoter_key authentication' do
      allow(controller).to receive(:session).and_return({})
      allow(UserSession).to receive(:find).and_return nil
      authentication_unstub
      get :force_send_for_underwriting, format: :json, id: kase.scor_guid, agent_id: user.id, key: user.quoter_key
      expect(response.status).to eq(401)
      mock_submit_service = double(send_to_scor: nil, errors: [])
      expect(Processing::Rtt::SubmitService).to receive(:new).and_return(mock_submit_service)
      authentication_stubs user
      get :force_send_for_underwriting, format: :json, id: kase.scor_guid, agent_id: user.id, key: user.quoter_key
      expect(response.status).to eq(200)
    end
  end

  describe '#save' do
    context 'with new consumer and policy' do
      it 'creates consumer and policy' do 
        pending('Spec needs implementation.')
      end
    end
    context 'with existing consumer and policy' do
      it 'updates consumer and policy' do 
        pending('Spec needs implementation.')
      end
      context 'when consumer has accepted the returned rate from underwriting' do
        it 'attempts to transfer the xml file to the carrier' do 
          pending('Spec needs implementation.')
        end
      end
    end
  end

  describe '#save_and_sign_for_consumer' do
    it 'saves consumer signature' do
      pending('Spec needs implementation.')
    end
    context 'when separate owner and payer are present' do
      it 'sends emails to owner and payer' do
        pending('Spec needs implementation.')
        #expect( Processing::Rtt ).to receive( :deliver_stakeholder_emails )
      end
      it 'does not send to SCOR for underwriting' do
        pending('Spec needs implementation.')
        #expect_any_instance_of( Processing::Rtt::SubmitService ).to not_receive( :send_to_scor)
      end
    end
    context 'when insured is the owner and payer' do
      it 'does not send emails' do
        pending('Spec needs implementation.')
        #expect( Processing::Rtt ).to not_receive( :deliver_stakeholder_emails )
      end
      it 'sends to SCOR for underwriting' do
        pending('Spec needs implementation.')
        #expect_any_instance_of( Processing::Rtt::SubmitService ).to receive( :send_to_scor)
      end
      context 'and sending to SCOR fails' do
        it 'attempts to transfer the xml file to the carrier' do
          pending('Spec needs implementation.')
        end
      end
      context 'and sending to SCOR succeeds' do
        it 'does not attempt to transfer the xml file to the carrier' do
          pending('Spec needs implementation.')
        end
      end
    end
  end

  describe '#force_send_for_underwriting' do
    pending('Spec needs implementation.')
  end

  describe '#force_transfer_xml' do
    pending('Spec needs implementation.')
  end

  describe '#deliver_resume_app_email' do
    pending('Spec needs implementation.')
  end

  describe '#poll_for_underwriting_decision' do
    pending('Spec needs implementation.')
  end

  describe '#recent_apps' do
    pending('Spec needs implementation.')
  end

end
