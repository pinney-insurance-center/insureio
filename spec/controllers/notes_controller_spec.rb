require 'spec_helper'

VCR.configure do |c|
  c.hook_into :excon
end

describe NotesController do

  before do
    authentication_stubs
  end

  describe '#index' do
    context 'when @notable has AgencyWorks id', :vcr do
      render_views

      let(:agency_works_id){78943789}
      let(:kase){build :crm_case, agency_works_id:agency_works_id}

      before do
        expect(controller).to receive(:require_notable){ controller.instance_variable_set '@notable', kase }
      end

      context 'when AgencyWorks returns 0 comments' do
        it 'returns empty array with a status code of 200' do
          get :index, format: :json
          expect(response.status).to be 200
          expect(response.body).to match /^\[\]$/
        end
      end

      context 'when AgencyWorks returns 6 comments' do
        let(:soap_response){ JSON.parse File.read "#{Rails.root}/spec/support/agency_works/comments_response_hash2.json" }

        it 'returns 200' do
          expect_any_instance_of(Processing::AgencyWorks::CommentsService).to receive(:send_soap_request).and_return(soap_response)
          get :index, format: :json
          expect(response.status).to be 200
          expect(response.body).to match /\(Acknowledgement Memo\) Entered on 09\/30\/2014 at 11:30 PM by EPPINNEY/
        end
      end
    end
  end

end
