
require 'spec_helper'
 
describe Reporting::ProductionSummariesController do
  render_views

  before do
    authentication_stubs
    current_user.can!(:super_view, true)
  end
end
