require 'spec_helper'

describe Reporting::SearchesController do
  render_views

  before do
    authentication_stubs
    current_user.can!(:reporting_export, true)
    controller.stub(:_require_membership).and_return(true)
    controller.stub(:_require_custom).and_return(true)
  end

  describe '#results' do
    let(:params){{ #"authenticity_token"=>"sSbpZq9cd4A4V1FrAQ9MQTzB7WxSGPg6uVVaht650YA=",
      "reporting_search"=>{"name"=>"test", "client_visible"=>"1", "case_visible"=>"1", "status_visible"=>"1", "fields_visible"=>"1",
        "criteria"=>{
          "consumer"=>{"age_min"=>"60", "age_max"=>"70", "source_id"=>"", "agent_id"=>"1", "lead_type_id"=>"", "brand_id"=>"35", "full_name"=>"ab foo lo", "city"=>"raccoon", "state_id"=>"5", "zip"=>"98706"},
          "phone"=>{"value"=>"7897897890"},
          "email"=>{"value"=>"foo@bar.com"},
          "sales_support_id"=>"5",
          "quoted_details"=>{"face_amount_min"=>"900", "face_amount_max"=>"9000000", "annual_premium_min"=>"800", "annual_premium_max"=>"9999", "carrier_id"=>"17", "product_type_id"=>"1"},
          "status"=>{"active"=>"1", "created_at_min"=>"11/11/1978", "created_at_max"=>"11/11/2000"}
        }, 
        "fields_to_show"=>{
          "owner_contact"=>{"name"=>"1"},
          "owner"=>{"gender_string"=>"1", "birth"=>"1", "primary_phone"=>"1"},
          "consumer"=>{"gender_string"=>"1", "birth"=>"1", "name"=>"1", "primary_phone"=>"1"},
          "case"=> {"id"=>"1", "product_type_name"=>"1", "face_amount"=>"1", "entered_stage_app_fulfillment"=>"1", "entered_stage_submitted_to_carrier"=>"1"},
          "status"=>{"name"=>"1"}
        }
      }, "commit"=>"Export Results"}}

    context 'as json, with valid params' do
      it 'returns 200' do
        get :results, params.merge(format:'json')
        expect(response.status).to eq(200), "Response status was #{response.status}. Response body was\n#{response.body}"
      end
    end
    context 'as json, with invalid params' do
      it 'returns 200' do
        new_params=params.merge(format:'json')
        new_params['reporting_search']['fields_to_show']['status']['some_bogus_field_that_doesnt_exist']='1'
        get :results, new_params
        expect(response.status).to eq(400), "Response status was #{response.status}. Response body was\n#{response.body}"
      end
    end
  end


end