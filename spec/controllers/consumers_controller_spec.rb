require 'spec_helper'

describe ConsumersController do
  render_views

  let(:complete_api_data) {
    data = JSON.parse(File.read "#{Rails.root}/spec/support/lead_with_case.json").with_indifferent_access
    data[:consumer][:agent_id] = agent.id # this is the user for whom leads are created, according to the POST data
    data[:key] = agent.api_key
    data[:consumer][:brand_id] = brand.id # this is the brand for which the leads are created, according to the POST data
    data[:format] = :json
    data
  }
  let(:initializtion_vector) { '123456789012345678901234567890' }
  let(:expected_encrypted_dln) { Base64.encode64 Encryptor.encrypt(key:APP_CONFIG['encryption_key'], value:'test this', iv: initializtion_vector) }
  let(:expected_encrypted_ssn) { Base64.encode64 Encryptor.encrypt(key:APP_CONFIG['encryption_key'], value:'123121234', iv: initializtion_vector) }
  let(:agent) { create :agent }
  let(:brand) { agent.default_brand }

  describe '#create' do
    before do
      authentication_stubs
    end

    context 'with complete API data' do
      it 'does not raise error' do
        expect{ post :create, complete_api_data }.to_not raise_error
      end

      it 'does not redirect' do
        post :create, complete_api_data
        expect(response).to_not be_redirect, "Redirect: #{response.body}"
      end

      it 'creates a consumer' do
        expect{ post :create, complete_api_data }.to change(Consumer, :count).by(1)
      end

      it 'creates a case' do
        expect{ post :create, complete_api_data }.to change(Crm::Case, :count).by(1)
      end

      it 'creates phones' do
        expect{ post :create, complete_api_data }.to change(Phone, :count).by(3)
        expect(assigns(:consumer).phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work') }.first.value).to eq "2222222222"
        expect(assigns(:consumer).phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home') }.first.value).to eq "2111111111"
        expect(assigns(:consumer).phones.select{|p| p.phone_type_id==Enum::PhoneType.id('fax')  }.first.value).to eq "2341234453"
      end

      it 'creates an address' do
        expect{
          post :create, complete_api_data
          expect(response.status).to eq(200), [response.status, response.body].to_yaml
        }.to change(Address, :count).by(1)
        expect(assigns(:consumer).addresses.first.value).to eq "street address\nCity, RI 12344"
      end

      it 'creates an email' do
        agent # Invoke agent to create record so that its email addresses will not contribute to the following count.
        expect{ post :create, complete_api_data }.to change(EmailAddress, :count).by(1)
        expect(assigns(:consumer).emails.first.value).to eq "test@bar.com"
      end

      it 'creates health_info' do
        expect{ post :create, complete_api_data }.to change(Crm::HealthInfo, :count).by(1)
        # (most) attributes are populated as expected
        attrs = assigns(:consumer).health_info.attributes
        .reject{|k,v| %w[id updated_at created_at consumer_id].include?(k) or v.nil? }
        expect(attrs).to include "feet"=>7,"inches"=>8, "tobacco"=>true, "weight"=>1
      end

      it 'creates financial_info' do
        expect{ post :create, complete_api_data }.to change(Crm::FinancialInfo, :count).by(1)
        fi_attrs=complete_api_data["consumer"]["financial_info_attributes"]
        expect(assigns(:consumer).financial_info.asset_earned_income).to eq fi_attrs["asset_earned_income"].to_i
        expect(assigns(:consumer).financial_info.net_worth_specified).to eq fi_attrs["net_worth_specified"].to_i
      end

      it 'creates tags' do
        post :create, complete_api_data
        expect(assigns(:consumer).lead_type).to match /CLU/i
        tag_strings = assigns(:consumer).tags.map(&:to_s)
        expect(tag_strings).to include "a unique tag fo testin teh trackinz"
      end

      context 'when brand but not agent specified' do
        it 'assigns case and consumer to an agent' do
          agent_id = 67
          complete_api_data.delete 'user_id'
          expect(complete_api_data['consumer']).to have_key 'brand_id'
          complete_api_data['consumer'].delete 'agent_id'
          expect(complete_api_data['consumer']).to_not have_key 'agent_id'
          lead_dist_cursor = LeadDistribution::Cursor.new brand_id:45, lead_type_id:10
          expect(lead_dist_cursor).to receive(:brand).at_least(:once).and_return(Brand.new)
          expect(lead_dist_cursor).to receive(:lead_type).at_least(:once).and_return(Crm::LeadType.new)
          expect(lead_dist_cursor).to receive(:agents).at_least(:once).and_return(build_list :agent, 1, id:agent_id, can_strict_lead_distribution:true)
          allow_any_instance_of(LeadDistribution).to receive(:cursor).and_return(lead_dist_cursor)
          post :create, complete_api_data.merge(format: 'json', response: 'medium')
          json = JSON.parse response.body
          expect(json['agent_id']).to eq agent_id
          expect(json['cases'][0]['agent_id']).to eq agent_id
        end
      end

      context 'with repeated calls' do
        it 'does not create duplicate email addresses' do
          post :create, complete_api_data
          json = JSON.parse response.body
          id = json['id']
          kase_id = json['cases'].last['id']
          emails_n  = json['emails'].length
          post :create, {format:'json'}.merge(complete_api_data)
          json = JSON.parse response.body
          expect(json['id']).to eq id
          expect(json['cases'].last['id']).to_not eq kase_id
          expect(json['emails'].length).to eq emails_n
        end
      end

      context 'with ezl_id' do
        it 'sets ezl_id' do
          complete_api_data['consumer']['cases_attributes'][0]['ezl_id'] = 3345
          post :create, complete_api_data
          json = JSON.parse response.body
          expect(json['cases'].last['ezl_id']).to be 3345
        end
      end
    end

    context 'with invalid JSON' do
      VCR.configure do |c|
        c.allow_http_connections_when_no_cassette = true
        c.hook_into :webmock
      end

      it 'produces a helpful error message' do
        broken_json= %Q({"consumer":{}) #is missing closing curly brace.

        curb = post_broken_json_to_api('/consumers.json', broken_json)

        expect(curb.response_code).to eq 400
        expect(curb.content_type).to match(/application\/json/)
        expect(curb.body_str).to match(/There was a problem parsing the JSON you submitted/)
      end

      def post_broken_json_to_api(path, broken_json)
        Curl.post("http://#{host}:#{port}#{path}", broken_json) do |curl|
          set_default_headers(curl)
        end
      end

      def host
        Capybara.current_session.server.host
      end

      def port
        Capybara.current_session.server.port
      end

      def set_default_headers(curl)
        curl.headers['Accept'] = 'application/json'
        curl.headers['Content-Type'] = 'application/json'
      end
    end

    context 'with identifying info that matches an existing consumer' do
      before do
        @params={agent_id: agent.id, key: agent.api_key,
          consumer:{
            agent_id: agent.id,
            brand_id: brand.id,
            full_name: Forgery::Name.full_name,
            birth_or_trust_date: '01/01/1980' 
          }
        }
        Thread.current[:tenant_id] = agent.tenant_id
        Consumer.create! @params[:consumer]
      end
      context 'and valid attributes for a new case' do
        before do
          @params[:consumer][:cases_attributes]=[{
            quoting_details_attributes:[{face_amount:500_000, product_name:'fhsfhf'}]
          }]
        end
        it 'succeeds and creates the case' do
          expect{
            post :create, @params
            expect( response.status ).to eq(200)
          }.to change(Consumer, :count).by(0)
          .and change(Crm::Case, :count).by(1)
        end
      end
      context 'and invalid attributes for a new case' do
        before do
          @params[:consumer][:cases_attributes]=[{
            quoting_details_attributes:[{face_name:500_000, product_amount:'fhsfhf'}]
          }]
        end
        it 'fails and returns the errors for the case' do
          expect{
            post :create, @params
          }.to change(Consumer, :count).by(0)
          .and change(Crm::Case, :count).by(0)

          expect( response.status ).to eq(422)
          expect( response.body ).to match(/errors/)
        end
      end
    end

    context 'with ESP worker' do
      before do
        # NMB brand (in db seeds) determines whether a case gets pushed to ESP
        complete_api_data[:consumer][:brand_id] = Brand::NMB_ID
        complete_api_data[:consumer][:cases_attributes] = [{
          sales_stage_id: 2,
          quoting_details_attributes: [{
            duration_id: 3,
            face_amount: 500_000,
          }]
        }].map &:with_indifferent_access
        complete_api_data[:consumer][:agent_id] = create(:user, tenant_id: 10).id
        @request.host = 'nmb.example.com'
      end

      # NMB-branded leads get sent to ESP and require extra validation
      it '422s when SSN is missing' do
        expect(EspPushWorker).to_not receive :perform_async
        complete_api_data[:consumer].delete :ssn
        expect {
          post :create, complete_api_data
          expect(response.status).to eq 422
          expect(response.body).to match /ssn cannot be blank/i
        }.to change { Consumer.unscoped.count }.by(0)
        .and change { Crm::Case.unscoped.count }.by(0)
      end
    end
  end

  describe '#batch_import' do
    before do
      authentication_stubs
    end
    it 'succeeds for GET' do
      expect{ get :csv }.to_not raise_error
      expect(response.status).to eq 200
    end
    it 'succeeds for POST' do
      # Create users to receive the leads
      User.find_by(id: 99) || create(:user, id: 99)
      User.find_by(id: 2) || create(:user, id: 2)
      csv_string = fixture_file_upload('spec/support/crm/batch_import_demo.csv', 'text/csv')
      batch_one = CSV.parse(csv_string, headers: :first_row).map(&:to_h)
      expect{ post :batch_import, rows: batch_one, offset: 0 }.to_not raise_error
      expect(response.status).to eq 200
    end
  end

  describe '#related' do
    it '401s when not logged in' do
      authentication_unstub
      get :related, id: 1, format: :json
      expect(response.status).to eq 401
    end
  end

  describe '#show' do
    subject { create :consumer, agent: agent }

    before { authentication_unstub }

    it '200s when authenticated with sess' do
      authentication_stubs agent
      get :show, id: subject.id, format: :json
      expect(response.status).to eq 200
    end

    it '200s when authenticated with ltd_access_code' do
      get :show, id: subject.id, format: :json, ltd_access_code: agent.access_code(subject)
      expect(response.status).to eq 200
    end

    it '401s when not logged in' do
      get :show, id: subject.id, format: :json
      expect(response.status).to eq 401
    end

    it '401s when quoter key' do
      get :show, id: subject.id, format: :json, agent_id: agent.id, key: agent.quoter_key
      expect(response.status).to eq 401
    end

    it '403s when authenticated with mis-matched ltd_access_code' do
      other_consumer = create :consumer, agent: agent
      get :show, id: subject.id, format: :json, ltd_access_code: agent.access_code(other_consumer)
      expect(response.status).to eq 403
    end
  end

  describe '#suggested_relations' do
    it '401s when not logged in' do
      authentication_unstub
      get :suggested_relations, id: 1, format: :json
      expect(response.status).to eq 401
    end
  end

  describe '#update' do
    subject { create :consumer, agent: agent }
    let(:message) { create :marketing_email_message, target: subject }
    let(:params) { { id: subject.id, consumer: {full_name: "derry foir"}, format: :json } }

    before { authentication_unstub }

    it '200s when authenticated with sess' do
      authentication_stubs agent
      put :update, params
      expect(response.status).to eq 200
    end

    it '200s when authenticated with ltd_access_code' do
      put :update, params.merge(ltd_access_code: agent.access_code(subject))
      expect(response.status).to eq 200
    end

    it '401s when not logged in' do
      put :update, params
      expect(response.status).to eq 401
    end

    it '401s when quoter key' do
      put :show, params.merge(agent_id: agent.id, key: agent.quoter_key)
      expect(response.status).to eq 401
    end

    it '403s when authenticated with mis-matched ltd_access_code' do
      other_consumer = create :consumer, agent: agent
      put :show, params.merge(ltd_access_code: agent.access_code(other_consumer))
      expect(response.status).to eq 403
    end
  end

  context 'when logged in' do
    let(:consumer) { create :consumer, agent: agent }

    before do
      authentication_stubs agent
      expect(controller).to receive(:require_consumer) { controller.instance_variable_set '@consumer', consumer }
    end

    describe '#update' do
      context 'when consumer lacks a financial_info and nested attributes are included,' do
        let(:params){ {id:consumer.id, consumer:{financial_info_attributes:{annual_income:'90000',net_worth_specified:'2000000'}}} }

        it 'does not raise error' do
          expect{ put :update, params }.to_not raise_error
        end

        it 'creates financial_info' do
          @fi_attrs=params[:consumer][:financial_info_attributes]
          expect{ put :update, params }.to change(Crm::FinancialInfo, :count).by(1)
          expect(assigns(:consumer).income).to eq @fi_attrs[:annual_income].to_i
          expect(assigns(:consumer).financial_info.net_worth_specified).to eq @fi_attrs[:net_worth_specified].to_i
        end
      end
  
      context 'when consumer already has financial_info and delegated attributes are included,' do
        let(:params){ {id:consumer.id, consumer:{income:'90000'}} }
        it 'does not raise error' do
          expect{ put :update, params }.to_not raise_error
        end
        it 'updates existing financial_info' do
          consumer.create_financial_info
          expect{ put :update, params }.not_to change(Crm::FinancialInfo, :count)
          expect(assigns(:consumer).income).to eq params[:consumer][:income].to_i
        end
      end

      context 'when giving info for existing case' do
        it 'updates case instead of creating a new one' do
          kase = create :crm_case, consumer: consumer, exam_company: "oldco"
          params = { format: :json, id: consumer.id, consumer: { cases_attributes: [ { id: kase.id, exam_company: "newco" } ] } }
          expect do
            put :update, params
            expect(response.status).to eq(200), response.body
          end
          .to change(Crm::Case, :count).by(0)
          .and change { kase.reload.exam_company }.from("oldco").to("newco")
        end
      end

      context 'when giving info for a new case' do
        it 'creates a new case' do
          kase = build :crm_case, consumer: consumer, exam_company: "exco"
          params = { format: :json, id: consumer.id, consumer: { cases_attributes: [ kase.attributes ] } }
          expect do
            put :update, params
            expect(response.status).to eq(200), response.body
          end
          .to change(Crm::Case, :count).by(1)
        end
      end
    end

    describe '#tag' do
      let(:consumer) { create :consumer }

      context 'with params[:tag]' do
        it 'updates Consumer record' do
          expect(consumer.tags).to match_array []
          post :tag, tag:'foo=bar', id: consumer.id, format: :json
          expect(response.status).to eq 200
          expect(consumer.tags).to match_array ['foo=bar']
        end
      end

      context 'with params[:tags]' do
        it 'updates Consumer record' do
          expect(consumer.tags).to match_array []
          post :tag, tags: %w[tag1 tag2], id: consumer.id, format: :json
          expect(response.status).to eq 200
          expect(consumer.tags).to match_array %w[tag1 tag2]
        end
      end

      context 'with params[:delete_tag]' do
        it 'updates Consumer record' do
          consumer.update tags: %w[foo bar baz]
          expect(consumer.tags).to match_array %w[foo bar baz]
          post :tag, delete_tag: 'bar', id: consumer.id, format: :json
          expect(response.status).to eq 200
          expect(consumer.tags).to match_array %w[foo baz]
        end
      end
    end
  end
end