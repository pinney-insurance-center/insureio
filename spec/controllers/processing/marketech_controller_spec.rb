require 'spec_helper'
require Rails.root+'app/models/processing/marketech'

describe ProcessingController do
  let(:current_user){ authenticated_user }
  let(:kase){ create :crm_case_w_assoc, id:65, consumer:client }
  let(:client){ build :consumer_w_assoc_b }
  let(:marketech_dataset){ build :processing_marketech_dataset, id:23, kase:kase }
  let(:marketech_carrier){ Processing::Marketech::Carrier.find(2) }
  
  describe '#submit_to_marketech' do

    context 'without logged-in user' do
      it 'should render a 401 unauthorized response' do
        post :submit_to_marketech, id:1
        response.status.should == 401
      end
    end

    context 'with logged-in user' do
      before do
        authentication_stubs
        current_user.stub(:can?).and_return(true)
        Crm::Case.should_receive(:find_by_id).with("65").and_return(kase)
      end
      context 'when user lacks edit permissions' do
        it 'should render a 401 unauthorized response' do
          Crm::Case.any_instance.should_receive(:editable?).with(current_user).and_return(false)
          post :submit_to_marketech, id:kase.id
          response.status.should == 401
        end
      end
      context 'when user has edit permissions' do
        before do
          kase.stub(:editable?).with(current_user).and_return(true)
          Crm::Case.any_instance.stub(:marketech_carrier).and_return(marketech_carrier)
        end
        context 'when kase is valid_marketech', :vcr do
          before do
            Crm::Case.any_instance.stub(:eligible_for_processing_with_marketech).and_return(true)
          end
          it 'should create a crm note' do
            expect{ post :submit_to_marketech, id:kase.id, format:'json' }.to change(Crm::Note, :count).by(1)
          end
          it 'should update marketech status id' do
            marketech_dataset.status_id.should_not == Processing::Marketech::Dataset::APP_UPLOADED
            expect{ post :submit_to_marketech, id:kase.id, format:'json' }.to_not raise_error
            Processing::Marketech::Dataset.find_by_case_id(kase.id).status_id.should == Processing::Marketech::Dataset::APP_UPLOADED
          end
        end
        context 'when kase is not valid_marketech' do
          before do
            Crm::Case.any_instance.stub(:eligible_for_processing_with_marketech).and_return(false)
          end
          it 'should render errors' do
            post :submit_to_marketech, id:kase.id, format: :json
            response.body.should match /errors/
          end
        end
      end

    end

  end
end

describe Processing::MarketechController do
  let(:current_user){ authenticated_user }
  let(:kase){ create :crm_case_w_assoc, id:65, consumer:client }
  let(:client){ build :consumer_w_assoc_b }
  let(:marketech_dataset){ build :processing_marketech_dataset, id:23, kase:kase }
  let(:marketech_carrier){ Processing::Marketech::Carrier.find(2) }
  
  describe '#update' do
    let(:request){ get :update, caseID:'foo', code:'bar', event:5, party:'qux' }

    context 'when a Dataset belonging to the given case exists in db' do
      before do
        Processing::Marketech::Update.any_instance.stub(:dataset).and_return(marketech_dataset)
      end
      it 'should create a record' do
        expect{request}.to change(Processing::Marketech::Update, :count).by(1)
      end
      it 'should render record' do
        request
        response.body.should include %q("case_id":"foo")
        response.body.should include %q("code":"bar")
        response.body.should include %q("event":5)
        response.body.should include %q("party":"qux")
        response.body.should match /"id":\d+/
      end
    end

    context 'when a Dataset belonging to the given case does not exist in db' do
      it 'should not create a record' do
        expect{request}.to change(Processing::Marketech::Update, :count).by(0)
      end
      it 'should write to the log' do
        AuditLogger['marketech_update'].should_receive(:info).exactly(2).times
        request
      end
    end

  end

end