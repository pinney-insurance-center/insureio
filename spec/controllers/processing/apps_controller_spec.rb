require 'spec_helper'
require 'support/apps/shared_context.rb'

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::AppsController do

  describe 'status', type: :request do
    it 'completes without error' do
      xml = File::read "#{Rails.root}/spec/support/apps/535statusex.xml"
      expect{ post '/processing/apps/status', xml, 'CONTENT_TYPE' => 'application/xml', 'ACCEPT' => 'application/xml' }.to_not raise_error
    end
  end
end
describe ProcessingController do

  describe 'submit_to_apps', type: :controller do
    include_context 'shared APPS context'
    render_views

    before do
      authentication_stubs
      current_user.stub(:can?).and_return(true)
      Processing::Apps::CaseDatum.any_instance.stub(:crm_case).and_return(kase)#kase is defined in shared context
      Crm::Case.stub(:find_by_id).and_return(kase)
    end

    it 'completes without error', :vcr do
      expect{ post :submit_to_apps, id:kase.id, format: :js }.to_not raise_error
    end

    context 'is eligible for submission to Apps' do
      before do
        kase.a_team_details.face_amount=1000*1000
        kase.consumer.birth=61.years.ago
      end
      it 'delivers success message' do
        Processing::Apps::UploadService.any_instance.stub(:run).and_return(true)
        post :submit_to_apps, id:kase.id, format: :js
        response.body.should match /Uploaded to APPS/
      end
    end

    it 'alerts Case errors' do
      kase.a_team_details.carrier = nil
      kase.client.ssn = nil
      Processing::Apps::UploadService.should_not receive :new
      post :submit_to_apps, id:kase.id, format: :js
      response.body.should match /SSN must be set on client/
      response.body.should match /Carrier must be American General/
    end
  end

end