require 'spec_helper'
require 'net/ftp'

describe IxnController do
  let(:params) { JSON.parse(json).with_indifferent_access.merge(format: :json) }
  let(:tenant_id) { 10 }
  let(:agent) { create :agent, tenant_id: tenant_id, default_brand_id: Brand::NMB_ID }

  describe '#create' do
    before do
      @request.host = 'nmb.insureio.test'
      authentication_stubs agent
    end

    let(:json) { File.read File.join(Rails.root, 'spec/support/ixn/create.json') }

    it 'saves a consumer on a successful post' do
      expect {
        post :create, params
        expect(response.status).to eq 201
      }.to change { Consumer.unscoped.count }.by(3) # 2 are beneficiaries
      .and change(Crm::Case, :count).by(1)
      .and change(Crm::HealthInfo, :count).by(1)
      .and change(Crm::FinancialInfo, :count).by(1)
      .and change(Quoting::Quote, :count).by(1)
    end

    it 'updates a case if id is provided' do
      cons = create :consumer, brand_id: Brand::NMB_ID, tenant_id: tenant_id, agent: agent
      cons.create_health_info
      cons.create_financial_info
      kase = create :crm_case, consumer: cons, agent: agent
      params[:insureio_consumer_id] = cons.id
      params[:insureio_case_id] = kase.id
      params[:first_name] = 'Jane6'
      params[:middle_initial] = ''
      params[:last_name] = 'Lane7'
      expect {
        post :create, params
        expect(response.status).to eq 201
      }.to change { Consumer.unscoped.count }.by(2) # 2 beneficiaries
      .and change(Crm::Case, :count).by(0)
      .and change(Crm::HealthInfo, :count).by(0)
      .and change(Crm::FinancialInfo, :count).by(0)
      .and change(Quoting::Quote, :count).by(0)
      .and change { cons.reload.full_name }.to 'Jane6 Lane7'
    end

    it 'returns 404 if id is bad' do
      params[:insureio_consumer_id] = 9999
      post :create, params
      expect(response.status).to eq 404
      expect(response.body).to eq "{\"errors\":\"Couldn't find Consumer with id=9999\"}"
    end

    it 'returns 422 for bad params' do
      expect {
        post :create, params.merge(phone_primary: 'foobar')
        expect(response.status).to eq 422
        expect(response.body).to eq "{\"errors\":{\"phones.value\":[\"should be 10 digits in length, starting with a valid area code\"]}}"
      }.to change { Consumer.unscoped.count }.by(0)
    end

    it 'returns 422 for missing ESP params' do
      expect {
        post :create, params.except(:ssn)
        expect(response.status).to eq 422
        expect(response.body).to eq "{\"errors\":{\"ssn\":[\"cannot be blank\"]}}"
      }.to change { Consumer.unscoped.count }.by(0)
    end
  end

  describe '#alq_save' do
    before { default_user }

    let(:json) { File.read File.join(Rails.root, 'spec/support/ixn/alq_save.json') }
    let(:default_user) {
      nmb_id = Enum::Tenant.find(10).default_parent_id
      User.find_by(id: nmb_id) || create(:user, id: nmb_id)
    }

    it 'saves a quote' do
      expect {
        post :alq_save, params
        expect(response.status).to eq 201
      }.to change(Quoting::Quote, :count).by(1)
      .and change(Crm::Case, :count).by(1)
      .and change(Consumer, :count).by(1)
    end

    it 'updates a quote' do
      consumer = create :consumer, agent: default_user
      quote = create :quoting_quote, consumer: consumer
      expect {
        post :alq_save, params.merge(insureio_quote_id: quote.id)
        expect(response.status).to eq 200
      }.to change(Quoting::Quote, :count).by(0)
      .and change(Crm::Case, :count).by(0)
      .and change(Consumer, :count).by(0)
    end

    it 'denies permission for record outside of scope' do
      default_user # Ensure this record exists
      consumer = create :consumer
      quote = create :quoting_quote, consumer: consumer
      expect {
        post :alq_save, params.merge(insureio_quote_id: quote.id)
        expect(response.status).to eq 403
      }.to change(Quoting::Quote, :count).by(0)
      .and change(Crm::Case, :count).by(0)
      .and change(Consumer, :count).by(0)
    end

    it 'returns an error message for invalid record' do
      expect {
        post :alq_save, params.except(:first_name, :last_name)
        expect(response.status).to eq 422
        expect(response.body).to match "full_name.*can't be blank"
      }.to change(Quoting::Quote, :count).by(0)
      .and change(Crm::Case, :count).by(0)
      .and change(Consumer, :count).by(0)
    end
  end

  describe '#xml' do
    let(:kase) { create :crm_case }

    it '200s when user can view case' do
      authentication_stubs
      kase.update_columns agent_id: authenticated_user.id
      mock = double presigned_url: 'http://fake.com/signed-url'
      expect(Aws::S3::Presigner).to receive(:new).and_return mock
      get :xml, id: kase.id, format: :json
      expect(response.status).to eq 200
      expect(response.body).to eq '{"url":"http://fake.com/signed-url"}'
    end

    it '401s when unauthenticated' do
      authentication_unstub
      get :xml, id: kase.id, format: :json
      expect(response.status).to eq 401
    end

    it '403s when case is not viewable by user' do
      user = create :user
      authentication_stubs user
      get :xml, id: kase.id, format: :json
      expect(response.status).to eq 403
    end

    it '404s when case does not exist' do
      authentication_stubs
      get :xml, id: 0, format: :json
      expect(response.status).to eq 404
    end
  end
end
