require 'spec_helper'

describe Usage::StripeController do

  describe '#create_event' do
    let(:params){
      {"id"=>"evt_155LOK4GgNdNSosPmCTsypVW", "created"=>1417544932, "livemode"=>true, "type"=>"charge.succeeded", "data"=>{"object"=>{"id"=>"ch_155LOJ4GgNdNSosPT6Y2gc1e", "object"=>"charge", "created"=>1417544931, "livemode"=>true, "paid"=>true, "amount"=>599, "currency"=>"usd", "refunded"=>false, "captured"=>true, "refunds"=>[], "card"=>{"id"=>"card_1555d74GgNdNSosP58e9m8dE", "object"=>"card", "last4"=>"3610", "brand"=>"MasterCard", "funding"=>"prepaid", "exp_month"=>6, "exp_year"=>2016, "fingerprint"=>"R1foSagcnhJFpUxl", "country"=>"US", "name"=>"phillipwajda@gmail.com", "address_line1"=>nil, "address_line2"=>nil, "address_city"=>nil, "address_state"=>nil, "address_zip"=>nil, "address_country"=>nil, "cvc_check"=>nil, "address_line1_check"=>nil, "address_zip_check"=>nil, "dynamic_last4"=>nil, "customer"=>"cus_5Fr61tluZBO5g3", "type"=>"MasterCard"}, "balance_transaction"=>"txn_155LOK4GgNdNSosP8zjF9AoC", "failure_message"=>nil, "failure_code"=>nil, "amount_refunded"=>0, "customer"=>"cus_5Fr61tluZBO5g3", "invoice"=>"in_155LOJ4GgNdNSosPEFfvXo2o", "description"=>nil, "dispute"=>nil, "metadata"=>{}, "statement_description"=>"Dataraptor 1", "fraud_details"=>{"stripe_report"=>"unavailable", "user_report"=>nil}, "receipt_email"=>nil, "receipt_number"=>nil, "shipping"=>nil}}, "object"=>"event", "pending_webhooks"=>1, "request"=>"iar_5Fr6D8Vbr6FY1v", "api_version"=>"2014-03-28"}.with_indifferent_access
    }

    it 'creates a Usage::StripeEvent' do
      expect{ post :create_event, params }.to change(Usage::StripeEvent, :count).by(1)
    end
    it 'returns 200' do
      Usage::StripeEvent.any_instance.stub(:save!).and_return(true)
      post :create_event, params
      response.status.should == 200
    end
    it 'does not raise error' do
      Usage::StripeEvent.any_instance.stub(:save!).and_return(true)
      expect{ post :create_event, params }.to_not raise_error
    end
    context 'when processable' do
      it 'calls process' do
        expect_any_instance_of(Usage::StripeEvent).to receive(:processable?).exactly(2).times.and_return(true)
        expect_any_instance_of(Usage::StripeEvent).to receive(:process)
        post :create_event, params
      end
    end
    context 'when not processable' do
      it 'does not call process' do
        expect_any_instance_of(Usage::StripeEvent).to receive(:processable?).exactly(2).times.and_return(false)
        expect_any_instance_of(Usage::StripeEvent).to_not receive(:process)
        post :create_event, params
      end
    end
  end

end
