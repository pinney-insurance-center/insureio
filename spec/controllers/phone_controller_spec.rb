require 'spec_helper'

describe PhoneController do
  before do
    authentication_stubs
    @authenticated_user.can! :edit_self
  end

  let(:auth_params) { { person_id: @authenticated_user.id, person_type: 'User' } }

  describe '#create' do
    it '401s when not logged in' do
      authentication_unstub
      post :create, auth_params.merge(format: :json,  phone: {value: '1234567890'})
      expect(response.status).to eq(401), response.body
    end
    it '422s when phone params are invalid' do
      post :create, auth_params.merge(format: :json,  phone: {value: '123456789'})
      expect(response.status).to eq(422), response.body
      expect(response.body).to match /should be 10 digits in length/
    end
    it 'creates a new Record' do
      expect {
        post :create, auth_params.merge(format: :json, phone: {value: '8014567890'})
      }.to change(Phone, :count).by(1)
      expect(response.status).to eq(201), response.body
    end
  end
end