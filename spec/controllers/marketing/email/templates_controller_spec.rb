require 'spec_helper'
require 'controllers/marketing/shared_examples_template_controller'

describe Marketing::Email::TemplatesController do
  before do
    authentication_stubs
    current_user.enable_permissions!(
      :email,
      :comm_template_status,
      :comm_template_marketing,
      :marketing,
      :marketing_canned_email,
      :marketing_custom_email,
      :marketing_canned_print,
      :marketing_custom_print,
      :marketing_canned_sms,
      :marketing_custom_sms,
    )
    current_user.emails = build_list(:email_address, 1)
    current_user.stub(:smtp_servers).and_return(build_list :marketing_email_smtp_server, 1)
    Mail::Message.any_instance.stub(:deliver).and_return(true)
    EmailMessageWorker.stub(:perform_at)
  end
  
  let(:model){ Marketing::Email::Template }
  
  it_behaves_like "template_controller"

  describe '#model' do
    context 'without relevant params' do
      it 'should return Email' do
        controller.params = {}
        controller.send(:model).should == Marketing::Email::Template
      end
    end
  end

end