shared_examples_for "template_controller" do

  describe '#model' do
    context 'params has task_type_id of 2' do
      it 'returns Email' do
        controller.params[:task_type_id] = 2
        controller.send(:model).should == Marketing::Email::Template
      end
    end
    context 'params has task_type_id of 6' do
      it 'returns Sms' do
        controller.params[:task_type_id] = 6
        controller.send(:model).should == Marketing::MessageMedia::Template
      end
    end
    context 'params has task_builder whose task_type is 8' do
      let(:task_builder){ Crm::TaskBuilder.new(task_type_id:8) }
      it 'returns Letter' do
        controller.params[:task_builder_id] = 5
        Crm::TaskBuilder.should_receive(:find).with(5).and_return(task_builder)
        controller.send(:model).should == Marketing::SnailMail::Template
      end
    end
  end

end