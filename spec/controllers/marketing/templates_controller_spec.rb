require 'spec_helper'
require 'controllers/marketing/shared_examples_template_controller'
require 'pry'
describe Marketing::TemplatesController do
  before do
    authentication_stubs
    model=Marketing::Template
    current_user=authenticated_user
    current_user.can!(:comm_template_status)
    current_user.can!(:comm_template_marketing)
    current_user.can!(:marketing)
    current_user.can!(:marketing_canned_email)
    current_user.can!(:marketing_custom_email)
    current_user.can!(:marketing_canned_print)
    current_user.can!(:marketing_custom_print)
    current_user.can!(:marketing_canned_sms)
    current_user.can!(:marketing_custom_sms)
  end
  
  it_behaves_like "template_controller"

  describe '#model' do
    context 'without relevant params' do
      it 'should return Template' do
        controller.params = {}
        controller.send(:model).should == Marketing::Template
      end
    end
  end

  describe '#index' do
    let(:tmpl_1){ build :marketing_message_media_template, ownership_id:Enum::Ownership.id("user"),owner_id:current_user.id }
    let(:tmpl_2){ build :marketing_email_template,         ownership_id:Enum::Ownership.id("user"),owner_id:current_user.id }
    let(:tmpl_3){ build :marketing_snail_mail_template,    ownership_id:Enum::Ownership.id("user"),owner_id:current_user.id }
    context 'when there are templates with ownership type "user", owned by various users,' do
      before do
        tmpl_1[:owner_id]=current_user.id;   tmpl_1.save
        tmpl_2[:owner_id]=current_user.id+1; tmpl_2.save
        tmpl_3[:owner_id]=current_user.id+1; tmpl_3.save
      end
      it 'selects only records owned by current user' do
        get :index, format:'json'
        expect( response.body ).to be_present
        templates = JSON.parse response.body
        expect( templates.length ).to eq(1)
        expect( templates.all?{|t| t['owner_id']==current_user.id } ).to eq(true)
      end
    end
    context 'when params specify medium "Email",' do
      before do
        [tmpl_1,tmpl_2,tmpl_3].map(&:save)
      end
      it 'selects records of only the Email model' do
        get :index, format:'json', medium:'Email'
        response.body.should_not be_empty
        templates = assigns(:scope)
        json = JSON.parse response.body
        json.length.should == templates.length
        templates.all?{|t| t.type.should == "Marketing::Email::Template"}
      end
    end
  end

  shared_examples_for :successful_deliver_call do
    it 'returns 200' do
      post :deliver, params
    end
    it 'does not raise error' do
      expect{ post :deliver, params }.to_not raise_error
    end
  end

  describe '#deliver' do
    let(:template){ create :marketing_email_template }
    let(:params){ {id:template.id, sending_option:'self'} }

    before do
      authentication_stubs
    end

    context 'when sending_option is "self"' do
      let(:params){{ sending_option:'self', id:template.id }}
      it_behaves_like :successful_deliver_call
      it 'calls @template.deliver' do
        Marketing::Email::Template.any_instance.stub(:deliver)
        expect_any_instance_of(Marketing::Email::Template).to receive(:deliver).at_least(1).times
        post :deliver, params
      end
    end
    context 'when sending_option is "custom"' do
      let(:params){{ sending_option:'custom', id:template.id.to_s, recipients:'foo@bar.com baz@qux.com fly@away.com' }}
      it_behaves_like :successful_deliver_call
      before do
        Marketing::Email::Blast.any_instance.stub(:has_valid_targets){true}
        Marketing::Email::Blast.any_instance.stub(:save){true}
        Marketing::Email::Blast.any_instance.stub(:deliver){"A message indicating that it delivered."}
      end
      it 'creates a new blast' do
        Marketing::Email::Blast.should receive(:new).with({user_id:current_user.id, template: template,recipients: params[:recipients]}).and_call_original
        post :deliver, params
      end
    end
    context 'when sending_option is "blast"' do
      let(:params){{ sending_option:'blast', id:template.id, search_id:'5' }}
      it_behaves_like :successful_deliver_call
      before do
        Marketing::Email::Blast.any_instance.stub(:has_valid_targets){true}
        Marketing::Email::Blast.any_instance.stub(:save){true}
        Marketing::Email::Blast.any_instance.stub(:deliver){"A message indicating that it delivered."}
      end
      it 'creates a new blast' do
        Marketing::Email::Blast.should receive(:new).with({user_id:current_user.id, template: template,search_id: params[:search_id]}).and_call_original
        post :deliver, params
      end
    end
  end

  describe '#gauge_size_of_delivery' do
    context 'when sending option is blast' do
      let(:report){ build :reporting_search }
      context 'and there are enough recipients to warrant a confirmation,' do
        before do
          allow(Reporting::Search).to receive(:find).and_return(report)
          allow(report).to receive(:get_matching_record_ids_for_user).and_return( [(1..500).to_a,[] ] )
          controller.stub(:deliver){render nothing:true}
        end
        it 'renders the javascript for a confirmation dialog' do
          post :gauge_size_of_delivery, {id:1, sending_option:'blast'}
          expect(response.body).to eq("{\"requires_confirmation\":true,\"size\":501}")
        end
      end
      context 'and there are NOT enough recipients to warrant a confirmation,' do
        before do
          allow(Reporting::Search).to receive(:find).and_return(report)
          allow(report).to receive(:get_matching_record_ids_for_user).and_return( [(1..499).to_a,[] ] )
        end
        it 'calls the #deliver action' do
          controller.should_receive(:deliver)
          post :gauge_size_of_delivery, {id:1, sending_option:'blast'}
        end
      end
    end
  end

end