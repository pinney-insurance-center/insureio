require 'spec_helper'

describe BrandsController do
  before { authentication_stubs }

  let(:owner) { create :user, security_questions_answered: true, enabled: true }
  let(:brand) { create :brand, owner: owner }

  describe '#agents' do
    it 'returns an array' do
      get :agents, format:'json', id: brand.id
      expect(response.status).to eq 200
      expect(response.body).to eq '[]'
    end

    context 'with params[:lead_types]' do
      it 'returns an array' do
        get :agents, format:'json', id: brand.id, lead_types:['foo bar baz']
        expect(response.status).to eq 200
        expect(response.body).to eq '[]'
      end
    end
  end

  describe '#update_assignments' do
    pending('Spec needs implementation.')
  end
end
