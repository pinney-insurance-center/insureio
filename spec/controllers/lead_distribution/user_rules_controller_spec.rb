require 'spec_helper'

describe LeadDistribution::UserRulesController do

  let(:agency_mgr) { create(:user).tap { |u| u.disable_permissions! :super_edit, :super_view; u.can! :agency_mgmt } }
  let(:regular_user) { create(:user).tap { |u| u.disable_permissions! :super_edit, :super_view, :agency_mgmt } }
  let(:brand) { create :brand, owner: agency_mgr }
  let(:cursor) { LeadDistribution::Cursor.new(brand: brand).tap { |c| c.save validate: false } }

  before do
    authentication_stubs agency_mgr
  end

  describe '#update_multiple' do
    let(:params){{rules:[rule_0, rule_1], format: :json}}
    let(:rule_0){{off:true,  sunday:0, monday:0, tuesday:0, wednesday:1, thursday:1, friday:2, saturday:3, id:55}}
    let(:rule_1){{off:false, sunday:1, monday:2, tuesday:3, wednesday:4, thursday:5, friday:6, saturday:7, id:56}}

    it "403's for non-agency-manager" do
      authentication_stubs regular_user
      put :update_multiple, format: :json
      expect(response.status).to eq 403
      expect(response.body).to match /You must be an agency manager to access this endpoint or resource/
    end

    it "422's for rules from a non-allowed Brand" do
      other_brand = create :brand
      other_cursor = LeadDistribution::Cursor.new(brand: other_brand).tap { |c| c.save validate: false }
      other_rule = create :lead_distribution_user_rule, cursor: other_cursor, sunday: 99
      put :update_multiple, format: :json, rules: [{ id: other_rule.id, sunday: 11 }]
      expect(response.status).to eq(422)
      expect(other_rule.reload.sunday).to eq 99
    end

    context 'with 2 good rules and 0 bad ones' do
      it 'returns 200' do
        # create necessary rules
        create :lead_distribution_user_rule, rule_0.merge(cursor: cursor)
        create :lead_distribution_user_rule, rule_1.merge(cursor: cursor)
        # execute request
        put(:update_multiple, params)
        expect(response.status).to be 200
      end
    end

    context 'with 1 good rule and 1 bad one' do
      it 'returns 422 and includes error messages' do
        # create necessary rules
        create :lead_distribution_user_rule, rule_1.merge(cursor: cursor)
        # execute request
        put(:update_multiple, params)
        expect(response.status).to be 422
        data = JSON.parse(response.body)
        expect(data['failures'].length).to be 1
        expect(data['failures'].first).to have_key('errors')
      end
    end

    context 'with empty rules param' do
      it 'returns 422 and renders JSON with an error message' do
        put(:update_multiple, format: :json)
        expect(response.status).to be 422
        data = JSON.parse(response.body)
        expect(data).to have_key('error')
      end
    end
  end
end
