require 'spec_helper'

describe LeadDistribution::CursorsController do

  describe '#create' do
    let(:brand){Brand.new}

    context 'missing brand' do
      it '422s with error messages' do
        post :create, format:'json', lead_type_text:'foo'
        expect(response.status).to be 422
        expect(response.body).to match /brand/
      end
    end
    context 'with lead_type & brand' do
      it '201s' do
        expect_any_instance_of(LeadDistribution::Cursor).to receive(:brand).and_return(brand)
        post :create, format:'json', brand_id:43, lead_type_text:'foo'
        expect(response.status).to be 201
        expect(response.body).to match /\{"id":\d+,"lead_type_id":\d+,"brand_id":\d+,"rule_id":0\}/
      end
    end

  end

  describe '/lead_distribution/cursors.json', type: :routing do
    it 'routes to #create' do
      expect(post:'/lead_distribution/cursors.json').to route_to(action:'create', controller:'lead_distribution/cursors', format:'json')
    end
  end

end