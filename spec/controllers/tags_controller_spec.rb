require 'spec_helper'

describe TagsController do

  before :all do
    Tag.delete_all
  end

  before do
    authentication_stubs
    Tag.new(tenant_id: authenticated_user.tenant_id, value: 'same-tenant').save validate: false
    Tag.new(tenant_id: 1+authenticated_user.tenant_id, value: 'diff-tenant').save validate: false
  end

  describe '#index' do
    context 'request from jQuery tag manager' do
      it 'returns 200' do
        get :index, format: 'json', q: 'matcher', tags_manager: true
        expect(response.code).to eql "200"
      end
    end

    it '200s when user is super' do
      authenticated_user.can! :super_view
      get :index, format:'json', q: 'tenant'
      expect(response.status).to eql 200
      data = JSON.parse(response.body)
      expect(data.map { |d| d['text'] }).to match_array %w[same-tenant diff-tenant]
    end

    it '200s when user is not super' do
      authenticated_user.can! :super_view, false
      authenticated_user.can! :motorists_rtt_process_details, false
      get :index, format: 'json', q: 'tenant'
      expect(response.status).to eql 200
      data = JSON.parse(response.body)
      expect(data.map { |d| d['text'] }).to match_array %w[same-tenant]
    end

    it 'does not return duplicates' do
      authenticated_user.can! :super_view
      Tag.new(tenant_id: authenticated_user.tenant_id, value: 'repeated-value').save validate: false
      Tag.new(tenant_id: 1+authenticated_user.tenant_id, value: 'repeated-value').save validate: false
      expect(Tag.all.pluck(:value).select { |t| t == 'repeated-value' }.length).to eq(2)
      get :index, format: 'json', q: 'ted'
      data = JSON.parse(response.body)
      expect(data.map { |d| d['text'] }).to match_array %w[repeated-value]
    end

    it '422s when q is absent or too short' do
      get :index, format: 'json', q: 'ma', tags_manager: true
      expect(response.status).to eq 422
      get :index, format: 'json', tags_manager: true
      expect(response.status).to eq 422
      expect(response.body).to match /"A parameter \\\"q\\\" of length 3 chars or greater is required"/
    end
  end

end