require 'spec_helper'
require 'support/authentication_helper'

describe ContactsController do
  render_views

  before do
    authentication_stubs
    current_user.can!(:contacts_users, true)
    current_user.can!(:contacts_consumers, true)
    current_user.can!(:contacts_brands, true)
  end

  shared_examples_for 'identity from message' do |endpoint|
    subject { create :marketing_email_message, target: kase, sender: cons.agent }
    let(:kase) { create :crm_case, consumer: cons }
    let(:cons) { create :consumer }

    it '200s when recpient matches message' do
      get endpoint, message_id: subject.id, recipient: subject.recipient
      expect(response.status).to eq 200
    end

    it '200s when :quoter_key matches message' do
      get endpoint, message_id: subject.id, key: subject.quoter_key, person_type: 'Consumer', person_id: subject.person.id
      expect(response.status).to eq 200
    end

    it '200s when :ltd_access_code matches message' do
      ltd_access_code = cons.agent.access_code cons
      get endpoint, message_id: subject.id, ltd_access_code: ltd_access_code
      expect(response.status).to eq 200
    end

    it '404s when message_id is absent' do
      get endpoint
      expect(response.status).to eq 404
    end

    it '404s when quoter_key mismatches person' do
      get endpoint, message_id: subject.id, key: subject.quoter_key, person_type: 'Consumer', person_id: 99999
      expect(response.status).to eq 404
    end

    it '404s when recipient mismatches message' do
      get endpoint, message_id: subject.id, recipient: 'bad-email@coca.col'
      expect(response.status).to eq 404
    end
  end

  describe '#index' do
    shared_examples_for 'search_by' do |field, status, body|
      it '200s' do
        get :index, format: :json, search_by: field, search_term: 'rainy'
        if status.nil?
          expect(response.status).to eq 200
          expect(JSON.parse response.body).to be_a Array
        else
          expect(response.status).to eq status
          expect(response.body).to match body
        end
      end
    end

    include_examples 'search_by', 'id'
    include_examples 'search_by', 'name'
    include_examples 'search_by', 'name_or_id'
    include_examples 'search_by', 'phone'
    include_examples 'search_by', 'email'
    include_examples 'search_by', 'rubbish', 422, /Unsupported search field: rubbish/

    it '200s when specifying no params' do
      get :index, format: :json
      expect(response.status).to eq 200
      expect(JSON.parse response.body).to be_a Array
    end
    it '200s when specifying with params[:name_starting_with]' do
      get :index, format: :json, name_starting_with: 'A-C'
      expect(response.status).to eq 200
      expect(JSON.parse response.body).to be_a Array
    end
    context 'with params[:paginate]' do
      it '422s without params[:person_type]' do
        get :index, format: :json, paginate: 1
        expect(response.status).to eq 422
        expect(response.body).to match /Pagination not supported on multi-model searches/
      end
      it 'returns object with records array' do
        get :index, format: :json, paginate: 1, person_type: 'Consumer'
        expect(response.status).to eq 200
        data = JSON.parse response.body
        expect(data['page']).to eq 1
        expect(data['page_count']).to be_a Fixnum
        expect(data['records']).to be_a Array
      end
    end
  end

  describe '#unsubscribe_from_mktg_step_two' do
    include_examples 'identity from message', :unsubscribe_from_mktg_step_two
  end

  describe '#unsubscribe_from_status_step_two' do
    include_examples 'identity from message', :unsubscribe_from_status_step_two
  end

  context 'when unauthenticated' do
    before do
      authentication_unstub
    end

    let(:consumer) { create :consumer }
    let(:quote) { create :quoting_quote, consumer: consumer }
    let(:template) { create :marketing_email_template }
    let(:message) { Marketing::Email::Message.create! template: template, target: quote }
    let(:key) { consumer.agent.quoter_key }

    describe '#index' do
      it 'redirects' do
        get :index
        expect(response.status).to eq 303
        expect(response.location).to match /\/login(\/|\?)/
      end
    end

    describe '#unsubscribe_from_mktg_step_one' do
      it '200s, given with Consumer and params[:person_id]' do
        get :unsubscribe_from_mktg_step_one, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: key
        expect(response.status).to eq 200
      end
      it '200s, given with Consumer and params[:recipient]' do
        get :unsubscribe_from_mktg_step_one, recipient: message.recipient, message_id: message.id
        expect(response.status).to eq 200
      end
      it '404s, given Consumer and bad key' do
        get :unsubscribe_from_mktg_step_one, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: 'badkey'
        expect(response.status).to eq 404
      end
      it '404s, given Consumer and bad message_id' do
        get :unsubscribe_from_mktg_step_one, recipient: message.recipient, message_id: -1
        expect(response.status).to eq 404
      end
    end

    describe '#unsubscribe_from_mktg_step_two' do
      it '200s, given with Consumer and params[:person_id]' do
        expect(consumer.reload.marketing_unsubscribe).to be false
        get :unsubscribe_from_mktg_step_two, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: key
        expect(response.status).to eq 200
        expect(consumer.reload.marketing_unsubscribe).to be true
      end
      it '200s, given with Consumer and params[:recipient]' do
        expect(message.person.marketing_unsubscribe).to be false
        get :unsubscribe_from_mktg_step_two, recipient: message.recipient, message_id: message.id
        expect(response.status).to eq 200
        expect(message.person.reload.marketing_unsubscribe).to be true
      end
      it '404s, given Consumer and bad key' do
        get :unsubscribe_from_mktg_step_two, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: 'badkey'
        expect(response.status).to eq 404
      end
      it '404s, given Consumer and bad message_id' do
        get :unsubscribe_from_mktg_step_two, recipient: message.recipient, message_id: -1
        expect(response.status).to eq 404
      end
    end

    describe '#unsubscribe_from_status_step_one' do
      it '200s, given with Consumer and params[:person_id]' do
        get :unsubscribe_from_status_step_one, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: key
        expect(response.status).to eq 200
      end
      it '200s, given with Consumer and params[:recipient]' do
        get :unsubscribe_from_status_step_one, recipient: message.recipient, message_id: message.id
        expect(response.status).to eq 200
      end
      it '404s, given Consumer and bad key' do
        get :unsubscribe_from_status_step_one, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: 'badkey'
        expect(response.status).to eq 404
      end
      it '404s, given Consumer and bad message_id' do
        get :unsubscribe_from_status_step_one, recipient: message.recipient, message_id: -1
        expect(response.status).to eq 404
      end
    end

    describe '#unsubscribe_from_status_step_two' do
      it '200s, given with Consumer and params[:person_id]' do
        expect(consumer.reload.status_unsubscribe).to be_falsey
        get :unsubscribe_from_status_step_two, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: key
        expect(response.status).to eq 200
        expect(consumer.reload.status_unsubscribe).to be true
      end
      it '200s, given with Consumer and params[:recipient]' do
        expect(message.person.reload.status_unsubscribe).to be_falsey
        get :unsubscribe_from_status_step_two, recipient: message.recipient, message_id: message.id
        expect(response.status).to eq 200
        expect(message.person.reload.status_unsubscribe).to be true
      end
      it '404s, given Consumer and bad key' do
        get :unsubscribe_from_status_step_two, person_id: consumer.id, person_type: :Consumer, message_id: message.id, key: 'badkey'
        expect(response.status).to eq 404
      end
      it '404s, given Consumer and bad message_id' do
        get :unsubscribe_from_status_step_two, recipient: message.recipient, message_id: -1
        expect(response.status).to eq 404
      end
    end
  end
end