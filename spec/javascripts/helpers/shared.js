module.exports = {};

module.exports.scroll = (element) => {
	browser.actions().mouseMove(element).perform();
	return element;
};

module.exports.formatCurrency = (value) => {
  let text = value.toString();
  let split = text.length % 3;
  let prefix = text.substring(0, split);
  let suffix = text.substring(split).replace(/(\d{3})/g, ',$1');
  return `$${prefix}${suffix}`.replace('$,', '$');
};

/* If a uibModal appears (e.g. to suggest duplicate Consumer records), click the close button */
module.exports.closeModal = async (modalSelector) => {
  modalCloseBtn = element(by.css(`body > ${modalSelector} div.modal-header a.close`));
  return browser.wait(modalCloseBtn.isDisplayed, 1000) /* Wait for modal to appear */
  .then(() => modalCloseBtn.click()) /* Return promise */
  .catch(err => null); /* Give up if modal doesn't appear */
}
