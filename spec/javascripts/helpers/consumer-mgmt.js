const helper = require(__dirname + '/shared.js');
const scroll = helper.scroll;
module.exports = {};

module.exports.verifyDiscovery = (values) => {
  /* Personal */
  let personal = element(by.id('contact-personal'));
  let personalText = personal.getText();
  expect(personalText).toMatch(new RegExp(`Occupation\\s*${values.occupation}`));
  expect(personalText).toMatch(new RegExp(`Employer\\s*${values.company}`));
  expect(personalText).toMatch(new RegExp(`Citizenship\\s*US Citizen`));
  /* Case */
  var until = protractor.ExpectedConditions;
  let kases = element(by.id('cases-panel'));
  browser.wait(until.presenceOf(kases), 60*1000, 'Element taking too long to appear in the DOM');
  scroll(kases).element(by.css('.details-row')).click();
  let kasesText = kases.getText();
  expect(kasesText).toMatch(new RegExp(`Duration\\s*${values.duration}`));
  /* Finance */
  scroll(element(by.id('financial-trigger'))).click();
  let finance = element(by.id('financial'));
  expect(finance.isPresent()).toBeTruthy();
  let netWorth = helper.formatCurrency(values.netWorth);
  expect(finance.element(by.model('finance.net_worth_specified')).getAttribute('value')).toEqual(netWorth);
};
