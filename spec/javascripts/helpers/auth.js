module.exports = {};
module.exports.login = () => {
    browser.get('/login');

    expect(element(by.css('a[ng-href="/quoting/quotes/new"]')).isPresent()).toBeFalsy('Link is present before login');
    expect(element(by.css('.alert-danger')).isPresent()).toBeFalsy('Danger alert is present');

    element(by.model('userName')).sendKeys(browser.params.login.user);
    element(by.model('password')).sendKeys(browser.params.login.password);
    element(by.css('[value="Login"]')).click();

    expect(element(by.css('.alert-danger')).isPresent()).toBeFalsy('Danger alert is present');
    expect(element(by.css('a[ng-href="/quoting/quotes/new"]')).isPresent()).toBeTruthy('Link is not present after login');
    browser.params.isLoggedIn = true;
    /* Remove the 'successful login' bulletin so that it doesn't intercept clicks intended for elements behind it. */
    element(by.css("body > div.wrapper > div.bulletin-container > div.alert-success > button > span")).click();
};
