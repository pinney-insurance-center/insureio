const MODULES = [
	//dependencies
	'ui.select', 'ui.mask', 'ui.bootstrap.datetimepicker', 'angularFileUpload', 'ui.tinymce', 'xeditable', 'ngSanitize', 'rzModule',
	//app modules
	'insureioNg',
	'ioConstants',
	'ioRequestSanitizer',
	"DrModels",
	"DrFilters",
	"dr.directives",
	'DrBulletins',
	"dr.controllers",
	'SocketClientApp',
	'ContactsMgmtApp',
	'UserHierarchyMgmtApp',
	"newQuoteApp",
	"WidgetApp",
	"drLeadDistribution",
	"dr.consumer",
	"dr.csvImport",
	"dr.marketing",
	"dr.users",
	"dr.brands",
	'DashboardApp',
	'noteApp',
	'ConsumerManagementApp',
	'PolicyMgmtApp',
	'SearchApp', 
	'TasksApp',
	'DrStatusTypeMgmtApp'
];

var helpers = {};

helpers.setRootScopeMembers = (scope) => {
  scope.getCurrentUser = () => {
    scope.currentUser = helpers.mockSuperUser();
    return scope.currentUser;
  };
  scope.getPolicyStatusTypes = () => ([]);
  scope.getSubscriptions = () => ([]);
}

helpers.loadModule = (name) => {
	beforeEach(module(name));
}

helpers.loadModules = (namesArray) => {
	for (var name of namesArray)
		helpers.loadModule(name);
}

helpers.stubAndLoadModule = (name) => {
	angular.module(name, []);
	helpers.loadModule(name);
}

helpers.stubAndLoadModules = (namesArray) => {
	for (var name of namesArray)
		helpers.stubAndLoadModule(name);
}

helpers.stubProviders = (namesArray) => {
	let mod = angular.module('dummy', []);
	for (var name of namesArray)
		mod.value(name, {});
	helpers.loadModule('dummy');
}

helpers.mockPerson = (attrs) => (
  Object.assign({id: 11}, attrs, {
    $promise: Promise.resolve(),
    $updateWithCustomParams: function (attrs) { for (key in attrs) this[key] = attrs[key] },
}));

helpers.mockSuperUser = (attrs) => (
  Object.assign(helpers.mockPerson(attrs), {
    id: 1,
    can: permission => (true),
}));
