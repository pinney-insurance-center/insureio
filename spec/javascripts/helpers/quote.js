const helper = require(__dirname + '/shared.js');
const scroll = helper.scroll;
/* `helpers/health-info.js` is generated from the task rake underwriting:js:spec */
const healthInfoHelper = require(__dirname + '/health-info.js')

module.exports = {};

/* Abstracting this to a separate function is useful because it allows us to
/* use `await` without requiring that all functions above it on the call stack
/* treat it as asynchronous (see `fillBasicAsync`) */
module.exports.fillNameAndEmail = async (name, email, basicInfo) => {
  await basicInfo.element(by.model('consumer.full_name')).sendKeys(name, protractor.Key.TAB);
  await helper.closeModal('.duplicate-consumer-check');
  await basicInfo.element(by.model('consumer.$primaryEmailAttributes.value')).sendKeys(email, protractor.Key.TAB);
  await helper.closeModal('.duplicate-consumer-check');
}

module.exports.fillBasicInfo = (basicInfo, values) => {
  if (!basicInfo) basicInfo = element(by.id('basic-info'));
  values = Object.assign({
    name: 'John Doe', email: 'foo@bar.baz',
    gender: '1', birth: '01011988',
    feet: '5', inches: '9', weight: '178',
    state_id: '3',
    tobaccoId: 'quoter-tobacco-0',
    health_class_id: '1', face_amount: '1234567', duration_id: '24',
  }, values);
  browser.wait(module.exports.fillNameAndEmail(values.name, values.email, basicInfo), 60000);
  basicInfo.element(by.model('consumer.gender')).$(`[value="${values.gender}"]`).click();
  basicInfo.element(by.model('health.feet')).sendKeys(values.feet);
  basicInfo.element(by.model('health.inches')).sendKeys(values.inches);
  basicInfo.element(by.model('health.weight')).sendKeys(values.weight);
  basicInfo.element(by.model('consumer.$primaryAddressAttributes.state_id')).$(`[value="number:${values.state_id}"]`).click();
  basicInfo.element(by.css('[id$="-birth_or_trust_date"]')).sendKeys(values.birth);
  basicInfo.element(by.id(values.tobaccoId)).click();
  if (values.health_class_id)
    basicInfo.element(by.model('quote.health_class_id')).$(`[value="number:${values.health_class_id}"]`).click();
  basicInfo.element(by.model('quote.face_amount')).sendKeys(values.face_amount);
  basicInfo.element(by.model('quote.duration_id')).$(`[value="number:${values.duration_id}"]`).click();
  return basicInfo;
};

module.exports.fillDiscovery = (values, container) => {
  if (!container) container = element(by.css('div[ng-controller="QuotePath.DiscoveryCtrl"]'));
  expect(container.isPresent()).toBeTruthy();
  scroll(container.element(by.model('finance.net_worth_specified'))).sendKeys(values.netWorth || 98765);
  scroll(container.element(by.model('consumer.occupation'))).sendKeys(values.occupation || 'food sherrif');
  scroll(container.element(by.model('consumer.company'))).sendKeys(values.company || 'hot diggity dog');
  container.element(by.model('quote.duration_id')).$(`[value="number:${values.durationId || 23}"]`).click();
}

/* This only does items _besides_ items handled by `helpers/health-info.js``
/* (particularly diseases) because they take so long to test. Use
/* `helpers/health-info.js` to test diseases and family diseases. */
module.exports.fillUnderwriting = (container, doHeightWeightTobacco) => {
  if (!container) container = element(by.id('health-underwriting'));
  expect(container.isPresent()).toBeTruthy();
  if (doHeightWeightTobacco) {
    throw new Error('Not implemented yet');
  }
  scroll(container.element(by.id('underwriting-citizen-1'))).click();
  scroll(container.element(by.id('underwriting-bankruptcy-1'))).click();
  scroll(container.element(by.css('[id$="-bankruptcy_declared"]'))).sendKeys('03/22/1971');
  scroll(container.element(by.css('[id$="-bankruptcy_discharged"]'))).sendKeys('03/30/1971');
  return container;
}

module.exports.fillApplication = (opts) => {
  let app = element(by.id('quote-path.application'));
  expect(app.isPresent()).toBeTruthy();
  scroll(app.element(by.model('consumer.birth_country_id'))).$('[value="number:1"]').click();
  scroll(app.element(by.model('consumer.dln'))).sendKeys('qwert1234');
  scroll(app.element(by.model('consumer.dl_state_id'))).$('[value="number:5"]').click();
  scroll(app.element(by.model('consumer.ssn'))).sendKeys('123456789');
  scroll(app.element(by.model('consumer.birth_state_id'))).$('[value="number:1"]').click();
  scroll(app.element(by.model('consumer.citizenship_id'))).$('[value="1"]').click();
  scroll(app.element(by.model('consumer.$primaryAddressAttributes.street'))).sendKeys('123 fake st');
  scroll(app.element(by.model('consumer.$primaryAddressAttributes.city'))).sendKeys('America city');
  scroll(app.element(by.model('consumer.$primaryAddressAttributes.zip'))).sendKeys('22661');
  scroll(app.element(by.model('consumer.$primaryAddressAttributes.state_id'))).$('[value="number:5"]').click();
  scroll(app.element(by.cssContainingText('a', 'Add Phone'))).click();
	scroll(app.element(by.model('phone.value'))).sendKeys('9164567890');
  scroll(app.element(by.model('phone.phone_type_id'))).$('[value="number:1"]').click();
	scroll(app.element(by.model('consumer.preferred_contact_time'))).sendKeys('pm');
  scroll(app.element(by.model('consumer.preferred_contact_method_id'))).$('[value="number:1"]').click();
  scroll(app.element(by.model('kase.purpose_type_id'))).$('[value="number:1"]').click();
  scroll(app.element(by.model('kase.purpose_id'))).$('[value="number:1"]').click();
  scroll(app.element(by.model('finance.annual_income'))).sendKeys('1234567');
	scroll(app.element(by.id('application-bind-1'))).click()
  scroll(app.element(by.cssContainingText('a', 'Add a Beneficiary / Owner'))).click();
	module.exports.addBeneficiaryNew(app, true, true);
  scroll(app.element(by.id('application-illustration-provided-0'))).click();
  if (opts && opts.relationshipToAgent) {
    scroll(app.element(by.model('consumer.relationship_to_agent'))).sendKeys('none');
    scroll(app.element(by.model('consumer.years_of_agent_relationship'))).sendKeys('3');
  }
  return app;
};

module.exports.addBeneficiaryNew = (container, isOwner, isPayer) => {
  let section = container.element(by.css('[ng-controller="stakeholderRelationshipFormCtrl"]'));
  scroll(section.element(by.model('stakeholderRelationship.is_beneficiary'))).click();
  scroll(section.element(by.model('stakeholderRelationship.percentage'))).sendKeys('100');
  if (isOwner) scroll(section.element(by.model('stakeholderRelationship.is_owner'))).click();
  if (isPayer) scroll(section.element(by.model('stakeholderRelationship.is_payer'))).click();
	scroll(section.element(by.model('stakeholderRelationship.relationship_type_id'))).$('[value="number:1"]').click();
	scroll(section.element(by.model('contact.full_name'))).sendKeys('Alicia Pallacious');
  scroll(section.element(by.model('contact.gender'))).$('[value="string:false"]').click();
  scroll(section.element(by.model('contact.ssn'))).sendKeys('123456789');
  scroll(section.element(by.css('[id$="-birth_or_trust_date"]'))).sendKeys('01011999');
  return section;
};

/* This only does items _besides_ items handled by `helpers/health-info.js``
/* (particularly diseases) because they take so long to test. Use
/* `helpers/health-info.js` to test diseases and family diseases. */
module.exports.verifyUnderwriting = (container) => {
  /* Verify Basic Info */
  expect(element(by.css('a[editable-text="person.full_name"]')).getText()).toEqual('John Doe');
  let personalText = element(by.id('contact-personal')).getText();
  expect(personalText).toContain('foo@bar.baz');
  /* Verify Underwriting */
  element(by.id('health-trigger')).click();
  if (!container) container = element(by.id('health-underwriting'));
  expect(container.element(by.css('[id^=underwriting-bankruptcy-]:checked')).getAttribute('value')).toEqual('true');
  expect(container.element(by.css('[id$="-bankruptcy_declared"]')).getAttribute('value')).toEqual('03/22/1971');
  expect(container.element(by.css('[id$="-bankruptcy_discharged"]')).getAttribute('value')).toEqual('03/30/1971');
};
