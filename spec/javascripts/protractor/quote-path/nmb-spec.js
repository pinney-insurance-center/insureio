const helper = global.requireHelper('shared.js');
const quoteHelper = global.requireHelper('quote.js');

var originalTimeout;

beforeEach(function() {
  /*
  These specs were timing out.
  Reference
  https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  */
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
});

describe('Quote Path for NMB', () => {
  describe('New Quote: Use Application Process', () => {
    it('Reaches the ALQ pane', () => {
      console.log('\n>>> Starting quote-path-nmb: Reaches the ALQ pane');
      let originalBaseUrl = browser.baseUrl;
      browser.baseUrl = browser.baseUrl.replace(/pinney/, 'nmb');
      // Go to Quote Path
      let brandId = browser.params.nmbUser.brandId;
      let agentId = browser.params.nmbUser.id;
      let quoterKey = browser.params.nmbUser.quoter_key;
      let getPath = `/quoting/quotes/new?agent_id=${agentId}\&key=${quoterKey}\&brand_id=${brandId}\&productLine=1\&requestType=APPLICATION`;
      console.log(`Getting ${browser.baseUrl}${getPath}`);
      browser.get(getPath);
      /* Basic Info */
      let basicInfo = element(by.id('basic-info'));
      quoteHelper.fillBasicInfo(basicInfo, {
        duration_id: '2',
        health_class_id: undefined,
      });
      /* To ALQ */
      element(by.cssContainingText('button', 'Next: Discovery')).click();
      element(by.cssContainingText('button', 'Next: Underwriting')).click();
      element(by.cssContainingText('button', 'Next: View Quotes')).click();
      expect(element(by.id('ixn-agency-quoter')).isDisplayed()).toBeTruthy();
      /* Cleanup */
      browser.baseUrl = originalBaseUrl;
    });
  });
});
