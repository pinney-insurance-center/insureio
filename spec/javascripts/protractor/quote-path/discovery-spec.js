const helper = global.requireHelper('shared.js')
const scroll = helper.scroll;
const authHelper = global.requireHelper('auth.js');
const quoteHelper = global.requireHelper('quote.js');
const consumerMgmtHelper = global.requireHelper('consumer-mgmt.js');

var originalTimeout;

beforeEach(function() {
  /*
  These specs were timing out.
  Reference
  https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  */
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
});

describe('Quote Path', () => {
  describe('New Quote with Discovery', () => {
    it('Saves Discovery data', () => {
      console.log('\n>>> Starting Quote Path with Discovery');
      /* Login */
      if (!browser.params.isLoggedIn) authHelper.login();
      /* Go to Quote Path */
      browser.get('/quoting/quotes/new');
      element(by.cssContainingText('a', 'Use Application Process')).click();
      let nav = element(by.id('quote-footer-nav'));
      /* Choose field values */
      values = {
        netWorth: 1233567,
        occupation: 'astronaut',
        company: 'nosa',
        durationId: 23,
        duration: '35 Years',
      };
      /* Fill */
      quoteHelper.fillBasicInfo();
      nav.element(by.cssContainingText('button', 'Next: Discovery')).click();
      quoteHelper.fillDiscovery(values);
      nav.element(by.cssContainingText('button', 'Skip to Quotes')).click();
      /* Save */
      let bestResult = element(by.repeater('result in results.slice(0,1)'));
      scroll(bestResult.element(by.cssContainingText('a', 'Save'))).click();
      /* Verify */
      scroll(element(by.cssContainingText('a', 'Click here to view their record'))).click();
      var until = protractor.ExpectedConditions;
      browser.wait(until.urlContains('consumers/'), 2000);
      consumerMgmtHelper.verifyDiscovery(values);
    });
  });
});
