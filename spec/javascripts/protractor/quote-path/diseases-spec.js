const helper = global.requireHelper('shared.js')
const scroll = helper.scroll;
const authHelper = global.requireHelper('auth.js');
const quoteHelper = global.requireHelper('quote.js');
const consumerMgmtHelper = global.requireHelper('consumer-mgmt.js');
/* `helpers/health-info.js` is generated from the task rake underwriting:js:spec */
const healthInfoHelper = global.requireHelper('health-info.js')

var originalTimeout;

beforeEach(function() {
  /*
  These specs were timing out.
  Reference
  https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  */
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
});

describe('Quote Path', () => {
  describe('New Quote with diseases', () => {
    it('Saves all data', () => {
      console.log('\n>>> Starting quote-path: diseases');
      /* Login */
      if (!browser.params.isLoggedIn) authHelper.login();
      /* Go to Quote Path */
      browser.get('/quoting/quotes/new');
      /* Remove the downtime bulletin so that it doesn't intercept clicks intended for elements behind it. */
      let alertBulletin = element(by.css("body > div.wrapper > div.bulletin-container > div.alert-warning > button > span"));
      alertBulletin.isPresent().then(isPresent => isPresent && alertBulletin.click());
      element(by.cssContainingText('a', 'Use Application Process')).click();
      let nav = element(by.id('quote-footer-nav'));
      /* Ignore alerts to save time when entering health impairments */
      browser.executeScript(() => { window.alert = text => {} });
      /* Basic Info */
      quoteHelper.fillBasicInfo();
      /* Discovery */
      nav.element(by.cssContainingText('button', 'Next: Discovery')).click();
      /* Underwriting */
      nav.element(by.cssContainingText('button', 'Next: Underwriting')).click();
      underwritingContainer = quoteHelper.fillUnderwriting();
      healthInfoHelper.fillHealthHistory(underwritingContainer);
      healthInfoHelper.verifyHealthHistory(underwritingContainer);
      /* Go to Consumer page */
      scroll(element(by.cssContainingText('div.navigation button', 'Save Consumer'))).click();
      scroll(element(by.cssContainingText('a', 'Click here to view their record'))).click();
      element(by.id('health-trigger')).click();
      quoteHelper.verifyUnderwriting();
      healthInfoHelper.verifyHealthHistory();
    });
  });
});
