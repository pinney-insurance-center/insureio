const helper = global.requireHelper('shared.js')
const scroll = helper.scroll;
const authHelper = global.requireHelper('auth.js');
const quoteHelper = global.requireHelper('quote.js');

var originalTimeout;

beforeEach(function() {
  /*
  These specs were timing out.
  Reference
  https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  */
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
});

describe('Quote Path', () => {
  describe('New Quote: Use Application Process', () => {
    it('Reach the end of the Quote Path', () => {
      console.log('\n>>> Starting quote-path: Reach the end of the Quote Path');
      if (!browser.params.isLoggedIn) authHelper.login();
      // Go to Quote Path
      element(by.css('a[ng-href="/quoting/quotes/new"]')).click();
      let nav = element(by.id('quote-footer-nav'));
      // Basic Info
      element(by.cssContainingText('a', 'Use Application Process')).click();
      quoteHelper.fillBasicInfo();
      // Discovery
      nav.element(by.cssContainingText('button', 'Next: Discovery')).click();
      scroll(element(by.model('finance.net_worth_specified'))).sendKeys('2345678');
      // Underwriting
      nav.element(by.cssContainingText('button', 'Next: Underwriting')).click();
      // Quote Details
      nav.element(by.cssContainingText('button', 'Next: View Quotes')).click();
      let bestResult = element(by.repeater('result in results.slice(0,1)'));
      var until = protractor.ExpectedConditions;
      browser.wait(until.presenceOf(bestResult), 60*1000, 'Element taking too long to appear in the DOM');
      expect(bestResult.isPresent()).toBeTruthy()
      expect(bestResult.element(by.cssContainingText('a', 'Apply')).isPresent()).toBeTruthy()
      helper.scroll(bestResult.element(by.cssContainingText('a', 'Apply'))).click();
      // Application
      quoteHelper.fillApplication({relationshipToAgent: true});
      helper.scroll(element(by.cssContainingText('button', 'Next: Submit For Processing'))).click();
      // End of path
      let thankYou = element(by.cssContainingText('h3', 'Thank You!'));
      expect(thankYou.isPresent()).toBeTruthy();
      /* Go to Consumer page to review case */
      scroll(element(by.cssContainingText('a', 'Click here to view or edit their information.'))).click();
      browser.wait(until.urlContains('consumers/'), 2000);
      let detailsRow = element(by.id('cases-panel')).element(by.css('.details-row:first-child'));
      browser.wait(until.presenceOf(detailsRow), 60*1000, 'Element taking too long to appear in the DOM');
      expect(detailsRow.getText()).toMatch(/Pending\s+-\s+Submit to App Team/);
      scroll(detailsRow).click();
      let openDetails = element(by.css('.open-details-section'));
      expect(openDetails.getText()).toMatch(/Coverage Amount\s+\$1,234,567.00/);
    });
  });
});
