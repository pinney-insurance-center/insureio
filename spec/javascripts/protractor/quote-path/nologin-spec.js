const helper = global.requireHelper('shared.js')
const authHelper = global.requireHelper('auth.js');
const quoteHelper = global.requireHelper('quote.js');

var originalTimeout;

beforeEach(function() {
  /*
  These specs were timing out.
  Reference
  https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  */
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 2 * 60 * 1000;
});

describe('Quote Path with `quoter_key` instead of login', () => {
  describe('New Quote: Use Application Process', () => {
    it('Reach the end of the Quote Path', () => {
      console.log('\n>>> Starting quote-path-nologin: Reach the end of the Quote Path');
      if (browser.params.isLoggedIn) authHelper.logout();
      // Go to Quote Path
      let brandId = browser.params.login.brandId;
      let agentId = browser.params.login.id;
      let quoterKey = browser.params.login.quoter_key;
      let getPath = `/quoting/quotes/new?agent_id=${agentId}\&key=${quoterKey}\&brand_id=${brandId}\&productLine=1\&requestType=APPLICATION`;
      console.log(`Getting ${browser.baseUrl}${getPath}`);
      browser.get(getPath);
      // Basic Info
      quoteHelper.fillBasicInfo();
      // Discovery
      element(by.cssContainingText('button', 'Next: Discovery')).click();
      quoteHelper.fillDiscovery({});
      // Underwriting
      element(by.cssContainingText('button', 'Next: Underwriting')).click();
      // Quote Details
      element(by.cssContainingText('button', 'Next: View Quotes')).click();
      let bestResult = element(by.repeater('result in results.slice(0,1)'));
      var until = protractor.ExpectedConditions;
      browser.wait(until.presenceOf(bestResult), 60*1000, 'Element taking too long to appear in the DOM');
      expect(bestResult.isPresent()).toBeTruthy()
      expect(bestResult.element(by.cssContainingText('a', 'Apply')).isPresent()).toBeTruthy()
      helper.scroll(bestResult.element(by.cssContainingText('a', 'Apply'))).click();
      // Application
      expect(element(by.cssContainingText('div.navigation button', 'Next: Send My Request')).isPresent()).toBeTruthy();
      quoteHelper.fillApplication();
      helper.scroll(element(by.cssContainingText('button', 'Next: Send My Request'))).click();
      // End of path
      expect(element(by.cssContainingText('h3', 'Congratulations!')).isDisplayed()).toBeTruthy();
    });
  })
});
