describe('BrandMgmtCtrl', () => {
  var $controller, $scope, $httpMock;

  beforeEach(module('dr.controllers', 'dr.brands', 'DrBulletins'));

  beforeEach(inject((_$controller_, _$rootScope_, _$resource_, _$httpBackend_, _$location_, _UserModel_, _requestSanitizer_, _bulletinService_) => {
    /* Mock HTTP server */
    $httpMock = _$httpBackend_;
    $httpMock.whenGET('brands.json').respond('[]');
    /* Prepare controller's scope */
    $scope = _$rootScope_.$new();
    helpers.setRootScopeMembers($scope);
    $scope.currentUser = $scope.getCurrentUser();
    /* Instantiate controller */
    $controller = _$controller_('BrandMgmtCtrl', { $scope: $scope, $resource: _$resource_, $location: _$location_, UserModel: _UserModel_, requestSanitizer: _requestSanitizer_, bulletinService: _bulletinService_ });
    $scope.init();
  }));

  describe('getBrands', () => {
    it('does not raise Error', () => $scope.getBrands() );
  });
  describe('getProfileNamesForMenu', () => {
    it('does not raise Error', () => $scope.getProfileNamesForMenu('search string') );
  });
  describe('getCurrentDefaultProfile', () => {
    it('does not raise Error', () => $scope.getCurrentDefaultProfile() );
  });
  describe('resetDefaultProfileMenu', () => {
    it('does not raise Error', () => $scope.resetDefaultProfileMenu() );
  });
  describe('editProfile', () => {
    it('does not raise Error', () => $scope.editProfile({}) );
  });
  describe('submitProfileForm', () => {
    it('does not raise Error', () => {
      $scope.activeProfile = {};
      $scope.submitProfileForm()
    });
  });
  describe('removeProfile', () => {
    it('does not raise Error', () => $scope.removeProfile() );
  });
});
