describe('ConsumerManagementCtrl', () => {
  var $controller, $scope;

  beforeEach(module('ioConstants', 'ConsumerManagementApp', 'PolicyMgmtApp', 'DrFilters', 'DrModels', 'ioRequestSanitizer'));

  beforeEach(inject((_$controller_, _$rootScope_, _$location_, _$httpBackend_) => {
    $scope = _$rootScope_.$new();
    $scope.consumerMgmtVars = {};
    $scope.person = helpers.mockPerson();
    _$location_.$$path = '/consumers/500';
    helpers.setRootScopeMembers($scope);
    $controller = _$controller_('ConsumerManagementCtrl', { $scope: $scope });
  }));

  describe('init', () => {
    it('runs without Error', () => $scope.init() );
  });
  describe('setConsumerMgmtPendingReqWatcher', () => {
    it('runs without Error', () => $scope.setConsumerMgmtPendingReqWatcher() );
  });
  describe('setActiveTab', () => {
    it('runs without Error', () => $scope.setActiveTab() );
  });
  describe('setTemplateUrlForActiveTab', () => {
    it('runs without Error', () => $scope.setTemplateUrlForActiveTab() );
  });
  describe('getConsumer', () => {
    it('runs without Error', () => $scope.getConsumer() );
  });
  describe('updateRecentConsumers', () => {
    it('runs without Error', () => $scope.updateRecentConsumers() );
  });
  describe('refreshSpouse', () => {
    it('runs without Error', () => $scope.refreshSpouse() );
  });
  describe('policyList', () => {
    it('runs without Error', () => $scope.policyList() );
  });
  describe('openContactMethodsModal', () => {
    it('runs without Error', () => $scope.openContactMethodsModal() );
  });
  describe('openEmploymentInfoModal', () => {
    it('runs without Error', () => $scope.openEmploymentInfoModal() );
  });
  describe('openPersonalInfoModal', () => {
    it('runs without Error', () => $scope.openPersonalInfoModal() );
  });
  describe('openTrackingDataModal', () => {
    it('runs without Error', () => $scope.openTrackingDataModal() );
  });
});

describe('employmentInfoModalCtrl', () => {
  it('loads without Error', () => {
    module('ConsumerManagementApp');
    inject((_$controller_, _$rootScope_) => {
      const $scope = _$rootScope_.$new();
      _$controller_('employmentInfoModalCtrl', { $scope: $scope });
    });
  });
});

describe('personalInfoModalCtrl', () => {
  it('loads without Error', () => {
    module('ConsumerManagementApp');
    inject((_$controller_, _$rootScope_) => {
      const $scope = _$rootScope_.$new();
      _$controller_('personalInfoModalCtrl', { $scope: $scope });
    });
  });
});

describe('trackingDataModalCtrl', () => {
  var $controller, $scope;

  beforeEach(() => {
    module('ConsumerManagementApp', 'PolicyMgmtApp', 'DrFilters');
    inject((_$controller_, _$rootScope_, _$location_, _$httpBackend_) => {
      $scope = _$rootScope_.$new();
      $scope.person = helpers.mockPerson({ brand_id: 1 });
      $scope.currentUser = helpers.mockSuperUser();
      helpers.setRootScopeMembers($scope);
      $controller = _$controller_('trackingDataModalCtrl', { $scope: $scope });
    });
  });

  describe('getBrands', () => {
    it('runs without Error', () => $scope.getBrands() );
  });
  describe('confirmAndUpdate', () => {
    it('runs without Error', () => $scope.confirmAndUpdate() );
  });
});
