describe('QuotePathCtrl', () => {

  helpers.stubAndLoadModules([
    'ui.bootstrap.datetimepicker',
    'rzModule',
    'detectChrome',
    'SocketClientApp',
    'DashboardApp',
    'SearchApp',
  ]);

  helpers.loadModules([
    'xeditable',
    'newQuoteApp',
    'DrBulletins',
    'ioConstants',
    'insureioNg',
  ]);


  var CONSUMER_ID, CASE_ID, QUOTE_ID;
  var $controller, $rootScope, $scope, $http, $httpBackend, $location;

  beforeEach(inject((_$controller_, _$rootScope_, _$httpBackend_, _$http_, _$location_, $injector) => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    $location = _$location_;
    $scope = _$rootScope_.$new();

    /* Stub current user */
    $httpBackend.when('GET', '/users/0.json')
    .respond(200, JSON.stringify({
      id: 1,
      permissions_0: Math.pow(2,32),
      permissions_1: Math.pow(2,32),
      permissions_2: Math.pow(2,32),
      permissions_3: Math.pow(2,32),
      permissions_4: Math.pow(2,32),
    }));

    /* Stub consumer */
    $location.search('consumer_id', CONSUMER_ID);
    $httpBackend.when('GET', '/consumers/'+CONSUMER_ID+'.json')
    .respond(200, JSON.stringify({id: CONSUMER_ID, cases_attributes: [$scope.case]}));
    /* Stub existing coverage */
    $httpBackend.when('GET', '/consumers/'+CONSUMER_ID+'/existing_coverage.json')
    .respond(200, JSON.stringify([]));
    /* Stub case */
    if (CASE_ID) {
      $location.search('kase_id', CASE_ID);
      $httpBackend.when('GET', '/crm/cases/'+CASE_ID+'.json')
      .respond(200, JSON.stringify({id: CASE_ID}));
    }
    /* Stub saved quote */
    if (QUOTE_ID) {
      $location.search('quote_id', QUOTE_ID);
      $httpBackend.when('GET', '/quoting/quotes/'+QUOTE_ID+'.json')
      .respond(200, JSON.stringify({id: QUOTE_ID}));
    }
    /* Stub brands */
    $httpBackend.when('GET', '/brands/autocomplete.json')
    .respond(200, JSON.stringify([{"id":3,"name":"Pinney Brokerage","ownership_id":3},{"id":29,"name":"Affinity Marketing","ownership_id":3},{"id":30,"name":"In House Generation","ownership_id":3},{"id":44,"name":"Purchased","ownership_id":3},{"id":49,"name":"Lifeline Direct","ownership_id":3},{"id":50,"name":"RootFin","ownership_id":2}]));

    /* Initialize controller */
    $controller = _$controller_('QuotePathCtrl', {
      $scope: $scope,
      $http: $http,
      $location: $location,
      bulletinService: $injector.get('bulletinService'),
    });
  }));

  describe('$scope.viewRates', () => {
    describe('when requoting', () => {
      CONSUMER_ID = 11;
      CASE_ID = 22;
      QUOTE_ID = 33;

      it("doesn't POST the case id but does retain it in scope", () => {
        /* Stub quote results */
        $httpBackend.expect('POST', '/quoting/get_quotes.json')
        .respond((method, url, data, headers, params) => {
          data = JSON.parse(data);
          expect(data.consumer.cases_attributes[0].id).toBeUndefined();
          return [200, JSON.stringify([])];
        });

        spyOn($scope, 'canViewRates').and.returnValue(true);

        /* Run functions */
        $scope.viewRates();
        $httpBackend.flush();
        expect($scope.kase.id).toEqual(22);
      });
    });
  });
});
