describe('NmbUtil', () => {
  let yearsAgo;
  let NmbUtil, Util;
  const consumer = { health_info_attributes: {}, financial_info_attributes: {} };
  const health = consumer.health_info_attributes;
  const quote = Object.freeze({
    duration_id: 2, /* 15 Year Term: one of the types allowed for IXN's NMB quotes */
  });
  const RATING_SUPER = 'Preferred Plus';
  const RATING_PREFERRED = 'Preferred';
  const RATING_STD = 'Standard';
  const RATING_STD_TOBACCO = 'Standard';
  const RATING_T1 = 'Table A';

  let mockResourceCallback = () => ({$promise: Promise.resolve()});

  /* Mock some data to be returned by /crm/cases/:id/involved_parties.json */
  const involvedParties = {"agent_id":10,"agent_of_record_id":10,"agent_name":"bru cam","agent_of_record_name":"bru cam","stakeholder_relationships":[{"id":141,"primary_insured_id":500,"stakeholder_id":530,"relationship_type_id":3,"crm_case_id":158,"is_owner":false,"is_payer":false,"is_beneficiary":true,"percentage":35,"contingent":false,"note":null,"created_at":"2020-05-28T17:30:51.000Z","updated_at":"2020-05-28T17:30:51.000Z","stakeholder":{"id":530,"agent_id":10,"birth_state_id":null,"birth_country_custom_name":null,"citizenship_id":1,"citizenship_noncitizen_registration_number":null,"dl_expiration":null,"dl_state_id":null,"email_send_failed":null,"ip_address":null,"quoter_url":null,"marital_status_id":null,"note":null,"actively_employed":null,"occupation":null,"occupation_description":null,"brand_id":21264,"product_type_id":null,"suffix":null,"years_at_address":null,"created_at":"2020-05-28T17:30:51.000Z","updated_at":"2020-05-28T17:30:51.000Z","health_info_id":null,"financial_info_id":null,"anniversary":null,"relationship_to_agent":null,"relationship_to_agent_start":null,"completed_discovery":null,"completed_financial":null,"completed_underwriting":null,"health_completed":false,"personal_completed":false,"marketing_unsubscribe":false,"completed_basic":null,"completed_compare":null,"completed_application":null,"case_manager_name":null,"tags":[],"lead_type":null,"referrer":null,"source":null,"physician_name":null,"test_lead":false,"product_cat_id":null,"primary_contact_relationship_id":null,"status_unsubscribe":null,"recently_applied_for_life_ins":null,"total_life_coverage_in_force":null,"last_applied_for_life_ins_outcome_id":null,"last_contacted":null,"last_applied_for_life_ins_date":null,"encrypted_eft_account_num":null,"encrypted_eft_account_num_iv":null,"encrypted_eft_routing_num":null,"encrypted_eft_routing_num_iv":null,"encrypted_eft_bank_name":null,"encrypted_eft_bank_name_iv":null,"full_name":"sally ride","title":null,"company":null,"preferred_contact_method_id":null,"preferred_contact_time":null,"primary_address_id":null,"primary_phone_id":null,"primary_email_id":null,"birth_or_trust_date":null,"active_or_enabled":true,"entity_type_id":1,"tenant_id":0,"gender":false,"trustee_name":null,"unscoped_attachments_count":null,"attachments_count":null,"recordings_count":null,"opportunities_count":null,"pending_policies_count":null,"placed_policies_count":null,"old_tasks_count":null,"birth_country_id":null,"relationships_w_self_as_primary_count":0,"relationships_w_self_as_related_count":1,"primary_contact_id":null,"ssn":"123-45-6789","has_opps_or_policies":false,"phones":[],"emails":[],"addresses":[]}},{"id":142,"primary_insured_id":500,"stakeholder_id":531,"relationship_type_id":6,"crm_case_id":158,"is_owner":true,"is_payer":true,"is_beneficiary":true,"percentage":65,"contingent":false,"note":null,"created_at":"2020-05-28T17:31:27.000Z","updated_at":"2020-05-28T17:31:27.000Z","stakeholder":{"id":531,"agent_id":10,"birth_state_id":null,"birth_country_custom_name":null,"citizenship_id":1,"citizenship_noncitizen_registration_number":null,"dl_expiration":null,"dl_state_id":null,"email_send_failed":null,"ip_address":null,"quoter_url":null,"marital_status_id":null,"note":null,"actively_employed":null,"occupation":null,"occupation_description":null,"brand_id":21264,"product_type_id":null,"suffix":null,"years_at_address":null,"created_at":"2020-05-28T17:31:27.000Z","updated_at":"2020-05-28T17:31:27.000Z","health_info_id":null,"financial_info_id":null,"anniversary":null,"relationship_to_agent":null,"relationship_to_agent_start":null,"completed_discovery":null,"completed_financial":null,"completed_underwriting":null,"health_completed":false,"personal_completed":false,"marketing_unsubscribe":false,"completed_basic":null,"completed_compare":null,"completed_application":null,"case_manager_name":null,"tags":[],"lead_type":null,"referrer":null,"source":null,"physician_name":null,"test_lead":false,"product_cat_id":null,"primary_contact_relationship_id":null,"status_unsubscribe":null,"recently_applied_for_life_ins":null,"total_life_coverage_in_force":null,"last_applied_for_life_ins_outcome_id":null,"last_contacted":null,"last_applied_for_life_ins_date":null,"encrypted_eft_account_num":null,"encrypted_eft_account_num_iv":null,"encrypted_eft_routing_num":null,"encrypted_eft_routing_num_iv":null,"encrypted_eft_bank_name":null,"encrypted_eft_bank_name_iv":null,"full_name":"eddie van halen","title":null,"company":null,"preferred_contact_method_id":null,"preferred_contact_time":null,"primary_address_id":null,"primary_phone_id":null,"primary_email_id":null,"birth_or_trust_date":null,"active_or_enabled":true,"entity_type_id":1,"tenant_id":0,"gender":true,"trustee_name":null,"unscoped_attachments_count":null,"attachments_count":null,"recordings_count":null,"opportunities_count":null,"pending_policies_count":null,"placed_policies_count":null,"old_tasks_count":null,"birth_country_id":null,"relationships_w_self_as_primary_count":0,"relationships_w_self_as_related_count":1,"primary_contact_id":null,"ssn":"987-65-4321","has_opps_or_policies":false,"phones":[],"emails":[],"addresses":[]}},{"id":143,"primary_insured_id":500,"stakeholder_id":532,"relationship_type_id":1,"crm_case_id":158,"is_owner":false,"is_payer":false,"is_beneficiary":true,"percentage":100,"contingent":true,"note":null,"created_at":"2020-05-28T17:31:50.000Z","updated_at":"2020-05-28T17:31:50.000Z","stakeholder":{"id":532,"agent_id":10,"birth_state_id":null,"birth_country_custom_name":null,"citizenship_id":1,"citizenship_noncitizen_registration_number":null,"dl_expiration":null,"dl_state_id":null,"email_send_failed":null,"ip_address":null,"quoter_url":null,"marital_status_id":null,"note":null,"actively_employed":null,"occupation":null,"occupation_description":null,"brand_id":21264,"product_type_id":null,"suffix":null,"years_at_address":null,"created_at":"2020-05-28T17:31:50.000Z","updated_at":"2020-05-28T17:31:50.000Z","health_info_id":null,"financial_info_id":null,"anniversary":null,"relationship_to_agent":null,"relationship_to_agent_start":null,"completed_discovery":null,"completed_financial":null,"completed_underwriting":null,"health_completed":false,"personal_completed":false,"marketing_unsubscribe":false,"completed_basic":null,"completed_compare":null,"completed_application":null,"case_manager_name":null,"tags":[],"lead_type":null,"referrer":null,"source":null,"physician_name":null,"test_lead":false,"product_cat_id":null,"primary_contact_relationship_id":null,"status_unsubscribe":null,"recently_applied_for_life_ins":null,"total_life_coverage_in_force":null,"last_applied_for_life_ins_outcome_id":null,"last_contacted":null,"last_applied_for_life_ins_date":null,"encrypted_eft_account_num":null,"encrypted_eft_account_num_iv":null,"encrypted_eft_routing_num":null,"encrypted_eft_routing_num_iv":null,"encrypted_eft_bank_name":null,"encrypted_eft_bank_name_iv":null,"full_name":"mama bear","title":null,"company":null,"preferred_contact_method_id":null,"preferred_contact_time":null,"primary_address_id":null,"primary_phone_id":null,"primary_email_id":null,"birth_or_trust_date":null,"active_or_enabled":true,"entity_type_id":1,"tenant_id":0,"gender":null,"trustee_name":null,"unscoped_attachments_count":null,"attachments_count":null,"recordings_count":null,"opportunities_count":null,"pending_policies_count":null,"placed_policies_count":null,"old_tasks_count":null,"birth_country_id":null,"relationships_w_self_as_primary_count":0,"relationships_w_self_as_related_count":1,"primary_contact_id":null,"ssn":null,"has_opps_or_policies":false,"phones":[],"emails":[],"addresses":[]}}]};

  /* Set constants (because constants.js.erb isn't sourced) */
  window.IXN_QUOTER_CONFIG_ID = 'dummy';

  function sortNum (a, b) { return a - b }

  /* Set all fields on health to values that will allow an NMB rate class of SUPER */
  function setSuper () {
    consumer.$resolved = true;
    consumer.gender = 1;
    consumer.citizenship_id = 1;
    consumer.birth_or_trust_date = yearsAgo(40);
    health.tobacco = true;
    health.cigarette_last = yearsAgo(3.5);
    delete health.tobacco_nicotine_patch_or_gum_current
    health.cholesterol = true;
    health.cholesterol_level = 240;
    health.cholesterol_hdl = 5;
    health.bp = true;
    health.bp_systolic = 140;
    health.bp_diastolic = 85;
    health.diseases = {
      alcohol_abuse: { treatment_end_date: yearsAgo(11),  },
      basal_cell_carcinoma: { true: true },
    };
    health.relatives_diseases = [];
    health.moving_violations = [{date: yearsAgo(8), dui_dwi: true}];
    health.crimes = [{date: yearsAgo(11), felony: true}]
    health.weight = 160;
    health.feet = 6;
    health.inches = 0;
  }

  /* Load modules & add mocks */
  beforeEach(module('ioConstants', 'io.util', 'io.nmbUtil',
  ($provide, $filterProvider) => {
    $provide.value('$filter', $filterProvider);
    $provide.value('$resource', () => ({ get: mockResourceCallback }));
  }));

  /* Set locals from injector */
  beforeEach(inject((_Util_, _NmbUtil_) => {
    Util = _Util_;
    NmbUtil = _NmbUtil_;
    yearsAgo = _Util_.yearsAgo;
    setSuper();
  }));

  describe('health class calculation from NmbUtil.Alq', () => {
    /* Define helper function */
    function calculate (alq) {
      if (!alq) alq = new NmbUtil.Alq(consumer, null, quote);
      let hcs = alq.config().quote_data.health_categories;
      if (hcs.length > 1 && hcs[0] != 'Table A')
        throw new Error(`Unexpected values in health_categories ${hcs}`);
      return hcs[0];
    }

    it('enforces required fields', () => {
      /* Check with all fields set */
      setSuper();
      expect(calculate()).toEqual(RATING_SUPER);
      /* Remove necessary fields. Should throw Strings */
      setSuper(); consumer.$resolved = false;
      expect(() => { calculate() }).toThrow(new String('Awaiting records. Please try again in 2 seconds.'));
      setSuper(); consumer.citizenship_id += 1;
      expect(() => { calculate() }).toThrow(new String('Products from this carrier are available only to U.S. citizens.'));
      setSuper(); delete consumer.gender;
      expect(() => { calculate() }).toThrow(new String('Gender must be set'));
      setSuper(); consumer.birth_or_trust_date = '44/22/1113';
      expect(() => { calculate() }).toThrow(new String('Birth must be a date.'));
      setSuper(); delete health.feet;
      expect(() => { calculate() }).toThrow(new String('Height (feet) must be set'));
      setSuper(); delete health.weight;
      expect(() => { calculate() }).toThrow(new String('Weight must be set'));
      /* Remove optional fields. (Should have no errors.) */
      setSuper();
      delete health.bp_diastolic; expect(calculate()).toEqual(RATING_SUPER);
      delete health.bp_systolic; expect(calculate()).toEqual(RATING_SUPER);
      delete health.cholesterol; expect(calculate()).toEqual(RATING_SUPER);
      delete health.cholesterol_hdl; expect(calculate()).toEqual(RATING_SUPER);
      delete health.cigarette_last; expect(calculate()).toEqual(RATING_SUPER);
      delete health.tobacco; expect(calculate()).toEqual(RATING_SUPER);
      health.diseases = {}; expect(calculate()).toEqual(RATING_SUPER);
      health.relatives_diseases = []; expect(calculate()).toEqual(RATING_SUPER);
      health.moving_violations = []; expect(calculate()).toEqual(RATING_SUPER);
      health.crimes = []; expect(calculate()).toEqual(RATING_SUPER);
    });
    it('computes SUPER for super criteria', () => {
      setSuper();
      expect(calculate()).toEqual(RATING_SUPER);
    });
    it('computes worse ratings for worse criteria', () => {
      // tobacco
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.cigarette_last = yearsAgo(2.5);
      expect(calculate()).toEqual(RATING_PREFERRED);
      health.tobacco = false;
      expect(calculate()).toEqual(RATING_SUPER);
      // isTobacco vs table-ratings
      let alq = new NmbUtil.Alq(consumer, null, quote);
      setSuper(); expect(calculate(alq)).toEqual(RATING_SUPER);
      expect(alq.isTobacco).toEqual(false);
      health.tobacco_nicotine_patch_or_gum_current = true;
      expect(calculate(alq)).toEqual(RATING_STD);
      expect(alq.klass).toEqual(3);
      expect(alq.isTobacco).toEqual(true);
      health.cholesterol_level = 301;
      expect(calculate(alq)).toEqual(RATING_T1);
      expect(alq.isTobacco).toEqual(true); /* The klass may not be STD_TOBACCO, but `isTobacco` is still set */
      // cholesterol
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.cholesterol_level = 241;
      expect(calculate()).toEqual(RATING_PREFERRED);
      health.cholesterol = false;
      expect(calculate()).toEqual(RATING_SUPER);
      // cholesterol hdl ratio
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.cholesterol_hdl = 5.1;
      expect(calculate()).toEqual(RATING_PREFERRED);
      // bp age 64-
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.bp_systolic = 141;
      health.bp_diastolic = 85;
      expect(calculate()).toEqual(RATING_PREFERRED);
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.bp_systolic = 140;
      health.bp_diastolic = 86;
      expect(calculate()).toEqual(RATING_PREFERRED);
      // bp age 65+
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      consumer.birth_or_trust_date = yearsAgo(65)
      health.bp_systolic = 146;
      health.bp_diastolic = 90;
      expect(calculate()).toEqual(RATING_PREFERRED);
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      consumer.birth_or_trust_date = yearsAgo(65)
      health.bp_systolic = 145;
      health.bp_diastolic = 91;
      expect(calculate()).toEqual(RATING_T1); /* Diastolic above 90 exceeds all health classes */
      // family history
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.relatives_diseases.push({death_age: 13});
      expect(calculate()).toEqual(RATING_STD);
      health.relatives_diseases.push({death_age: 14});
      expect(calculate()).toEqual(RATING_T1);
      // personal history
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.diseases.other_internal_cancer = {true: true};
      expect(calculate()).toEqual(RATING_STD);
      // driving history
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.moving_violations = [{date: yearsAgo(1)},{date: yearsAgo(2)},{date: yearsAgo(2.5)}];
      expect(calculate()).toEqual(RATING_STD);
      health.moving_violations.push({date: yearsAgo(1)});
      expect(calculate()).toEqual(RATING_T1);
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.moving_violations = [{date: yearsAgo(6), dui_dwi: true}];
      expect(calculate()).toEqual(RATING_PREFERRED);
      // substance abuse
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.diseases.alcohol_abuse.treatment_end_date = yearsAgo(9);
      expect(calculate()).toEqual(RATING_PREFERRED);
      // criminal record
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.crimes = [{date: yearsAgo(9), felony: true}];
      expect(calculate()).toEqual(RATING_STD);
      // build
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      health.weight = 216;
      expect(calculate()).toEqual(RATING_PREFERRED);
      setSuper(); expect(calculate()).toEqual(RATING_SUPER);
      consumer.birth_or_trust_date = yearsAgo(66);
      health.weight = 136;
      expect(calculate()).toEqual(RATING_T1);
    });
  });

  /* NB: Don't test `Alq.load` b/c that pulls IXN scripts into karma's
  /* browser, and IXN's buggy scripts raise errors which karma identifies as
  /* failures. */
  describe('NmbUtil.Alq.config()', () => {
    it('does not raise Error', () => {
      consumer.$primaryAddressAttributes = {state_id: 3}
      health.bp_last_treatment = Util.yearsAgo(-1);
      health.diseases.alcohol_abuse = { currently_treating: true};
      let config = new NmbUtil.Alq(consumer, null, quote).config();
      expect(config.quote_data.carrier_ids).toEqual([1800]);
    });
    it('does not raise Error when case exists', () => {
      let config;
      mockResourceCallback = () => (Object.assign({$promise: Promise.resolve()}, involvedParties));
      function requireBeneficiary(index, label) {
        return expect(config.quote_params.find(obj => (
          obj.name == `${label}_beneficiary_${index}_first_name` && !!obj.value
        )));
      }
      consumer.$primaryAddressAttributes = {state_id: 3}
      health.bp_last_treatment = Util.yearsAgo(-1);
      health.diseases.alcohol_abuse = { currently_treating: true};
      let kase = { id: 1 };
      config = new NmbUtil.Alq(consumer, kase, quote).config();
      expect(config.quote_data.carrier_ids).toEqual([1800]);
      requireBeneficiary(1, 'primary').not.toBeUndefined();
      requireBeneficiary(2, 'primary').not.toBeUndefined();
      requireBeneficiary(3, 'primary').toBeUndefined();
      requireBeneficiary(1, 'contingent').not.toBeUndefined();
      requireBeneficiary(2, 'contingent').toBeUndefined();
    });
  });
});
