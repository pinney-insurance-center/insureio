describe('CsvImportCtrl', () => {

	helpers.stubAndLoadModules([
		'ui.select',
		'ui.mask',
		'ui.bootstrap.datetimepicker',
		'ui.tinymce',
		'rzModule',
		'detectChrome',
		"dr.directives",
		'SocketClientApp',
		'ContactsMgmtApp',
		"WidgetApp",
		"dr.consumer",
		"dr.marketing",
		"dr.users",
		'DashboardApp',
		'noteApp',
		'SearchApp',
		'TasksApp',
		'DrStatusTypeMgmtApp',
	]);
	helpers.stubProviders([
		'editableOptions',
		'editableThemes',
	]);
	helpers.loadModules([
		'ioConstants',
		'insureioNg',
		'ioRequestSanitizer',
		'DrBulletins',
		"dr.controllers",
		'dr.csvImport',
		'xeditable',
	]);

	var $controller, $scope;

	beforeEach(inject((_$controller_, _$rootScope_, _$upload_, _$httpBackend_) => {
		var $rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		var $http = _$httpBackend_;
		$http.when('GET', '/users/0.json').respond(200, {});
		$controller = _$controller_('CsvImportCtrl', { $scope: $scope, $upload: _$upload_, $http: $http });
		jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000;
	}));

	describe('$scope.readAndParseFile', () => {
		it('sets error when the mime type is bad', () => {
			let csvBlob = new Blob([CSV_DATA_GOOD], {type: 'application/json'});
			csvBlob.name = 'foo.csv';
			$scope.stateVars.csvFile = [csvBlob];
			$scope.readAndParseFile(); // Run the code from the actual angular controller
			expect($scope.stateVars.errors).toBeDefined();
			expect($scope.stateVars.errors[0].message).toMatch(/This does not appear to be a CSV file/);
		});
	});

	describe('$scope.csvFileReadCallback', () => {
		it('sets an error when csvFile is blank', () => {
			return testCsvSubmission(() => {
				expect($scope.stateVars.errors).toBeDefined();
				expect($scope.stateVars.errors.map(errMsgs)).toContain('Cannot process CSV with no data rows');
			}, '');
		});
		it('sets error when csvFile contains Unicode', async () => {
			return testCsvSubmission(() => {
				expect($scope.stateVars.errors).toBeDefined();
				expect($scope.stateVars.errors.map(errMsgs)).toContain('The CSV file contains disallowed characters starting at char 210. Please restrict your inputs to ASCII characters.');
			}, CSV_DATA_W_UNICODE);
		});
		it('sets error when agent_id has non-decimal chars', async () => {
			return testCsvSubmission(() => {
				expect($scope.stateVars.errors).toBeDefined();
				expect($scope.stateVars.errors.map(errMsgs)).toContain('Key agent_id must be numeric and nonzero and a real id.');
			}, CSV_DATA_BAD_AGENT_ID);
		});
		it('sets no error when csvFile is valid', async () => {
			return testCsvSubmission(() => {
				expect($scope.stateVars.errors).not.toBeDefined();
			}, CSV_DATA_GOOD);
		});
	});

	function errMsgs (value) { return value.message }
	function testCsvSubmission (testFunction, csvData) {
		let csvBlob = new Blob([csvData], {type: 'text/csv'});
		csvBlob.name = 'foo.csv';
		fr = new FileReader();
		var promise = new Promise((resolve, reject) => {
			fr.onload = function () {
				$scope.csvFileReadCallback(fr); // Run the code from the actual angular controller
				testFunction(); // Run our test expectations
				resolve(); // Let jasmine (test framework) know it's time to terminate
			}
			fr.readAsText(csvBlob, "UTF-8");
		});
		return promise;
	}
});

const CSV_DATA_GOOD = 'agent_id,brand_id,full_name,gender,birth,ssn,dln,dl_state_id,marital_status_id,address1.street,address1.city,address1.state_id,address1.zip,phone1.phone_type_id,phone1.value,phone1.ext,email1.value\n\
9,13,Chris Redfield,Male,6/5/1976,7897897890,S23609,13,1,123 Fake St,Detroit Rock City,16,12345,1,,123,foo@bar.com\n\
2,1,Michael Jackson,Male,8/9/1972,1232344567,D789789,9,2,311 Center St,Fargo,36,45678,2,8025551234,,test@gmail.com';
const CSV_DATA_BAD_AGENT_ID = 'agent_id,brand_id,full_name,gender,birth,ssn,dln,dl_state_id,marital_status_id,address1.street,address1.city,address1.state_id,address1.zip,phone1.phone_type_id,phone1.value,phone1.ext,email1.value\n\
S9,13,Chris Redfield,Male,6/5/1976,7897897890,S23609,13,1,123 Fake St,Detroit Rock City,16,12345,1,,123,foo@bar.com\n\
2,1,Michael Jackson,Male,8/9/1972,1232344567,D789789,9,2,311 Center St,Fargo,36,45678,2,8025551234,,test@gmail.com';
const CSV_DATA_W_UNICODE = 'agent_id,brand_id,full_name,gender,birth,ssn,dln,dl_state_id,marital_status_id,address1.street,address1.city,address1.state_id,address1.zip,phone1.phone_type_id,phone1.value,phone1.ext,email1.value\n\
S9,13,Chris ハッピーRedfield,Male,6/5/1976,7897897890,S23609,13,1,123 Fake St,Detroit Rock City,16,12345,1,,123,foo@bar.com\n\
2,1,Michael Jackson,Male,8/9/1972,1232344567,D789789,9,2,311 Center St,Fargo,36,45678,2,8025551234,,test@gmail.com';
