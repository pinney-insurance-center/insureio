beforeEach(module('ioConstants', 'PolicyMgmtApp', 'ConsumerManagementApp', 'ioRequestSanitizer', 'DrBulletins', 'DrFilters', 'DrModels', 'ui.bootstrap', module($provide => {
  /* The ..ModalCtrl controllers below are initialized by $uiModal, which
  /* injects $uibModalInstance. Therefore, we mock $uibModalInstance in these
  /* tests. */
  $provide.value('$uibModalInstance', {})
})));

describe('PolicyMgmtApp', () => {
  var scope, kase, quote, person;

  beforeEach(inject(($injector, _$controller_, _$rootScope_, _$q_, _$httpBackend_) => {
    $scope = _$rootScope_.$new();
    /* Mock kase, etc. */
    quote = { sales_stage_id: 3 };
    kase = {
      product_type_id: 1,
      quoting_details: [quote],
      sales_stage_id: 3,
    };
    person = {
      $promise: Promise.resolve(),
      entity_type_id: 1,
      years_of_agent_relationship: 2,
    };
    /* Mock members inherited from higher scopes (particularly consumer_mgmt.js) */
    $scope.currentUser = helpers.mockSuperUser();
    $scope.task = {};
    $scope.person = helpers.mockPerson();
    $scope.policies = [];
    $scope.policies.data = [];
    $scope.stateVars = { activePolicy: kase };
    $scope.consumerMgmtVars = { scope: $scope };
    $scope.policyList = () => [kase];
    $scope.updateRecordSingleField = () => {};
    helpers.setRootScopeMembers($scope);
    /* Stub http */
    _$httpBackend_.when('GET', '/users.json?full_name=Daisy&no_pagination=true&select[]=id&select[]=full_name&select[]=company',
      () => ({ data: [] }));
    _$controller_('ConsumerManagementCtrl', { $scope: $scope }); /* Get functions from higher scope */
    _$controller_('PolicyCtrl', { $scope: $scope });
  }));

  it('runs $scope.init without Error', () => $scope.init());
  it('runs $scope.getPolicies without Error', () => $scope.getPolicies());
  it('runs $scope.policyListTrackingFunction without Error', () => $scope.policyListTrackingFunction(kase));
  it('runs $scope.getStatus without Error', () => $scope.getStatus(kase));
  it('runs $scope.replacingPolicyIsInForce without Error', () => $scope.replacingPolicyIsInForce());
  it('runs $scope.requoteLinkUrl without Error', () => $scope.requoteLinkUrl(kase));
  it('runs $scope.policyType without Error', () => $scope.policyType(kase));
  it('runs $scope.policyProductType without Error', () => $scope.policyProductType(kase));
  it('runs $scope.statusTypeGroupName without Error', () => $scope.statusTypeGroupName(kase));
  it('runs $scope.getRequirements without Error', () => $scope.getRequirements());
  it('runs $scope.getStaffAssignment without Error', () => $scope.getStaffAssignment(kase));
  it('runs $scope.getAgents without Error', () => $scope.getAgents('Daisy'));
  it('runs $scope.refreshAgentsInProfile without Error', () => $scope.refreshAgentsInProfile('Daisy'));
  it('runs $scope.updateAgentForPolicy without Error', () => $scope.updateAgentForPolicy());
  it('runs $scope.updateAgentOfRecordForPolicy without Error', () => $scope.updateAgentOfRecordForPolicy());
  it('runs $scope.loadInvolvedParties without Error', () => $scope.loadInvolvedParties());
  it('runs $scope.formatStakeholders without Error', () => $scope.formatStakeholders());
  it('runs $scope.addStakeholder without Error', () => $scope.addStakeholder(kase));
  it('runs $scope.initSubsection without Error', () => $scope.initSubsection());
  it('runs $scope.openAttachments without Error', () => $scope.openAttachments());
  it('runs $scope.roleFieldForRoleName without Error', () => $scope.roleFieldForRoleName('Dapper Dromedary'));
  it('runs $scope.sendConsumerResumeAppLink without Error', () => $scope.sendConsumerResumeAppLink());
  it('runs $scope.forceScorUnderwriting without Error', () => $scope.forceScorUnderwriting());
  it('runs $scope.forceRttXmlTransmission without Error', () => $scope.forceRttXmlTransmission());
  it('runs $scope.openPastStatusesModal without Error', () => $scope.openPastStatusesModal());
  it('runs $scope.openReqNoteDetailModal without Error', () => $scope.openReqNoteDetailModal());
  it('runs $scope.openNewRequirementModal without Error', () => $scope.openNewRequirementModal());
  it('runs $scope.openStaffAssignmentModal without Error', () => $scope.openStaffAssignmentModal());
  it('runs $scope.openSpecialHandlingModal without Error', () => $scope.openSpecialHandlingModal());
  it('runs $scope.openstakeholderRelationshipModal without Error', () => $scope.openstakeholderRelationshipModal());
  it('runs $scope.openAddPolicyModal without Error', () => $scope.openAddPolicyModal());
  it('runs $scope.openPlacedPolicyInfoModal without Error', () => $scope.openPlacedPolicyInfoModal());
  it('runs $scope.openEditQuotingDetailsModal without Error', () => $scope.openEditQuotingDetailsModal());
  it('runs $scope.openEditPlacedPolicyModal without Error', () => $scope.openEditPlacedPolicyModal());
  it('runs $scope.openExamModal without Error', () => $scope.openExamModal());
  it('runs $scope.openScorUnderwritingResponseModal without Error', () => $scope.openScorUnderwritingResponseModal());
  it('runs $scope.setActivePolicyByTypeAndId without Error', () => $scope.setActivePolicyByTypeAndId());
  it('runs $scope.setActivePolicy without Error', () => $scope.setActivePolicy(kase));
  it('runs $scope.openCurrentDetails without Error', () => $scope.openCurrentDetails());
  it('runs $scope.currentDetails without Error', () => $scope.currentDetails(kase));
  it('runs $scope.healthClass without Error', () => $scope.healthClass(kase));
  it('runs $scope.setActiveSalesStage without Error', () => $scope.setActiveSalesStage());
  it('runs $scope.openSubsection without Error', () => $scope.openSubsection());
  it('runs $scope.setActiveSubsectionTab without Error', () => $scope.setActiveSubsectionTab());
  it('runs $scope.setPolicyFilterStr without Error', () => $scope.setPolicyFilterStr());
  it('runs $scope.activeDetailsTabClass without Error', () => $scope.activeDetailsTabClass());
  it('runs $scope.activeDetailsTabAnchorClass without Error', () => $scope.activeDetailsTabAnchorClass());
  it('runs $scope.stageHasDetails without Error', () => $scope.stageHasDetails({id: 3}));
  it('runs $scope.detailsForStage without Error', () => $scope.detailsForStage());
  it('runs $scope.activeSalesStageTabClass without Error', () => $scope.activeSalesStageTabClass());
  it('runs $scope.activeSubsectionTabClass without Error', () => $scope.activeSubsectionTabClass());
  it('runs $scope.currentStatusTypeIsInStatusTypes without Error', () => $scope.currentStatusTypeIsInStatusTypes());
  it('runs $scope.yearsKnown without Error', () => $scope.yearsKnown());
  it('runs $scope.updateYearsKnown without Error', () => $scope.updateYearsKnown());
  it('runs $scope.quotedByName without Error', () => $scope.quotedByName(quote));
  it('runs $scope.getDaysSince without Error', () => $scope.getDaysSince());
  it('runs $scope.riderNames without Error', () => $scope.riderNames());
  it('runs $scope.isLifeAndLacksPB without Error', () => $scope.isLifeAndLacksPB() );
  it('runs $scope.ownerCount without Error', () => $scope.ownerCount());
  it('runs $scope.payerCount without Error', () => $scope.payerCount());
  it('runs $scope.beneficiaries without Error', () => $scope.beneficiaries());
  it('runs $scope.benPercentageTotal without Error', () => $scope.benPercentageTotal());
  it('runs $scope.checkOutstandingReqsForAppFulfillment without Error', () => $scope.checkOutstandingReqsForAppFulfillment());
  it('runs $scope.updateQuoteInclusion without Error', () => $scope.updateQuoteInclusion(quote));
  it('runs $scope.updateQuotingDetails without Error', () => $scope.updateQuotingDetails(quote));
  it('runs $scope.updateRelationshipToMatchType without Error', () => $scope.updateRelationshipToMatchType(person));
  it('runs $scope.destroyStakeholder without Error', () => $scope.destroyStakeholder({ id: 38, stakeholder_attributes: {} }));
  it('runs $scope.updateActivePolicy without Error', () => $scope.updateActivePolicy(['face_amount'], 'terrific', null));
  it('runs $scope.addCheckboxStateToPlacedPolicies without Error', () => $scope.addCheckboxStateToPlacedPolicies());
  it('runs $scope.addReplacement without Error', () => $scope.addReplacement({}));
  it('runs $scope.updatePolicyStatus without Error', () => {
    /* Mock $('#modal-edit-status-details').modal('hide'); */
    let modalArg;
    spyOn(window, '$').and.returnValue({ modal: (arg) => { modalArg = arg } });
    $scope.updatePolicyStatus(3);
    expect(modalArg).toEqual('hide');
  });
  it('runs $scope.displayNextTaskType without Error', () => $scope.displayNextTaskType(kase));
  it('runs $scope.displayNextTask without Error', () => $scope.displayNextTask(kase));
  it('runs $scope.canProcessWith without Error', () => $scope.canProcessWith('marketech'));
  it('runs $scope.processingBtnTitle without Error', () => $scope.processingBtnTitle('marketech'));
  it('runs $scope.updateProcessingTimeStamp without Error', () => $scope.updateProcessingTimeStamp());
  it('runs $scope.updateAgencyWorksTimeStamp without Error', () => $scope.updateAgencyWorksTimeStamp());
  it('runs $scope.submitForProcessing without Error', () => $scope.submitForProcessing('marketech'));
});

describe('pastStatusesModalCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('pastStatusesModalCtrl', { $scope: $scope });
  }));

  it('runs $scope.statusChangedByName without Error', () => $scope.statusChangedByName({}));
});

describe('newRequirementModalCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('newRequirementModalCtrl', { $scope: $scope });
  }));

  it('runs $scope.save without Error', () => $scope.save());
});

describe('reqNoteDetailModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('reqNoteDetailModalCtrl', { $scope: $scope, note: {} });
  }));
});

describe('staffAssignmentCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.formHolder = {};
    $scope.stateVars = { activePolicy: {} };
    _$controller_('staffAssignmentCtrl', { $scope: $scope });
  }));

  it('runs $scope.init without Error', () => $scope.init());
  it('runs $scope.setStaffAssignmentFormObj without Error', () => $scope.setStaffAssignmentFormObj());
  it('runs $scope.saveStaffAssignment without Error', () => $scope.saveStaffAssignment());
});

describe('specialHandlingModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('specialHandlingModalCtrl', { $scope: $scope });
  }));
});

describe('stakeholderRelationshipModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('stakeholderRelationshipModalCtrl', { $scope: $scope, stakeholderRelationship: {} });
  }));
});

describe('stakeholderRelationshipFormCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_, _$httpBackend_) => {
    $scope = _$rootScope_.$new();
    $scope.formHolder = { sRForm: {} };
    $scope.newStakeholderRelationshipTemplate = {};
    $scope.person = helpers.mockPerson();
    $scope.currentUser = helpers.mockSuperUser();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('stakeholderRelationshipFormCtrl', { $scope: $scope });
  }));

  it('runs $scope.searchConsumers without Error', () => $scope.searchConsumers('daisy'));
  it('runs $scope.getExistingRelationsToPI without Error', () => $scope.getExistingRelationsToPI());
  it('runs $scope.consumerGroupName without Error', () => $scope.consumerGroupName({}));
  it('runs $scope.setSDFormObj without Error', () => $scope.setSDFormObj());
  it('runs $scope.setExistingConsumerAsStakeholder without Error', () => $scope.setExistingConsumerAsStakeholder({ id: 2 }));
  it('runs $scope.isStakeholderRelationshipValid without Error', () => $scope.isStakeholderRelationshipValid());
  it('runs $scope.createOrUpdateStakeholder without Error', () => $scope.createOrUpdateStakeholder({ stakeholder_attributes: {} }));
});

describe('addPolicyModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('addPolicyModalCtrl', { $scope: $scope, recordTypeToAdd: 'Consumer', hideRecordTypeOptions: true });
  }));
});

describe('addPlacedPolicyFormCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.newPlacedPolicyTemplate = { quoting_details_attributes: [{}] };
    $scope.modalScope = {};
    $scope.formHolder = {};
    _$controller_('addPlacedPolicyFormCtrl', { $scope: $scope });
  }));

  it('runs $scope.clearNewPlacedPolicyFormObj without Error', () => $scope.clearNewPlacedPolicyFormObj());
  it('runs $scope.createPlacedPolicy without Error', () => $scope.createPlacedPolicy());
});

describe('addOpportunityFormCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.newPlacedPolicyTemplate = { quoting_details_attributes: [{}] };
    $scope.modalScope = {};
    $scope.formHolder = {};
    _$controller_('addOpportunityFormCtrl', { $scope: $scope });
  }));

  it('runs $scope.clearNewOpportunityFormObj without Error', () => $scope.clearNewOpportunityFormObj());
  it('runs $scope.createOpportunity without Error', () => $scope.createOpportunity());
});

describe('placedPolicyInfoModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('placedPolicyInfoModalCtrl', { $scope: $scope });
  }));
});

describe('editQuotingDetailsModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('editQuotingDetailsModalCtrl', { $scope: $scope, details: {} });
  }));
});

describe('editPlacedPolicyModalCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.origPolicy = { quoting_details: [{}] };
    $scope.formHolder = {};
    _$controller_('editPlacedPolicyModalCtrl', { $scope: $scope, policy: $scope.origPolicy });
  }));

  it('runs $scope.resetEditPlacedPolicyFormObj without Error', () => $scope.resetEditPlacedPolicyFormObj());
  it('runs $scope.updatePlacedPolicy without Error', () => $scope.updatePlacedPolicy());
});

describe('examModalCtrl', () => {
  let $scope;

  beforeEach(inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    let kase = { exam_one_case_data: [] };
    $scope.stateVars = { activePolicy: kase };    
    _$controller_('examModalCtrl', { $scope: $scope });
  }));

  it('runs $scope.getExamOneControlCodes without Error', () => $scope.getExamOneControlCodes());
  it('runs $scope.examStatusUrl without Error', () => $scope.examStatusUrl());
  it('runs $scope.eOScheduledNotSubmitted without Error', () => $scope.eOScheduledNotSubmitted());
  it('runs $scope.eOScheduleTitle without Error', () => $scope.eOScheduleTitle());
  it('runs $scope.eOSubmitTitle without Error', () => $scope.eOSubmitTitle());
  it('runs $scope.eOSubmitText without Error', () => $scope.eOSubmitText());
  it('runs $scope.scheduleWithExamOne without Error', () => $scope.scheduleWithExamOne());
  it('runs $scope.submitToExamOne without Error', () => $scope.submitToExamOne());
  it('runs $scope.scheduleAndSubmitWithSMM without Error', () => $scope.scheduleAndSubmitWithSMM());
});

describe('scorUnderwritingResponseModalCtrl', () => {
  it('runs without Error', inject((_$controller_, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $scope.stateVars = { activePolicy: {} };
    _$controller_('scorUnderwritingResponseModalCtrl', { $scope: $scope });
  }));
});
