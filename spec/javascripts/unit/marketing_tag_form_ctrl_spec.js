describe('MarketingTagFormCtrl', () => {

	var $controller, $scope;

	beforeEach(inject((_$controller_, _$rootScope_, _$resource_, _$httpBackend_, _UserModel_) => {
		var $rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		var $http = _$httpBackend_;
		/* mock getCurrentUser */
		$scope.getCurrentUser = () => { return _UserModel_.get({id: 0}); }
		$controller = _$controller_('MarketingTagFormCtrl', { $scope: $scope, $resource: _$resource_, $http: _$httpBackend_ });
	}));

	describe('$scope.parseAndSaveNewTag', () => {
		it('sets $scope.tagObj', () => {
			expect($scope.tagObj).toBeUndefined();
			$scope.tagObj = [];
			$scope.person = { $newTag: 'foo' };
			$scope.parseAndSaveNewTag(true);
			expect($scope.tagObj).toEqual(['foo']);
		});
	});

});

helpers.loadModules([
  'DrBulletins',
	'DrModels',
	'ngResource',
	'ioConstants',
	'ioRequestSanitizer',
	'ui.bootstrap',
	'dr.controllers',
]);

helpers.stubAndLoadModule('detectChrome');