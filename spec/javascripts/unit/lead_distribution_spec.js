
describe('LeadDistCtrl', () => {
  var scope, kase, quote, person;

  beforeEach(module('ioConstants', 'drLeadDistribution', 'DrBulletins', 'DrFilters', 'DrModels', 'ui.bootstrap', module($provide => {
    $provide.value('$resource', (url, params) => {
      if (url == '/users/agents.json')
        return { query: () => mockAgents() };
      if (url == '/lead_distribution/cursors/join_tables.json')
        return { get: () => mockJoinTables() };
      if (url == '/lead_distribution/user_rules.json')
        return { query: () => mockRules() };
      if (/\/users\/\d+\.json/.test(url))
        return { get: () => mockAgents()[0] };
    });
    /* The ..ModalCtrl controllers below are initialized by $uiModal, which
    /* injects $uibModalInstance. Therefore, we mock $uibModalInstance in these
    /* tests. */
    $provide.value('$uibModalInstance', {})
  })));

  beforeEach(inject(($injector, _$controller_, _$rootScope_, _$q_, _$httpBackend_) => {
    $scope = _$rootScope_.$new();
    $scope.currentUser = helpers.mockSuperUser();
    helpers.setRootScopeMembers($scope);
    _$controller_('LeadDistCtrl', {$scope: $scope});
  }));

  it('runs $scope.init without Error', () => $scope.init());
  it('runs $scope.clearAgent without Error', () => $scope.clearAgent());
  it('runs $scope.clearBrand without Error', () => $scope.clearBrand());
  it('runs $scope.clearCriterion without Error', () => $scope.clearCriterion());
  it('runs $scope.clearLeadType without Error', () => $scope.clearLeadType());
  it('runs $scope.createCursor without Error', () => $scope.createCursor());
  it('runs $scope.enabledLabel without Error', () => $scope.enabledLabel());
  it('runs $scope.findById without Error', () => $scope.findById());
  it('runs $scope.getAgents without Error', () => {
    return $scope.getAgents('abc').$promise.then(agents => {
      expect($scope.agents.length).toEqual(2);
    });
  });
  it('runs $scope.getJoinTables without Error', () => $scope.getJoinTables());
  it('runs $scope.getLeadDistributionRules without Error', () => $scope.getLeadDistributionRules());
  it('runs $scope.lowerPriority without Error', () => $scope.lowerPriority());
  it('runs $scope.resolvedPromiseFactory without Error', () => {
    $scope.resolvedPromiseFactory();
    $scope.$apply(); /* Resolve `$q` promises */
  });
  it('runs $scope.saveSettings without Error', () => $scope.saveSettings());
  it('runs $scope.selectAgentCb without Error', () => {
    const agent = mockAgents()[0];
    $scope.selectAgentCb(agent);
  });
  it('runs $scope.selectBrandCb without Error', () => $scope.selectBrandCb());
  it('runs $scope.selectLeadTypeCb without Error', () => $scope.selectLeadTypeCb());
  it('runs $scope.setAll without Error', () => {
    $scope.rules = mockRules();
    $scope.setAll({on: false})
  });
  it('runs $scope.showSortBtn without Error', () => $scope.showSortBtn());
  it('runs $scope.sortFnFactory without Error', () => $scope.sortFnFactory());
  it('runs $scope.sortRules without Error', () => {
    $scope.rules = mockRules();
    $scope.sortRules();
  });
  it('runs $scope.trustAsHtml without Error', () => $scope.trustAsHtml());
  it('runs $scope.updateAgentSuspension without Error', () => {
    $scope.criteria.agent = mockAgents()[0];
    $scope.updateAgentSuspension();
  });

  function mockAgents () {
    return mockResource([{id: 1, name: 'Harry Potter', temporary_suspension: false}, {id: 3, name: 'Hermione Granger', temporary_suspension: false}]);
  }

  function mockJoinTables () {
    return mockResource({"brands":[{"id":1,"full_name":"First Brand","warnings":null,"$$hashKey":"object:259"},{"id":3,"full_name":"First Brand","warnings":null,"$$hashKey":"object:260"},{"id":4,"full_name":"First Brand","warnings":null,"$$hashKey":"object:261"},{"id":2,"full_name":"Good Happy","warnings":null,"$$hashKey":"object:262"},{"id":21264,"full_name":"National Mutual Benefit","warnings":null,"$$hashKey":"object:263"}],"lead_types":[{"id":3,"text":null,"$$hashKey":"object:269"},{"id":28,"text":"05ijNOAtvt98dU","$$hashKey":"object:270"},{"id":26,"text":"0TzfuiMYH5V","$$hashKey":"object:271"},{"id":4,"text":"1TUwv8d72b5X7","$$hashKey":"object:272"},{"id":13,"text":"4frrm5utSLs","$$hashKey":"object:273"},{"id":22,"text":"7w6X7TcXt6mT","$$hashKey":"object:274"},{"id":11,"text":"854Q7ZIh01AEb","$$hashKey":"object:275"},{"id":6,"text":"9Fa4hzRJSVPj","$$hashKey":"object:276"},{"id":8,"text":"A19D9WgXnu","$$hashKey":"object:277"},{"id":25,"text":"ABsxgNLPqYutP8","$$hashKey":"object:278"},{"id":10,"text":"Ai7zoNoYjNT","$$hashKey":"object:279"},{"id":18,"text":"aNLngrMTxAzJrD","$$hashKey":"object:280"},{"id":43,"text":"asdf","$$hashKey":"object:281"},{"id":23,"text":"c0ggxW56BxxbD","$$hashKey":"object:282"},{"id":21,"text":"cYbJO4tQDWbyKXe","$$hashKey":"object:283"},{"id":27,"text":"fqgFZGXzcId","$$hashKey":"object:284"},{"id":9,"text":"fw0AxTCHGemFat","$$hashKey":"object:285"},{"id":14,"text":"i8sJOHVBI054XjG","$$hashKey":"object:286"},{"id":1,"text":"IO Quote Path Full Application","$$hashKey":"object:287"},{"id":31,"text":"IO Quote Path Full Application","$$hashKey":"object:288"},{"id":34,"text":"IO Quote Path Full Application","$$hashKey":"object:289"},{"id":36,"text":"IO Quote Path Full Application","$$hashKey":"object:290"},{"id":2,"text":"IO Quote Path Referral","$$hashKey":"object:291"},{"id":32,"text":"IO Quote Path Referral","$$hashKey":"object:292"},{"id":35,"text":"IO Quote Path Referral","$$hashKey":"object:293"},{"id":37,"text":"IO Quote Path Referral","$$hashKey":"object:294"},{"id":15,"text":"KlkN35vVXQZrW","$$hashKey":"object:295"},{"id":7,"text":"kyvCs3ztVfSrMg","$$hashKey":"object:296"},{"id":24,"text":"LMz12uurv7epr","$$hashKey":"object:297"},{"id":29,"text":"lZuXjYdde01R","$$hashKey":"object:298"},{"id":17,"text":"M6IbTESDQpbw","$$hashKey":"object:299"},{"id":16,"text":"mhU49GZpoXn2","$$hashKey":"object:300"},{"id":30,"text":"mr142qpjbjv","$$hashKey":"object:301"},{"id":44,"text":"nom","$$hashKey":"object:302"},{"id":33,"text":"Purchased","$$hashKey":"object:303"},{"id":42,"text":"qwes","$$hashKey":"object:304"},{"id":38,"text":"rqwe","$$hashKey":"object:305"},{"id":5,"text":"SRTzaGy1H6Fe","$$hashKey":"object:306"},{"id":41,"text":"trentrez","$$hashKey":"object:307"},{"id":40,"text":"trewt","$$hashKey":"object:308"},{"id":19,"text":"Uaf8XfbINz2h","$$hashKey":"object:309"},{"id":20,"text":"V6vfKGsVRmL","$$hashKey":"object:310"},{"id":39,"text":"wer","$$hashKey":"object:311"},{"id":12,"text":"x9RWmnLxbQKT5D","$$hashKey":"object:312"}],"lead_types_brands":[[1,1],[1,2],[1,21265],[4,21264],[5,21264],[6,21264],[7,21267],[8,21267],[9,21267],[10,21268],[11,21268],[12,21268],[13,21269],[14,21269],[15,21269],[16,21270],[17,21270],[18,21270],[19,21271],[20,21271],[21,21271],[22,21272],[23,21272],[24,21272],[25,21273],[26,21273],[27,21273],[28,21274],[29,21274],[30,21274],[33,21264]],"brands_users":[[2,2],[1,10],[21265,10]],"cursors":[{"lead_type_id":3,"brand_id":2,"id":null},{"lead_type_id":1,"brand_id":2,"id":null},{"lead_type_id":33,"brand_id":21264,"id":null},{"lead_type_id":1,"brand_id":21265,"id":null},{"lead_type_id":38,"brand_id":3,"id":null},{"lead_type_id":39,"brand_id":3,"id":null},{"lead_type_id":43,"brand_id":21264,"id":null},{"lead_type_id":44,"brand_id":21264,"id":null}]});
  }

  function mockRules () {
    return mockResource([{"id":2,"count":3,"tag_blacklist":null,"user_id":18,"off":false,"sunday":1,"monday":1,"tuesday":1,"wednesday":1,"thursday":1,"friday":1,"saturday":1,"cursor_id":9,"lead_type_id":1,"brand_id":2,"user_name":"Adam Nelson","lacks_permission":true,"brand":{"id":2,"full_name":"Good Happy","warnings":null,"$$hashKey":"object:262"},"leadType":{"id":1,"text":"IO Quote Path Full Application","$$hashKey":"object:287"},"agentName":"Adam Nelson","brandName":"Good Happy"},{"id":1,"count":1,"tag_blacklist":null,"user_id":2,"off":false,"sunday":1,"monday":1,"tuesday":1,"wednesday":1,"thursday":1,"friday":1,"saturday":1,"cursor_id":1,"lead_type_id":3,"brand_id":2,"user_name":"Good Happy","lacks_permission":false,"brand":{"id":2,"full_name":"Good Happy","warnings":null,"$$hashKey":"object:262"},"leadType":{"id":3,"text":null,"$$hashKey":"object:269"},"agentName":"Good Happy","brandName":"Good Happy","$$hashKey":"object:370"},{"id":3,"count":0,"tag_blacklist":null,"user_id":2,"off":false,"sunday":1,"monday":1,"tuesday":1,"wednesday":1,"thursday":1,"friday":1,"saturday":1,"cursor_id":9,"lead_type_id":1,"brand_id":2,"user_name":"Good Happy","lacks_permission":false,"brand":{"id":2,"full_name":"Good Happy","warnings":null,"$$hashKey":"object:262"},"leadType":{"id":1,"text":"IO Quote Path Full Application","$$hashKey":"object:287"},"agentName":"Good Happy","brandName":"Good Happy","$$hashKey":"object:371"}]);
  }

  function mockResource (resource) {
    resource.$promise = Promise.resolve();
    resource.$resolved = true;
    return resource;
  }
});

