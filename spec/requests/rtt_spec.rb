require 'spec_helper'

describe '/motorists' do
  it 'redirects to /rtt, with the query string intact' do
    get '/motorists?foo=bar&baz=qux'
    expect(response.status).to eq 301
    expect(response.location).to match /^http:\/\/www.example.com\/rtt\?/
    expect(response.location).to match /\Wbaz=qux(\W|$)/
    expect(response.location).to match /\Wfoo=bar(\W|$)/
  end
end
