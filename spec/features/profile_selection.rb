require 'spec_helper'

describe 'brand-select' do
  before(:all) do
    @user=FactoryBot.create(:users_in_hierarchy)

    @brand_1=FactoryBot.create(:brand); @user.brands << @brand_1
    @brand_2=FactoryBot.create(:brand); @user.brands << @brand_2
    @brand_3=FactoryBot.create(:brand); @user.brands << @brand_3
  end
  before(:each) do
  	#login(@user)
  	ApplicationController.stub(:current_user).and_return(@user)#using stub instead of my helper function.
  end

  #not needed because of config.after :each filter in spec_helper.rb?
  #gives an error saying no method destroy for nil:NilClass, but then @user and @brands are still in db.
  after(:all) do #necessary because selenium doesn't play nice with transactional records
    @user.destroy
    @brand_1.destroy
    @brand_2.destroy
    @brand_3.destroy
  end#still getting the below error:
  #An error occurred in an after(:all) hook.
  #NoMethodError: undefined method `destroy' for nil:NilClass
  #occurred at C:/Users/mauricef/Desktop/CLU2/spec/features/brand_selection.rb
	#18:in `block (2 levels) in <top (required)>'
  
	it 'should show options for all brands which belong to the current user' do
		within('#brand-select') do
			#make sure the right option values are there
			page.has_css?("option[value=#{@brand_1.id}]")
			page.has_css?("option[value=#{@brand_2.id}]")
			page.has_css?("option[value=#{@brand_3.id}]")
			#make sure the right option text is there
			page.has_content?("@brand_1.name")
			page.has_content?("@brand_2.name")
			page.has_content?("@brand_3.name")
		end
	end
	it 'should show option for blank' do
		within('#brand-select') do
			#one option without a value for the prompt
			#and another option without a value for selecting blank/nil brand
			page.has_css?("option[value='']", :minimum => 2)
			page.has_css?("option[value='']", :maximum => 2)
		end
	end
	#it 'should set cookie to match selection' do
		#select(@brand_1.name, :from => 'brand-select')
		#response.cookies['brand'].should eq(@brand_1.id)
	#end
	#it 'should update selected brand to match selection' do
		#
	#end
	#it 'should set selected brand to nil to match blank selection' do
		#
	#end
end