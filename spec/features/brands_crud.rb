require 'spec_helper'
require 'support/feature_helpers.rb'
include RequestsHelper
require 'database_cleaner'
require 'capybara/rails'

RSpec.configure do |config|
  config.include FeatureHelpers
end

describe "Create/Update/Destroy A Brand" do

  before(:each) do
    User.destroy_all
    Brand.destroy_all
    @user   = FactoryBot.create(:user)
    @brands = FactoryBot.create_list(:brand, :owner: @user, 5)
    request_login(@user)
    navigate_to_brand_mgmt_page
  end

   it "should create brand successfull", js:true do
    pending('Spec needs implementation.')
    #
  end
  
  it "should edit and update brand successfully", js:true do
    pending('Spec needs implementation.')
    brand = Brand.first
    starting_brand_count = Brand.count

    #open modal
    #fill new brand form
    #click submit

    sleep 1
    find( css_selectors[:bulletins] ).should have_content('Successfully updated')
    Brand.count.should eq( starting_brand_count )
  end

  it "should delete brand successfully", js:true do
    pending('Spec needs implementation.')
    brand = Brand.first
    brand_count = Brand.count

    find("#delete-brand-"+"#{brand.id}").click
    page.driver.browser.switch_to.alert.accept
    sleep 1
    Brand.count.should eq(brand_count-1)
    find( css_selectors[:bulletins] ).should have_content('Successfully deleted')
    sleep 2
  end
end

describe "Assign Members To A Brand" do
  before(:each) do
    User.destroy_all
    Brand.destroy_all
    @user   = FactoryBot.create(:user)
    @brands = FactoryBot.create_list(:brand, :owner: @user, 5)
    request_login(@user)
    navigate_to_brand_assignments_page
  end

  it "should add a member user to a brand successfully", js:true do
    pending('Spec needs implementation.')
    @another_user = FactoryBot.create(:user)

    #add @another_user as a member to one of the brands
    sleep 2
    find( css_selectors[:bulletins] ).should have_content('Successfully assigned.')
    #check brands_users database table
  end

end

def navigate_to_brand_mgmt_page
  find( css_selectors[:navbar_toggle] ).click

  within( css_selectors[:navbar_links] ) do
    click_on 'Agency Management'
    click_on 'Brands'
  end
end

def navigate_to_brand_assignments_page
  find( css_selectors[:navbar_toggle] ).click
  within( css_selectors[:navbar_links] ) do
    click_on 'Agency Management'
    click_on 'Brand Assignments'
  end
end
