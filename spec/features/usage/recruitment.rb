require 'spec_helper'
require 'support/feature_helpers.rb'
include RequestsHelper
require 'database_cleaner'

RSpec.configure do |config|
  config.include FeatureHelpers
end

describe "Create New user" do

  before(:each) do
    User.destroy_all
    @user = FactoryBot.create(:user)
  end

  it "should create a user with enabled field false and parent should be nil", :js => true do
    user_count = User.count
    visit login_path
    submit_create_user_form(1)
    latest_count = User.count
    (latest_count - user_count).should eql 1
    new_user = User.last
    new_user.enabled.should eql false
    new_user.parent_id.should eql nil
  end

  it "should create a user with enabled and should assign a parent  passed via params[:parent_id]", :js => true do
    visit login_path(:parent_id => @user.id)
    submit_create_user_form(1)
    new_user = User.last
    new_user.parent === @user
  end

  it "should create a user with enabled and should assign a parent  passed via params[:parent_id]", :js => true do
    visit login_path
    submit_create_user_form(1)
    ['Dashboard', 'Reporting', 'Marketing', 'Sales Tools', 'Underwriting / Suitability', 'Contacts', 'System Management', 'Agency Management'].each do |side_bar_option|
      click_on(side_bar_option)
      assert page.has_text?("This is the application layout")
    end
    ['New Quote', 'Consumer Management', 'Help', 'My Account'].each do |side_bar_option|
      visit root_path
      click_on(side_bar_option)
      assert page.has_no_text?("This is the application layout")
    end
  end

  it "should invite a new user for recruitment", :js => true do
    request_login(@user)
    click_on("Agency Management")
    click_on("Recruit")
    fill_in 'email', :with => "invalid-email"
    page.find(:image,"[@value='Send']").click
    assert page.has_text?('Please insert a valid invitation email.')
    @smtp_server = FactoryBot.create(:marketing_email_smtp_server, owner_id: @user.id)
    update_smtp_server_settings
    recruitment_email = Forgery(:internet).email_address
    fill_in 'email', :with => recruitment_email
    page.find(:image,"[@value='Send']").click
    assert page.has_text?('e-mail successfully dispatched.')
    ActionMailer::Base.deliveries.size.should eq(1)
    ActionMailer::Base.deliveries.last.to.should include recruitment_email 
    mesage_content = ActionMailer::Base.deliveries.last.body.raw_source
    mesage_content.should have_content login_path(:parent_id => @user.id)
  end

  private

  def submit_create_user_form(count)
    fill_in 'user[full_name]',             :with => "test-firstname#{count} test-middlename#{count} test-lastname#{count}"
    fill_in 'user[login]',                 :with => "test-login#{count}@foo.com"
    fill_in 'user[password]',              :with => "123456"
    fill_in 'user[password_confirmation]', :with => "123456"
    find("#create-new-user").click
    sleep 2
    assert page.has_text?('Sucessfully account created.')
  end

  def set_user_role(user)
    user.role_id = 1
    user.save
  end

  def update_smtp_server_settings
    #======= Need to add valid credentials========================
    @smtp_server.update_attributes(address: "smtp.gmail.com",
      username: "skrathore.ror@gmail.com", password: "rubyonrails420", port: 587)
    #=============================================================
  end
end
