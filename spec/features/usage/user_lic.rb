require 'spec_helper'
require 'support/feature_helpers.rb'
include RequestsHelper

RSpec.configure do |config|
  config.include FeatureHelpers
end

describe "User l & c" do
  before(:each) do
    User.delete_all
    Usage::AmlVendor.delete_all
    Usage::Aml.delete_all
    Usage::License.delete_all
    Usage::Contract.delete_all
    Delayed::Worker.delay_jobs = false
    @user = FactoryBot.create(:user, :can_strict_have_children => true, :can_strict_edit_descendants => true, :can_strict_edit_siblings => true ,:role_id => 1)
    @child = FactoryBot.create(:user, :can_strict_have_children => true, :can_strict_edit_descendants => true, :can_strict_edit_siblings => true, :parent_id => @user.id)
    @aml_vendor = FactoryBot.create(:usage_aml_vendor)
    @states = states = Enum::State
    request_login(@user)
  end
  
  it "should create new license with all states" do
    goto_l_and_c
    @states.map(&:id).each do |state_id|
      number = Forgery(:basic).number
      license_count = Usage::License.count
      click_link "New License"
      create_new_license(state_id,number)
      sleep 2
      assert page.has_text?("Successfully created")
      Usage::License.count.should eq(license_count+1)
      sleep 1
    end
    Usage::License.count.should eq(@states.count)
  end
  
  it "should not create new license if state and number is blank" do
    license_count = Usage::License.count
    goto_l_and_c
    click_link "New License"
    create_new_license(nil,nil)
    sleep 2
    assert page.has_text?("State can't be blank")
    assert page.has_text?("Number can't be blank")
    Usage::License.count.should eq(license_count)
  end
  
  it "should not create new license if state already exists" do
    create_license_for_single_state
    license_count = Usage::License.count
    goto_l_and_c
    click_link "New License"
    create_new_license(@states.first.id, Forgery(:basic).number)
    sleep 2
    assert page.has_text?("State has already been taken")
    Usage::License.count.should eq(license_count)
  end
  
  it "should update license for each state" do
    create_license_for_each_state
    license_count = Usage::License.count
    update_license_for_each_state
    sleep 2
    assert page.has_text?("Successfully updated")
    Usage::License.count.should eq(license_count)
  end
  
  it "should delete license for each state" do
    create_license_for_each_state
    goto_l_and_c
    delete_license_for_each_state
    Usage::License.count.should eq(0)
  end

  it "should create new contract with all states" do
    create_license_for_each_state
    sleep 5
    goto_l_and_c
    @states.map(&:id).each do |state_id|
      contract_count = Usage::Contract.count
      click_link "New Contract"
      create_new_contract(state_id)
      sleep 2
      assert page.has_text?("Successfully created")
      Usage::Contract.count.should eq(contract_count+1)
      sleep 1
    end
    Usage::Contract.count.should eq(@states.count)
  end
  
  it "should not create new contract if state is blank" do
    contract_count = Usage::Contract.count
    goto_l_and_c
    click_link "New Contract"
    create_new_contract(nil)
    sleep 2
    assert page.has_text?("State can't be blank")
    Usage::Contract.count.should eq(contract_count)
  end
  
  it "should not create new contract if license not have for select state" do
    create_license_for_single_state
    contract_count = Usage::Contract.count
    goto_l_and_c
    click_link "New Contract"
    create_new_contract(@states.last.id)
    sleep 2
    assert page.has_text?("Agent must be licensed for this state before setting up contracts in this state.")
    Usage::Contract.count.should eq(contract_count)
  end
  
  it "should update contract for each state" do
    create_license_for_each_state
    create_contract_for_each_state
    contract_count = Usage::Contract.count
    update_contract_for_each_state
    sleep 2
    assert page.has_text?("Successfully updated")
    Usage::Contract.count.should eq(contract_count)
  end
  
  it "should delete contract for each state" do
    create_license_for_each_state
    create_contract_for_each_state
    goto_l_and_c
    delete_contract_for_each_state
    Usage::Contract.count.should eq(0)
  end
  
  def goto_l_and_c
		click_on "Agency Management"
    sleep 1
    find_by_id("personal-link#{@child.id}").click
		click_on "Lic. & Contracting"
		sleep 2
  end
    
  def create_new_license(state_id, number)
		fill_license_values(state_id, number)
    click_button "Create License"
  end
 
	def fill_license_values(state_id, number)
    fill_in "usage_license[number]", :with => number
		page.execute_script %Q{ $('#usage_license_effective_date').trigger("focus") }
		page.execute_script %Q{ $(".day:contains('15')").trigger("click") } # click on day 15
		sleep 1
		page.execute_script %Q{ $('#usage_license_expiration').trigger("focus") }
		page.execute_script %Q{ $(".day:contains('25')").trigger("click") } # click on day 25
		sleep 1
    find_by_id('usage_license_status_id').find('option[value="1"]').click
		find_by_id('usage_license_state_id').find("option[value='#{state_id.to_s}']").click
	end
  
  def create_license_for_each_state
    @states.map(&:id).each do |state_id|
      FactoryBot.create(:usage_license, :state_id => state_id, :user_id => @child.id)
    end
    sleep 3
  end
  
  def create_license_for_single_state
    FactoryBot.create(:usage_license, :state_id => @states.first.id, :user_id => @child.id)
  end
   
  def update_license_for_each_state
    goto_l_and_c
    Usage::License.all.each do |license|
      lic_number = license.number
      click_on "Lic. & Contracting"
      sleep 1
      find("#edit_license_#{license.id}").click
      sleep 1
      fill_update_license_values
      click_button "Update License"
      sleep 2
      Usage::License.where(id: license.id).first.number.should_not eq(lic_number) 
    end
  end
  
  def fill_update_license_values
    fill_in "usage_license[number]", :with => Forgery(:basic).number
  end
  
  def delete_license_for_each_state 
    Usage::License.all.each do |license|
      count = Usage::License.count
      find("#delete_license_#{license.id}").click
      sleep 1
      page.driver.browser.switch_to.alert.accept
      sleep 1
      assert page.has_text?('Successfully deleted')
      Usage::License.count.should eq(count-1)
    end
  end
    
  def create_new_contract(state_id)
		fill_contract_values(state_id)
    click_button "Create Contract"
  end
  
  def fill_contract_values(state_id)
    page.execute_script %Q{ $('#usage_contract_effective_date').trigger("focus") }
		page.execute_script %Q{ $(".day:contains('20')").trigger("click") } # click on day 20
		sleep 1
		page.execute_script %Q{ $('#usage_contract_expiration').trigger("focus") }
		page.execute_script %Q{ $(".day:contains('27')").trigger("click") } # click on day 27
		sleep 1
    find_by_id('usage_contract_status_id').find('option[value="1"]').click
		find_by_id('usage_contract_state_id').find("option[value='#{state_id.to_s}']").click
  end
  
  def create_contract_for_each_state
    @states.map(&:id).each do |state_id|
      FactoryBot.create(:usage_contract, :state_id => state_id, :user_id => @child.id, :carrier_contract_id => Forgery(:basic).text)
    end
    sleep 3
  end
  
  def update_contract_for_each_state
    goto_l_and_c
    Usage::Contract.all.each do |contract|
      carrier_contract_id = contract.carrier_contract_id
      click_on "Lic. & Contracting"
      sleep 1
      find("#edit_contract_#{contract.id}").click
      sleep 1
      fill_update_contract_values
      click_button "Update Contract"
      sleep 2
      Usage::Contract.where(id: contract.id).first.carrier_contract_id.should_not eq(carrier_contract_id) 
    end
  end
  
  def fill_update_contract_values
    fill_in "usage_contract[carrier_contract_id]", :with => Forgery(:basic).text
  end
  
  def delete_contract_for_each_state
    Usage::Contract.all.each do |contract|
      count = Usage::Contract.count
      find("#delete_contract_#{contract.id}").click
      sleep 1
      page.driver.browser.switch_to.alert.accept
      sleep 1
      assert page.has_text?('Successfully deleted')
      Usage::Contract.count.should eq(count-1)
    end
  end
end 
