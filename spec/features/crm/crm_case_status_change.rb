require 'spec_helper'
require 'support/feature_helpers.rb'
include RequestsHelper

RSpec.configure do |config|
  config.include FeatureHelpers
end

describe "Crm Notes CRUD" do
  before(:each) do
    User.delete_all
    Crm::StatusType.delete_all
    Crm::TaskBuilder.delete_all
    Consumer.delete_all
    Crm::Case.delete_all
    Crm::Status.delete_all
    Crm::SystemTask.delete_all
    @user = FactoryBot.create(:user, :can_strict_have_children => true, :can_strict_edit_descendants => true, :role_id => 1)
    create_status_types(@user, 5)
    create_auto_system_task_rules(@user, 5)
    create_consumers(@user, 5)
    create_cases(@user, 5)
    request_login(@user)
  end

  it "should appear SystemTasks as they should in the Statuses#show view" do
    click_on "Consumer Management"
    create_system_tasks(@user, Crm::Case.all, 5)
    @user.consumers.each do |consumer|
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      Crm::StatusType.viewables(@user).each do |st|
        select(st.name, :from => 'crm_status_status_type_id')
        Crm::SystemTask.where(:status_id => consumer.cases.first.status.id).each do |task|
          if task.task_type
            assert page.has_text?(task.task_type.name)
          end
          if task.marketing_email_template
            assert page.has_text?(task.marketing_email_template.name)
          end
        end
      end
      click_on "Consumer Management"
    end
  end

  it "When a StatusType which has never been chosen for the given Case is selected" do
    # a new Status created, and new SystemTasks are created for that status.
    # Each SystemTask must correspond to an AutoSystemTaskRule belonging to the Status.status_type.
    click_on "Consumer Management"
    create_system_tasks(@user, Crm::Case.all, 5)
    @user.consumers.each do |consumer|
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      new_status_type = Crm::StatusType.viewables(@user).last
      status_count = Crm::Status.count
      system_task_count = Crm::SystemTask.count
      select(new_status_type.name, :from => 'crm_status_status_type_id')
      sleep 2
      new_status_count = Crm::Status.count
      (new_status_count - status_count).should eql 1
      if new_status_type.task_builders
        new_system_task_count = Crm::SystemTask.count
        (new_system_task_count - system_task_count).should eql new_status_type.task_builders.count   
      end
      click_on "Consumer Management"
    end
  end

  # If 'auto system task rule' role is not nil and case's staff assignment has user with similar role to 'auto system task rule'
  it "should auto assign the system task to case's staff_assignment if auto system task rule has rol id and one of staff has the same role" do
    click_on "Consumer Management"
    @user.consumers.each do |consumer|
      crm_case = consumer.cases.first
      create_staff_assignment_for_case(crm_case)
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      status_type = Crm::StatusType.viewables(@user).last
      crm_case.reload.status.system_tasks[0].assigned_to.should eql nil
      select(status_type.name, :from => 'crm_status_status_type_id')
      sleep 4
      system_task = Crm::SystemTask.where(:status_id => crm_case.reload.status.id).last
      crm_case.reload.staff_assignment.manager.should eql system_task.reload.assigned_to
      click_on "Consumer Management"
    end
  end

  # If 'auto system task rule' role is not nil and case.agent.staff_assignment has user with similar role to 'auto system task rule'  
  it "should auto assign the system task to case.agent's staff_assignment if auto system task rule has role id and one of staff has the same role" do
    click_on "Consumer Management"
    @user.consumers.each do |consumer|
      crm_case = consumer.cases.first
      create_staff_assignment_for_case_agent(crm_case)
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      status_type = Crm::StatusType.viewables(@user).last
      crm_case.reload.status.system_tasks[0].assigned_to.should eql nil
      select(status_type.name, :from => 'crm_status_status_type_id')
      sleep 4
      system_task = Crm::SystemTask.where(:status_id => crm_case.reload.status.id).last
      crm_case.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      crm_case.agent.reload.staff_assignment.manager.should eql system_task.reload.assigned_to
      click_on "Consumer Management"
    end
  end

  # If 'auto system task rule' role is not nil and case's staff assignment does not have user with similar role to 'auto system task rule' but case.consumer.staff_assignment has
  it "should auto assign the system task to case.consumer's staff_assignment if auto system task rule has role id and one of staff has the same role" do
    click_on "Consumer Management"
    @user.consumers.each do |consumer|
      crm_case = consumer.cases.first
      create_staff_assignment_for_case_consumer(crm_case)
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      status_type = Crm::StatusType.viewables(@user).last
      crm_case.reload.status.system_tasks[0].assigned_to.should eql nil
      select(status_type.name, :from => 'crm_status_status_type_id')
      sleep 4
      system_task = Crm::SystemTask.where(:status_id => crm_case.reload.status.id).last
      crm_case.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      crm_case.agent.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      crm_case.consumer.reload.staff_assignment.manager.should eql system_task.reload.assigned_to
      click_on "Consumer Management"
    end
  end

  # If 'auto system task rule' role is not nil and none case, case.agent and 
  it "should auto assign the system task to any user with the role similar to 'auto system task rule'" do
    click_on "Consumer Management"
    @user.consumers.each do |consumer|
      crm_case = consumer.cases.first      
      click_on consumer.full_name.slice(0..6)
      click_on "Policy"
      click_on "Follow up"
      status_type = Crm::StatusType.viewables(@user).last
      crm_case.reload.status.system_tasks[0].assigned_to.should eql nil
      select(status_type.name, :from => 'crm_status_status_type_id')
      sleep 4
      system_task = Crm::SystemTask.where(:status_id => crm_case.reload.status.id).last
      crm_case.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      crm_case.agent.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      crm_case.consumer.reload.staff_assignment.try(:manager).should_not eql system_task.reload.assigned_to
      system_task.reload.assigned_to.role.should eql Enum::UsageRole.find_by_name("manager")
      click_on "Consumer Management"
    end
  end

  it "When a Status Type which was chosen at some earlier time for the given Case is selected again" do
    click_on "Consumer Management"
    create_system_tasks(@user, Crm::Case.all, 5)
    consumer = @user.consumers.first
    click_on consumer.full_name.slice(0..6)
    click_on "Policy"
    click_on "Follow up"
    temp_status_type = old_status_type = consumer.cases.first.status.status_type
    2.times do |i|
      if i == 0
        new_status_type = Crm::StatusType.viewables(@user).last
      else
        new_status_type = temp_status_type
      end
      sleep 2
      select(new_status_type.name, :from => 'crm_status_status_type_id')
      sleep 2
      changed_status_type = consumer.cases.first.reload.status.status_type
      old_status_type.should_not eql changed_status_type
      new_status_type.should eql changed_status_type
      old_status_type = consumer.cases.first.reload.status.status_type
    end
  end

  def create_status_types(user, count)
    count.times { FactoryBot.create(:crm_status_type, :ownership_id => 1, :owner_id => user.id) }
  end

  def create_auto_system_task_rules(user ,count)
    status_types = Crm::StatusType.viewables(user)
    count.times do |i|
      FactoryBot.create(:crm_task_builder, :status_type_id => status_types[i].id, :role_id => Enum::UsageRole.find_by_name("manager").id)
    end
  end

  def create_consumers(user, count)
    count.times { FactoryBot.create(:consumer, :agent_id => user.id) }
  end

  def create_cases(user, count)
    consumers = user.consumer
    agent_user = FactoryBot.create(:user)
    count.times do |i| 
      FactoryBot.create(:crm_case, :consumer_id => consumers[i].id, :agent_id => agent_user.id)
    end
    user = FactoryBot.create(:user, :can_strict_have_children => true, :can_strict_edit_descendants => true, :role_id => Enum::UsageRole.find_by_name("manager").id)
  end

  def create_system_tasks(user, crm_cases, count)
    count.times do |i|
      FactoryBot.create(:task, :created_by => user.id.to_i, :consumer_id => crm_cases[i].consumer.id, :status_id => crm_cases[i].status_id, :completed_at => nil )
    end 
  end

  def create_staff_assignment_for_case(crm_case)
    usage_staff_assignment = create_staff_assignment
    usage_staff_assignment.owner=crm_case
    usage_staff_assignment.save
  end

  def create_staff_assignment_for_case_agent(crm_case)
    usage_staff_assignment = create_staff_assignment
    usage_staff_assignment.owner=crm_case.agent
    usage_staff_assignment.save
  end

  def create_staff_assignment_for_case_consumer(crm_case)
    usage_staff_assignment = create_staff_assignment
    usage_staff_assignment.owner=crm_case.consumer
    usage_staff_assignment.save
  end

  def create_staff_assignment
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    usage_staff_assignment = FactoryBot.create(:usage_staff_assignment, :case_manager_id => user2.id, :manager_id => user1.id)
    return usage_staff_assignment 
  end

  def create_users( count )
    count.times { FactoryBot.create(:user) }
  end

end
