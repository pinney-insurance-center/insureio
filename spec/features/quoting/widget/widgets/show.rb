require 'spec_helper'


describe 'quoting/widget/widgets/show.html.erb', js: true do#the html file is just a canvas for the js file to act on.
  VCR.configure do |c|#make it visit the pages everytime, since they're local, except cdns, which are cached.
    c.ignore_request{|r| true }
  end
  let(:widget) { Quoting::Widget.create quoter_type_id: 1, lead_type:'the best kind', referrer:'a guy who refers people', source:'a website', tags: "foo=bar, abc 123=the basics, lalala",agent_split: "Jack gets nil. Jill gets 100%.", brand_id:1, user: user, hostname:'http://www.example.com'}
  let(:user){ build :user }
  let(:brand){ build :brand }
  before do
    Quoting::Widget.stub(:find).and_return(widget)
    Quoting::Widget.stub(:find_by_id).and_return(widget)
    brand.id=1
    Brand.stub(:find){brand}
  end

  it 'prepopulates tags from the model' do
    visit("/quoting/widget/widgets/#{widget.id}")
    tags=page.evaluate_script("window.dataRaptorQuoter#{widget.id}.leadImportData.consumer.tags_attributes")
    tags.should include(
      {"key"=>"lead_type",   "value"=>"the best kind"},
      {"key"=>"referrer",    "value"=>"a guy who refers people"},
      {"key"=>"source",      "value"=>"a website"},
      {"key"=>"foo",         "value"=>"bar"},
      {"key"=>"abc 123",     "value"=>"the basics"},
      {"key"=>"lalala",      "value"=>""},
      {"key"=>"agent split", "value"=>"Jack gets nil. Jill gets 100%."},
    )
  end

  it 'parses and adds tags from the host page\'s url query string' do
    visit("/quoting/widget/widgets/#{widget.id}?dr_tags%5Bpromo_code%3DH-13-1%5D=&dr_tags%5Bpromo_code%3Dinsurance%5D=&a-non-nested-param=not-intended-to-be-a-tag&dr_tags%5Ba_key%5D=a%20value")
    tags=page.evaluate_script("window.dataRaptorQuoter#{widget.id}.leadImportData.consumer.tags_attributes")
    tags.should include(
      {"key"=>"promo_code=H-13-1",    "value"=>""},
      {"key"=>"promo_code=insurance", "value"=>""},
      {"key"=>"a_key",                "value"=>"a value"},
    )
    tags.should_not include( {"key"=>"a-non-nested-param", "value"=>"not-intended-to-be-a-tag"} )
  end

  context 'when cookies hold consumer details from a previous page load' do
    before do
      visit("/quoting/widget/widgets/#{widget.id}")
      page.evaluate_script("window.dataRaptorQuoter#{widget.id}.bakeACookie('id','5')")
      page.evaluate_script("window.dataRaptorQuoter#{widget.id}.bakeACookie('case_id','10')")
      visit("/quoting/widget/widgets/#{widget.id}")
    end
    it 'parses and populates id and case id from cookies' do
      page.should have_css('.whom-for')
      page.evaluate_script("window.dataRaptorQuoter#{widget.id}.leadImportData.consumer.id==5").should be true
      page.evaluate_script("window.dataRaptorQuoter#{widget.id}.leadImportData.consumer.cases_attributes[0].id==10").should be true
    end
  end

  context 'when a name and either a phone or email is present' do
    before do
      widget.show_name_field =true
      widget.show_email_field=true
      widget.save
      visit("/quoting/widget/widgets/#{widget.id}")
      fill_in_stage1
      click_button('Compare Quotes')
    end
    it 'submits connection info to #get_quotes' do
      #+find+ blocks further execution until either the needed element is found, or the query times out
      #but +find+ does not produce an element that can be reliably tested for its content (probably because it's a script element).
      selector="#dr-quoter-widget-#{widget.id}-modal #quote-results-list script"
      find(selector,visible:false)
      script_element=page.evaluate_script "jQuery('#{selector}').text();"
      script_element.should include("t.prepopulateIdAndHealthInfoFromUrlAndCookies()")
    end
  end

  context 'when a name or phone/email is lacking' do
    before do
      widget.show_name_field =false
      widget.save
      visit("/quoting/widget/widgets/#{widget.id}")
      fill_in_stage1 false
      click_button('Compare Quotes')
    end
    it 'does NOT return consumer id or details (because it did not create/update consumer)' do
      selector="#dr-quoter-widget-#{widget.id}-modal #quote-results-list script"
      find(selector,visible:false)
      script_element=page.evaluate_script "jQuery('#{selector}').text();"
      script_element.should_not include("t.prepopulateIdAndHealthInfoFromUrlAndCookies()")
    end
  end

  #different from the above test because the field is present on the form, it's just not filled in
  context 'when required fields are left blank' do
    before do
      visit("/quoting/widget/widgets/#{widget.id}")
      fill_in_stage1 false
      click_button('Compare Quotes')
    end
    it 'highlights missing fields and shows an alert' do
      page.driver.browser.switch_to.alert.accept
      page.should have_css(".stage1 [name='name'].error")
    end
    it 'does NOT return consumer id or details (because it did not create/update consumer)' do
      page.driver.browser.switch_to.alert.accept
      selector="#dr-quoter-widget-#{widget.id}-modal"
      find(selector,visible:false)
    end
  end
  context 'when highlighted blank fields are corrected' do
    before do
      visit("/quoting/widget/widgets/#{widget.id}")
      fill_in_stage1 false
      click_button('Compare Quotes')
    end
    it 'highlight is removed and form submits' do
      page.driver.browser.switch_to.alert.accept
      page.should have_css(".stage1 [name='name'].error")
      fill_in('name', with: "John Smith")
      click_button('Compare Quotes')
      page.should_not have_css(".stage1 [name='name'].error")
      find("#dr-quoter-widget-#{widget.id}-modal").should be_visible
    end
  end

  context 'when widget type is "ltc"' do
    before do
      widget.quoter_type_id=4
      widget.save
    end
    it 'adds the widget to the dom without error' do
      visit("/quoting/widget/widgets/#{widget.id}")
      find("#dr-quoter-widget-#{widget.id}")
    end
  end

  context 'when widget type is "lead capture"' do
    before do
      widget.quoter_type_id=0
      widget.save
    end
    it 'adds the widget to the dom without error' do
      visit("/quoting/widget/widgets/#{widget.id}")
      find("#dr-quoter-widget-#{widget.id}")
    end
  end
end

def fill_in_stage1 include_name=true, include_email=true
  page.execute_script 'jQuery("#phone").val("9998887777");jQuery("#phone").change();'#b/c masked input plugin doesn't play nice with +fill_in+

  find('#birth_month option[value="01"]'  ).select_option
  find('#birth_day   option[value="01"]'  ).select_option
  find('#birth_year  option[value="1965"]').select_option

  fill_in('weight',      with: "111")
  fill_in('name',        with: "John Smith")       if include_name
  fill_in('email',       with: "john@smithco.com") if include_email
end
