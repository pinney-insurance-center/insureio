ENV['RAILS_ENV'] = 'test'
require 'rubygems'
require 'spork' if ((ENV['OS'] == 'Windows_NT') || RUBY_PLATFORM =~ /mswin/)
require 'factory_bot_rails'
require 'sidekiq'
require 'sidekiq/testing'
require 'vcr'
require 'pry'
Sidekiq::Testing.fake!

unless defined?(Spork)
  # Stub out spork so it's not a requirement of the project
  module Spork
    def self.prefork(&block)
      block.call
    end
    def self.each_run(&block)
      block.call
    end
  end
end

Spork.prefork do
  require File.expand_path("../../config/environment", __FILE__)
  require 'shoulda'
  require 'rspec/rails'
  require 'capybara/rspec'
  require 'capybara/rails'
  require 'launchy'
  #require 'database_cleaner'
  require "webmock/rspec"

  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.

  # simplecov-html prints the used test suites in the footer of the generated coverage report.
  require 'simplecov'
  SimpleCov.start 'rails' do
    add_filter '/vendor'
    use_merging false
  end

  # Requires supporting ruby files with custom matchers and macros, etc,
  (Dir[Rails.root.join("spec/support/**/*.rb")]+Dir[Rails.root.join("spec/support/*.rb")]).each {|f| require f}

  # truncate log file
  File::open("#{Rails.root}/log/test.log",'w'){}

  Capybara.default_max_wait_time = 10
  WebMock.disable_net_connect!

  RSpec.configure do |config|
    # ## Mock Framework
    #
    # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
    #
    # config.mock_with :mocha
    # config.mock_with :flexmock
    # config.mock_with :rr

    # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
    # config.fixture_path = "#{::Rails.root}/spec/fixtures"

    # If you're not using ActiveRecord, or you'd prefer not to run each of your
    # examples within a transaction, remove the following line or assign false
    # instead of true.
    config.use_transactional_fixtures = true

    # If true, the base class of anonymous controllers will be inferred
    # automatically. This will be the default behavior in future versions of
    # rspec-rails.
    config.infer_base_class_for_anonymous_controllers = false

    config.infer_spec_type_from_file_location!

    # Run specs in random order to surface order dependencies. If you find an
    # order dependency and want to debug it, you can fix the order by providing
    # the seed, which is printed after each run.
    #     --seed 1234
    config.order = "random"
    config.include CapybaraHelpers, :type => :feature
    config.include AuthenticationHelper, type: :controller

    # Use :build and :create without writing namespace
    config.include FactoryBot::Syntax::Methods

    #Cutoff specs that take over 15 seconds
    #config.around(:each){|c| Timeout::timeout(15){ c.run}}

    #This behavior must be explicitly set as of version 3.
    config.infer_spec_type_from_file_location!

    config.before :each do
      # Clear thread-specific annotations
      Thread.current.keys
      .select { |key| key =~ /current_user|^sess_/ }
      .each { |key| Thread.current[key] = nil }
      Thread.current[:tenant_id] = nil
    end
  end

  #Ensure that feature specs use the same browser we do most manual testing on.
  #Also, Chrome has better dev tools.
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end
  # in spec/support/ and its subdirectories.
  Capybara.default_driver = :selenium

  #Ensure that capybara uses the same host as all the url helpers,
  #so that absolute urls (like those used in embeddable quoter widgets) work.
  Capybara.app_host    = APP_CONFIG['base_url']
  Capybara.server_host = APP_CONFIG['default_url_options'][:host]
  Capybara.server_port = APP_CONFIG['default_url_options'][:port]
end

Spork.each_run do
  # This code will be run each time you run your specs.
  FactoryBot.reload
end

FactoryBot.use_parent_strategy = true