require 'webmock'

RSpec.configure do |c|
  c.before(:each) {
    stub_request(:get, "https://wssim.labone.com/services/eoservice.asmx?WSDL").
    to_return(:status => 200, :body => LABONE_EOS_RESPONSE, :headers => {"Content-Type" => "text/xml; charset=utf-8"})
  }
end

LABONE_EOS_RESPONSE = <<-LABONE_EOS_RESPONSE
<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://QuestWebServices/EOService" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://QuestWebServices/EOService" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://QuestWebServices/EOService">
      <s:element name="LoopBack">
        <s:complexType />
      </s:element>
      <s:element name="LoopBackResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="LoopBackResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeliverExamOneContent">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="destinationID" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="payload" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeliverExamOneContentResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="DeliverExamOneContentResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="LoopBackSoapIn">
    <wsdl:part name="parameters" element="tns:LoopBack" />
  </wsdl:message>
  <wsdl:message name="LoopBackSoapOut">
    <wsdl:part name="parameters" element="tns:LoopBackResponse" />
  </wsdl:message>
  <wsdl:message name="DeliverExamOneContentSoapIn">
    <wsdl:part name="parameters" element="tns:DeliverExamOneContent" />
  </wsdl:message>
  <wsdl:message name="DeliverExamOneContentSoapOut">
    <wsdl:part name="parameters" element="tns:DeliverExamOneContentResponse" />
  </wsdl:message>
  <wsdl:portType name="EOServiceSoap">
    <wsdl:operation name="LoopBack">
      <wsdl:input message="tns:LoopBackSoapIn" />
      <wsdl:output message="tns:LoopBackSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="DeliverExamOneContent">
      <wsdl:input message="tns:DeliverExamOneContentSoapIn" />
      <wsdl:output message="tns:DeliverExamOneContentSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="EOServiceSoap" type="tns:EOServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="LoopBack">
      <soap:operation soapAction="http://QuestWebServices/EOService/LoopBack" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeliverExamOneContent">
      <soap:operation soapAction="http://QuestWebServices/EOService/DeliverExamOneContent" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="EOServiceSoap12" type="tns:EOServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="LoopBack">
      <soap12:operation soapAction="http://QuestWebServices/EOService/LoopBack" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeliverExamOneContent">
      <soap12:operation soapAction="http://QuestWebServices/EOService/DeliverExamOneContent" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="EOService">
    <wsdl:port name="EOServiceSoap" binding="tns:EOServiceSoap">
      <soap:address location="https://wssim.labone.com/services/eoservice.asmx" />
    </wsdl:port>
    <wsdl:port name="EOServiceSoap12" binding="tns:EOServiceSoap12">
      <soap12:address location="https://wssim.labone.com/services/eoservice.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>
LABONE_EOS_RESPONSE
