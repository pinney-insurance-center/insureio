class BaseRedisService

  def connection
    if !(defined?(@@connection) && @@connection)
      @@connection =  Redis.new({ host: APP_CONFIG['socket']['redis_host'], port: APP_CONFIG['socket']['redis_port'] })
    end
    @@connection
  end
end

class RedisWriterService <  BaseRedisService
  attr_reader :key, :value

  def initialize key, value
    @key, @value = key, value
  end

  def process
    connection.set key, value
  end
end

class RedisReaderService < BaseRedisService
  attr_reader :key

  def initialize key
    @key = key
  end

  def process
    connection.get key
  end
end
