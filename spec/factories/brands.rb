require 'active_record_base_sample.rb'
#Note: For most tests, it will be more useful to use the brand defined though an instance of the user factory.
#This is due to the circular nature of relationships between them.
#User belongs to one default brand, has many owned brands, and has and belongs to many brands as a member.
#All of these relationships are validated when saving a user, saving a brand, or saving a consumer.

$last_brand_id_from_memory ||= Brand.table_exists? ? Brand.maximum(:id).to_i.next : 0

FactoryBot.define do
  factory :brand, :class => 'Brand' do
    contactable_fields
    id { $last_brand_id_from_memory=[ Brand.maximum(:id).to_i, $last_brand_id_from_memory ].max+1; $last_brand_id_from_memory }
    tenant_id             { Thread.current[:tenant_id] || 0 }
    initial_status_type_id{ Crm::StatusType::PRESETS[:prospect][:id] }
    lead_types            { build_list(:lead_type, 3) }
    referrers             { build_list(:referrer, 3) }
  end

  factory :brand_w_assoc, parent: :brand do
    contactable_fields_w_assoc
  end

  factory :brand_w_assoc_b, parent: :brand do
    contactable_fields_w_assoc_b
    initial_status_type_id{ Crm::StatusType::PRESETS[:prospect][:id] }
  end
end
