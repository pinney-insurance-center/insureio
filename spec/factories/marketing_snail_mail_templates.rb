# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_snail_mail_template, :class => 'Marketing::SnailMail::Template', parent: :marketing_template do
    name {Forgery::Name.first_name}
    body {"MyText"}
  end
end
