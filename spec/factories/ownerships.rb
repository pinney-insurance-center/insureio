# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  sequence :ownership_value do |n|
    enum = ['global','user','user and descendants']
    enum[n % enum.length]
  end
end

FactoryBot.define do
  factory :ownership do
    value {generate :ownership_value}
  end

  factory :ownership_global, :class => 'Enum::Ownership' do
    value {'global'}
  end
end
