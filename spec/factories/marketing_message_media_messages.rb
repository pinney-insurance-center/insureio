# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_message_media_message, :class => 'Marketing::MessageMedia::Message' do
    template_id     { 1 }
    body            { "MyText" }
    target_id       { 1 }
    target_type     { 'Consumer' }
    brand_id        { 1 }
    recipient       { 9179328589 }
    sender_id       { 1 }
    failed_attempts { 1 }
  end
end
