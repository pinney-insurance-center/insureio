# Read about factories at https://github.com/thoughtbot/factory_bot
FactoryBot.define do
  factory :usage_staff_assignment, :class => 'Usage::StaffAssignment' do
    administrative_assistant_id { nil }
    case_manager_id             { nil }
    manager_id                  { nil }
    policy_specialist_id        { nil }
    application_specialist_id   { nil }
    underwriter_id              { nil }
    insurance_coordinator_id    { nil }
  end

  factory :usage_staff_assignment_w_sequential_ids, parent: :usage_staff_assignment do
    sequence(:administrative_assistant_id) { |n| n * 8 }
    case_manager_id           {|o| o.administrative_assistant_id + 1}
    manager_id                {|o| o.administrative_assistant_id + 2}
    policy_specialist_id      {|o| o.administrative_assistant_id + 3}
    application_specialist_id {|o| o.administrative_assistant_id + 4}
    underwriter_id            {|o| o.administrative_assistant_id + 5}
    insurance_coordinator_id  {|o| o.administrative_assistant_id + 6}
    agent_id                  {|o| o.administrative_assistant_id + 7}
  end

  factory :usage_staff_assignment_w_assoc, parent: :usage_staff_assignment do
    association :owner,                    factory: :user
    association :administrative_assistant, factory: :user
    association :case_manager,             factory: :user
    association :manager,                  factory: :user
    association :policy_specialist,        factory: :user
    association :application_specialist,   factory: :user
    association :underwriter,              factory: :user
    association :insurance_coordinator,    factory: :user
  end
end
