# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :crm_physician, :class => 'Crm::Physician' do
    address          { "#{Forgery(:address).street_address}\n#{Forgery(:address).city}, #{Forgery(:address).state_abbrev} #{Forgery(:address).zip}" }
    findings         { "MyString" }
    last_seen        { 1.year.ago }
    name             { "MyString" }
    phone            { Forgery::Address.phone }
    reason           { "MyString" }
    years_of_service { rand(20) }
  end
end
