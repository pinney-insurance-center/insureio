# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :reporting_search, :class => 'Reporting::Search' do
    ownership_id   { 1 }
    name           { "MyString" }
    criteria       { {consumer:{full_name:'Alice'}} }
    fields_to_show {{owner:{name:1}}}
  end
end
