# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_email_template, :class => 'Marketing::Email::Template', parent: :marketing_template do
    body         { "MyText" }
    subject      { "MyString" }
    name         { Forgery::Name.first_name }
    enabled      { true }
  end
end
