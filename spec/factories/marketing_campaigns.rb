# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_campaign, :class => 'Marketing::Campaign' do
    name      { Forgery::Name.first_name }
    ownership { Enum::Ownership::sample }
  end
end
