# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_email_smtp_server, :class => 'Marketing::Email::SmtpServer', aliases:[:smtp_config] do
    host         { Forgery::Internet.domain_name }
    username     { Forgery::Internet.user_name }
    password     { "MyString" }
    password_confirmation {password}
    address      { "#{username}@#{host}" }
    port         { 25 }
    tls          { (0..2).to_a.sample }
  end
end
