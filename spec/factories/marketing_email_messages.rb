# Read about factories at https://github.com/thoughtbot/factory_bot
require 'forgery'

FactoryBot.define do
  factory :marketing_email_message, :class => 'Marketing::Email::Message' do
    subject         { "MyString" }
    topic           { "MyString" }
    body            { "MyText" }
    recipient       { Forgery::Internet.email_address }
    brand_id        { 1 }
    template_id     { 1 }
    sender_id       { 1 }
    failed_attempts { 1 }
  end
end
