# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :phone, :class => 'Phone' do
    value { "2%09d" % rand(999999999) }#they all start with 2. simpler than generating a random first digit in range 2-9.
    ext   { "%03d" % rand(9999999999) }
    sequence(:phone_type_id){|n| (n%3)+1}#generates phones of type 1, 2, and 3 (in order! important if you want your sample data to have one of each)
  end
  factory :blank_phone, parent: :phone do
    value         {nil}
    ext           {nil}
    phone_type_id {nil}
  end
end
