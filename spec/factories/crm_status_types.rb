FactoryBot.define do
  factory :crm_status_type, class:'Crm::StatusType' do
    name                { Forgery::Name.company_name }
    indicates_duplicate { [true,false].sample }
    indicates_test      { [true,false].sample }
  end
end