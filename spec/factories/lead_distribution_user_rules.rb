# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :lead_distribution_user_rule, :class => 'LeadDistribution::UserRule' do
    count {rand(3)}
    cursor_id {rand(999)}
    user_id {rand(999)}
    association :user
  end
end
