# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_template, :class => 'Marketing::Template' do
    name        {Forgery::Name.first_name}
    description {"This is a desc"}
    subject     {"subject Line"}
    body        {"You should be using this email with your eyes. Use eyes."}
    template_purpose { Enum::MarketingTemplatePurpose.sample }
  end
end
