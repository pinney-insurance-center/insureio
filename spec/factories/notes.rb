# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :note, :class => 'Note' do
    note_type_id           { Enum::NoteType::sample }
    sequence(:text        ){|n| "MyText+#{n}" }
    #First note will be critical only, second will be personal only, and third will be confidential only.
    #After that, they will be random.
    sequence(:critical    ){|n| n<4 ? n==1 : [true,false].sample }
    sequence(:personal    ){|n| n<4 ? n==2 : [true,false].sample }
    sequence(:confidential){|n| n<4 ? n==3 : [true,false].sample }
  end
end
