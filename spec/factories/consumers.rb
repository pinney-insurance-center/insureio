require 'active_record_base_sample.rb'

$last_consumer_id_from_memory ||= Consumer.table_exists? ? Consumer.maximum(:id).to_i.next : 0

FactoryBot.define do

  factory :consumer, class: 'Consumer' do
    id { $last_consumer_id_from_memory=[ Consumer.maximum(:id).to_i, $last_consumer_id_from_memory ].max+1; $last_consumer_id_from_memory }
    tenant_id           { Thread.current[:tenant_id] || 0 }
    association :agent, factory: :user
    brand               { agent.default_brand }
    contactable_fields
    gender              { [true,false].sample }
    preferred_contact_method { Enum::ContactMethod::sample }
    addresses           { build_list :address, 2 }
    emails              { build_list :email_address, 2 }
    phones              { build_list :phone, 3 }
    birth_state_id      { Enum::State::sample }
    birth_country_id    { Enum::Country::sample.id }
    citizenship         { Enum::Citizenship::sample }
    dl_expiration       { 1.year.from_now }
    dl_state            { Enum::State::sample }
    dln                 { "D97843" }
    health_info_id      { 1 }
    ip_address          { Forgery::Internet.ip_v4 }
    marital_status      { Enum::MaritalStatus::sample }
    occupation          { Forgery::Name.job_title }
    product_type        { Enum::ProductType::sample }
    ssn                 { "%010d" % rand(9999999999) }
    suffix              { Forgery::Name.suffix }
    years_at_address    { rand * 10 }
    relationship_to_agent { 'roadie' }
    relationship_to_agent_start { 5.years.ago }
  end

  factory :consumer_w_assoc, parent: :consumer do
    contactable_fields_w_assoc
    association :agent,          factory: :user_w_assoc
    association :brand,          factory: :brand
    association :financial_info, factory: :crm_financial_info
    association :health_info,    factory: :crm_health_info_w_assoc_b
    staff_assignment  { build :usage_staff_assignment }
    physicians        { build_list :crm_physician, 2 }
    notes             { build_list :note, 3 }
    tags              { %w[foo-bar bazqux] }
  end

  factory :consumer_w_assoc_b, parent: :consumer do
    contactable_fields_w_assoc_b
    association :agent, factory: :user_w_assoc_b
    financial_info      { build :crm_financial_info }
    health_info         { build :crm_health_info_w_assoc_b }
    brand               { build :brand_w_assoc_b }
    staff_assignment    { build :usage_staff_assignment }
  end
end
