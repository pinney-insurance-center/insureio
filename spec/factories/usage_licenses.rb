# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :usage_license, :class => 'Usage::License' do
    status_id      { 1 }
    state_id       { Enum::State.all.sample }
    corporate      { [true,false].sample }
    effective_date { Date.today }
    expiration     { Date.today + 2.days }
    number         { "7894561230" }
    expiration_warning_sent { false }
  end
end
