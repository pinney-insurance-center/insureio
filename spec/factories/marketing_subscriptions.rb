# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_subscription, :class => 'Marketing::Subscription' do
  	association :campaign, factory: :marketing_campaign
  	person         { nil }
  	queue_num      { 1 }
  	order_in_queue { 1 }
  	active         { 0 }
  end
end
