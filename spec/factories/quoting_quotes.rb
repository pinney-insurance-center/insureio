# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :quoting_quote, :class => 'Quoting::Quote' do
    face_amount           { APP_CONFIG['face_amounts'].sample }
    planned_modal_premium { rand 1_000 }
    premium_mode          { Enum::PremiumModeOption::sample }
  end

  factory :quoting_result, parent: :quoting_quote do
    carrier_health_class  { "preferred for us" }
    plan_name             { "my plan" }
    # enums
    duration     { Enum::TliDurationOption::sample }
    health_class { Enum::TliHealthClassOption::sample }
    product_type { Enum::ProductType::sample }
    # associations
    association :carrier, factory: :carrier
  end

end
