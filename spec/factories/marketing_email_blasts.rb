# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do

  #This factory is not valid. Use the other two below it.
  factory :marketing_email_blast, :class => 'Marketing::Email::Blast' do
    user     {build_stubbed(:user)}
    template {build_stubbed(:marketing_email_template, body: "Body Text", subject: "Subject Text")}
  end

  factory :blast_with_custom_list, parent: :marketing_email_blast do
    recipients {'foo@bar.com baz@qux.com fly@away.com'}
  end

  factory :blast_with_search, parent: :marketing_email_blast do
    search {build_stubbed :reporting_search}
  end
end
