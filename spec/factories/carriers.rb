# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :carrier, class:'Carrier' do
    naic_code { 100000+rand(99999) }
    name { Forgery::Name.company_name }
  end
end
