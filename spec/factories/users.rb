require 'active_record_base_sample.rb'

def after_cb user
  user.default_brand&.owner= user # owner will be null for certain types of user regardless of what the DB says
end

$last_user_id_from_memory ||= User.table_exists? ? User.maximum(:id).to_i.next : 0

FactoryBot.define do 

  factory :user, aliases: [:parent, :owner], :class => 'User' do
    contactable_fields
    id                    { $last_user_id_from_memory=[ User.maximum(:id).to_i, $last_user_id_from_memory ].max+1; $last_user_id_from_memory }
    tenant_id             { Thread.current[:tenant_id] || 0 }
    emails                { build_list :email, 1 }
    commission_level      { Enum::CommissionLevel::sample }
    can_strict_brand_multiple { true }
    tos                   { true }
    password              { "1234567890" }
    password_confirmation { "1234567890" }
    quoter_key            { SecureRandom.hex(16) }
    api_key               { SecureRandom.hex(16) }
    membership_id         { Enum::Membership.id('Free') }
    premium_limit         { 10 ** rand(3) * 100000 }
    association :default_brand,    factory: :brand
    #These are defined in tracking_tables.rb.
    lead_sources          { build_list(:source,  3) }
    %w[build stub create].each do |hook|
      after(hook.to_sym) { |user| after_cb(user) }
    end
  end

  factory :agent, parent: :user do
    can_strict_lead_distribution { true }
  end
  
  factory :user_w_assoc, parent: :user do
    contactable_fields_w_assoc
    association :manager, factory: :user
    association :parent,  factory: :user
    association :sales_support_field_set, factory: :usage_sales_support_field_set
    association :staff_assignment,        factory: :usage_staff_assignment_w_assoc
  end

  factory :user_w_assoc_b, parent: :user do
    contactable_fields_w_assoc_b
    association :manager,                 factory: :user
    association :parent,                  factory: :user
    association :sales_support_field_set, factory: :usage_sales_support_field_set
  end



  factory :users_in_hierarchy, parent: :user do
  #for testing user permissions page, which deals with user hierarchy
  #each user will have parent_id matching id of the last generated
    #sequence(:login){ |n| "mylogin-#{n}" }
    sequence(:full_name){ |n| "First#{n} Last" }
    #password "123456"
    #password_confirmation "123456"
    sequence(:parent_id){ |n| n-1 }
  end
end