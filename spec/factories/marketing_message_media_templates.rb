# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :marketing_message_media_template, :class => 'Marketing::MessageMedia::Template', parent: :marketing_template do
    body         { "MyText" }
    name         { Forgery::Name.first_name }
    enabled      { true }
  end
end
