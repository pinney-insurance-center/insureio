FactoryBot.define do
  factory :usage_security_answer, :class => 'Usage::SecurityAnswer' do
    question_1_id {1}
    question_2_id {1}
    question_1_value {'simple answer'}
    question_2_value {'simple answer'}
  end
end