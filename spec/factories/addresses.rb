# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :address, :class => 'Address' do
    address_type { Enum::AddressType.all.sample }
    street{ Forgery(:address).street_address }
    city  { Forgery(:address).city }
    state { Enum::State.sample }
    zip   { Forgery(:address).zip }
  end
end
