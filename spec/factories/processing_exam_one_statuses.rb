FactoryBot.define do
  factory :exam_one_status, :class => 'Processing::ExamOne::Status' do
    completed_at  { Forgery::Date.date }
    description   { Forgery::Basic.password }
  end
end
