# Read about factories at https://github.com/thoughtbot/factory_bot


FactoryBot.define do
  factory :lead_type, :class => 'Crm::LeadType' do
    text { Forgery::Basic.text }
  end

  factory :referrer, :class => 'Crm::Referrer' do
    text { Forgery::Basic.text }
  end

  factory :source, :class => 'Crm::Source' do
    text { Forgery::Basic.text }
  end
end