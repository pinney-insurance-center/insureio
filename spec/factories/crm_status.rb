FactoryBot.define do
	factory :crm_status, class: 'Crm::Status' do
		active         { true }
		statusable     { nil }
		creator        { nil }
		current        { true }
		status_type    { build :crm_status_type }
	end
end
