FactoryBot.define do
  factory :usage_docusign_cred, class: 'Usage::DocusignCred' do
    account_id  { '81ed0033-1706-4fb6-a36f-3769d0425d4b' }
    email       { 'eclarizio@pinneyinsurance.com' }
    password    { 'ECDocusign%#!&' }
  end
end