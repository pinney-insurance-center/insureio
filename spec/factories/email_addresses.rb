# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :email_address, aliases:[:email], :class => 'EmailAddress' do
    value { generate :unique_email_value }
    #value { Forgery::Internet.email_address }
  end

  sequence :unique_email_value do |n|
    u_n= Forgery::Internet.user_name
    host=Forgery::Internet.domain_name
    "#{u_n}#{n}@#{host}"
  end

end
