FactoryBot.define do
  factory :crm_case, class: 'Crm::Case' do
    active                      { true }
    approved_premium_due        { rand * rand(100) }
    bind                        { false }
    cross_sell                  { false }
    effective_date              { nil }
    exam_company                { "foooo" }
    exam_num                    { "3rjkl234" }
    exam_status                 { "not done" }
    exam_time                   { nil }
    is_a_preexisting_policy     { false }
    ipo                         { false }
    policy_number               { "342k423" }
    underwriter_assist          { false }
  end

  factory :crm_case_w_assoc, parent: :crm_case do
    notes             { build_list :note, 1 }
    #Guarantee 1 quoting_details record per sales stage (not including "Placed")
    #quoting_details   { build :quoting_result, sequence(:sales_stage_id){|n| n%4+1}, 4 }
    after(:build) do |c|
      c.quoting_details=
      (0..3).to_a.map do |n|
        build :quoting_result, crm_case: c, sales_stage_id: n%4+1
      end
    end
    after(:stub) do |c|
      c.quoting_details=
      (0..3).to_a.map do |n|
        q=build :quoting_result, crm_case: c, sales_stage_id: n%4+1
        q.stub(:update_case)
        q.stub(:update_case_quoted_at)
        q
      end
    end
    after(:create) do |c|
      c.quoting_details=
      (0..3).to_a.map do |n|
        build :quoting_result, crm_case: c, sales_stage_id: n%4+1
      end
    end
  end


  factory :crm_case_w_beneficiary_and_owner, parent: :crm_case do
    stakeholder_relationships{ build_list :owner_relationship, 1 }
    after(:create) do |crm_case, evaluator|
        FactoryBot.create_list(:crm_beneficiary, 1, case: crm_case, contingent: false)
    end
  end

  factory :crm_case_w_connection, parent: :crm_case do
    association :consumer, factory: :consumer
  end

end
