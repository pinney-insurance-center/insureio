FactoryBot.define do
  factory :task, :class => 'Task' do
    due_at       { (Date.today + rand(10) - 5).to_time }
    label        { Forgery::Name.company_name }
    origin_type  { 'Crm::Status' }
  end
end
