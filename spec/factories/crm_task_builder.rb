FactoryBot.define do
  factory :crm_task_builder, :class => 'Crm::TaskBuilder' do
    template_type {['Marketing::Email::Template', 'Marketing::SnailMail::Template', 'Marketing::MessageMedia::Template'].sample}
    label         {Forgery::Name.company_name}
  end
end
