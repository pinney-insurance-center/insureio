FactoryBot.define do

  factory :attachment, class: 'Attachment' do
    # `StubbedFileForUpload` used when populating "existing" attachments for tests
    StubbedFileForUpload = Struct.new(:read) unless defined?('StubbedFileForUpload')

    file_name { (Forgery::Internet.user_name)+'.jpg' }
    # Removed the constant declaration that was previously on this stub so
    # that FactoryBot doesn't complain every time this loads that the constant
    # has already been defined.
    file_for_upload { StubbedFileForUpload.new }
  end

end
