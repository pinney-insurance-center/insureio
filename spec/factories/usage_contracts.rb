# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :usage_contract, :class => 'Usage::Contract' do
    carrier_id     { 1 }
    status_id      { 1 }
    user_id        { 1 }
    corporate      { [true,false].sample }
    effective_date { Date.today }
    expiration     { Date.today + 2.days }
    carrier_contract_id { "MyString" }
  end
end
