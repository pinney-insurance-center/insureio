# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :usage_aml_vendor, :class => 'Usage::AmlVendor' do
    name { "MyString" }
  end
end
