# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :crm_consumer_relationship, :class => 'Crm::ConsumerRelationship' do
    relationship_type { Enum::RelationshipType.sample }
  end

  factory :crm_stakeholder_relationship, parent: :crm_consumer_relationship do
    #
  end

  factory :owner_relationship, parent: :crm_stakeholder_relationship do
    is_owner {true}
  end

  factory :owner_relationship_w_assoc, parent: :owner_relationship do
    relationship_type { Enum::RelationshipType.sample }
    stakeholder {build :consumer }
  end

  factory :bene_relationship, parent: :crm_stakeholder_relationship do
    is_beneficiary {true}
  end

end
