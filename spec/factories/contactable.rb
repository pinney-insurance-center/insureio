FactoryBot.define do

  trait :contactable_fields do
    full_name                   { Forgery::Name.full_name }
    company                     { Forgery::Name.company_name }
    birth_or_trust_date         { Forgery::Date.date past:true, min_delta:1, max_delta:(365*60) }
    preferred_contact_method_id { Enum::ContactMethod::sample }
  end

  trait :contactable_fields_w_assoc do
    contactable_fields
    addresses { build_list :address, 2 }
    emails    { build_list :email, 2 }
    phones    { build_list :phone, 3 }
  end

   trait :contactable_fields_w_assoc_b do
    contactable_fields
    phones    { [build(:phone, phone_type_id:1), build(:phone, phone_type_id:2)] }
    addresses { build_list :address, 2 }
    emails    { build_list :email, 3 }
  end

end