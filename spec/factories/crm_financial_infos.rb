# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :crm_financial_info_mostly_blank, :class => 'Crm::FinancialInfo' do
    #
  end

  factory :crm_financial_info, :class => 'Crm::FinancialInfo' do
    asset_home_equity {             rand(1000) * 10000 }
    asset_investments {             rand(1000) * 10000 }
    asset_pension {                 rand(1000) * 10000 }
    asset_real_estate {             rand(1000) * 10000 }
    asset_savings {                 rand(1000) * 10000 }
    asset_checking {                rand(1000) * 10000 }
    asset_earned_income {           rand(1000) * 10000 }
    asset_life_insurance {          rand(1000) * 10000 }
    asset_retirement {              rand(1000) * 10000 }
    assumed_interest_rate {         rand(1000) * 10000 }
    income {                        rand(1000) * 1000 }
    bankruptcy          {           true }
    bankruptcy_declared {           rand(Time.at(0,0)..Time.now) }
    bankruptcy_discharged {         rand(Time.at(0,0)..Time.now) }
    liability_auto {                rand(100) * 100 }
    liability_credit {              rand(100) * 100 }
    liability_education {           rand(100) * 100 }
    # liability_estate_settlement { rand(100) * 100 }
    liability_mortgage_1 {          rand(100) * 100 }
    liability_mortgage_2 {          rand(100) * 100 }
    liability_other {               rand(100) * 100 }
    liability_final_expense {       rand(100) * 100 }
    liability_heloc {               rand(100) * 100 }
    liability_personal_loans {      rand(100) * 100 }
    liability_student_loans {       rand(100) * 100 }
    liability_years_income_needed { rand(100) * 100 }
    net_worth_specified {           rand(1000) * 10000 }
  end
end
