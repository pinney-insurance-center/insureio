# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :webs, :class => 'Web' do
  	web_type { Enum::WebType.all.sample }
    url { Forgery::Internet.domain_name }
  end
end
