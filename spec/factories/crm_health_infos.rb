# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :crm_health_info, :class => 'Crm::HealthInfo' do
    feet   { rand(3)+4 }
    inches { rand(12) }
    weight { rand(200) + 100 }
    #All health conditions default to false.
  end

  factory :crm_health_info_w_assoc, parent: :crm_health_info do
    #
  end

  factory :crm_health_info_w_assoc_b, parent: :crm_health_info do
    #
  end

  # A health info with poor health, such as is likely to trigger no results from a quoter
  factory :crm_health_info_questionable, parent: :crm_health_info do
    bp_control_start           { rand(20).years.ago }
    bp_diastolic               { (APP_CONFIG['bp_diastolic_min']..APP_CONFIG['bp_diastolic_max']).sample }
    bp_systolic                { (APP_CONFIG['bp_systolic_min']..APP_CONFIG['bp_systolic_max']).sample  }
    bp_last_treatment          { rand(20).years.ago }

    cholesterol_level          { (APP_CONFIG['cholesterol_min']..APP_CONFIG['cholesterol_max']).sample }
    cholesterol_control_start  { rand(20).years.ago }
    cholesterol_hdl            { (APP_CONFIG['cholesterol_hdl_step'].step(APP_CONFIG['cholesterol_hdl_min']..APP_CONFIG['cholesterol_hdl_max']) ).sample }
    cholesterol_last_treatment { rand(20).years.ago }

    tobacco_cigarettes_per_day { rand(50)}
    tobacco_cigarette_last     { rand(20).years.ago }
    tobacco_cigars_per_month   { rand(50)}
    tobacco_cigar_last         { rand(20).years.ago }
    tobacco_nicotine_patch_or_gum_last { rand(20).years.ago }
    tobacco_pipe_last          { rand(20).years.ago }
    tobacco_chewed_last        { rand(20).years.ago }


    moving_violation_last_6_mo {rand(4)}
    moving_violation_last_1_yr {rand(4)}
    moving_violation_last_2_yr {rand(4)}
    moving_violation_last_3_yr {rand(4)}
    moving_violation_last_5_yr {rand(4)}
    moving_violation_last_dl_suspension { rand(20).years.ago }
    moving_violation_last_dui_dwi { rand(20).years.ago }
    moving_violation_last_reckless_driving { rand(20).years.ago }
    moving_violation_penultimate_car_accident { rand(20).years.ago }

    criminal             { [true, false].sample }
    hazardous_avocation  { [true, false].sample }
    diabetes_1           {[false,true].sample}
    diabetes_2           {[false,true].sample}
    diabetes_neuropathy  {[false,true].sample}
    anxiety              {[false,true].sample}
    depression           {[false,true].sample}
    epilepsy             {[false,true].sample}
    parkinsons           {[false,true].sample}
    mental_illness       {[false,true].sample}
    alcohol_abuse        {[false,true].sample}
    drug_abuse           {[false,true].sample}
    elft                 {[false,true].sample}
    hepatitis_c          {[false,true].sample}
    rheumatoid_arthritis {[false,true].sample}
    asthma               {[false,true].sample}
    copd                 {[false,true].sample}
    emphysema            {[false,true].sample}
    sleep_apnea          {[false,true].sample}
    crohns               {[false,true].sample}
    ulcerative_colitis_iletis {[false,true].sample}
    weight_loss_surgery  {[false,true].sample}
    breast_cancer        {[false,true].sample}
    prostate_cancer      {[false,true].sample}
    skin_cancer          {[false,true].sample}
    internal_cancer      {[false,true].sample}
    atrial_fibrillations {[false,true].sample}
    heart_murmur_valve_disorder {[false,true].sample}
    irregular_heart_beat {[false,true].sample}
    heart_attack         {[false,true].sample}
    stroke               {[false,true].sample}
    vascular_disease     {[false,true].sample}
    after :create do |health_info, evaluator|
      create_list :quoting_relatives_disease, rand(4), health_info:health_info
    end
  end
end