require 'spec_helper'

describe Esp::XmlReader do
  before do
    User.find_by(id: user_id) || create(:user, id: user_id, tenant: tenant) # Ensure an agent exists for the NMB tenant
    # Set current tenant so search for Consumer will find ESP's record
    Thread.current[:tenant_id] = tenant.id # nmb.insureio.com
  end

  let (:user_id) { tenant.default_parent_id }
  let (:tenant) { Enum::Tenant.find 10 }

  it 'updates Consumer, Case, & Phones when extant' do
    kase0 = create :crm_case_w_connection
    kase0.consumer.phones.create value: '9998675309'
    xml = EXAMPLE.sub('<ClientData>{}</ClientData>', "<ClientData>{\"case_id\":#{kase0.id}}</ClientData>")
    expect {
      kase1 = Esp::XmlReader.new(xml).update
      expect(kase1.policy_number).to eq('MX01999992')
      expect(kase1.consumer.full_name).to eq('SHAWN R JONES')
      expect(kase1.consumer.phones.last.value).to eq('9998675309')
    }.to change(Consumer, :count).by(0)
    .and change(Crm::Case, :count).by(0)
    .and change(Phone, :count).by(0)
  end

  EXAMPLE = <<-EOF
    <Root>
      <PI>
        <LastName_TX>JONES</LastName_TX>
        <FirstName_TX>SHAWN</FirstName_TX>
        <MiddleName_TX>R</MiddleName_TX>
        <Address1_TX>2999 BIG Bob RD</Address1_TX>
        <Address2_TX />
        <City_TX>BeverlyHills</City_TX>
        <State_TX>CA</State_TX>
        <Zip_TX>50555</Zip_TX>
        <HomePhone_TX>9998675309</HomePhone_TX>
        <CellPhone_TX></CellPhone_TX>
        <BusinessPhone_TX></BusinessPhone_TX>
        <BusinessPhoneExt_TX></BusinessPhoneExt_TX>
        <SocSec_TX>555229999</SocSec_TX>
        <BirthDate_DT>10/1/1955</BirthDate_DT>
        <Sex_TX>M</Sex_TX>
        <Occupation_TX></Occupation_TX>
        <TimeZone_TX/>
        <BestTimetoCall_TX></BestTimetoCall_TX>
        <PolicyNum_TX>MX01999992</PolicyNum_TX>
        <OrderDate_DT>5/05/2020</OrderDate_DT>
        <CompanyCode_TX>National Mutual Benefit</CompanyCode_TX>
        <Initials_TX/>
        <Requestor_TX>UserXYZ</Requestor_TX>
        <Contact_TX/>
        <Agent_TX>Smith, JOHN T</Agent_TX>
        <FaceAmount_MNY>100000</FaceAmount_MNY>
        <CoverageType_TX></CoverageType_TX>
        <SpecialAttention_TX>Put any free form text here for special instructions</SpecialAttention_TX>
        <TelNum_TX/>
        <MVRNum_TX/>
        <ClientData>{}</ClientData>
        <Orders>
           <Order>
            <OrderCode_TX>3100</OrderCode_TX>
            <OrderNotes_TX>Tele App Pt 1 Request</OrderNotes_TX>
            <MDFirstName_TX/>
            <MDLastName_TX/>
            <MDFacilityName_TX/>
            <MDAddress_TX/>
            <MDCity_TX/>
            <MDState_TX/>
            <MDZip_TX/>
            <MDPhone_TX/>
          </Order>
          <Order>
            <OrderCode_TX>3100P2</OrderCode_TX>
            <OrderNotes_TX>Tele App Pt 2 Request</OrderNotes_TX>
            <MDFirstName_TX/>
            <MDLastName_TX/>
            <MDFacilityName_TX/>
            <MDAddress_TX/>
            <MDCity_TX/>
            <MDState_TX/>
            <MDZip_TX/>
            <MDPhone_TX/>
          </Order>
          <Order>
            <OrderCode_TX>3105</OrderCode_TX>
            <OrderNotes_TX>Underwriting Request</OrderNotes_TX>
            <MDFirstName_TX/>
            <MDLastName_TX/>
            <MDFacilityName_TX/>
            <MDAddress_TX/>
            <MDCity_TX/>
            <MDState_TX/>
            <MDZip_TX/>
            <MDPhone_TX/>
           </Order>
        </Orders>
      </PI>
    </Root>
  EOF
end
