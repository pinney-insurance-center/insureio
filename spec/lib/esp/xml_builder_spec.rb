require 'spec_helper'

describe Esp::XmlBuilder do
  before do
    Thread.current[:tenant_id] = 10 # NMB
  end

  let(:state) { Enum::State.efind abbrev: 'CA' }
  let(:address) { build :address, street: '2999 BIG Bob RD', city: 'Beverly Hills', state: state, zip: '50555' }
  let(:finance) { create :crm_financial_info }
  let(:health) { create :crm_health_info }
  let(:phones) { [9998675309, 5558887777].map.with_index do |v, i|
    build :phone, value: v, phone_type_id: 1+i, ext: nil
  end }
  let(:brand) { create :brand }
  let(:aor) { create :user, full_name: "Nikolaus Lloyd", brands: [brand] }
  let(:agent) { create :user, full_name: "Flannel Pants", brands: [brand], agent_of_record: aor }
  let(:consumer) { Consumer.create! birth_or_trust_date: Date.new(1955,10,1), agent: agent, brand: brand, gender: 'male', full_name: 'SHAWN JONES', health_info: health, financial_info: finance, phones: phones, addresses: [address], ssn: '567345234', preferred_contact_time: 'two minutes til midnight' }
  let(:kase) { create :crm_case, consumer: consumer, policy_number: nil, quoting_details:[(create :quoting_quote, face_amount: 321, product_type_name: 'Term')] }

  it 'builds XML for ESP' do
    Timecop.travel Time.zone.local(2020, 5, 2) do
      builder = Esp::XmlBuilder.new kase
      heredoc = <<-EOF
      <Root>
        <PI>
          <LastName_TX>JONES</LastName_TX>
          <FirstName_TX>SHAWN</FirstName_TX>
          <MiddleName_TX/>
          <Address1_TX>2999 BIG Bob RD</Address1_TX>
          <Address2_TX/>
          <City_TX>Beverly Hills</City_TX>
          <State_TX>CA</State_TX>
          <Zip_TX>50555</Zip_TX>
          <HomePhone_TX>9998675309</HomePhone_TX>
          <CellPhone_TX/>
          <BusinessPhone_TX>5558887777</BusinessPhone_TX>
          <BusinessPhoneExt_TX/>
          <SocSec_TX>567345234</SocSec_TX>
          <BirthDate_DT>10/01/1955</BirthDate_DT>
          <Sex_TX>M</Sex_TX>
          <Occupation_TX/>
          <TimeZone_TX/>
          <BestTimetoCall_TX>two minutes til midnight</BestTimetoCall_TX>
          <PolicyNum_TX/>
          <OrderDate_DT>05/02/2020</OrderDate_DT>
          <CompanyCode_TX>National Mutual Benefit</CompanyCode_TX>
          <Initials_TX>IEO</Initials_TX>
          <Requestor_TX>Flannel Pants</Requestor_TX>
          <Contact_TX/>
          <Agent_TX>Nikolaus Lloyd</Agent_TX>
          <AgentIdNumber_TX>#{aor.id}</AgentIdNumber_TX>
          <FaceAmount_MNY>321</FaceAmount_MNY>
          <CoverageType_TX>Term</CoverageType_TX>
          <SpecialAttention_TX>Health class: Best Class</SpecialAttention_TX>
          <TelNum_TX/>
          <MVRNum_TX/>
          <Client_Data>{"case_id":#{kase.id}}</Client_Data>
          <Orders>
            <Order>
              <OrderCode_TX>3100</OrderCode_TX>
              <OrderNotes_TX>Tele App Pt 1 Request</OrderNotes_TX>
              <MDFirstName_TX/>
              <MDLastName_TX/>
              <MDFacilityName_TX/>
              <MDAddress_TX/>
              <MDCity_TX/>
              <MDState_TX/>
              <MDZip_TX/>
              <MDPhone_TX/>
            </Order>
            <Order>
              <OrderCode_TX>3100P2</OrderCode_TX>
              <OrderNotes_TX>Tele App Pt 2 Request</OrderNotes_TX>
              <MDFirstName_TX/>
              <MDLastName_TX/>
              <MDFacilityName_TX/>
              <MDAddress_TX/>
              <MDCity_TX/>
              <MDState_TX/>
              <MDZip_TX/>
              <MDPhone_TX/>
            </Order>
            <Order>
              <OrderCode_TX>3105</OrderCode_TX>
              <OrderNotes_TX>Underwriting Request</OrderNotes_TX>
              <MDFirstName_TX/>
              <MDLastName_TX/>
              <MDFacilityName_TX/>
              <MDAddress_TX/>
              <MDCity_TX/>
              <MDState_TX/>
              <MDZip_TX/>
              <MDPhone_TX/>
            </Order>
          </Orders>
        </PI>
      </Root>
      EOF
      expect(builder.to_s).to eq heredoc.gsub(/^ {6}/, '').strip()
    end
  end

  context 'for Iowa' do
    let(:state) { Enum::State.efind abbrev: 'IA' }

    it 'includes extra orders for Iowa' do
      expect(Esp::XmlBuilder.new(kase).to_s).to match /HIV Consent Form/
      expect(Esp::XmlBuilder.new(kase).to_s).to match /Replacement Form\(s\)/
    end
  end
end
