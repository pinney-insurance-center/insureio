require 'spec_helper'

describe Esp::PdfBuilder do
  before do
    Thread.current[:tenant_id] = 10 # NMB
  end

  let(:cons) { create :consumer }
  let(:kase) { create :crm_case_w_assoc, agent: agent, agent_of_record: aor, consumer: cons }
  let(:agent) { create :user }
  let(:aor) { create :user }

  describe '#to_bin' do
    before do
      # Create policy to be replaced
      create :crm_case_w_assoc, replaced_by_id: kase.id
      # Create beneficiaries
      create_list(:consumer, 2).map do |bene|
        Crm::ConsumerRelationship.create!(
          is_beneficiary: true,
          primary_insured: cons,
          crm_case: kase,
          relationship_type_id: 2,
          stakeholder: bene,
          percentage: 50,
        )
      end
    end

    it 'runs for a term case without raising an error' do
      kase.current_details.update! duration_id: 24 # ART / 1yr Level Term
      expect { Esp::PdfBuilder.new(kase).to_bin }.to_not raise_exception
    end

    it 'runs for a whole-life case without raising an error' do
      kase.current_details.update! duration_id: 17 # To Age 121 (No Lapse U/L)
      expect { Esp::PdfBuilder.new(kase).to_bin }.to_not raise_exception
    end
  end
end
