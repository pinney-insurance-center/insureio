require 'spec_helper'

describe Compulife, :vcr do
  # Don't use factories because randomized params will prevent re-use of vcr cassettes
  let(:cons) { Consumer.new }
  let(:kase) { cons.cases.build }
  let(:quote) { kase.quoting_details.build duration_id: 24, face_amount: 1_000_000 }

  it 'raises no errors' do
    expect { Compulife.new(cons, quote).get_quotes }.not_to raise_error
  end

  it 'with moving violations, raises no errors ' do
    cons.health_info.moving_violations << Crm::HealthInfo::MovingViolation.new(date: 3.years.ago, dui_dwi: true)
    expect { Compulife.new(cons, quote).get_quotes }.not_to raise_error
  end
end
