require 'spec_helper'

describe Ixn::IxnToPic do
  let(:agent) { create :agent }
  let(:json) { File.read 'spec/support/ixn/ixn_to_pic.json' }
  let(:params) { JSON.parse(json).with_indifferent_access }
  let(:cons) { create :consumer, brand_id: Brand::NMB_ID }
  let(:kase) { create :crm_case, consumer: cons, quoting_details:[(create :quoting_quote, face_amount: 9999)] }

  shared_examples_for 'IxnToPic' do
    let(:ixn) { Ixn::IxnToPic.new(params) }

    it 'sets address attributes' do
      expect { ixn.run(agent).save }
      .to change { ixn.consumer.addresses&.first&.street }.to '111111111'
    end

    it 'sets phone attributes' do
      expect { ixn.run(agent).save }
      .to change{ ixn.consumer.phones&.first&.value }.to '2628934451'
    end

    it 'sets email attributes' do
      expect { ixn.run(agent).save }
      .to change{ ixn.consumer.emails&.first&.value }.to 'jilling@nmblife.org'
    end

    it 'sets health_info attributes' do
      expect { ixn.run(agent).save }
      .to change{ ixn.consumer&.health_info&.inches }.to 6
    end

    it 'sets financial_info attributes' do
      expect { ixn.run(agent).save
      }.to change{ ixn.consumer.financial_info&.bankruptcy }.to false
    end

    it 'sets quote attributes' do
      quotes_relation = ixn.consumer.quoting_details
      expect { ixn.run(agent).save }
      .to change{ quotes_relation.reload.last&.face_amount }.to 10000
      quote = quotes_relation.last
      expect(quote.premium_mode_id).to eq 4
      expect(quote.planned_modal_premium).to eq 36.71
    end

    it 'sets beneficiaries' do
      Ixn::IxnToPic.new(params).run(agent).save
      kase = Crm::Case.last
      expect(kase.beneficiaries.length).to eq 2
    end

    it 'sets kase.ixn_ext attributes' do
      Ixn::IxnToPic.new(params).run(agent).save
      ext = Crm::Case.last.reload.ixn_ext
      expect(ext.ixn_id).to eq '3ac5d6f4-d3ae-400d-a302-600cddc3349f'
      expect(ext.quote_guid).to eq '076dda41-afa6-4324-b6fd-219d99089dfc'
      expect(ext.src_guid).to eq '387467d1-8b41-4f4f-afd5-80c6b61f8c80'
      expect(ext.heart_stroke_cancer).to eq false
      expect(ext.parent_death_p60).to eq false
      expect(ext.sibling_death_p60).to eq false
    end
  end

  context 'when insureio ids are blank' do
    it 'creates consumer, case, & quote' do
      expect {
        Ixn::IxnToPic.new(params).run(agent).save
      }.to change(Consumer, :count).by(3) # 2 are beneficiaries
      .and change(Crm::Case, :count).by(1)
      .and change(Quoting::Quote, :count).by(1)
      .and change(Note, :count).by(1) # for status change
    end

    it 'creates a non-opportunity case' do
      Ixn::IxnToPic.new(params).run(agent).save
      expect(Crm::Case.last.opportunity?).to be false
    end

    it_behaves_like 'IxnToPic'
  end

  context 'when a case id is present' do
    before { params[:insureio_case_id] = kase.id }

    it 'updates the case' do
      expect {
        Ixn::IxnToPic.new(params).run(agent).save
      }.to change(Crm::Case, :count).by(0)
      .and change { kase.reload.current_details.face_amount }.to(10000)
    end

    it 'sets a fixed sales stage and status type' do
      expect {
        Ixn::IxnToPic.new(params).run(agent).save
      }.to change(Crm::Case, :count).by(0)
      .and change(Quoting::Quote, :count).by(0)
      .and change { kase.reload.sales_stage_id }.to(3) # App Fulfillment
      .and change { kase.reload.status.status_type_id }.to(966)
      .and change { kase.current_details.reload.sales_stage_id }.to(3) # App Fulfillment
    end

    it 'sets rider info' do
      Ixn::IxnToPic.new(params).run(agent).save
      ext = Crm::Case.last.ixn_ext
      expect(ext.wop_rider_quarterly).to eq 7.67
      expect(ext.child_rider_semi_annual).to eq 15.3
      expect(ext.adb_rider_monthly).to eq 11.31
    end

    it 'sets quote guid' do
      Ixn::IxnToPic.new(params).run(agent).save
      quote = Quoting::Quote.last
      expect(quote.ext['ixn_guid']).to eq '076dda41-afa6-4324-b6fd-219d99089dfc'
    end

    it_behaves_like 'IxnToPic'
  end

  it 'sets ext data on IxnExt' do
    params[:agent_notes] = 'baz'
    params[:family_member_names_with_applications] = 'foo'
    params[:previous_address] = 'qux'
    expect {
      Ixn::IxnToPic.new(params).run(agent).save
    }.to change(IxnExt, :count).by(1)
    ext = IxnExt.last
    expect(ext.agent_notes).to eq 'baz'
    expect(ext.fam_w_apps).to eq 'foo'
    expect(ext.prev_addr).to eq 'qux'
    expect(ext.adb_rider_annual).to eq 133 # see spec/support/ixn/ixn_to_pic.json
    expect(ext.wop_rider_semi_annual).to eq 14.75 # see spec/support/ixn/ixn_to_pic.json
  end
end
