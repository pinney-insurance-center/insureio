require 'spec_helper'

describe Ixn::Quotes, :vcr do
  let(:health_class_id) { 1 }
  let(:duration_id) { Enum::TliDurationOption['20 Years'].id }
  let(:health_info) { build :crm_health_info, tobacco: false }
  let(:address) { Address.new state_id: Enum::State['UT'].id }
  let(:consumer) { create :consumer_w_assoc, health_info: health_info, birth: 30.years.ago, addresses_attributes: [address.attributes] }
  let(:kase) { create :crm_case, consumer: consumer, quoting_details: [qq] }
  let(:qq) { build :quoting_quote, consumer: consumer, health_class_id: health_class_id, duration_id: duration_id, face_amount: 250_000 }
  subject { Ixn::Quotes.new qq }

  describe '#get' do
    it 'returns a 200 response' do
      subject.get
      expect(subject.response.code).to eq("200"), subject.response.body
    end

    it 'honors optional settings' do
      consumer.primary_address.state = Enum::State['UT']
      kase.has_rider_ad_and_d = true
      kase.has_rider_waiver_of_premium = 100
      kase.build_ixn_ext
      kase.ixn_ext.child_rider_units = 1
      subject = Ixn::Quotes.new qq, carrier_ids: [140]
      data = subject.get
      expect(data).to be_present, subject.response.body
      expect(data.length).to eq(1)
      quote = OpenStruct.new data.first
      expect(quote.adb_rider).to be true
      expect(quote.child_rider_units).to be 1
      expect(quote.wop_rider_annual).to be > 0
    end

    it 'uses ixn_guid to perform a requote when present' do
      subject.get
      expect(subject.request.uri.to_s).to match /v1\/quotes$/
      first_result = subject.to_quote_objects.first
      new_request = Ixn::Quotes.new first_result
      new_request.get
      expect(new_request.request.uri.to_s).to match /v1\/quotes\/#{first_result.ext[:ixn_guid]}\/re_quote$/
    end
  end

  describe '#to_quote_objects' do
    it 'returns an Array of Quoting::Quote' do
      records = subject.to_quote_objects
      expect(records).to be_a Array
      expect(records).to all( be_a Quoting::Quote )
    end

    it 'supports table rates' do
      table_name = 'Table 7/G - Tobacco'
      selected_health_class = Enum::TliHealthClassOption.efind(name: table_name)
      expect(selected_health_class.name).to eq(table_name)
      qq.health_class = selected_health_class
      subject = Ixn::Quotes.new qq
      records = subject.to_quote_objects
      expect(records.length).to eq 20
      expect(records.map &:health_class).to all( eq selected_health_class )
      expect(records.map &:annual_premium).to all( be_present )
    end
  end

  describe '#to_quote_objects' do
    it 'returns an Array of Quoting::Quote' do
      records = subject.to_quote_objects
      expect(records).to be_a Array
      expect(records).to all( be_a Quoting::Quote )
    end

    it 'supports table rates' do
      table_name = 'Table 7/G - Tobacco'
      selected_health_class = Enum::TliHealthClassOption.efind(name: table_name)
      expect(selected_health_class.name).to eq(table_name)
      qq.health_class = selected_health_class
      subject = Ixn::Quotes.new qq
      records = subject.to_quote_objects
      expect(records.length).to eq 20
      expect(records.map &:health_class).to all( eq selected_health_class )
      expect(records.map &:annual_premium).to all( be_present )
    end
  end

  describe '#valid?' do
    it 'sets errors when values are missing' do
      qq = Quoting::Quote.new crm_case: Crm::Case.new(consumer: Consumer.new)
      subject = Ixn::Quotes.new qq
      expect(subject.valid?).to be false
      expect(subject.errors[:age]).to eq ["can't be blank"]
      expect(subject.errors[:base]).to match_array ["Specify health_categories or table_rates, not both"]
      expect(subject.errors[:face_amount]).to eq ["can't be blank"]
      expect(subject.errors[:gender]).to eq ["can't be blank"]
      expect(subject.errors[:state]).to eq ["can't be blank"]
      expect(subject.errors[:tobacco]).to eq ["can't be blank"]
    end

    it 'returns true when values are valid' do
      expect(subject.valid?).to be(true), subject.errors.full_messages.join("\n")
    end
  end
end
