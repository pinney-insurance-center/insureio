require 'spec_helper'

describe Ixn, :vcr do

  let(:duration) { Enum::TliDurationOption.find 24 }
  let(:health_class) { Enum::TliHealthClassOption.find 1 }
  let(:consumer) { create :consumer_w_assoc, birth: 30.years.ago }
  let(:kase) { create :crm_case, consumer: consumer }
  let(:quote) { kase.current_details.tap { |q|
    q.update! face_amount: 500_000, duration: duration, health_class: health_class
  }}

  describe '::quote' do
    it 'sets Consumer and Case on the output Quotes' do
      res = Ixn.quote(quote)
      expect(res.map &:consumer_id).to all( be consumer.id )
      expect(res.map &:case_id).to all( be kase.id )
    end
  end
end
