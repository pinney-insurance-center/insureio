require 'spec_helper'

describe Acord::Update do

  let(:xml) { File.read 'spec/support/acord/1125.xml' }
  let(:doc) { Nokogiri::XML.parse xml }
  let(:policy_number) { doc.at_xpath('//PolNumber').content.to_i }
  let(:kase) { create :crm_case, policy_number: policy_number }
  let(:request_node) { doc.at_xpath '/TXLife/TXLifeRequest' }
  subject { Acord::Update.new(request_node, 'sbli_202007211101391.xml') }


  describe '#apply' do
    before { kase }

    it 'updates the Crm::Case' do
      expect { subject.apply }
      .to change { kase.statuses.reload.last.id }
      .and change { kase.reload.sales_stage_id }
      .and change { kase.reload.status_type_category_id }
    end

    it 'creates Notes' do
      expect { subject.apply }.to change(Note, :count).by(2)
      expect(Note.last.text).to match(/^Manulife Decision: \[Final\] - Final Underwriting Assessment only. Approved /)
      expect(kase.notes[-2].text).to match(/ changed status from Prospect to Issued - Holding$/)
    end

    it 'creates new Quote (current_details)' do
      expect { subject.apply }.to change { kase.quoting_details.reload.count }.by(1)
      expect(doc.at_xpath('//PlanName').content).to eq('John Hancock Term 10')
      expect(kase.current_details.plan_name).to eq('John Hancock Term 10')
      expect(kase.current_details.planned_modal_premium).to eq(7660)
      expect(kase.current_details.premium_mode_id).to eq(1)
    end

    it 'creates a Status' do
      expect { subject.apply }.to change(Crm::Status, :count).by(1)
      status = Crm::Status.last
      expect(status.statusable_type).to eq('Crm::Case')
      expect(status.statusable_id).to eq(kase.id)
      expect(status.status_type.name).to eq('Issued - Holding')
    end

    it 'does not create a Status when the ACORD Status does not map to any changes' do
      doc.at_xpath('//PolicyStatus')['tc'] = 0
      updater = Acord::Update.new doc.at_xpath('/TXLife/TXLifeRequest'), 'sbli_202007211101391.xml'
      expect { updater.apply }.to change(Crm::Status, :count).by(0)
    end

    context 'when current Case status matches new Case status' do
      before do
        status_type = Crm::StatusType.find_by name: 'Issued - Holding'
        kase.statuses.create! status_type_id: status_type.id, sales_stage_id: status_type.sales_stage_id, status_type_category_id: status_type.status_type_category_id
        kase.update! sales_stage_id: status_type.sales_stage_id
      end

      it 'does not create a Status when new status type is the same as the old one' do
        expect { subject.apply }.to change(Crm::Status, :count).by(0)
      end

      it 'updates current_details if current Quote sales_stage_id matches the new one' do
        expect { subject.apply }.to_not change { kase.quoting_details.reload.count }
        expect(kase.current_details.plan_name).to eq('John Hancock Term 10')
        expect(kase.current_details.planned_modal_premium).to eq(7660)
        expect(kase.current_details.premium_mode_id).to eq(1)
      end
    end
  end
end
