PDA = Processing::Docusign::Api

RSpec.configure do |c|
	c.before(:each) {
		stub_docusign_requests
	}
end

def stub_docusign_requests
  json_headers = {
    "Content-Type" => "application/json; charset=utf-8"
  }

  # stub_request(:get, "#{PDA::Client::API_URL}/login_information").
  # to_return(status: 200, body: LOGIN_INFO_BODY, headers: json_headers)


  # stub_request(:get, /.*\/accounts\/\d+\/templates.*/).
  # to_return(status: 200, body: TEMPLATES_BODY, headers: json_headers)

  # stub_request(:post, /.*\/accounts\/\d+\/envelopes.*/).
  # to_return(status: 201, body: ENVELOPE_FROM_TEMPLATE_BODY, headers: json_headers)
end

LOGIN_INFO_BODY = <<-JSON
{
  "loginAccounts": [
    {
      "name": "The Company!",
      "accountId": "38177",
      "baseUrl": "https://demo.docusign.net/restapi/v2/accounts/38177",
      "isDefault": "true",
      "userName": "Erik Clarizio",
      "userId": "55253dde-fe0a-4f43-87ef-59ec449bce39",
      "email": "eclarizio@pinneyinsurance.com",
      "siteDescription": ""
    }
  ]
}
JSON

ENVELOPE_FROM_TEMPLATE_BODY = <<-JSON
{
  "envelopeId": "a4a54bc4-4f0b-47b5-afc7-71b24f2a2131",
  "uri": "/envelopes/a4a54bc4-4f0b-47b5-afc7-71b24f2a2131",
  "statusDateTime": "2013-08-09T21:10:10.9820691Z",
  "status": "sent"
}
JSON

# The first two aren't shared, the last two are
TEMPLATES_BODY = <<-JSON
{
  "envelopeTemplates": [
    {
      "templateId": "3a6112f8-bff3-469f-8474-deaae5f77006",
      "name": "Auto Insurance Application",
      "shared": "false",
      "password": "",
      "description": "This template is an auto insurance application.",
      "lastModified": "2012-02-15T05:00:00.6330000Z",
      "pageCount": 1,
      "uri": "/templates/3a6112f8-bff3-469f-8474-deaae5f77006",
      "folderName": "Templates",
      "folderUri": "/folders/73768959-ae1f-4567-b3d7-ee8b1f282833"
    },
    {
      "templateId": "6247309b-8e68-42df-af42-23a7a677c091",
      "name": "CA_Metlife",
      "shared": "false",
      "password": "",
      "description": "",
      "lastModified": "2011-10-19T16:27:22.9500000Z",
      "pageCount": 32,
      "uri": "/templates/6247309b-8e68-42df-af42-23a7a677c091",
      "folderName": "Templates",
      "folderUri": "/folders/73768959-ae1f-4567-b3d7-ee8b1f282833"
    },
    {
      "templateId": "ded0aa48-9b69-4b6c-a8d0-b16f0f7d2d9d",
      "name": "CO_Metlife",
      "shared": "true",
      "password": "",
      "description": "",
      "lastModified": "2011-10-17T17:52:55.0530000Z",
      "pageCount": 42,
      "uri": "/templates/ded0aa48-9b69-4b6c-a8d0-b16f0f7d2d9d",
      "folderName": "Templates",
      "folderUri": "/folders/73768959-ae1f-4567-b3d7-ee8b1f282833"
    },
    {
      "templateId": "62b9a7f4-9398-4da4-81b5-313ebad3808a",
      "name": "NY_MetLife",
      "shared": "true",
      "password": "",
      "description": "",
      "lastModified": "2012-02-15T07:14:00.5630000Z",
      "pageCount": 32,
      "uri": "/templates/62b9a7f4-9398-4da4-81b5-313ebad3808a",
      "folderName": "Templates",
      "folderUri": "/folders/0161c973-f937-44f3-9052-ffe2c7de260b"
    },
    {
      "templateId": "c6c0df2b-7197-4194-be9b-aab7494eedd4",
      "name": "CA_Banner",
      "shared": "true",
      "password": "",
      "description": "",
      "lastModified": "2011-11-02T18:10:47.9000000Z",
      "pageCount": 30,
      "uri": "/templates/c6c0df2b-7197-4194-be9b-aab7494eedd4",
      "folderName": "Templates",
      "folderUri": "/folders/73768959-ae1f-4567-b3d7-ee8b1f282833"
    }
  ]
}
JSON

