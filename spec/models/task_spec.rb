require 'spec_helper'
require 'net/http'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end

shared_examples_for "task auto assignment" do
  context 'when role is case manager' do
    let(:role_id) { Enum::UsageRole.id('case manager') }
    it 'gets case manager' do
      task.auto_assign! role_id
      expected_assignment_id.should_not == nil
      task.assigned_to_id.should == expected_assignment_id
    end
  end
  context 'when role is underwriter' do
    let(:role_id) { Enum::UsageRole.id('underwriter') }
    it 'gets underwriter' do
      task.auto_assign! role_id
      expected_assignment_id.should_not == nil
      task.assigned_to_id.should == expected_assignment_id
    end
  end
end

describe Task do

  let(:task_type_id){Enum::TaskType.id('email consumer')}
  let(:task)    {build :task, id:43, person:client, template:template, task_type_id:task_type_id, origin:status}
  let(:status)  {build :crm_status, statusable:sequenceable}
  let(:sequenceable)    {build :crm_case, agent:agent}
  let(:template){build :marketing_email_template, id:44}
  let(:client)  {build :consumer, id:45, agent:agent, brand:brand, email_value:'baz@qux.com'}
  let(:agent)   {build :user, id:46, smtp_servers:build_list(:smtp_config, 3)}
  let(:brand) {build :brand, id:47}

  it "scopes can be called" do
    Task.evergreen.to_a
    Task.active.to_a
    Task.due.to_a
    Task.system_tasks.to_a
  end

  describe 'callbacks' do
    let(:task)       { build :crm_task, sequenceable:sequenceable, person:client, status:status, assigned_to:assignee, completed_by:completer }
    let(:sequenceable)       { build :crm_case_w_assoc, consumer:client }
    let(:client)     { build :consumer_w_assoc}
    let(:status)     { build :crm_status, status_type:status_type }
    let(:status_type){ build :crm_status_type, name:'barfyst' }
    let(:assignee)   { build :user, name:'the given assignee' }
    let(:completer)  { build :user, name:'the xoso completer' }

    before do
      Task.any_instance.stub(:publish_to_redis)
    end

    describe 'infer_tz_offset callback' do
      it 'fires before create and sets tz_offset' do
        task.should_receive(:infer_tz_offset).exactly(1).times.and_call_original
        task.save
        task.tz_offset.should_not be_nil
        task.tz_offset.should == task.person.tz_offset
        task.save
      end
    end

    describe 'infer_status_type_name callback' do
      it 'fires before create and sets status_type_name' do
        task.should_receive(:infer_status_type_name).exactly(1).times.and_call_original
        task.save
        task.status_type_name.should == 'barfyst'
        task.save
      end
    end

    describe 'infer_assigned_to_name callback' do
      it 'fires before create and sets assigned_to_name' do
        task.should_receive(:infer_assigned_to_name).exactly(1).times.and_call_original
        task.assigned_to_name.should be_nil
        task.save
        task.assigned_to_name.should == 'the g assignee'
      end
    end

    describe 'infer_completed_by_name callback' do
      it 'fires before create and sets completed_by_name' do
        task.should_receive(:infer_completed_by_name).exactly(1).times.and_call_original
        task.completed_by_name.should be_nil
        task.save
        task.completed_by_name.should == 'the x completer'
      end
    end

  end

  describe '#auto_assign!' do
    let(:client_staff) { build :usage_staff_assignment_w_sequential_ids }
    let(:sequenceable_staff)   { build :usage_staff_assignment_w_sequential_ids }
    let(:gparent_staff){ build :usage_staff_assignment_w_sequential_ids }
    let(:parent_staff) { build :usage_staff_assignment_w_sequential_ids }
    let(:agent_staff)  { build :usage_staff_assignment_w_sequential_ids }
    let(:gparent)      { double(User, id:1, staff_assignment:gparent_staff, parent:nil) }
    let(:parent)       { double(User, id:2, staff_assignment:parent_staff, parent:gparent) }
    let(:agent)        { double(User, id:3, staff_assignment:agent_staff, parent:parent) }
    let(:client)       { double(Consumer, agent:agent, staff_assignment:client_staff) }
    let(:sequenceable)         { double(Crm::Case, agent:agent, staff_assignment:sequenceable_staff) }
    let(:status)       { double(Crm::Status, statusable:sequenceable) }
    let(:task)         { Task.new }
    let(:role_id)      { Enum::UsageRole.id('sales support') }
    let(:expected_assignment_id) { expected_staff.user_id_for_role_id(role_id) }

    before do
      # ensure that all staff ids are unique so that they can be tested against
      ids = [client, gparent, parent, agent].map{|x| x.staff_assignment.ids}.flatten
      ids.should == ids.uniq
      # stub task methods
      task.stub(:sequenceable) {sequenceable}
      task.stub(:agent) {agent}
      task.stub(:person) {client}
      sequenceable.stub(:staff_assignment).and_return(nil)
    end

    it 'gets staff assignment from agent' do
      agent.should_receive(:staff_assignment).and_return(agent_staff)
      agent.should_not_receive(:parent)
      parent.should_not_receive(:staff_assignment)
      task.auto_assign! role_id
    end
  end

  describe '#status=' do
    it 'sets origin_type' do
      task                    =  Task.new
      task.status             =  Crm::Status.new
      task.origin_type.should == Crm::Status.name
      task.status             =  Marketing::Subscription.new
      task.origin_type.should == Marketing::Subscription.name
    end
  end

  describe '#_send_email' do
    before do
      task.stub(:active?).and_return(true)
      agent.stub(:delayed_update_ascendant_associations).and_return(true)
      allow(agent).to receive(:can?).with(:email).and_return(true)
      User.stub(:find).with(agent.id){agent}
      Consumer.stub(:find).with(client.id){client}
      Brand.stub(:find).with(brand.id){brand}
      Marketing::Email::Template.stub(:find).with(template.id){template}
      task.stub(:save){true}
      task.stub(:update_attribute){true}
      task.stub(:completed_at){nil}
      task.stub(:due?){true}
      task.stub(:person){client}
      Mail::Message.any_instance.stub(:deliver){true}
      Marketing::Email::Message.any_instance.stub(:save){true}
      brand.stub(:email){'foo@' + agent.smtp_servers.last.host}
    end
    context 'lacking smtp config' do
      it 'should call SystemMailer.warning' do
        brand.stub(:email).and_return('foo@bar.com')
        agent.smtp_servers = []
        task.should_receive(:_send_email).and_call_original
        # SystemMailer.should_receive(:warning)
        # expect{ task.execute_from_bg }.to raise_exception(Marketing::SMTPError)
        task.execute_from_bg
      end
    end
    context 'lacking template' do
      it 'should send warning email' do
        task.stub(:active?){ task[:active] != false }
        task.save
        task.template = nil
        task.should_receive(:_send_email).and_call_original
        SystemMailer.should_not receive(:error)
        SystemMailer.should receive(:warning)
        task.execute_from_bg
        task.active.should == false
      end
    end
    context 'given template, client, smtp config' do
      it 'should not raise error or send SystemMailer' do
        SystemMailer.should_not_receive(:error)
        SystemMailer.should_not_receive(:warning)
        task.should_receive(:_send_email).and_call_original
        expect{ task.execute_from_bg }.to_not raise_error
      end
    end
  end

  describe '#update_sequenceable_staff_assignment' do
    let(:task){ build :crm_task, sequenceable:sequenceable,
      role_id:Enum::UsageRole.id('insurance coordinator'),
      task_type_id:Enum::TaskType.id('to-do'),
      assigned_to_id:4, status:status }
    let(:status){ build :crm_status, statusable:sequenceable }
    let(:sequenceable) { build :crm_case}

    context 'when a task has a sequenceable Crm::Case' do
      it 'does not fire when a task is created' do
        task.should_not_receive(:update_sequenceable_staff_assignment)
        task.save
      end

      it 'fires when a task is updated, creates staff assignment, sets field on staff assignment' do
        task.save
        task.sequenceable.staff_assignment.should be nil
        task.should_receive(:update_sequenceable_staff_assignment).and_call_original
        task.assigned_to_id = 5
        expect{ task.save }.to change(Usage::StaffAssignment, :count).by(1)
        task.sequenceable.staff_assignment.should_not be_nil
        task.sequenceable.staff_assignment.insurance_coordinator_id.should == 5
      end
    end

    context 'when a task has a sequenceable Quoting::Quote' do
      let(:sequenceable) { build :quoting_quote, consumer:client}
      let(:agent){ build :user_w_assoc }
      let(:client){ build :consumer_w_assoc, agent:agent }

      it 'fires when a task is updated, creates staff assignment, sets field on staff assignment' do
        task.save
        task.should_receive(:update_sequenceable_staff_assignment).and_call_original
        task.assigned_to_id = 5
        expect{ task.save }.to change(Usage::StaffAssignment, :count).by(1)
        task.sequenceable.staff_assignment.should_not be_nil
        task.sequenceable.staff_assignment.insurance_coordinator_id.should == 5
      end
    end
  end

  describe '#active?' do
    let(:task){build :crm_task, evergreen:false}
    #Statuses and subscriptions should only apply to recruit records, and not to paying user records.
    let(:recruit){build :user, is_recruit:true, active:false}
    let(:status){build :crm_status}
    let(:sequenceable){build :crm_case, consumer:client, active:true}
    let(:client){build :consumer, active:true}
    let(:subscription){build :marketing_subscription}

    before do
      sequenceable.consumer = client
    end

    context 'when origin is a Status' do
      before do
        task.origin = status
      end
      context 'when status is inactive' do
        before do
          status.active = false
        end
        it 'returns false' do
          task.active?.should == false
        end
      end
      context 'when status is active' do
        before do
          status.active = true
        end
        context 'when statusable is a Recruit' do
          before do
            status.statusable = recruit
          end
          it 'returns true' do
            status.statusable.should_receive(:active).at_least(1).times.and_call_original
            task.active?.should == true
          end
        end
      end
    end
    context 'when origin is a Subscription' do
      before do
        task.origin = subscription
      end
      context 'when subscription is inactive' do
        before do
          subscription.active = false
        end
        it 'returns false' do
          task.active?.should == false
        end
      end
      context 'when subscription is active' do
        before do
          subscription.active = true
        end
        context 'when person is a Connection' do
          before do
            subscription.person = client
          end
          context 'when subscriber is inactive' do
            before do
              client.active = false
            end
            it 'returns false' do
              client.should_receive(:active).and_call_original
              task.active?.should == false
            end
          end
          context 'when subscriber is active' do
            before do
              client.active = true
            end
            it 'returns true' do
              client.should_receive(:active).and_call_original
              task.active?.should == true
            end
          end
        end
        context 'when person is a Recruit' do
          before do
            subscription.person = recruit
          end
          it 'returns true' do
            subscription.should_receive(:active).at_least(1).times.and_call_original
            task.active?.should == true
          end
        end
      end
    end
  end

  describe 'template validation' do
    it 'allows save when task type is not email and template is nil' do
      task = Task.new task_type:Enum::TaskType['to-do'], template:nil
      task.save.should == true
    end
    it 'allows save when task type is email and template is present' do
      template = Marketing::Email::Template.create
      task = Task.new task_type:Enum::TaskType['email'], template:template
      task.save.should == true
    end
    it 'prevents save when task type is email and template is nil' do
      task = Task.new task_type:Enum::TaskType['email'], template:nil
      task.save.should == false
    end
    it 'prevents save when task type is email agent and template is nil' do
      task = Task.new task_type:Enum::TaskType['email agent'], template:nil
      task.save.should == false
    end
  end

  describe '#recipient' do
    let(:agent){ build :user_w_assoc }
    let(:client){ build :consumer_w_assoc, agent:agent }
    let(:sequenceable){ build :crm_case, consumer:client }
    let(:status){ build :crm_status, statusable:sequenceable }
    let(:task){ build :crm_task, task_type_id:Enum::TaskType.id(task_type), origin:status }
    before do
      MessageMedia.stub(:send_messages) # Don't actually want to send from this spec
      client.build_contact.owner = agent
    end
    context 'when type is "email"' do
      let(:task_type){'email'}
      it 'calls client.primary_email' do
        client.should_receive(:primary_email)
        task.send :recipient
      end
    end
    context 'when type is "agent email"' do
      let(:task_type){'email agent'}
      it 'calls client.primary_email' do
        agent.should_receive(:primary_email)
        task.send :recipient
      end
    end
    context 'when type is "sms"' do
      let(:task_type){'sms'}
      it 'calls client.primary_phone' do
        pending 'needs reimplementaion'
        #client.should_receive(:primary_phone)
        #task.send :recipient
      end
    end
    context 'when type is "agent sms"' do
      let(:task_type){'sms agent'}
      it 'calls client.primary_phone' do
        pending 'needs reimplementaion'
        #agent.should_receive(:primary_phone)
        #task.send :recipient
      end
    end
  end

  describe '#_send_sms' do
    let(:agent){ build :user_w_assoc }
    let(:client){ build :consumer_w_assoc, agent:agent, primary_phone:build(:phone) }
    let(:sequenceable){ build :crm_case, consumer:client }
    let(:status){ build :crm_status, statusable:sequenceable }
    let(:task){ build :crm_task, task_type_id:Enum::TaskType.id('sms'), origin:status, template:template }
    let(:message){ Marketing::MessageMedia::Message.new sender:agent, template:template, recipient:mobile_phone }
    let(:mobile_phone){ build :phone, phone_type_id:Enum::PhoneType.id('mobile')}
    before do
      MessageMedia.stub(:send_messages)
      agent.can!(:sms)
    end
    it 'builds a message' do
      pending 'needs reimplementaion'
      #task.should_receive(:_send_sms).and_call_original
      #Marketing::MessageMedia::Message.should_receive(:new).and_call_original
      #task.execute(true)
    end
    it 'renders message.body and sets message.format' do
      message # called here so that it doesn't trigger Message.new
      pending 'needs reimplementaion'
      #Marketing::MessageMedia::Message.should_receive(:new).and_return(message)
      #task.execute(true)
      #message.send(:body).should_not be_blank
      #message.send(:format).should == 'SMS'
      #message.send(:body).length.should <= 400
    end
    it 'calls MessageMedia::send_message' do
      pending 'needs reimplementaion'
      #MessageMedia.should_receive(:send_message)
      #task.execute
    end
  end

  describe '#schedule' do
    subject{ build_stubbed :crm_task, task_type_id:Enum::TaskType.id('email consumer'), suspended:suspended }
    before do
      allow(subject).to receive(:active?).and_return(true)
    end
    context 'when suspended is true' do
      let(:suspended){true}
      it 'does not trigger a TaskWorker' do
        expect(TaskWorker).to_not receive(:perform_at)
        expect(TaskWorker).to_not receive(:perform_async)
        subject.send :schedule
      end
    end
    context 'when suspended is false' do
      let(:suspended){false}
      it 'triggers a TaskWorker' do
        expect(TaskWorker).to receive(:perform_at)
        subject.send :schedule
      end
    end
    context 'when suspended is nil' do
      let(:suspended){nil}
      it 'triggers a TaskWorker' do
        expect(TaskWorker).to receive(:perform_at)
        subject.send :schedule
      end
    end
  end

  describe '#execute' do
    subject{ build_stubbed :crm_task, task_type_id:Enum::TaskType.id('email consumer'), suspended:suspended }
    context 'when suspended is true' do
      let(:suspended){true}
      it 'returns without taking an action' do
        expect(subject).to_not receive(:_send_sms)
        expect(subject).to_not receive(:_send_email)
        expect(subject).to_not receive(:_dial_phone)
        expect(subject).to_not receive(:save)
        expect(subject.execute).to be false
      end
    end
    context 'when suspended is false' do
      let(:suspended){false}
      it 'takes an action and saves' do
        expect(subject).to receive(:_send_email).and_return(true)
        expect(subject).to receive(:save).and_return(true)
        expect(subject.execute).to be true
      end
    end
  end

end
