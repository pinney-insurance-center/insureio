require 'spec_helper'

describe Enum do

  describe 'habtm in Usage::Contract' do
    let(:association_foreign_key){ 'state_id' }
    let(:foreign_key){ 'contract_id' }
    let(:join_table){ 'usage_contracts_states' }
    let(:user){ build_stubbed :user }
    let(:carrier) { Carrier.first }
    let(:contract){ create :usage_contract, user: user, states: nil, carrier: carrier }
 
    context 'when Contract#states starts off empty,' do
      it 'setter updates db join table' do
        contract.states.should be_a Array
        contract.states.should be_empty

        expect{contract.states = Enum::State.find(1,3,6)}.to change{count_joins 'usage_contracts_states'}.by(3)
        contract.states.length.should == 3
        contract.states.map(&:id).sort.should == [1,3,6]
        expect{contract.states = Enum::State.find(8)}.to change{count_joins('usage_contracts_states')}.by(-2)
        contract.states.should be_a Array
        contract.states.length.should == 1
      end
    end
    context 'when instance variable Contract@states is nil,' do
      it 'getter queries db join table and loads the state records' do
        contract.states = Enum::State.find(2,3,9)
        contract.instance_variable_get('@states').should_not be_nil
        contract.send :instance_variable_set, '@states', nil

        contract.instance_variable_get('@states').should be_nil
        contract.states.map(&:id).should == [2,3,9]
        contract.instance_variable_get('@states').length.should == 3
      end
    end
  end

  describe '::ewhere' do
    it 'returns an array of state records that match the constraint clause' do
      states = Enum::State.ewhere(id:[3,6,33,41])
      states.length.should == 4
      states.map(&:id).sort.should == [3,6,33,41]
    end
  end

  describe '::efind' do
    it 'returns one record, given a block' do
      found = Enum::TliHealthClassOption.efind { |o| o.name == 'Standard' }
      expect(found).to be_a Enum::TliHealthClassOption
      expect(found.name).to eq 'Standard'
    end
    it 'returns one record, given a hash' do
      found = Enum::TliHealthClassOption.efind name: 'Standard'
      expect(found).to be_a Enum::TliHealthClassOption
      expect(found.name).to eq 'Standard'
    end
  end
end

def count_joins(table_name)
  ActiveRecord::Base.connection.execute('select count(*) from ' + table_name).to_a.flatten.first
end
