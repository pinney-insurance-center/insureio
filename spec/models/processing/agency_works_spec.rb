require 'spec_helper'
require "processing"
require "processing/agency_works"
require "savon"

RSpec.configure do |c|
  c.include Processing::AgencyWorks
end

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::AgencyWorks do

  describe "#send_soap_request", :vcr do

    context "when sending a case," do
      let(:request_body){ File::read('spec/support/agency_works/submit_request.xml') }


      it "receives a response with the expected structure." do
        response_hash = send_soap_request request_body
        typical_deeply_nested_node = response_hash.dig(:tx_life, :user_auth_response, :trans_result, :result_code)
        expect(typical_deeply_nested_node).to be_a(String)
      end
    end

    context "when requesting updates," do
      let(:request_body){ File::read('spec/support/agency_works/updates_f3_default_request.xml') }

      it "receives a response with the expected structure." do
        response_hash = send_soap_request request_body
        response_hash[:tx_life][:tx_life_response].should include(:o_lif_e)
      end
    end

  end

end
