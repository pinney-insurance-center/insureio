require "spec_helper"
require 'net/ftp'
require 'savon/operation'

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::ExamOne do
  let!(:exam_one) { Processing::ExamOne.new }

  let (:control_code) { Processing::ExamOne::ControlCode.new paramed:"dummy beta", subsidiary:"dummy alpha" }
  let (:case_datum) { Processing::ExamOne::CaseData.new }
  let (:client) { build_stubbed :consumer, name:'Claire Redfield', gender:MALE }
  let (:kase) { build_stubbed :crm_case_w_assoc, id:42, created_at: 6.minutes.ago, consumer:client, product_type: Enum::ProductType::sample }

  before do
    #kase.id = 42
    #kase.created_at = 6.minutes.ago
    kase.stub(:exam_one_case_datum).and_return case_datum
    case_datum.control_code = control_code
  end

  describe '#build_and_send_or01' do
    it 'runs successfully', :vcr do
      kase.current_details.send(:update_case)
      Processing::ExamOne::ControlCode.stub(:like).and_return([1])
      kase.eligible_for_processing_with_exam_one.should eql(true), kase.errors.full_messages.join(" -- ")
      Processing::ExamOne::build_and_send_or01(kase).should == true
    end
  end

  describe "#get_statuses" do
    let(:ftp) { double Object }
    let(:filebasename) { 'testfile' }
    let(:filepath) { "#{Rails.root}/tmp/testfile.out" }

    before do
      File::write(filepath, "OR01V01ST18014550                      CASEONE                      0360000005                  171196                        N     \nAN01Qwe                           Priy                                          19690317491802999M                                  \nAN07890 Forty St                            8165210999                             United States       US                           \nAN03                                                                                Peloij                     MO64012              \nAN04MOK125238011                                              roydshur@yahoo.com                                                    \nCO01    OOOQ      ING Reliaster Life Ins Co/Pinney EZ/NP                171196                                                      \nPI011 0000020000017704376                                                                            NYN        NNMO                \nSR01PIN1PBU       Paramedical Examination, Blood and Urine collectio            OPEN                N                               \nSE0120130816070000                                                                                                                  \nST010000000001EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000002EXAM1     7         Other : desc2                                                                201308221504         \nST010000000003EXAM1     7         Other : third thing                                                          201308221505         \nSR02PIN1PPWK      Paperwork                                                     OPEN                N                               \nSR03PIN1LABUB     Urine and Blood testing                                       OPEN                N                               \nSR04PIN1HCONSNB   HIV Consent Form                                              OPEN                N                               \nSE0420130816070000                                                                                                                  \n")
      Net::FTP.stub(:new).with("xfer.labone.com").and_return(ftp)
      ftp.stub(:login)
      ftp.stub(:passive=)
      ftp.stub(:chdir)
      ftp.stub(:getbinaryfile)
      Crm::Case.stub(:exists?).and_return(true)
    end

    it 'executes without error' do
      ftp.should_receive(:nlst).with("*.PGP").and_return([filebasename])
      File.should_receive(:read).with(filepath).and_call_original
      Processing::ExamOne::Status.should_receive(:new).at_least(1).times.and_call_original
      Processing::ExamOne.get_statuses
    end

    after do
      File.delete("#{Rails.root}/tmp/testfile.out")
    end
  end

  describe "::schedule_url" do

    it 'builds a string with the expected fields' do
      control_code.stub(:paramed).and_return(411)
      ci = kase.consumer
      ci.primary_email.value='claire@stars.gov'
      ci.primary_address.street='223 fake st'
      ci.primary_address.state = Enum::State.find_by_abbrev("IL")
      ci.primary_address.city = 'Raccoon City'
      ci.primary_address.zip = 99911
      url = Processing::ExamOne.schedule_url(kase)
      url.should =~(/^http/)
      url.should =~ /(\?|&)control_code=411/
      url.should =~ /(\?|&)control_num=42/
      url.should =~ /(\?|&)fname=Claire/
      url.should =~ /(\?|&)lname=Redfield/
      url.should =~ /(\?|&)addr=223\+fake\+st/
      url.should =~ /(\?|&)state=IL/
      url.should =~ /(\?|&)city=Raccoon\+City/
      url.should =~ /(\?|&)zip=99911/
      url.should =~ /(\?|&)gender=0/
      url.should =~ /(\?|&)email=claire%40stars\.gov/
    end

    context "when the consumer's gender is male" do
      it "sets gender=0" do
        kase.consumer.gender = MALE
        url = Processing::ExamOne.schedule_url(kase)
        url.should =~ /(\?|&)gender=0/
      end
    end

    context "when the consumer's gender is male" do
      it "sets gender=1" do
        kase.consumer.gender = 'not male... female'
        url = Processing::ExamOne.schedule_url(kase)
        url.should =~ /(\?|&)gender=1/
      end
    end

    context "when the consumer's state is longer than 2 characters (NY-B or NY-NB)" do
      it "truncates the state" do
        kase.consumer.primary_address.state = Enum::State.find_by_abbrev('NY')
        url = Processing::ExamOne.schedule_url(kase)
        url.should =~ /(\?|&)state=NY/
      end
    end

    context "when the alpha control code is blank" do
      it "adds an error to the case" do
        case_datum.control_code.paramed = nil
        Processing::ExamOne.schedule_url(kase)
        kase.errors[:exam_one][0].should include('Control code is blank')
      end
    end

    context "when the control code record is nil" do
      it "adds an error to the case" do
        case_datum.control_code = nil
        Processing::ExamOne.schedule_url(kase)
        kase.errors[:exam_one][0].should include('Control code is blank')
      end
    end
  end
end
