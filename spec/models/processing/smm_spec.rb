require "spec_helper"

module Processing
  describe Smm do
    describe "#schedule_url" do
      let(:kase) { create :crm_case, id: 111, a_team_details: a_team_details, consumer: consumer }
      let(:a_team_details) { build :quoting_quote, face_amount: 2_345_678 }
      let(:smm_carrier) { double("Processing::Smm::Carrier", smm_code:43)}
      let(:consumer) { build :consumer, full_name: "test man", ssn: ssn, ezl_join: nil, gender: MALE, addresses: [address], phones: phones, emails: [email], birth: Date.new(1971, 3, 14) }
      let(:ssn) { "123-45-6789" }
      let(:phones) { ["1231231231", "3213213213", "4564564564"].map.with_index { |v,i| Phone.new value: v, type_id: 1+i } }
      let(:email) { EmailAddress.new value: 'mid@night.rambler' }
      let(:address) { Address.new street: "123 here street", zip: "23456", city: "hereville", state: Enum::State.efind(abbrev: "CA") }

      it 'returns the web_url with expected path, id, name, birth, phones, address, email, faceamt' do
        url = Smm.new.schedule_url(kase)
        parsed = URI.parse(url)
        expect(parsed.path).to eq '/NewOrder'
        expect(parsed.query).to eq "addr=123+here+street&appaltphone=W%3A+321-321-3213&appphone=H%3A+123-123-1231&cid=&city=hereville&crm_casenumber=111&dob=03%2F14%2F1971&email=mid%40night.rambler&entityid=13663&entitytype=2&faceamt=2345678&first_name=test&gender=male&last_name=man&noaddtolist=true&ref=111&ssn=123456789&state=CA&test=true&zip=23456"
      end
    end
  end
end
