require 'spec_helper'

describe Processing::Docusign::Api::Envelope, :vcr do
	require 'docusign_spec_helper'

	describe Builder do
		before(:each) do
			@template = get_a_template
			@builder = Processing::Docusign::Api::Envelope::Builder.new(
				email_subject: "A subject!",
				recievers: [{
					name: "Dylan Knutson",
					email: "dknutson@pinneyinsurance.com",
					role_name: "Signer 1"
				}],
				email_blurb: "Foobar!",
				template: @template
			)
		end

		it "is valid" do
			pending 'needs reimplementaion'
			@builder.should be_valid
		end

		it "is invalid without an email subject" do
			pending 'needs reimplementaion'
			@builder.email_subject = ""
			@builder.should_not be_valid

			@builder.email_subject = nil
			@builder.should_not be_valid
		end

		it "is invalid without a template" do
			pending 'needs reimplementaion'
			@builder.template = nil
			@builder.should_not be_valid
		end

		it "is invalid without at least one reciever" do
			pending 'needs reimplementaion'
			@builder.recievers = []
			@builder.should_not be_valid

			@builder.recievers = nil
			@builder.should_not be_valid
		end

		describe "is invalid when recievers is modified" do
			it "without an email" do
				pending 'needs reimplementaion'
				@builder.recievers.first[:email] = ""
				@builder.should_not be_valid
			end
			it "without a name" do
				pending 'needs reimplementaion'
				@builder.recievers.first[:name] = ""
				@builder.should_not be_valid
			end
			it "without a role_name" do
				pending 'needs reimplementaion'
				@builder.recievers.first[:role_name] = ""
				@builder.should_not be_valid
			end
		end

		it "is invalid without a status" do
			pending 'needs reimplementaion'
			@builder.status = ""
			@builder.should_not be_valid
		end
	end
end

def get_a_template
	@case = FactoryBot.create(:crm_case_w_assoc,
		consumer: FactoryBot.create(:consumer,
			state: Enum::State.find_by_abbrev("CA")
		),
		agent: FactoryBot.create(:user,
			docusign_cred: FactoryBot.create(:usage_docusign_cred)
		)
	)
	Processing::Docusign::Api::Client.new(@case).accounts.first.templates.first
end
