require 'spec_helper'

describe Processing::Docusign, :vcr do
	require 'docusign_spec_helper'

	before(:each) do
		# Set up a case that is located in California under Banner Insurance
		@case = FactoryBot.create(:crm_case_w_assoc,
			consumer: FactoryBot.create(:consumer,
				state: Enum::State.find_by_abbrev("CA")
			),
			agent: FactoryBot.create(:user,
				docusign_cred: FactoryBot.create(:usage_docusign_cred)
			)
		)
		@case.consumer.stub(:email).and_return("test_email@email.com")

		# Banner insurance, which is defined in docusign_spec_helper
		@case.current_details.stub(:carrier).and_return(Carrier.find(2))
	end

	describe ".eligible_to_send?" do

		it "returns true for a case with a state and carrier" do
			pending 'needs reimplementaion'
			Processing::Docusign.eligible_to_send?(@case).should be true, @case.errors[:docusign].join("\n")
		end

		it "returns false for a case without a corrosponding template" do
			pending 'needs reimplementaion'
			# Genworth, for which there is no template
			@case.current_details.stub(:carrier).and_return(Carrier.find(3))
			Processing::Docusign.eligible_to_send?(@case).should be false
		end

		it "returns false for a consumer without an email" do
			@case.consumer.stub(:email).and_return('')
			Processing::Docusign.eligible_to_send?(@case).should be false
		end
	end
	describe ".send_to_docusign" do
		describe "with a sendable case" do
			it "sucesfully sends the case" do
				pending 'needs reimplementaion'
				Processing::Docusign.send_to_docusign(@case).should be_a(Processing::Docusign::Api::Envelope)
			end
		end
	end
end
