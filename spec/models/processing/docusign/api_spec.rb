require 'spec_helper'

describe Processing::Docusign::Api::Client, :vcr do
  require 'docusign_spec_helper'

  before(:each) do
    @case = FactoryBot.create(:crm_case_w_assoc,
      consumer: FactoryBot.create(:consumer,
        state: Enum::State.find_by_abbrev("CA")
      ),
      agent: FactoryBot.create(:user,
        docusign_cred: FactoryBot.create(:usage_docusign_cred)
      )
    )
    @case_no_creds = FactoryBot.create(:crm_case_w_assoc,
      consumer: FactoryBot.create(:consumer,
        state: Enum::State.find_by_abbrev("CA")
      ),
      agent: FactoryBot.create(:user)
    )
    @api = PDA::Client.new(@case)
  end

  describe "#login_information" do
    it "returns a hash of login information" do
      li = @api.login_information
      li.should be_a(Hash)
      li["loginAccounts"].should be_a(Array)
    end
	end

	describe "#accounts" do
		it "returns an array of PDA::Account objects" do
			accounts = @api.accounts
			accounts.first.should be_a(PDA::Account)
		end

		describe "#accounts" do
			before(:each) do
				@account = @api.accounts.first
			end

			it "has a #base_url" do
				@account.base_url.should =~ /docusign.net\/restapi\/v2\/accounts\//
			end
			it "has a name" do
				@account.name.should == "Pinney Insurance"
			end
			it "has an ID" do
				@account.accountId.should == "38177"
				@account.id.should == "38177"
			end

      describe "#template_for" do
        before(:each) do
          @state = Enum::State.ewhere(abbrev: "CA").first
          @carrier = Carrier.where(['name like ?', 'Metlife%']).first
        end
        it "return the correct template" do
          template = @account.template_for(@carrier, @state)
          template.should be_a(PDA::Template)
          template.id.should == "6247309b-8e68-42df-af42-23a7a677c091"
        end
        it "return nil for a not found template" do
          # Carrier is Genworth
          template = @account.template_for(Carrier.find(3), @state)
          template.should be_nil
        end
      end

			describe " #templates" do
				before(:each) do
					@ftemplate = @account.templates(true).first
					@ltemplate = @account.templates(true).last
				end

				it "is an array" do
					@account.templates.should be_a(Array)
				end

        it "has a reference to the right account" do
          @ftemplate.account.should == @account
        end

				it "has an #id method" do
					@ftemplate.id.should == "3a6112f8-bff3-469f-8474-deaae5f77006"
				end

				it "has a state" do
					@ftemplate.name = "CA Metlife"
					@ftemplate.state.should == Enum::State.find_by_name("California")
				end

				it "has a carrier" do
					@ftemplate.name = "CA Metlife Investors USA"
					@ftemplate.carrier.should == Carrier.find(16)
				end

				it "has a carrier given an ambigious carrier name (uses the first found)" do
					@ftemplate.name = "CA Metlife"
					@ftemplate.carrier.should == Carrier.find(16)
				end
			end # describe #templates
		end # describe #accounts
	end
end
