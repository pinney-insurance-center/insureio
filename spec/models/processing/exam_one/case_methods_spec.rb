require 'spec_helper'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end

describe Crm::Case do
  let(:control_code) {Processing::ExamOne::ControlCode.new(paramed:'DUMMY VALUE')}
  let(:case_datum) {Processing::ExamOne::CaseData.new}
  let(:kase) { build :crm_case_w_assoc }
  let(:consumer) { build :consumer_w_assoc }

  before do
    kase.stub(:consumer).and_return(consumer)
    kase.stub(:current_details).and_return(kase.approved_details)
  end

  describe '#eligible_for_processing_with_exam_one' do
    it 'should fail if no control code is available' do
      kase.eligible_for_processing_with_exam_one.should == false
      kase.errors[:exam_one].flatten.should include "There are no control codes available for this carrier (#{kase.current_details.carrier_name})."
    end

    it 'should fail if no contact info is supplied' do
      consumer.stub(:primary_address).and_return(nil)
      consumer.stub(:primary_email).and_return(nil)
      kase.eligible_for_processing_with_exam_one.should == false
      kase.errors[:exam_one].flatten.should include "Email is required."
    end

    it 'should pass when required items are set' do
      kase.stub(:exam_one_case_datum).and_return(case_datum)
      kase.stub(:consumer).and_return(consumer)
      kase.stub(:product_type).and_return(Enum::ProductType.new)
      Processing::ExamOne::ControlCode.stub(:like).and_return([1])
      #case_datum.stub(:control_code).and_return(control_code)
      kase.eligible_for_processing_with_exam_one.should eql(true), kase.errors.full_messages.join("\n")
    end
  end

end
