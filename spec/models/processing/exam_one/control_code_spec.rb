require 'spec_helper'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end

ControlCode = Processing::ExamOne::ControlCode

describe Processing::ExamOne::ControlCode do
  describe 'scope "like"' do
    let(:kase) { build :crm_case_w_assoc }

    context 'when kase.current_details.nil?' do
      it 'runs a valid sql query' do
        kase.stub(:current_details).and_return nil
        expect{ ControlCode.like(kase).to_a }.to_not raise_error
      end
    end
    context 'when kase.current_details.carrier.nil?' do
      it 'runs a valid sql query' do
        kase.current_details.stub(:carrier).and_return nil
        expect{ ControlCode.like(kase).to_a }.to_not raise_error
      end
    end
    it 'should make well-formed where clause' do
      kase.stub(:current_details).and_return(kase.approved_details)
      kase.current_details.carrier.name = 'Dummy carrier name'
      ControlCode.like(kase).to_sql.should =~ /WHERE \(name LIKE ('|")Dummy%('|")\)/
    end
    it 'should execute without error' do
      expect{ ControlCode.like(kase).to_a }.to_not raise_error
    end
  end
end
