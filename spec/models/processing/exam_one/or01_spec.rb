require 'spec_helper'

describe Processing::ExamOne::Or01 do

  let (:client) { build :consumer }
  let (:kase) { build :crm_case_w_assoc, consumer:client }
  let (:control_code) { Processing::ExamOne::ControlCode.new paramed:"dummy beta", subsidiary:"dummy alpha" }
  let (:case_datum) { Processing::ExamOne::CaseData.new }

  before do
    client.stub(:priority_note){"Foo"}
    case_datum.stub(:control_code).and_return(control_code)
    kase.stub(:exam_one_case_datum).and_return(case_datum)
    kase.stub(:created_at).and_return 6.minutes.ago
  end

  it 'should build proper text' do
    or01 = Processing::ExamOne::Or01.new kase
    output = or01.to_s
    lines = output.split("\n")
    lines.length.should == 10
    lines[0].should match /^OR01/
    lines[1].should match /^ON01/
    lines[2].should match /^AN01/
    lines[3].should match /^AN02/
    lines[4].should match /^AN03/
    lines[5].should match /^AN04/
    lines[6].should match /^CO01/
    lines[7].should match /^PI01/
    lines[8].should match /^SE01/
  end

end