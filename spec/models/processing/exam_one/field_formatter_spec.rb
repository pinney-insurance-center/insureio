require File.expand_path("app/models/processing/exam_one/field_formatter")

class TestClass
  include Processing::ExamOne::FieldFormatter
end

describe TestClass do
  let(:test) { TestClass.new }

  describe "stringf" do
    describe "with only 1 argument" do
      it "outputs spaces" do
        test.stringf(9).should == "         "
      end
    end

    describe "with a text argument" do
      it "left justifies the string" do
        test.stringf(9, "foo").should == "foo      "
      end

      it "eliminates newline characters" do
        test.stringf(9, "fo\no\n").should == "foo      "
      end

      it "truncates overlong strings" do
        test.stringf(9, "foobarbazqux").should == "foobarbaz"
      end
    end

    describe "when strict arg is true" do
      it "eliminates chars [ ()-]" do
        test.stringf(9, ")f-o(-)ob  ar b)az(", true).should == "foobarbaz"
      end
    end
  end

end
