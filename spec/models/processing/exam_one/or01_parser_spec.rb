require "spec_helper"
require 'net/ftp'

RSpec.configure do |c|
  c.include FactoryBot::Syntax::Methods
end

describe Processing::ExamOne::Or01Parser do
  let(:case_id) { 17704376 }
  let(:klass) { Processing::ExamOne::Or01Parser }
  let(:kase)  { build :crm_case, id:case_id }

  describe "with only single or01 to parse" do
    let(:parser) { klass.new(text) }
    let(:text) { "OR01V01ST18014550                      CASEONE                      0360000005                  171196                        N     \nAN01Qwe                           Priy                                          19690317491802999M                                  \nAN07890 Forty St                            8165210999                             United States       US                           \nAN03                                                                                Peloij                     MO64012              \nAN04MOK125238011                                              roydshur@yahoo.com                                                    \nCO01    OOOQ      ING Reliaster Life Ins Co/Pinney EZ/NP                171196                                                      \nPI011 0000020000017704376                                                                            NYN        NNMO                \nSR01PIN1PBU       Paramedical Examination, Blood and Urine collectio            OPEN                N                               \nSE0120130816070000                                                                                                                  \nST010000000001EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000002EXAM1     7         Other : desc2                                                                201308221504         \nST010000000003EXAM1     7         Other : third thing                                                          201308221505         \nSR02PIN1PPWK      Paperwork                                                     OPEN                N                               \nSR03PIN1LABUB     Urine and Blood testing                                       OPEN                N                               \nSR04PIN1HCONSNB   HIV Consent Form                                              OPEN                N                               \nSE0420130816070000                                                                                                                  \n" }

    it 'extracts policy number' do
      parser.policy_number.should == case_id
    end

    context 'from unique ST lines' do
      it 'builds statuses from unique ST lines' do
        parser.send(:statuses).length.should == 3
        parser.send(:statuses).all?{|s| s.case_id.should == case_id }
        parser.send(:statuses)[0].description.should == "Other : sent another email for both to sched"
        parser.send(:statuses)[0].completed_at.should == DateTime.parse("2013-08-22 15:03:00")
        parser.send(:statuses)[1].description.should == "Other : desc2"
        parser.send(:statuses)[1].completed_at.should == DateTime.parse("2013-08-22 15:04:00")
        parser.send(:statuses)[2].description.should == "Other : third thing"
        parser.send(:statuses)[2].completed_at.should == DateTime.parse("2013-08-22 15:05:00")
      end
    end

    context 'from redundant ST lines' do
      let(:text) { "OR01V01ST18014550                      CASEONE                      0360000005                  171196                        N     \nAN01Qwe                           Priy                                          19690317491802999M                                  \nAN07890 Forty St                            8165210999                             United States       US                           \nAN03                                                                                Peloij                     MO64012              \nAN04MOK125238011                                              roydshur@yahoo.com                                                    \nCO01    OOOQ      ING Reliaster Life Ins Co/Pinney EZ/NP                171196                                                      \nPI011 0000020000017704376                                                                            NYN        NNMO                \nSR01PIN1PBU       Paramedical Examination, Blood and Urine collectio            OPEN                N                               \nSE0120130816070000                                                                                                                  \nST010000000001EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000002EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000003EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nSR02PIN1PPWK      Paperwork                                                     OPEN                N                               \nSR03PIN1LABUB     Urine and Blood testing                                       OPEN                N                               \nSR04PIN1HCONSNB   HIV Consent Form                                              OPEN                N                               \nSE0420130816070000                                                                                                                  \n" }
      it 'does not build stauses' do
        parser.send(:statuses).length.should == 1
      end
    end
  end

  context "with multiple or01s to parse" do
    let(:text)  { "OR01V01ST18014550                      CASEONE                      0360000005                  171196                        N     \nAN01Qwe                           Priy                                          19690317491802999M                                  \nAN07890 Forty St                            8165210999                             United States       US                           \nAN03                                                                                Peloij                     MO64012              \nAN04MOK125238011                                              roydshur@yahoo.com                                                    \nCO01    OOOQ      ING Reliaster Life Ins Co/Pinney EZ/NP                171196                                                      \nPI011 0000020000017704376                                                                            NYN        NNMO                \nSR01PIN1PBU       Paramedical Examination, Blood and Urine collectio            OPEN                N                               \nSE0120130816070000                                                                                                                  \nST010000000001EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000002EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000003EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nSR02PIN1PPWK      Paperwork                                                     OPEN                N                               \nSR03PIN1LABUB     Urine and Blood testing                                       OPEN                N                               \nSR04PIN1HCONSNB   HIV Consent Form                                              OPEN                N                               \nSE0420130816070000                                                                                                                  \nOR01V01ST18014551                      CASEONE                      0360000005                  171195                        N     \nAN01Poi                           Lkjhg                                         19740728244716573F                                  \nAN07890 Fifty St                            8165210333                             United States       US                           \nAN03                                                                                Peloij                     MO64012              \nAN04MOW125201001                                              fordscae@yahoo.com                                                    \nCO01    OOOQ      ING Reliaster Life Ins Co/Pinney EZ/NP                171195                                                      \nPI011 0000020000017704356                                                                            NYN        NNMO                \nSR01PIN1PBU       Paramedical Examination, Blood and Urine collectio            OPEN                N                               \nSE0120130816070000                                                                                                                  \nST010000000001EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000002EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nST010000000003EXAM1     7         Other : sent another email for both to sched                                 201308221503         \nSR02PIN1PPWK      Paperwork                                                     OPEN                N                               \nSR03PIN1LABUB     Urine and Blood testing                                       OPEN                N                               \nSR04PIN1HCONSNB   HIV Consent Form                                              OPEN                N                               \nSE0420130816070000                                                                                                                  \nTOTL00000000020000000032                                                                                                            \n" }
  
    describe "::parse_and_save_statuses" do
      before do
        File.stub(:read).and_return(text)
      end

      context 'when Cases exist in db for OR01' do
        it 'creates Status records in db' do
          Crm::Case.stub(:exists?).and_return(true)
          expect{ klass.parse_and_save_statuses('pretend-filepath') }.to change(Processing::ExamOne::Status, :count).by(2)
        end
      end

      context 'when Cases do not exist in db for OR01' do
        it 'creates Status records in db' do
          Crm::Case.stub(:exists?).and_return(false)
          expect{ klass.parse_and_save_statuses('pretend-filepath') }.to change(Processing::ExamOne::Status, :count).by(0)
        end
      end
    end
  end

end
