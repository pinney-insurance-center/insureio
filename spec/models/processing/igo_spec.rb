require "spec_helper"

describe Processing::Igo, :vcr do
  let(:igo) { Processing::Igo.new }

  describe "#post_xml" do
    let(:crm_case) { build :crm_case_w_assoc, consumer:client, agent:agent, agent_of_record:agent_of_record }
    let(:client) { build :consumer_w_assoc, agent:agent }
    let(:agent) { build :agent}
    let(:agent_of_record) { build :agent }

    it "makes a request to the correct endpoint" do
      expect{ igo.post_xml(crm_case, client) }.to_not raise_exception
    end
  end
end
