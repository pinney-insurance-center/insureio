require 'spec_helper'
require 'support/apps/shared_context.rb'

VCR.configure do |c|
  c.hook_into :excon
end
require 'pry'
shared_examples_for "an APPS test case" do
  it 'uploads case' do
    Processing::Apps::XmlBuilder.any_instance.stub(:waiver_of_premium?).and_return(waiver_of_premium)
    Processing::Apps::XmlBuilder.any_instance.stub(:carrier_code){ |xml_builder, car, request_type| car.code }
    output=Processing::Apps::UploadService.new(kase).submit_new_business
    expect(output).to eq(true)
    expect(kase.sent_103_to_apps_at).to be_a(Date)
  end
end

describe Processing::Apps::UploadService do

  describe '#submit_new_business', :vcr do

    context 'for default APPS test case' do
      include_context 'shared APPS context'
      #let(:xml){ builder.submit_new_business_xml }

      it 'runs Savon operation and returns true', :vcr do
        Savon::Operation.any_instance.should_receive(:call).and_call_original
        #File.write('tmp/apps-upload-103.xml', xml)
        Processing::Apps::UploadService.new(kase).submit_new_business.should == true
      end
    end

    context 'for APPS test cases' do
      context '#1' do
        include_context 'Case 1'
        it_behaves_like "an APPS test case"
      end
      context '#2' do
        include_context 'Case 2'
        it_behaves_like "an APPS test case"
      end
      context '#3' do
        include_context 'Case 3'
        it_behaves_like "an APPS test case"
      end
      context '#5' do
        include_context 'Case 5'
        it_behaves_like "an APPS test case"
      end
    end
   
  end

end
