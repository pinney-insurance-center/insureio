require 'spec_helper'
require 'support/apps/shared_context.rb'

describe Processing::Apps::XmlBuilder do

  describe '#new_business_submission_xml' do
    include_context 'shared APPS context'
    #let(:generated_xml){ Timecop.freeze( start_time ){ builder.submit_new_business_xml(indent:2) } }

    context 'with a representative case' do
      include_context 'shared APPS context'
      it 'renders good xml' do
          generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml.should_not match 'PENDING'
      end
    end

    context 'when Jan is agent of record' do
      include_context 'shared APPS context'
      #Jan is the default set in 'shared APPS context'.
      it 'matches the expected document' do
        consumer.financial_info.stub(:net_worth).and_return(1_000_000)
        consumer.stub(:priority_note){"This man wrote Leaves of Grass"}
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_jan_103.xml', generated_xml)
        expected_xml=File::read("#{Rails.root}/spec/support/apps/case_jan_103.xml")

        expect(generated_xml).to eq(expected_xml)
        expect(generated_xml).to include('Additional Writing Agent')
      end
    end

    context 'when agent_of_record is blank' do
      include_context 'shared APPS context'
      before do
        agent.agent_of_record = nil
        kase.agent_of_record=agent
        agent.stub_chain(:contracts, :where, :first).and_return(build :usage_contract, carrier_id:1, carrier_contract_id:'789fds')
      end
      it 'has only one agent, with 100% interest' do
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml.should include %q(OriginatingObjectID="Holding_insured" RelatedObjectID="Party_primary_agent">
        <OriginatingObjectType tc="4">Holding</OriginatingObjectType>
        <RelatedObjectType tc="6">Party</RelatedObjectType>
        <RelationRoleCode tc="37">Primary Writing Agent</RelationRoleCode>
        <InterestPercent>100</InterestPercent>
      </Relation>)
        generated_xml.should_not include %q(Additional Writing Agent)
      end
    end

    context 'for Case 1' do
      include_context 'Case 1'
      it 'outputs case 1 XML' do
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_1_103.xml', generated_xml)
        expected_xml=File.read('spec/support/apps/case_1_103.xml')

        expect(generated_xml).to eq(expected_xml)
      end
    end

    context 'for Case 2' do
      include_context 'Case 2'
      it 'outputs case 2 XML' do
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_2_103.xml', generated_xml)
        expected_xml=File.read('spec/support/apps/case_2_103.xml')

        expect(generated_xml).to eq(expected_xml)
      end
    end

    context 'for Case 3' do
      include_context 'Case 3'
      it 'outputs case 3 XML' do
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_3_103.xml', generated_xml)
        expected_xml=File.read('spec/support/apps/case_3_103.xml')

        expect(generated_xml).to eq(expected_xml)
      end
    end

    context 'for Case 4' do
      include_context 'Case 4'
      it 'outputs case 4 XML' do
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_4_103.xml', generated_xml)
        expected_xml=File.read('spec/support/apps/case_4_103.xml')

        expect(generated_xml).to eq(expected_xml)
      end
    end

    context 'for Case 5' do
      include_context 'Case 5'
      it 'outputs case 5 XML' do
        consumer.stub(:priority_note){"This man is known to the State of California to be a total trash boat"}
        generated_xml=builder.submit_new_business_xml(indent:2)
        generated_xml_file=File.write('tmp/case_5_103.xml',generated_xml)
        expected_xml=File.read('spec/support/apps/case_5_103.xml')

        expect(generated_xml).to eq(expected_xml)
      end
    end

  end

  describe '#order_requirements_xml' do
    include_context 'shared APPS context'
    it 'generates xml with basic data present' do
      generated_xml=builder.order_requirements_xml(indent:2)

      expect(generated_xml).to include(consumer.first_name)
      expect(generated_xml).to include(consumer.last_name)
      expect(generated_xml).to include(quote.plan_name)
      expect(generated_xml).to include(quote.carrier.name)

    end
  end

end