require "spec_helper"
require File.join Rails.root, 'app/models/processing.rb'

describe Processing::Igo::ClientDataXmlBuilder do
  let(:client_data_xml_builder) { Processing::Igo::ClientDataXmlBuilder.new }

  describe "#build_xml" do
    let(:crm_case) { double("Crm::Case") }
    let(:consumer) { double("Consumer") }
    let(:beneficiary_owner_xml_builder) { double("Processing::Igo::BeneficiaryOwnerXmlBuilder") }
    let(:carrier_xml_builder) { double("Processing::Igo::CarrierXmlBuilder") }
    let(:other_insurance_xml_builder) { double("Processing::Igo::OtherInsuranceXmlBuilder") }
    let(:primary_insured_xml_builder) { double("Processing::Igo::PrimaryInsuredXmlBuilder") }
    let(:xml) { <<-XML
<ClientData>
  <primaryinsuredxml/>
  <otherinsxml/>
  <beneficiaryownerxml/>
</ClientData>
                XML
    }

    before do
      Processing::Igo::BeneficiaryOwnerXmlBuilder.stub(:new).and_return(beneficiary_owner_xml_builder)
      Processing::Igo::CarrierXmlBuilder.stub(:new).and_return(carrier_xml_builder)
      Processing::Igo::PrimaryInsuredXmlBuilder.stub(:new).and_return(primary_insured_xml_builder)
      Processing::Igo::OtherInsuranceXmlBuilder.stub(:new).and_return(other_insurance_xml_builder)
      beneficiary_owner_xml_builder.stub(:add_beneficiary_owner_xml).with(crm_case).and_return("  <beneficiaryownerxml/>\n")
      carrier_xml_builder.stub(:add_carrier_xml).with(crm_case).and_return("  <carrierxml/>\n")
      other_insurance_xml_builder.stub(:add_other_insurance).with(crm_case, consumer).and_return("  <otherinsxml/>\n")
      primary_insured_xml_builder.stub(:add_primary_insured_xml).with(consumer).and_return("  <primaryinsuredxml/>\n")
    end

    it "builds the xml" do
      client_data_xml_builder.build_xml(crm_case, consumer).should == xml
    end
  end
end
