require "spec_helper"
require File.join Rails.root, 'app/models/processing.rb'

describe Processing::Igo::CarrierXmlBuilder do
  let(:carrier_xml_builder) { Processing::Igo::CarrierXmlBuilder.new }

  describe "#add_carrier_xml" do
    let(:carrier_name) { "Carrier" }
    let(:product_name) { "Product" }
    let(:product_type) { "product type" }
    let(:carrier_product) { double("Processing::Igo::CarrierProduct", product_id: 43, product_type_id: 44) }
    let(:crm_case) { double("Crm::Case", quoted_details: quoted_details) }
    let(:premium_mode) { double("Enum::PremiumModeOption", name: "Annual") }
    let(:quoted_details) { double("Quoting::Quote", face_amount: 1_000_000, premium_mode: premium_mode, modal_premium: 300, carrier_name: carrier_name, plan_name: product_name, product_type: product_type, carrier_id: carrier_id) }
    let(:carrier_id) { 42 }

    let(:built_xml) { <<-XML
<Data Name="ProductID">43</Data>
<Data Name="ProductTypeID">44</Data>
<Data Name="FaceAmount">1000000</Data>
<Data Name="POL_PaymentMethod">Annual</Data>
<Data Name="POL_PremiumAmount">300</Data>
                      XML
    }

    before do
      Processing::Igo::CarrierProduct.stub(:where).with(dr_carrier_id: carrier_id).and_return(double('Object', first:carrier_product))
      quoted_details.stub(:premium_mode_name) { premium_mode.name }
    end

    it "builds the xml" do
      #carrier_xml_builder.add_carrier_xml(crm_case).should == built_xml
      xml = carrier_xml_builder.add_carrier_xml(crm_case)
      proc = lambda{|xml|
        doc = Nokogiri::XML.fragment(xml.strip)
        doc.children.select{|x| x.name == 'Data'}.map(&:to_s).sort.join("\n")
      }
      proc.call(xml).should == proc.call(built_xml)
    end
  end
end
