require "spec_helper"

describe Processing::Igo::UserDataXmlBuilder do
  let(:user_data_xml_builder) { Processing::Igo::UserDataXmlBuilder.new }

  describe "#build_xml" do
    let(:agent) { double("User", first_name: "Dylan", last_name: "Knutson", phone_value: "18002235174") }

    let(:built_xml) { <<-XML
<UserData>
  <Data Name="FirstName">Dylan</Data>
  <Data Name="LastName">Knutson</Data>
  <Data Name="Phone">18002235174</Data>
</UserData>
                      XML
    }

    it "returns the built xml" do
      user_data_xml_builder.build_xml(agent).should == built_xml
    end
  end
end
