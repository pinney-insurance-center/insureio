require "spec_helper"
require File.join Rails.root, 'app/models/processing.rb'

describe Processing::Igo::BeneficiaryOwnerXmlBuilder do
  let (:beneficiary_owner_xml_builder) { Processing::Igo::BeneficiaryOwnerXmlBuilder.new }

  describe "#add_primary_beneficiary_xml" do
    
      let (:crm_case) {FactoryBot.create(:crm_case_w_beneficiary_and_owner, agent:FactoryBot.build(:user))}
      let (:ben) {crm_case.beneficiaries.first}
      let (:owner) {crm_case.owner}

      let (:built_xml) {%Q(
        <Data Name="PB_FirstName__1">#{ben.first_name}</Data>
        <Data Name="PB_MiddleName__1">#{ben.middle_name}</Data>
        <Data Name="PB_LastName__1">#{ben.last_name}</Data>
        <Data Name="PB_EntityName__1">#{ben.trustee_name}</Data>
        <Data Name="PB_DOB__1">#{ben.birth_or_trust_date.strftime("%m/%d/%Y")}</Data>
        <Data Name="PB_RelationType__1">#{ben.relationship.name}</Data>
        <Data Name="PB_Share__1">#{ben.percentage}</Data>
        <Data Name="PB_SSN__1">#{ben.ssn}</Data>
        <Data Name="PIOwnerInd">#{owner.nil? ? "Yes" : "No"}</Data>
        <Data Name="OWN_Ind_FirstName">#{owner.first_name}</Data>
        <Data Name="OWN_Ind_MiddleName">#{owner.middle_name}</Data>
        <Data Name="OWN_Ind_LastName">#{owner.last_name}</Data>
        <Data Name="OWN_ Ind_PHONE">#{owner.primary_phone}</Data>
        <Data Name="OWN_Ind_SSN">#{owner.ssn}</Data>
        <Data Name="OWN_Ind_Relationship">#{owner.relationship.name}</Data>
      )}
    

    it "builds the xml" do
      xml = beneficiary_owner_xml_builder.add_beneficiary_owner_xml(crm_case)
      proc = lambda{|xml|
        doc = Nokogiri::XML.fragment(xml.strip)
        doc.children.select{|x| x.name == 'Data'}.map(&:to_s).sort.join("\n")
      }
      proc.call(xml).should == proc.call(built_xml)
    end
  end
end
