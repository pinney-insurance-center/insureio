require "spec_helper"

describe Processing::Igo::OtherInsuranceXmlBuilder do
  let(:other_insurance_xml_builder) { Processing::Igo::OtherInsuranceXmlBuilder.new }

  let(:approved_details) { double("Quoting::Quote", carrier_name: "Genworth", face_amount: 500000, product_type: product_type) }
  let(:crm_case        ) { double("Crm::Case", is_a_preexisting_policy: is_a_preexisting_policy) }
  let(:crm_case_2      ) { double("Crm::Case", approved_details: approved_details, policy_number: "123232", effective_date: Date.new(2000, 12, 27)) }
  let(:consumer        ) { double("Consumer", cases: [crm_case, crm_case_2]) }
  let(:product_type    ) { Enum::ProductType["Term"] }

  describe "#add_other_insurance" do
    context "when the crm case has existing insurance case" do
      let(:is_a_preexisting_policy) { true }
      let(:built_xml) { <<-XML
<Data Name="OTHINS_InforceInd">Yes</Data>
<Data Name="OTHINS_EI_CoverageAmt">500000</Data>
<Data Name="OTHINS_EI_Company__1">Genworth</Data>
<Data Name="OTHINS_EI_Policy">123232</Data>
<Data Name="OTHINS_EI_Replaced">Yes</Data>
<Data Name="OTHINS_EI_CoverageType">Term</Data>
<Data Name="OTHINS_EI_IssueDate">12/27/2000</Data>
                        XML
      }

      it "returns the built xml" do
        other_insurance_xml_builder.add_other_insurance(crm_case, consumer).should == built_xml
      end
    end

    context "when the crm case does not have existing insurance" do
      let(:is_a_preexisting_policy) { false }
      let(:built_xml) { <<-XML
<Data Name="OTHINS_InforceInd">No</Data>
                        XML
      }

      it "returns the built xml" do
        other_insurance_xml_builder.add_other_insurance(crm_case, consumer).should == built_xml
      end
    end
  end
end
