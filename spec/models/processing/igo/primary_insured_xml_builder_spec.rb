require "spec_helper"
require File.join Rails.root, 'app/models/processing.rb'
require 'pry'

describe Processing::Igo::PrimaryInsuredXmlBuilder do
  let(:primary_insured_xml_builder) { Processing::Igo::PrimaryInsuredXmlBuilder.new }

  describe "#add_primary_insured_xml" do
    consumer=FactoryBot.build_stubbed(:consumer_w_assoc_b)

    built_xml=%Q(
      <Data Name="PIFirstName">#{consumer.first_name}</Data>
      <Data Name="PILastName">#{consumer.last_name}</Data>
      <Data Name="PIMiddleName">#{consumer.middle_name}</Data>
      <Data Name="PIGender">#{consumer.gender ? 'Male' : 'Female'}</Data>
      <Data Name="PIDOB">#{consumer.birth.strftime("%m/%d/%Y")}</Data>
      <Data Name="PIDriversLicense_YesNo">#{consumer.dln ? 'Yes' : 'No'}</Data>
      <Data Name="PIBirthState">#{consumer.birth_state.try(:abbrev)}</Data>
      <Data Name="PIBirthCountry">#{consumer.birth_country.try(:name)||consumer.birth_country_custom_name}</Data>
      <Data Name="PIMStatus">#{consumer.marital_status.name}</Data>
      <Data Name="PIDLicense">#{consumer.dl_state.try(:abbrev)}</Data>
      <Data Name="PITobaccoInfo">#{consumer.health_info.tobacco? ? "Yes" : "No"}</Data>
      <Data Name="PIFNDCitizenCountry">#{consumer.citizenship.name}</Data>
      <Data Name="PICitizen">Yes</Data>
      <Data Name="PIEmployed">#{consumer.company.nil? ? "No" : "Yes"}</Data>
      <Data Name="PIEMP_Name">#{consumer.company}</Data>
      <Data Name="PIEmp_Occupation">#{consumer.occupation}</Data>
      <Data Name="PIAnnualEarnedIncome">#{consumer.financial_info.income}</Data>
      <Data Name="AgntType">Individual</Data>
      <Data Name="MED_Q1_HtIn">#{consumer.health_info.inches}</Data>
      <Data Name="MED_Q1_HtFt">#{consumer.health_info.feet}</Data>
      <Data Name="MED_Q1_Wt">#{consumer.health_info.weight}</Data>
      <Data Name="NonMed_PhysInd">Yes</Data>
      <Data Name="PIEmail">#{consumer.primary_email.try(:value)}</Data>
      <Data Name="PIADDR_Street">#{consumer.primary_address.street}</Data>
      <Data Name="PIADDR_City">#{consumer.primary_address.city}</Data>
      <Data Name="PIADDR_State">#{consumer.primary_address.state.abbrev}</Data>
      <Data Name="StateID">#{consumer.primary_address.state.abbrev}</Data>
      <Data Name="PIADDR_Zip">#{consumer.primary_address.zip}</Data>
      <Data Name="PIPhone_Home">#{consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home')}.first.try(:value)}</Data>
      <Data Name="PIPhone_Work">#{consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work')}.first.try(:value)}</Data>
      <Data Name="PIPhone_Work_EXT">#{consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work')}.first.try(:ext)}</Data>
      <Data Name="PIPhysicianType">Individual</Data>
    )

    it "returns the built xml" do

      xml = primary_insured_xml_builder.add_primary_insured_xml(consumer)
      proc = lambda{|xml|
        doc = Nokogiri::XML.fragment(xml.strip)
        doc.children.select{|x| x.name == 'Data'}.map(&:to_s).sort.join("\n")
      }
      proc.call(xml).should == proc.call(built_xml)
    end
  end
end