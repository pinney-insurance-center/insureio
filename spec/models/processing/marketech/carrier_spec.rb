require 'spec_helper'

shared_examples_for "::for_face_amount" do
  it 'returns only records where operator is and face amount agree' do
    records = Processing::Marketech::Carrier.for_face_amount(face_amount)
    records.select{|r| r.face_amount_operator == '<=' }.all?{|r| face_amount <= r.face_amount }.should eq true
    records.select{|r| r.face_amount_operator == '<'  }.all?{|r| face_amount <  r.face_amount }.should eq true
    records.select{|r| r.face_amount_operator == '>=' }.all?{|r| face_amount >= r.face_amount }.should eq true
    records.select{|r| r.face_amount_operator == '>'  }.all?{|r| face_amount >  r.face_amount }.should eq true
  end
end

describe Processing::Marketech::Carrier do

  it 'can be built from YAML seed file' do
    expect{
      yaml = YAML::load_file "#{Rails.root}/db/seeding/marketech_carriers.yml"
      yaml.each{|data|
        data.delete 'rules'
        data.delete 'states'
        Processing::Marketech::Carrier.new(data)
      }
    }.to_not raise_error
  end

  describe 'scope ::for_face_amount' do
    context 'with 60000' do
      let(:face_amount){ 60000 }
      it_behaves_like "::for_face_amount"
    end
    context 'with 50000' do
      let(:face_amount){ 50000 }
      it_behaves_like "::for_face_amount"
      it 'has at least one result whose face amount is 50000' do
        records = Processing::Marketech::Carrier.for_face_amount(face_amount)
        records.any?{|r| r.face_amount == 50000 }.should eq true
      end
    end
  end

  describe 'scope ::for_state_id' do
    context 'with NY id' do
      it 'returns only and all records where NY is enabled' do
        records = Processing::Marketech::Carrier.for_state_id(Enum::State.find_by_abbrev('NY-NB').id)
        records.map(&:code).sort.should == ["Empire", "LNLShort", "LTGUltraCNY", "NYTerm", "PENNSeries", "ProtectorUL", "SelectATermNY", "TransAce", "TransTermUL", "TrueTermNY"]
      end
    end
    context 'with MN id' do
      it 'returns only and all records where MN is enabled' do
        records = Processing::Marketech::Carrier.for_state_id(Enum::State.find_by_abbrev('MN').id)
        records.map(&:code).sort.should == ["Colony", "GTO02", "GULC", "ING", "LNLShort", "LTGUltraC", "LevelTERM", "OPTerm", "ProtectorUL", "Pru", "Pru", "SLD", "SecureTPLIC", "SelectATerm", "SureTerm", "TransAce", "TransTermUL", "TrendsetterSuper", "WholeLifeElite"]
      end
    end      
  end

  describe 'scope ::for_case' do
    let(:kase) { build :crm_case, a_team_details:build(:quoting_quote, face_amount:face_amount, carrier_name:carrier_name) }

    before do
      kase.stub_chain(:consumer, :state_id).and_return(1)
    end

    context 'with ING ReliaStar' do
      let(:carrier_name){ "Reliastar Life Insurance Company (VOYA)" }
      context 'with face amount of 50000' do
        let(:face_amount){ 50000 }
        it 'should return ING' do
          Processing::Marketech::Carrier.for_case(kase).first.code.should == "ING"
        end
      end
      context 'with face amount of 40000' do
        let(:face_amount){ 40000 }
        it 'should return empty' do
          Processing::Marketech::Carrier.for_case(kase).should be_empty
        end
      end
    end
    context 'with Sagicor with face amount of 50000' do
      let(:carrier_name){ "Sagicor Life Insurance Company" }
      let(:face_amount){ 50000 }
      it 'should return empty' do
        Processing::Marketech::Carrier.for_case(kase).should be_empty
      end
    end
  end

end