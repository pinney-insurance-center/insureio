require 'spec_helper'

describe Crm::Case do # viz. Marketech::CaseMethods

  let(:kase){ build :crm_case, consumer:client, a_team_details:quote}
  let(:client){ build :consumer_w_assoc_b }
  let(:quote){ build :quoting_quote, carrier:build(:carrier), carrier_id:carrier_id, face_amount:face_amount }
  let(:carrier_id){ "dummy-id" }
  let(:face_amount){ 50000 }

  it 'responds to methods from Processing::Marketech::CaseMethods' do
    Crm::Case.new.should respond_to :eligible_for_processing?
    Crm::Case.new.should respond_to :marketech_dataset
    Crm::Case.new.should respond_to :marketech_updates
  end

  describe '#eligible_for_processing?' do
    # let(:carrier_id){ @marketech_supported_carrier_id ||= Processing::Marketech::Carrier.first.carrier_id }

    context 'when marketech support for carrier is stubbed (ignored)' do
      before do
        %w[where for_case for_state_id for_face_amount].each do |the_method|
          Processing::Marketech::Carrier.stub(the_method).and_return(['dummy response'])
        end
      end
      it 'returns true if requisite data is set and carrier is supported by marketech' do
        kase.eligible_for_processing?(:marketech).should eq(true), kase.errors.full_messages.join("\n")
      end
      it 'returns false unless state is set' do
        kase.consumer.addresses=[]
        kase.consumer.state_id = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
      it 'returns false unless driver\'s license is set' do
        kase.consumer.dln = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
      it 'returns false unless driver\'s license state is set' do
        kase.consumer.addresses=[]
        kase.consumer.dl_state_id = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
      it 'returns false unless birth is set' do
        kase.consumer.birth = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
      it 'returns false unless ssn is set' do
        kase.consumer.ssn = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
      it 'returns false unless zip is set' do
        kase.consumer.addresses=[]
        kase.consumer.zip = nil
        kase.eligible_for_processing?(:marketech).should eq(false)
      end
    end

    context 'when required quote data and client data are set' do
      context 'and marketech does not support carrier' do
        let(:carrier_id){ 19 } # Assuming AAA Life Insurance Company is not supported by Marketech
        let(:face_amount){ 9999999999999999999 }
        it 'cannot find a Marketech::Carrier' do
          Processing::Marketech::Carrier.for_case(kase).first.should be_nil
        end
        it 'returns false' do
          kase.eligible_for_processing?(:marketech).should eq(false)
          kase.errors[:marketech].flatten.should include('Carrier is not supported at all by Marketech')
        end
      end

      context 'and marketech supports the carrier' do
        let(:carrier_id){ 2 } # Assuming Banner Life Insurance Company is supported by Marketech
        let(:state_id){ 40 } # Assuming Marketech supports Banner Life in PA (Pennsylvania)
        before do
          kase.consumer.stub(:state_id).and_return(state_id)
        end
        it 'can find a Marketech::Carrier' do
          Processing::Marketech::Carrier.for_case(kase).first.should_not be_nil
        end
        it 'returns true' do
          kase.eligible_for_processing?(:marketech).should eq true
        end
      end
    end
  end

end