require 'spec_helper'

describe Processing::Marketech::Update do
  let(:caseID){ Processing::Marketech::CASE_PREFIX+kase.id.to_s }
  let(:params){ {caseID:caseID, event:6, party:'agent', code:nil} }
  let(:dataset){ Processing::Marketech::Dataset.new kase:kase }
  let(:kase){ build_stubbed :crm_case_w_assoc, id:998, status:status, consumer: build(:consumer) }
  let(:status){ create :crm_status } # create is used here b/c the association to `origin`, used in Task#infer_status_type cannot do an IdentityMap lookup on a polymorphic association (`origin`)
  subject{ Processing::Marketech::Update.build params }

  before do
    allow(subject).to receive(:dataset).and_return(dataset)
  end

  it 'creates without error' do
    subject.save
  end

end
