require 'spec_helper'
require 'nokogiri'

describe Processing::Marketech::UploadService do
  let(:agent_id){ 64 }
  let(:kase_id){ 65 }
  let(:kase){ build :crm_case_w_assoc, id:kase_id, consumer:consumer, a_team_details:a_team_details, agent:agent, stakeholder_relationships:srs }
  let(:consumer){ build :consumer,
    agent:agent,
    health_info:health_info,
    financial_info:financial_info,
    first_name:'Kim',
    last_name:'Hwa',
    middle_name:'Song',
    emails:[{value:'test@bar.com'}],
    state:Enum::State.find_by_abbrev('RI'),
    addresses:[{
      streeet:"Go,Away,You",
      city:"Ci",
      state_id:41,
      zip:"65432"
    }],
    company:'BigCorp',
    city:'Ci',
    zip:'65432',
    phones:[
      {
        phone_type_id:Enum::PhoneType.id('home'),
        value:'6543210987'
      },
      {
        phone_type_id:Enum::PhoneType.id('work'),
        value:'3987123456',
        ext:'234'
      }
    ],
    gender:MALE,
    ssn:'123121234',
    birth:Date.new(1963,9,19),
    birth_state:Enum::State.find_by_abbrev('ND'),
    birth_country_id:Enum::Country.id('Poland'),
    marital_status:Enum::MaritalStatus.find_by_name('Single'),
    dl_state:Enum::State.find_by_abbrev('MA'),
    dln:'fake-dl-num',
    occupation:'My Job',
    citizenship_id:1
  }
  # consumer associations
  let(:health_info){ build :crm_health_info, inches:8, feet:7, weight: 229 }
  let(:financial_info){ build :crm_financial_info, income:68000, net_worth_specified:6480000,net_worth_use_specified:true }

  #kase asssociations
  let(:marketech_dataset){ build :processing_marketech_dataset, kase:kase }
  let(:a_team_details){ build :quoting_quote,
    carrier_id:carrier_id,
    face_amount:face_amount,
    duration:Enum::TliDurationOption.find(3),
    plan_name:'some plan name',
    annual_premium:11197.0,
    monthly_premium:3256.0,
    premium_mode:Enum::PremiumModeOption.find_by_value('YEAR'),
    product_type:Enum::ProductType.find_by_name('Variable Life')
  }
  let(:carrier_id){ 2 } # Banner Life Insurance Company
  let(:face_amount){ 50000 }
  let(:agent){ build :user, id:agent_id, marketech_signature:marketech_signature, name:'Lisa Martin',company:'Wikivu' }
  let(:marketech_signature){ 'marketech-sign-32' }
  let(:srs){ [
    build(:bene_relationship, contingent:false, percentage:50, relationship_type_id:1, stakeholder:
      build( :consumer, genre_id:1, birth_or_trust_date:'1970/01/02', ssn:'000000009', full_name:'Foo P Renkins')
    ),
    build(:bene_relationship, contingent:false, percentage:50, relationship_type_id:2, stakeholder:
      build( :consumer, genre_id:1, birth_or_trust_date:'1975/09/02', ssn:'000000008', full_name:'Gene Simmons')
    ),
    build(:bene_relationship, contingent:true,  percentage:90, relationship_type_id:3, stakeholder:
      build( :consumer, genre_id:1, birth_or_trust_date:'1970/07/08', ssn:'000000007', full_name:'Ace Frehley')
    ),
    build(:bene_relationship, contingent:true,  percentage:5, relationship_type_id:4, stakeholder:
      build( :consumer, genre_id:2, birth_or_trust_date:'1971/01/02', ssn:'000000006', full_name:'Paul Stanley')
    ),
    build(:bene_relationship, contingent:true,  percentage:5, relationship_type_id:5, stakeholder:
      build( :consumer, genre_id:2, birth_or_trust_date:'1974/01/02', ssn:'000000005', full_name:'Peter Criss')
    )
  ] }

  before do
    agent.stub(:save).and_return(true)
  end

  describe '#to_ffxml' do
    it 'makes the expected xml document' do
      pattern_xml   = Nokogiri::XML::parse File.read(File::expand_path '../ffxml.xml', __FILE__), nil, nil, Nokogiri::XML::ParseOptions::NOBLANKS
      generated_xml = Nokogiri::XML::parse Processing::Marketech::UploadService.new(marketech_dataset).send(:to_ffxml)
      generated_xml.to_xml.should eq pattern_xml.to_xml
    end
  end

  describe '#upload', :vcr do
    it 'should post data to Marketech' do
      expect{ Processing::Marketech::UploadService.new(marketech_dataset).upload(agent) }.to_not raise_error
    end

    context 'when HTTP post succeeds' do
      it 'should create a Note' do
        marketech_dataset.save
        expect{ Processing::Marketech::UploadService.new(marketech_dataset).upload(agent) }.to change(Crm::Note, :count).by(1)
      end
      it 'should set xml_id and status_id on Marketech::Dataset' do
        marketech_dataset.xml_id.should be_nil
        marketech_dataset.status_id.should be_nil
        Processing::Marketech::UploadService.new(marketech_dataset).upload(agent)
        marketech_dataset.xml_id.should_not be_nil
        marketech_dataset.status_id.should eq Processing::Marketech::Dataset::APP_UPLOADED
      end
    end
    context 'when HTTP post fails' do
      let(:service_obj){ Processing::Marketech::UploadService.new(marketech_dataset) }

      before do
        service_obj.stub(:to_ffxml){ 'bad xml' }
      end

      it 'should raise an error' do
        expect{ service_obj.upload(agent) }.to raise_error
      end
      it 'should not set xml_id or status_id on Marketech::Dataset' do
        service_obj.upload(agent) rescue
        marketech_dataset.xml_id.should be_nil
        marketech_dataset.status_id.should be_nil
      end
    end
  end

end