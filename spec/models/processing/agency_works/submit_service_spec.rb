require 'spec_helper'

RSpec.configure do |c|
  c.include Processing::AgencyWorks
end

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::AgencyWorks::SubmitService do
  let(:submit_service){ Processing::AgencyWorks::SubmitService }
  let(:xml_builder   ){ Processing::AgencyWorks::XmlBuilder }
  describe '#send_to_agency_works', :vcr do
    let(:agency_works_response) do
      {tx_life:
        {user_auth_response:{trans_result:{result_code:result_code}},
         tx_life_response:{trans_result:{confirmation_id:confirmation_id}}
        }
      }
    end
    let(:message     ){ {"TXLifeArg" => 'xml'} }
    let(:current_user){ double("User", id:10) }
    let(:crm_case    ){ double("Crm::Case", notes:notes) }
    let(:notes       ){ double("Crm::Notes") }

    before do
      xml_builder.any_instance.stub(:build_new_business_submission).and_return('xml')
      submit_service.any_instance.stub(:send_soap_request).and_return( agency_works_response )
      crm_case.stub(:agency_works_id=)
      crm_case.stub(:save)
      notes.stub(:create)
    end

    context "When the result code is a failure," do
      let(:result_code    ){ "Failure" }
      let(:confirmation_id){ "o noes" }
      it "returns an error message." do
        submit_service.new(crm_case).send_to_agency_works.should == "Error submitting to AgencyWorks. Please contact support for assistance."
      end
    end

    context "When the result code is successful," do
      let(:result_code    ){ "Success" }
      let(:confirmation_id){ 42 }
      it "assigns the confirmation id." do
        crm_case.should_receive(:agency_works_id=).with( confirmation_id )
        submit_service.new(crm_case).send_to_agency_works
      end
    end
  end
end