require 'spec_helper'

RSpec.configure do |c|
  c.include Processing::AgencyWorks
end

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::AgencyWorks::UpdateService do
  let(:updater         ){ Processing::AgencyWorks::UpdateService }
  let(:updater_instance){ updater.new }

  describe '::request_all_updates', :vcr do
    let(:xml_builder    ){ double("Processing::AgencyWorks::XmlBuilder", build_request_updates:'xml') }

    before do
      $redis={}
      AwUpdateWorker.stub(:perform_at)
    end

    context 'when no AW updates are queued or started' do
      before do
        Sidekiq::ScheduledSet.stub(:new).and_return([])
        Sidekiq::Queue.stub(:new).and_return([])
        Sidekiq::Workers.stub_chain(:new,:entries).and_return([])
      end

      it "schedules the update workers for each status type code and account" do
        updater::ALL_ACCOUNT_STATUS_COMBOS.each do |aw_account_key, status_type_code|
          expect(AwUpdateWorker).to receive(:perform_at).with(
            instance_of(ActiveSupport::TimeWithZone),
            status_type_code,
            aw_account_key
          ).ordered.exactly(1).times
        end
        updater.request_all_updates
      end
    end

    context 'when too many AW updates are already queued' do
      before do
        #
      end

      it "schedules the update workers for each status type code and account" do
        #
      end
    end

    context 'when too many AW updates are already started' do
      before do
        #
      end

      it "schedules the update workers for each status type code and account" do
        #
      end
    end

  end


  describe '#request_single_update' do
    let(:case_model       ){ Crm::Case }
    let(:status_type_model){ Crm::StatusType }
    let(:empty_response   ){
      {:tx_life=>{
        :xmlns=>"http://ACORD.org/Standards/Life/2", :"xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", :"xsi:schema_location"=>"http://ACORD.org/Standards/Life/2  https://swww.agencyworks.com/2.7.92NC/TXLife2.7.92.NewCo.xsd",
        :user_auth_response=>{:trans_result=>{:result_code=>"Success"}, :svr_date=>"2014-10-30", :svr_time=>"09:32:13-06:00"},
        :tx_life_response=>{
          :trans_type=>"Holding Search", :trans_exe_date=>"2014-10-30", :trans_exe_time=>"15:32:52+0000", :inquiry_level=>"Objects", :trans_result=>{
            :result_code=>"Warning", :result_info=>{:result_info_code=>"Warning", :result_info_desc=>"No Records Found"}}, :o_lif_e=>nil}}}
    }
    let(:soap_response    ){ Processing::parse_and_format File::read('spec/support/agency_works/updates_response.xml') }
    let(:aw_policy_ct     ){ 482 } # number of policies in update response
    let(:xml_policy_ids   ){ load('spec/support/agency_works/updates_response_aw_ids.rb'); get_xml_policy_ids } #ids from update response
    let(:aw_account_key   ){ updater::ALL_ACCOUNT_STATUS_COMBOS[0][0] }
    let(:status_type_code ){ updater::ALL_ACCOUNT_STATUS_COMBOS[0][1] }

    before do
      Savon.should_not_receive(:client) # because :send_soap_request shall be stubbed
      updater_instance.stub(:send_soap_request){
        if @first_soap_request_sent
          empty_response
        else
          @first_soap_request_sent = true
          soap_response
        end
      }
      case_model.stub( scoped: case_model, where: case_model, preload: case_model, includes: case_model)
      expect( SystemMailer ).to_not receive(:error)
    end

    context 'when NO policies in the xml have matches in the database,' do
      before do
        case_model.stub(:to_a).and_return([])
      end
      it 'follows the appropriate flow' do
        expect(updater_instance).to receive(:extract_policies!     ).exactly(1            ).times.and_call_original
        expect(updater_instance).to receive(:apply_update          ).exactly(aw_policy_ct ).times.and_call_original
        expect(updater_instance).to receive(:extract_status_type_id).exactly(0            ).times.and_call_original
        expect(updater_instance).to receive(:log_totals            ).exactly(1            ).times.and_call_original

        updater_instance.request_single_update status_type_code, aw_account_key
      end

      it 'counts indicate that NO policies match' do
        updater_instance.request_single_update status_type_code, aw_account_key

        expect( updater_instance.xml_policy_ids.length ).to eq(aw_policy_ct)
        expect( updater_instance.dr_policy_ids.length  ).to eq(0)
      end
    end

    context 'when all policies in the xml have matches in the database,' do
      before do
        @matched_policies||=xml_policy_ids.map do |p|
          kase = build_stubbed :crm_case, policy_number:nil, effective_date:nil, agency_works_id: p, current_details: (build_stubbed :quoting_quote)
          kase.stub(:status_type_id=  ).and_return(true)
          kase.stub(:update_attributes){|attrs| kase.assign_attributes(attrs) }
          kase.stub(:reload           ).and_return(true)
          kase.current_details.stub(:update_attributes){|attrs| kase.current_details.assign_attributes(attrs) }
          kase
        end
        case_model.stub(:to_a).and_return(@matched_policies)
      end

      it 'follows the appropriate flow' do
        expect(updater_instance).to receive(:extract_policies!     ).exactly(1           ).times.and_call_original
        expect(updater_instance).to receive(:apply_update          ).exactly(aw_policy_ct).times.and_call_original
        expect(updater_instance).to receive(:extract_status_type_id).exactly(aw_policy_ct).times.and_call_original
        expect(updater_instance).to receive(:log_totals            ).exactly(1           ).times.and_call_original

        updater_instance.request_single_update status_type_code, aw_account_key
      end

      it 'finds all insureio policies to match' do
        updater_instance.request_single_update status_type_code, aw_account_key

        updater_instance.stub(:apply_update)

        expect( updater_instance.xml_policy_ids.length ).to eq(aw_policy_ct)
        expect( updater_instance.dr_policy_ids.length  ).to eq(aw_policy_ct)
      end

      context 'and NO status types match the AW statuses specified in the XML,' do
        before do
          #Since we're using the `let` syntax to define `updater_instance`,
          #it already has a map of statuses populated.
          #So we just need to empty the status map.
          updater_instance.stub(:status_types).and_return({})
        end
        it 'counts should indicate that ALL policies lack a proscribed status' do
          expect( instance_of(Crm::Case) ).not_to receive(:status_type_id=)

          updater_instance.request_single_update status_type_code, aw_account_key

          expect( updater_instance.lack_status_ids.length              ).to eq(aw_policy_ct)
          expect( updater_instance.successful_status_change_ids.length ).to eq(0)
        end
      end
      context 'and local status types match all of the AW statuses specified in the XML,' do
        let(:status_type){ build :crm_status_type, id:1, name:'fiddle-dee-dee' }
        let(:aw_policy_ct){ 480 }#two fewer, because we're removing them in the `before` block below.
        before do
          updater_instance.status_types.each do |k,v|
            updater_instance.status_types[k]=status_type#replace any nils with our stub
          end
          updater_instance.stub(:send_soap_request){
            #Remove the two with statuses "Void" and "Informal application submitted".
            aw_policies=soap_response.dig(:tx_life,:tx_life_response,:o_lif_e,:holding)
            aw_policies.reject!{|p| ['Void','Informal application submitted'].include?(p[:policy][:policy_status]) }
            soap_response
          }
        end
        it 'counts should indicate status changes' do
          updater_instance.request_single_update status_type_code, aw_account_key

          expect( updater_instance.successful_status_change_ids.length  ).to eq(aw_policy_ct)
          expect( updater_instance.lack_status_ids.length               ).to eq(0)
        end
      end
      it 'updates `policy_number` and `effective_date` where provided' do

        expect( updater_instance.errors ).to be_blank, "`updater_instance.errors`:\n#{updater_instance.errors}"
        expect( @matched_policies[3] ).to receive(:policy_number=).with(
          "180992620"
        ).exactly(1).times
        expect( @matched_policies[14] ).to receive(:effective_date=).with(
          Date.parse("2014-09-12")
        ).exactly(1).times

        updater_instance.request_single_update status_type_code, aw_account_key
      end
    end
  end

  #In future we may want to add separate tests covering the private methods called by `#request_single_update`.
  #These include: extract_policies!, apply_update, extract_status_type_id, log_totals
end
