require 'spec_helper'

VCR.configure do |c|
  c.hook_into :excon
end

describe Processing::AgencyWorks::CommentsService, :vcr do

  describe '#request' do
    let(:consumer       ){ build :consumer, tenant_id:0 }
    let(:agency_works_id){ 78943789 }
    let(:kase           ){ build :crm_case, agency_works_id:agency_works_id, consumer: consumer }
    subject {Processing::AgencyWorks::CommentsService.new(kase)}

    context 'given invalid AW id' do
      it 'receives and handles an error response' do
        notes = subject.request
        expect(notes).to be_a Array
        expect(notes).to be_empty
        expect(subject.error).to eq "No Records Found"
      end
    end

    context 'given nil AW id' do
      let(:agency_works_id){nil}
      it 'receives and handles an error response' do
        notes = subject.request
        expect(notes        ).to be_a Array
        expect(notes        ).to be_empty
        expect(subject.error).to eq "Attempted to get AgencyWorks Case Requirements and Notes for a case that lacks an AgencyWorks Id."
      end
    end

    context 'with response stubbed as valid data' do
      let(:response){ load 'spec/support/agency_works/comments_response_hash.rb'; get_response_hash }

      before do
        allow(subject).to receive(:send_soap_request).and_return(response)
      end

      it 'returns Array of Crm::Note' do
        #It looks like our example from 2014 is out of date with the current format of notes, which are always threaded.
        #Need to collect a newer sample for this test.
        notes = subject.request
        expect(notes).to be_a Array
        expect(notes.length).to eq 0
        # notes.each do |note|
        #   expect(note             ).to be_a Crm::Note
        #   expect(note.creator.full_name).to be_present
        #   expect(note.id          ).to be > 0
        #   expect(note.text        ).to be_present
        #   expect(note.confidential).to_not be_nil
        #   expect(note.created_at  ).to be_present
        # end
      end

      it 'holds Array of Crm::CaseRequirement' do
        subject.request
        expect(subject.requirements).to be_a Array
        expect(subject.requirements.length).to eq 23
        subject.requirements.each do |req|
          expect(req).to be_a Crm::CaseRequirement
        end
        req = subject.requirements.first
        expect(req.id                                  ).to eq 216106666
        expect(req.name                                ).to eql "Abbreviated Blood Chemistry" 
        expect(req.case_req_responsible_party_type.name).to eql "MGA"
        expect(req.ordered_at.to_date                  ).to eql Date.parse "2014-09-30"
        expect(req.completed_at.to_date                ).to eql Date.parse "2014-10-06"
        expect(req.case_requirement_type.name          ).to eql "Underwriting"
        expect(req.case_req_status.name                ).to eql "Received"
        expect(req.created_at.to_date                  ).to eql Date.parse "2014-09-30"
      end
    end
  end

end
