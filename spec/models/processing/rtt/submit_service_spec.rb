require 'spec_helper'
require "processing"
require "savon"

VCR.configure do |c|
  c.hook_into :excon
end

describe 'Processing::Rtt::SubmitService' do

  describe "#send_scor_soap_request", :vcr do
    before do
      @sample_req_content=File::read('spec/support/rtt/sample_scor_underwriting_req.xml')
      @sample_req_content=@sample_req_content.sub('<ns2:password>XXXXXXXXXXXXXX</ns2:password>','<ns2:password>'+APP_CONFIG['scor']['test']['xml_password']+'</ns2:password>')
      @policy_id="IO_SCOR_#{SecureRandom.uuid}"
      @sample_req_content=@sample_req_content.sub('<application policyNumber="486a11111111-fbcf-4023-8057-b4575c9caf1e">','<application policyNumber="'+@policy_id+'">')
    end
    it "does not raise an exception" do
      Processing::Rtt.stub(:extract_rtt_and_scor_data_from_note).and_return({})
      expect{
        Processing::Rtt::SubmitService.new(Crm::Case.new(consumer:Consumer.new)).send(:send_scor_soap_request,@sample_req_content)
      }.to_not raise_exception
    end
  end
  # describe "#send_to_motorists", :vcr do
  # end

  # describe '#transfer_xml_file' do
  # end
end
