require 'spec_helper'
require "processing"
require "savon"

VCR.configure do |c|
  c.hook_into :excon
end

describe 'Processing::Rtt::PollingService' do
  let(:current_user){ build :user }
  before do
    @sample_req_content=File::read('spec/support/rtt/sample_scor_polling_req.xml')
    @sample_response_hash=JSON.parse( File::read('spec/support/rtt/sample_scor_polling_response.json')).with_indifferent_access
    @policy=Crm::Case.new(agent: current_user, consumer:Consumer.new)
    @polling_service_instance=Processing::Rtt::PollingService.new(@policy)
  end

  describe "#compose_status_request", :vcr do
    pending('Spec needs implementation.')
  end

  context 'when response indicates a decision' do
    before do
      #allow(@policy).to receive(:update_attributes).and_return(true)
      allow(@policy).to receive_message_chain(:reload,:current_details,:update_attributes).and_return(true)
      pretend_client=Struct.new(:operation,:call,:body).new
      allow(pretend_client).to receive(:operation).and_return(pretend_client)
      allow(pretend_client).to receive(:call).and_return(pretend_client)
      allow(pretend_client).to receive(:body).and_return({fetch_status_response:{fetch_status_return:'<?xml version="1.0"?><placeholder></placeholder>'}})
      allow(Savon).to receive(:client).and_return(pretend_client)
      allow(@polling_service_instance).to receive(:response_hash=).and_return(@sample_response_hash)
      allow(@polling_service_instance).to receive(:response_hash).and_return(@sample_response_hash)
    end

    describe '#send_scor_soap_request' do
      it "does not raise an exception" do
        expect{
          @polling_service_instance.send(:send_scor_soap_request, @sample_req_content)
        }.to_not raise_exception
      end
      it 'extracts the `decision_reasons` from the polling response' do
        @polling_service_instance.send(:send_scor_soap_request, @sample_req_content)
        expect( @polling_service_instance.decision_reasons ).to include("Policy Accepted - Better than Applied For")
      end
    end

    describe '#update_policy_status' do
      before do
        @polling_service_instance.send(:send_scor_soap_request, @sample_req_content)
      end
      it 'does not raise an exception' do
        expect{
          @polling_service_instance.send(:update_policy_status)
        }.to_not raise_exception
      end
      it 'sets the proper status based on the polling response' do
        st_id=Crm::StatusType::PRESETS[:rtt_scor_approved_ot_applied][:id]
        expect( @policy ).to receive(:update_attributes).with(status_type_id: st_id)
        @polling_service_instance.send(:update_policy_status)
      end
    end

  end
end
