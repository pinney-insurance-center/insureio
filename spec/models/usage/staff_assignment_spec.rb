require 'spec_helper'

describe Usage::StaffAssignment do
  let(:staff){ build :usage_staff_assignment, agent_id:1, underwriter_id:2, application_specialist_id:3, insurance_coordinator_id:4, administrative_assistant_id:5, case_manager_id:6, manager_id:7 }

  describe '#symbol_for_role_id' do
    it 'returns correct symbols for each role id' do
      staff.symbol_for_role_id(Enum::UsageRole['agent'].id).should                    == :agent_id
      staff.symbol_for_role_id(Enum::UsageRole['underwriter'].id).should              == :underwriter_id
      staff.symbol_for_role_id(Enum::UsageRole['application specialist'].id).should   == :application_specialist_id
      staff.symbol_for_role_id(Enum::UsageRole['insurance coordinator'].id).should    == :insurance_coordinator_id
      staff.symbol_for_role_id(Enum::UsageRole['administrative assistant'].id).should == :administrative_assistant_id
      staff.symbol_for_role_id(Enum::UsageRole['case manager'].id).should             == :case_manager_id
      staff.symbol_for_role_id(Enum::UsageRole['manager'].id).should                  == :manager_id
    end
  end

  describe '#user_id_for_role_id' do
    context 'when staff assignment owner is a User or Crm::Case,' do
      before do
        staff.owner_type='Crm::Case'
      end
      it 'returns the saved id for each role' do
        staff.user_id_for_role_id(Enum::UsageRole['agent'].id).should                    == 1
        staff.user_id_for_role_id(Enum::UsageRole['underwriter'].id).should              == 2
        staff.user_id_for_role_id(Enum::UsageRole['application specialist'].id).should   == 3
        staff.user_id_for_role_id(Enum::UsageRole['insurance coordinator'].id).should    == 4
        staff.user_id_for_role_id(Enum::UsageRole['administrative assistant'].id).should == 5
        staff.user_id_for_role_id(Enum::UsageRole['case manager'].id).should             == 6
        staff.user_id_for_role_id(Enum::UsageRole['manager'].id).should                  == 7
      end
    end
    context 'when staff assignment owner is a Consumer,' do
      before do
        staff.owner_type='Consumer'
      end
      it 'ignores saved ids and returns preset id for each role' do
        staff.user_id_for_role_id(Enum::UsageRole['underwriter'].id).should              == 139#Rachel Sanfilippo
        staff.user_id_for_role_id(Enum::UsageRole['application specialist'].id).should   == 152#Parisa  Masegian
        staff.user_id_for_role_id(Enum::UsageRole['insurance coordinator'].id).should    == 152#Parisa  Masegian
        staff.user_id_for_role_id(Enum::UsageRole['administrative assistant'].id).should == 180#Maggie Scanlon
      end
    end
  end

  describe '#set_user_id_for_role_id' do
    it 'sets id for each role' do
      staff.set_user_id_for_role_id Enum::UsageRole['agent'].id, 11
      staff.set_user_id_for_role_id Enum::UsageRole['underwriter'].id, 12
      staff.set_user_id_for_role_id Enum::UsageRole['application specialist'].id, 13
      staff.set_user_id_for_role_id Enum::UsageRole['insurance coordinator'].id, 14
      staff.set_user_id_for_role_id Enum::UsageRole['administrative assistant'].id, 15
      staff.set_user_id_for_role_id Enum::UsageRole['case manager'].id, 16
      staff.set_user_id_for_role_id Enum::UsageRole['manager'].id, 17
      staff.agent_id.should                    == 11
      staff.underwriter_id.should              == 12
      staff.application_specialist_id.should   == 13
      staff.insurance_coordinator_id.should    == 14
      staff.administrative_assistant_id.should == 15
      staff.case_manager_id.should             == 16
      staff.manager_id.should                  == 17
    end
  end

end
