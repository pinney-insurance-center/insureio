require 'spec_helper'

describe Brand do

  describe 'adding lead_types' do
    it 'should be uniq' do
      brand = Brand.new
      brand.lead_types << Crm::LeadType.find_or_create_by_text('foo')
      brand.lead_types << Crm::LeadType.find_or_create_by_text('foo')
      brand.lead_types << Crm::LeadType.find_or_create_by_text('bar')
      brand.lead_types << Crm::LeadType.find_or_create_by_text('foo')
      brand.lead_types.length.should == 2
    end
  end

  describe 'adding referrers' do
    it 'should be uniq' do
      brand = Brand.new
      brand.referrers << Crm::Referrer.find_or_create_by_text('foo')
      brand.referrers << Crm::Referrer.find_or_create_by_text('foo')
      brand.referrers << Crm::Referrer.find_or_create_by_text('bar')
      brand.referrers << Crm::Referrer.find_or_create_by_text('foo')
      brand.referrers.length.should == 2
    end
  end
end
