require 'spec_helper'

describe User do
  let(:user){ build :user }

  describe '::editable_by' do

    shared_examples_for 'editable_by_scope' do
      it 'returns a scope' do
        user.enable_permissions! *enabled_permissions
        User.editable_by(user).should be_a ActiveRecord::Relation
      end
    end

    before do
      user.stub(:super?).and_return(false)
    end
    context 'when user has no view privileges for sibling, nephew' do
      let(:enabled_permissions){ [] }
      it_behaves_like 'editable_by_scope'
    end
    context 'when user has no view privileges for nephew' do
      let(:enabled_permissions){ [:view_siblings] }
      it_behaves_like 'editable_by_scope'
    end
    context 'when user has no view privileges for sibling' do
      let(:enabled_permissions){ [:view_nephews] }
      it_behaves_like 'editable_by_scope'
    end
    context 'when user has view privileges for sibling, nephew' do
      let(:enabled_permissions){ [:view_nephews, :view_siblings] }
      it_behaves_like 'editable_by_scope'
    end
    context 'when user is has :super_edit permission' do
      let(:enabled_permissions){ [:super_edit] }
      it_behaves_like 'editable_by_scope'
    end
    context 'when user has edit privileges for sibling, descendants, nephew, lacks cross-domain permission, and parent has different subdomain' do
      let(:enabled_permissions){ [:edit_nephews, :edit_descendants, :edit_siblings] }
      let(:parent){ User.create full_name: Forgery::Name.full_name, emails_attributes:[{value:"theParentUser@#{Forgery::Internet.domain_name}"}], tenant_id:2 }
      let(:user  ){ User.create full_name: Forgery::Name.full_name, emails_attributes:[{value:"theChildUser@#{Forgery::Internet.domain_name}"}], tenant_id:1 }
      before do
        #Assigning parent_id here versus above more accurately simulates a typical scenario in which
        #one would use the scope, where the parent association is probably not already loaded into memory.
        user.update_column(:parent_id, parent.id)
        user.disable_permissions! *Usage::Permissions::CROSS_TENANCY_PERMISSIONS
        Thread.current[:tenant_id]=user.tenant_id
        Thread.current[:current_user_acts_across_tenancies]=false
        expect( user.parent_id ).to_not eq(nil)
        expect( user.parent_id ).to     eq(parent.id)
        expect( user.parent    ).to     eq(nil)#due to the default scope enforcing tenancy restriction
      end
      it_behaves_like 'editable_by_scope'
    end
  end

  describe '::viewable_by' do

    shared_examples_for 'viewable_by_scope' do
      it 'returns a scope' do
        user.enable_permissions! *enabled_permissions
        User.editable_by(user).should be_a ActiveRecord::Relation
      end
    end

    before do
      user.stub(:super?).and_return(false)
    end
    context 'when user has no view privileges for sibling, nephew' do
      let(:enabled_permissions){ [] }
      it_behaves_like 'viewable_by_scope'
    end
    context 'when user has no view privileges for nephew' do
      let(:enabled_permissions){ [:view_siblings] }
      it_behaves_like 'viewable_by_scope'
    end
    context 'when user has no view privileges for sibling' do
      let(:enabled_permissions){ [:view_nephews] }
      it_behaves_like 'viewable_by_scope'
    end
    context 'when user has view privileges for sibling, nephew' do
      let(:enabled_permissions){ [:view_nephews, :view_siblings] }
      it_behaves_like 'viewable_by_scope'
    end
    context 'when user is has :super_edit permission' do
      let(:enabled_permissions){ [:super_edit] }
      it_behaves_like 'viewable_by_scope'
    end

  end

end