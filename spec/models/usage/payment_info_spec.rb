require 'spec_helper'

describe Usage::PaymentRelatedMethods, :vcr do
  let(:host_with_port){'localhost:3000'}
  #let(:user){build :user }


  describe '#stripe_create_customer' do
    let(:current_user){ build :agent, membership_id:1 }
    it 'raises error when token is nil' do
      current_user.stripe_create_customer(nil, 1).should be false
    end
    # !!! IMPORTANT !!!
    # If you delete the vcr cassette for this test, you need to come up with a
    # new, valid token on your own (possibly by putting a pry in the
    # controller and capturing a token before it gets used).
    it 'succeeds when given valid token', :vcr do
      token = JSON.parse(File.read('spec/support/stripe/create.json')).with_indifferent_access[:stripeToken]
      current_user.stripe_create_customer(token, 1).should be true, current_user.errors.full_messages.join(". ")
      current_user.stripe_customer_id.should == 'cus_4x592fpqDMVfmb'
    end
  end

end
