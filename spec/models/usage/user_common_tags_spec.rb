require 'spec_helper'

describe User do
  let (:user){ build :user }
  
  describe '#common_tags' do
    context 'when never set' do
      it 'returns Array' do
        user.save
        user.reload.common_tags.should be_a Array
      end
    end
    context 'when set' do
      it 'returns Array' do
        user.common_tags<< ['foo', nil]
        user.common_tags<< ['bar','baz']
        user.save
        user.reload.common_tags.should be_a Array
      end
    end
    it 'can store nil-value common_tags' do
      user.common_tags.should_not include(['foo', nil])
      user.common_tags.should_not include(['bar','baz'])

      user.common_tags<< ['foo', nil]
      user.common_tags<< ['bar',nil]

      user.common_tags.should include(['foo', nil])
      user.common_tags.should include(['bar', nil])
    end
    it 'can store non-nil-value common_tags' do
      user.common_tags.should_not include(['foo', 'qux'])
      user.common_tags.should_not include(['bar','baz'])

      user.common_tags<< ['foo', 'qux']
      user.common_tags<< ['bar', 'baz']

      user.common_tags.should include(['foo', 'qux'])
      user.common_tags.should include(['bar', 'baz'])
    end
  end
  it 'can store multiple common_tags with the same key but different values' do
    user.common_tags<< ['foo', 'qux']
    user.common_tags<< ['foo', 'baz']

    user.common_tags.should include(['foo', 'qux'])
    user.common_tags.should include(['foo', 'baz'])
  end
end