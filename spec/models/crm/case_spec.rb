require 'spec_helper'
require 'support/crm/accessible_examples'
require 'support/data_helper'

RSpec.configure do |config|
  config.include DataHelper
end

describe Crm::Case do
  let(:brand){ Brand.find_or_create_by_owner_id(2001, name:'Brutus') }

  describe '#viewables' do
    # Define users
    subject { create(:user, parent: parent).tap do |user|
      user.can! :super_view, false
      user.can! :super_edit, false
      user.save!
    end }
    let(:parent) { create :user }
    let(:sibling) { create :user, parent: parent }
    let(:unrelated_user) { create :user, parent_id: 0 }
    # Define brands
    let(:owned_brand) { create :brand, owner: subject }
    let(:member_brand) { create :brand, members: [subject] }
    let(:sibling_brand) { create :brand, owner: sibling }
    let(:unrelated_brand) { create :brand }
    # Define cases
    let(:kase1) { create :crm_case, consumer: create(:consumer, brand: owned_brand) }
    let(:kase2) { create :crm_case, consumer: create(:consumer, brand: member_brand) }
    let(:kase3) { create :crm_case, consumer: create(:consumer, brand: sibling_brand) }
    let(:kase4) { create :crm_case, consumer: create(:consumer, brand: unrelated_brand, agent: unrelated_user) }
    let(:kase5) { create :crm_case, consumer: create(:consumer, brand: unrelated_brand, agent: sibling) }

    before do
      kase1; kase2; kase3; kase4; kase5
    end

    context 'for user who can view sibling\'s resources' do
      before do
        subject.can! :view_siblings_resources
        subject.save!
      end

      it 'scopes cases owned by siblings' do
        expect(Crm::Case.viewables(subject).pluck :id).to include kase5.id
      end

      it 'excludes cases not owned by siblings, under sibling-owned brands' do
        expect(Crm::Case.viewables(subject).pluck :id).not_to include kase3.id
      end
    end

    context 'for user who can NOT edit or view sibling\'s resources' do
      before do
        subject.can! :view_siblings_resources, false
        subject.save!
      end

      it 'excludes cases owned by siblings' do
        expect(Crm::Case.viewables(subject).pluck :id).not_to include kase5.id
      end
    end

    context 'for user who can view all brand resources' do
      before do
        subject.can! :view_brand_resources
        subject.save!
      end

      it 'scopes cases for all brands of which subject is a member or owner' do
        expect(Crm::Case.viewables(subject).pluck :id).to match_array [kase1, kase2].map &:id
      end
    end
  end

  context "for sales support" do
    SUPPORT_ROLES = [:administrative_assistant, :case_manager, :application_specialist, :sales_coordinator, :insurance_coordinator]
    let(:kase) { build :crm_case, id:rand(500) }
    let(:user) { u = User.new; u.save(validate:false); u }
    let(:staff_assignment) { Usage::StaffAssignment.new }

    before do
      user.can!(:sales_support,true)
      kase.stub_chain(:agent, :staff_assignment).and_return(staff_assignment)
      kase.stub(:set_initial_status!).and_return(true)
      kase.stub_chain(:person, :staff_assignment).and_return(staff_assignment)
    end

    context "who is on agent's staff assignment" do
      it "is accessible" do
        # stubbing
        staff_assignment.stub(:include?).and_return(true)
        # running
        staff_assignment.should_receive(:include?).with(user)
        kase.editable?(user).should == true
        kase.viewable?(user).should == true
      end
    end

    context "who is not on agent's staff assignment" do
      let(:agent){build :agent}
      
      before do
        staff_assignment.stub(:include?).and_return(false)
        allow(kase).to receive(:agent).and_return(agent)
      end      

      context "who has a task on kase" do
        it "is accessible" do
          status_type = build :crm_status_type, id:0
          status_type.stub(:task_builders).and_return []
          status = build_stubbed :crm_status, statusable: kase, status_type: status_type
          Task.any_instance.stub(:_ensure_person!).and_return(true)
          task = create :task, origin: status, assigned_to_id: user.id
          expect(kase.editable? user).to be true
          expect(kase.viewable? user).to be true
        end
      end

      context "who has no case and is not on agent's staff assignment" do
        it "is not accessible" do
          expect(kase.editable? user).to be false
          expect(kase.viewable? user).to be false
        end
      end
    end
  end

  describe '#find_agent_for_assignment' do
    pending "to do"
  end

  describe '#infer_termination_date' do
    it 'works even on leap year' do
      subject = create :crm_case, consumer: create(:consumer)
      subject.current_details.update! duration: Enum::TliDurationOption.find(5)
      subject.termination_date = nil
      subject.effective_date = Date.new(2020, 2, 29) # leap day
      expect(subject.termination_date).to be_nil
      expect {
        subject.infer_termination_date
      }.to_not raise_exception
      expect(subject.termination_date).to be_present
    end
  end

  describe '#push_to_esp' do
    let(:cons) { build :consumer, brand_id: Brand::NMB_ID }
    let(:kase) { build :crm_case, sales_stage_id: 2, consumer: cons, current_details: quote }
    let(:quote) { build :quoting_quote, duration_id: 23 }

    it 'is triggered :after_commit' do
      expect(kase.esp_push_pending?).to be true
      expect(kase).to receive(:push_to_esp)
      kase.run_callbacks :commit
    end

    it 'is not triggered for opportunities' do
      kase.sales_stage_id = 1
      expect(kase.esp_push_pending?).to be false
      expect(kase).to_not receive(:push_to_esp)
      kase.run_callbacks :commit
    end

    it 'is not triggered if ext[:esp_push]' do
      kase.ext[:esp_push] = 1.minute.ago
      expect(kase.esp_push_pending?).to be false
      expect(kase).to_not receive(:push_to_esp)
      kase.run_callbacks :commit
    end
  end

  describe '#status_type_id=' do
    let(:kase){ create :crm_case }
    let(:type1){ create :crm_status_type }
    let(:type2){ create :crm_status_type }
    let(:tasks1){ create_list :crm_task_builder, owner:type1 }
    let(:tasks2){ create_list :crm_task_builder, owner:type2 }
    it 'deactivates old tasks' do
      kase.status_type_id=(type1)
      status1 = kase.status
      status1.tasks.each do |t|
        t.active.should == true
      end
      kase.status_type_id=(type2)
      status2 = kase.status
      status2.tasks.each do |t|
        t.active.should == true
      end
      status1.tasks.each do |t|
        t.active.should == false
      end
    end
  end
  describe '#update_attributes with key :status_type_id' do
    let(:kase){   create :crm_case }
    let(:type1){  create :crm_status_type }
    let(:type2){  create :crm_status_type }
    let(:tasks1){ create_list :crm_task_builder, owner:type1 }
    let(:tasks2){ create_list :crm_task_builder, owner:type2 }
    it 'deactivates old tasks' do
      kase.update_attributes(status_type_id:type1.id)
      status1 = kase.status
      status1.tasks.each do |t|
        t.active.should == true
      end
      kase.update_attributes(status_type_id:type2.id)
      status2 = kase.status
      status2.tasks.each do |t|
        t.active.should == true
      end
      status1.tasks.each do |t|
        t.active.should == false
      end
    end
  end

  describe '#create_note_for_aor_change' do
    subject{kase.create_note_for_aor_change(perpetrator, former_aor)}
    let(:kase){build_stubbed :crm_case, agent_of_record:aor}
    let(:perpetrator){build_stubbed :user}
    let(:former_aor){build_stubbed :user}
    let(:aor){build_stubbed :user}
    context 'when former aor is nil' do
      let(:former_aor){nil}
      it 'creates a Note' do
        expect{subject}.to change(Note, :count).by(1)
      end
    end
    context 'when former aor is a String' do
      let(:former_aor){'Bruce Winning'}
      it 'creates a Note' do
        expect{subject}.to change(Note, :count).by(1)
      end
    end
    context 'when former aor is a User' do
      it 'creates a Note' do
        expect{subject}.to change(Note, :count).by(1)
      end
    end
    context 'when perpetrator is a String' do
      let(:former_aor){'Parisa M'}
      it 'creates a Note' do
        expect{subject}.to change(Note, :count).by(1)
      end
    end
  end

  describe 'creating an attachment' do
    let(:cons) { create :consumer }
    let(:kase) { create :crm_case, consumer: cons }
    let(:file) { ActionDispatch::Http::UploadedFile.new tempfile: File.new('public/images/join-2.png'), content_type: 'image/png', original_filename: 'join-2.png' }
    subject {
      kase.attachments.build(file_for_upload: file).tap do |attachment|
        aws_s3_client = Aws::S3::Client.new stub_responses: true
        attachment.instance_variable_set :@aws_s3_client, aws_s3_client
      end
    }

    it 'does not raise error' do
      expect { subject.save! }.to_not raise_error
      expect(kase.attachments.reload.length).to eq 1
    end

    it 'infers consumer' do
      subject.save!
      expect(subject.person).to be_a Consumer
      expect(subject.person_id).to eq kase.consumer_id
    end
  end

  describe '#viewable?' do
    subject { create :crm_case, consumer: cons }
    let(:cons) { create :consumer, agent: user }
    let(:user) { create :user }
    let(:other_cons) { create :consumer, agent: user }

    context 'auth is :ltd_access_code is set' do
      it 'restricts access based on associated consumer' do
        expect(subject.viewable?(user)).to be true
        expect(cons.viewable?(user)).to be true
        expect(other_cons.viewable?(user)).to be true
        Thread.current[:current_user_auth] = :ltd_access_code
        expect(subject.viewable?(user)).to be false
        expect(cons.viewable?(user)).to be false
        expect(other_cons.viewable?(user)).to be false
        Thread.current[:sess_consumer_id] = cons.id
        expect(subject.viewable?(user)).to be true
        expect(cons.viewable?(user)).to be true
        expect(other_cons.viewable?(user)).to be false
        Thread.current[:sess_consumer_id] = other_cons.id
        expect(subject.viewable?(user)).to be false
        expect(cons.viewable?(user)).to be false
        expect(other_cons.viewable?(user)).to be true
      end
    end
  end
end
