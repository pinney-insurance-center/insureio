require 'spec_helper'

VCR.configure do |c|
  c.hook_into :faraday
end

describe Crm::Status do
  let(:kase_id)     {33}
  let(:role_id)     {55}
  let(:client)      { Consumer.find_by_id(44) || build(:consumer, id:44) }
  let(:kase)        { build :crm_case_w_assoc, id:kase_id, consumer:client }
  let(:status_type) { build :crm_status_type, id:1 }
  let(:status)      { build :crm_status, id:22, statusable:kase, status_type:status_type }
  let(:task_builder){ build :crm_task_builder, role_id:role_id, owner:status_type }
  let(:assignee)    { build :user }

  before do
    assignee.stub(:save).and_return true
    kase.stub(:save).and_return true
    status_type.stub(:save).and_return true
    task_builder.stub(:save).and_return true
  end

  describe '#create_tasks' do
    context 'with 0 task builders' do
      before do
        status_type.stub(:task_builders).and_return []
      end
      it 'builds no tasks' do
        expect{ status.create_tasks }.to change(Task, :count).by(0)
      end
    end

    context 'with 1 task builder' do
      before do
        status_type.stub(:task_builders).and_return [task_builder]
        Task.any_instance.stub(:auto_assign!).and_return assignee
        Task.any_instance.stub(:publish_to_redis)
        Task.any_instance.stub(:status_model).and_return 'Crm::Status'
        Crm::Status.any_instance.stub(:statusable).and_return kase

      end
      it 'creates task w/ correct case' do
        expect{ status.save }.to change(Task, :count).by(1)
        @task = Task.last
        @task.person_id.should == client.id
        @task.status.statusable_id.should == kase_id
      end
    end
  end

  describe 'callback to deactivate tasks' do
    it 'should deactivate tasks only if status.active is false' do
      status.save
      status.tasks.create label:'test'
      status.tasks.to_a.should_not be_empty
      status.tasks.each do |t|
        t.active.should == true
      end
      status.update_attributes status_type_name: 'foo'
      status.tasks.reload.each do |t|
        t.active.should == true
      end
      status.update_attributes active: false
      status.tasks.reload.each do |t|
        t.active.should == false
      end
    end
  end

end