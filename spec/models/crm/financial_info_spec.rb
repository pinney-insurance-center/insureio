require 'spec_helper'

describe Crm::FinancialInfo do
  let(:fin) { Crm::FinancialInfo.new }
  describe '#spouse_years_to_work=' do
    it 'works with string' do
      fin.spouse_years_to_work = "4"
      fin.spouse_years_to_work.should == 4
    end
    it 'works with int' do
      Timecop.freeze {
        fin.spouse_years_to_work = 4
        fin.spouse_retirement.to_date.should == 4.years.from_now.to_date
        fin.spouse_years_to_work.should == 4
      }
    end
    it 'doesn\'t fail with nil' do
      fin.spouse_years_to_work = nil
      fin.spouse_years_to_work.should be_nil
    end
  end
end