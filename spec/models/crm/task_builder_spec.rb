require 'spec_helper'

describe Crm::TaskBuilder do

  describe 'email template validation' do
    it 'allows save on record that is not an email task and does not have a template' do
      tb = Crm::TaskBuilder.new task_type:Enum::TaskType['to-do'], template_id:nil
      tb.save.should == true
    end
    it 'allows save on record that is an email task and has a template' do
      template = Marketing::Email::Template.create
      tb = Crm::TaskBuilder.new task_type:Enum::TaskType['email'], template:template, role_id:5
      tb.save.should == true
    end
    it 'prevents save on record that is an email task and does not have a template' do
      tb = Crm::TaskBuilder.new task_type:Enum::TaskType['email'], template_id:nil
      tb.save.should == false
    end
    it 'prevents save on record that is an email-agent task and does not have a template' do
      tb = Crm::TaskBuilder.new task_type:Enum::TaskType['email agent'], template_id:nil
      tb.save.should == false
    end
  end

end