require 'spec_helper'

describe Crm::HealthInfo do
  subject { Crm::HealthInfo.new.tap { |h| h.save validate: false } }
  let(:brand) { create :brand }
  let(:agent) { create :user, brands: [brand] }

  describe 'attributes=' do
    it 'can set build' do
      attrs = {"feet":5,"inches":6,"weight":167}
      health = Crm::HealthInfo.new(attrs).tap(&:save!)
      expect(health.feet).to eq 5
      expect(health.inches).to eq 6
      expect(health.weight).to eq 167
    end

    it 'can set alcohol_abuse' do
      attrs = [{"type_id":1,"date":"11/11/1999","currently_consuming":"1","last_consumed_date":"11/11/1999","ever_treated":"1","currently_treated":"1","treatment_end_date":"11/11/1999","ever_relapse":"1","last_relapse_date":"11/11/1999"}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.alcohol_abuse?).to eq true
      diseases = health.diseases
      expect(diseases.alcohol_abuse.currently_consuming).to eq true
      expect(diseases.alcohol_abuse.currently_treating).to eq true
      expect(diseases.alcohol_abuse.date).to eq Date.new(1999,11,11)
      expect(diseases.alcohol_abuse.ever_relapse).to eq true
      expect(diseases.alcohol_abuse.ever_treated).to eq true
      expect(diseases.alcohol_abuse.last_consumed_date).to eq Date.new(1999,11,11)
      expect(diseases.alcohol_abuse.last_relapse_date).to eq Date.new(1999,11,11)
      expect(diseases.alcohol_abuse.last_treatment).to eq Date.new(1999,11,11)
    end

    it 'can set anxiety' do
      attrs = [{"type_id":2,"date":"11/11/1999","condition_degree":"Mild","ever_hospitalized":"1","last_hospitalized_date":"11/11/1999","episodes_year":6,"currently_treated":"1","outpatient_care":"1","inpatient_care":"1","work_absence":"1","ever_suicide_attempt":"1","last_suicide_attempt_date":"11/11/1999"}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.anxiety?).to eq true
      diseases = health.diseases
      expect(diseases.anxiety.condition_degree).to eq 'Mild'
      expect(diseases.anxiety.currently_treating).to eq true
      expect(diseases.anxiety.date).to eq Date.new(1999,11,11)
      expect(diseases.anxiety.episodes_year).to eq 6
      expect(diseases.anxiety.ever_hospitalized).to eq true
      expect(diseases.anxiety.ever_suicide_attempt).to eq true
      expect(diseases.anxiety.inpatient_care).to eq true
      expect(diseases.anxiety.last_hospitalized_date).to eq Date.new(1999,11,11)
      expect(diseases.anxiety.last_suicide_attempt_date).to eq Date.new(1999,11,11)
      expect(diseases.anxiety.outpatient_care).to eq true
      expect(diseases.anxiety.work_absence).to eq true
    end

    it 'can set asthma' do
      attrs = [{"type_id":4,"condition_degree":"Moderate","episodes_year":7,"date":"11/11/1988","ever_treated":"1","currently_treated":"1","inhaled_bronchodilators":"1","inhaled_corticosteroids":"1","oral_medication_no_steroids":"1","rescue_inhaler":"1","oral_medication_steroids":"1","ever_hospitalized":"1","hospitalized_count_past_year":8,"last_hospitalized_date":"11/11/1998","work_absence":"1","fev1":5,"ever_life_threatening":"1","last_life_threatening_date":"11/11/2000","is_treatment_compliant":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.asthma?).to be true
      diseases = health.diseases
      expect(diseases.asthma.condition_degree).to eq 'Moderate'
      expect(diseases.asthma.currently_treating).to eq true
      expect(diseases.asthma.date).to eq Date.new(1988,11,11)
      expect(diseases.asthma.episodes_year).to eq 7
      expect(diseases.asthma.ever_hospitalized).to eq true
      expect(diseases.asthma.ever_life_threatening).to eq true
      expect(diseases.asthma.ever_treated).to eq true
      expect(diseases.asthma.fev1).to eq 5
      expect(diseases.asthma.hospitalized_count_past_year).to eq 8
      expect(diseases.asthma.inhaled_bronchodilators).to eq true
      expect(diseases.asthma.inhaled_corticosteroids).to eq true
      expect(diseases.asthma.is_treatment_compliant).to eq true
      expect(diseases.asthma.last_hospitalized_date).to eq Date.new(1998,11,11)
      expect(diseases.asthma.last_life_threatening_date).to eq Date.new(2000,11,11)
      expect(diseases.asthma.oral_medication_no_steroids).to eq true
      expect(diseases.asthma.oral_medication_steroids).to eq true
      expect(diseases.asthma.rescue_inhaler).to eq true
      expect(diseases.asthma.work_absence).to eq true
    end

    it 'can set atrial_fibrillation' do
      attrs = [{"type_id":5,"date":"11/11/1999","condition_degree":"Mild","fibrillation_type":"cvbn","last_episode_date":"11/11/1999","episode_length":7,"self_resolve":"1","ever_cardiac_eval":"1","cardiac_eval_date":"11/11/1999","cardiac_eval_result":"1","heart_disease":"1","shortness_of_breath":"1","current_medications":"1","ablation_procedure":"1","ablation_date":"11/11/1999","ablation_result":"1","is_controlled":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.atrial_fibrillation?).to be true
      diseases = health.diseases
      expect(diseases.atrial_fibrillation.ablation_date).to eq Date.new(1999,11,11)
      expect(diseases.atrial_fibrillation.ablation_procedure).to eq true
      expect(diseases.atrial_fibrillation.ablation_result).to eq true
      expect(diseases.atrial_fibrillation.cardiac_eval_date).to eq Date.new(1999,11,11)
      expect(diseases.atrial_fibrillation.cardiac_eval_result).to eq true
      expect(diseases.atrial_fibrillation.condition_degree).to eq 'Mild'
      expect(diseases.atrial_fibrillation.current_medications).to eq true
      expect(diseases.atrial_fibrillation.date).to eq Date.new(1999,11,11)
      expect(diseases.atrial_fibrillation.episode_length).to eq 7
      expect(diseases.atrial_fibrillation.ever_cardiac_eval).to eq true
      expect(diseases.atrial_fibrillation.fibrillation_type).to eq 'cvbn'
      expect(diseases.atrial_fibrillation.heart_disease).to eq true
      expect(diseases.atrial_fibrillation.is_controlled).to eq true
      expect(diseases.atrial_fibrillation.last_episode_date).to eq Date.new(1999,11,11)
      expect(diseases.atrial_fibrillation.self_resolve).to eq true
      expect(diseases.atrial_fibrillation.shortness_of_breath).to eq true
    end

    it 'can set bp' do
      attrs = {"systolic":130,"diastolic":82,"last_treatment":"04/05/2000","control_start":"2019/09/26",}
      health = Crm::HealthInfo.new(bp_details: attrs).tap(&:save!)
      expect(health.bp?).to eq true
      expect(health.bp_details.control_start).to eq Date.new(2019,9,26)
      expect(health.bp_details.diastolic).to eq 82
      expect(health.bp_details.last_treatment).to eq Date.new(2000,4,5)
      expect(health.bp_details.systolic).to eq 130
    end

    it 'can set breast_cancer' do
      attrs = [{"type_id":6,"date":"11/11/1999","cancer_stage":7,"metastatis":"1","tumorsize":4,"last_treatment_date":"11/11/1999","reoccurrence":"1","last_mammogram_date":"11/11/1999","currently_treating":"1","ductal_carcinoma":"1","diagnosed_dcis":"1","dcis_removed":"1","lobular_carcinoma":"1","diagnosed_lcis":"1","number_of_lesions":8,"lesion_size":7,"comedonecrosis":"1","lumpectomy":"1","mastectomy":"1","single_mastectomy":"1","double_mastectomy":"1","negative_sentinel_lymph_exam":"1","radiation_treatment":"1","endocrine_therapy":"1","endocrine_treated":"1","cancer_grade":4,}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.breast_cancer?).to eq true    
      diseases = health.diseases
      expect(diseases.breast_cancer.cancer_grade).to eq 4
      expect(diseases.breast_cancer.cancer_stage).to eq 7
      expect(diseases.breast_cancer.comedonecrosis).to eq true
      expect(diseases.breast_cancer.currently_treating).to eq true
      expect(diseases.breast_cancer.date).to eq Date.new(1999,11,11)
      expect(diseases.breast_cancer.dcis_removed).to eq true
      expect(diseases.breast_cancer.diagnosed_dcis).to eq true
      expect(diseases.breast_cancer.diagnosed_lcis).to eq true
      expect(diseases.breast_cancer.double_mastectomy).to eq true
      expect(diseases.breast_cancer.ductal_carcinoma).to eq true
      expect(diseases.breast_cancer.endocrine_therapy).to eq true
      expect(diseases.breast_cancer.endocrine_treated).to eq true
      expect(diseases.breast_cancer.last_mammogram_date).to eq Date.new(1999,11,11)
      expect(diseases.breast_cancer.last_treatment).to eq Date.new(1999,11,11)
      expect(diseases.breast_cancer.lesion_size).to eq 7
      expect(diseases.breast_cancer.lobular_carcinoma).to eq true
      expect(diseases.breast_cancer.lumpectomy).to eq true
      expect(diseases.breast_cancer.mastectomy).to eq true
      expect(diseases.breast_cancer.metastatis).to eq true
      expect(diseases.breast_cancer.negative_sentinel_lymph_exam).to eq true
      expect(diseases.breast_cancer.number_of_lesions).to eq 8
      expect(diseases.breast_cancer.radiation_treatment).to eq true
      expect(diseases.breast_cancer.reoccurrence).to eq true
      expect(diseases.breast_cancer.single_mastectomy).to eq true
      expect(diseases.breast_cancer.tumorsize).to eq 4
    end

    it 'can set cholesterol' do
      attrs = {"level":134,"hdl":3.7,"last_treatment":"02/06/1987",}
      health = Crm::HealthInfo.new(cholesterol_details: attrs).tap(&:save!)
      expect(health.cholesterol?).to be true
      expect(health.cholesterol_details.hdl).to eq 3.7
      expect(health.cholesterol_details.last_treatment).to eq Date.new(1987,2,6)
      expect(health.cholesterol_details.level).to eq 134
    end

    it 'can set copd' do
      attrs = [{"type_id":12,"date":"11/11/1998","condition_degree":"Mild","fev1":-26,"has_symptoms":"1","copd_severity":"Severe",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.copd?).to be true
      diseases = health.diseases
      expect(diseases.copd.condition_degree).to eq 'Mild'
      expect(diseases.copd.copd_severity).to eq 'Severe'
      expect(diseases.copd.date).to eq Date.new(1998,11,11)
      expect(diseases.copd.fev1).to eq -26
      expect(diseases.copd.has_symptoms).to eq true
    end

    it 'can set crimes' do
      attrs = [{"date":'07/04/1991',"detail":"wertyu",}]
      health = Crm::HealthInfo.new(crimes: attrs).tap(&:save!)
      expect(health.crimes?).to eq true
      expect(health.crimes.map &:detail).to match_array ["wertyu"]
    end

    it 'can set crohns' do
      attrs = [{"type_id":14,"date":"11/11/1999","condition_degree":"Mild","stabilization_date":"11/11/1999","last_attack_date":"11/11/1999","ever_steroid":"1","currently_steroids":"1","steroid_stop_date":"11/11/1999","ever_immuno_suppressants":"1","current_immuno_suppressants":"1","immunosuppressants_stop_date":"11/11/1999","limited_to_colon":"1","complications":"1","surgery":"1","surgery_date":"11/11/1999","weight_stable":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.crohns?).to be true
      diseases = health.diseases
      expect(diseases.crohns.complications).to eq true
      expect(diseases.crohns.condition_degree).to eq 'Mild'
      expect(diseases.crohns.current_immuno_suppressants).to eq true
      expect(diseases.crohns.currently_steroids).to eq true
      expect(diseases.crohns.date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.ever_immuno_suppressants).to eq true
      expect(diseases.crohns.ever_steroid).to eq true
      expect(diseases.crohns.immunosuppressants_stop_date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.last_attack_date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.limited_to_colon).to eq true
      expect(diseases.crohns.stabilization_date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.steroid_stop_date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.surgery).to eq true
      expect(diseases.crohns.surgery_date).to eq Date.new(1999,11,11)
      expect(diseases.crohns.weight_stable).to eq true
    end

    it 'can set depression' do
      attrs = [{"type_id":15,"date":"11/11/1999","condition_degree":"Mild","ever_hospitalized":"1","last_hospitalized":"11/11/1999","currently_treated":"1","in_psychotherapy":"1","responding_well":"1","work_absence":"1","ever_suicide_attempt":"1","last_suicide_attempt_date":"11/11/1999",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.depression?).to be true
      diseases = health.diseases
      expect(diseases.depression.condition_degree).to eq 'Mild'
      expect(diseases.depression.currently_treating).to eq true
      expect(diseases.depression.date).to eq Date.new(1999,11,11)
      expect(diseases.depression.ever_hospitalized).to eq true
      expect(diseases.depression.ever_suicide_attempt).to eq true
      expect(diseases.depression.in_psychotherapy).to eq true
      expect(diseases.depression.last_hospitalized).to eq Date.new(1999,11,11)
      expect(diseases.depression.last_suicide_attempt_date).to eq Date.new(1999,11,11)
      expect(diseases.depression.responding_well).to eq true
      expect(diseases.depression.work_absence).to eq true
    end

    it 'can set diabetes_1' do
      attrs = [{"type_id":16,"date":"11/11/1988","last_a1_c":"5","complications":"","currently_treated":"1","insulin_units":5,"average_a1_c":"7.8",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.diabetes_1?).to be true
      diseases = health.diseases
      expect(diseases.diabetes_1.average_a1_c).to eq 7.8
      expect(diseases.diabetes_1.complications).to be_nil
      expect(diseases.diabetes_1.currently_treating).to eq true
      expect(diseases.diabetes_1.date).to eq Date.new(1988,11,11)
      expect(diseases.diabetes_1.insulin_units).to eq 5
      expect(diseases.diabetes_1.last_a1_c).to eq 5
    end

    it 'can set diabetes_2' do
      attrs = [{"type_id":17,"date":"11/11/1998","last_a1_c":"7.8","average_a1_c":"9","complications":"","currently_treated":"","last_treatment_date":"11/11/1998","insulin_units":67,}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.diabetes_2?).to be true
      diseases = health.diseases
      expect(diseases.diabetes_2.average_a1_c).to eq 9
      expect(diseases.diabetes_2.complications).to eq nil
      expect(diseases.diabetes_2.currently_treating).to eq nil
      expect(diseases.diabetes_2.date).to eq Date.new(1998,11,11)
      expect(diseases.diabetes_2.insulin_units).to eq 67
      expect(diseases.diabetes_2.last_a1_c).to eq 7.8
      expect(diseases.diabetes_2.last_treatment).to eq Date.new(1998,11,11)
    end

    it 'can set drug_abuse' do
      attrs = [{"type_id":18,"date":"11/11/1999","currently_using":"1","last_used_date":"11/11/1999","ever_treated":"1","currently_treated":"1","treatment_end_date":"11/11/1999","ever_relapse":"1","ever_convicted":"1","last_relapse_date":"11/11/1999",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.drug_abuse?).to be true
      diseases = health.diseases
      expect(diseases.drug_abuse.currently_treating).to eq true
      expect(diseases.drug_abuse.currently_using).to eq true
      expect(diseases.drug_abuse.date).to eq Date.new(1999,11,11)
      expect(diseases.drug_abuse.ever_convicted).to eq true
      expect(diseases.drug_abuse.ever_relapse).to eq true
      expect(diseases.drug_abuse.ever_treated).to eq true
      expect(diseases.drug_abuse.last_relapse_date).to eq Date.new(1999,11,11)
      expect(diseases.drug_abuse.last_used_date).to eq Date.new(1999,11,11)
      expect(diseases.drug_abuse.last_treatment).to eq Date.new(1999,11,11)
    end

    it 'can set epilepsy' do
      attrs = [{"type_id":19,"date":"11/11/1999","condition_degree":"Mild","ever_treated":"1","ever_surgery":"1","surgery_date":"11/11/1999","seizure_type":"poiu","controlled_seizures":"1","neurological_evaluation":"1","neurological_normal":"1","caused_by_other":"1","last_seizure_date":"11/19/1999","seizures_per_year":99,"thirty_minute_plus":2,"ever_medication":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.epilepsy?).to be true
      diseases = health.diseases
      expect(diseases.epilepsy.caused_by_other).to eq true
      expect(diseases.epilepsy.condition_degree).to eq 'Mild'
      expect(diseases.epilepsy.controlled_seizures).to eq true
      expect(diseases.epilepsy.date).to eq Date.new(1999,11,11)
      expect(diseases.epilepsy.ever_medication).to eq true
      expect(diseases.epilepsy.ever_surgery).to eq true
      expect(diseases.epilepsy.ever_treated).to eq true
      expect(diseases.epilepsy.last_seizure_date).to eq Date.new(1999,11,19)
      expect(diseases.epilepsy.neurological_evaluation).to eq true
      expect(diseases.epilepsy.neurological_normal).to eq true
      expect(diseases.epilepsy.seizure_type).to eq 'poiu'
      expect(diseases.epilepsy.seizures_per_year).to eq 99
      expect(diseases.epilepsy.surgery_date).to eq Date.new(1999,11,11)
      expect(diseases.epilepsy.thirty_minute_plus).to eq 2
    end

    it 'can set foreign_travel' do
      attrs = {"country_id":1,"when":"11/11/1999","duration":"8 yrs",}
      health = Crm::HealthInfo.new(foreign_travel_details: attrs).tap(&:save!)
      expect(health.foreign_travel?).to eq true
      expect(health.foreign_travel_details.country_id).to eq 1
      expect(health.foreign_travel_details.when).to eq '11/11/1999'
      expect(health.foreign_travel_details.duration).to eq '8 yrs'
    end

    it 'can set hazardous_avocation' do
      attrs = {"further_detail":"fooooooooooooooooo",}
      health = Crm::HealthInfo.new(hazardous_avocation_details: attrs).tap(&:save!)
      expect(health.hazardous_avocation?).to be true
      expect(health.hazardous_avocation_details.detail).to eq "fooooooooooooooooo"
    end

    it 'can set heart_murmur' do
      attrs = [{"type_id":23,"date":"11/11/1999","condition_degree":"Mild","ever_echocardiogram":"1","valve_structures_normal":"1","valve_surgery":"1","heart_enlargement":"1","symptomatic":"1","progression":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.heart_murmur?).to be true
      diseases = health.diseases
      expect(diseases.heart_murmur.condition_degree).to eq 'Mild'
      expect(diseases.heart_murmur.date).to eq Date.new(1999,11,11)
      expect(diseases.heart_murmur.ever_echocardiogram).to eq true
      expect(diseases.heart_murmur.heart_enlargement).to eq true
      expect(diseases.heart_murmur.progression).to eq true
      expect(diseases.heart_murmur.symptomatic).to eq true
      expect(diseases.heart_murmur.valve_structures_normal).to eq true
      expect(diseases.heart_murmur.valve_surgery).to eq true
    end

    it 'can set hepatitis_c' do
      attrs = [{"type_id":24,"date":"11/11/1999","contraction_date":"11/11/1999","condition_degree":"Mild","normal_viral_loads":"1","normal_viral_date":"11/11/1999","normal_liver_functions":"1","normal_liver_date":"11/11/1999","ever_treated":"1","currently_treated":"1","treatment_start_date":"11/11/1999","treatment_end_date":"11/11/1999","liver_cirrhosis":"1","liver_biopsy":"1","complications":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.hepatitis_c?).to be true
      diseases = health.diseases
      expect(diseases.hepatitis_c.complications).to eq true
      expect(diseases.hepatitis_c.condition_degree).to eq 'Mild'
      expect(diseases.hepatitis_c.contraction_date).to eq Date.new(1999,11,11)
      expect(diseases.hepatitis_c.currently_treating).to eq true
      expect(diseases.hepatitis_c.date).to eq Date.new(1999,11,11)
      expect(diseases.hepatitis_c.ever_treated).to eq true
      expect(diseases.hepatitis_c.liver_biopsy).to eq true
      expect(diseases.hepatitis_c.liver_cirrhosis).to eq true
      expect(diseases.hepatitis_c.normal_liver_date).to eq Date.new(1999,11,11)
      expect(diseases.hepatitis_c.normal_liver_functions).to eq true
      expect(diseases.hepatitis_c.normal_viral_date).to eq Date.new(1999,11,11)
      expect(diseases.hepatitis_c.normal_viral_loads).to eq true
      expect(diseases.hepatitis_c.last_treatment).to eq Date.new(1999,11,11)
      expect(diseases.hepatitis_c.treatment_start_date).to eq Date.new(1999,11,11)
    end

    it 'can set irregular_heartbeat' do
      attrs = [{"type_id":26,"date":"11/11/1999","underlying_heart_disease":"1","current_symptoms":"1","current_medications":"1","atrioventricular_block":"1","second_degree_av_block":"1","third_degree_av_block_atr_disassociation":"1","born_with_third_degree_av_block":"1","pacemaker_implanted":"1","pacemaker_implant_date":"11/11/1999","attrioventricular_junctional_rhythm":"1","paroxysmal_super_tachycardia":"1","cardiac_evaluations":"1","cardiac_eval_result_normal":"1","last_experience_symptoms":"11/11/1999","symptoms_per_year":6,"premature_atrial_complexes":"1","history_of_cardiovascular_disease":"1","premature_ventricular_contraction":"1","simple_pvc":"1","complex_pvc":"1","require_treatment_for_pvc":"1","sick_sinus_syndrome":"1","pacemaker_for_sick_sinus_syndrome":"1","pacemaker_for_sick_sinus_syndrome_implant_date":"11/11/1999","history_of_fainting":"1","sinus_bradycardia":"1","sinus_bradycardia_caused_by_another_condition":"1","sinus_bradycardia_caused_by_medication":"1","sinus_bradycardia_cause_unknown":"1","wandering_pacemaker":"1","pulse_rate":9,"cardiac_eval_for_wandering_pacemaker":"1","idoventricular_rhythm":"1","mobitz_type_1_block":12,"mobitz_type_2_block":6,}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.irregular_heartbeat?).to be true
      diseases = health.diseases
      expect(diseases.irregular_heartbeat.atrioventricular_block).to eq true
      expect(diseases.irregular_heartbeat.attrioventricular_junctional_rhythm).to eq true
      expect(diseases.irregular_heartbeat.born_with_third_degree_av_block).to eq true
      expect(diseases.irregular_heartbeat.cardiac_eval_for_wandering_pacemaker).to eq true
      expect(diseases.irregular_heartbeat.cardiac_eval_result_normal).to eq true
      expect(diseases.irregular_heartbeat.cardiac_evaluations).to eq true
      expect(diseases.irregular_heartbeat.complex_pvc).to eq true
      expect(diseases.irregular_heartbeat.current_medications).to eq true
      expect(diseases.irregular_heartbeat.current_symptoms).to eq true
      expect(diseases.irregular_heartbeat.date).to eq Date.new(1999,11,11)
      expect(diseases.irregular_heartbeat.history_of_cardiovascular_disease).to eq true
      expect(diseases.irregular_heartbeat.history_of_fainting).to eq true
      expect(diseases.irregular_heartbeat.idoventricular_rhythm).to eq true
      expect(diseases.irregular_heartbeat.last_experience_symptoms).to eq Date.new(1999,11,11)
      expect(diseases.irregular_heartbeat.mobitz_type_1_block).to eq 12
      expect(diseases.irregular_heartbeat.mobitz_type_2_block).to eq 6
      expect(diseases.irregular_heartbeat.pacemaker_for_sick_sinus_syndrome).to eq true
      expect(diseases.irregular_heartbeat.pacemaker_for_sick_sinus_syndrome_implant_date).to eq Date.new(1999,11,11)
      expect(diseases.irregular_heartbeat.pacemaker_implant_date).to eq Date.new(1999,11,11)
      expect(diseases.irregular_heartbeat.pacemaker_implanted).to eq true
      expect(diseases.irregular_heartbeat.paroxysmal_super_tachycardia).to eq true
      expect(diseases.irregular_heartbeat.premature_atrial_complexes).to eq true
      expect(diseases.irregular_heartbeat.premature_ventricular_contraction).to eq true
      expect(diseases.irregular_heartbeat.pulse_rate).to eq 9
      expect(diseases.irregular_heartbeat.require_treatment_for_pvc).to eq true
      expect(diseases.irregular_heartbeat.second_degree_av_block).to eq true
      expect(diseases.irregular_heartbeat.sick_sinus_syndrome).to eq true
      expect(diseases.irregular_heartbeat.simple_pvc).to eq true
      expect(diseases.irregular_heartbeat.sinus_bradycardia).to eq true
      expect(diseases.irregular_heartbeat.sinus_bradycardia_cause_unknown).to eq true
      expect(diseases.irregular_heartbeat.sinus_bradycardia_caused_by_another_condition).to eq true
      expect(diseases.irregular_heartbeat.sinus_bradycardia_caused_by_medication).to eq true
      expect(diseases.irregular_heartbeat.symptoms_per_year).to eq 6
      expect(diseases.irregular_heartbeat.third_degree_av_block_atr_disassociation).to eq true
      expect(diseases.irregular_heartbeat.underlying_heart_disease).to eq true
      expect(diseases.irregular_heartbeat.wandering_pacemaker).to eq true
    end

    it 'can set moving_violations' do
      expect(Crm::HealthInfo.new.moving_violations?).to be false
      attrs = [{"date":'10/14/1922',"dl_suspension":true,"dui_dwi":true,"reckless_driving":true,"accident":true,"dl_suspension_end":'03/13/1955',"detail":"he won't do it again"},{"date":'10/14/1925'}]
      health = Crm::HealthInfo.new(moving_violations: attrs).tap(&:save!)
      expect(health.moving_violations?).to be true
      expect(health.moving_violations.length).to eq 2
      expect(health.moving_violations[0].date).to eq Date.new(1922,10,14)
      expect(health.moving_violations[0].dl_suspension).to be true
      expect(health.moving_violations[0].dui_dwi).to be true
      expect(health.moving_violations[0].reckless_driving).to be true
      expect(health.moving_violations[0].accident).to be true
      expect(health.moving_violations[0].dl_suspension_end).to eq Date.new(1955,3,13)
      expect(health.moving_violations[0].detail).to eq "he won't do it again"
      expect(health.moving_violations[1].date).to eq Date.new(1925,10,14)
      expect(health.moving_violations[1].dl_suspension).to be_nil
      expect(health.moving_violations[1].dui_dwi).to be_nil
      expect(health.moving_violations[1].reckless_driving).to be_nil
      expect(health.moving_violations[1].accident).to be_nil
      expect(health.moving_violations[1].dl_suspension_end).to be_nil
    end


    it 'can set multiple_sclerosis' do
      attrs = [{"type_id":30,"date":"11/11/1999","condition_degree":"Mild","attacks_per_year":3,"last_attack_date":"11/11/1999","condition_type":"lkjh",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.multiple_sclerosis?).to be true
      diseases = health.diseases
      expect(diseases.multiple_sclerosis.attacks_per_year).to eq 3
      expect(diseases.multiple_sclerosis.condition_degree).to eq 'Mild'
      expect(diseases.multiple_sclerosis.condition_type).to eq 'lkjh'
      expect(diseases.multiple_sclerosis.date).to eq Date.new(1999,11,11)
      expect(diseases.multiple_sclerosis.last_attack_date).to eq Date.new(1999,11,11)
    end

    it 'can set parkinsons' do
      attrs = [{"type_id":35,"date":"11/11/1999","age_at_diagnosis":8,"condition_degree":"Mild","live_independently":"1","condition_stable":"1","currently_disabled":"1","disabled_severity":"dfg","currently_receive_treatment":"1","rigidity":"1","rigidity_severity":"wert","stable_date":"11/11/1999","walking_impairment":"1","walking_impairment_severity":"ijh","mental_deterioration":"1","affect_fingers_only":"1","affect_hands_only":"1","affect_multiple_areas":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.parkinsons?).to be true
      diseases = health.diseases
      expect(diseases.parkinsons.affect_fingers_only).to eq true
      expect(diseases.parkinsons.affect_hands_only).to eq true
      expect(diseases.parkinsons.affect_multiple_areas).to eq true
      expect(diseases.parkinsons.age_at_diagnosis).to eq 8
      expect(diseases.parkinsons.condition_degree).to eq 'Mild'
      expect(diseases.parkinsons.condition_stable).to eq true
      expect(diseases.parkinsons.currently_disabled).to eq true
      expect(diseases.parkinsons.currently_receive_treatment).to eq true
      expect(diseases.parkinsons.date).to eq Date.new(1999,11,11)
      expect(diseases.parkinsons.disabled_severity).to eq 'dfg'
      expect(diseases.parkinsons.live_independently).to eq true
      expect(diseases.parkinsons.mental_deterioration).to eq true
      expect(diseases.parkinsons.rigidity).to eq true
      expect(diseases.parkinsons.rigidity_severity).to eq 'wert'
      expect(diseases.parkinsons.stable_date).to eq Date.new(1999,11,11)
      expect(diseases.parkinsons.walking_impairment).to eq true
      expect(diseases.parkinsons.walking_impairment_severity).to eq 'ijh'
    end

    it 'can set relatives_diseases' do
      attrs = [{"type_id":1,"relationship_type_id":1,"onset_age":4,"death_age":5,},{"type_id":2,"relationship_type_id":4,"date":'05/02/1922'}]
      health = Crm::HealthInfo.new(relatives_diseases: attrs).tap(&:save!)
      relatives = health.relatives_diseases
      expect(relatives.length).to eq(2)
      expect(relatives[0].relationship_type_id).to eq 1
      expect(relatives[1].relationship_type_id).to eq 4
      expect(relatives[0].onset_age).to eq 4
      expect(relatives[1].onset_age).to be_nil
      expect(relatives[0].death_age).to eq 5
      expect(relatives[1].death_age).to be_nil
      expect(relatives[0].date).to be_nil
      expect(relatives[1].date).to eq Date.new(1922,5,2)
    end

    it 'can set sleep_apnea' do
      attrs = [{"type_id":37,"date":"11/11/1978","condition_degree":"Mild","ever_treated":"1","treatment_start_date":"11/11/1999","use_cpap":"1","rd_index":7,"apnea_index":6,"ah_index":5,"o2_saturation":4,"cpap_machine_complaint":"1","sleep_study":"1","sleep_study_followup":"1","on_oxygen":"1","currently_sleep_apnea":"1",}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.sleep_apnea?).to be true
      diseases = health.diseases
      expect(diseases.sleep_apnea.ah_index).to eq 5
      expect(diseases.sleep_apnea.apnea_index).to eq 6
      expect(diseases.sleep_apnea.condition_degree).to eq 'Mild'
      expect(diseases.sleep_apnea.cpap_machine_complaint).to eq true
      expect(diseases.sleep_apnea.currently_sleep_apnea).to eq true
      expect(diseases.sleep_apnea.date).to eq Date.new(1978,11,11)
      expect(diseases.sleep_apnea.ever_treated).to eq true
      expect(diseases.sleep_apnea.o2_saturation).to eq 4
      expect(diseases.sleep_apnea.on_oxygen).to eq true
      expect(diseases.sleep_apnea.rd_index).to eq 7
      expect(diseases.sleep_apnea.sleep_study).to eq true
      expect(diseases.sleep_apnea.sleep_study_followup).to eq true
      expect(diseases.sleep_apnea.treatment_start_date).to eq Date.new(1999,11,11)
      expect(diseases.sleep_apnea.use_cpap).to eq true
    end

    it 'can set stroke' do
      attrs = [{"type_id":38,"condition_degree":"Mild","last_stroke_date":"11/11/1999","multiple_strokes":"1","first_stroke_age":8,}]
      health = Crm::HealthInfo.new(diseases: attrs).tap(&:save!)
      expect(health.stroke?).to be true
      diseases = health.diseases
      expect(diseases.stroke.condition_degree).to eq 'Mild'
      expect(diseases.stroke.onset_age).to eq 8
      expect(diseases.stroke.last_stroke_date).to eq Date.new(1999,11,11)
      expect(diseases.stroke.multiple_strokes).to eq true
    end

    it 'can set tobacco' do
      attrs = {"cigarettes_per_day":5,"tobacco":true,"cigarettes_current":"1","cigars_current":"1","cigars_per_month":6,"pipe_current":false,"pipes_per_year":3,"pipe_last":"03/04/1967",}
      health = Crm::HealthInfo.new(tobacco_details: attrs).tap(&:save!)
      expect(health.tobacco?).to eq true
      expect(health.tobacco_details.cigarettes_current).to eq true
      expect(health.tobacco_details.cigarettes_per_day).to eq 5
      expect(health.tobacco_details.cigars_current).to eq true
      expect(health.tobacco_details.cigars_per_month).to eq 6
      expect(health.tobacco_details.pipe_current).to eq false
      expect(health.tobacco_details.pipe_last).to eq Date.new(1967,3,4)
      expect(health.tobacco_details.pipes_per_year).to eq 3
    end

    context 'with legacy attributes' do
      it 'can set alcohol_abuse' do
        attrs = {"alcohol_abuse":true,"alcohol_abuse_diagnosed_date":"11/11/1999","alcohol_abuse_currently_consuming":"1","alcohol_abuse_last_consumed_date":"11/11/1999","alcohol_abuse_ever_treated":"1","alcohol_abuse_currently_treated":"1","alcohol_abuse_treatment_end_date":"11/11/1999","alcohol_abuse_ever_relapse":"1","alcohol_abuse_last_relapse_date":"11/11/1999"}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.alcohol_abuse?).to eq true
        diseases = health.diseases
        expect(diseases.alcohol_abuse.currently_consuming).to eq true
        expect(diseases.alcohol_abuse.currently_treating).to eq true
        expect(diseases.alcohol_abuse.date).to eq Date.new(1999,11,11)
        expect(diseases.alcohol_abuse.ever_relapse).to eq true
        expect(diseases.alcohol_abuse.ever_treated).to eq true
        expect(diseases.alcohol_abuse.last_consumed_date).to eq Date.new(1999,11,11)
        expect(diseases.alcohol_abuse.last_relapse_date).to eq Date.new(1999,11,11)
        expect(diseases.alcohol_abuse.last_treatment).to eq Date.new(1999,11,11)
      end

      it 'can set anxiety' do
        attrs = {"anxiety":true,"anxiety_diagnosed_date":"11/11/1999","anxiety_condition_degree":"Mild","anxiety_ever_hospitalized":"1","anxiety_last_hospitalized_date":"11/11/1999","anxiety_episodes_year":6,"anxiety_currently_treated":"1","anxiety_outpatient_care":"1","anxiety_inpatient_care":"1","anxiety_work_absence":"1","anxiety_ever_suicide_attempt":"1","anxiety_last_suicide_attempt_date":"11/11/1999"}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.anxiety?).to eq true
        diseases = health.diseases
        expect(diseases.anxiety.condition_degree).to eq 'Mild'
        expect(diseases.anxiety.currently_treating).to eq true
        expect(diseases.anxiety.date).to eq Date.new(1999,11,11)
        expect(diseases.anxiety.episodes_year).to eq 6
        expect(diseases.anxiety.ever_hospitalized).to eq true
        expect(diseases.anxiety.ever_suicide_attempt).to eq true
        expect(diseases.anxiety.inpatient_care).to eq true
        expect(diseases.anxiety.last_hospitalized_date).to eq Date.new(1999,11,11)
        expect(diseases.anxiety.last_suicide_attempt_date).to eq Date.new(1999,11,11)
        expect(diseases.anxiety.outpatient_care).to eq true
        expect(diseases.anxiety.work_absence).to eq true
      end

      it 'can set asthma' do
        attrs = {"asthma":true,"asthma_condition_degree":"Moderate","asthma_episodes_year":7,"asthma_diagnosed_date":"11/11/1988","asthma_ever_treated":"1","asthma_currently_treated":"1","asthma_inhaled_bronchodilators":"1","asthma_inhaled_corticosteroids":"1","asthma_oral_medication_no_steroids":"1","asthma_rescue_inhaler":"1","asthma_oral_medication_steroids":"1","asthma_ever_hospitalized":"1","asthma_hospitalized_count_past_year":8,"asthma_last_hospitalized_date":"11/11/1998","asthma_work_absence":"1","asthma_fev1":5,"asthma_ever_life_threatening":"1","asthma_last_life_threatening_date":"11/11/2000","asthma_is_treatment_compliant":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.asthma?).to be true
        diseases = health.diseases
        expect(diseases.asthma.condition_degree).to eq 'Moderate'
        expect(diseases.asthma.currently_treating).to eq true
        expect(diseases.asthma.date).to eq Date.new(1988,11,11)
        expect(diseases.asthma.episodes_year).to eq 7
        expect(diseases.asthma.ever_hospitalized).to eq true
        expect(diseases.asthma.ever_life_threatening).to eq true
        expect(diseases.asthma.ever_treated).to eq true
        expect(diseases.asthma.fev1).to eq 5
        expect(diseases.asthma.hospitalized_count_past_year).to eq 8
        expect(diseases.asthma.inhaled_bronchodilators).to eq true
        expect(diseases.asthma.inhaled_corticosteroids).to eq true
        expect(diseases.asthma.is_treatment_compliant).to eq true
        expect(diseases.asthma.last_hospitalized_date).to eq Date.new(1998,11,11)
        expect(diseases.asthma.last_life_threatening_date).to eq Date.new(2000,11,11)
        expect(diseases.asthma.oral_medication_no_steroids).to eq true
        expect(diseases.asthma.oral_medication_steroids).to eq true
        expect(diseases.asthma.rescue_inhaler).to eq true
        expect(diseases.asthma.work_absence).to eq true
      end

      it 'can set atrial_fibrillation' do
        attrs = {"atrial_fibrillation":true,"atrial_fibrillation_diagnosed_date":"11/11/1999","atrial_fibrillation_condition_degree":"Mild","atrial_fibrillation_fibrillation_type":"cvbn","atrial_fibrillation_last_episode_date":"11/11/1999","atrial_fibrillation_episode_length":7,"atrial_fibrillation_self_resolve":"1","atrial_fibrillation_ever_cardiac_eval":"1","atrial_fibrillation_cardiac_eval_date":"11/11/1999","atrial_fibrillation_cardiac_eval_result":"1","atrial_fibrillation_heart_disease":"1","atrial_fibrillation_shortness_of_breath":"1","atrial_fibrillation_current_medications":"1","atrial_fibrillation_ablation_procedure":"1","atrial_fibrillation_ablation_date":"11/11/1999","atrial_fibrillation_ablation_result":"1","atrial_fibrillation_is_controlled":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.atrial_fibrillation?).to be true
        diseases = health.diseases
        expect(diseases.atrial_fibrillation.ablation_date).to eq Date.new(1999,11,11)
        expect(diseases.atrial_fibrillation.ablation_procedure).to eq true
        expect(diseases.atrial_fibrillation.ablation_result).to eq true
        expect(diseases.atrial_fibrillation.cardiac_eval_date).to eq Date.new(1999,11,11)
        expect(diseases.atrial_fibrillation.cardiac_eval_result).to eq true
        expect(diseases.atrial_fibrillation.condition_degree).to eq 'Mild'
        expect(diseases.atrial_fibrillation.current_medications).to eq true
        expect(diseases.atrial_fibrillation.date).to eq Date.new(1999,11,11)
        expect(diseases.atrial_fibrillation.episode_length).to eq 7
        expect(diseases.atrial_fibrillation.ever_cardiac_eval).to eq true
        expect(diseases.atrial_fibrillation.fibrillation_type).to eq 'cvbn'
        expect(diseases.atrial_fibrillation.heart_disease).to eq true
        expect(diseases.atrial_fibrillation.is_controlled).to eq true
        expect(diseases.atrial_fibrillation.last_episode_date).to eq Date.new(1999,11,11)
        expect(diseases.atrial_fibrillation.self_resolve).to eq true
        expect(diseases.atrial_fibrillation.shortness_of_breath).to eq true
      end

      it 'can set bp' do
        attrs = {"bp":true,"bp_systolic":130,"bp_diastolic":82,"bp_last_treatment":"04/05/2000","bp_control_start":"2019/09/26",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.bp?).to eq true
        expect(health.bp_details.control_start).to eq Date.new(2019,9,26)
        expect(health.bp_details.diastolic).to eq 82
        expect(health.bp_details.last_treatment).to eq Date.new(2000,4,5)
        expect(health.bp_details.systolic).to eq 130
      end

      it 'can set cancer_breast' do
        attrs = {"cancer_breast":true,"cancer_breast_diagnosed_date":"11/11/1999","cancer_breast_cancer_stage":7,"cancer_breast_metastatis":"1","cancer_breast_tumorsize":4,"cancer_breast_last_treatment_date":"11/11/1999","cancer_breast_reoccurrence":"1","cancer_breast_last_mammogram_date":"11/11/1999","cancer_breast_currently_treating":"1","cancer_breast_ductal_carcinoma":"1","cancer_breast_diagnosed_dcis":"1","cancer_breast_dcis_removed":"1","cancer_breast_lobular_carcinoma":"1","cancer_breast_diagnosed_lcis":"1","cancer_breast_number_of_lesions":8,"cancer_breast_lesion_size":7,"cancer_breast_comedonecrosis":"1","cancer_breast_lumpectomy":"1","cancer_breast_mastectomy":"1","cancer_breast_single_mastectomy":"1","cancer_breast_double_mastectomy":"1","cancer_breast_negative_sentinel_lymph_exam":"1","cancer_breast_radiation_treatment":"1","cancer_breast_endocrine_therapy":"1","cancer_breast_endocrine_treated":"1","cancer_breast_cancer_grade":4,}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.breast_cancer?).to eq true    
        expect(health.cancer_breast?).to be true
        diseases = health.diseases
        expect(diseases.cancer_breast.cancer_grade).to eq 4
        expect(diseases.cancer_breast.cancer_stage).to eq 7
        expect(diseases.cancer_breast.comedonecrosis).to eq true
        expect(diseases.cancer_breast.currently_treating).to eq true
        expect(diseases.cancer_breast.date).to eq Date.new(1999,11,11)
        expect(diseases.cancer_breast.dcis_removed).to eq true
        expect(diseases.cancer_breast.diagnosed_dcis).to eq true
        expect(diseases.cancer_breast.diagnosed_lcis).to eq true
        expect(diseases.cancer_breast.double_mastectomy).to eq true
        expect(diseases.cancer_breast.ductal_carcinoma).to eq true
        expect(diseases.cancer_breast.endocrine_therapy).to eq true
        expect(diseases.cancer_breast.endocrine_treated).to eq true
        expect(diseases.cancer_breast.last_mammogram_date).to eq Date.new(1999,11,11)
        expect(diseases.cancer_breast.last_treatment).to eq Date.new(1999,11,11)
        expect(diseases.cancer_breast.lesion_size).to eq 7
        expect(diseases.cancer_breast.lobular_carcinoma).to eq true
        expect(diseases.cancer_breast.lumpectomy).to eq true
        expect(diseases.cancer_breast.mastectomy).to eq true
        expect(diseases.cancer_breast.metastatis).to eq true
        expect(diseases.cancer_breast.negative_sentinel_lymph_exam).to eq true
        expect(diseases.cancer_breast.number_of_lesions).to eq 8
        expect(diseases.cancer_breast.radiation_treatment).to eq true
        expect(diseases.cancer_breast.reoccurrence).to eq true
        expect(diseases.cancer_breast.single_mastectomy).to eq true
        expect(diseases.cancer_breast.tumorsize).to eq 4
      end

      it 'can set cholesterol' do
        attrs = {"cholesterol":true,"cholesterol_level":134,"cholesterol_hdl":3.7,"cholesterol_last_treatment":"02/06/1987",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.cholesterol?).to be true
        expect(health.cholesterol_details.hdl).to eq 3.7
        expect(health.cholesterol_details.last_treatment).to eq Date.new(1987,2,6)
        expect(health.cholesterol_details.level).to eq 134
      end

      it 'can set copd' do
        attrs = {"copd":true,"copd_diagnosed_date":"11/11/1998","copd_condition_degree":"Mild","copd_fev1":-26,"copd_has_symptoms":"1","copd_copd_severity":"Severe",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.copd?).to be true
        diseases = health.diseases
        expect(diseases.copd.condition_degree).to eq 'Mild'
        expect(diseases.copd.copd_severity).to eq 'Severe'
        expect(diseases.copd.date).to eq Date.new(1998,11,11)
        expect(diseases.copd.fev1).to eq -26
        expect(diseases.copd.has_symptoms).to eq true
      end

      it 'can set criminal' do
        attrs = {"criminal":true,"criminal_further_detail":"wertyu",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.crimes?).to eq true
        expect(health.crimes.map &:detail).to match_array ["wertyu"]
      end

      it 'can set crohns' do
        attrs = {"crohns":true,"crohns_diagnosed_date":"11/11/1999","crohns_condition_degree":"Mild","crohns_stabilization_date":"11/11/1999","crohns_last_attack_date":"11/11/1999","crohns_ever_steroid":"1","crohns_currently_steroids":"1","crohns_steroid_stop_date":"11/11/1999","crohns_ever_immuno_suppressants":"1","crohns_current_immuno_suppressants":"1","crohns_immunosuppressants_stop_date":"11/11/1999","crohns_limited_to_colon":"1","crohns_complications":"1","crohns_surgery":"1","crohns_surgery_date":"11/11/1999","crohns_weight_stable":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.crohns?).to be true
        diseases = health.diseases
        expect(diseases.crohns.complications).to eq true
        expect(diseases.crohns.condition_degree).to eq 'Mild'
        expect(diseases.crohns.current_immuno_suppressants).to eq true
        expect(diseases.crohns.currently_steroids).to eq true
        expect(diseases.crohns.date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.ever_immuno_suppressants).to eq true
        expect(diseases.crohns.ever_steroid).to eq true
        expect(diseases.crohns.immunosuppressants_stop_date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.last_attack_date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.limited_to_colon).to eq true
        expect(diseases.crohns.stabilization_date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.steroid_stop_date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.surgery).to eq true
        expect(diseases.crohns.surgery_date).to eq Date.new(1999,11,11)
        expect(diseases.crohns.weight_stable).to eq true
      end

      it 'can set depression' do
        attrs = {"depression":true,"depression_diagnosed_date":"11/11/1999","depression_condition_degree":"Mild","depression_ever_hospitalized":"1","depression_last_hospitalized":"11/11/1999","depression_currently_treated":"1","depression_in_psychotherapy":"1","depression_responding_well":"1","depression_work_absence":"1","depression_ever_suicide_attempt":"1","depression_last_suicide_attempt_date":"11/11/1999",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.depression?).to be true
        diseases = health.diseases
        expect(diseases.depression.condition_degree).to eq 'Mild'
        expect(diseases.depression.currently_treating).to eq true
        expect(diseases.depression.date).to eq Date.new(1999,11,11)
        expect(diseases.depression.ever_hospitalized).to eq true
        expect(diseases.depression.ever_suicide_attempt).to eq true
        expect(diseases.depression.in_psychotherapy).to eq true
        expect(diseases.depression.last_hospitalized).to eq Date.new(1999,11,11)
        expect(diseases.depression.last_suicide_attempt_date).to eq Date.new(1999,11,11)
        expect(diseases.depression.responding_well).to eq true
        expect(diseases.depression.work_absence).to eq true
      end

      it 'can set diabetes_1' do
        attrs = {"diabetes_1":true,"diabetes_1_diagnosed_date":"11/11/1988","diabetes_1_last_a1_c":"5","diabetes_1_complications":"","diabetes_1_currently_treated":"1","diabetes_1_insulin_units":5,"diabetes_1_average_a1_c":"7.8",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.diabetes_1?).to be true
        diseases = health.diseases
        expect(diseases.diabetes_1.average_a1_c).to eq 7.8
        expect(diseases.diabetes_1.complications).to be_nil
        expect(diseases.diabetes_1.currently_treating).to eq true
        expect(diseases.diabetes_1.date).to eq Date.new(1988,11,11)
        expect(diseases.diabetes_1.insulin_units).to eq 5
        expect(diseases.diabetes_1.last_a1_c).to eq 5
      end

      it 'can set diabetes_2' do
        attrs = {"diabetes_2":true,"diabetes_2_diagnosed_date":"11/11/1998","diabetes_2_last_a1_c":"7.8","diabetes_2_average_a1_c":"9","diabetes_2_complications":"","diabetes_2_currently_treated":"","diabetes_2_last_treatment_date":"11/11/1998","diabetes_2_insulin_units":67,}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.diabetes_2?).to be true
        diseases = health.diseases
        expect(diseases.diabetes_2.average_a1_c).to eq 9
        expect(diseases.diabetes_2.complications).to eq nil
        expect(diseases.diabetes_2.currently_treating).to eq nil
        expect(diseases.diabetes_2.date).to eq Date.new(1998,11,11)
        expect(diseases.diabetes_2.insulin_units).to eq 67
        expect(diseases.diabetes_2.last_a1_c).to eq 7.8
        expect(diseases.diabetes_2.last_treatment).to eq Date.new(1998,11,11)
      end

      it 'can set drug_abuse' do
        attrs = {"drug_abuse":true,"drug_abuse_diagnosed_date":"11/11/1999","drug_abuse_currently_using":"1","drug_abuse_last_used_date":"11/11/1999","drug_abuse_ever_treated":"1","drug_abuse_currently_treated":"1","drug_abuse_treatment_end_date":"11/11/1999","drug_abuse_ever_relapse":"1","drug_abuse_ever_convicted":"1","drug_abuse_last_relapse_date":"11/11/1999",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.drug_abuse?).to be true
        diseases = health.diseases
        expect(diseases.drug_abuse.currently_treating).to eq true
        expect(diseases.drug_abuse.currently_using).to eq true
        expect(diseases.drug_abuse.date).to eq Date.new(1999,11,11)
        expect(diseases.drug_abuse.ever_convicted).to eq true
        expect(diseases.drug_abuse.ever_relapse).to eq true
        expect(diseases.drug_abuse.ever_treated).to eq true
        expect(diseases.drug_abuse.last_relapse_date).to eq Date.new(1999,11,11)
        expect(diseases.drug_abuse.last_used_date).to eq Date.new(1999,11,11)
        expect(diseases.drug_abuse.last_treatment).to eq Date.new(1999,11,11)
      end

      it 'can set epilepsy' do
        attrs = {"epilepsy":true,"epilepsy_diagnosed_date":"11/11/1999","epilepsy_condition_degree":"Mild","epilepsy_ever_treated":"1","epilepsy_ever_surgery":"1","epilepsy_surgery_date":"11/11/1999","epilepsy_seizure_type":"poiu","epilepsy_controlled_seizures":"1","epilepsy_neurological_evaluation":"1","epilepsy_neurological_normal":"1","epilepsy_caused_by_other":"1","epilepsy_last_seizure_date":"11/19/1999","epilepsy_seizures_per_year":99,"epilepsy_thirty_minute_plus":2,"epilepsy_ever_medication":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.epilepsy?).to be true
        diseases = health.diseases
        expect(diseases.epilepsy.caused_by_other).to eq true
        expect(diseases.epilepsy.condition_degree).to eq 'Mild'
        expect(diseases.epilepsy.controlled_seizures).to eq true
        expect(diseases.epilepsy.date).to eq Date.new(1999,11,11)
        expect(diseases.epilepsy.ever_medication).to eq true
        expect(diseases.epilepsy.ever_surgery).to eq true
        expect(diseases.epilepsy.ever_treated).to eq true
        expect(diseases.epilepsy.last_seizure_date).to eq Date.new(1999,11,19)
        expect(diseases.epilepsy.neurological_evaluation).to eq true
        expect(diseases.epilepsy.neurological_normal).to eq true
        expect(diseases.epilepsy.seizure_type).to eq 'poiu'
        expect(diseases.epilepsy.seizures_per_year).to eq 99
        expect(diseases.epilepsy.surgery_date).to eq Date.new(1999,11,11)
        expect(diseases.epilepsy.thirty_minute_plus).to eq 2
      end

      it 'can set foreign_travel' do
        attrs = {"foreign_travel":true,"travel_country_id":1,"travel_when":"11/11/1999","travel_duration":"8 yrs",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.foreign_travel?).to eq true
        expect(health.foreign_travel_details.country_id).to eq 1
        expect(health.foreign_travel_details.when).to eq '11/11/1999'
        expect(health.foreign_travel_details.duration).to eq '8 yrs'
      end

      it 'can set hazardous_avocation' do
        attrs = {"hazardous_avocation":true,"hazardous_avocation_further_detail":"fooooooooooooooooo",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.hazardous_avocation?).to be true
        expect(health.hazardous_avocation_details.detail).to eq "fooooooooooooooooo"
      end

      it 'can set heart_murmur' do
        attrs = {"heart_murmur":true,"heart_murmur_diagnosed_date":"11/11/1999","heart_murmur_condition_degree":"Mild","heart_murmur_ever_echocardiogram":"1","heart_murmur_valve_structures_normal":"1","heart_murmur_valve_surgery":"1","heart_murmur_heart_enlargement":"1","heart_murmur_symptomatic":"1","heart_murmur_progression":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.heart_murmur?).to be true
        diseases = health.diseases
        expect(diseases.heart_murmur.condition_degree).to eq 'Mild'
        expect(diseases.heart_murmur.date).to eq Date.new(1999,11,11)
        expect(diseases.heart_murmur.ever_echocardiogram).to eq true
        expect(diseases.heart_murmur.heart_enlargement).to eq true
        expect(diseases.heart_murmur.progression).to eq true
        expect(diseases.heart_murmur.symptomatic).to eq true
        expect(diseases.heart_murmur.valve_structures_normal).to eq true
        expect(diseases.heart_murmur.valve_surgery).to eq true
      end

      it 'can set hepatitis_c' do
        attrs = {"hepatitis_c":true,"hepatitis_c_diagnosed_date":"11/11/1999","hepatitis_c_contraction_date":"11/11/1999","hepatitis_c_condition_degree":"Mild","hepatitis_c_normal_viral_loads":"1","hepatitis_c_normal_viral_date":"11/11/1999","hepatitis_c_normal_liver_functions":"1","hepatitis_c_normal_liver_date":"11/11/1999","hepatitis_c_ever_treated":"1","hepatitis_c_currently_treated":"1","hepatitis_c_treatment_start_date":"11/11/1999","hepatitis_c_treatment_end_date":"11/11/1999","hepatitis_c_liver_cirrhosis":"1","hepatitis_c_liver_biopsy":"1","hepatitis_c_complications":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.hepatitis_c?).to be true
        diseases = health.diseases
        expect(diseases.hepatitis_c.complications).to eq true
        expect(diseases.hepatitis_c.condition_degree).to eq 'Mild'
        expect(diseases.hepatitis_c.contraction_date).to eq Date.new(1999,11,11)
        expect(diseases.hepatitis_c.currently_treating).to eq true
        expect(diseases.hepatitis_c.date).to eq Date.new(1999,11,11)
        expect(diseases.hepatitis_c.ever_treated).to eq true
        expect(diseases.hepatitis_c.liver_biopsy).to eq true
        expect(diseases.hepatitis_c.liver_cirrhosis).to eq true
        expect(diseases.hepatitis_c.normal_liver_date).to eq Date.new(1999,11,11)
        expect(diseases.hepatitis_c.normal_liver_functions).to eq true
        expect(diseases.hepatitis_c.normal_viral_date).to eq Date.new(1999,11,11)
        expect(diseases.hepatitis_c.normal_viral_loads).to eq true
        expect(diseases.hepatitis_c.last_treatment).to eq Date.new(1999,11,11)
        expect(diseases.hepatitis_c.treatment_start_date).to eq Date.new(1999,11,11)
      end

      it 'can set irregular_heartbeat' do
        attrs = {"irregular_heartbeat":true,"irregular_heartbeat_diagnosed_date":"11/11/1999","irregular_heartbeat_underlying_heart_disease":"1","irregular_heartbeat_current_symptoms":"1","irregular_heartbeat_current_medications":"1","irregular_heartbeat_atrioventricular_block":"1","irregular_heartbeat_second_degree_av_block":"1","irregular_heartbeat_third_degree_av_block_atr_disassociation":"1","irregular_heartbeat_born_with_third_degree_av_block":"1","irregular_heartbeat_pacemaker_implanted":"1","irregular_heartbeat_pacemaker_implant_date":"11/11/1999","irregular_heartbeat_attrioventricular_junctional_rhythm":"1","irregular_heartbeat_paroxysmal_super_tachycardia":"1","irregular_heartbeat_cardiac_evaluations":"1","irregular_heartbeat_cardiac_eval_result_normal":"1","irregular_heartbeat_last_experience_symptoms":"11/11/1999","irregular_heartbeat_symptoms_per_year":6,"irregular_heartbeat_premature_atrial_complexes":"1","irregular_heartbeat_history_of_cardiovascular_disease":"1","irregular_heartbeat_premature_ventricular_contraction":"1","irregular_heartbeat_simple_pvc":"1","irregular_heartbeat_complex_pvc":"1","irregular_heartbeat_require_treatment_for_pvc":"1","irregular_heartbeat_sick_sinus_syndrome":"1","irregular_heartbeat_pacemaker_for_sick_sinus_syndrome":"1","irregular_heartbeat_pacemaker_for_sick_sinus_syndrome_implant_date":"11/11/1999","irregular_heartbeat_history_of_fainting":"1","irregular_heartbeat_sinus_bradycardia":"1","irregular_heartbeat_sinus_bradycardia_caused_by_another_condition":"1","irregular_heartbeat_sinus_bradycardia_caused_by_medication":"1","irregular_heartbeat_sinus_bradycardia_cause_unknown":"1","irregular_heartbeat_wandering_pacemaker":"1","irregular_heartbeat_pulse_rate":9,"irregular_heartbeat_cardiac_eval_for_wandering_pacemaker":"1","irregular_heartbeat_idoventricular_rhythm":"1","irregular_heartbeat_mobitz_type_1_block":12,"irregular_heartbeat_mobitz_type_2_block":6,}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.irregular_heartbeat?).to be true
        diseases = health.diseases
        expect(diseases.irregular_heartbeat.atrioventricular_block).to eq true
        expect(diseases.irregular_heartbeat.attrioventricular_junctional_rhythm).to eq true
        expect(diseases.irregular_heartbeat.born_with_third_degree_av_block).to eq true
        expect(diseases.irregular_heartbeat.cardiac_eval_for_wandering_pacemaker).to eq true
        expect(diseases.irregular_heartbeat.cardiac_eval_result_normal).to eq true
        expect(diseases.irregular_heartbeat.cardiac_evaluations).to eq true
        expect(diseases.irregular_heartbeat.complex_pvc).to eq true
        expect(diseases.irregular_heartbeat.current_medications).to eq true
        expect(diseases.irregular_heartbeat.current_symptoms).to eq true
        expect(diseases.irregular_heartbeat.date).to eq Date.new(1999,11,11)
        expect(diseases.irregular_heartbeat.history_of_cardiovascular_disease).to eq true
        expect(diseases.irregular_heartbeat.history_of_fainting).to eq true
        expect(diseases.irregular_heartbeat.idoventricular_rhythm).to eq true
        expect(diseases.irregular_heartbeat.last_experience_symptoms).to eq Date.new(1999,11,11)
        expect(diseases.irregular_heartbeat.mobitz_type_1_block).to eq 12
        expect(diseases.irregular_heartbeat.mobitz_type_2_block).to eq 6
        expect(diseases.irregular_heartbeat.pacemaker_for_sick_sinus_syndrome).to eq true
        expect(diseases.irregular_heartbeat.pacemaker_for_sick_sinus_syndrome_implant_date).to eq Date.new(1999,11,11)
        expect(diseases.irregular_heartbeat.pacemaker_implant_date).to eq Date.new(1999,11,11)
        expect(diseases.irregular_heartbeat.pacemaker_implanted).to eq true
        expect(diseases.irregular_heartbeat.paroxysmal_super_tachycardia).to eq true
        expect(diseases.irregular_heartbeat.premature_atrial_complexes).to eq true
        expect(diseases.irregular_heartbeat.premature_ventricular_contraction).to eq true
        expect(diseases.irregular_heartbeat.pulse_rate).to eq 9
        expect(diseases.irregular_heartbeat.require_treatment_for_pvc).to eq true
        expect(diseases.irregular_heartbeat.second_degree_av_block).to eq true
        expect(diseases.irregular_heartbeat.sick_sinus_syndrome).to eq true
        expect(diseases.irregular_heartbeat.simple_pvc).to eq true
        expect(diseases.irregular_heartbeat.sinus_bradycardia).to eq true
        expect(diseases.irregular_heartbeat.sinus_bradycardia_cause_unknown).to eq true
        expect(diseases.irregular_heartbeat.sinus_bradycardia_caused_by_another_condition).to eq true
        expect(diseases.irregular_heartbeat.sinus_bradycardia_caused_by_medication).to eq true
        expect(diseases.irregular_heartbeat.symptoms_per_year).to eq 6
        expect(diseases.irregular_heartbeat.third_degree_av_block_atr_disassociation).to eq true
        expect(diseases.irregular_heartbeat.underlying_heart_disease).to eq true
        expect(diseases.irregular_heartbeat.wandering_pacemaker).to eq true
      end

      it 'can set moving_violations' do
        expect(Crm::HealthInfo.new.moving_violations?).to be false
        attrs = {"moving_violation":true,"moving_violation_last_6_mo":2,"moving_violation_last_1_yr":3,"moving_violation_last_2_yr":4,"moving_violation_last_5_yr":6,"moving_violation_last_dl_suspension":3.years.ago.to_date,"moving_violation_last_dui_dwi":'11/17/1966',"moving_violation_last_reckless_driving":'02/28/1978',}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.moving_violations?).to be true
        expect(health.moving_violations.length).to eq 8
        duis, non_duis = health.moving_violations.partition { |v| v.dui_dwi }
        expect(duis.length).to eq 1
        expect(duis.first.date).to eq Date.new(1966,11,17)
        expect(duis.map &:dui_dwi).to all(be true)
        expect(non_duis.map &:dui_dwi).to all(be_nil)
        suspensions, non_suspensions = health.moving_violations.partition { |v| v.dl_suspension }
        expect(suspensions.length).to eq 1
        expect(suspensions.map &:dl_suspension).to all(be true)
        expect(non_suspensions.map &:dl_suspension).to all(be_nil)
        reckless, non_reckless = health.moving_violations.partition { |v| v.reckless_driving }
        expect(reckless.length).to eq 1
        expect(reckless.map &:reckless_driving).to all(be true)
        expect(non_reckless.map &:reckless_driving).to all(be_nil)
      end

      it 'fails to set bad moving_violations' do
        attrs = {"bad_attr":3}
        expect {
          Crm::HealthInfo.new(moving_violation_history_attributes: attrs)
        }.to raise_error ActiveRecord::UnknownAttributeError
      end

      it 'can set multiple_sclerosis' do
        attrs = {"multiple_sclerosis":true,"multiple_sclerosis_diagnosed_date":"11/11/1999","multiple_sclerosis_condition_degree":"Mild","multiple_sclerosis_attacks_per_year":3,"multiple_sclerosis_last_attack_date":"11/11/1999","multiple_sclerosis_condition_type":"lkjh",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.multiple_sclerosis?).to be true
        diseases = health.diseases
        expect(diseases.multiple_sclerosis.attacks_per_year).to eq 3
        expect(diseases.multiple_sclerosis.condition_degree).to eq 'Mild'
        expect(diseases.multiple_sclerosis.condition_type).to eq 'lkjh'
        expect(diseases.multiple_sclerosis.date).to eq Date.new(1999,11,11)
        expect(diseases.multiple_sclerosis.last_attack_date).to eq Date.new(1999,11,11)
      end

      it 'can set parkinsons' do
        attrs = {"parkinsons":true,"parkinsons_diagnosed_date":"11/11/1999","parkinsons_age_at_diagnosis":8,"parkinsons_condition_degree":"Mild","parkinsons_live_independently":"1","parkinsons_condition_stable":"1","parkinsons_currently_disabled":"1","parkinsons_disabled_severity":"dfg","parkinsons_currently_receive_treatment":"1","parkinsons_rigidity":"1","parkinsons_rigidity_severity":"wert","parkinsons_stable_date":"11/11/1999","parkinsons_walking_impairment":"1","parkinsons_walking_impairment_severity":"ijh","parkinsons_mental_deterioration":"1","parkinsons_affect_fingers_only":"1","parkinsons_affect_hands_only":"1","parkinsons_affect_multiple_areas":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.parkinsons?).to be true
        diseases = health.diseases
        expect(diseases.parkinsons.affect_fingers_only).to eq true
        expect(diseases.parkinsons.affect_hands_only).to eq true
        expect(diseases.parkinsons.affect_multiple_areas).to eq true
        expect(diseases.parkinsons.age_at_diagnosis).to eq 8
        expect(diseases.parkinsons.condition_degree).to eq 'Mild'
        expect(diseases.parkinsons.condition_stable).to eq true
        expect(diseases.parkinsons.currently_disabled).to eq true
        expect(diseases.parkinsons.currently_receive_treatment).to eq true
        expect(diseases.parkinsons.date).to eq Date.new(1999,11,11)
        expect(diseases.parkinsons.disabled_severity).to eq 'dfg'
        expect(diseases.parkinsons.live_independently).to eq true
        expect(diseases.parkinsons.mental_deterioration).to eq true
        expect(diseases.parkinsons.rigidity).to eq true
        expect(diseases.parkinsons.rigidity_severity).to eq 'wert'
        expect(diseases.parkinsons.stable_date).to eq Date.new(1999,11,11)
        expect(diseases.parkinsons.walking_impairment).to eq true
        expect(diseases.parkinsons.walking_impairment_severity).to eq 'ijh'
      end

      it 'can set sleep_apnea' do
        attrs = {"sleep_apnea":true,"sleep_apnea_diagnosed_date":"11/11/1978","sleep_apnea_condition_degree":"Mild","sleep_apnea_ever_treated":"1","sleep_apnea_treatment_start_date":"11/11/1999","sleep_apnea_use_cpap":"1","sleep_apnea_rd_index":7,"sleep_apnea_apnea_index":6,"sleep_apnea_ah_index":5,"sleep_apnea_o2_saturation":4,"sleep_apnea_cpap_machine_complaint":"1","sleep_apnea_sleep_study":"1","sleep_apnea_sleep_study_followup":"1","sleep_apnea_on_oxygen":"1","sleep_apnea_currently_sleep_apnea":"1",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.sleep_apnea?).to be true
        diseases = health.diseases
        expect(diseases.sleep_apnea.ah_index).to eq 5
        expect(diseases.sleep_apnea.apnea_index).to eq 6
        expect(diseases.sleep_apnea.condition_degree).to eq 'Mild'
        expect(diseases.sleep_apnea.cpap_machine_complaint).to eq true
        expect(diseases.sleep_apnea.currently_sleep_apnea).to eq true
        expect(diseases.sleep_apnea.date).to eq Date.new(1978,11,11)
        expect(diseases.sleep_apnea.ever_treated).to eq true
        expect(diseases.sleep_apnea.o2_saturation).to eq 4
        expect(diseases.sleep_apnea.on_oxygen).to eq true
        expect(diseases.sleep_apnea.rd_index).to eq 7
        expect(diseases.sleep_apnea.sleep_study).to eq true
        expect(diseases.sleep_apnea.sleep_study_followup).to eq true
        expect(diseases.sleep_apnea.treatment_start_date).to eq Date.new(1999,11,11)
        expect(diseases.sleep_apnea.use_cpap).to eq true
      end

     it 'can set relatives_diseases' do
        attrs = {"family_diseases_attributes":[{"parent":"1","age_of_contraction":4,"age_of_death":5,"coronary_artery_disease":true,"breast_cancer":true,"malignant_melanoma":true,"ovarian_cancer":false}]}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        relatives = health.relatives_diseases
        expect(relatives.length).to eq(3)
        expect(relatives.map &:relationship_type_id).to all(eq(Enum::RelationshipType.id('parent')))
        expect(relatives.map &:onset_age).to all(eq(4))
        expect(relatives.map &:death_age).to all(eq(5))
        type_ids = %w[breast_cancer malignant_melanoma coronary_artery_disease].map { |l| Enum::DiseaseType.id(l) }
        expect(relatives.map &:type_id).to match_array type_ids
      end

      it 'can set stroke' do
        attrs = {"stroke":true,"stroke_condition_degree":"Mild","stroke_last_stroke_date":"11/11/1999","stroke_multiple_strokes":"1","stroke_first_stroke_age":8,}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.stroke?).to be true
        diseases = health.diseases
        expect(diseases.stroke.condition_degree).to eq 'Mild'
        expect(diseases.stroke.onset_age).to eq 8
        expect(diseases.stroke.last_stroke_date).to eq Date.new(1999,11,11)
        expect(diseases.stroke.multiple_strokes).to eq true
      end

      it 'can set tobacco' do
        attrs = {"tobacco_cigarettes_per_day":5,"tobacco":true,"tobacco_cigarettes_current":"1","tobacco_cigars_current":"1","cigars_per_month":6,"tobacco_pipe_current":false,"pipes_per_year":3,"tobacco_pipe_last":"03/04/1967",}
        health = Crm::HealthInfo.new(attrs).tap(&:save!)
        expect(health.tobacco?).to eq true
        expect(health.tobacco_details.cigarettes_current).to eq true
        expect(health.tobacco_details.cigarettes_per_day).to eq 5
        expect(health.tobacco_details.cigars_current).to eq true
        expect(health.tobacco_details.cigars_per_month).to eq 6
        expect(health.tobacco_details.pipe_current).to eq false
        expect(health.tobacco_details.pipe_last).to eq Date.new(1967,3,4)
        expect(health.tobacco_details.pipes_per_year).to eq 3
      end
    end
  end

  describe 'diseases' do
    it 'is an empty DiseaseSet when initialized' do
      expect(subject.diseases).to be_a Health::Disease::DiseaseSet
      expect(subject.diseases).to be_empty
    end
  end

  describe '#diseases=' do
    it 'overwrites the existing value of diseases' do
      expect(subject.diseases).to be_a Health::Disease::DiseaseSet
      expect(subject.diseases).to be_empty
      subject.diseases = { other_skin_cancer: { grade: '3' }, basal_cell_carcinoma: { onset_age: 31, death_age: 33 } }
      expect(subject.diseases).to be_a Health::Disease::DiseaseSet
      expect(subject.diseases.length).to eq 2
      expect(subject.diseases.values).to all(be_a Health::Disease)
      expect(subject.diseases.values.map &:type_id).to all(be_present)
      expect(subject.diseases['other_skin_cancer']).to eq subject.diseases[:other_skin_cancer]
      expect(subject.diseases['other_skin_cancer'].grade).to eq 3
      expect(subject.diseases['basal_cell_carcinoma'].onset_age).to eq 31
      expect(subject.diseases['basal_cell_carcinoma'].death_age).to eq 33
      subject.diseases = { other_skin_cancer: { date: 3.days.ago } }
      expect(subject.diseases.length).to eq 1
      expect(subject.diseases['other_skin_cancer'].grade).to be_nil
      expect(subject.diseases['other_skin_cancer'].date).to be_a Date
      expect(subject.diseases_changed?).to be true
    end

    it 'updates *change* values' do
      subject.diseases = {other_skin_cancer: {grade: '8'}}
      expect(subject.changed?).to be true
      expect(subject.diseases_changed?).to be true
      expect(subject.changes).to have_key 'diseases'
      subject.save!
      expect(subject.changed?).to be false
      expect(subject.diseases_changed?).to be false
      subject.diseases = {other_skin_cancer: {onset_age: 12}}
      expect(subject.changed?).to be true
      expect(subject.diseases_changed?).to be true
    end

    it 'enforces date type-casting' do
      subject.diseases = { copd: { date: '2020-01-01' } }
      expect(subject.diseases.copd.date).to be_a Date
    end

    it 'enforces boolean type-casting' do
      subject.diseases = { copd: { has_symptoms: '0' } }
      expect(subject.diseases.copd.has_symptoms).to be false
      subject.diseases = { copd: { has_symptoms: '1' } }
      expect(subject.diseases.copd.has_symptoms).to be true
    end

    it 'enforces integer type-casting' do
      subject.diseases = { depression: { medication_count: '4' } }
      expect(subject.diseases.depression.medication_count).to be_a Integer
      expect(subject.diseases.depression.medication_count).to eq 4
    end

    it 'enforces float type-casting' do
      subject.diseases = { diabetes_1: { last_a1_c: '4.6' } }
      expect(subject.diseases.diabetes_1.last_a1_c).to be_a Float
      expect(subject.diseases.diabetes_1.last_a1_c).to eq 4.6
    end    
  end

  describe '#relatives_diseases' do
    it 'is an empty DiseaseArray when initialized' do
      expect(subject.relatives_diseases).to be_a Health::Disease::DiseaseArray
      expect(subject.relatives_diseases).to be_empty
    end
  end

  # This is a legacy pseudo-accessor which the quote path uses
  describe '#family_diseases_attributes' do
    it 'is an empty DiseaseArray when initialized' do
      expect(subject.family_diseases_attributes).to be_a Health::Disease::DiseaseArray
      expect(subject.relatives_diseases).to be_empty
    end
  end

  # This is a legacy pseudo-accessor which the quote path uses
  describe '#family_diseases_attributes=' do
    it 'set relatives_diseases' do
      subject.family_diseases_attributes = [{"parent":"1","age_of_contraction":4,"age_of_death":5,"coronary_artery_disease":true,"breast_cancer":true,"ovarian_cancer":false}]
      expect(subject.family_diseases_attributes).to be_a Health::Disease::DiseaseArray
      expect(subject.relatives_diseases.length).to eq 2
    end
  end

  describe '#relatives_diseases=' do
    it 'overwrites the existing value of relatives_diseases' do
      expect(subject.relatives_diseases).to be_a Health::Disease::DiseaseArray
      expect(subject.relatives_diseases).to be_empty
      subject.relatives_diseases = [{ type_id: 1, ever_treated: '1' }, { type_id: 7, onset_age: 31, death_age: 33 }]
      expect(subject.relatives_diseases).to be_a Health::Disease::DiseaseArray
      expect(subject.relatives_diseases.length).to eq 2
      expect(subject.relatives_diseases).to all(be_a Health::Disease)
      expect(subject.relatives_diseases.map &:type_id).to all(be_present)
      expect(subject.relatives_diseases[0]).to be_a Crm::HealthInfo::AlcoholAbuse
      expect(subject.relatives_diseases[1]).to be_a Crm::HealthInfo::OtherDisease
      expect(subject.relatives_diseases[0].ever_treated).to eq true
      expect(subject.relatives_diseases[1].onset_age).to eq 31
      expect(subject.relatives_diseases[1].death_age).to eq 33
      subject.relatives_diseases = [{ type_id: 1, date: 3.days.ago }]
      expect(subject.relatives_diseases.length).to eq 1
      expect(subject.relatives_diseases[0].ever_treated).to be_nil
      expect(subject.relatives_diseases[0].date).to be_a Date
      expect(subject.relatives_diseases_changed?).to be true
    end
  end

  shared_examples_for 'stores as Disease subtype' do |label, klass, attrs|
    it "stores #{label} as #{klass.name.demodulize}" do
      subject.diseases = { label => attrs }
      expect(subject.diseases[label]).to be_a klass
      expect(subject.send label).to be_a klass
      expect(subject.send "#{label}_details").to be_a klass
    end
  end
  begin
    it_behaves_like 'stores as Disease subtype', :alcohol_abuse, Crm::HealthInfo::AlcoholAbuse, { last_consumed_date: 3.days.ago }
    it_behaves_like 'stores as Disease subtype', :anxiety, Crm::HealthInfo::Anxiety, { episodes_year: 3 }
    it_behaves_like 'stores as Disease subtype', :arthritis, Crm::HealthInfo::Arthritis, { current_suppressants: true }
    it_behaves_like 'stores as Disease subtype', :asthma, Crm::HealthInfo::Asthma, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :atrial_fibrillation, Crm::HealthInfo::AtrialFibrillation, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :breast_cancer, Crm::HealthInfo::BreastCancer, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :basal_cell_carcinoma, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :cardiovascular_disease, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :cardiovascular_impairments, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :cerebrovascular_disease, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :colon_cancer, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :copd, Crm::HealthInfo::Copd, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :coronary_artery_disease, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :crohns, Crm::HealthInfo::Crohns, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :depression, Crm::HealthInfo::Depression, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :diabetes_1, Crm::HealthInfo::Diabetes1, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :diabetes_2, Crm::HealthInfo::Diabetes2, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :drug_abuse, Crm::HealthInfo::DrugAbuse, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :epilepsy, Crm::HealthInfo::Epilepsy, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :elevated_liver_function, Crm::HealthInfo::ElevatedLiverFunction, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :emphysema, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :heart_attack, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :heart_murmur, Crm::HealthInfo::HeartMurmur, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :hepatitis_c, Crm::HealthInfo::HepatitisC, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :intestinal_cancer, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :irregular_heartbeat, Crm::HealthInfo::IrregularHeartbeat, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :kidney_disease, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :malignant_melanoma, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :mental_illness, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :multiple_sclerosis, Crm::HealthInfo::MultipleSclerosis, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :other_internal_cancer, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :other_skin_cancer, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :ovarian_cancer, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :parkinsons, Crm::HealthInfo::Parkinsons, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :prostate_cancer, Crm::HealthInfo::ProstateCancer, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :sleep_apnea, Crm::HealthInfo::SleepApnea, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :stroke, Crm::HealthInfo::Stroke, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :ulcerative_colitis_iletis, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :vascular_disease, Crm::HealthInfo::OtherDisease, { date: 2.years.ago }
    it_behaves_like 'stores as Disease subtype', :weight_reduction, Crm::HealthInfo::WeightReduction, { date: 2.years.ago }
  end

  shared_examples_for 'HealthInfo::Detail' do |prefix, suffix, changed_field|
    describe "#{prefix}_#{suffix}=" do
      it 'updates *change* values' do
        expect(subject.changed?).to be false
        subject.send "#{prefix}_#{suffix}=", :dummy
        expect(subject.changed?).to be true
        expect(subject.send "#{changed_field}_changed?").to be true
      end
    end
  end
  begin
    # Non-diseases
    it_behaves_like 'HealthInfo::Detail', :tobacco, :cigar_last, :tobacco_details
    it_behaves_like 'HealthInfo::Detail', :bp, :systolic, :bp_details
    it_behaves_like 'HealthInfo::Detail', :cholesterol, :hdl, :cholesterol_details
    it_behaves_like 'HealthInfo::Detail', :foreign_travel, :when, :foreign_travel_details
    it_behaves_like 'HealthInfo::Detail', :hazardous_avocation, :flying, :hazardous_avocation_details
    # Diseases
    it_behaves_like 'HealthInfo::Detail', :alcohol_abuse, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :anxiety, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :arthritis, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :asthma, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :atrial_fibrillation, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :breast_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :basal_cell_carcinoma, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :cardiovascular_disease, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :cardiovascular_impairments, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :cerebrovascular_disease, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :colon_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :copd, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :coronary_artery_disease, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :crohns, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :depression, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :diabetes_1, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :diabetes_2, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :drug_abuse, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :epilepsy, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :elevated_liver_function, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :emphysema, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :heart_attack, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :heart_murmur, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :hepatitis_c, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :intestinal_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :irregular_heartbeat, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :kidney_disease, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :malignant_melanoma, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :mental_illness, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :multiple_sclerosis, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :other_internal_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :other_skin_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :ovarian_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :parkinsons, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :prostate_cancer, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :sleep_apnea, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :stroke, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :ulcerative_colitis_iletis, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :vascular_disease, :date, :diseases
    it_behaves_like 'HealthInfo::Detail', :weight_reduction, :date, :diseases
  end
end
