require 'spec_helper'

describe Consumer do
  let(:married) { Enum::MaritalStatus.id('Married') }
  let(:divorced){ Enum::MaritalStatus.id('Divorced') }

  let(:agent) { build :user }

  let(:person_1){ FactoryBot.create :consumer, agent: agent, brand:agent.default_brand, financial_info: fi }
  let(:person_2){ FactoryBot.create :consumer, agent: agent, brand:agent.default_brand }
  let(:person_3){ FactoryBot.create :consumer, agent: agent, brand:agent.default_brand }
  let(:fi){       FactoryBot.create :crm_financial_info, net_worth_specified:500_000, net_worth_use_specified:true }

  describe '#age' do
    it 'returns the expected value' do
      Timecop.travel Time.local(2000, 12, 1) do
        person_1.birth = Date.new(2000, 6, 5)
        expect(person_1.age).to eq 0
        expect(person_1.age true).to eq 0
      end
      Timecop.travel Time.local(2001, 6, 1) do
        person_1.birth = Date.new(2000, 6, 5)
        expect(person_1.age).to eq 0
        expect(person_1.age true).to eq 1
      end
      Timecop.travel Time.local(2001, 6, 5) do
        person_1.birth = Date.new(2000, 6, 1)
        expect(person_1.age).to eq 1
        expect(person_1.age true).to eq 1
      end
    end
  end

  describe '#coverage_in_force' do
    subject { create :consumer }

    it 'returns 0 when there are no cases in force' do
      expect(subject.coverage_in_force).to eq 0
      kase0 = subject.cases.create effective_date: 1.day.ago
      expect(subject.coverage_in_force).to eq 0
      kase0.quoting_details.create sales_stage_id: 3, face_amount: 4
      expect(subject.coverage_in_force).to eq 0
    end

    it 'returns sum' do
      kase1 = subject.cases.create effective_date: 1.day.ago
      kase1.quoting_details.create sales_stage_id: 4, face_amount: 4
      expect(subject.coverage_in_force).to eq 4
      kase1 = subject.cases.create effective_date: 1.day.ago
      kase1.quoting_details.create sales_stage_id: 4, face_amount: 3
      expect(subject.coverage_in_force).to eq 7
    end
  end

  describe "#update_tracking_tables" do
    let(:client){ build :consumer }

    context 'for new record' do
      context 'when record has :source' do
        it 'sets :source from :source' do
          client.source = 'foo'
          client.tags.delete :source
          client.should_receive(:update_tracking_tables).and_call_original
          client.save
          client.reload.source.should == 'foo'
        end
      end
      context 'when record has "source" tag' do
        it 'sets :source from "source" tag' do
          client.source = nil
          client.tags << 'source=bar'
          client.should_receive(:update_tracking_tables).and_call_original
          client.save
          client.reload.source.should == 'bar'
        end
      end
      context 'when record has :source and receives a "source" tag' do
        it 'sets :source from the tag and removes the tag' do
          client.source = 'foo'
          client.tags = %w[source=bar foo=baz]
          expect(client.tags).to match_array %w[source=bar foo=baz]
          client.should_receive(:update_tracking_tables).and_call_original
          client.save
          client.reload.source.should == 'bar'
          expect(client.tags).to match_array %w[foo=baz]
        end
      end
      context 'when record has :source and receives a "source" tag but the new "source" tag value is blank' do
        it 'does not change :source but removes the tag' do
          client.source = 'foo'
          client.tags = %w[source= foo=baz]
          expect(client.tags).to match_array %w[source= foo=baz]
          client.should_receive(:update_tracking_tables).and_call_original
          client.save
          client.reload.source.should == 'foo'
          expect(client.tags).to match_array %w[foo=baz]
        end
      end
    end
    context 'when record has neither :source nor tags[:source]' do
      it 'source does not get set' do
        client.source = nil
        client.tags.delete :source
        client.should_receive(:update_tracking_tables).and_call_original
        client.save
        client.reload.source.should be_nil
      end
    end
  end

  describe '#merge_to' do
    let(:dup){create :consumer_w_assoc}
    context 'with required fields, no optional ones' do
      let(:client){build :consumer, birth:nil}
      it 'raises no error' do
        expect{ client.merge_to(dup) }
      end
    end
    context 'with required fields, & optional ones' do
      let(:client){build :consumer_w_assoc}
      it 'raises no error' do
        expect{ client.merge_to(dup) }
      end
    end
  end
  describe '#financial_info' do
    context 'when persisted consumer already has financial_info' do
      it 'returns the existing associated record' do
        person_1.financial_info.should equal(fi)
      end
    end
    context 'when persisted consumer lacks a financial_info' do
      it 'creates and returns a new one' do
        person_1.financial_info.destroy
        person_1.reload
        person_1.financial_info.id.should_not be_nil
        person_1.financial_info.should_not equal(fi)
      end
    end
  end
  describe '#delete' do
    let(:c){create :consumer}
    it 'should set active and active_or_enabled to false' do
      c.delete
      c.active.should == false
      c.contact.active_or_enabled.should == false
    end
  end
  describe '#destroy' do
    let(:c){create :consumer}
    it 'should set active and active_or_enabled to false' do
      c.destroy
      c.active.should == false
      c.contact.active_or_enabled.should == false
    end
  end
  describe '.delete_all' do
    it 'should raise error' do
      expect{Consumer.delete_all}.to raise_error("Not allowed for paranoid class")
    end
  end
  describe '.destroy_all' do
    it 'should raise error' do
      expect{Consumer.destroy_all}.to raise_error("Not allowed for paranoid class")
    end
  end
end
