require 'spec_helper'

describe Address do
  describe '#update_contact callback' do
    it 'fires when address saves' do
      addr = build :address
      addr.should_receive(:update_contact).and_call_original
      addr.save
    end
  end

  describe '::parse' do
    describe 'with multi-line street' do
      it 'captures all data' do
        a = Address.parse "123 fake st.\nc/o Maurice\nRoseville, CA 95661"
        a.street.should == "123 fake st.\nc/o Maurice"
        a.city.should == "Roseville"
        a.state_abbrev.should == "CA"
        a.zip.should == "95661"
      end
    end
    describe 'single line' do
      it 'captures all data' do
        a = Address.parse "123 fake st., c/o Maurice, Roseville, CA 95661"
        a.street.should == "123 fake st., c/o Maurice"
        a.city.should == "Roseville"
        a.state_abbrev.should == "CA"
        a.zip.should == "95661"
      end
    end
  end

  describe '#state_abbrev' do
    it 'gets a valid abbrev' do
      a = Address.new value:"123 fake st.\nRoseville, CA 95661"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nRoseville, CA 95661-7890"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nSt Cloud CA 95661-7890"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nCA 95661"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\n CA 95661"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\\Roseville, CA 95661"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nRoseville, CA"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nCA"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st. CA"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\\CA"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.CA"
      a.state_abbrev.should == "CA"
      a = Address.new value:"123 fake st.\nCA."
      a.state_abbrev.should be_nil
      a = Address.new value:"123 fake st.\nRoseville, CAP 95661-7890"
      a.state_abbrev.should be_nil
      a = Address.new value:"123 fake st.\nRoseville, CA- 95661-7890"
      a.state_abbrev.should be_nil
      a = Address.new value:"123 fake st.\nRoseville, CAPB 95661-7890"
      a.state_abbrev.should be_nil
      a = Address.new value:"123 fake st.\nRoseville, CA-PBJ 95661-7890"
      a.state_abbrev.should be_nil
      a = Address.new value:"123 fake st.\nRoseville, NY 95661-7890"
      a.state_abbrev.should == "NY"
    end
  end

  describe '#zip' do
    it 'gets a zip' do
      a = Address.new value:"123 fake st.\nRoseville, CA 95661"
      a.zip.should == "95661"
      a = Address.new value:"123 fake st.\nRoseville, CA 95661\n"
      a.zip.should == "95661"
      a = Address.new value:"123 fake st. Roseville, CA 95661"
      a.zip.should == "95661"
      a = Address.new value:"123 fake st. Roseville, CA 95661-7892 "
      a.zip.should == "95661-7892"
    end
  end

  describe '#matches?' do
    it 'raises no error' do
      expect{ build(:address).matches?(build :address) }.to_not raise_error
    end
  end
end