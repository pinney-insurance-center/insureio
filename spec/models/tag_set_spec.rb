require 'spec_helper'

describe TagSet do
  subject { TagSet.new(%w[foo bar baz]) }

  describe 'constructor' do
    it 'works when input is not supplied' do
      t = TagSet.new
      expect(t.length).to eq(0)
    end
    it 'works when input is nil' do
      t = TagSet.new nil
      expect(t.length).to eq(0)
    end
    it 'works when input is a Hash' do # This is a legacy format
      t = TagSet.new a:'b', c:'d'
      expect(t).to match_array(%w[a-b c-d])
    end
    it 'works when input is an Array of Hashes' do # This is a legacy format
      t = TagSet.new [{key: 'a', value: 'b'}, {key: 'c'}, {value: 'd'}]
      expect(t).to match_array(%w[a-b c d])
    end
    it 'works when input is an Array of Strings' do
      t = TagSet.new ['a', 'b', 'c']
      expect(t).to match_array(%w[a b c])
    end
    it 'works when input is a Set' do
      t = TagSet.new Set.new(['a', 'b', 'c'])
      expect(t).to match_array(%w[a b c])
    end
    it 'raises an error when input is an arbitrary object' do
      expect { TagSet.new OpenStruct.new(foo: 'bar') }
      .to raise_error ArgumentError
    end
    it 'drops tagN-format keys' do
      t = TagSet.new tag8: 'val8', value7: 'val7', key16: 'val16', tagB: 'valB'
      expect(t).to match_array(%w[val8 val7 val16 tagB-valB])
    end
  end

  describe '#add' do
    it 'taints when new value is truly new' do
      subject << 'qux'
      expect(subject.changed?).to be true
    end
    it 'does not taint when new value is alread in set' do
      subject << 'foo'
      expect(subject.changed?).to be false
    end
    it 'does not taint when added value was newly deleted' do
      subject.delete 'foo'
      expect(subject.changed?).to be true
      subject << 'foo'
      expect(subject.changed?).to be false
    end
    it 'remains tainted if not all newly deleted values are added' do
      subject.delete 'foo'
      subject.delete 'bar'
      subject << 'foo'
      expect(subject.changed?).to be true
    end
  end

  describe '#delete' do
    it 'taints when deleted value was in set' do
      subject.delete 'foo'
      expect(subject.changed?).to be true
    end
    it 'does not taint when deleted value was not in set' do
      subject.delete 'qux'
      expect(subject.changed?).to be false
    end
    it 'does not taint when deleted value was newly added' do
      subject << 'qux'
      expect(subject.changed?).to be true
      subject.delete 'qux'
      expect(subject.changed?).to be false
    end
    it 'remains tainted if not all newly added values are deleted' do
      subject << 'qux'
      subject << 'bin'
      subject.delete 'qux'
      expect(subject.changed?).to be true
    end
  end
end