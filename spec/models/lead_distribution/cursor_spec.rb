require 'spec_helper'

describe LeadDistribution::Cursor do

  subject{c=LeadDistribution::Cursor.new brand_id:1, lead_type:Crm::LeadType.new; c.id=1; c}
  let(:agent_0) { build_stubbed :agent, id:2+0 }
  let(:agent_1) { build_stubbed :agent, id:2+1 }
  let(:agent_2) { build_stubbed :agent, id:2+2 }
  let(:agent_3) { build_stubbed :agent, id:2+3 }
  let(:agent_4) { build_stubbed :agent, id:2+4 }
  let(:agent_5) { build_stubbed :agent, id:2+5 }
  let(:agents)  { [agent_0, agent_1, agent_2, agent_3, agent_4, agent_5] }
  let(:rule_0)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+0, user_id:2+0, sunday:1, monday:0, tuesday:1, wednesday: 1 }
  let(:rule_1)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+1, user_id:2+1, sunday:2, monday:1, tuesday:1, wednesday: 2 }
  let(:rule_2)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+2, user_id:2+2, sunday:3, monday:2, tuesday:2, wednesday: 2 }
  let(:rule_3)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+3, user_id:2+3, sunday:1, monday:1, tuesday:3, wednesday: 2 }
  let(:rule_4)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+4, user_id:2+4, sunday:1, monday:1, tuesday:4, wednesday: 2 }
  let(:rule_5)  { build_stubbed :lead_distribution_user_rule, count:0, id:2+5, user_id:2+5, sunday:1, monday:1, tuesday:5, wednesday: 2 }
  let(:rules)   { [rule_0, rule_1, rule_2, rule_3, rule_4, rule_5] }
  let(:brand) { build :brand }

  before do
    LeadDistribution::Cursor.any_instance.stub(:brand){brand}
  end

  describe '#rules' do
    context 'when cursor has 6 agents and only 3 rules' do
      before do
        User.any_instance.stub(:save).and_return(true)
        expect(subject).to receive(:all_rules).and_return(FakeRelation.new(rules[0..2]))
        expect(subject).to receive(:agents).and_return(FakeRelation.new(agents))
      end
      it 'creates UserRules for agents who lack them' do
        expect{ subject.rules }.to change(LeadDistribution::UserRule, :count).by(3)
      end
    end
    context 'with stubbed :brand' do
      it 'runs without error' do
        expect(subject).to receive(:brand).at_least(:once).and_return(brand)
        expect{subject.send(:rules) }.to_not raise_exception
      end
    end
  end

  describe '#agents' do
    it 'returns a non-suspended agent' do
      agents = create_list :agent, 2
      brand.save
      brand.members = agents
      agents.first.update_attributes temporary_suspension:true
      subject.brand = brand
      cursor_agents = subject.send(:agents)
      expect(cursor_agents.length).to be 1
      expect(cursor_agents.first.id).to be agents.last.id
    end
  end

  describe '#next!' do
    before do
      expect(subject).to receive(:all_rules).at_least(:once).and_return(FakeRelation.new(rules[0..2]))
      expect(subject).to receive(:agents).and_return(FakeRelation.new(agents[0..2]))
    end
    context 'when none of the weights is zero' do
      it 'returns the next rule whose ratio is no higher than the population minimum' do
        wday = 0 # sunday
        expect(subject.next!(wday)).to be rule_0
        expect(subject.rule_id).to be 2+0
        rule_0.count += 1
        expect(subject.next!(wday)).to be rule_1
        expect(subject.rule_id).to be 2+1
        rule_1.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_1
        expect(subject.rule_id).to be 2+1
        rule_1.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_0
        expect(subject.rule_id).to be 2+0
        rule_0.count += 1
      end
    end
    context 'when one of the weights is zero' do
      it 'returns the next rule whose ratio is no higher than the population minimum' do
        wday = 1 # monday
        expect(subject.next!(wday)).to be rule_1
        expect(subject.rule_id).to be 2+1
        rule_1.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_1
        expect(subject.rule_id).to be 2+1
        rule_1.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
      end
    end
    context 'when one of the rules is deactivated' do
      it 'returns the next rule whose ratio is no higher than the population minimum' do
        wday = 2 # tuesday
        rule_1.off = true
        expect(subject.next!(wday)).to be rule_0
        expect(subject.rule_id).to be 2+0
        rule_0.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_0
        expect(subject.rule_id).to be 2+0
        rule_0.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
        expect(subject.next!(wday)).to be rule_2
        expect(subject.rule_id).to be 2+2
        rule_2.count += 1
      end
    end
  end

  it 'validates uniquness on lead_type+brand' do
    allow_any_instance_of(LeadDistribution::Cursor).to receive(:brand).and_return(Brand.new)
    allow_any_instance_of(LeadDistribution::Cursor).to receive(:lead_type).and_return(Crm::LeadType.new)
    cursor_0 = LeadDistribution::Cursor.new(brand_id:1, lead_type_id:1)
    cursor_1 = LeadDistribution::Cursor.new(brand_id:1, lead_type_id:1)
    expect(cursor_0.save).to be true
    expect(cursor_1.save).to be false
    expect(cursor_1.errors[:lead_type_id]).to include("has already been taken")
  end
  
  class FakeRelation
    def initialize(val); @val = val; end
    def to_a; @val; end
    def each; @val.each{|item| yield(item)}; end
    def where(*args); self; end
    alias_method :order, :where
  end

end

