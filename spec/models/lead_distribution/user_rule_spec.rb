 require 'spec_helper'

describe LeadDistribution::UserRule do

  subject{build :lead_distribution_user_rule, sunday:21, monday:1, tuesday:2, wednesday:3, thursday:5, friday:8, saturday:13}

  describe '#weight' do
    it 'returns the right weight for a given day' do
      Timecop.freeze(Time.new 2014, 10, 26) do
        expect(subject.weight(Date.today.wday)).to be 21
      end
      Timecop.freeze(Time.new 2014, 10, 27) do
        expect(subject.weight(Date.today.wday)).to be 1
      end
      Timecop.freeze(Time.new 2014, 10, 28) do
        expect(subject.weight(Date.today.wday)).to be 2
      end
      Timecop.freeze(Time.new 2014, 10, 29) do
        expect(subject.weight(Date.today.wday)).to be 3
      end
      Timecop.freeze(Time.new 2014, 10, 30) do
        expect(subject.weight(Date.today.wday)).to be 5
      end
      Timecop.freeze(Time.new 2014, 10, 31) do
        expect(subject.weight(Date.today.wday)).to be 8
      end
      Timecop.freeze(Time.new 2014, 11, 1) do
        expect(subject.weight(Date.today.wday)).to be 13
      end
    end
  end

  describe '#ratio' do
    it 'returns floor integer' do
      subject.count = 400
      expect(subject.ratio(0)).to be 19
      expect(subject.ratio(1)).to be 400
      expect(subject.ratio(2)).to be 200
      expect(subject.ratio(3)).to be 133
      expect(subject.ratio(4)).to be 80
      expect(subject.ratio(5)).to be 50
      expect(subject.ratio(6)).to be 30
    end

    it 'does not choke on negative weights' do
      subject.count = 400
      subject.sunday = 0
      expect(subject.ratio(0)).to be LeadDistribution::UserRule::MAX_INT
    end
  end

  describe '::USER_MODIFIABLE_ATTRS' do
    let(:fields) { %w[sunday monday tuesday wednesday thursday friday saturday tag_blacklist off] }

    it 'references the expected fields' do
      expect(LeadDistribution::UserRule::USER_MODIFIABLE_ATTRS)
      .to match_array fields
    end

    it 'holds fields to which the model responds' do
      fields.each do |name|
        expect(subject).to respond_to("#{name}="),
        "Instance of #{subject.class.name} does not respond to `#{name}=`. Check your database schema."
      end
    end
  end

end