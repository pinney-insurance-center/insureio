require 'spec_helper'

describe PhoneRecording do

  describe '::create_records' do
    let(:party1) { create :user }
    let(:party2) { create :consumer }
    let(:phone1) { create :phone, value: '8005555555', contactable: party1, carrier_id: 1 }
    let(:phone2) { create :phone, value: '6005555555', contactable: party2, carrier_id: 1 }

    it 'creates records without duplicates' do
      params = {
        'Party1' => phone1.value,
        'Party2' => phone2.value,
        'Timestamp' => 3.hours.ago.strftime('%Y%m%d%H%M%S'),
        'S3FilePath' => '8745\\[DirectLine%3ASomeone]_1234567890-8745_20140619162827(46136).mp3',
      }.freeze
      expect {
        PhoneRecording.create_records params
      }.to change(PhoneRecording, :count).by(2)
      expect {
        PhoneRecording.create_records params
      }.to change(PhoneRecording, :count).by(0)
    end
  end
end
