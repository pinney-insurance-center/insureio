require 'spec_helper'

describe Reporting::Search do
  let(:criteria){ {
    "consumer"=>{
      "age_min"=>"60", "age_max"=>"70", "source_id"=>"", "agent_id"=>"1", "lead_type_id"=>"", "brand_id"=>"35", "full_name"=>"ab foo lo", "city"=>"raccoon", "state_id"=>"5", "zip"=>"98706"
    },
    "phone"=>{"value"=>"7897897890"},
    "email"=>{"value"=>"foo@bar.com"},
    "quoted_details"=>{
      "face_amount_min"=>"900", "face_amount_max"=>"9000000", "annual_premium_min"=>"800", "annual_premium_max"=>"9999", "carrier_id"=>"17", "product_type_id"=>"1"
    },
    "status"=>{
      "status_type_id"=>"5", "active"=>"1", "created_at_min"=>"11/11/1978", "created_at_max"=>"11/11/2000"
    }
  } }
  let(:search){ Reporting::Search.new criteria:criteria }

  describe '#scope_cases' do
    context 'when consumer, phone, email are present' do
      it 'joins related tables' do
        sql = search.send(:scope_cases).to_sql
        sql.should include 'INNER JOIN `consumers`'
        sql.should include 'INNER JOIN `email_addresses`'
        sql.should include 'INNER JOIN `phones`'
      end
      it 'executes without error' do
        expect{search.send(:scope_cases).to_a}.to_not raise_error
      end
    end

    context 'when consumer, phone, email are not present' do
      before do
        search.criteria.delete 'consumer'
        search.criteria.delete 'email'
        search.criteria.delete 'phone'
      end
      it 'does not join given tables' do
        sql = search.send(:scope_cases).to_sql
        sql.should_not include 'consumers'
        sql.should_not include 'email_addresses'
        sql.should_not include 'phones'
      end
      it 'executes without error' do
        expect{search.send(:scope_cases).to_a}.to_not raise_error
      end
    end

    context 'when lead_type and source are present' do
      before do
        search.criteria.consumer.lead_type = 'QRF'
        search.criteria.consumer.source = 'Google PPC'
      end
      it 'scopes a WHERE for lead_type and source' do
        sql = search.send(:scope_cases).to_sql
        sql.should match /WHERE.*consumers.?\..?lead_type.? = ['"]QRF['"]/m
        sql.should match /WHERE.*consumers.?\..?source.? = ['"]Google PPC['"]/m
      end
      it 'executes without error' do
        expect{search.send(:scope_cases).to_a}.to_not raise_error
      end
    end

    context 'when lead_type is not present' do
      before do
        search.criteria.consumer.delete 'lead_type'
      end
      it 'does not scope for lead_type' do
        sql = search.send(:scope_cases).to_sql
        sql.should_not include 'lead_type'
      end
      it 'executes without error' do
        expect{search.send(:scope_cases).to_a}.to_not raise_error
      end
    end
  end

  #Called by `#get_fields_for_display`
  describe '#preload_associations' do
    before do
      @the_results_scope=@scope=Crm::Case.all
      search.instance_variable_set('@scope',@the_results_scope)
    end
    it 'preloads all tables whose fields are specified in `fields_to_show`' do
      search.fields_to_show={"consumer":{"brand_id":1},"phone":{"value":1}}
      assocs_needing_preload=[:consumer, {'consumer'=>'phones'}]
      assocs_needing_preload.each do |assoc_name|
        expect(@the_results_scope).to receive(:preload).once.ordered.with(assoc_name).and_call_original
      end
      search.send(:preload_associations,Crm::Case)
    end
    it 'preloads the tables needed for delegated/derived fields specified in `fields_to_show`' do
      search.fields_to_show={"consumer":{"brand_id":1,"brand_name":1},"phone":{"value":1}}
      assocs_needing_preload=[:consumer, {'consumer'=>'phones'}, {'consumer'=>'brand'}]
      assocs_needing_preload.each do |assoc_name|
        expect(@the_results_scope).to receive(:preload).once.ordered.with(assoc_name).and_call_original
      end
      search.send(:preload_associations,Crm::Case)
    end
  end

end
