require 'spec_helper'

describe Extension do
  let(:quote) { Quoting::Quote.new }

  it 'gets built on #ext call' do
    expect(quote.extension).to be_nil
    expect(quote.ext).to_not be_nil
    expect(quote.ext).to contain_exactly()
    expect(quote.extension).to_not be_nil
  end

  it 'gets built on #ext= call' do
    expect(quote.extension).to be_nil
    quote.ext = { a: 4, b: 9 }
    expect(quote.ext).to contain_exactly(*{ 'a' => 4, 'b' => 9 }.to_a)
    expect(quote.extension).to_not be_nil
  end

  it 'gets built by accepting attributes' do
    quote = Quoting::Quote.new(ext: {c: 87, d: 60})
    expect(quote.extension).to_not be_nil
    expect(quote.ext).to contain_exactly(*{ 'c' => 87, 'd' => 60 }.to_a)
  end

  it 'saves when origin saves' do
    quote.ext[:a] = 'poi'
    expect { quote.save }
    .to change(Quoting::Quote, :count).by(1)
    .and change(Extension, :count).by(1)
    expect(quote.reload.ext[:a]).to eq 'poi'
  end

  it 'does not save when its data field is blank' do
    expect(quote.ext).to contain_exactly()
    expect { quote.save }
    .to change(Quoting::Quote, :count).by(1)
    .and change(Extension, :count).by(0)
    expect(quote.extension.id).to be_nil
    expect(quote.reload.extension).to be_nil
    expect(quote.persisted?).to be true
  end

  it 'provides indifferent access' do
    ext = Extension.new
    ext.data[:a] = 'foo'
    expect(ext.data[:a]).to eq 'foo'
    expect(ext.data['a']).to eq 'foo'
    expect(ext.data).to contain_exactly(*{'a' => 'foo'}.to_a)
  end
end