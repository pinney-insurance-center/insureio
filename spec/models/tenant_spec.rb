require 'spec_helper'

describe Enum::Tenant do

  describe '::subdomain_uri' do
    subject{ Enum::Tenant.subdomain_uri(url, tenant) }
    let(:tenant){nil}
    let(:url){'ftp://a.test.insureio.qadev/fiddle/dee/dee.json?a=b&cde=def'}
    context 'with nil tenant' do
      it 'returns a pinney.insureio URI' do
        expect(subject.to_s).to eql 'ftp://pinney.insureio.qadev/fiddle/dee/dee.json?a=b&cde=def'
      end
    end
    context 'with non-insureio url' do
      let(:url){'ftp://a.test.google.qa/fiddle/dee/dee.json?a=b&cde=def'}
      it 'returns nil' do
        expect(subject).to be_nil
      end
    end
    context 'with a tenant of "foo.bar"' do
      let(:tenant){'foo.bar'}
      it 'returns a foo.bar.insureio URI' do
        expect(subject.to_s).to eql 'ftp://foo.bar.insureio.qadev/fiddle/dee/dee.json?a=b&cde=def'
      end
    end
  end

  describe '::subdomain_string' do
    subject{ Enum::Tenant.subdomain_string(url, nil).to_s }
    context 'with a insureio url' do
      let(:url){'ftp://a.test.insureio.qadev/fiddle/dee/dee.json?a=b&cde=def'}
      it 'returns a pinney.insureio URI' do
        expect(subject).to eql 'ftp://pinney.insureio.qadev/fiddle/dee/dee.json?a=b&cde=def'
      end
    end
    context 'with non-insureio url' do
      let(:url){'ftp://a.test.google.qa/fiddle/dee/dee.json?a=b&cde=def'}
      it 'returns url' do
        expect(subject).to eql url
      end
    end
  end

end
