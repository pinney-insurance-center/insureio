require 'spec_helper'

describe Phone do
  let(:contact_id){0}
  let(:phone){ build :phone, phone_type_id:phone_type.id, value:'2345678903', ext:'456', contact_id:contact_id}
  let(:phone_type ){ Enum::PhoneType.find_by_name('home') }


  describe '#update_contactable callback' do
    it 'fires when phone saves' do
      phone.should_receive(:update_contactable).and_call_original
      phone.save
    end
  end

  describe '#matches?' do
    it 'raises no error' do
      expect{ build(:phone).matches?(build :phone) }.to_not raise_error
    end
  end

end