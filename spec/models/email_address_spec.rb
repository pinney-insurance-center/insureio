require 'spec_helper'

describe EmailAddress do
  
  describe '#matches?' do
    it 'raises no error' do
      expect{ build(:email_address).matches?(build :email_address) }.to_not raise_error
    end
  end

end