require 'spec_helper'

describe Marketing::MessageMedia::Message do

  let(:recipient) { create :user }
  let(:carrier) { Enum::PhoneCarrier.find 1 }
  let(:phone_type) { Enum::PhoneType.all.find { |t| t.name == 'mobile' } }
  let(:phone_num) { '5551231234' }
  let(:phone) { create :phone, contactable: recipient, value: phone_num, phone_type: phone_type, carrier: carrier }
  let(:message){ Marketing::MessageMedia::Message.new recipient: recipient.carrier_provided_email }

  before do
    phone
  end

  describe '#recipient=' do
    it 'sets recipient' do # Yes, this was broken
      expect(message.recipient).to eq '5551231234@text.wireless.alltel.com'
      message.recipient = '1234567890@foo.bar'
      expect(message.recipient).to eq '1234567890@foo.bar'
    end
  end
end
