require 'spec_helper'

describe Marketing::Template do

  let(:template_body) { "this is the body" }
  let(:template)  { build :marketing_email_template, body:template_body, subject:'this is subj' }
  let(:brand)     { build :brand }
  let(:agent)     { build :user, full_name:'Arthur B Hansen' }
  let(:case_mgr)  { build :user }
  let(:consumer)  { build :consumer, brand:brand }
  let(:kase)      { build :crm_case, agent_id: agent.id }
  let(:task)      { build :crm_task, agent:agent, case:kase, case_manager:case_mgr, person:consumer }
  let(:data_for_rendering) {{
    'tracking_pixel'      =>'',
    'marketech_esign_url' => '<a>marketech email LINK</a>',
    'letter_signature'    => c::LETTER_SIGNATURE,
    'agent'               => agent,
    'case'                => kase,
    'case_manager'        => case_mgr,
    'recipient'           => consumer,
    'brand'               => brand
  }}

  before do
    agent.stub(:primary_email).and_return 'sday@zooveo.mil'
  end

  context 'when template is a status Email' do
    let(:c) { Marketing::Email::Template }
    before do
      template.template_purpose=Enum::MarketingTemplatePurpose['Status']
      stub_const("Marketing::Email::Template::HEADER",{status:"the header"})
      stub_const("Marketing::Email::Template::FOOTER",{status:"the footer"})
    end

    describe '#render_body' do
      context "when template body lacks header and footer fields" do
        let(:template_body) { "test test" }
        it "should add the template header, footer, and top and bottom content" do

          template.render_body(data_for_rendering).should match(/.*the header.*test test.*the footer.*/)
        end
      end

      context 'when template body includes an address field' do
        let(:template_body) { "test {{ recipient.address }} test" }
        before do
          consumer.addresses.destroy_all
          consumer.addresses_attributes= [{street:"22 Acacia Ave.",city:"City",state_abbreviation:"CA",zip:"99999"}]
        end
        it 'includes address' do
          rendered = template.render_body(data_for_rendering)

          template.body.should include(template_body)
          template_body.should_not include "test 22 Acacia Ave.\nCity, CA 99999 test"

          rendered.should_not include(template_body)
          rendered.should include "test 22 Acacia Ave.\nCity, CA 99999 test"
        end
      end
    end
  end

  describe 'default scope' do
    it 'makes ::find require enabled' do
      @connection = Marketing::Email::Template.connection
      expect(@connection).to receive(:exec_query) do |sql, name, binds|
        # compare mysql
        expect(sql.gsub("\n"," ")).to match(/(WHERE|AND)\s+`marketing_templates`.`enabled` = 1/)
        # return phony result containing 1 row
        ActiveRecord::Result.new ["id"], [[1]]
      end
      Marketing::Email::Template.find(500)
    end
    it 'makes ::where require enabled' do
      expect(Marketing::Email::Template.where(nil).to_sql.gsub("\n"," ")).to match /(WHERE|AND)\s+`marketing_templates`.`enabled` = 1/
    end
  end

end
