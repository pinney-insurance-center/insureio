require 'spec_helper'

describe Marketing::Subscription do
  let(:campaign){      build :marketing_campaign, task_builders:task_builders }
  let(:task_builders){ [] }
  let(:template){      build :marketing_template }
  let(:brand){       build_stubbed :brand}
  let(:person){        build_stubbed :consumer, brand:brand }
  let(:subscription1){ Marketing::Subscription.new person:person, campaign:campaign, queue_num:1}
  let(:subscription2){ Marketing::Subscription.new person:person, campaign:campaign, queue_num:1}

  context "when subscription is first in queue" do
    let(:task){        Task.new origin:subscription1}

    it 'calls #create_tasks after #create' do
      subscription1.should_receive(:create_tasks)
      subscription1.save
    end

    it 'calls #activate_next_in_queue after its own tasks are completed' do
      subscription1.save
      subscription2.save
      #stub all the before_save callbacks
      task.stub_chain(:create_activity, :infer_assigned_to_name, :infer_completed_by_name).and_return(true)
      #stub all the after_save callbacks except the one we want to ensure gets run
      task.stub_chain(:schedule,:publish_to_redis).and_return(true)
      task.save

      task.should_receive(:update_subscription).once.and_call_original
      subscription1.should_receive(:activate_next_in_queue).once.and_call_original

      task.update_attributes(completed_at: Time.now)
    end

    it 'overrides #destroy and calls #dequeue instead' do
      subscription1.save
      subscription2.save

      subscription1.should_receive(:dequeue)
      subscription1.destroy
      person.subscriptions.count.should eq(2)
    end
  end

  context "when subscription is second in queue" do

    it 'does not call #create_tasks after #create' do
      subscription1.save
      subscription2.should_not_receive(:create_tasks)
      subscription2.save
    end

    it '#destroy removes the subscription completely' do
      subscription1.save
      subscription2.save

      subscription2.destroy
      person.subscriptions.count.should eq(1)
    end
  end
 
  describe '#activate_next_in_queue' do
    it "makes the next subscription active and builds the next subscription's tasks" do
      subscription1.save
      subscription2.save

      #not sure why this line always causes a failure. the method is in fact being called.
      #to be revisited later.
      #subscription2.should_receive(:create_tasks)

      subscription1.dequeue

      expect(subscription2.reload.active).to eq(true)
    end
  end

  describe '#create_tasks' do
    let(:task_builders){ build_list :crm_task_builder, 3, template:template, task_type:Enum::TaskType['email'] }
    it 'does not raise error' do
      expect{ subscription1.create_tasks }.to_not raise_error
    end
    it 'calls build on each task_builder' do
      task = Task.new
      task.stub(:save).and_return(true)
      task_builders[0].should receive(:build).and_return(task)
      task_builders[1].should receive(:build).and_return(task)
      task_builders[2].should receive(:build).and_return(task)
      subscription1.create_tasks
    end
  end

end