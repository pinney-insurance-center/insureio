require 'spec_helper'

describe Marketing::Email::Message do

  let(:message     ){ build :marketing_email_message, template:template, sender:agent, target:kase }
  let(:kase        ){ build :crm_case, id:20, agent:agent, consumer:consumer }
  let(:consumer    ){ build :consumer, agent:agent, brand: agent.default_brand }
  let(:template    ){ build :marketing_email_template, body:body }
  let(:smtp_config ){ build :marketing_email_smtp_server }
  let(:agent       ){ build_stubbed :agent, id:1 }
  let(:body        ){'test -- test'}

  before do
    Mail::Message.any_instance.stub(:deliver).and_return(true)
  end

  describe '#custom_data_for_rendering' do
    before do
      message.stub(:case).and_return(kase)
    end
    context 'when template body includes {{ marketech_esign_url }},' do
      let(:body){'test {{ marketech_esign_url }} test'}
      it 'also includes key "marketech_esign_url"' do
        message.stub_chain(:case, :marketech_dataset, :esign_url).and_return('the esign url')
        message.send(:custom_data_for_rendering).should have_key 'marketech_esign_url'
      end
      it 'differs from super (contains marketech_esign_url)' do
        parent_message = message.class.superclass.new message.attributes.reject{|k,v| %w[id type].include?(k)}
        parent_message.sender = message.sender
        message.stub_chain(:case, :marketech_dataset, :esign_url).and_return('the esign url')
        key_diff = message.send(:custom_data_for_rendering).keys - parent_message.send(:custom_data_for_rendering).keys
        key_diff.should include('marketech_esign_url')
      end
    end
    context 'when template body lacks {{ marketech_esign_url }},' do
      it 'also lacks key "marketech_esign_url"' do
        message.send(:custom_data_for_rendering).should_not have_key 'marketech_esign_url'
      end
    end
  end
  describe '#deliver' do
    before do
      SystemMailer.stub(:warning)
      SystemMailer.stub(:error)
      message.stub(:settings).and_return(smtp_config)
      agent.can!(:email)
    end
    context 'with Exception' do
      before do
        message.stub(:render){ raise Exception }
      end
      it 'should reschedule Message' do
        EmailMessageWorker.should_receive :perform_at
        message.deliver
      end
    end
    context 'with Timeout::Error' do
      before do
        message.stub(:render){ raise Timeout::Error }
      end
      it 'should reschedule Message' do
        EmailMessageWorker.should_receive :perform_at
        message.deliver
      end
    end
    context 'with Marketing::SMTPError' do
      before do
        message.stub(:render){ raise Marketing::SMTPError }
      end
      it 'should reschedule Message' do
        EmailMessageWorker.should_receive :perform_at
        message.deliver
      end
    end
    context 'without Exception' do
      it 'should not call #fail' do
        message.should_not_receive :fail
        message.deliver
      end
    end
  end

  #The following two tests are not of this model per se,
  #but are each one-off sort of tests, which effect this model more so than others.
  describe 'ActionMailer::Base' do
    it 'has the minimum default url options set' do
      ActionMailer::Base.default_url_options[:host].should be_present
    end
  end
  describe 'APP_CONFIG' do
    it 'has a valid base url' do
      APP_CONFIG['base_url'].should be_present
      URI.parse(APP_CONFIG['base_url']).host.should be_present
    end
  end
end
