require 'spec_helper'

describe Marketing::Email::Blast do
  let(:custom_blast){build_stubbed :blast_with_custom_list}
  let(:search_blast){build_stubbed :blast_with_search}
  before do
    #
  end



  describe '#deliver' do
    context 'when there are only one batch worth of targets to send to' do
      before do
        custom_blast.stub_chain(:targets,:length).and_return(Marketing::Email::Blast::RECIPIENTS_PER_BATCH)
      end
      it 'calls #send_instant' do
        custom_blast.should_receive(:send_instant).and_return("")
        custom_blast.deliver
      end
    end
    context 'when there are multiple batches worth of targets to send to' do
      before do
        custom_blast.stub_chain(:targets,:length).and_return(Marketing::Email::Blast::RECIPIENTS_PER_BATCH+1)
      end
      it 'calls #send_async' do
        custom_blast.should_receive(:send_async).and_return("")
        custom_blast.deliver
      end
    end
  end

  describe '#targets' do
    context 'when blast has a custom list of recipient emails,' do
      before do
        #stubbing to avoid persisting records to the database
        custom_blast.stub(:scope_possible_matches){|email_str| consumers.select{|c| c.primary_email&.value==email_str} }
      end
      context 'and some email strings are invalid,' do
        let(:consumers){[
          build_stubbed(:consumer, emails:[build(:email_address, value:'foo@bar.com')]),
          build_stubbed(:consumer, emails:[build(:email_address, value:'fly@away.com')])
          ]}
        before do
          custom_blast.recipients='foo@ foo@bar.com fly@away.com'#foo@ is invalid.
        end
        it 'should contain the correct number of targets' do
          custom_blast.targets.length.should eq(2)
        end
        it 'should add the correct warning messages' do
          custom_blast.targets
          custom_blast.warnings.any?{|w| w=~/not valid.+foo@/}.should be(true), custom_blast.warnings.to_s
        end
      end
      context 'and some recipients do not match a consumer or recruit record,' do
        let(:consumers){[
          build_stubbed(:consumer, emails:[build(:email_address, value:'foo@bar.com')]),
          build_stubbed(:consumer, emails:[build(:email_address, value:'foo@bar.com')]),
          build_stubbed(:consumer, emails:[build(:email_address, value:'fly@away.com')])
          ]}
        before do
          #baz@qux.com and lorem@ipsum.co are unmatched. foo@bar.com is ambiguous.
          custom_blast.recipients='foo@bar.com baz@qux.com lorem@ipsum.co fly@away.com'
        end
        it 'should include all matchable targets and no others' do
          targets= custom_blast.targets
          puts "Warnings: #{custom_blast.warnings}" if custom_blast.warnings
          expect( targets ).to     include(consumers[2])
          expect( targets ).not_to include(consumers[0])
          expect( targets ).not_to include(consumers[1])
        end
        it 'should contain the correct number of targets' do
          custom_blast.targets.length.should eq(4), custom_blast.targets.to_s
        end
        it 'should add the correct warning messages' do
          custom_blast.targets
          custom_blast.warnings.any?{|w| w=~/1.+ambiguous/}.should be(true), custom_blast.warnings.to_s
          custom_blast.warnings.any?{|w| w=~/2.+did not match any/}.should be(true), custom_blast.warnings.to_s
        end
      end
    end
    context 'when blast has a search that returns cases' do
      let(:consumers){[
        build_stubbed(:consumer, emails:[build(:email_address, value:'foo@bar.com')]),
        build_stubbed(:consumer, emails:[build(:email_address, value:'fly@away.com')])
        ]}
      let(:cases){[
        build_stubbed(:crm_case, consumer: consumers[0]),
        build_stubbed(:crm_case, consumer: consumers[1]),
        ]}
      before do
        search_blast.stub_chain(:search,:get_matching_record_ids_for_user).and_return([ consumers.map(&:id), cases.map(&:id), [] ])
        allow(Crm::Case).to receive(:where).and_return(cases)
      end
      context 'and some targets are missing an email,' do
        before do
          consumers[0].stub(:primary_email){nil}
        end
        it 'should include all targets with emails and no others' do
          search_blast.targets.should     include(cases[1])
          search_blast.targets.should_not include(cases[0])
        end
        it 'should contain the correct number of targets' do
          search_blast.targets.length.should eq(1)
        end
        it 'should add the correct warning messages' do
          missing_message_warning_pattern=Regexp.new("lack an email address:\\s+#{consumers[0].name}\\s+\\(#{consumers[0].id}\\)")
          search_blast.targets
          search_blast.warnings.any?{|w| w=~missing_message_warning_pattern}.should be(true), search_blast.warnings.to_s
        end
      end
      context 'and all targets have emails,' do
        it 'should contain all targets' do
          search_blast.targets.should include(cases[0])
          search_blast.targets.should include(cases[1])
        end
        it 'should produce no warning messages' do
          search_blast.targets
          search_blast.warnings.should be_empty
        end
      end
    end
  end

  describe '#send_instant' do
    let(:consumers){[
      build_stubbed(:consumer, emails:[build(:email_address, value:'foo@bar.com')]),
      build_stubbed(:consumer, emails:[build(:email_address, value:'fly@away.com')])
    ]}
    before do
      consumers.each{|c| allow( c ).to receive(:update_column).and_return(true) }
      #stubbing to avoid persisting records to the database
      custom_blast.stub(:scope_possible_matches){|email_str| consumers.select{|c| c.primary_email&.value==email_str} }
      custom_blast.recipients='foo@bar.com fly@away.com'
      #If this part fails, it's outside the scope of these particular tests,
      #so better to fail before entering individual test scenarios.
      custom_blast.targets
    end
    context 'when a recipient email address is deemed invalid by the email server (Net::SMTPSyntaxError),' do
      #This should be a very infrequent scenario,
      #since blasts validate email format for custom lists,
      #and the email model validates format also.
      before do
        allow( Mail ).to receive(:new).with( a_hash_including(to:consumers[0].primary_email.value) ){ raise Net::SMTPSyntaxError }
        allow( Mail ).to receive(:new).with( a_hash_including(to:consumers[1].primary_email.value) ).and_call_original
        allow_any_instance_of( Mail::Message ).to receive(:deliver).and_return(true)#no need to actually send
      end
      it 'should still send to other valid emails' do
        user_feedback=custom_blast.send_instant
        user_feedback.should eq("Dispatched emails to 1 of 2 recipients."),
          "Msg to User: #{user_feedback}\nErrors:#{custom_blast.errors.full_messages.join("\n")}\nWarnings: #{custom_blast.warnings}"
      end
      it 'should add the correct error message' do
        custom_blast.send_instant
        invalid_error_pattern=Regexp.new("SMTP Syntax.+#{consumers[0].primary_email.value}")
        custom_blast.errors[:base].any?{|w| w=~invalid_error_pattern}.should be(true), custom_blast.errors[:base].to_s
      end
    end
    context 'when there is a problem authenticating with the email server (Net::SMTPAuthenticationError),' do
      before do
        allow_any_instance_of( Mail::Message ).to receive(:deliver){ raise Net::SMTPAuthenticationError }
      end
      it 'should not attempt to send to further emails' do
        user_feedback=custom_blast.send_instant
        user_feedback.should eq("Dispatched emails to 0 of 2 recipients.")
      end
      it 'should add the correct error message' do
        custom_blast.send_instant
        invalid_error_pattern=Regexp.new("SMTP Authentication Error")
        custom_blast.errors[:base].any?{|w| w=~invalid_error_pattern}.should be(true), custom_blast.errors[:base].to_s
      end
    end
  end

  describe '#send_async' do
    let(:case_count){   rand(0..100) }
    let(:cases){        build_list :crm_case_w_connection, case_count }
    let(:worker_count){ (case_count.to_f/Marketing::Email::Blast::RECIPIENTS_PER_BATCH).ceil }
    before do
      custom_blast.instance_variable_set('@targets',cases)#should be faster running than going through the setter method
    end
    it 'should schedule the proper number of workers' do
      BlastEmailWorker.should_receive(:perform_at).exactly(worker_count).times
      custom_blast.send_async
    end
    it 'should accurately report the number of workers that were scheduled' do
      custom_blast.send_async.should match(/Scheduled #{worker_count} sets/)
    end
  end

  describe '#rerender_body' do
    let(:agent             ){ build_stubbed :user }
    let(:data_for_rendering){ {'agent'=>agent} }
    before do
      custom_blast.body="{{agent.phone}}"
    end
    it 'should render the body using the provided data for rendering' do
      output=custom_blast.rerender_body data_for_rendering, {no_boilerplate:true}
      output.should eq "#{agent.phone.to_s}"
    end
  end
end