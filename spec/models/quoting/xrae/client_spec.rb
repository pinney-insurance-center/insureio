require 'spec_helper'

VCR.configure do |c|
  c.hook_into :excon
end

describe Quoting::Xrae::Client do
	describe 'get_quotes', :vcr do
		let(:consumer){ build :consumer_w_assoc, birth:35.years.ago }
	    let(:quoted_details){ build :quoting_result, face_amount:750000, category_id: 3 }
	    let(:kase){ build :crm_case_w_assoc, consumer:consumer, quoted_details:quoted_details, submitted_details:nil }

	    it 'should return an array of results', :vcr do
	    	client = Quoting::Xrae::Client.new(kase)
	    	results = client.get_quotes
	    	results.should_not be_blank
	    	results.all?{|r| r.is_a? Hash }.should == true
		end

    context 'with diabetes_2' do
      let(:quoted_details){ build :quoting_result, face_amount:100000, category_id: 3 }
      let(:consumer){build :consumer_w_assoc, birth:(36.years.ago), feet:5, inches:9, weight:178}
      before do
        hi = consumer.health_info
        hi.diabetes_2 = true
        (hi.diabetes_type_two_details || hi.build_diabetes_type_two_details).attributes = {
          last_a1_c:8.0, diagnosed_date:4.years.ago, currently_treated:true
        }
      end
      context 'specifying a table rating' do
        it 'should return results only for table' do
          client = Quoting::Xrae::Client.new(kase)
          results = client.get_quotes(Enum::TliHealthClassOption.find(9).name)
          results.length.should eql 3
          results.all?{|r| r.health_class == "Table 2/B"}.should be true
        end
      end
      context 'NOT specifying a table rating' do
        it 'should return results' do
          client = Quoting::Xrae::Client.new(kase)
          results = client.get_quotes()
          results.length.should eql 12
          results.any?{|r| r.health_class == "Table 2/B"}.should be true
          results.any?{|r| r.health_class != "Table 2/B"}.should be true
        end
      end
    end
	end
end