require 'spec_helper'

describe Quoting::Quote do
  subject { Quoting::Quote.new attrs }
  let(:consumer) { create :consumer }
  let(:attrs) { { consumer: consumer, planned_modal_premium: planned_modal_premium, premium_mode: premium_mode } }
  let(:premium_mode) { Enum::PremiumModeOption.find_by_value('QTR') }
  let(:planned_modal_premium) { 1006 }

  describe '#initialize' do
    it 'succeeds' do
      expect(subject.save).to be true
    end
  end

  describe '#must_meet_app_fulfillment_reqs' do
    it 'does not run with validations when status_type_id is 1' do
      subject.save validate: false
      subject.status_type_id = 1
      expect(subject).not_to receive :must_meet_app_fulfillment_reqs
      expect(subject.valid?).to be true
    end
    it 'runs with validations when status_type :submit_to_app_team' do
      subject.save validate: false
      subject.status_type_id = Crm::StatusType::PRESETS[:submit_to_app_team][:id]
      expect(subject).to receive :must_meet_app_fulfillment_reqs
      expect(subject.valid?).to be true
    end
    it 'sets errors for missing requirements' do
      quote = Quoting::Quote.new
      quote.save validate: false
      quote.status_type_id = Crm::StatusType::PRESETS[:submit_to_app_team][:id]
      expect(quote.valid?).to be false
      error = quote.errors[:base].find { |err| err =~ /Before you can submit for processing/ }
      expect(error).to be_present
      expect(error).to match /full name(,|$)/
      expect(error).to match /ssn(,|$)/
      expect(error).to match /driver's license number(,|$)/
      expect(error).to match /driver's license state(,|$)/
      expect(error).to match /birth country(,|$)/
      expect(error).to match /street(,|$)/
      expect(error).to match /city(,|$)/
      expect(error).to match /state(,|$)/
      expect(error).to match /zip(,|$)/
      expect(error).to match /phones(,|$)/
      expect(error).to match /emails(,|$)/
      expect(error).to match /preferred contact method(,|$)/
      expect(error).to match /annual income(,|$)/
      expect(error).to match /net worth specified(,|$)/
      expect(error).to match /relationship to agent(,|$)/
      expect(error).to match /years of agent relationship(,|$)/
      expect(error).to match /beneficiaries(,|$)/
      expect(error).to match /primary beneficiary percentages/
      expect(error).to match /temporary insurance/
    end
  end

  describe '#status_type_id=' do
    it 'creates a new case' do
      subject.save
      type1a = Crm::StatusType.create sales_stage_id: 1
      type1b = Crm::StatusType.create sales_stage_id: 1
      expect {
        subject.status_type_id = type1a.id
        subject.status_type_id = type1b.id
      }.to change(Crm::Case, :count).by(2)
      .and change(Quoting::Quote, :count).by(0)
    end
  end

  describe 'unique sales_stage_id' do
    it 'is enforced per-case' do
      kase_1 = create :crm_case
      kase_2 = create :crm_case
      Quoting::Quote.create! crm_case: kase_1, sales_stage_id: 2
      Quoting::Quote.create! crm_case: kase_2, sales_stage_id: 2
      expect { Quoting::Quote.create! crm_case: kase_1, sales_stage_id: 2 }
      .to raise_exception "Validation failed: Sales stage has already been taken"
    end

    it 'is not enforced when case_id is nil' do
      expect {
        Quoting::Quote.create! consumer: consumer, sales_stage_id: 1
        Quoting::Quote.create! consumer: consumer, sales_stage_id: 1
      }.to change(Quoting::Quote, :count).by 2
    end
  end
end
