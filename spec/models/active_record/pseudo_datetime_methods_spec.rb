require 'spec_helper'

RSpec.configure do |c|
  c.include FactoryBot::Syntax::Methods
end

describe "Crm::Case" do
  let(:kase) { build :crm_case }

  before do#in practice, this would be set by before_filter in ApplicationController
    Time.zone="Hawaii"#10 hours behind UTC (all year long, daylight savings not observed)
  end

  describe "#exam_time" do
    describe "pseudo-setter" do
      it "works for 24-hour string (no am/pm)" do
        kase.exam_time_human = "01/02/1985 14:15"
        kase.exam_time.in_time_zone('UTC').year.should == 1985
        kase.exam_time.in_time_zone('UTC').month.should == 1
        kase.exam_time.in_time_zone('UTC').day.should == 3
        kase.exam_time.in_time_zone('UTC').hour.should == 0
        kase.exam_time.in_time_zone('UTC').min.should == 15
      end
       it "works for 12-hour string" do
        kase.exam_time_human = "01/02/1985 2:15 PM"
        kase.exam_time.in_time_zone('UTC').year.should == 1985
        kase.exam_time.in_time_zone('UTC').month.should == 1
        kase.exam_time.in_time_zone('UTC').day.should == 3
        kase.exam_time.in_time_zone('UTC').hour.should == 0
        kase.exam_time.in_time_zone('UTC').min.should == 15
      end
    end

    describe "pseudo-getter" do
      it "outputs good string" do
        kase.exam_time = Time.utc 1984, 3, 4, 5, 14
        kase.exam_time_human.should == "03/03/1984 07:14 PM"
      end
    end
  end

end