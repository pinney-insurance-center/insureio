require 'spec_helper'
include AuthenticationHelper

describe Consumer do
  let(:agent    ){ authenticated_user }
  let(:client   ){ build :consumer_w_assoc, agent: agent }
  let(:tags_hash){ { a:'x', b:'y', c:'z' } }

  describe '#tags' do
    context 'when never set' do
      before do
        client[:tags] = nil
      end
      it 'returns empty TagSet' do
        expect(client.tags).to be_a Set
        client.save
        expect(client.tags).to be_a Set
        expect(client.tags).to match_array []
      end
    end
    context 'when set' do
      it 'returns TagSet' do
        expect(client.tags).to be_a Set
        client.save
        expect(client.tags).to be_a Set
        expect(client.tags).to match_array %w[foo-bar bazqux]
      end
    end
    context 'when holding legacy (Hash) tags' do
      it 'returns a set of tags' do
        expect(client).to_not receive(:tags=)
        client[:tags] = tags_hash
        client.send :instance_variable_set, :@tags, nil
        expect(client.tags).to be_a Set
        expect(client.tags).to match_array %w[a-x b-y c-z]
      end
    end
  end

  describe '#tags=' do
    it 'makes a set of tags when given legacy (Hash) tags' do
      client.tags = tags_hash
      expect(client.tags).to be_a Set
      expect(client.tags).to match_array %w[a-x b-y c-z]
    end
    it 'sets tags when given an Array of String' do
      expect(client.tags).to match_array %w[foo-bar bazqux]
      client.tags = %w[foo bar baz]
      expect(client.tags.size).to eq 3
      expect(client.tags).to match_array %w[foo bar baz]
    end
    it 'empties tags when given nil' do
      client.tags = %w[foo bar baz]
      expect(client.tags.size).to eq 3
      client.tags = nil
      expect(client.tags.size).to eq 0
      expect(client.tags).to match_array []
    end
  end

  #This association should consistently mirror the content of the serialized field.
  #There may be a better place for these tests.
  #But this works for now since the logic is very tightly coupled.
  #In future we may want to move logic from `TagSet` into `TaggableStereotype`
  #to reflect that it is essentially a single unit.
  describe '#tag_records' do
    context 'for a new consumer' do
      context 'when no tags are set' do
        before do
          client[:tags] = nil
        end
        it 'returns empty array' do
          client.save
          expect( client.tag_records.map(&:value) ).to match_array []
        end
      end
      context 'when 2 tags are set' do
        it 'returns those same tags' do
          client.save
          expect( client.tag_records.map(&:value) ).to match_array %w[foo-bar bazqux]
        end
      end
    end
    context 'for an existing consumer, after updating its tags' do
      context 'with a combination of additions, deletions, and retained existing tags' do
        let(:new_tag_array){ %w[hooplah shenanigans bazqux] }
        before do
          client.save
          client.tags=new_tag_array
          client.save
        end
        it 'returns the updated set of tags' do
          expect( client.tag_records.map(&:value) ).to match_array new_tag_array
        end
      end
    end
  end

end
