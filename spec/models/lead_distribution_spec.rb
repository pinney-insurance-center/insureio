require 'spec_helper'
describe LeadDistribution do

  before do
    Consumer.any_instance.stub(:brand).and_return(brand)
  end

  let(:lead_type)    { 'qrf' }
  let(:state_id)     { 45 }
  let(:user_id)      { 46 }
  let(:kase_id)      { 47 }
  let(:user)      { build :user, id:user_id, can_strict_lead_distribution:true }
  let(:state)     { double Enum::State, id:state_id }
  let(:brand)   { create :brand, name:'Foo', owner_id: user_id, owner:user}
  let(:kase)      { build :crm_case, id:kase_id, agent:nil, agent_id:nil, consumer:client, quoted_details:(build :quoting_quote) }
  let(:client)    { build :consumer, lead_type:lead_type, state_id:state.id, birth:rand(50+20).years.ago, brand:brand, brand_id:brand.id }
  let(:agents)    { Array.new(6){|i| create :user, can_strict_lead_distribution:true } }
  let(:dist)      { LeadDistribution.new(kase) }
  let(:rules)     { build_list :lead_distribution_user_rule, (agents.length - 2), user:(build :user, can_strict_lead_distribution:true) }
  let(:sql)       { LeadDistribution.new(kase).send(:agent_scope).to_sql }
  let(:user_rule) { build :lead_distribution_user_rule, user:user }

  before :each do
    unique_name = FactoryBot.generate :login_sequence
    params = JSON.parse %Q({"consumer":{"birth":"1923-09-19","cases_attributes":[{"active":true,"quoted_details_attributes":{"annual_premium":null,"carrier_name":null,"carrier_health_class":null,"category_name":"To Age 95","face_amount":"50000","health":"PP","monthly_premium":null,"plan_name":null}}],"addresses_attributes":[{"value":null,"type_id":1}],"city":null,"emails_attributes":[{"value":"takingdomlight@yahoo.com"}],"phones_attributes":[{"phone_type_id":1,"value":"2567609270"},{"phone_type_id":2,"value":""}],"state_id":"1","zip":null,"full_name":"#{unique_name} Wardlaw","gender":"Female","health_info_attributes":{"birth":"1923-09-19","feet":null,"inches":null,"tobacco":false,"weight":null},"brand_id":#{brand.id},"source":"InsuranceBlogByChris","lead_type":"HUNTLEYLEADS","referrer":"\/life-insurance-to-age-90-95-100-american-general-prudential\/"}})
    # mimic the workings of clients#create
    # (in which contacts attributes are located directly within the consumer, not within a nested hash)
    @consumer=Consumer.new
    @consumer.attributes = (params['consumer'])
    @consumer.active = true
    @consumer.agent_id = nil # to ensure lead distribution fires
    @consumer.agent    = nil # to ensure lead distribution fires
    allow_any_instance_of(LeadDistribution::Cursor).to receive(:agents).and_return(agents)
  end

  it 'sends no (error) emails' do
    expect(SystemMailer).to_not             receive(:error)
    expect(SystemMailer).to_not             receive(:warning)
    expect(LeadDistribution).to             receive(:new).at_least(1).times.and_call_original
    @consumer.save!
  end

  it 'runs without error, sets case.agent_id & client.agent_id' do
    expect(LeadDistribution).to                         receive(:new).at_least(1).times.and_call_original
    expect{ @consumer.save! }.to_not              raise_exception
    expect(@consumer.cases.first.agent_id).to_not be_blank
    expect(@consumer.reload.agent_id).to_not      be_blank
  end

  it 'does not create extra Contact for new Consumer' do
    LeadDistribution.should_receive(:new).and_call_original
    LeadDistribution.any_instance.stub(:choose).and_return(double id:66, user_id:99, countdown:2, decrement_countdown!:nil, user:user)
    expect{ @consumer.save! }.to change(Contact, :count).by(1)
  end

  context 'when no candidates exist' do
    it 'assigns to brand owner' do
      allow_any_instance_of(LeadDistribution::Cursor).to receive(:agents).and_return(User.where('0<>0'))
      @consumer.save!
      expect(@consumer.agent).to equal(@consumer.brand.owner)
    end
  end

end
