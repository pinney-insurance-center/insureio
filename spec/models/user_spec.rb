require 'spec_helper'
require 'support/data_helper'
RSpec.configure do |config|
  config.include DataHelper
end

viewables = []

describe User do

  it "should respond for all attributes" do
    user = FactoryBot.build(:user)
    [ :agent_of_record_id, :submit_application, :submit_referral, :marketech_signature, :commission_level_id,
      :current_login_at, :current_login_ip, :crypted_password,
      :enabled, :failed_login_count, :last_login_at, :last_login_ip, :last_request_at,
      :commission_level,
      :login_count, :login, :manager_id, :note, :parent_id, :sales_support_field_set_id].
    each do |attr|
      user.should respond_to(attr), "User failed to respond to #{attr}"
    end
    [ :manager, :parent, :sales_support_field_set].
    each do |attr|
      expect(user.association(attr).reflection.macro).to eq :belongs_to
    end
  end

  describe 'agents scope' do
    it 'should return only and all agents' do
      pending; fail
    end
  end

  describe 'licensed scope' do
    it 'should return only and all licensed users for given state' do
      pending; fail
    end
  end

  describe 'having_brand scope' do
    it 'should return only users and all users associated with the given brand ' do
      pending; fail
    end
  end

  describe 'for_premium_limit scope' do
    it 'should return only and all users elligible for premium limit for given lead type' do
      pending; fail
    end
  end

  describe 'creating a minimal example' do
    it 'succeeds for membership_id -1' do
      u = User.new(membership_id: -1, full_name: 'John Doe', emails_attributes: [{value: 'foo@bar.baz'}])
      expect(u.save).to be(true), u.errors.full_messages
      expect(u.active_or_enabled).to be(true)
    end
    it 'succeeds for membership_id 0' do
      u = User.new(membership_id: 0, full_name: 'John Doe', emails_attributes: [{value: 'foo@bar.baz'}])
      expect(u.save).to be(true), u.errors.full_messages
      expect(u.active_or_enabled).to be(true)
    end
    it 'succeeds for membership_id 1' do
      u = User.new(membership_id: 1, full_name: 'John Doe', emails_attributes: [{value: 'foo@bar.baz'}])
      expect(u.save).to be(true), u.errors.full_messages
      expect(u.active_or_enabled).to be(true)
    end
  end
 
  describe 'saving' do
    context 'creates ascendant associations' do
      before :all do
        @gparent = FactoryBot.create :user
        @parent = FactoryBot.create :user, parent:@gparent
      end

      context 'for self' do
        it 'on new record' do
          user = FactoryBot.create :user, parent: @parent
          user.ascendants.should include @parent
          user.ascendants.should include @gparent
        end

        it 'on existing record' do
          user = FactoryBot.build :user, parent_id: nil
          expect(user).to receive(:tenant).and_return(double default_parent_id: nil) # Stub this b/c a parent gets inferred on User creation if none is specified
          user.save
          user.ascendants.should be_empty
          user.update_attributes parent_id:@parent.id
          ascendant_ids = user.ascendants.select(:id).map(&:id)
          ascendant_ids.should include @parent.id
          ascendant_ids.should include @gparent.id
        end
      end

      it 'for children' do
        user = FactoryBot.create :user
        child1 = FactoryBot.create :user, parent:user
        child2 = FactoryBot.create :user, parent:user
        gchild = FactoryBot.create :user, parent:child2
        user.update_attributes parent_id:@parent.id
        pending "user must have parent" if user.parent.nil?
        pending "user must have children" if user.children.empty?
        [child1, child2, gchild].each { |descendant|
          descendant.reload # must reload so that child can get ascendants correctly
          ascendant_ids = descendant.ascendants.select(:id).map(&:id)
          ascendant_ids.should include(user.id), "user #{user.id} not included among ascendants #{ascendant_ids.inspect} for descendant #{descendant.id}"
          ascendant_ids.should include(@parent.id), "parent #{@parent.id} not included among ascendants #{ascendant_ids.inspect} for descendant #{descendant.id}"
          ascendant_ids.should include(@gparent.id), "gparent #{@gparent.id} not included among ascendants #{ascendant_ids.inspect} for descendant #{descendant.id}"
        }
      end

      it 'unless parent_id has not changed' do
        user = FactoryBot.create :user, parent:@parent
        user.should_not_receive(:_update_ascendant_associations)
        user.update_attributes birth:30.years.ago
      end
    end
  end

  describe '#indexable_brands' do
    let(:user){ User.new }
    it 'returns a scope' do
      user.stub(:super?).and_return(false)
      user.stub(:super_view).and_return(false)
      user.indexable_brands.should be_a ActiveRecord::Relation
      user.stub(:super?).and_return(true)
      user.indexable_brands.should be_a ActiveRecord::Relation
    end
  end

  describe '#copy_login_from_email' do
    context 'when a new user is a recruit and has user_account_id' do
      before do
        @user = FactoryBot.build :user
        @login = @user.login
        @email = @user.emails.first.value
        @user.is_recruit=true
        @user.user_account_id = 1
      end
      it 'does not copy email to login' do
        @user.save
        expect(@user.login).to(eql(@login))
      end
    end
    context 'when it is a new user and email is provided' do
      before do
        @user = FactoryBot.build :user
        @email = @user.emails.first.value
      end
      it 'copies email to login field' do
        @user.save
        expect(@user.login).to(eql(@email))
      end
      it 'creates without error a user' do
      end
    end
  end

  describe '#payment_due' do
    let(:user){ build_stubbed :user, payment_due:2.days.ago, stripe_subscription_id:"foobar", stripe_customer_id:"foobar" }
    it 'should update payment_due with API value' do
      Stripe::Customer.stub(:retrieve).and_return(Stripe::Customer)
      Stripe::Customer.stub_chain(:subscriptions,:retrieve,:current_period_end).with(no_args).with("foobar").with(no_args).and_return(Time.now.to_i())
      expect(user).to receive(:save!)
      expect(user.payment_due-1.day).to(eql(Date.today))
    end
  end

  describe '#membership' do
    let(:user){ build :user,
      membership_id: Enum::Membership.id('Basic'),
      payment_due: 1.days.ago
    }
    before do
      user.can! :super_edit, false
    end
    it 'returns 0 when membership expired' do
      user.membership_id.should == 0
    end
    context 'when payment_due is nil' do
      before do
        user.payment_due = nil
      end
      it 'returns non-0 when membership_id < 0' do
        user.membership_id = Enum::Membership.id('Pinney')
        user.membership_id.should_not == 0
      end
      it 'returns 0 when membership_id > 0' do
        user.membership_id = Enum::Membership.id('Basic')
        user.membership_id.should == 0
      end
    end
  end

  describe '#set_default_permissions!' do
    before do
      @user=build :user
    end

    shared_examples_for 'has expected permissions' do |level, extra_perms=[]|
      context "when membership level is #{level}#{extra_perms.present? ? ', and additional permissions are manually set' : ''}" do
        before do
          @user.clear_permissions!
          @user.enable_permissions!(*extra_perms)
          @user.set_default_permissions!
        end
        it "user has all #{level} permissions #{extra_perms.present? ? 'as well as the manually set ones ': ''}and no others" do
          expect(Enum::Membership.all.map(&:name)).to include(level)
          all_perms       =Usage::Permissions::ENUM
          perms_to_include=@user.membership.permissions+extra_perms.map(&:to_s)
          perms_to_exclude=all_perms-perms_to_include
          perms_to_include.each{|p| expect(@user.can?(p)).to be(true), "#{p} #{@user.can?(p)}"}
          perms_to_exclude.each{|p| expect(@user.can?(p)).to be(false), "#{p} #{@user.can?(p)}"}
        end
      end
    end

    it_should_behave_like 'has expected permissions', "Pinney"
    it_should_behave_like 'has expected permissions', "Free"
    it_should_behave_like 'has expected permissions', "Disabled"
    it_should_behave_like 'has expected permissions', "Basic"
    it_should_behave_like 'has expected permissions', "Basic + Marketing"
    it_should_behave_like 'has expected permissions', "Basic + Agency"
    it_should_behave_like 'has expected permissions', "Basic + Agency + Marketing"
    it_should_behave_like 'has expected permissions','Basic',[:have_children,:moderate_unassigned_tasks]
  end

  describe '#update_permissions_based_on_membership_change!' do
    before do
      @user=build :user
      @user.clear_permissions!
      @defaults=lambda{|level_name| Enum::Membership[level_name].permissions}
    end

    shared_examples_for 'permissions change as expected' do |old_level, new_level, extra_perms=[]|
      context "when membership level changes from #{old_level} to #{new_level}#{extra_perms.present? ? ', and additional permissions are manually set' : ''}" do
      before do
          @user.clear_permissions!
          @user.enable_permissions!(*(@defaults.call(old_level)+extra_perms) )
          @user.stub(:membership_id_changed?).and_return(true)
          @user.stub(:membership_id_was     ).and_return(Enum::Membership.id(old_level))
          @user.stub(:membership_id         ).and_return(Enum::Membership.id(new_level))
          @user.update_permissions_based_on_membership_change!
        end
        it "user has all #{new_level} permissions #{extra_perms.present? ? 'as well as the manually set ones ': ''}and no others" do
          all_perms       =Usage::Permissions::ENUM
          perms_to_include=@defaults.call(new_level)+extra_perms.map(&:to_s)
          perms_to_exclude=all_perms-perms_to_include
          perms_to_include.each{|p| expect(@user.can?(p)).to be(true), p}
          perms_to_exclude.each{|p| expect(@user.can?(p)).to be(false), p}
        end
      end
    end

    it 'should be called before update' do
      User._update_callbacks.select{|cb| cb.kind == :before}.map(&:filter).should include :update_permissions_based_on_membership_change!
    end

    it_should_behave_like 'permissions change as expected','Basic + Marketing','Basic',[:have_children,:moderate_unassigned_tasks]
    it_should_behave_like 'permissions change as expected','Basic',       'Basic + Marketing'

    context "when membership level changes from Basic + Marketing to Pinney" do
      before do
        @user.clear_permissions!
        @user.enable_permissions!(*@defaults.call('Basic + Marketing') )
        @user.stub(:membership_id_changed?).and_return(true)
        @user.stub(:membership_id_was).and_return(Enum::Membership.id('Basic + Marketing'))
        @user.stub(:membership_id    ).and_return(Enum::Membership.id('Pinney'))
        @perms_before=@user.permissions
        @user.update_permissions_based_on_membership_change!
      end
      it 'should NOT change permissions' do
        @user.permissions.should == @perms_before
      end
    end
  end

  describe '#update_payment_due!' do
    let(:user){ build :user, payment_due:nil }
    before do
      Timecop.freeze(Time.new 2007, 8, 19)
    end
    after do
      Timecop.return
    end

    context 'when payment_due is set' do
      before do
        user.payment_due = Time.new 2007, 8, 19
      end
      it 'sets payment_due to one month from now' do
        user[:payment_due].should_not be_nil
        user.update_payment_due!
        user[:payment_due].should == Time.now + 1.month
      end
    end
  end

  describe '#update_attributes with key :status_type_id' do
    let(:user){   create :user }
    let(:type1){  create :crm_status_type }
    let(:type2){  create :crm_status_type }
    let(:tasks1){ create_list :crm_task_builder, owner:type1 }
    let(:tasks2){ create_list :crm_task_builder, owner:type2 }
    it 'deactivates old tasks' do
      user.update_attributes(status_type_id:type1.id)
      status1 = user.status
      status1.tasks.each do |t|
        t.active.should == true
      end
      user.update_attributes(status_type_id:type2.id)
      status2 = user.status
      status2.tasks.each do |t|
        t.active.should == true
      end
      status1.tasks.each do |t|
        t.active.should == false
      end
    end
  end

  describe 'adding lead_sources' do
    it 'should be uniq' do
      user = User.new
      user.lead_sources << Crm::Source.find_or_create_by(text: 'foo')
      user.lead_sources << Crm::Source.find_or_create_by(text: 'foo')
      user.lead_sources << Crm::Source.find_or_create_by(text: 'bar')
      user.lead_sources << Crm::Source.find_or_create_by(text: 'foo')
      user.lead_sources.length.should == 2
    end
  end

  describe '#photo_data_url=' do

    let(:sample_data_uri){ #A small red circle with transparent background.
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=='
    }

    before :each do
      allow_any_instance_of(Aws::S3::Client).to receive(:delete_object).and_return(true)#prevent network access
      allow_any_instance_of(Aws::S3::Client).to receive(:put_object   ).and_return(true)#prevent network access
      allow_any_instance_of(Aws::S3::Client).to receive(:get_object   ).and_return(true)#prevent network access
      allow( Attachment).to receive(:create){|args| build_stubbed :attachment, args }
      allow( user ).to receive(:photo_file){#Replace accessor so it doesn't need to access the database.
        user.attachments.select{|a| a.file_name.match(/user_\d+_photo\.\w+/) }.first
      }
    end
    context 'with NO pre-existing photo file' do
      let(:user){ build_stubbed :user }
      it 'creates the attachment record, and triggers file upload' do
        user.assign_attributes photo_data_url: sample_data_uri

        expect_any_instance_of(Attachment).to receive(:upload_file).and_call_original
        expect( user.photo_file ).to be_present
      end
      it 'creates the attachment record with the correct data for relevant fields' do
        user.assign_attributes photo_data_url: sample_data_uri

        expect( user.photo_file ).to be_present
        #Allow the local object to act more like a remote s3 file object.
        allow(  user.photo_file ).to receive(:s3_file).and_return( user.photo_file.file_for_upload )
        allow(  user.photo_file.file_for_upload).to receive(:body).and_return( user.photo_file.file_for_upload )

        original_encoded_data=sample_data_uri.split(',')[1]
        encoded_data_from_attachment=Base64.strict_encode64( user.photo_file.binary_content )
        expect( encoded_data_from_attachment ).to eq( original_encoded_data )
        expect( user.photo_file.content_type ).to eq('image/png')
        expect( user.photo_file.extension ).to eq('png')
      end
    end
    context 'WITH a pre-existing photo file' do
      let(:user){ build_stubbed :user, attachments:[build_stubbed(:attachment)] }
      before :each do
        @old_photo_file=user.attachments.first
        @old_photo_file.file_name="user_#{user.id}_photo.old"
        allow( @old_photo_file ).to receive(:destroy){ user.attachments=[] }#prevent db access
      end

      it 'replaces the old record with the new record' do
        user.assign_attributes photo_data_url: sample_data_uri

        expect( user.attachments ).not_to include( @old_photo_file )

        new_photo_file= user.photo_file
        expect( new_photo_file ).to be_present
        expect( new_photo_file ).not_to eq( @old_photo_file )
      end
    end
  end

  shared_examples_for "short-circuited" do
    it 'exits callback without copying anything' do
      new_obj.stub(:new_record?) { false }
      new_obj_before_callback=new_obj
      new_obj.should eq(new_obj_before_callback)
      new_obj.valid?
    end
  end

  shared_examples_for "completed" do#requires src_obj, new_obj
    it 'populates whitelisted source object values onto the new object' do
      new_obj.birth_or_trust_date = nil
      src_obj.birth_or_trust_date = "10/10/1965"
      new_obj.valid?
      new_obj.birth_or_trust_date.should eq(src_obj.birth_or_trust_date)
    end
  end

  describe '#build_user_from_recruit' do
    let(:new_obj){ build :user }
    context 'before validation' do
      it 'it gets called for the user' do
        new_obj.should_receive(:build_user_from_recruit)
        new_obj.valid?
      end
    end
    context 'when user being saved is not new,' do
      before(:each){ new_obj.stub(:new_record?) { false } }
      it_should_behave_like "short-circuited"
    end
    context 'when user being saved has no recruits,' do
      before(:each){ new_obj.stub(:recruits) { [] } }
      it_should_behave_like "short-circuited"
    end
    context 'when user is new and has a recruit' do
      let(:src_obj){ build :user, is_recruit:true }
      let(:new_obj){ build :user, recruits:[src_obj] }
      before(:each) do
        src_obj.stub(:update_column)
        src_obj.stub(:reload){ src_obj }
      end
      it_should_behave_like "completed"
      it 'stores the recruit in an instance variable, so the user_account_id can be added to the recruit after save' do
        new_obj.valid?
        new_obj.instance_variable_get('@built_from_recruit').should eq(src_obj)
      end
      context 'and recruit provides a login,' do
        it 'the new user validates' do
          src_obj.login = 'superunique@login.value'
          new_obj.valid?.should eq(true), new_obj.errors.full_messages
        end
      end
      
    end

  end

  describe 'after creation of a user from a recruit' do
    let(:recruit){ build :user, is_recruit:true, id:User.maximum(:id).to_i+1 }
    let(:user){    build :user, recruits:[recruit], id:User.maximum(:id).to_i+2 }
    before(:each) do
      recruit.stub(:update_column)
      recruit.stub(:reload){ recruit }
      user.stub(:build_user_from_recruit){@built_from_recruit=recruit}
    end
    it '#save_association_on_recruit gets called for the user' do
      user._commit_callbacks.select { |cb| cb.kind.eql?(:after) }.map(&:raw_filter).include?(:save_association_on_recruit).should == true
    end
    it '#reassociate_l_and_c_from_recruit_to_user gets called for the user' do
      user._commit_callbacks.select { |cb| cb.kind.eql?(:after) }.map(&:raw_filter).include?(:reassociate_l_and_c_from_recruit_to_user).should == true
    end
    context 'in after_commit callbacks' do
      before(:all) {
        FactoryBot.create :user
      } # This is a kludge because this old version of Rails doesn't makes it otherwise impossible to trigger commit callbacks in specs
      it 'creates a NewUserWorker object, for notifying project heads that a user was created' do
        expect(NewUserWorker.jobs.size).to eq 1 # this expectation is based on the creation of the User in the before(:all) callback
      end
    end
  end

  describe '#build_recruit_from_user' do
    let(:new_obj){ build :user }
    context 'before validation' do
      it 'it gets called for the user' do
        new_obj.should_receive(:build_recruit_from_user)
        new_obj.valid?
      end
    end
    context 'when user being saved is not new,' do
      before(:each){ new_obj.stub(:new_record?){ false } }
      it_should_behave_like "short-circuited"
    end
    context 'when user being saved has no user_account,' do
      before(:each){ new_obj.stub(:user_account_id){ nil } }
      it_should_behave_like "short-circuited"
    end
    context 'when user is new and has a user_account' do
      let(:src_obj){ build :user }
      let(:new_obj){ build :user, is_recruit:true, user_account: src_obj }
      it_should_behave_like "completed"
    end
  end

  describe 'after creation of a recruit from a user' do
    let(:recruit){ build :user, is_recruit:true, id:User.maximum(:id).to_i+1 }
    let(:user){    build :user, recruits:[recruit], id:User.maximum(:id).to_i+2 }
    it "does NOT create a NewUserWorker object, because project heads don't need to know when a recruit is created" do
      User.any_instance.stub(:save).and_return(true)
      expect{recruit.run_callbacks(:create)}.to change(NewUserWorker.jobs, :size).by(0)
    end
  end

  describe '#misc_permissions_changed?' do
    subject{User.new}
    context 'when nothing is changed' do
      it 'returns false' do
        expect(subject.misc_permissions_changed?).to be false
      end
    end
    context 'when :lead_distribution is changed' do
      it 'returns false' do
        subject.can! :lead_distribution, true
        expect(subject.misc_permissions_changed?).to be false
        subject.can! :lead_distribution, false
        expect(subject.misc_permissions_changed?).to be false
      end
    end
    context 'when :super_edit is changed' do
      it 'returns true' do
        subject.can! :super_edit, true
        expect(subject.misc_permissions_changed?).to be true
        subject.can! :super_edit, false
        expect(subject.misc_permissions_changed?).to be false
      end
    end
  end

  #Note: Attribute +enabled+ is delegated to the Contact record attribute +active_or_enabled+.
  describe '#delete' do
    let(:u){create :user}
    it 'should set enabled to false' do
      u.delete
      u.enabled.should == false
    end
  end
  describe '#destroy' do
    let(:u){create :user}
    it 'should set enabled to false' do
      u.destroy
      u.enabled.should == false
    end
    it 'should eliminate LeadDistribution::UserRules' do
      create :lead_distribution_user_rule, user: u
      expect { u.destroy }.to change{ LeadDistribution::UserRule.where(user_id:u.id).count }.from(1).to(0)
    end
  end
  describe '.delete_all' do
    it 'should raise error' do
      expect{User.delete_all}.to raise_error("Not allowed for paranoid class")
    end
  end
  describe '.destroy_all' do
    it 'should raise error' do
      expect{User.destroy_all}.to raise_error("Not allowed for paranoid class")
    end
  end
end
