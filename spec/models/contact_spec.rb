require 'spec_helper'

RSpec.configure do |c|
  c.include FactoryBot::Syntax::Methods
end

shared_examples_for "has ContactableStereotype methods" do
  it 'should respond to contactable methods' do
    cs_methods=[
      :full_name, :entity_type, :tenant,
      :phones, :addresses, :emails, :preferred_contact_method,
      :primary_phone, :primary_address, :primary_email
    ]
    cs_methods.each do |m|
      expect(obj).to respond_to m
    end
  end

end

describe User do
  let (:obj) { build :user_w_assoc }
  it_should_behave_like "has ContactableStereotype methods"
end

describe Consumer do
  let(:agent){ build_stubbed :user }

  context 'when a new record is instantiated with minimal data' do
    let (:obj) { build :consumer }
    it_should_behave_like "has ContactableStereotype methods"
  end

  context 'with no prior setup' do
    it 'should add one row to the database table upon create with minimal data' do
      expect{ create :consumer, agent: agent }.to change(Consumer, :count).by(1)
    end
  end

  context 'when a new record is instantiated from typical JSON w many associations' do
    before do
      json_str=%Q(
        {
          "agent_id": #{agent.id},
          "consumer": {
            "brand_id": #{agent.default_brand_id},
            "birth_or_trust_date": "1958-03-05",
            "phones_attributes": [{"value": "9991119999"}],
            "emails_attributes": [{"value": "nwest@pinneyinsurance.com"}],
            "addresses_attributes": [{"street": "123 Main St.","city":"City","state_id":5,"zip": "95816"}],
            "tags": [
              "source_id - Tier 1",
              "source_agency - tier 1 - /",
              "source_user"
            ],
            "source":"TrustedQuote",
            "lead_type":"TRQ",
            "gender": "Male",
            "full_name": "Nic9a Testingtrq13",
            "health_info_attributes": {
              "weight": 222,
              "inches": 0,
              "feet": 6
            },
            "cases_attributes": [
              {
                "quoting_details_attributes": [
                  {
                    "sales_stage_id": 1,
                    "category_name": "10 Year",
                    "product_type_name": "Term",
                    "annual_premium": "1632.5",
                    "face_amount": "750000",
                    "health": "Preferred Plus",
                    "premium_mode_name": "Monthly",
                    "carrier_name": "Genworth",
                    "planned_modal_premium": "142.84"
                  },
                  {
                    "sales_stage_id": 3,
                    "product_type_name": "Term",
                    "annual_premium": "1632.5",
                    "face_amount": "750000",
                    "health": "Preferred Plus",
                    "premium_mode_name": "Monthly",
                    "carrier_name": "Genworth",
                    "planned_modal_premium": "142.84"
                  }
                ],
                "product_type_name": "Term"
              }
            ]
          }
        }
      )
      data = JSON.parse json_str
      @consumer = Consumer.new data['consumer']
      @consumer.brand = agent.default_brand
      Crm::Case.any_instance.stub(:auto_assign_to_agent!)
    end
    it 'should add one row to the database table upon save' do
      expect{ @consumer.save }.to change(Consumer, :count).by(1)
    end
  end

end