require 'spec_helper'

describe OwnedStereotype do
  let(:parent) { create :user }
  let(:user) { create :user, parent: parent }
  let(:child) { create :user, parent: user }

  describe 'editable_by' do

    it 'scopes editables for non-admin' do
      user.can! :edit_descendants
      st_for_parent = create :crm_status_type, owner: parent
      st_for_user = create :crm_status_type, owner: user
      st_for_child = create :crm_status_type, owner: child
      expect(Crm::StatusType.include? OwnedStereotype).to be true
      expect(Crm::StatusType.editable_by(user).map &:id).to match_array [st_for_user.id, st_for_child.id]
    end
  end
end