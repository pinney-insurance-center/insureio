require 'spec_helper'

describe Tag do
  let(:consumer) { create :consumer }
  let(:user) { create :user }
  let(:existing_tag) { Tag.create value: 'pre-existing' }

  before do
    existing_tag
  end

  describe '#owner' do
    it 'gets a User' do
      tag = Tag.create person: user, owner_id: user.id, value: 'new'
      expect(tag.reload.owner).to be_a User
    end
  end

  describe '#person' do
    it 'can be a User' do
      tag = Tag.create person: user, value: 'new'
      expect(tag.reload.person).to be_a User
    end
    it 'can be a Consumer' do
      tag = Tag.create person: consumer, value: 'new'
      expect(tag.reload.person).to be_a Consumer
    end
  end

  it 'enforces uniqueness' do
    tag1 = Tag.create person: user, value: 'foo'
    expect(tag1.persisted?).to be true
    tag2 = Tag.create person: user, value: 'foo'
    expect(tag2.persisted?).to be false
    expect(tag2.errors[:value]).to include('has already been taken')
  end
end
