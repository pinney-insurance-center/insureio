require 'spec_helper'

describe '#error' do
  context 'with a String and a Hash' do
    it 'does not raise an error' do
      expect{SystemMailer.error "this is a string", debug:build(:user), recipients:'me@foo.bar'}.to_not raise_error
    end
  end

  context 'with an Exception' do
    it 'does not raise an error' do
      expect{SystemMailer.error ArgumentError.new}.to_not raise_error
    end
  end
end