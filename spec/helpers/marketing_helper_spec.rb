require 'spec_helper'

describe MarketingHelper do
  subject { create :marketing_email_message, target: kase, sender: cons.agent }
  let(:kase) { create :crm_case, consumer: cons }
  let(:cons) { create :consumer }

  before do
    include MarketingHelper
  end

  shared_examples_for 'email links' do |fn_name|
    it 'includes essential params' do
      html = send fn_name, subject
      expect(html).to match /key=#{cons.agent.quoter_key}/
      access_code = URI.encode_www_form_component subject.sender.access_code(subject.person)
      expect(html).to match /access_code=#{access_code}/
      expect(html).to match /message_id=#{subject.id}/
      expect(html).to match /person_id=#{cons.id}/
      encoded_email = URI.encode_www_form_component subject.recipient
      expect(html).to match /recipient=#{encoded_email}/
    end
  end

  describe '#unsubscribe_link' do
    include_examples 'email links', :unsubscribe_link
  end

  describe '#message_public_view_link' do
    include_examples 'email links', :message_public_view_link
  end
end
