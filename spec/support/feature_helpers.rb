module FeatureHelpers

  def current_user
    @current_user ||= User..last
  end

  css_selectors={
    navbar_toggle:'#wrapper > header > .toggle-container > a.toggle',
    navbar_links: 'nav > #main-menu-inner > ul.navigation',
    bulletins:    '.wrapper > .bulletin-container',
    main_scope:   '#main-container [ng-controller]',
  }

end