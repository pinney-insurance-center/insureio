class QueryCounter

  def initialize
    @queries = []
    @query_count = 0
  end

  IGNORED_SQL = [/^PRAGMA (?!(table_info))/, /^SELECT currval/, /^SELECT CAST/, /^SELECT @@IDENTITY/, /^SELECT @@ROWCOUNT/, /^SAVEPOINT/, /^ROLLBACK TO SAVEPOINT/, /^RELEASE SAVEPOINT/, /^SHOW max_identifier_length/]

  attr_reader :queries
  attr_reader :query_count

  def call(name, start, finish, message_id, values)
    # FIXME: this seems bad. we should probably have a better way to indicate
    # the query was cached
    unless 'CACHE' == values[:name]
      @query_count += 1 unless IGNORED_SQL.any? { |r| values[:sql] =~ r }
      @queries << values[:sql]
    end

  end
end
