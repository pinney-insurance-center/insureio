module AuthenticationHelper

  def authenticated_user
    @authenticated_user ||= FactoryBot.build(
      :user,
      enabled: true,
      membership_id: 2,
      tenant_id:0,
      payment_due: 2.days.from_now,
      stripe_customer_id: 'xxxxxxxx',
      stripe_subscription_id: 'xxxxxxxx',
      tos: true
    ).tap { |u| u.save validation: false }
  end
  alias_method :current_user, :authenticated_user

  def authentication_stubs(user=nil, current_user_auth=:session)
    @authenticated_user = user if user
    Thread.current[:current_user_auth] = current_user_auth
    controller.instance_variable_set :@current_user, (user||authenticated_user)
    allow(controller).to receive(:check_security_question_answered).and_return(true)
  end

  def authentication_unstub
    allow(controller).to receive(:current_user).and_call_original
    controller.instance_variable_set :@current_user, nil
    controller.session.clear
  end

  # For request specs, don't try `authentication_stubs`, since no controller
  # instance is available.
  def create_login_session host="www.example.com"
    host! host
    creds = { login: authenticated_user.login, password: authenticated_user.password }
    post '/user_sessions.json', format: :json, usage_user_session: creds
    cookies # `cookies` is defined automatically in request specs
  end
end