# This module is used with web mocking to save web responses in the code so that the web
# request need not be run except when the test is first built.
VCR.configure do |c|
  c.cassette_library_dir = 'spec/vcr'
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.default_cassette_options = { :record => :new_episodes }
end