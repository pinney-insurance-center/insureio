require 'spec_helper'

RSpec::configure do |config|
  config.include FactoryBot::Syntax::Methods
end

shared_examples_for "privilege" do
  
  context "with only" do
    before do; agent.permissions = 0; end

    context "view" do
      before do; agent.send("can_strict_view_#{privilege}_resources=", true); end
      it "#viewable?" do; obj.viewable?(agent).should == identity; end
      it "::viewable_by" do; pending; obj.class.viewable_by(agent).include?(obj).should == identity; end
    end

    context "edit" do
      before do; agent.send("can_strict_edit_#{privilege}_resources=", true); end
      it "#editable?" do; obj.editable?(agent).should == identity; end
      it "::editable_by" do; pending; obj.class.editable_by(agent).include?(obj).should == identity; end
    end
  end

  context "without only" do
    before do; agent.permissions = 15,15,15,15; end

    context "view" do
      before do; agent.send("can_strict_view_#{privilege}_resources=", false); end
      it "#viewable?" do; obj.viewable?(agent).should == negative_identity; end
      it "::viewable" do; pending; obj.class.viewable_by(agent).include?(obj).should == negative_identity; end
    end

    context "edit" do
      before do
        agent.send("can_strict_edit_#{privilege}_resources=", false)
      end
      it "#editable?" do; obj.editable?(agent).should == negative_identity; end
      it "::editable" do; pending; obj.class.editable_by(agent).include?(obj).should == negative_identity; end
    end
  end

end

shared_examples_for "accessible" do
  
  context "descendant privilege:" do
    let(:identity) { self? || descendant? }
    let(:negative_identity) { self? || nephew? || sibling? }
    let(:privilege) { :descendants }
    it_should_behave_like "privilege"
  end

  context "sibling privilege:" do
    let(:identity) { self? || sibling? }
    let(:negative_identity) { self? || descendant? || nephew? }
    let(:privilege) { :siblings }
    it_should_behave_like "privilege"
  end

  context "nephew privilege:" do
    let(:identity) { self? || nephew? }
    let(:negative_identity) { self? || sibling? || descendant? }
    let(:privilege) { :nephews }
    it_should_behave_like "privilege"
  end

end

shared_examples_for "Crm::Accessible" do
  # define users
  before :all do
    @gparent = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:nil
    @parent = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@gparent
    @agent = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@parent
    @unrelated_user = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:nil
    @child = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@agent
    @gchild = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@child
    @sibling = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@parent
    @nephew = create :user, role_id:Enum::UsageRole::AGENT_ID, parent:@sibling
  end
  # define qualities of user who owns the object
  let(:self?){false} # whether the assigned agent == agent
  let(:descendant?){false} # whether the assigned agent is a descendant of agent
  let(:sibling?){false} # whether the assigned agent is a sibling of agent
  let(:nephew?){false} # whether the assigned agent is a nephew of agent
  let(:agent){@agent}
  let(:parent){@parent}
  let(:gparent){@gparent}
  let(:child){@child}
  let(:gchild){@gchild}
  let(:sibling){@sibling}
  let(:nephew){@nephew}
  let(:unrelated_user){@unrelated_user}

  context "for agent's lead" do
    let(:obj) { create :crm_case, agent:agent }
    let(:self?){true}
  end

  context "for parent's lead" do
    let(:obj) { create :crm_case, agent:parent }
  end

  context "for grandparent's lead" do
    let(:obj) { create :crm_case, agent:gparent }
  end

  context "for child's lead" do
    let(:obj) { create :crm_case, agent:child }
    let(:descendant?){true}
  end

  context "for grandchild's lead" do
    let(:obj) { create :crm_case, agent:gchild }
    let(:descendant?){true}
  end

  context "for sibling's lead" do
    let(:obj) { create :crm_case, agent:sibling }
    let(:sibling?){true}
  end

  context "for nephew's lead" do
    let(:obj) { create :crm_case, agent:nephew }
    let(:nephew?){true}
  end

  context "for unrelated user's lead" do
    let(:obj) { create :crm_case, agent:unrelated_user }
  end

end

