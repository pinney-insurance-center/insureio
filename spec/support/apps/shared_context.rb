XLS_FILEPATH = 'spec/support/apps/Pinney Detailed Test Case Spreadsheet for E2E v.2.xls'
require 'spreadsheet'

shared_context 'shared APPS context' do

  let(:start_time   ){ Time.new(2014, 12, 30, 10, 40, 0, 0) }
  let(:builder      ){ Timecop.freeze( start_time ){ Processing::Apps::XmlBuilder.new kase } }
  let(:spouse       ){ build_stubbed :consumer, full_name:'Grace Chapman',
                          birth_or_trust_date:Date.new(1966,9,10),
                          ssn:'7272457270',
                          gender:FEMALE,
                          financial_info:build_stubbed(:crm_financial_info_mostly_blank)
                     }
  let(:relationship0){ build_stubbed :crm_consumer_relationship,
                        relationship_type:Enum::RelationshipType['spouse/domestic partner'],
                        stakeholder: spouse
                     }
  let(:consumer     ){ build_stubbed :consumer_w_assoc,
                      full_name:'John A Doe',
                      suffix:'Jr',
                      occupation:'Programmer',
                      company:'Auto Office Services',
                      marital_status:Enum::MaritalStatus['Married'],
                      financial_info:financial,
                      health_info:health_info,
                      gender:MALE,
                      ssn:'123456794',
                      birth_or_trust_date:Date.new(1980,2,28),
                      birth_country_id:Enum::Country.id('Brazil'),
                      dln:'D97843',
                      dl_state:Enum::State['CT'],
                      birth_state_id:nil,
                      citizenship:Enum::Citizenship['US Citizen'],
                      relationships: [relationship0],
                      addresses:[ build_stubbed(:address, address_type_id:Enum::AddressType.id('home'), street:'1930 Guinevere St', city:'Springfield', state:Enum::State['IL'], zip:'62713') ],
                      phones:[ build_stubbed(:phone, value:'2173456794', phone_type:Enum::PhoneType['home']) ],
                      emails:[ build_stubbed(:email, value:'JohnIroc@hotmail.com') ]
                    }
  let(:financial   ){ build_stubbed :crm_financial_info_mostly_blank, asset_earned_income:50_000, bankruptcy:true, bankruptcy_declared:Date.new(1999,1,1) }
  let(:health_info ){ build_stubbed :crm_health_info, foreign_travel:true, tobacco_cigarette_last: Date.new( 2000,12,31) }
  let(:kase        ){ build_stubbed :crm_case,
                        agent:agent,
                        agent_of_record:agent_of_record,
                        quoting_details:[quote],
                        id:8675309,
                        consumer:consumer,
                        stakeholder_relationships:beneficiaries,
                        apps_id_offset:55#renders to apps_id YTP1889803
                    }
  let(:quote       ){ build_stubbed :quoting_quote,
                        product_type_id:1,
                        health:'P',
                        duration_name:'15 Years',
                        face_amount:250_000,
                        plan_name:"AG Select-a-Term",
                        carrier:Carrier.where('name like "American General%"').first,
                        premium_mode_id: Enum::PremiumModeOption.id('Annually'),
                        planned_modal_premium:3_557,
                        monthly_premium:746,
                        annual_premium:3_557
                      }
  let(:beneficiaries){[
    build(:bene_relationship, contingent:false, percentage:50, stakeholder_attributes:{genre_id:1, name:'Jane B Doe', ssn:'777889999', birth_or_trust_date:Date.new(2009,8,29)}),
    build(:bene_relationship, contingent:false, percentage:30, stakeholder_attributes:{genre_id:2, name:'Legal Trust for Children', ssn:'123121234', birth_or_trust_date:Date.new(2007,10,2)}),
    build(:bene_relationship, contingent:false, percentage:20, stakeholder_attributes:{genre_id:3, name:'Some Company', ssn:'555664444', birth_or_trust_date:Date.new(2006,8,1)})
  ]}
  let(:related_consumers){[]}
  let(:agent){ build_stubbed :user, full_name:'Testy B Testing', birth_or_trust_date:'1970-12-01', emails:[build_stubbed(:email_address,value:'awilson1@yabox.net')] }
  let(:agent_of_record){ build_stubbed :user, id:jan_id, login:'jpinney', full_name:'Russel Jan Pinney', birth_or_trust_date:nil }
  let(:jan_id){ 283 } # random. just needs to differ from agent's id
  let(:existing_cov0){ build_stubbed :crm_case, id:78, effective_date:'2014-01-18',
                          current_details: build_stubbed(:quoting_quote, product_type_id:1, face_amount: 150_000,
                            carrier: build_stubbed(:carrier, id:13, name:'CarrierEx', naic_code:'89876'))
                      }
  let(:existing_cov1){ build_stubbed :crm_case, id:67, effective_date:'2014-12-18', replaced_by_id: 789_789,
                          current_details: build_stubbed(:quoting_quote, product_type_id:1, face_amount: 600_000,
                            carrier: build_stubbed(:carrier, id:11, name:'CarrierWy', naic_code:'67890'))
                      }

  before do
    Timecop.freeze(Time.local(2014,12,30))
    #Substitute the implementation so changes do not attempt to hit the database.
    allow(kase).to receive(:update_attributes){|attrs| kase.assign_attributes(attrs) }

    kase.stub(:existing_coverages).and_return( [existing_cov0, existing_cov1] )
    consumer.stub(:spouse).and_return(spouse)

    quote.crm_case=kase
  end

  after do
    Timecop.return
  end

end

shared_context 'base apps context' do
  let(:builder)             { Timecop.freeze( start_time ){ Processing::Apps::XmlBuilder.new kase } }
  let(:consumer)            { build_stubbed :consumer,
                                agent:agent,
                                name:name,
                                suffix:nil,
                                gender:gender,
                                birth_country_custom_name:nil,
                                birth_country_id:birth_country_id,
                                birth_state:birth_state,
                                birth_or_trust_date:birth_or_trust_date,
                                ssn:ssn,
                                dln:dln,
                                dl_state:dl_state,
                                addresses:addresses,
                                emails:emails,
                                phones:phones,
                                company:employer,
                                occupation:duties,
                                financial_info:financial_info,
                                health_info:health_info,
                                cases:existing_coverages,
                                citizenship:citizenship,
                                critical_note:critical_note,
                                priority_note:priority_note
                            }
  let(:citizenship)         { Enum::Citizenship['US Citizen'] }
  let(:financial_info)      { build_stubbed :crm_financial_info_mostly_blank,
                                spouse_income:spouse_income,
                                asset_earned_income:earned_income,
                                bankruptcy:bankruptcy,
                                bankruptcy_declared:bankruptcy_declared,
                                bankruptcy_discharged:bankruptcy_discharged,
                                net_worth_specified:net_worth
                            }
  let(:health_info)         { build_stubbed :crm_health_info,
                                moving_violation_last_dl_suspension: (12.years.ago),
                                moving_violation_last_2_yr: moving_violation_last_2_yr,
                                tobacco_cigarette_last:tobacco_cigarette_last,
                                tobacco_cigar_last:tobacco_cigar_last,
                                hazardous_avocation:hazardous_avocation,
                                criminal:criminal,
                                tobacco_chewed_last:last_tobacco_chewed,
                                tobacco_nicotine_patch_or_gum_last:last_nicotine_patch_or_gum,
                                foreign_travel:true
                            }
  let(:kase)                { build_stubbed :crm_case,
                                id:411,
                                consumer:consumer,
                                current_details:a_team_details,
                                agent:agent,
                                agent_of_record:agent_of_record,
                                stakeholder_relationships:beneficiaries,
                                created_at: kase_created_at
                            }
  let(:a_team_details)      { build_stubbed :quoting_quote,
                                health_class_id:health_class_id,
                                product_type:Enum::ProductType['Term'],
                                carrier:Carrier.find_by_name('American General Life Insurance Company'),
                                plan_name:product_name,
                                duration:duration,
                                face_amount:face_amount,
                                premium_mode_id:premium_mode_id,
                                planned_modal_premium:planned_modal_premium,
                                annual_premium:annual_premium,
                                monthly_premium:monthly_premium,
                                quarterly_premium:quarterly_premium
                            }
  let(:planned_modal_premium){947}
  let(:annual_premium)      {nil}
  let(:monthly_premium)     {nil}
  let(:quarterly_premium)   {nil}
  let(:agent)               { build_stubbed :user }
  let(:agent_of_record)     { build_stubbed :user, id:5555}
  let(:phones)              {[
                                build_stubbed(:blank_phone, value:phone1, phone_type:Enum::PhoneType['home']),
                                build_stubbed(:blank_phone, value:phone2)
                            ]}
  let(:emails)              { [ build_stubbed(:email_address, value:email) ] }
  let(:spouse_income)       { (household_income.to_i - earned_income.to_i) }
  let(:asset_checking)      { (net_worth.to_i - earned_income.to_i - spouse_income.to_i) }
  let(:duration)            { Enum::TliDurationOption.ewhere( years: duration_in_years ).first }
  let(:product_name)        {'AG Select-A-Term'}
  let(:earned_income)       { 0 }
  let(:net_worth)           { nil }
  let(:existing_coverages)  { [] }
  let(:employer)            {nil}
  let(:duties)              {nil}
  let(:hazardous_avocation) {false}
  let(:moving_violation_last_2_yr){0}
  let(:criminal)            {false}
  let(:foreign_travel)      {false}
  let(:tobacco_cigarette_last){nil}
  let(:tobacco_cigar_last)  {nil}
  let(:last_tobacco_chewed) {nil}
  let(:last_nicotine_patch_or_gum){nil}
  let(:health_class_id)     {1}
  let(:bankruptcy)          {nil}
  let(:bankruptcy_declared) {nil}
  let(:bankruptcy_discharged){nil}
  let(:waiver_of_premium)   {false}
  let(:birth_state)         {nil}
  let(:dln)                 {nil}
  let(:dl_state)            {nil}
  let(:critical_note)       {nil}
  let(:priority_note)       {nil}
  let(:agent_phone)         {nil}

  before do
    Timecop.freeze(Time.local(2014))
    #Substitute the implementation so changes do not attempt to hit the database.
    allow(kase).to receive(:update_attributes){|attrs| kase.assign_attributes(attrs) }

    kase.stub(:apps_id).and_return(apps_id)
    kase.stub(:existing_coverages).and_return(existing_coverages)
    builder.stub(:waiver_of_premium?).and_return(waiver_of_premium)
    builder.stub(:carrier_code){ |car,request_type| car.code }
    consumer.cases.each do |cov|
      cov.replaced_by = kase if cov.replaced_by_id == 0
    end
    agent.phones = [Phone.new(value:agent_phone)]
    # Producer code
    if agent_number.present?
      agent.stub_chain(:contracts, :where, :first).and_return(build :usage_contract, carrier_contract_id:agent_number)
    end
    a_team_details.crm_case=kase
  end

  after do
    Timecop.return
  end

end

shared_context 'Case 1' do
  include_context 'base apps context'
  let(:start_time)                { Time.new 2014, 9,24, 15,52,42, 0 }
  let(:kase_created_at)           { Time.new 2014, 9,24, 15,52,42, 0 }
  let(:name)                      {'ADAM A SNMMMMTESTCASEAA'}
  let(:gender)                    {MALE}
  let(:birth_country_id)          {nil}
  let(:birth_state)               {Enum::State.find_by_abbrev('CA')}
  let(:birth_or_trust_date)       {Date.new 1981,12,26}
  let(:ssn)                       {'854654789'}
  let(:dln)                       {'T12345'}
  let(:dl_state)                  {Enum::State.find_by_abbrev('CA')}
  let(:addresses)                 {[Address.new(street:'123 Main Ave', city:'Scramento', state:Enum::State['CA'], zip:'94207')] }
  let(:phone1)                    {'555-555-5555'}
  let(:phone2)                    {'777-777-7777'}
  let(:email)                     {'chrisanne-m.pascua@aig.com'}
  let(:household_income)          {15000}
  let(:net_worth)                 {20000}
  let(:tobacco_cigarette_last)    {Date.new 2014,5,1}
  let(:hazardous_avocation)       {true}
  let(:moving_violation_last_2_yr){1}
  let(:bankruptcy)                {true}
  let(:bankruptcy_declared)       {Date.new 2007,1,25}
  let(:criminal)                  {true}
  let(:foreign_travel)            {true}
  let(:apps_id)                   {'YTPCASE103'}
  let(:duration_in_years)         {10}
  let(:face_amount)               {350000}
  let(:health_class_id)           {9}
  let(:waiver_of_premium)         {true}
  let(:existing_coverages)        {[kase_a, kase_b, kase_c] }
  let(:kase_a)                    {existing_coverage 'CA56789', 'MET LF-STATE ST FUNDS', 'Individual', 200000, '11/18/1995', true }
  let(:kase_b)                    {existing_coverage 'CA12345', 'UNIVERSAL PRESIDENTIAL LF', 'Individual', 300000, '12/2/1998', true }
  let(:kase_c)                    {existing_coverage 'MM002359', 'PHOENIX LF', 'Individual', 500000, '12/2/2005', false }
  let(:beneficiaries)             {[bene_a]}
  let(:bene_a)                    {build :bene_relationship, percentage:'34', contingent:false, relationship_type:Enum::RelationshipType.find_by_name('child'),
                                    stakeholder_attributes:{
                                      name:'First Primary Beneficiary', ssn:'357867364', birth_or_trust_date:'5/17/2000', genre:Enum::EntityType['person']
                                    }
                                  }
  let(:premium_mode_id)           {Enum::PremiumModeOption.id('Monthly')}
  let(:planned_modal_premium)     {monthly_premium}
  let(:monthly_premium)           {947}
  let(:annual_premium)            {5_194}
  let(:agent)                     {build_stubbed :user, name:'Jeff Root', birth_or_trust_date:'10/4/1949' }
  let(:agent_number)              {'8TK87'}
  let(:state_license)             {Enum::State.find_by_abbrev('CA')}
  let(:agent_phone)               {'555-555-5555'}
  let(:agent_of_record)           {agent}
end

shared_context 'Case 2' do
  include_context 'base apps context'
  let(:start_time)                { Time.new 2014, 9,24, 15,52,40, 0 }
  let(:kase_created_at)           { Time.new 2014, 9,24, 15,52,40, 0 }
  let(:name)                      {'FRANCIS F. SNMMMMTESTCASEFF'}
  let(:gender)                    {MALE}
  let(:birth_country_id)          {Enum::Country.id('Philippines')}
  let(:birth_or_trust_date)       {'9/10/1986'}
  let(:ssn)                       {'216198888'}
  let(:addresses)                 {[Address.new(street:'Po Box 112', city:'Dallas', state:Enum::State['TX'], zip:'75231', address_type:Enum::AddressType['mailing'])]}
  let(:phone1)                    {'310-123-4567'}
  let(:phone2)                    {nil}
  let(:email)                     {'chrisanne-m.pascua@aig.com'}
  let(:employer)                  {'ACME Industries'}
  let(:earned_income)             {10000}
  let(:household_income)          {20000}
  let(:net_worth)                 {30000}
  let(:tobacco_cigar_last)        {'1/1/2010'}
  let(:apps_id)                   {'YTPCASE205'}
  let(:duration_in_years)         {15}
  let(:face_amount)               {2000000}
  let(:waiver_of_premium)         {false}
  let(:beneficiaries)             {[bene_a]}
  let(:bene_a)                    {build :bene_relationship, percentage:100, contingent:false, relationship_type:Enum::RelationshipType.find_by_name('sibling'),
                                    stakeholder_attributes:{
                                      name:'Dos Bene', ssn:'123456789', birth_or_trust_date:'10/10/1989', genre:Enum::EntityType['person']
                                    }
                                  }
  let(:premium_mode_id)           {Enum::PremiumModeOption.id('Quarterly')}
  let(:quarterly_premium)         {345}
  let(:planned_modal_premium)     {quarterly_premium}
  let(:monthly_premium)           {947}
  let(:annual_premium)            {5194}
  let(:agent)                     {build_stubbed :user, full_name:'Rob Eisenberg', ssn:nil, birth_or_trust_date:'1991-11-18'}
  let(:agent_of_record)           {agent}
  let(:agent_number)              {'9VC31'}
  let(:state)                     {nil}
  let(:state_license)             {Enum::State.find_by_abbrev('TX')}
  let(:agent_phone)               {'444-444-4444'}
end

shared_context 'Case 3' do
  include_context 'base apps context'
  let(:start_time)                { Time.new 2014, 9,24, 15,52,39, 0 }
  let(:kase_created_at)           { Time.new 2014, 9,24, 15,52,39, 0 }
  let(:name)                      {'GEORGIA G. SNMMMMTESTCASEGG'}
  let(:gender)                    {FEMALE}
  let(:birth_country_id)          {nil}
  let(:citizenship)               {Enum::Citizenship['Resident'] }
  let(:birth_state)               {Enum::State.find_by_abbrev 'IL'}
  let(:birth_or_trust_date)       {'6/5/1976'}
  let(:ssn)                       {'612198888'}
  let(:dln)                       {'IL12345'}
  let(:dl_state)                  {Enum::State['IL']}
  let(:addresses)                 {[Address.new(street:'Po Box 113', city:'Chicago', state:Enum::State['IL'], zip:'90001', address_type:Enum::AddressType['mailing'])]}
  let(:phone1)                    {'714-123-4567'}
  let(:phone2)                    {'310-111-1111'}
  let(:email)                     {'chrisanne-m.pascua@aig.com'}
  let(:household_income)          {20_000}
  let(:net_worth)                 {25_000}
  let(:bankruptcy)                {true}
  let(:bankruptcy_declared)       {'1/20/1998'}
  let(:foreign_travel)            {true}
  let(:hazardous_avocation)       {true}
  let(:apps_id)                   {'YTPCASE305'}
  let(:duration_in_years)         {20}
  let(:face_amount)               {500_000}
  let(:existing_coverages)        {[kase_a, kase_b]}
  let(:kase_a)                    {existing_coverage 'AM100000', 'GOLDEN SECURITY LF', 'Individual', 100000, '1998-10-10', false }
  let(:kase_b)                    {existing_coverage 'PM2020000', 'JOHN HANCOCK MUTUAL LF', 'Individual', 250000, '2000-11-12', false }
  let(:beneficiaries)             {[bene_a]}
  let(:bene_a)                    {build :bene_relationship, percentage:'100', contingent:false, relationship_type:Enum::RelationshipType.find_by_name('child'),
                                    stakeholder_attributes:{
                                      name:'Tres Bene', ssn:'111111111', birth_or_trust_date:'1/1/1997', genre:Enum::EntityType['person']
                                    }
                                  }
  let(:premium_mode_id)           {Enum::PremiumModeOption.id('Annually')}
  let(:planned_modal_premium)     {annual_premium}
  let(:annual_premium)            {5_194}
  let(:monthly_premium)           {947}
  let(:agent)                     {build_stubbed :user, full_name:'Liran Hirschkorn', birth_or_trust_date:'1955-10-14'}
  let(:agent_of_record)           {agent}
  let(:agent_number)              {'9VW52'}
  let(:state)                     {nil}
  let(:state_license)             {'IL'}
  let(:agent_phone)               {'777-777-7777'}
end

shared_context 'Case 4' do
  include_context 'base apps context'
  let(:start_time)                { Time.new 2014, 9,24, 15,54,42, 0 }
  let(:kase_created_at)           { Time.new 2014, 9,24, 15,54,42, 0 }
  let(:name)                      {'KATE SNMMMMTESTCASEBK'}
  let(:gender)                    {FEMALE}
  let(:birth_country_id)          {Enum::Country.id('United States')}
  let(:birth_state)               {Enum::State.find_by_abbrev('WI')}
  let(:birth_or_trust_date)       {'5/31/1976'}
  let(:ssn)                       {'162198888'}
  let(:addresses)                 {[Address.new(street:'Po Box 114', city:'Milwaukee', state:Enum::State['WI'], zip:'53208', address_type:Enum::AddressType['mailing'])]}
  let(:phone1)                    {'949-123-4567'}
  let(:phone2)                    {'760-222-2222'}
  let(:email)                     {'chrisanne-m.pascua@aig.com'}
  let(:employer)                  {'AG'}
  let(:earned_income)             {30_000}
  let(:household_income)          {30_000}
  let(:net_worth)                 {60_000}
  let(:last_nicotine_patch_or_gum){'1/10/2013'}
  let(:criminal)                  {true}
  let(:apps_id)                   {'YTPCASE404'}
  let(:duration_in_years)         {25}
  let(:face_amount)               {3_000_000}
  let(:health_class_id)           {10}
  let(:beneficiaries)             {[bene_a]}
  let(:bene_a)                    {build :bene_relationship, percentage:100, contingent:false, relationship_type:Enum::RelationshipType.find_by_name('sibling'),
                                    stakeholder_attributes:{
                                      name:'Mark Beneficiary', ssn:'111-55-4444', birth_or_trust_date:'9/28/1975', genre:Enum::EntityType['person']
                                    }
                                  }
  let(:premium_mode_id)           {Enum::PremiumModeOption.id('Semi-annl.')}
  let(:planned_modal_premium)     {semiannual_premium}
  let(:semiannual_premium)        {9_998}
  let(:agent)                     {build_stubbed :user, name:'R Jan Pinney', birth_or_trust_date:'1991-07-16'}
  let(:agent_of_record)           {agent}
  let(:agent_number)              {'7YV29'}
  let(:state_license)             {Enum::State.find_by_abbrev('WI')}
  let(:agent_phone)               {'999-888-8888'}
end

shared_context 'Case 5' do
  include_context 'base apps context'
  let(:start_time)                { Time.new 2014, 9,24, 14,54,42, 0 }
  let(:kase_created_at)           { Time.new 2014, 9,24, 14,54,42, 0 }
  let(:name)                      {'Oprah Snmmmmtestcasebo'}
  let(:gender)                    {FEMALE}
  let(:birth_country_id)          {nil}
  let(:birth_state)               {Enum::State.find_by_abbrev('CA')}
  let(:birth_or_trust_date)       {'4/1/1970'}
  let(:ssn)                       {'123846818'}
  let(:dln)                       {'C55115'}
  let(:dl_state)                  {Enum::State['CA']}
  let(:addresses)                 {[Address.new(street:'555 Main Ave', city:'Scramento', state:Enum::State['CA'], zip:'94204', address_type:Enum::AddressType['mailing'])]}
  let(:phone1)                    {'444-444-3333'}
  let(:phone2)                    {'777-777-7777'}
  let(:email)                     {'melissa.harley@aglife.com'}
  let(:employer)                  {'ABC Incorporated'}
  let(:earned_income)             {0}
  let(:household_income)          {15_000}
  let(:net_worth)                 {20_000}
  let(:tobacco_cigarette_last)    {'5/1/2013'}
  let(:hazardous_avocation)       {true}
  let(:moving_violation_last_2_yr){1}
  let(:bankruptcy)                {true}
  let(:bankruptcy_declared)       {'1/25/2007'}
  let(:bankruptcy_discharged)     {Date.new(2014,11,25)}
  let(:criminal)                  {true}
  let(:apps_id)                   {'YTP500CASE'}
  let(:duration_in_years)         {10}
  let(:face_amount)               {360000}
  let(:health_class_id)           {9}
  let(:waiver_of_premium)         {true}
  let(:existing_coverages)        {[kase_a, kase_b]}
  let(:kase_a)                    {existing_coverage 'CA56777', 'MET LF-STATE ST FUNDS', 'Individual', 200000, '11/18/1994', true }
  let(:kase_b)                    {existing_coverage 'CA123444', 'UNIVERSAL PRESIDENTIAL LF', 'Individual', 300000, '12/2/1997', true }
  let(:beneficiaries)             {[bene_a]}
  let(:bene_a)                    {build :bene_relationship, percentage:34, contingent:false, relationship_type:Enum::RelationshipType.find_by_name('child'),
                                    stakeholder_attributes:{
                                      name:'First Primary Beneficiary', ssn:'357867364', birth_or_trust_date:'5/17/2000', genre:Enum::EntityType['person']
                                    }
                                  }
  let(:premium_mode_id)           {Enum::PremiumModeOption.id('Monthly')}
  let(:planned_modal_premium)     {monthly_premium}
  let(:monthly_premium)           {947}
  let(:annual_premium)            {5_194}
  let(:agent)                     {build_stubbed :user, name:'R Jan Pinney', birth_or_trust_date:'1965-12-03'}
  let(:agent_of_record)           {agent}
  let(:agent_number)              {'7YV29'}
  let(:state_license)             {Enum::State.find_by_abbrev('CA')}
  let(:priority_note)             {'This man is known to the State of California to be a total trash boat'}
  let(:agent_phone)               {'555-555-5555'}
end


def existing_coverage id, carrier_name, type, amt, date, replaced
  ad= build(:quoting_quote, face_amount:amt)
  k = build :crm_case, consumer:build(:consumer), current_details:ad, effective_date:date, termination_date:nil
  ad.crm_case=k
  k.stub(:id).and_return(id)
  mock_carrier = MockCarrier.new carrier_name, find_carrier_id(carrier_name)
  k.current_details.stub(:carrier).and_return(mock_carrier)
  k.replaced_by_id = 0 if replaced
  k
end

def find_carrier_id ag_carrier_name
  file_path=Processing::Apps::XmlBuilder::CARRIER_CODE_SPREADSHEET
  @carriers_table ||= Spreadsheet.open(file_path).worksheet('AG Co-Carrier for Existing Ins')
  row = @carriers_table.find{|r| r[0].strip =~ Regexp.new(ag_carrier_name,'i') }
  row[1]
end

# Holds fields for AG codes
MockCarrier = Struct.new(:name, :code, :naic_code)
