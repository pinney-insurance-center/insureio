module.exports = function(handshakeData, callback) {
	// get session cookie string from request header
  var cookie = handshakeData.headers.cookie.match(/_clu2_session=(.*?)(;|$)/)[1];
  // decrypt session cookie
	var rubyCmd = "ruby -e \"require 'rubygems'; require 'base64'; require 'json'; puts Marshal.load(Base64.decode64('"+ cookie +"')).to_json\"";
  require("child_process").exec(rubyCmd, function(err, stdout, stderr) {
  	if(err) throw err;
  	cookie = JSON.parse(stdout);
  	handshakeData.userId = cookie['usage/user_credentials_id'];
  	callback(null, true);
  });
  
}