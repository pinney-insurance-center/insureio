/*
  Comments can't be in the yaml file because the node %yaml% module can't parse them,
  so here are comments for the socket.yml file.

  NodeJS should listen to `redis_host` on `redis_port`.
  It should run on `nodejs_socket_app_port`
  The front-end (socket-client.js) should listen to the NodeJS app through `nodejs_socket_app_port`.
*/

process.env.NODE_ENV || (process.env.NODE_ENV = 'development');

var config = function () {

  var text = require('fs').readFileSync('../config/data/socket.yml', {encoding:'utf-8'});
  var configs = require('yaml').eval( text );
  var configObj=configs['all']||{};
  var envConfigObj=configs[process.env.NODE_ENV]||{};
  for(const key in envConfigObj){//Environment values override global ones.
    if(envConfigObj[key]){
      configObj[key]=envConfigObj[key];
    }
  }

  return configObj;

}();

module.exports = config;
