var noOfWorkers = process.env.NODE_WORKERS || require('os').cpus().length;

if (noOfWorkers == 1) {
	console.log("Only one processor detected. No clustering shall be done. Set number of workers with NODE_WORKERS.")
	require('./app.js');
} else {
	var cluster = require('cluster');
	if (cluster.isMaster) {
		console.log("Going to start Socket.IO app with "+noOfWorkers+" workers");
		for (var i = 0; i < noOfWorkers; i ++) {
			console.log("forking");
			cluster.fork();
		}
	} else {
		console.log("forked");
		require('./app.js');
	}
}
