module.exports = function(config) {

	var logger = require("winston");
	var logLevel = config.log_level || 'info';
	var logDir = config.logDir || './log/';
	var logFile = logDir + process.env.NODE_ENV + '.log';
	var errLogFile = logDir + 'error.log';
	// Configure logger
	logger.add(logger.transports.File, { filename: logFile, datePattern: '.yyyyMMdd', level: logLevel, maxsize: 32000, maxFiles: 20 });
	logger.handleExceptions(new logger.transports.File({filename: errLogFile}));
	logger.exitOnError = false;
	logger.info("Starting up in " + process.env.NODE_ENV);

	return logger;
}
