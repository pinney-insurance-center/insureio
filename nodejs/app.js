process.title = 'dataraptor-sockets';
process.env.NODE_ENV || (process.env.NODE_ENV = 'development');

// modules in top-level namespace
var config = require('./config');
var express = require('express');
var redis = require("redis")
var helper = require("./message_helper");
var logger = require("./logger")(config);

// build server, and socket.io
var expressApp = express();
var server;
if (config.ssl) {
  var fs = require('fs');
  config.ssl.key = fs.readFileSync(config.ssl.key_file, 'utf8');
  config.ssl.cert = fs.readFileSync(config.ssl.cert_file, 'utf8');
  server = require('https').createServer(config.ssl, expressApp);
}
else {
  server = require('http').createServer(expressApp);
}
var io = require('socket.io').listen(server);

// redis clients etc
var redisOptions={host:config.redis_host, port:config.redis_port};
var dataStoreClient = redis.createClient(redisOptions);
var dataStorePub = redis.createClient(redisOptions);
var dataStoreSub = redis.createClient(redisOptions);
var RedisStore = require('socket.io/lib/stores/redis');

// socket.io configuration
io.configure(function(){
  // authorize; set socket.handshake.userId
  io.set('authorization',require('./authorization'));
  // use redis for session store
  io.set('store', new RedisStore({ redisPub: dataStorePub, redisSub: dataStoreSub, redisClient: dataStoreClient }));
});

// subscribe to redis
var railsListener = redis.createClient(redisOptions);
railsListener.subscribe(config.redis_channel_for_socket);

// relay redis messages to connected sockets
railsListener.on("message", function(channel, message) {
  logger.info("from rails to subscriber:", channel, message);
  var parsed;
  try {
    parsed = JSON.parse(message);
  }
  catch(ex) {
    logger.error('failed to parse')
  }
  if (helper.isUniversal(parsed)) {
    for(s in ss){
      ss[s].emit('message', message);
    }
  }else{
    for(s in ss){
      if( helper.isAddressedTo(parsed, ss[s]) ){
        ss[s].emit('message', message);
      }
    }
  }
});

var ss = []//list of sockets

io.sockets.on('connection', function (socket) {
  /* immediately upon connect */
  logger.info("user connected", socket.handshake.userId);
  ss.push(socket);//add this socket to the list
  socket.emit("message", {body:'connected!'});

  // disconnect callback
  socket.on('disconnect', function () {
    logger.info("user disconnected");
    ss.splice(ss.indexOf(socket),1);//remove this socket from the list
  });


});

// start HTTP server listening
server.listen(config.nodejs_socket_app_port);
logger.info("Socket.IO listening on port " + (config.nodejs_socket_app_port));