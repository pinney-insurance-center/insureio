module.exports = {
	userIdForSocket: function (socket, logger) {
		var handshake = socket.handshake || socket.manager.handshaken[socket.id];
		if (logger && typeof(handshake) === "undefined")
			{ logger.error("No handshake for socket:", socket); }
		return handshake && handshake.userId;
	},
	isUniversal: function(message) {
		return typeof message['recipients'] === 'undefined' || message.recipients.length == 0;
	},
	isAddressedTo: function(message, socket, logger) {
		return message.recipients.indexOf(this.userIdForSocket(socket, logger)) > -1;
	}
};