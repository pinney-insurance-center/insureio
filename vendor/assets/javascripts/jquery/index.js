/* This directory holds jQuery and jQuery-dependent libs, not homebrew code */

//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
/* bootstrap-tooltip needs to come before bootstrap-popover */
//= require ./bootstrap-tooltip
/* bootstrap-popover needs to come before bootstrap-editable */
//= require ./bootstrap-popover
/* select2 provides an autocomplete select box, which can be integrated with
  x-editable (bootstrap-editable) */
//= require select2
//= require_tree .