class UserMailer < ActionMailer::Base

  default from: APP_CONFIG['email']['sender']

  def password_reset(user, email)
    @user = user
    mail :to => email.value do |format|
      format.html
    end
  end

  def activation_instructions(user, email)
    @user = user
    mail :to => email do |format|
      format.html
    end
  end

  def test_message(user,smtp_settings)
    subject='Insureio - Testing Your SMTP Settings'
    body   ='This is a test email from Insureio, verifying that your SMTP Settings were successfully updated.'
    message=mail to:user.primary_email.try(:value), subject:subject, body:body, :from => smtp_settings[:user_name]
    message.delivery_method :smtp, smtp_settings
  end

  def email_message(message, message_body)
    @message_body = message_body.html_safe
    message.attachments.each do |attach|
      unless attach.file_file_name.blank?
        attachments[attach.file_file_name] = File.read(attach.file.path)
      end
    end
    mail :to => receiver_emails(message) , :subject => message.subject.blank? ? message.template.subject : message.subject do |format|
      format.html
    end
  end

  def email_template_message(recipient, message_body, subject, attachments = nil)
    @message_body = message_body
    message.attachments.each do |attach|
      unless attach.file_file_name.blank?
        attachments[attach.file_file_name] = File.read(attach.file.path)
      end
    end
    mail :to => recipient, :subject => subject do |format|
      format.html
    end
  end

  def task_has_no_assignment_notification(agent, task)
    @task = task
    mail :to => agent.email, :subject => "Task has no assigned user" do |format|
      format.html
    end
  end

  def invite_email(recipient_email, recipient_name, smtp_settings, parent=nil)
    @recipient_email=recipient_email
    @recipient_name=recipient_name
    @parent=parent
    if smtp_settings.is_a? Hash
      @parent_id = smtp_settings.delete(:parent_id) || parent.try(:id)
    else
      @parent_id = parent.try(:id) || smtp_settings.owner_id
      smtp_settings = smtp_settings.to_delivery_method
    end
    @signup_url="#{root_url}/signup?parent_id=#{@parent_id}"
    message = mail :to => recipient_email, :subject => "Invitation to Signup", :from => smtp_settings[:user_name]
    message.delivery_method :smtp, smtp_settings
  end

  private

  def receiver_emails(message)
    emails = []
    consumer_email = message.consumer.try(:primary_email).try(:value)
    user_email     = message.user.try(:primary_email).try(:value)
    emails << consumer_email if consumer_email.present?
    emails << user_email.value if user_emaill.present?
    emails << message.recipient unless message.recipient.blank?
    return emails.join(',')
  end


end
