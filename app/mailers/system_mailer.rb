class SystemMailer < ActionMailer::Base
	default from:APP_CONFIG['email']['sender'], to:APP_CONFIG['email']['admin']

  def generic recipients, body, subject=nil
    subject ||= body[0..50]
    mail to:recipients, subject:subject, body:body
  end

	def suggestion_email(suggestion)
		@suggestion = suggestion
		mail(to:APP_CONFIG['email']['admin'], subject:"Suggestion from CLU2 User").deliver
	end

  # Like #warning, but whereas a warning generally pertains to a data failure
  # (e.g. can't assign a lead to an agent because no elligible agent exists in 
  # the database), an error pertains to a code failure (e.g. an exception was
  # raised).
  # Warning emails should go to admin (Nic), and error emails should go to
  # developers.
  # This function is intended to replace error_email, but I don't have time to
  # refactor right now, so this is just a going-forward solution for now.
  # !!DEPRECATED!! USE `ExceptionNotifier.notify_exception(ex)` INSTEAD.
  def error *args
    ActiveSupport::Deprecation.warn('USE `ExceptionNotifier.notify_exception(ex)` INSTEAD.')
    options = args.extract_options!
    options[:recipients] = APP_CONFIG['email']['developers'] if options[:recipients].blank?
    options[:level] ||= "ERROR"
    _backtrace_email *args, options
  end

  #Notify a user that a recipient who the system cannot identify has requested to be unsubscribed.
  def unsubscribe_request message, email_type
    @message, @email_type=[message, email_type]
    mail to:@message.user.try(:primary_email).try(:value), bcc:APP_CONFIG['email']['admin'], subject:"Please unsubscribe from #{@email_type}"
  end

  # Deprecated
  def error_email exception_or_backtrace, *args
    _normalize_exception_or_caller exception_or_backtrace, *args
    mail(to:APP_CONFIG['email']['developers'], subject:'CLU2 Exception SystemMailer#error_email').deliver
  end

  # Deliver a warning email to admin (Nic) or +options[:recipients]+
  def warning *args
    options = args.extract_options!
    options[:recipients] = APP_CONFIG['email']['admin'] if options[:recipients].blank?
    options[:level] ||= "WARNING"
    _backtrace_email *args, options
  end

  # Deliver a warning email to admin(s), and log the event.
  # Admin emails will allow incorrect API usage to be corrected.
  # Logging will inform future code refactors.
  def deprecated_method_warning obj_of_interest=nil
    recipients= APP_CONFIG['email']['admin']
    backtrace= Rails.backtrace_cleaner.clean(caller[1..-1])
    return if backtrace.any?{|str| str.include?('deprecated_method_warning') }#avoid system stack errors.

    method_name=(backtrace[0].match(/`([\w|=|\?]+)'\Z/)[1] rescue nil)
    message ="A deprecated method (a block of code marked for future removal due to inefficiency or redundancy) was called.\n"
    message+="If this call originated from the lead import API or CSV upload, please inform the user in question "
    message+="that we will no longer be supporting that method, and refer them to our documentation."
    message+="If this call originated elsewhere, it indicates a segment of code where refactoring is needed.\n\n"
    backtrace=backtrace.reject{|str| str.match(/app\/middleware/) }
    details ="Method name: #{method_name}\n"+
             "Current User Id:#{Thread.current[:current_user_id]}\n"+
             "API User Id:#{Thread.current[:current_api_user_id]}\n\n"+
             "Stack trace (chain of events leading to the method call):\n#{backtrace.join("\n")}\n"

    if Rails.env.test?
      msg_for_dev_testers="DEPRECATED: #{method_name}\n#{backtrace.join("\n")}"
      onscreen_logger=Logger.new(STDOUT)
      onscreen_logger.warn msg_for_dev_testers
      logger.warn msg_for_dev_testers
      return
    end

    if obj_of_interest
      if obj_of_interest.is_a?(String)
        obj_string=obj_of_interest
      elsif obj_of_interest.respond_to?(:consumer) && obj_of_interest.consumer
        obj_string=JSON.pretty_generate obj_of_interest.consumer.as_json rescue nil
      else#Using `as_json` here makes the operation safer, because it honors any model overrides for that method.
        obj_string=JSON.pretty_generate obj_of_interest.as_json rescue nil
      end
      details+="\nObject of interest: #{ obj_string }\n"
      if obj_of_interest.respond_to?(:warnings) && method_name.present?
        obj_of_interest.warnings||=[]
        obj_of_interest.warnings.push "Method `#{method_name}` will no longer be supported in the near future. Please check academy.insureio.com for the correct method to use going forward."
      end
    end

    if backtrace.any?{|l| l.match(/_controller\.rb/) }
      req_path=Thread.current[:request_store][:authlogic_controller].env["ORIGINAL_FULLPATH"] rescue nil
      req_method=Thread.current[:request_store][:authlogic_controller].env["REQUEST_METHOD"] rescue nil
      req_params=Thread.current[:request_store][:authlogic_controller].env[ "action_controller.instance"].request.filtered_parameters rescue nil
      if req_path || req_method || req_params
        details+="\nRequest Path: #{req_path}\nRequest HTTP Method: #{req_method}\nRequest Parameters: #{JSON.pretty_generate(req_params||{})}"
      end
    end

    message+=details

    AuditLogger['deprecated_method_warnings'].warn details

    subj = "Re: [Insureio: WARNING] #{message.to_s[0,64]}"
    mail = mail(to:recipients, subject:subj, body:message)
    begin
      mail.deliver
    rescue Exception => ex
      AuditLogger['errors'].error [ex.class.name, ex.message, ex.backtrace].flatten.join("\n")
    ensure
      mail
    end
  end

  def notify_admin_of_new_user user
    @user = user
    mail to: 'signups@pinneyinsurance.com', subject:'DR: New User'
  end

private 

  # Helper method for warnings or errors
  # +message+ can be String or Array
  def _backtrace_email *args
    AuditLogger['errors'].warn args.inspect
    options = args.extract_options!
    first_arg = options[:exception] || args[0]
    # Ensure message and backtrace
    if first_arg.is_a? Exception
      options[:exception] = first_arg
      options[:backtrace] ||= first_arg.backtrace || caller[1..-1]
    else
      options[:message] ||= first_arg
      options[:backtrace] ||= caller[1..-1]
    end
    # build message
    @exception  = options[:exception]
    @message    = options[:message]
    @backtrace  = options[:backtrace]
    @debug      = options[:debug]
    subj = "Re: [Insureio:#{options[:level]}] #{@message.to_s[0,64]}"
    mail = mail(to:options[:recipients], subject:subj, template_name:'backtrace', cc:options[:cc], bcc:options[:bcc])
    begin
      mail.deliver
    rescue Exception => ex
      AuditLogger['errors'].error [ex.class.name, ex.message, ex.backtrace].flatten.join("\n")
    ensure
      mail
    end
  end

  # Sets @exception and @message. Takes args exception_or_backtrace, message_1_or_2, message_2
  def _normalize_exception_or_caller exception_or_backtrace, *args
    @exception = exception_or_backtrace
    if @exception.is_a? Array
      backtrace = @exception
      @exception = RuntimeError.new(args[0])
      @exception.set_backtrace backtrace
      @message = args[1]
    else
      @message = args[0]
    end
  end
end
