class CatchUnsupportedCharacters
  def initialize(app)
    @app = app
  end

  def call(env)
    @app.call(env)
  rescue ActiveRecord::StatementInvalid => ex
    if is_db_encoding_error? ex
      [400, {'Content-type': 'text/plain'}, ["#{ex.cause.message}. It's possible that you have an unsupported character encoding in your request. We are using the charset #{ActiveRecord::Base.connection.charset.upcase}"]]
    else
      throw ex
    end
  end

private

  def is_db_encoding_error? ex
    ex.cause.is_a?(Mysql2::Error) and \
    ex.cause&.message&.starts_with?("Illegal mix of collocations")
  end
end
