class DefaultContentType
  def initialize app; @app = app; end

  def call env
    if env['CONTENT_TYPE'].nil? && env['PATH_INFO'] =~ /\.json$/
      env['CONTENT_TYPE'] = 'application/json'
    end
    @app.call env
  end
end
