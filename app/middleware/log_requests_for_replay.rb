# Log a request for later replay.
# (1) if a manual flag is set (short-circuit the rails app)
# (2) if certain db errors occur
class LogRequestsForReplay
  def initialize(app)
    @app = app
  end

  def call(env)
    # Check for a manual flag
    log(env) if ENV['LOG_ONLY'].present? && 'GET' != env['REQUEST_METHOD']
    @app.call(env)
  rescue Mysql2::Error, ActiveRecord::StatementInvalid => ex
    # Handle certain kinds of db error
    if 'GET' != env['REQUEST_METHOD'] && ex.message =~ ERR_REGEX
      log(env)
      return [503,  {'Content-type': 'application/json'}, [{error: ERR_MSG}]]
    else
      throw ex
    end
  end

  private

  def cookie_store
    @cookie_store ||= begin
      app = @app
      while app.instance_variable_get(:@app) && !app.is_a?(ActionDispatch::Session::CookieStore)
        app = app.instance_variable_get(:@app)
      end
      app if app.is_a?(ActionDispatch::Session::CookieStore)
    end
  end

  # Log method, host, path, body, headers, user_id
  def log env
    logger = Logger.new 'log/lost-requests.log', 'daily', level: :debug
    logger.formatter = proc { |severity, time, progname, msg| "#{msg}\n" }
    req = Rack::Request.new(env) # Using a thin wrapper for the sake of portability
    data = {
      time: Time.now,
      body: req.body.read,
      env: env.select { |k,v| k =~ /^[A-Z_]+$/ && v.is_a?(String) }, # Headers, path, method, host
    }
    if cookie_store
      sess_id, data[:sess] = cookie_store.load_session env # Verify user now in case session expires
    end
    logger.info data.to_json
    req.body.rewind # Rewind shouldn't be needed in standard usage, but it makes this fn more portable
  end

  ERR_REGEX = /MySQL server has gone away|Lost connection to MySQL server|Can't connect to MySQL server/
  ERR_MSG = "Service unavailable. Your submission has been stored, and manual processing will be attempted later, but the service is unable to return an ordinary response at this time. A service outage such as this may be caused by server maintenace, which tends to conclude within 2 hours of its commencement."
end
