#To test on a dev machine, run `rails server webrick`.
#Production servers run webrick by default.
class CatchQueryParseErrors
  def initialize(app)
    @app = app
  end

  def call(env)
    query = Rack::Utils.parse_nested_query(env['QUERY_STRING'].to_s) rescue :bad_query
    if query == :bad_query
      [400, {'Content-Type' => 'text/plain'}, ["Improperly formed query string."]]
    else
      @app.call(env)
    end
  end
end