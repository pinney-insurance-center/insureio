class RemoveUnsupportedIpHeader
  def initialize(app)
    @app = app
  end

  def call(env)
    #Our node balancer sets "X_FORWARDED_FOR", and there can be only one such header.
    #So, if another proxy between client and node balancer sets "CLIENT_IP", remove it.
    #Note: Rack prepends "HTTP_" to each header key
    client_ip_header = env.delete('HTTP_CLIENT_IP')
    if client_ip_header.present?
      env['HTTP_X_ALTERNATE_POSSIBLE_CLIENT_IP'] = client_ip_header.to_s
    end
    @app.call(env)#calls the next middleware in the chain and returns its result
  end
end