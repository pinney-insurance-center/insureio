# ApplicationController sets values on `Thread.current`, e.g.
# `Thread.current[:tenant_id]`. These persist from one request to the next and
# carry security implications. Therefore, middleware removes any values for
# our custom keys which were set during the previous stack invocation.
class RemoveThreadAnnotations
  def initialize(app)
    @app = app
  end

  def call(env)
    # Clear user-related Thread stores
    Thread.current.keys
    .select { |key| key =~ /current_user|^sess_/ }
    .each { |key| Thread.current[key] = nil }
    Thread.current[:tenant_id] = nil
    @app.call(env)
  end
end
