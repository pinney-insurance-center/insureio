class Usage::License < ActiveRecord::Base
  

  validates_presence_of   :user, :state, :number, :effective_date, :expiration
  validates_uniqueness_of :state_id, :scope => [:user_id]
  validate :has_category

  # Associations
  belongs_to      :user,   class_name: "User", inverse_of: :licenses
  belongs_to_enum :state,  class_name: "Enum::State"
  belongs_to_enum :status, class_name: "Enum::LicenseStatus"

  delegate :name, to: :user, prefix: true

  scope :in_effect, lambda { self.not_pending.not_expired.in_active_status }
  scope :not_pending, lambda {
    where('effective_date IS NOT NULL AND effective_date <= ?', Date.today)
  }
  scope :not_expired, lambda {
    where('expiration IS NOT NULL AND expiration >= ?', Date.today)
  }
  scope :in_active_status, lambda {
    where('status_id=?', Enum::LicenseStatus.id('approved') )
  }

  def state_name= s_name
    self.state_id||=Enum::State.id(s_name)
  end

  def is_in_effect?
    self.effective_date && self.effective_date<=Date.today &&
    self.expiration && self.expiration>=Date.today &&
    (Enum::LicenseStatus.id('approved')+[nil]).include?(self.status_id) &&
    self.valid?
  end

  def has_category
    has_c=self.corporate || self.life || self.health || self.variable || self.p_and_c
    if !has_c
      errors.add :base, 'This license must apply to at least one of: corporate, life, health, variable, or property and casualty.'
    end
  end

end
