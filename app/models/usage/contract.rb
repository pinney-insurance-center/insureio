class Usage::Contract < ActiveRecord::Base
  
  
  # Associations
  belongs_to_enum :status,     class_name: "Enum::ContractStatus"
  belongs_to      :user,       class_name: "User", inverse_of: :contracts
  belongs_to      :carrier,    class_name: "Carrier"

  has_and_belongs_to_many_enum :states, class_name: "Enum::State", join_table: "usage_contracts_states"


  validates_presence_of   :user, :carrier, :carrier_contract_id, :effective_date, :expiration
  validate :check_agent_license
  validate :check_carrier

  scope :in_effect, lambda { self.not_pending.not_expired.in_active_or_nil_status }

  scope :not_pending, lambda {
    where('effective_date IS NOT NULL AND effective_date <= ?', Date.today)
  }
  #Not all contracts (unlike licenses) have a set expiration/termination date.
  scope :not_expired, lambda {
    where(' (expiration IS NULL OR expiration >= ?) ', Date.today)
  }
  scope :in_active_or_nil_status, lambda {
    where('status_id IN (?) OR status_id IS NULL', Enum::ContractStatus.id('approved','active w/ application') )
  }

  def carrier_name
    if self.carrier
      self.carrier.name
    else
      nil
    end
  end

  def carrier_name= cname
    self.carrier=Carrier.find_or_create_by_name(cname)
  end

  def state_ids
    states.map(&:id)
  end

  def state_ids= ids
    self.states=ids.map{ |id| Enum::State.find_by_id(id.to_i)}.compact
  end

  def is_in_effect?
    self.effective_date && self.effective_date<=Date.today &&
    self.expiration && self.expiration>=Date.today &&
    (Enum::ContractStatus.id('approved','active w/ application')+[nil]).include?(self.status_id) &&
    self.state_ids.length>0 &&
    self.valid?
  end

  private 
  
  def check_agent_license
    self.states.each do |s|
      if self.user.licenses.where("state_id = #{s.id}").blank?
        errors.add(:states,": Agent must be licensed for the state of #{s.name} before setting up contracts in #{s.name}.")
      end
    end
  end

  def check_carrier
    return if user.nil? || carrier.nil?
    duplicates=self.user.contracts.where("carrier_id = #{self.carrier.id}")

    duplicates=duplicates.where("id<> #{self.id}") if self.id

    unless duplicates.blank?
      errors.add(:carrier,": A user can only have one contract with each carrier. #{self.carrier.name} is already taken.")
    end
  end

end
