class Usage::DocusignCred < ActiveRecord::Base
	

	validates_presence_of :account_id, :email, :password

	belongs_to :user, class_name: "User"

	def set?
		email.present? and password.present?
	end
end
