module Usage::UserScopes
  extend ActiveSupport::Concern

  included do
    default_scope -> {
      tenant_id=Thread.current[:tenant_id] rescue nil
      current_user_acts_across_tenancies=Thread.current[:current_user_acts_across_tenancies] rescue nil
      if tenant_id && !current_user_acts_across_tenancies
        where(active_or_enabled:true).
        where(tenant_id:tenant_id)
      else
        where(active_or_enabled:true)
      end
    }

    # Scope Users who have an 'agent' Role
    scope :agents, lambda { can(:lead_distribution) }

    # Scope Users who share consumer's brand, own the case or consumer, or are in owner's staff assignment
    scope :having_access_to_case, lambda { |kase|
      ids=[kase.agent_id, kase.consumer.agent_id, kase.agent.staff_assignment.ids, kase.consumer.agent.staff_assignment.ids]
      joins(:brands)
      .where('brands.id=? OR users.id IN (?)', kase.consumer.brand_id, ids)
    }

    # Scope Users who are not temporarily suspended
    scope :not_suspended, lambda {
      where(temporary_suspension:false)
    }

    # Scope Users who are active at their computer
    scope :available, lambda { where("last_request_at >= ?", 30.minutes.ago.round(0).to_s(:db)) }

    # Scope Users who have a high enough premium limit (for the given lead type)
    scope :for_premium_limit, lambda { |lead_type_id, premium|
      if premium.nil? or lead_type_id.nil?
        where(nil)
      else
        ldw_table = Usage::LeadDistributionWeight.table_name
        joins( :lead_distribution_weights )
        .where( "#{ldw_table}.tag_value_id = ?", lead_type_id )
        .where( "#{ldw_table}.premium_limit >= ?", premium )
      end
    }

    # Scope Users who are licensed in the Case's state
    scope :licensed, lambda { |state_id|
      joins( :licenses )
      .where( "#{Usage::License.table_name}.state_id = ?", state_id )
    }

    # Scope Users whose countdown is > 0
    scope :countdown_positive, lambda { |lead_type_id|
      joins( :lead_distribution_weights )
      .where( "tag_value_id = ?", lead_type_id )
      .where( "countdown > 0" )
    }

    scope :sales_support, lambda { can(:sales_support) }

    scope :enabled, -> {
      SystemMailer.deprecated_method_warning rescue nil
      active_or_enabled #defined in `ContactableStereotype`.
    }

    scope :excluding_recruits, lambda { where(is_recruit:false) }

    scope :able_to_log_in, lambda {
      excluding_recruits.
      where('membership_id != ?', Enum::Membership.id('Disabled') )
    }

  end

end
