module Usage::Permissions

  #DEPRECATED. USE `Enum::PermissionList['All'].list` INSTEAD.
  ENUM=Enum::PermissionList['All'].list
  CROSS_TENANCY_PERMISSIONS = [:sales_support, :super_edit, :super_view, :motorists_rtt_process_details]

  def self.included(base)
    (0...PERMISSIONS_FIELD_CT).each do |i|
      unless base.attribute_method?("permissions_#{i}")
        raise "Usage::Permissions::ENUM has #{ENUM.length} items but #{base.name} lacks field permissions_#{i}"
      end
    end if base.table_exists?

    base.scope :can,    -> permission {
      col, bit = Usage::Permissions.column_and_bit(permission)
      base.where("#{base.table_name}.#{col} & (1<<#{bit})")
    }

    base.scope :can_all,    -> *permissions {
      where_clauses=[]
      permissions.each do |p|
        col, bit = Usage::Permissions.column_and_bit(p)
        where_clauses << "#{base.table_name}.#{col} & (1<<#{bit})"
      end
      where_clause=where_clauses.join(' AND ')
      base.where(where_clause)
    }

    base.scope :can_any,    -> *permissions {
      where_clauses=[]
      permissions.each do |p|
        col, bit = Usage::Permissions.column_and_bit(p)
        where_clauses << "#{base.table_name}.#{col} & (1<<#{bit})"
      end
      where_clause=where_clauses.join(' OR ')
      base.where(where_clause)
    }

    base.scope :cannot, -> permission {
      col, bit = Usage::Permissions.column_and_bit(permission)
      base.where("#{base.table_name}.#{col} & (1<<#{bit}) = 0")
    }

    base.before_create    :set_default_permissions!
    base.before_update    :update_permissions_based_on_membership_change!, if: :persisted?
  end

  # +permission+ can be a +Symbol+ or +Fixnum+
  def can? permission, strict=nil
    if !strict && (strict == false || !DEFAULT_TO_STRICT.include?(permission))
      return true if bit_set?(:super_edit)
      return true if permission.to_s =~ /view_/ and bit_set?(:super_view)
    end
    bit_set? permission
  end

  def can_all? *permissions
    return permissions.all?{|p| can?(p)}
  end

  def can_any? *permissions
    return permissions.any?{|p| can?(p)}
  end

  def can! permission, value=true
    if value != false && (value == true || value.to_i != 0)
      enable_permissions!(permission)
    else
      disable_permissions!(permission)
    end
  end

  def super?
    can? :super_edit
  end

  def disable_permissions! *permissions
    permissions.each do |permission|
      col, bit = Usage::Permissions::column_and_bit(permission)
      self[col] ||= 0
      self[col] &= ~(1 << bit)
    end
  end

  def enable_permissions! *permissions
    permissions.each do |permission|
      col, bit = Usage::Permissions::column_and_bit(permission)
      self[col] ||= 0
      self[col] |= (1 << bit)
    end
  end

  # @return boolean indicating whether any Misc permission is changed
  def misc_permissions_changed?
    [:impersonate, :super_edit, :super_view, :sales_support].each do |permission|
      col, bit = Usage::Permissions::column_and_bit(permission)
      pre, post = changes[col]
      if pre || post # check if there are changes for this column
        return true if (pre.to_i & (1<<bit)) != (post.to_i & (1<<bit)) # check if there are changes for this bit
      end
    end
    false
  end

  def set_default_permissions!
    enable_permissions!(*membership&.permissions)
  end

  #This method removes permissions that were added as part of a trial,
  #while attempting to preserve any custom permissions.
  def update_permissions_based_on_membership_change!
    if membership_id_changed?
      old_level=membership_id_was
    else
      return
    end
    return if membership_id <= Enum::Membership.id('Free')

    old_level_permissions=Enum::Membership.find(old_level).try(:permissions)||[]
    new_level_permissions = membership.permissions || []
    disable_permissions!(*old_level_permissions)
    enable_permissions!( *new_level_permissions)
  end

  def clear_permissions!
    (0...PERMISSIONS_FIELD_CT).each do |i|
      self["permissions_#{i}"] = 0
    end
  end

  def super!
    (0...PERMISSIONS_FIELD_CT).each do |i|
      self["permissions_#{i}"] = 2**32-1
    end
  end

  PERMISSIONS_FIELD_CT = (ENUM.length / 32) + ((ENUM.length % 32) == 0 ? 0 : 1)

  ### SUPPORT FUNCTIONS (FOR EASE OF INTERACTION WITH FORMS) ###

  ENUM.each do |permission, function_name|
    function_name = permission if function_name.nil?
    # Define getter. Defaults to strict.
    define_method "can_strict_#{function_name}" do
      can?(permission, true)
    end
    # Define setter
    define_method "can_strict_#{function_name}=" do |bool|
      can!(permission, bool)
    end
  end

  ### DEPRECATED FUNCTIONS ###

  [
    [:impersonate, :impersonation], [:brand, :can_edit_brands],
    :marketing_canned, :marketing_custom, :reporting_custom, :reporting_canned,
    :dialer, :lead_distribution, :super_edit, :super_view
  ].each do |permission, function_name|
    function_name = permission if function_name.nil?
    # Define getter
    define_method function_name do
      r=Regexp.new("#{Rails.root}")
      warn "[DEPRECATED] Usage::Permissions##{function_name} @ #{caller.select{|c| c.match(r) }.join("\n")}\n"
      can?(permission)
    end
    # Define setter
    define_method "#{function_name}=" do |bool|
      r=Regexp.new("#{Rails.root}")
      warn "[DEPRECATED] Usage::Permissions##{function_name}= @ #{caller.select{|c| c.match(r) }.join("\n")}\n"
      can!(permission, bool)
    end
  end

  # !!! Deprecated !!!
  def permissions
    r=Regexp.new("#{Rails.root}")
    warn "[DEPRECATED] Usage::Permissions#permissions @ #{caller.select{|c| c.match(r) }.join("\n")}\n"
    (0...PERMISSIONS_FIELD_CT).map{|i| self["permissions_#{i}"] }
  end

  # !!! Deprecated !!!
  def permissions= val
    r=Regexp.new("#{Rails.root}")
    warn "[DEPRECATED] Usage::Permissions#permissions= @ #{caller.select{|c| c.match(r) }.join("\n")}\n"
    if val == 0
      clear_permissions!
    elsif val == -1
      super!
    else
      raise "Obsolete usage"
    end
  end

  ### END OF DEPRECATED FUNCTIONS ###


  def self.bulk_set permission, user_ids, value_to_set=true
    col, bit_position = Usage::Permissions::column_and_bit(permission)
    value_of_bit_position = 2**bit_position
    #In SQL, the single pipe character means OR (used for turning a bit on),
    #and the single ampersand character means AND (used for turning a bit off).
    bitwise_operator = value_to_set ? '| ' : '& ~'

    #handle large user sets in batches to prevent timeouts or lock issues
    user_ids.in_groups_of(100) do |uid_group|
      uid_group.compact!
      query_string="
        UPDATE users
        SET #{col}=#{col} #{bitwise_operator}#{value_of_bit_position}
        WHERE id IN (#{ uid_group.join(',') })
      "
      ActiveRecord::Base.connection.execute(query_string)
    end
  end

  def self.column_and_bit permission
    raise "Nil permission not allowed" if permission.nil?
    idx = permission.is_a?(Fixnum) ? permission : ENUM.index(permission.to_s)
    raise "Permission not in ENUM: #{permission}" if idx.nil?
    col_i = idx / 32
    bit_i = idx % 32
    return ["permissions_#{col_i}", bit_i]
  end

private

  def bit_set? permission
    col, bit = Usage::Permissions::column_and_bit(permission)
    self[col] & (1<<bit) != 0
  end

  DEFAULT_TO_STRICT = [
    :lead_distribution,
    :sales_support,
    :dash_my_business_self,
    :dash_my_business_team,
    :dash_opportunities_self,
    :dash_opportunities_team,
    :dash_pending_tasks_self,
    :dash_pending_tasks_team,
    :moderate_unassigned_tasks,
    :dash_events_self,
    :dash_events_team,
  ]

end
