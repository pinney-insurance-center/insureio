module Usage::PaymentRelatedMethods

  # Returns number representing fee, useful for vendor APIs
  def amount membership_id=nil
    membership_id ||= self[:membership_id]
    the_amount=Enum::Membership.find(membership_id).try(:pricing)||0
    raise "You shouldn't be seeking a fee for this membership level: #{membership_id}" if the_amount==0
    the_amount
  end

  # Create `Stripe::Customer` for recurring payments on whichever subscription
  # plan to which the user belongs. This can be done after you have
  # collected the customer's credit card. Credit card collection is done at
  # the controller level in `StripeController`.
  #
  # token => token returned by Stripe when creating a Stripe::Card. (We use
  # Stripe 'Checkout' to create/collect cards.) Identifies customer's credit
  # card on Stripe system.
  def stripe_find_or_create_customer stripe_token=nil, stripe_plan=nil, change_payment_source=nil, coupon_code=nil
    self.stripe_email = stripe_token.present? ? stripe_token[:email] : primary_email.try(:value)
    ## below unless block is used for multiple purpose
    ##1: to add payment source/card if no stripe user just create and add source and existing
    ##2. if someone newly subscribe it will create user, add source
    ## and subscribe user to that plan
    stripe_customer = (Stripe::Customer.retrieve(self.stripe_customer_id)) rescue nil
    if stripe_customer.blank?
      stripe_create_customer stripe_token, stripe_plan
    else
      ## when there is existing customer and he want to change payment source/card
      ## we will use below if block
      if change_payment_source
        stripe_update_payment_source
      else
        #incase stripe_customer_id is present and unsubscribred and again subscribe so there will no
        #subscription object present so checking here
        stripe_update_plan stripe_plan
      end
    end
  rescue Exception, Stripe::InvalidRequestError, Stripe::CardError => e
    errors.add :base, e.message
  ensure
    return errors.empty?
  end

  def stripe_subscribed
    !!stripe_subscription_id
  end
  alias_method :stripe_subscribed?, :stripe_subscribed

  def stripe_create_customer stripe_token=nil, stripe_plan=nil,  coupon_code=nil
    customer_params = {
      source: stripe_token['id'],
      email: email
    }
    customer = Stripe::Customer.create customer_params
    self.stripe_customer_id = customer.id
    #if plan present , this will create subscriptin in stripe
    if stripe_plan.present?
      subscription = customer.subscriptions.create(plan: stripe_plan, coupon:coupon_code)
      subscription.save
      customer.save
      self.stripe_subscription_id = subscription.id
      self.stripe_plan_id = stripe_plan
      unless customer.subscriptions.data.any?{|d| d.plan.id == stripe_plan }
        errors.add :base, "Incorrect plan. Should be #{stripe_plan}"
      end
    end
    self.save!
  end

  def stripe_update_payment_source stripe_token=nil
    stripe_customer = (Stripe::Customer.retrieve(self.stripe_customer_id)) rescue nil
    stripe_customer.source = stripe_token['id']
    stripe_customer.save
  end

  def stripe_update_plan stripe_plan=nil, coupon_code=nil
    stripe_customer = (Stripe::Customer.retrieve(self.stripe_customer_id)) rescue nil
    subscription = stripe_customer.subscriptions.retrieve(self.stripe_subscription_id)  rescue nil
    if stripe_customer.keys.include?(:subscriptions) && subscription.present? && subscription.status!='canceled'
      subscription.plan = stripe_plan
      subscription.coupon=coupon_code if coupon_code.present?
      subscription.save
      stripe_customer.save
      update_attributes(stripe_plan_id: stripe_plan)
    else
      #existing stripe customer, no existing, non-canceled  subscription
      subscription = stripe_customer.subscriptions.create(plan: stripe_plan)
      subscription.save
      stripe_customer.save
      update_attributes(stripe_subscription_id: subscription.id, stripe_plan_id: stripe_plan)
    end
  end

  def stripe_unsubscribe!
    customer = Stripe::Customer.retrieve(stripe_customer_id)
    subscription = customer.subscriptions.retrieve(self.stripe_subscription_id)
    subscription.delete
    update_attributes(stripe_subscription_id: nil, stripe_plan_id: nil)
  rescue => e
    ExceptionNotifier.notify_exception e
    errors.add :base, e.message
  ensure
    return errors.empty?
  end


private

  def description membership_id=nil
    membership_id ||= user[:membership_id]
    "#{PAYMENT_DESCRIPTION} level #{membership_id}"
  end

  # Returns a URL suitable for providing to a vendor as a redirect back to DR
  def url host_with_port, path
    protocol = Rails.env.production? ? 'https' : 'http'
    "#{protocol}://#{host_with_port}#{path}"
  end

end
