class Usage::SecurityAnswer < ActiveRecord::Base
  

  belongs_to :user, class_name: "User", foreign_key: :user_id

  validates_presence_of :question_1_value, :question_2_value

  QUE_ANS_HASH = {question_1_id: :question_1_value, question_2_id: :question_2_value}
  QUE_ARRAY = [:question_1_id, :question_2_id]
end
