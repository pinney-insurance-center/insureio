module Usage::HierarchyMethodsAndScopes
  extend ActiveSupport::Concern

  included do

    scope :descendants_and_self, lambda {|user_id|
      joins('LEFT OUTER JOIN usage_ascendants_descendants ON users.id = usage_ascendants_descendants.descendant_id')
      .where('users.id = ? or usage_ascendants_descendants.ascendant_id = ?', user_id, user_id)
    }

    scope :join_ascendants, -> id {
      joins('JOIN usage_ascendants_descendants ON users.id = usage_ascendants_descendants.descendant_id')
      .where('usage_ascendants_descendants.ascendant_id = ?', id)
    }

    scope :siblings, lambda { |user| where(parent_id:user.parent_id).where('id != ?', user.id) }

    # Get all descendants of user.parent
    scope :nephews_and_siblings, -> user { user.parent_id ? join_ascendants(user.parent_id) : scoped }
    scope :siblings_and_nephews, -> user { nephews_and_siblings(user) }

    # Get all descendants of user.parent except immediate children
    scope :nephews_only, -> user { nephews_and_siblings(user).where(user.parent_id ? 'parent_id != ?' : 'parent_id IS NOT ?', user.parent_id) }

    scope :accessible, lambda { |user, access_type, suffix|
      is_view = (access_type.to_s == 'view')
      # Return everyone if super user
      return where(nil) if user.can?(is_view ? :super_view : :super_edit)
      # Infer access privileges
      self_permission       = is_view || user.can?(:edit_self)
      sibling_permission    = user.can?( (access_type+'_siblings'+suffix).to_sym )
      descendant_permission = user.can?( (access_type+'_descendants'+suffix).to_sym )
      nephew_permission     = user.can?( (access_type+'_nephews'+suffix).to_sym )
      # Return self or nothing if no access privilege exists
      if !(sibling_permission or descendant_permission or nephew_permission)
        return self_permission ? where(id:user.id) : where(id:nil)
      end
      # Build scope
      rel = all
      if user.parent_id && sibling_permission and descendant_permission and nephew_permission
        parent=User.unscoped.where(id:user.parent_id).first
        rel = where(id:parent.descendant_ids)
      else
        type_of_join=user.parent_id ? 'INNER' : 'LEFT'
        rel = joins("#{type_of_join} JOIN `usage_ascendants_descendants` ON `usage_ascendants_descendants`.`descendant_id` = `users`.`id`")
        statement_string = []
        statement_params = []
        if sibling_permission
          statement_string << (user.parent_id.nil? ? 'parent_id IS ?' : 'parent_id = ?')
          statement_params << user.parent_id
        end
        if descendant_permission
          statement_string << 'ascendant_id = ?'
          statement_params << user.id
        end
        if nephew_permission
          statement_string << 'ascendant_id in (?)'
          statement_params << siblings(user).select(:id).map(&:id)
        end
        if self_permission
          statement_string << 'users.id = ?'
          statement_params << user.id
        end
        rel = rel.where(statement_string.join(' OR '), *statement_params)
      end
      rel.uniq
    }

    # Scope Users which are viewable/editable by a given User
    scope :viewable_by, lambda { |user| accessible(user, 'view', '') }
    scope :editable_by, lambda { |user| accessible(user, 'edit', '') }

    #DEPRECATED. USE `viewable_by` and `editable_by`.
    scope :viewables, lambda { |user|
      SystemMailer.deprecated_method_warning rescue nil
      viewable_by(user)
    }
    scope :editables, lambda { |user|
      SystemMailer.deprecated_method_warning rescue nil
      editable_by(user)
    }

    # Scope Users whose resources are viewable/editable by a given User
    scope :whose_resources_viewable_by, lambda { |user| accessible(user, 'view', '_resources') }
    scope :whose_resources_editable_by, lambda { |user| accessible(user, 'edit', '_resources') }

  end#end of included scopes and class methods.

  def siblings
    self.class.siblings(self)
  end

  def nephews_and_siblings
    self.class.nephews_and_siblings(self)
  end
  alias_method :siblings_and_nephews, :nephews_and_siblings

  def nephews_only
    self.class.nephews_only(self)
  end

  # Returns whether this record is a nephew of arg user
  def nephew? user
    self.parent_id != user.parent_id and # not sibling
    user.parent and self.descendant?(user.parent) and # is descendant of user.parent
    not self.ascendant_ids.include?(user.id) # not descendant of user
  end

  def can_edit_user?(arg, params=nil)
    return true if self.can?(:super_edit)
    return false if arg.blank?
    arg_user = arg.kind_of?(User) ? arg : User.find(arg)
    return true if self.can?(:edit_self) and self.id === arg_user.id
    return true if self.can?(:edit_siblings) and self.parent_id === arg_user.parent_id
    return true if arg_user.ascendant_ids.include?(self.id)
    return true if self.id === arg_user.id && params && (
      (params['name'].nil? || User::SELF_EDITABLE_FIELDS.include?(params['name']) ) &&
      params.keys.all?{|k|
        k=='name' ||
        User::SELF_EDITABLE_FIELDS.include?(k)
      }
    )
    false
  end

  def can_view_user?(arg)
    arg_user = arg.kind_of?(User) ? arg : User.find_by_id(arg)
    return true if self.id === arg_user.id || self.can?(:super_edit) || self.can?(:super_view)
    return true if arg_user.ascendant_ids.include?(self.id)
    if (self.can_any?(:view_siblings, :edit_siblings)) && self.parent_id === arg_user.parent_id
      return true
    end
    false
  end

  def viewable?(arg)
    arg&.can_view_user?(self)
  end
  alias_method :viewable_by?, :viewable?

  def editable?(user)
    user&.can_edit_user?(self)
  end

  def is_editable_by_current_user
    User.find( Thread.current[:current_user_id] ).can_edit_user?(self)
  end
  alias_method :is_editable, :is_editable_by_current_user

  def can_view_descendants?
    can_any?(:view_descendants, :edit_descendants)
  end

  def can_view_siblings?
    can_any?(:view_siblings, :edit_siblings)
  end

  def can_view_nephews?
    can_any?(:view_nephews, :edit_nephews)
  end

  def can_view_descendants_resources?
    can_any? :view_descendants_resources, :edit_descendants_resources
  end

  def can_view_siblings_resources?
    can_any? :view_siblings_resources, :edit_siblings_resources
  end

  def can_view_nephews_resources?
    can_any? :view_nephews_resources, :edit_nephews_resources
  end

  # Make delayed job for updating ascendant associations (This is an after_save callback)
  def delayed_update_ascendant_associations force=false
    if self.parent_id_changed? or force
      if Rails.env.production?
        self._update_ascendant_associations # could be self.delay._update_ascendant_associations
      else
        self._update_ascendant_associations
      end
    end
  end

  def ascendant?(other_user)
    ascendant_ids.include?(other_user.id)
  end

  # Returns true if this user is a descendant of arg user
  def descendant? user
    reflection = User.reflect_on_association :descendants
    table = Arel::Table.new(reflection.options[:join_table])
    query = table \
      .where( table[reflection.foreign_key].eq(user.id) ) \
      .where( table[reflection.association_foreign_key].eq(self.id) ) \
      .project('*').take(1).to_sql
    results = self.class.connection.execute query
    results.first.present?
  end

  def descendant_ids
    self.descendants.select(:id).map(&:id)
  end

  def self.trace_ancestry(user, ancestor=nil)
    lineage = [user]
    while ( user.id != ancestor.try(:id) and user.parent ) do
      lineage << (user = user.parent)
    end
    lineage
  end

  # Delete all ascendant associations and recreate them
  def _update_ascendant_associations
    self.reload
    if parent.present?
      self.primogen_id = parent.primogen_id || parent.id
      self.ascendants = parent.ascendants.where('id != ?', id).select(:id) << parent
      # instruct children to update their ascendant associations as well
      self.children.each{|child|
        # protect against loops in ascendency
        break if child == self
        child._update_ascendant_associations
      }
    else
      self.ascendants = []
    end
  end
end
