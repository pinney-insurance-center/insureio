class Usage::AscendantAssociation < ActiveRecord::Base
  self.table_name = 'usage_ascendants_descendants'
  belongs_to :ascendant, class_name: 'User'
  belongs_to :descendant, class_name: 'User'
end