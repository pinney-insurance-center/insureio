class Usage::StaffAssignment < ApplicationRecord
  

  attr_accessor :administrative_assistant_name, :case_manager_name, :manager_name,
                :policy_specialist_name, :application_specialist_name, :underwriter_name,
                :insurance_coordinator_name

  belongs_to :agent,                    class_name: "User"
  belongs_to :owner,                    polymorphic: true
  belongs_to :administrative_assistant, class_name: "User"
  belongs_to :case_manager,             class_name: "User"
  belongs_to :manager,                  class_name: "User"
  belongs_to :policy_specialist,        class_name: "User"
  belongs_to :application_specialist,   class_name: "User"
  belongs_to :underwriter,              class_name: "User"
  belongs_to :insurance_coordinator,    class_name: "User"

  delegate :name, to: :administrative_assistant, prefix:true, allow_nil: true
  delegate :name, to: :case_manager,             prefix:true, allow_nil: true
  delegate :name, to: :manager,                  prefix:true, allow_nil: true
  delegate :name, to: :policy_specialist,        prefix:true, allow_nil: true
  delegate :name, to: :application_specialist,   prefix:true, allow_nil: true
  delegate :name, to: :underwriter,              prefix:true, allow_nil: true
  delegate :name, to: :insurance_coordinator,    prefix:true, allow_nil: true

  validates :owner, presence: true

  # Scope Users which are viewable by a given User
  scope :viewable_by, lambda { |user|
    if !(user.can?(:view_siblings) || user.can?(:view_descendants) || user.can?(:view_nephews))
      return where(:id => user.id)
    end
    rel = joins('INNER JOIN `usage_ascendants_descendants` ON `usage_ascendants_descendants`.`descendant_id` = `users`.`id`')
    if user.can?(:view_siblings) and user.can?(:view_descendants) and user.can?(:view_nephews)
      rel = rel.where('usage_ascendants_descendants.ascendant_id' => user.parent_id)
    else
      statement_string = []
      statement_params = []
      if user.can?(:view_descendants)
        statement_string << 'ascendant_id = ?'
        statement_params << user.id
      end
      if user.can?(:view_siblings)
        statement_string << 'parent_id = ?'
        statement_params << user.parent_id
      end
      if user.can?(:view_nephews)
        statement_string << 'ascendant_id in (?)'
        statement_params << siblings(user).select(:id).map(&:id)
      end
      rel = rel.where(statement_string.join(' OR '), *statement_params)
    end
    rel
  }
  #DEPRECATED. USE `viewable_by`.
  scope :viewables, lambda {|user| viewable_by(user) }

  def editable? user
    if owner_type == user.class.name && owner_id == user.id
      return user.can?(:edit_staff_assignment_self)
    else
      user.can?(:edit_staff_assignment) && owner.try(:editable?, user)
    end
  end

  def viewable? user
    (owner_type == user.class.name && owner_id == user.id) || owner.try(:viewable?, user)
  end

  # Returns true, false, or nil
  def include? user_or_id
    user = user_or_id.is_a?(Fixnum) ? User.find(user_or_id) : user_or_id
    includes_user_id = self.attributes.any?{|k,v| k.match(/_id\z/) && v==user.id }
  end

  # Returns the field on this model that matches the role_id supplied
  # Except for the hard-coded list.
  def user_id_for_role_id role_id
    if owner_type=='Crm::Case'
      self[symbol_for_role_id(role_id)]
    else
      case role_id
      when Enum::UsageRole::ADMINISTRATIVE_ASSISTANT_ID
        180#Maggie Scanlon
      when Enum::UsageRole::APPLICATION_SPECIALIST_ID
        154#A-Team Queue
      when Enum::UsageRole::UNDERWRITER_ID
        139#Rachel Sanfilippo
      when Enum::UsageRole::INSURANCE_COORDINATOR_ID
        154#A-Team Queue
      when Enum::UsageRole::CASE_MANAGER_ID
        126#Amber Alberts
      when Enum::UsageRole::AGENT_ID
        #If StaffAssignment's agent_id is nil, use owner's agent
        return self[symbol_for_role_id(role_id)] unless self[symbol_for_role_id(role_id)].nil?
        return self.owner.agent_id if self.owner.respond_to?(:agent_id)
        return self.owner.id if self.owner_type == "User"
      else
        self[symbol_for_role_id(role_id)]
      end
    end
  end

  def set_user_id_for_role_id role_id, user_id
    return if role_id.blank? || user_id.blank?
    return if symbol_for_role_id(role_id).blank?
    self[symbol_for_role_id(role_id)] = user_id
  end

  def symbol_for_role_id role_id
    case role_id
    when Enum::UsageRole::UNDERWRITER_ID
      :underwriter_id
    when Enum::UsageRole::APPLICATION_SPECIALIST_ID
      :application_specialist_id
    when Enum::UsageRole::INSURANCE_COORDINATOR_ID
      :insurance_coordinator_id
    when Enum::UsageRole::ADMINISTRATIVE_ASSISTANT_ID
      :administrative_assistant_id
    when Enum::UsageRole::CASE_MANAGER_ID
      :case_manager_id
    when Enum::UsageRole::MANAGER_ID
      :manager_id
    when Enum::UsageRole::AGENT_ID
      :agent_id
    end
  end

  def self.default_user_id_for_role_id role_id
    case role_id
    when Enum::UsageRole::ADMINISTRATIVE_ASSISTANT_ID
      180#Maggie Scanlon
    when Enum::UsageRole::APPLICATION_SPECIALIST_ID
      154#A-Team Queue
    when Enum::UsageRole::UNDERWRITER_ID
      139#Rachel Sanfilippo
    when Enum::UsageRole::INSURANCE_COORDINATOR_ID
      154#A-Team Queue
    end
  end

  # Returns an array of all the users on this StaffAssignment
  def get_staff
    staff_user_types = [:administrative_assistant, :case_manager, :manager, :application_specialist, :underwriter, :insurance_coordinator ]
    assigned_staff = []
    staff_user_types.each do |staff_role|
      assigned_staff << self.try(staff_role) unless self.try(staff_role).blank?
    end
    return assigned_staff
  end

  def ids(include_agent_id=false)
    cols = self.class.columns.select{|c| c.name =~ /_id$/ }
    cols.reject!{|c| c.name == 'owner_id' }
    cols.reject!{|c| c.name == 'agent_id' } unless include_agent_id
    cols.map{|c| self.send(c.name)}
  end
end
