class Usage::StripeEvent < ActiveRecord::Base
  

  attr_accessor :params

  def self.build params, raw_post
    instance = new
    instance.params   = params
    instance.body     = raw_post
    instance.evt_id   = params[:id]
    instance.evt_type = params[:type]
    instance
  end

  def process
    user.update_payment_due!
  end

  def processable?
    params[:livemode] == true && # Handle tests in non-prod environments; handle lives in prod environment
    params[:type]     == 'charge.succeeded' &&
    data[:paid]   == true &&
    data[:amount] == user.try(:amount).try(:*, 100).try(:to_i)
    #need to add caveat here for users with a discount
  end

  def user
    @user ||= (customer_id && User.where(stripe_customer_id:customer_id).first)
  end

private

  def customer_id
    data.try(:[], :card).try(:[], :customer)
  end

  def data
    params[:data].try(:[], :object)
  end

  def params
    @params ||= JSON.parse(body).with_indifferent_access
  end
end
