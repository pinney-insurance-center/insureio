class Reporting::Search < ActiveRecord::Base

  include OwnedStereotype
  audit_logger_name 'reporting'


  class Data < HashWithIndifferentAccess
    def initialize hash={}
      merge! hash
      each{|k,v| self[k] = self.class.new(v) if v.is_a?(Hash) and not v.is_a?(self.class)}
    end

    def method_missing meth, *args, &block
      if Hash.public_instance_methods.include?(meth) or meth == :to_ary
        super meth, *args, &block
      elsif meth.to_s =~ /=$/
        send :[]=, meth.slice(0...-1), *args
      elsif args.empty?
        fetch meth, Data.new
      else
        super meth, *args, &block
      end
    end

    def compact!
      delete_if do |k,v|
        if k == 'tags'
          v = TagSet.new(v)
        elsif v.is_a? Data
          v.compact!
        end
        v.blank? || v=='0' || v==0
      end
    end

    def to_s
      empty? ? "" : super
    end

    def zip
      self[:zip]
    end

    #This hash is intended to nest only one level deep (several hashes within one root-level hash).
    #So when we duplicate, it needs to be deeper than dup, but not fully recursive. Just deep-ish.
    def deepish_dup
      hash={}
      self.keys.each do |k|
        hash[k]=self[k].dup
      end
      Data.new hash
    end
  end

  attr_accessor :warnings
  after_initialize ->{ self.warnings||=[] }

  serialize :criteria, Data
  serialize :fields_to_show, Data

  validates    :name, presence: true, allow_blank: false
  validate     :specifies_criteria
  validate     :criteria_are_sane
  validate     :specifies_fields_to_show
  validate     :fields_to_show_match_whitelist

  # Used for the view
  def agent; User.find(criteria.consumer['agent_id']) if criteria.consumer['agent_id'].present?; end

  def criteria= hash
    self[:criteria] = Data.new hash
  end


  def fields_to_show= hash
    super Data.new(hash)
  end

  # Used for the view
  def brand; Brand.find(criteria.consumer['brand_id']) if criteria.consumer['brand_id'].present?; end

  # Used for the view
  def sales_support; User.find(criteria['sales_support_id']) if criteria['sales_support_id'].present?; end

  def specifies_criteria
    criteria.compact!
    errors.add(:base,"You must specify at least one search criterion (limitation)") if self.criteria.empty?
  end

  def specifies_fields_to_show
    fields_to_show.compact!
    errors.add(:base,"You must specify at least one field to show in your results") if self.fields_to_show.empty?
  end

  #Reports created via API users versus the gui may try to add their own fields.
  #This should produce a warning, but no errors up front.
  def fields_to_show_match_whitelist
    gui_editor_fields=Enum::ReportingByCaseShowableFields.all
    rogue_fields=[]
    disallowed_fields=[]
    disallowed_models=[]
    fields_to_show.each do |model_key,field_group|
      model_mapping=Enum::ReportingByCaseModelMapping.ewhere(model_key:model_key).first
      if !model_mapping
        disallowed_models.push model_key
        next
      end
      field_group.each do |field_name,is_shown|
        next if is_shown.to_i !=1
        next if gui_editor_fields.select{|f| f.model_key==model_key && f.field_name==field_name }.present?
        rogue_fields.push "#{model_key}.#{field_name}"
        assoc_chain=model_mapping&.map_for_join
        if !terminal_model_from_assoc_chain(assoc_chain,Crm::Case)&.new&.respond_to?(field_name)
          disallowed_fields.push "#{model_key}.#{field_name}"
        end
      end
    end
    if rogue_fields.present?
      warning_msg="The model/field combinations [ #{rogue_fields.join(', ')} ] do not have checkboxes in the"+
        " \"Fields To Show\" section of the report editor gui,"+
        " so setting these fields cannot be undone through the editor gui.\n"+
        "Use of this field has not been vetted by our dev team and may also cause issues when trying to run your report."
      self.warnings.push warning_msg
    end
    if disallowed_fields.present?
      errors.add :base, "The model/field combinations [ #{disallowed_fields.join(', ')} ] in the `fields_to_show` list are not valid."
    end
    if disallowed_models.present?
      self.warnings.push "The models [ #{disallowed_models.join(', ')} ] in the `fields_to_show` list are not valid and will be skipped."
    end
    return disallowed_fields.present?
  end

  #This method should sanity check the criteria before saving or attempting to retrieve results.
  def criteria_are_sane
    if criteria.staff_assignment && criteria.staff_assignment[:column_name] && !criteria.staff_assignment[:user_id]
      #It's never useful to check for custom staff assignment records that
      #have a nil id for a specific role, since in practice, someone will still fill that role.
      #Rather than return an error, this will silently ignore the unusable criterion.
      self.criteria.staff_assignment.delete('column_name')
    end
    criteria.compact!
  end

  #Return the ids of matching cases, opportunities, and consumers.
  #This is sufficient for building a partial summary (matching record counts).
  #This is also enough information to allow a second query to immediately
  #start gathering fields for display, and not have to re-filter by the criteria.
  def get_matching_record_ids_for_user(current_user)
    consumer_ids=[]

    kases   = scope_cases
    kases   = kases.merge( kases.viewable_by(current_user) ).uniq
    kase_ids= kases.select('crm_cases.id, crm_cases.consumer_id')
    kase_ids= kase_ids.map{|k| consumer_ids<<k.consumer_id; k.id}

    opps    = scope_opportunities
    opps    = opps.merge( opps.viewable_by(current_user) ).uniq
    opp_ids = opps.select('quoting_details.id, quoting_details.consumer_id')
    opp_ids = opp_ids.map{|k| consumer_ids<<k.consumer_id; k.id}

    consumer_ids=consumer_ids.uniq

    [consumer_ids, kase_ids, opp_ids]
  end

  #This function should not be called if there are no records,
  #because in that instance, it would be irrelevant.
  def get_policies_summary kase_ids, opp_ids
    kases=[]
    opps=[]
    kase_ids.in_groups_of(100) do |id_group|
      id_group.compact!
      kases+=Crm::Case.where(id:id_group).
        preload(:current_details)
    end
    opp_ids.in_groups_of(100) do |id_group|
      id_group.compact!
      opps+=Quoting::Quote.where(id:id_group)
    end
    records=kases+opps

    summary={
      closed_count:0,
      test_count:0,
      dup_count:0,
      preexisting_count:0,
      record_count:records.length,
      stages_to_display:Enum::SalesStage.all.map(&:id),
      total_premium:0,
      record_count_in_stage:{},
      ratio_in_stage:{},
      total_premium_in_stage:{},
      total_times_in_stage:{},
      total_closed_in_stage:{},
      avg_time_in_stage:{},
    }

    return summary unless records.length

    count_tests=!criteria.case.exclude_test.present?
    count_dups =!criteria.case.exclude_duplicate.present?
    count_pres = criteria.case.include_preexisting.present?

    #If current sales stage is specified as a criterion,
    #then don't collect info for other stages. We know matching records
    #already passed all previous stages and didn't progress to any later stages.
    only_ss_id  =criteria.case.sales_stage_id.is_a?(Integer) && criteria.case.sales_stage_id
    only_ss_id||=criteria.case.status_type_category_id && Enum::StatusTypeCategory.find(criteria.case.status_type_category_id)
    #If min sales stage is specified as a criterion,
    #then don't collect info for earlier stages.
    min_ss_id=criteria.case.sales_stage_id_min.is_a?(Integer) && criteria.case.sales_stage_id_min
    if only_ss_id
      summary[:stages_to_display]=[only_ss_id]
    elsif min_ss_id
      summary[:stages_to_display].select!{|ss_id| ss_id>=min_ss_id }
    end

    records.each do |k|
      summary[:stages_to_display].each do |ss_id|
        next unless k.sales_stage_id.to_i>=ss_id
        summary[:record_count_in_stage][ss_id]||=0
        summary[:record_count_in_stage][ss_id]+=1
        #Last timestamp is for submitted to carrier, so time in stage can only be calculated for earlier stages.
        if(ss_id<Enum::SalesStage.id('Submitted To Carrier'))
          summary[:total_times_in_stage][ss_id]||=0
          ss_name=Enum::SalesStage.find(ss_id).name.gsub(' ','_').downcase
          summary[:total_times_in_stage][ss_id]+=k.send("time_in_#{ss_name}_stage",false,true).to_i
        end
        summary[:total_premium_in_stage][ss_id]||=0
        unless k.sales_stage_id==ss_id && k.active
          summary[:total_premium_in_stage][ss_id]+=k.current_details.try(:annualized_premium).to_i
        end
        summary[:total_closed_in_stage][ss_id]||=0
        summary[:total_closed_in_stage][ss_id]+=1 if k.sales_stage_id==ss_id && !k.active
      end
      summary[:total_premium]+=k.current_details.try(:annualized_premium).to_i
      if !k.active
        summary[:closed_count]     +=1
        summary[:test_count]       +=1 if count_tests && k.is_test
        summary[:dup_count]        +=1 if count_dups  && k.is_duplicate
      end
      summary[:preexisting_count]+=1 if count_pres  && k.is_a?(Crm::Case) && k.is_a_preexisting_policy
    end

    summary[:stages_to_display].each do |ss_id|
      summary[:ratio_in_stage][ss_id]=summary[:record_count_in_stage][ss_id].to_i*100/summary[:record_count]
      if(ss_id<Enum::SalesStage.id('Submitted To Carrier'))
        avg_time=summary[:total_times_in_stage][ss_id].to_i/summary[:record_count]
        summary[:avg_time_in_stage][ss_id]=ActiveRecord::Base.dhm_between Time.now, (Time.now+avg_time)
      end
    end
    summary
  end

  def get_fields_for_display kase_ids, opp_ids
    fields=[]
    get_fields_for_scope= lambda do |s|
      s.each do |k|
        the_row=get_results_row(k).map(&:to_s).map(&:titlecase)
        fields<< the_row
      end
    end

    #Querying in batches limits the extent of table locking.
    #It means a somewhat slower response to the user,
    #but ensures the entire system doesn't slow down.
    kase_ids.in_groups_of(100) do |id_group|
      id_group.compact!
      @scope=Crm::Case.where(id:id_group)
      preload_associations Crm::Case
      kases=@scope
      get_fields_for_scope.call(@scope)
    end

    opp_ids.in_groups_of(100) do |id_group|
      id_group.compact!
      @scope=Quoting::Quote.where(id:opp_ids)
      preload_associations Quoting::Quote
      opps=@scope
      get_fields_for_scope.call(@scope)
    end

    [headings,fields]
  end

  def consumers_summary(user)
    consumer_ids = get_matching_record_ids_for_user(user)[0]
    filtered_consumer_ids = Consumer.where(id:consumer_ids,marketing_unsubscribe:false).pluck(:id)
    skipped_ids = (consumer_ids - filtered_consumer_ids)
    [consumer_ids, filtered_consumer_ids, skipped_ids]
  end

private

  def headings
    columns=[]
    fields_to_show.each do |model_name,fields|
      if fields_to_show[model_name].values.map(&:to_i).include?(1)
        fields.each do |field_name,v|
          columns << [model_name, field_name] if v.to_i==1
        end
      end
    end
    columns
  end

  def get_results_row kase
    #Loop through `fields_to_show`, adding each item to an array.
    row=[]
    fields_to_show.each do |model_key,fields_for_model|
      next unless fields_for_model.values.map(&:to_i).include?(1)
      begin
        if kase.is_a?(Crm::Case)
          assoc_map=Enum::ReportingByCaseModelMapping.ewhere(model_key:model_key).first.map_for_access
          associated_object=object_from_assoc assoc_map, kase
        elsif kase.is_a?(Quoting::Quote) && !model_key.in?(['contacted_details','app_fulfillment_details','submitted_details','placed_details','owner','owner_contact'])
          assoc_map=(Enum::ReportingByOpportunityModelMapping.ewhere(model_key:model_key).first ||
                     Enum::ReportingByCaseModelMapping.ewhere(model_key:model_key).first ).map_for_access
          associated_object=object_from_assoc assoc_map, kase
        else
          associated_object=Reporting::Search::Data.new#an object that never generates a NoMethodError
        end
      rescue =>ex
        audit level: :error, body:{
          msg:ex, backtrace:ex.backtrace, model_key: model_key,
          starting_record_class:kase.class.name, starting_record_id:kase.id
        }
        ExceptionNotifier.notify_exception ex, data:{message: "Got an exception/timeout when trying to find association 'model_key' for object #{kase.class.name} #{kase.id}." }
        next
      end
      fields_for_model.each do |field_name,v|
        next unless v.to_i==1
        value=''
        begin#Possible types that may get database errors:
          if associated_object.is_a?(ActiveRecord::Base)
            value=(associated_object.respond_to?(field_name) ? associated_object.try(field_name) : '')
          end
        rescue =>ex
          audit level: :error, body:{
            msg:ex, backtrace:ex.backtrace, model_key: model_key, field_name:field_name,
            starting_record_class:kase.class.name, starting_record_id:kase.id,
            associated_object_class:associated_object.class.name, associated_object_id:associated_object.id
          }
          if ex.is_a?(ActiveRecord::StatementInvalid) || ex.is_a?(Timeout::Error) || ex.message=~/closed MySQL connection/
            ActiveRecord::Base.connection.reconnect! if !!(ex.message=~/Mysql2::Error: closed MySQL connection/)
            ExceptionNotifier.notify_exception ex, data:{message: "Got an exception/timeout when trying to run method '#{field_name}' on #{associated_object.class.name} id #{associated_object.id}." }
            value='(Timeout Error. Please notify support team.)'
          else
            ExceptionNotifier.notify_exception ex, data:{message: "Got an exception when trying to run method '#{field_name}' on #{associated_object.class.name} id #{associated_object.id}.\n\n#{ex}" }
            value='(Error. Please notify support team.)'
          end
        end
        value="" if value.is_a?(Hash)
        value=value.strftime('%m/%d/%Y %H:%M') if value.is_a?(Time)
        value=value ? 'Yes' : 'No' if !!value==value#value is a True/False class
        value=value.to_a if value.is_a?(ActiveRecord::Associations::CollectionProxy)
        value=value.map(&:to_s).join(",\n") if value.is_a?(Array)
        row << value
      end
    end
    row
  end

  def scope_cases
    criteria.compact!
    @scope = Crm::Case.unscoped
    map_criteria_to_scope Enum::ReportingByCaseModelMapping.all, Crm::Case
    audit "Running query: #{@scope.to_sql}"
    @scope
  end

  def scope_opportunities
    criteria.compact!
    @scope = Quoting::Quote.where(sales_stage_id:1, case_id:nil, include_in_forecasts:true).joins(:consumer)
    map_criteria_to_scope Enum::ReportingByOpportunityModelMapping.all, Quoting::Quote
    audit "Running query: #{@scope.to_sql}"
    @scope
  end

  def map_criteria_to_scope the_map, the_model
    tmp_criteria=criteria.deepish_dup
    if the_model==Quoting::Quote && tmp_criteria.case.agent_id.present?
      tmp_criteria[:consumer]=Data.new if tmp_criteria[:consumer].nil?
      tmp_criteria.consumer.agent_id=tmp_criteria.case.agent_id
    end
    the_map.each do |map_row|
      next if map_row.for_preloading_only
      key=map_row.model_key
      association=map_row.map_for_join
      join=false
      if tmp_criteria[key].present?
        join=true
        call_add_where=true

        #This `case` statement handles fields that need special treatment.
        #The rest will skip over this logic.
        case key
        when 'case'
          if the_model==Crm::Case && tmp_criteria.case.agent_id.present?
            @scope = @scope.where("crm_cases.agent_id=?",tmp_criteria.case.delete('agent_id'))
          elsif the_model==Quoting::Quote && tmp_criteria.case.agent_id.present?
            @scope = @scope.joins(:consumer)
            tmp_criteria.case.delete('agent_id')
          end
          if tmp_criteria.case.exclude_duplicate.present?
            add_where('is_duplicate', false, table_name_from_assoc(association,the_model))
            tmp_criteria.case.delete('exclude_duplicate')
          end
          if tmp_criteria.case.exclude_test.present?
            add_where('is_test', false, table_name_from_assoc(association,the_model))
            tmp_criteria.case.delete('exclude_test')
          end
          if the_model==Crm::Case && !tmp_criteria.case.include_preexisting.present?
            add_where('is_a_preexisting_policy', false, table_name_from_assoc(association,the_model))
          end
        when 'current_details'
          cd=tmp_criteria.current_details
          annualized_min=cd.delete('annualized_premium_min')
          annualized_max=cd.delete('annualized_premium_max')
          if the_model==Crm::Case
            #By default rails does not seem to use the conditions on an association when joining,
            #so the condition must be added manually in the where clause.
            @scope=@scope.where('quoting_details.sales_stage_id=crm_cases.sales_stage_id')
          end
          #Since `annualized_premium` is a derived attribute, and not a database column,
          #it needs translating into restrictions on actual database columns.
          if annualized_min || annualized_max
            where_clause_for_a_p=''
            Enum::PremiumModeOption.all.each do |o|
              where_clause_for_a_p+="( quoting_details.premium_mode_id=#{o.id} /*#{o.name}*/ "
              if annualized_min
                modal_min=annualized_min.to_f*o.months/12
                where_clause_for_a_p+="AND quoting_details.planned_modal_premium>=#{modal_min} "
              end
              if annualized_max
                modal_max=annualized_max.to_f*o.months/12
                where_clause_for_a_p+="AND quoting_details.planned_modal_premium<=#{modal_max} "
              end
              where_clause_for_a_p+=') OR '
            end
            where_clause_for_a_p=where_clause_for_a_p[0..-4]#remove last 3 characters
            @scope=@scope.where(where_clause_for_a_p)
          end
        when 'consumer'
          #get birth from age (a smaller number for age means a bigger number for date, so min and max switch)
          tmp_criteria.consumer['birth_max'] = tmp_criteria.consumer.delete('age_min').to_i.years.ago if tmp_criteria.consumer['age_min']
          tmp_criteria.consumer['birth_min'] = tmp_criteria.consumer.delete('age_max').to_i.years.ago if tmp_criteria.consumer['age_max']
          tags = TagSet.new( tmp_criteria.consumer.delete('tags') ).to_a.map(&:strip)
          if tags.present?
            tagged_consumer_ids = Tag.where(value: tags, person_type: 'Consumer').pluck(:person_id)
            tagged_consumer_ids<< 0 unless tagged_consumer_ids.present?
            #The following sql comment is for debugging purposes, to indicate where this list of ids came from,
            #since the query will not include "JOIN" or mention the "tags" table.
            @scope = @scope.where('consumers.id IN (?) /* see tag constraints */', tagged_consumer_ids)
          end
        when 'staff_assignment'
          if tmp_criteria.staff_assignment.column_name.present?
            @scope = @scope.where("usage_staff_assignments.#{tmp_criteria.staff_assignment.column_name} = #{tmp_criteria.staff_assignment.user_id}")
          else
            #get list of '*_id' columns from staff assignment model
            cols = Usage::StaffAssignment.columns.map(&:name).select{|name| name =~ /_id/ }
            #create custom where clause
            query = cols.map{|c| "usage_staff_assignments.#{c} = #{tmp_criteria['staff_assignment']['user_id']}" }.join(" OR ")
            @scope = @scope.where(query)
          end
          call_add_where=false
        end
        tmp_criteria[key].each{|k,v| add_where k, v, table_name_from_assoc(association,the_model) } if call_add_where
      end
      @scope=@scope.joins(association) if join
    end
  end

  def preload_associations the_model
    model_map= the_model==Crm::Case ? Enum::ReportingByCaseModelMapping : Enum::ReportingByOpportunityModelMapping

    model_map.all.each do |map_row|
      fields_to_show_for_model=self.fields_to_show[map_row.model_key]
      next unless fields_to_show_for_model.present? && fields_to_show_for_model.values.map(&:to_i).include?(1)
      @scope=@scope.preload(map_row.map_for_preload)
    end
    #Since we are phasing out searches by opportunity records, it's probably fine to leave those slightly less optimized for the moment.
    if the_model==Crm::Case
      field_map=Enum::ReportingByCaseShowableFields
      field_map.all.each do |map_row|
        fields_to_show_for_model=self.fields_to_show[map_row.model_key]
        next unless fields_to_show_for_model.present?
        next unless map_row.extra_map_for_preload && fields_to_show_for_model[map_row.field_name].to_i==1
        @scope=@scope.preload(map_row.extra_map_for_preload)
      end
    end
  end

  #@param assoc could be nil, a string/symbol association name, or a nested hash of model association names
  #All values passed in for `assoc` should originate from the relevant `ReportingModelMapping`.
  #So this method should not fail, as long as enums are kept up to date.
  #@return an activerecord class
  def terminal_model_from_assoc_chain assoc, originating_model
    if assoc.is_a?(String) || assoc.is_a?(Symbol)
      originating_model.reflect_on_association(assoc.to_sym).class_name.constantize
    elsif assoc.nil?
      originating_model
    else
      assoc_model_name=assoc.keys.first.to_sym
      assoc_model=originating_model.reflect_on_association(assoc_model_name).class_name.constantize
      next_assoc=assoc.values.first
      terminal_model_from_assoc_chain next_assoc, assoc_model
    end
  end

  #@param assoc could be nil, a string/symbol association name, or a nested hash of model association names
  #All values passed in for `assoc` should originate from `#map_for_join` on the relevant `ReportingModelMapping`.
  #So this method should not fail, as long as enums are kept up to date.
  #@return a database table name
  def table_name_from_assoc assoc, originating_model
    terminal_model_from_assoc_chain(assoc, originating_model).table_name
  end


  #@param assoc could be nil, a string/symbol association name, or a nested hash of model association names
  #All values passed in for `assoc` should originate from `#map_for_access` on the relevant `ReportingModelMapping`.
  #So the possible failure point is if a particular object lacks an associated object.
  #@return an activerecord class
  def object_from_assoc assoc, originating_object
    if assoc.is_a?(String) || assoc.is_a?(Symbol)
      originating_object.try(assoc)
    elsif assoc.nil?
      originating_object
    else
      assoc_object_name=assoc.first
      assoc_object=originating_object.try(assoc_object_name)
      next_assoc=assoc.keys.first
      object_from_assoc next_assoc, assoc_object
    end
  end

  def process_date string, min_or_max
    if    string=~ /days ago$/
      output=string.to_i.days.ago
    elsif string=~ /today/
      output=0.days.ago
    elsif string=~ /yesterday/
      output=1.days.ago
    elsif string=~ /ages ago/
      output=50.years.ago

    elsif string=~ /start of this week/
      output=0.days.ago.beginning_of_week
    elsif string=~ /start of this month/
      output=0.days.ago.beginning_of_month
    elsif string=~ /start of this quarter/
      output=0.days.ago.beginning_of_quarter
    elsif string=~ /start of this year/
      output=0.days.ago.beginning_of_year

    elsif string=~ /end of last week/
      output=1.week.ago.end_of_week
    elsif string=~ /end of last month/
      output=1.month.ago.end_of_month
    elsif string=~ /end of last quarter/
      output=4.months.ago.end_of_quarter
    elsif string=~ /end of last year/
      output=1.year.ago.end_of_year

    elsif string=~ /start of last week/
      output=1.week.ago.beginning_of_week
    elsif string=~ /start of last month/
      output=1.month.ago.beginning_of_month
    elsif string=~ /start of last quarter/
      output=4.months.ago.beginning_of_quarter
    elsif string=~ /start of last year/
      output=1.year.ago.beginning_of_year
    else
      #convert to date.
      output=string.to_date
    end
    output= min_or_max=='max' ? output+1.days : output
    output=output.beginning_of_day
    rescue
      #return original value if it can't be made into a formatted date string.
      output=string
  end

  def add_where field, value, table_name=nil, fuzzy_search=false
    return if value.blank? && value!=false

    column_for_criterion=field.sub(/_(max|min)$/,'')
    unless ActiveRecord::Base.connection.column_exists?( table_name, column_for_criterion )
      logger.error "Report id #{self.id||'-'} attempted to set a criterion for nonexistent column #{table_name}.#{column_for_criterion}. Skipping."
      return
    end

    min_or_max=field.match(/_(max|min)$/).try([].to_s,1)#returns 'min', 'max', or nil

    if min_or_max
      value=process_date value, min_or_max
    end

    if field =~ /_max$/
      operator = '<='
      field = field.sub(/_max$/,'')
    elsif field =~ /_min$/
      operator = '>='
      field = field.sub(/_min$/,'')
    elsif field =~ /_name$/ || fuzzy_search
      operator = 'LIKE'
      value = value.gsub(/^|\s+|$/,'%')
    else
      operator = '='
    end
    field = table_name + '.' + field if table_name
    @scope = @scope.where "#{field} #{operator} ?", value
  end

end
