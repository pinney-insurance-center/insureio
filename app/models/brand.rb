class Brand < ActiveRecord::Base

  OWNER_ID_COLUMN=:owner_id
  include ContactableStereotype
  include OwnedStereotype

  validate     :enforce_multiple_brands_permission, on: :create, if: :owner_id
  validates_presence_of :full_name

  before_create -> { self.tenant_id ||= owner.tenant_id }, if: :owner
  before_create :set_preferred_insurance_division_subdomain
  after_save :give_owner_automatic_membership

  default_scope -> {
    tenant_id=Thread.current[:tenant_id] rescue nil
    current_user_acts_across_tenancies=Thread.current[:current_user_acts_across_tenancies] rescue nil
    if tenant_id && !current_user_acts_across_tenancies
      where(active_or_enabled:true).
      where(tenant_id:tenant_id)
    else
      where(active_or_enabled:true)
    end
  }

  scope :viewable_by, lambda { |user|
    if user.super?
      where('TRUE')
    else
      where("
        (owner_id = ?)
        OR (ownership_id in (?) AND owner_id in (?))",
        user.id,
        [Enum::Ownership::global_id, Enum::Ownership::user_and_descendants_id],
        user.ascendant_ids
      )
    end
  }
  #DEPRECATED. USE `viewable_by`.
  scope :viewables, lambda {|user|
    SystemMailer.deprecated_method_warning rescue nil
    viewable_by(user)
  }

  scope :not_private, lambda{
    where(ownership_id: Enum::Ownership.id('global','user and descendants') )
  }

  

  delegate :city, :zip, :state, to: :primary_address, allow_nil: true, prefix: false

  validates_presence_of :name

  # Associations
  belongs_to              :owner,               class_name: "User",            inverse_of: :owned_brands
  belongs_to              :initial_status_type, class_name: "Crm::StatusType"

  has_many                :consumers,           class_name: "Consumer",        foreign_key: :brand_id
  has_many                :snail_mail_messages, class_name: "Marketing::SnailMail::Message"
  has_many                :attachments,         class_name: "Attachment",      as: :person
  has_one                 :logo_file,           ->{ where 'file_name LIKE "brand_%_logo.%" OR file_name LIKE "profile_%_logo.%" ' },     class_name: "Attachment",      as: :person
  has_one                 :header_file,         ->{ where 'file_name LIKE "brand_%_header.%" OR file_name LIKE "profile_%_header.%" ' }, class_name: "Attachment",      as: :person

  has_and_belongs_to_many :members,             class_name: 'User',            join_table: :brands_users
  has_and_belongs_to_many :lead_types,          -> { uniq }, class_name: "Crm::LeadType",   join_table: :crm_lead_types_brands
  has_and_belongs_to_many :referrers,           -> { uniq }, class_name: "Crm::Referrer",   join_table: :crm_referrers_brands


  # Expects a data url.
  #Our current frontend uploader uses canvas, and converts all images to png.
  #As this may be subject to change, format is derived from the data_url, then appended to the filename.
  def logo_data_url= data_url
    logo_file.destroy if logo_file.present?#We want to replace, not keep both.
    new_logo_file=DataUrlFile.new(data_url)
    attachment = Attachment.create person: self, file_for_upload:new_logo_file, file_name:"brand_#{id}_logo.#{new_logo_file.extension}"
  end

  def header_data_url= data_url
    header_file.destroy if header_file.present?#We want to replace, not keep both.
    new_header_file=DataUrlFile.new(data_url)
    attachment = Attachment.create person: self, file_for_upload:new_header_file, file_name:"brand_#{id}_header.#{new_header_file.extension}"
  end

  def header
    return '' unless header_url
    "<img width='540' src=\"#{APP_CONFIG['base_url']}#{header_url}\" />"
  end

  def header_small
    return '' unless header_url
    "<img width='250' src=\"#{APP_CONFIG['base_url']}#{header_url}\" />"
  end

  def logo_medium
    return '' unless logo_url
    "<img src=\"#{APP_CONFIG['base_url']}#{logo_url}\" />"
  end

  def logo_thumb
    return '' unless logo_url
    "<img src=\"#{APP_CONFIG['base_url']}#{logo_url}\" />"
  end

  def logo_url
    f=logo_file
    if f
      #Use the `AttachmentsController#show` action,
      #which hides the secure AWS url.
      #The user's key allows authorized consumers to view the attachment.
      "/attachments/#{f.id}.#{f.extension}?key=#{owner.try(:quoter_key)}"
    else
      '/logos/medium/insurance_division.png'
    end
  end
  alias_method :logo_thumb_url, :logo_url

  def header_url
    f=header_file
    if f
      #Use the `AttachmentsController#show` action,
      #which hides the secure AWS url.
      #The user's key allows authorized consumers to view the attachment.
      "/attachments/#{f.id}.#{f.extension}?key=#{owner.try(:quoter_key)}"
    else
      nil
    end
  end
  alias_method :header_image_url, :header_url

  def link
    "<a href=\"#{self.website}\">#{self.website}</a>"
  end

  def give_owner_automatic_membership#they can cancel this later, but it seems intuitive that they be able to use it immediately after creating it.
    self.owner.brands << self if self.owner && !self.owner.brands.include?(self)
  rescue ActiveRecord::RecordNotUnique =>ex
    #Do nothing. It's fine.
  end

  def preferred_insurance_division_subdomain= subdomain_string
    if preferred_subdomain_set_already
      already_changed_msg="can be changed only once without special permission from an Insureio administrator."
      self.errors.add :preferred_insurance_division_subdomain, already_changed_msg
      return
    elsif subdomain_string.blank? || subdomain_string.length>20 || !subdomain_string.match(/\A\w\Z/)
      wrong_format_msg="must consist of only letters, numbers, and underscores, and be no longer than 20 characters."
      self.errors.add :preferred_insurance_division_subdomain, wrong_format_msg
      return
    end
    self[:preferred_insurance_division_subdomain]=subdomain_string
    self.preferred_subdomain_set_already=true
  end

  REFERRAL_BRAND_ID=107#Used for distributing referral leads.
  NMB_ID = 21264

private

  def enforce_multiple_brands_permission
    if owner && !owner.can?(:brand_multiple) && Brand.where(owner_id:owner.try(:id)).count() > 0
      errors.add :base, "This user (#{owner_id}) is permitted only one brand. (S)he has at least one brand already."
      return false
    end
  end

  def set_preferred_insurance_division_subdomain
    return if self.preferred_insurance_division_subdomain.present?
    str=(self.name||self.company)
    str=str.gsub(/\sinc\./i,'').gsub(/(\W+)/,' ').strip.gsub(/\s+/,'_').downcase
    self[:preferred_insurance_division_subdomain]=str
  end

end
