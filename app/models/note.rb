class Note < ActiveRecord::Base
  

  belongs_to_enum :note_type, class_name: "Enum::NoteType"
  belongs_to      :notable,   polymorphic:true
  belongs_to      :creator,   class_name: "User"
  before_update   :note_if_critical_flag_was_cleared

  RESTRICTED_TYPES=[Enum::NoteType.id('exam'), Enum::NoteType.id('admin'), Enum::NoteType.id('licensing')]

  scope :viewable_by, lambda { |user|
    where 'confidential IS NOT TRUE OR (confidential IS TRUE AND creator_id = ?)', user.id
  }

  scope :regarding, lambda { |notable, user|
    where('notable_type=? AND notable_id = ?', notable.class.name, notable.id)
    .viewable_by(user)
  }

  scope :of_type, lambda { |type|
    where('note_type_id=?', Enum::NoteType.id(type) )
  }

  scope :except_restricted_types, lambda {
    where 'note_type_id NOT IN (?) OR note_type_id IS NULL', RESTRICTED_TYPES
  }

  delegate :name, to: :creator,   prefix:true, allow_nil:true
  delegate :name, to: :note_type, prefix:true, allow_nil:true
  delegate :photo_thumb_url, to: :creator, prefix:true, allow_nil:true

  def preview
    self.text.try(:truncate, 40, separator: ' ', omission: '...')
  end
  alias_method :title, :preview

  def note_type_name= name
    self.note_type_id=Enum::NoteType.id(name)
  end

  def note_if_critical_flag_was_cleared
    return unless self.critical_changed? && !self.critical
    u_id=Thread.current[:current_user_id]
    self.text||=''
    self.text+="\n\n*Was marked as no longer critical by #{User.find(u_id).full_name} (id:#{u_id}) at #{Time.now.strftime('%Y-%m-%d %H:%M')}."
  end

  def creator_name
    creator.try(:name)
  end

  pseudo_datetime_accessor :created_at

end