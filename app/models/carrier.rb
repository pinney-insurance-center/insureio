class Carrier < ActiveRecord::Base

  validates_presence_of :name

  default_scope -> { where(deleted: [false, nil]) }
  scope :for_naic_codes,      ->(naic_codes) { where('naic_code IN (?)', naic_codes) }
  scope :for_compulife_codes, ->(cl_codes) { where('compulife_code IN (?)', cl_codes) }
  scope :valid,               ->{where('id IN (?)',VALID_CARRIER_IDS).order('name ASC')}

  has_many :contracts,          class_name: "Usage::Contract"
  has_many :marketech_carriers, class_name: 'Processing::Marketech::Carrier'

  has_many :excluded_carriers
  has_many :widgets, :class_name => "Quoting::Widget", :through => :excluded_carriers, :foreign_key => "carrier_id"

  has_one :smm_carrier, class_name: 'Processing::Smm::Carrier'

  def short_name
    black_list=%w[the insurance company co corporation]
    shortened=self.name ? self.name.split.delete_if{|x| black_list.include?(x.downcase)}.join(' ') : ''

    replace_list={
      'and'         =>'&',
      'of NY'       =>'NY',
      'of New York' =>'NY'
    }.each{|k,v| shortened=shortened.gsub(k,v)}

    return shortened
  end

  def has_sprite?
    self.sprite_offset.present?
  end

  def sprite_bg_position
    return nil unless self.has_sprite?
    -self.sprite_offset * self.class.sprite_height
  end

  def self.sprite_height
    60
  end

  def works_for_bsb
    [1,61,24,63,2,3,18,40,32,16,77,7,118,104,4,5,48,54,55,9,35,42,122].include?(self.id)
  end

  VALID_CARRIER_IDS= YAML.load_file(File::expand_path "#{Rails.root}/db/seeding/carriers.yml", __FILE__).map{|c| c['id']}.freeze
  ID_FOR_MOTORISTS=88
  ID_FOR_NMB = 123

  class UnknownCarrier < StandardError; end
end
