class Visit < ActiveRecord::Base
  has_many        :ahoy_events, class_name: "Ahoy::Event"
  belongs_to      :user,        polymorphic:true#this line is here to make Ahoy happy, since it referrs internally to user
  belongs_to      :owner,       class_name: "User"
  belongs_to      :brand,     class_name: "Brand"


  alias_attribute :participant,      :user
  alias_attribute :participant_id,   :user_id
  alias_attribute :participant_type, :user_type
end
