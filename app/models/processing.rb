module Processing

  def self.table_name_prefix
    'processing_'
  end

  def self.build_xml_for xml_obj, source_obj, mapping_hash
  #allows drier code in individual xml builder classes,
  #and performs checks first, making them less prone to break
    mapping_hash.each do |k,v|
      if source_obj.respond_to?(k) && !source_obj.send(k).nil?
        xml_obj.Data(source_obj.send(k), Name: v)
      end
    end
  end

  # This function is abstracted out of places where we send soap requests in order to make tests easier.
  # I.e. any test in this namespace can build a valid @response_hash by calling this function with the contents of an XML file.
  def self.parse_and_format(xml)
    # This duplication of labour (parsing then outputting xml) is performed because Savon (or AgencyWorks) returns invalid xml,
    # which cannot be parsed by `Hash::from_xml` because it includes '&' characters which are not properly escaped.
    output = Hash.from_xml Nokogiri::XML(xml).to_xml
    underscore_keys output
  end

  def self.underscore_keys arg
    if arg.is_a? Hash
      Hash[arg.map { |k, v| [k.to_s.underscore.to_sym, underscore_keys(v)] }]
    elsif arg.is_a? Array
      arg.map{ |m| underscore_keys m }
    else
      arg
    end
  end
end
