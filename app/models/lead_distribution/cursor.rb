class LeadDistribution::Cursor < ActiveRecord::Base
  

  belongs_to :brand,   class_name:'Brand'
  belongs_to :lead_type, class_name:'Crm::LeadType'

  has_many   :all_rules, class_name:'LeadDistribution::UserRule', inverse_of: :cursor, dependent: :destroy

  validates :brand, presence:true
  validates :lead_type, presence:true
  validates :lead_type_id, uniqueness:{scope: :brand_id}

  # Returns rules which are on and whose users have lead_distribution permission
  def active_rules
    rules.select{|r| !r.off && r.user.try(:can?, :lead_distribution, true) }
  end

  def create_missing_rules
    all_agents.each do |agent|
      unless all_rules.any?{|r| r.user_id == agent.id }
        all_rules.create user_id:agent.id
      end
    end
  end

  def reset_counts
    all_rules.update_all(count:0)
  end

  # Find minimum ratio of count/weight among all rules for this Cursor.
  # Returns an integer (floor) value.
  def min_ratio wday
    wday ||= Date.today.wday
    active_rules.min{|a,b| a.ratio(wday) <=> b.ratio(wday) }.try(:ratio, wday).to_i
  end

  # Find next rule whose id is greater than current rule_id.
  # Repeat (round-robin) until a rule is found whose ratio <= the minimum ratio for the whole set of candidates.
  # Update self.rule_id to new current rule.id.
  # Return new current rule.
  def next! wday=nil
    wday ||= Date.today.wday
    min_ratio = self.min_ratio(wday)
    chosen_rule = active_rules.min do |a,b| # Get a rule with the lowest distribution ratio for today, preferring a rule that isn't the current one in the case of a tie
      cmp = a.ratio(wday) <=> b.ratio(wday)
      if cmp == 0
        delta_a = a.id.to_i - rule_id.to_i
        delta_b = b.id.to_i - rule_id.to_i
        if delta_a == 0
          1
        elsif delta_b == 0
          -1
        else
          delta_a <=> delta_b
        end
      else
        cmp
      end
    end
    update_attributes!(rule_id:chosen_rule.try(:id))
    if chosen_rule.nil?
      raise Exception, "Failed to get next Rule for LeadDistribution Cursor ##{id}.\n\n#{self.inspect}\n\nmin_ratio: #{min_ratio}\n\nactive_rules: #{active_rules.inspect}\n\nrules: #{rules.inspect}"
    end
    return chosen_rule
  end

protected

  # Return array of all UserRule for this Cursor.
  # Create a UserRule for any agent belonging to this Cursor's Brand who lacks a UserRule for this Cursor.
  # Rules array must be sorted by id (for the sake of the #next function's round-robin behaviour).
  def rules
    @rules ||=
    begin
      save! unless persisted?
      saved_rules = all_rules.order(:id).to_a
      agents.each do |agent|
        if matching_rule = saved_rules.find{|r| r.user_id == agent.id }
          # Associate rule with its user
          matching_rule.user = agent
        else
          # Create rule for agents who lacks one
          saved_rules.push LeadDistribution::UserRule.create!(user:agent, cursor:self)
        end
      end
      saved_rules
    end
  end

  def agents
    @agents=[] if !brand#because sometimes (tho infrequently) brands are destroyed
    @agents ||= brand.members.can(:lead_distribution).where(temporary_suspension:false)
  end

  def all_agents
    @all_agents=[] if !brand#because sometimes (tho infrequently) brands are destroyed
    @all_agents ||= brand.members.can(:lead_distribution)
  end

end
