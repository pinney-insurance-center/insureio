class LeadDistribution::UserRule < ActiveRecord::Base

  belongs_to :user, class_name:'User'
  belongs_to :cursor, class_name:'LeadDistribution::Cursor', inverse_of: :all_rules

  delegate :lead_type_id, to: :cursor, allow_nil: true
  delegate :brand_id,     to: :cursor, allow_nil: true
  delegate :name,         to: :user,   prefix: true, allow_nil: true

  validates :user, presence: true

  def lacks_permission
    !user.try(:can?,:lead_distribution)
  end

  def increment!
    self.class.increment(id)
  end

  def self.increment(id)
    connection.execute("update #{table_name} set count=1+count where id = #{id}")
  end

  # Returns ratio of count to weight as an integer (floor).
  def ratio wday
    weight(wday).to_i <= 0 ? MAX_INT : count / weight(wday)
  end

  # Returns weight for a given day of the week
  def weight wday
    sym = [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday][wday]
    self[sym]
  end

  MAX_INT = (2**(0.size * 8 -2) -1) # minus 2 bits in the exponent for 1 bit of marking as an int, 1 bit for sign
  # Some fields are not to be directly modified by user updates, but these ones are:
  USER_MODIFIABLE_ATTRS = Set.new(
    %w[sunday monday tuesday wednesday thursday friday saturday tag_blacklist off]
    .select { |attr_name| new.respond_to? "#{attr_name}=" }
  ).freeze
end
