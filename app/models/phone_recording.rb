# Example input from DrTcxSweeper
#
#    {"S3FilePath":"8767\\[Tracy Meier]_8767-7000%2A%2A8767_20140610093804(32490).mp3","Party2":"8767",
#    "Party1":"7000%2A%2A8767","Title":"Tracy Meier","Timestamp":"20140610093804"}
#    {"S3FilePath":"8767\\[Cell Phone   CA]_9167198601-8767_20140516113319(1640).mp3","Party2":"9167198601","Party1":"8767","Title":"Cell Phone   CA","Timestamp":"20140516113319"}

class PhoneRecording < ActiveRecord::Base
  PARTY_DELIMITER = '%2A%2A'
  TCX_SERVER_TZ   = 'Pacific Time (US & Canada)'
  TCX_S3_BUCKET   = '3cx-recordings'

  belongs_to :person, polymorphic:true

  counter_culture :person, column_name: proc {|pr| pr.person_type=='Consumer' ? 'recordings_count' : nil }

  validates :person, presence:true

  #This method uses `Net::HTTP` to connect to AWS and read the file content.
  def binary_content
    open(secure_url).read
  end

  def file_name
    name_from_path=/_(.*?)_/.match(s3_filepath).try(:[],1) || s3_filepath
    "phone recording - #{name_from_path}"
  end

  def extension
    self.file_name.try(:split,'.').try(:last) || ''
  end

  #The name except for the extension.
  def title
    parts=self.file_name.try(:split,'.')||[]
    parts[0..parts.length-2].join('.')
  end

  def content_type
    if extension.match(/mp3/)
      'audio/mpeg'
    elsif extension.match(/wav/)
      'audio/wav'
    else
      'audio'
    end
  end

  def size; nil; end

  def default_serialization
    as_json({
      only:[:id,:created_at,:updated_at],
      methods:[:title,:extension,:content_type,:size]
    })
  end

  def self.create_records params
    parties = params['Party1'].split(PARTY_DELIMITER)
    parties.push *params['Party2'].split(PARTY_DELIMITER)
    parties.select!{|party| party =~ /^\d{7,}$/}#selects numbers with 7 or more characters
    if parties.present?
      #create an SQL prepared statement
      fmt    = (['value REGEXP ?']*parties.length).join(' OR ')
      args   = parties.map do |party|
        (["[^0-9]*"]*(1+party.length)).zip(party.chars).join # => "[^0-9]*3[^0-9]*0[^0-9]*3[^0-9]*7[^0-9]*9[^0-9]*1[^0-9]*6[^0-9]*3[^0-9]*0[^0-9]*6[^0-9]*"
      end
      phones = Phone.where(fmt, *args).includes(:contactable)
      timestamp = ActiveSupport::TimeZone[TCX_SERVER_TZ].parse(params['Timestamp'])
      phones.map do |phone|
        next unless phone.contactable
        find_or_create_by!({
          person: phone.contactable,
          created_at: timestamp,
          s3_filepath: params['S3FilePath']
        })
        if phone.contactable.respond_to?(:last_contacted) && (phone.contactable.last_contacted.nil? || phone.contactable.last_contacted<timestamp)
          phone.contactable.update_column(:last_contacted,timestamp)
        end
      end.compact
    end
  end

  private

  #This time-sensitive https url provides an alternate way to read AWS S3 file content.
  #DO NOT EXPOSE in the GUI. Instead, send users/consumers to the PhoneRecordingsController#show action.
  #This is functionally equivalent to `uploader.url`, but may still prove useful for debugging.
  def secure_url
    s3  = AWS::S3.new access_key_id:APP_CONFIG['aws']['s3_key_id'], secret_access_key:APP_CONFIG['aws']['s3_secret']
    obj = s3.buckets[TCX_S3_BUCKET].objects[s3_filepath]
    obj.url_for(:get, expires:5.minutes.from_now, secure:true)
  end

end
