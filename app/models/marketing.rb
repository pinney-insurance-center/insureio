module Marketing
  def self.table_name_prefix
    'marketing_'
  end

  class SMSError < Clu2::Error; end
  
  class SMTPError < Clu2::Error; end

  class SMTPAuthenticationError < SMTPError
    attr_reader :smtp # Marketing::Email::SmtpServer which failed to authenticate
    attr_reader :user # user who owns given smtp settings
    attr_reader :ex   # Net::SMTPAuthenticationError

    # +smtp+ can be Hash or Marketing::Email::SmtpServer
    def initialize(ex, smtp, user=nil)
      super(ex.message)
      @ex = ex
      @smtp = smtp.is_a?(Hash) ? Marketing::Email::SmtpServer.new(smtp) : smtp
      @user = user
    end

    def loggable_string
      lines = []
      lines << "#{ex.class}: #{ex.message}"
      lines << "for user #{user.id}" if user
      lines << smtp.inspect
      lines += ex.backtrace
      lines.join("\n")
    end
  end

  class TemplateError < Clu2::Error; end
  class RenderingError < Clu2::Error; end
end
