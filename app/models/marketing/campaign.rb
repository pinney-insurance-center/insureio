class Marketing::Campaign < ActiveRecord::Base
  
  include OwnedStereotype

  # Association
  belongs_to_enum         :planning_category, class_name:'Enum::PlanningCat'
  belongs_to_enum         :product_category,  class_name:'Enum::ProductTypeCat'

  has_many :task_builders,    class_name: "Crm::TaskBuilder", as: :owner, dependent: :destroy
  has_many :subscriptions,    class_name: 'Subscription'
  has_many :tasks,            class_name: "Task",        through: :subscriptions

  delegate :name, to: :owner, prefix: true, allow_nil:true

  def planning_category_name
    planning_category.try :name
  end

  def planning_category_name= name
    self.planning_category = Enum::PlanningCat.find_by_name(name)
  end

  def associate_consumers(filtered_consumer_ids, created_via_report_id)
    successful = []
    unsuccessful = {}
    errors.add(:base,'Cannot create subscriptions without a list of consumers') unless filtered_consumer_ids.is_a?(Array) && filtered_consumer_ids.present?

    filtered_consumer_ids.each do |consumer_id|
      queue_num = Consumer.find(consumer_id).last_campaign_subscription_queue_num + 1
      attempted_subscription=Marketing::Subscription.create({
        campaign_id: self.id,
        person_id: consumer_id,
        person_type: 'Consumer',
        created_by_id: Thread.current[:current_user_id],
        created_via_report_id: created_via_report_id,
        active: true,
        queue_num: queue_num
      })
      if attempted_subscription
        successful << consumer_id
      else
        error_message = "Could not subscribe consumer #{consumer_id} (#{attempted_subscription.errors.full_messages.join(', ')})."
        errors.add :subscriptions, error_message
        unsuccessful[consumer_id] = error_message
      end
    end
    {successful: successful, successful_count: successful.count, unsuccessful_count: unsuccessful.count, unsuccessful: unsuccessful}
  end


end
