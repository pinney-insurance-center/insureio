class Marketing::RenderingService
  include AuditLogger::Helpers
  audit_logger_name 'marketing_rendering_service'
  #Accepts a string which contains whitelisted method chains, denoted by double curly braces.
  #Parses to extract method chain and any preceding arguments.
  #Evaluates those method chains with the given arguments.
  #Returns the provided string, substituting the results of said method chains.
  #eg "some text {{ arg1, arg2, [ ...argN ] | method_chain }}" => "some text some result"

  attr_reader   :data_for_rendering
  attr_accessor :errors, :fields_present

  def initialize custom_data_for_rendering={}
    @data_for_rendering=DEFAULT_RENDERING_DATA.merge(custom_data_for_rendering).with_indifferent_access
    @errors=[]
  end

  def render string_with_dynamic_fields
    @fields_present=parse_fields(string_with_dynamic_fields).uniq
    raise Marketing::RenderingError.new( self.errors.join(' ') ) unless can_render_fields?
    rendered = string_with_dynamic_fields
    fields_present.each do |field|
      value=render_single_field field[:method_chain], field[:arg_list]
      #Find and replace all occurences of this method_chain and arg_list combo, ignoring any variations in whitespace.
      pattern=Regexp.new("\\{\\{\\s*(#{field[:arg_list].join(',\\s*')}\\s*\\|)?\\s*#{field[:method_chain].join('\\.')}\\s*\\}\\}")
      rendered=rendered.gsub(pattern,value.to_s)
    end
    if self.errors.present?
      raise Marketing::RenderingError.new( self.errors.join(' ') ) unless self.errors.blank?
    end
    rendered
  end

  def render_single_field method_chain, arg_list
    method_chain=method_chain.dup
    output=data_for_rendering[ method_chain.shift ]
    output=output.call(*arg_list) if output.is_a?(Proc)
    last_index=method_chain.length-1
    method_chain.each_with_index do |method,idx|
      if idx==last_index
        output=output&.send(method,*arg_list)
      else
        output=output&.send(method)
      end
    end
    output
  rescue => ex
    audit level: 'error', body:{
      method_chain: method_chain, arg_list: arg_list, data_for_rendering:data_for_rendering,
      ex:{class_name:ex.class.name,msg:ex.message,app_backtrace:Rails.backtrace_cleaner.clean(ex.backtrace)}
    }
    errors.push "Render error in field {{ #{method_chain} #{arg_list} }}: #{ex.class} #{ex.message}."
  end

  #Returns an array of hashes, each with keys "method_chain" and "arg_list", and values which are arrays of strings.
  def parse_fields string_with_dynamic_fields
    field_strings=string_with_dynamic_fields.scan( FIELD_EXTRACTION_PATTERN ).flatten
    field_strings.map do |f|
      begin
        parts=f.split('|')
        arg_list= parts.length>1 ? parts.shift.split(',').map(&:strip) : []
        method_chain=parts[0].split('.').map(&:strip)
        {method_chain:method_chain, arg_list: arg_list}
      rescue =>ex
        audit level: 'error', body:{field: f,
          ex:{class_name:ex.class.name,msg:ex.message,app_backtrace:Rails.backtrace_cleaner.clean(ex.backtrace)}
        }
        errors.push "Parse error in field {{ #{f} }}."
      end
    end
  rescue =>ex
    audit level: 'error', body:{field_strings: field_strings,
      ex:{class_name:ex.class.name,msg:ex.message,app_backtrace:Rails.backtrace_cleaner.clean(ex.backtrace)}
    }
    errors.push "Parse error."
  end

  def can_render_fields?
    allowed_field_rules=Enum::MarketingDynamicField.all
    fields_present.each do |f|
      method_chain=f[:method_chain]
      arg_list=f[:arg_list]
      rule=allowed_field_rules.select{|r| r.method_chain==method_chain.join('.') }.first
      if !rule
        errors.push "Pre-render error: \"#{method_chain.join('.')}\" is not an allowed dynamic field."
        break
      end
      if arg_list.length != (rule.expected_arg_types || []).length
        errors.push "Pre-render error: \"#{method_chain.join('.')}\" requires #{(rule.expected_arg_types || []).length} inputs, but was given #{arg_list.length}."
        break
      end
      if !data_for_rendering.has_key?(method_chain[0])
        errors.push "Pre-render error: \"#{method_chain.join('.')}\" requires data not provided to the renderer."
        break
      end
    end
    return errors.length==0
  end

  FIELD_EXTRACTION_PATTERN=/\{\{\s*([^\}]+)\}\}/#This finds text between double curly braces.
  DEFAULT_RENDERING_DATA={
    days_from_now: lambda{|n| n.to_i.days.from_now },
    copyright_year: lambda{ Time.now.year.to_s },
  }
end