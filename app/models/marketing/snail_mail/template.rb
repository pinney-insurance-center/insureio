class Marketing::SnailMail::Template < Marketing::Template
  has_many :messages, :class_name => "Marketing::SnailMail::Message"

  
  HEADER ={
    status:
    %Q(
    <div id="header" style="background:url({{brand.header}})">
      <div style="float:right;">
        {{agent.contact_info.website}}<br />
        {{agent.phone}}
      </div>
    </div>
    ),
    marketing:
    %Q(
    <div id="header" style="background:url({{brand.header}})">
      <div style="float:right;">
        {{agent.website}}<br />
        {{agent.phone}}
      </div>
    </div>
    )
  }

  FOOTER ={
    status:
    %Q(
    <div id="footer">
      {{agent.signature}}<br />
      {{agent.first_name}} {{agent.last_name}}<br />
      {{ brand_specific_fallback_primary_email_for_agent }}<br />
      {{brand.city}}, {{agent.state}} {{brand.zip}}<br /><br />
      {{agent.phone}}<br />
      Fax: {{agent.fax_number}}
    </div>
    ),
    marketing:
    %Q(
    <div id="footer">
      {{agent.signature}}<br />
      {{agent.first_name}} {{agent.last_name}}<br />
      {{ brand_specific_fallback_primary_email_for_agent }}<br />
      {{brand.city}}, {{agent.state}} {{brand.zip}}<br /><br />
      {{agent.phone}}<br />
      Fax: {{agent.fax_number}}
    </div>
    )
  }

end
