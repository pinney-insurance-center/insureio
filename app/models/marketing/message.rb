class Marketing::Message < ActiveRecord::Base
  audit_logger_name 'marketing_message'

  
  attr_accessor :cc, :from_addr, :suppress_retries

  belongs_to :template
  belongs_to :target,  polymorphic:true
  belongs_to :brand,   class_name: "Brand"
  belongs_to :sender,  class_name: "User"
  belongs_to :user,    class_name: "User"
  belongs_to :blast,   class_name: "Marketing::Email::Blast", inverse_of: :messages

  has_one    :task,    class_name: "Task", inverse_of: :message

  scope      :sent,   lambda { where('marketing_messages.sent IS NOT NULL') }
  scope      :viewed, lambda { where('marketing_messages.viewed>0') }

  alias_method :agent, :sender

  MAX_FAILED_ATTEMPTS = 4
  RECIPIENT_LACKS_EMAIL_MSG_ID=3499

  #In time, the subclasses will likely be merged and an enum class will be used to indicate type.
  def medium
    case self.class.name
    when 'Marketing::Email::Message'
      'Email'
    when 'Marketing::MessageMedia::Message'
      'SMS'
    when 'Marketing::SnailMail::Message'
      'Print'
    end
  end

  def sent_by_host= value
    value=value.to_s.sub(/^(dataraptor|insureio)-/,'')
    if self[:sent_by_host].present?
      #Never overwrite, only append to this field.
      self[:sent_by_host]+=', '+value
    else
      self[:sent_by_host]=value
    end
  end

  def failure_msgs= value
    value=value.to_s.sub(/^(dataraptor|insureio)-/,'')
    if self[:failure_msgs].present?
      #Never overwrite, only append to this field.
      self[:failure_msgs]+=', '+value
    else
      self[:failure_msgs]=value
    end
  end

  def subject
    self[:subject] || (blast.present? ? blast.rerender_subject(custom_data_for_rendering) : nil)
  end

  def body
    self[:body] || (blast.present? ? blast.rerender_body(custom_data_for_rendering) : nil)
  end

  #This function should be useful in debugging rendering issues.
  def fields_in_body
    (self.body||'').scan( Marketing::RenderingService::FIELD_EXTRACTION_PATTERN ).flatten.map{|f| f.gsub(/\s/,'') }.uniq
  end
  def fields_in_body= val; nil; end

  # Delivers a message, returns the raw message
  def deliver
    return unless valid_permissions_and_smtp_settings?
    #Save message so it gets an id before composing the link to itself in the rendering step.
    save

    # Render subject and body from template
    render

    self.sent_by_host=`echo $HOSTNAME` if `echo $HOSTNAME`#for diagnostics
    self.body=filter_external_links_for_tracking(body) if medium=='Email'

    #For diagnostics, so we know without digging through logs whether it failed in rendering or in delivering.
    save

    # Build Mail::Message
    mail = Mail.new from:from_addr, to:recipient, cc:cc, subject:subject, body:body

    raise Marketing::SMTPError, 'Blank message body' unless mail.body.present?
    mail.content_type 'text/html; charset=UTF-8'
    mail.delivery_method :smtp, @delivery_method

    # Attempt delivery
    if (raw_message = mail.deliver)
      audit 'SUCCESS', self.id
      if blast_id#save space in db by omitting subject and body from blast emails
        self.update_attributes sent:Time.now, subject:nil, body:nil
      else
        self.update_attributes sent:Time.now
      end
      self.person.update_column(:last_contacted,Time.now) if self.person.is_a?(Consumer)
    elsif self.target.is_a?(Crm::Case) && self.target.scor_guid && @settings
      #Retry failed deliveries for RTT policies using the system SMTP settings.
      mail.delivery_method :smtp, ActionMailer::Base.smtp_settings
      audit "Retrying with the system SMTP settings."
      raw_message = mail.deliver
      fail if !raw_message
    else
      fail
    end
    return raw_message
  rescue ::ArgumentError, Marketing::SMTPError => ex
    fail ex, warning_email:true
  rescue Timeout::Error, Exception => ex
    #Rescues ALL other exceptions.
    #Models which called this method should check `failure_msgs`,
    #instead of checking `errors` or using separate `rescue` blocks.
    fail ex, error_email:!retry?
  end

  def valid_permissions_and_smtp_settings?
    if self.marketing
      if !sender.present?#Status emails are important to send regardless of whether there's a sender.
        raise Marketing::SMTPError, "No sender for message #{inspect}"
      elsif target_type=='User' && target_id==sender.id && !sender.can?(:marketing_email_self)
        raise Marketing::SMTPError, "Sender #{sender.id} lacks permission to send themselves marketing emails. (Message #{inspect})"
      elsif task && !sender.can?(:marketing_campaigns_canned) && !sender.can?(:marketing_campaigns_custom)
        raise Marketing::SMTPError, "Sender #{sender.id} lacks permission to send marketing emails via campaigns. (Message #{inspect})"
      elsif blast && blast.search_id && !sender.can?(:marketing_email_search)
        raise Marketing::SMTPError, "Sender #{sender.id} lacks permission to send marketing emails in bulk via search. (Message #{inspect})"
      elsif blast && blast.recipients.present? && !sender.can?(:marketing_email_custom)
        raise Marketing::SMTPError, "Sender #{sender.id} lacks permission to send marketing emails in bulk via custom list. (Message #{inspect})"
      end
    end

    @settings = sender.try(:branded_smtp_config,brand)
    self.from_addr=@settings.try(:from_addr)||APP_CONFIG['email']['consumer_facing_sender']

    if !@settings
      if target.class==User
        self.from_addr=@settings.try(:from_addr) || APP_CONFIG['email']['sender']
      elsif target.class==Crm::Case || target.class==Consumer || target.class==Quoting::Quote
        role_id=Enum::UsageRole::APPLICATION_SPECIALIST_ID
        app_specialist_id=target.try(:staff_assignment).try(:user_id_for_role_id, role_id) || Usage::StaffAssignment.default_user_id_for_role_id(role_id)
        app_specialist=User.find_by_id(app_specialist_id)
        @settings||= app_specialist.try(:smtp_servers).try(:first)
        self.from_addr=@settings.try(:from_addr) || APP_CONFIG['email']['consumer_facing_sender']
      end
    end
    audit "Attempt to deliver msg to #{recipient} from #{from_addr}"
    audit (@settings.try(:to_json, except:[:password,:crypted_password]) || "(Fell back to system SMTP)")
    @delivery_method=(@settings.try(:to_delivery_method) || ActionMailer::Base.smtp_settings)

    raise Marketing::SMTPError, "No \"to\" address. Does client have an email address?" unless recipient.present?
    return true
  end

  def person
    @person||= self.target.is_a?(Crm::Case) || self.target.is_a?(Quoting::Quote) ? self.target.try(:consumer) : self.target
  end

  def case
    target_is_opp_or_case=(self.target.is_a?(Crm::Case) || self.target.is_a?(Quoting::Quote) )#treat opportunity records as cases
    @case||= target_is_opp_or_case ? self.target : nil
  end

  def infer_brand
    self.brand||= (self.person.respond_to?(:default_brand) && self.person.default_brand) || (self.person.respond_to?(:brand) && self.person.brand)
    self.brand||= self.sender.default_brand
  end

  def quoter_key
    (person.respond_to?(:agent) ? person.agent : person.parent)&.quoter_key
  end

  #Determines which person should receive the communication.
  def recipient_record
    @recipient_record ||=
    begin
      if self.task && self.task.task_type.try(:name) =~ /agent/
        if self.target_type=='Crm::Case'
          self.target.try(:agent)
        else
          self.person.try(:owner)
        end
      else
        self.person
      end
    end
  end

  #Determines which email address should receive the communication.
  #An arbitrary recipient may be passed in on message creation,
  #or one may calculated from the `recipient_record`.
  def recipient
    self[:recipient] ||=
    begin
      return nil unless recipient_record
      case self.class.name
      when 'Marketing::Email::Message'
        recipient_record.primary_email.to_s
      when 'Marketing::MessageMedia::Message'
        recipient_record.carrier_provided_email
      end
    end
  end

  def marketing
    self[:marketing]||=(self.template.try(:template_purpose).try(:name)||'').match(/marketing/i)
  end

  # Set self.body & self.subject
  def render
    # Set subject & body
    self.subject ||= template.render_subject self.custom_data_for_rendering if medium=='Email'
    self.body    ||= template.render_body self.custom_data_for_rendering
  rescue Marketing::TemplateError => ex
    SystemMailer.warning(ex, debug:self)
  end

  def retry?
    !suppress_retries && (self.failed_attempts ||= 0) < MAX_FAILED_ATTEMPTS
  end

  def brand_specific_fallback_primary_email_for_agent
    self.agent.brand_specific_fallback_primary_email self.brand
  end

  def brand_specific_fallback_primary_email_for_case_manager
    return '' unless self.case && self.case.case_manager
    self.case.case_manager.brand_specific_fallback_primary_email self.brand
  end

  def brand_specific_fallback_primary_phone_for_agent
    self.agent.brand_specific_fallback_primary_phone self.brand
  end


  def brand_fallback_to_agent_address
    brand.primary_address ||
    agent.primary_address
  end

  def rtt_quote_url
    return '' unless sender.can?(:motorists_rtt_process)
    'https://'+sender.tenant_name+'.insureio.com/rtt/?agent_id='+sender.id.to_s+'&key='+sender.quoter_key+'&brand_id='+brand.id.to_s
  end

protected

  def fail exception, options={}
    audit 'FAILED'
    audit level:'warn', body:exception
    # Increment failure count
    self.failed_attempts ||= 0
    self.failed_attempts += 1

    if exception
      first_line_of_app_backtrace=exception.backtrace.select{|l| l.include?(Rails.root.to_s) }.first
      first_line_of_app_backtrace.sub!( Rails.root.to_s, '')
      self.failure_msgs="#{Time.now}- #{exception.class} #{exception.message} #{first_line_of_app_backtrace}"
    end

    save
    if recipient.blank?
      template=Marketing::Email::Template.find(RECIPIENT_LACKS_EMAIL_MSG_ID)
      subject =Marketing::RenderingService.render template.subject, {'recipient'=>person, 'agent'=>sender}
      body    =Marketing::RenderingService.render template.body,    {'recipient'=>person, 'agent'=>sender}

      mail=SystemMailer.generic(sender.primary_email.try(:value), body, subject)
      mail.content_type('text/html; charset=UTF-8')
      mail.deliver
      options[:warning_email]=false
    end
    # Reschedule or specify a backtrace email recipient
    if retry? || options[:force_retry]
      EmailMessageWorker.perform_at 5.minutes.from_now, self.id
    else
      options[:error_email] || (options[:warning_email] = true) # If we aren't retrying, SOMEONE has to get emailed
    end

    smtp_config_string=if @settings.present?
      JSON.pretty_generate( @settings.as_json(except:[:crypted_password, :password]) )
    else
      '(Fell back to system SMTP)'
    end
    person_string=if person.present?
      "(#{person.try(:class).try(:name)})\n"+
      JSON.pretty_generate( {id: person.id, name: person.name} )
    else
      '-'
    end
    sender_string=if sender.present?
      JSON.pretty_generate( {id: sender.id, name: sender.name} )
    else
      '-'
    end
    humanized_failure_string=if exception && exception.class==Net::SMTPFatalError
      #Gotta make this as outrageously simple as possible for people to get it.
      "The SMTP server responded with an error message.\n"+
      "This type of error relates to the configuration of the SMTP server or the user's account on that server."+
      "Please contact the administrator of that server for any further assistance.\n"+
      "\nSMTP SERVER'S ERROR MESSAGE:\n#{exception.message}"
    else
      "#{exception.class}: #{exception.message}"
    end
    system_email_message = [
      'EMAIL SEND FAILURE:',humanized_failure_string,
      'FAILED ATTEMPT COUNT:', failed_attempts,
      'RETRY?', retry?,
      'SMTP CONFIG:', smtp_config_string,
      'MESSAGE:', self.inspect,
      'RECIPIENT EMAIL ADDRESS:', recipient,
      'RECIPIENT PERSON RECORD:', person_string,
      'SENDER:', sender_string
    ]
    # Send email to admins
    if options[:warning_email]
      SystemMailer.warning exception, message:system_email_message
    end
    # Send email to developers
    if options[:error_email]
      ExceptionNotifier.notify_exception exception, data: system_email_message
    end
  rescue Redis::CannotConnectError => ex
    %x( [ -x #{APP_CONFIG['redis_executable']} ] && #{APP_CONFIG['redis_executable']} stop && #{APP_CONFIG['redis_executable']} start )
    ExceptionNotifier.notify_exception(ex)
  rescue Redis::TimeoutError => ex
    if options[:recursion].to_i < 1
      load 'config/initializers/redis.rb' # replace redis connection
      fail exception, recursion:options[:recursion].to_i+1
    else
      ExceptionNotifier.notify_exception(ex)
    end
  ensure
    # Return nil so that #deliver returns nil so that Task#send_email doesn't think that an email was sent successfully
    return nil
  end

  # Return Hash of params for rendering, suitable for passing to <tt>Marketing::Template#render_body</tt>.
  # +marketech_esign_url+ is set only in the overriden function in a sublcass.
  def custom_data_for_rendering
    infer_brand if !brand
    #Keys only added for emails, via the email message class:
    #[unsubscribe_link, view_our_site_link, tracking_pixel, privacy_policy, outstanding_requirements]
    {
      'agent'                => self.sender,
      'case'                 => self.case,
      'case_manager'         => self.case.try(:case_manager),
      'recipient'            => self.person,
      'brand'                => self.brand,
      'license_info'         => self.sender.license_info(self.brand_id),
      'rtt_quote_url'        => self.rtt_quote_url,
      'brand_specific_fallback_primary_email_for_agent' => self.brand_specific_fallback_primary_email_for_agent,
      'brand_specific_fallback_primary_email_for_case_manager' => self.brand_specific_fallback_primary_email_for_case_manager,
      'brand_specific_fallback_primary_phone_for_agent' => self.brand_specific_fallback_primary_phone_for_agent,
      'brand_fallback_to_agent_address' =>self.brand_fallback_to_agent_address,
    }.with_indifferent_access
  end

end
