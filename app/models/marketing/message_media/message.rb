class Marketing::MessageMedia::Message < Marketing::Message
  include ActionView::Helpers::SanitizeHelper
  include AuditLogger::Helpers
  audit_logger_name 'message_media'
  # attr_accessor :from_addr

  # MAX_FAILED_ATTEMPTS = 4

#   def deliver
#     raise Marketing::SMSError, "No recipient specified" unless recipient.present?
#     raise Marketing::SMSError, "No rendered body" unless body.present?
#     raise Marketing::SMSError, "Content length of 400 characters exceeded" unless body.length <= 918
#     audit attributes

#     mail = Mail.new from:from_addr, to:recipient, body:body

#     mail.content_type 'text/plain; charset=UTF-8'
#     mail.delivery_method :smtp, (@settings.try(:to_delivery_method) ? @settings.try(:to_delivery_method) : ActionMailer::Base.smtp_settings)
#     audit "Attempt to deliver msg to #{recipient} from #{from_addr}"
#     audit (@settings.try(:to_json, except:[:password,:crypted_password]) || "(Fell back to system SMTP)")

#     # Attempt delivery
#     if (raw_message = mail.deliver)
#       audit 'SUCCESS', raw_message
#       self.person.update_column(:last_contacted,Time.now) if self.person.is_a?(Consumer)
#     else
#       fail
#     end
#     return raw_message
#   rescue ::ArgumentError, Marketing::SMTPError => ex
#     fail ex, warning_email:true
#   rescue Timeout::Error, Exception => ex
#     fail ex, error_email:!retry?
#   end
#   alias_method :send_sms, :deliver

#   def body
#     self[:body] || (render && self[:body])
#   end

# private

#   def retry?
#     (self.failed_attempts ||= 0) < MAX_FAILED_ATTEMPTS
#   end

#   def fail exception, options={}
#     audit 'FAILED'
#     audit level:'warn', body:exception
#     # Increment failure count
#     self.failed_attempts ||= 0
#     self.failed_attempts += 1
#     save
#     system_email_message = [exception.message,
#       'FAILED ATTEMPT COUNT:', failed_attempts,
#       'RETRY?', retry?,
#       'SMTP CONFIG:', @settings.try(:to_json, except:[:crypted_password])||'(Fell back to system SMTP)',
#       'MESSAGE:', self.inspect, 'RECIPIENT:', recipient.inspect,
#       person.inspect, person.try(:contact).inspect,
#       'SENDER:', sender.inspect, sender.try(:contact).inspect
#     ]
#     SystemMailer.warning exception, message:system_email_message
#   end

end
