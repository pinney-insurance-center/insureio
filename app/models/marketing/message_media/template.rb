class Marketing::MessageMedia::Template < Marketing::Template

  # DEPRECATED. Logic to compose messages should be consolidated in the message models.
  def deliver options={}
    Marketing::Email::Message.new({
      template:self,
    }.merge(options)).deliver
  end

end
