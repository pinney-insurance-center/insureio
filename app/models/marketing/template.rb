class Marketing::Template < ActiveRecord::Base
  include OwnedStereotype

  

  validates :name, presence: true
  validates :body, presence: true
  validates :template_purpose, presence: true
  validate  :fields_match_template_purpose

  # Associations
  belongs_to_enum :product_category,  class_name:'Enum::ProductTypeCat'
  belongs_to_enum :planning_category, class_name:'Enum::PlanningCat'
  belongs_to_enum :template_purpose,  class_name:'Enum::MarketingTemplatePurpose'

  default_scope ->{ where(enabled:true) }

  #In time, the subclasses will likely be merged and an enum class will be used to indicate type.
  def medium
    case self.class.name
    when 'Marketing::Email::Template'
      'Email'
    when 'Marketing::MessageMedia::Template'
      'SMS'
    when 'Marketing::SnailMail::Template'
      'Print'
    end
  end

  def render_body data_for_rendering={}, options={}
    raise(Marketing::TemplateError, "Template body is blank") if self.body.blank?
    rendered = self.body.dup

    header = ''
    footer = ''
    unless options[:no_boilerplate]
      rendered=add_boilerplate rendered
    end
    renderer=Marketing::RenderingService.new(data_for_rendering)
    rendered=renderer.render(rendered)
    rendered.html_safe
  end

  alias_method :apply, :render_body

  #Boilerplate includes header, footer, and possibly tracking pixel.
  def add_boilerplate rendered
      purpose_name=self.template_purpose.try(:name)
      #retrieve proper header and footer based on class and whether it is for a status update or for marketing
      if purpose_name=="Status"
        header = self.class::HEADER[:status]
        footer = self.class::FOOTER[:status]
      elsif purpose_name=="Consumer Marketing"
        header = self.class::HEADER[:marketing]
        footer = self.class::FOOTER[:marketing]
      elsif purpose_name=="Agent Marketing"

      elsif purpose_name=="System"

      end

      # Add HEADER, either at {{ header }} or at TOP
      if rendered.match(/{{\s*header\s*}}/)
        rendered.gsub!(/{{\s*header\s*}}/, header)
      else
        rendered="#{header} #{rendered}"
      end
      # Add FOOTER, either at {{ footer }} or at BOTTOM
      if rendered.match(/{{\s*footer\s*}}/)
        rendered.gsub!(/{{\s*footer\s*}}/, footer)
      else
        rendered="#{rendered} #{footer}"
      end

      rendered="#{rendered} #{self.class::TRACKING_PIXEL}" if medium=='Email'
      rendered
  end

  def render_subject data_for_rendering={}
    Marketing::RenderingService.new(data_for_rendering).render(self.subject).html_safe
  end

  def planning_category_name
    planning_category.try :name
  end

  def planning_category_name= name
    self.planning_category = Enum::PlanningCat.find_by_name(name)
  end

  def self.template_type_for_task_type_id task_type_id
    case task_type_id
      when 2, 3
        Marketing::Email::Template
      when 6, 7
        Marketing::MessageMedia::Template
      when 8
        Marketing::SnailMail::Template
      else
        Marketing::Template
    end
  end

  #This function should be useful in debugging rendering issues.
  def fields_in_body
    fields=(self.body||'').scan( Marketing::RenderingService::FIELD_EXTRACTION_PATTERN ).flatten.map{|f| f.gsub(/\s/,'') }.uniq
  end

  def fields_match_template_purpose
    purpose_name =self.template_purpose.try(:name)
    target_type  =self.template_purpose.try(:allowed_target_type)
    person_type  =self.template_purpose.try(:allowed_person_type)
    fib          =self.fields_in_body
    allowed_fibs =Enum::MarketingDynamicField.map(&:method_chain)
    fields_requiring_case     =fib.select{|f| f.match(/case\./) || f.match(/case_manager/) || f=='marketech_esign_url' }.uniq
    fields_requiring_consumer =fib.select{|f| ['details_for_quoter', 'saved_quote_list', 'tobacco', 'case_manager'].any?{|x| f.include?(x) } }.uniq
    fields_disallowed         =fib.select{|f| !allowed_fibs.include?(f) }.uniq
    passes_tests=true

    if target_type!='Crm::Case' && fields_requiring_case.present?
      passes_tests=false
      errors.add :template_purpose, "#{purpose_name} does not allow the following fields, which require a policy: #{fields_requiring_case}"
    end
    if person_type!='Consumer' && fields_requiring_consumer.present?
      passes_tests=false
      errors.add :template_purpose, "#{purpose_name} does not allow the following fields, which require a consumer: #{fields_requiring_consumer}"
    end
    if fields_disallowed.present?
      passes_tests=false
      errors.add :body, "should not contain the following fields: #{fields_disallowed}"
    end
    passes_tests
  end

  #these are overridden in the classes that inherit from Marketing::Template
  #to access the overridden versions, and not the versions below, prepend with self.class:: in all instance methods
  TOP_CONTENT = ""
  BOTTOM_CONTENT = ""
  HEADER ={status:"", marketing:""}
  FOOTER ={status:"", marketing:""}
  LETTER_SIGNATURE = ""

end