class Marketing::Subscription < ActiveRecord::Base
  
 
  belongs_to :created_by, class_name: 'User'
  belongs_to :created_via_report, class_name: 'Reporting::Search'
  belongs_to :person,     polymorphic: true
  belongs_to :statusable, polymorphic: true,               foreign_key: :person_id#so it responds to the same methods as Crm::Status
  belongs_to :campaign,   class_name: "Marketing::Campaign"
  has_many   :tasks,      class_name: 'Task',         as: :origin #acts as an origin for tasks, as does Crm::Status

  before_create  :set_initial_order_in_queue,
                 -> {self.created_by_id||=Thread.current[:current_user_id] }
  after_create   -> {self.update_column(:active,true) && self.create_tasks if self.order_in_queue==1}
  #after_create   :create_tasks
  after_update   :activate_next_in_queue

  delegate       :name,      to: :campaign, prefix:true
  delegate       :viewable?,
                 :editable?, to: :person

  alias_attribute :statusable_id,   :person_id
  alias_attribute :statusable_type, :person_type

  scope :inactive, lambda {
    #subscriptions that have been bumped out of their queue, and been marked inactive
    #(this is carried out by after_save callback +update_subscription+ on task)
    #-or-
    #subscriptions that have been moved back in the queue after their tasks were already started
    #(in which case the incomplete tasks were all set to never come due)
    where(active:false)
  }
  scope :active, lambda {where(active:true)}

  alias_method :original_destroy, :destroy
  #overriding destroy so that subscriptions that are active cannot be destroyed, only dequeued.
  #destroy should only ever be called from the gui, on subscriptions that are in a queue
  def destroy
    if self.active
      self.dequeue
    else
      self.original_destroy
    end
  end

  def dequeue
    self.update_attributes(active:false, queue_num:nil, order_in_queue:nil)
    self.destroy_incomplete_tasks
  end

  #called when a new subscription is created
  def set_initial_order_in_queue
    self.queue_num||=1
    last_in_queue=Marketing::Subscription.
      where(person_id: self.person_id, queue_num: self.queue_num).
      order('order_in_queue ASC').last

    self.order_in_queue=(last_in_queue.try(:order_in_queue) || 0)+1
  end

  #Called from controller action of same name.
  #Fetches a queue of subscriptions, reorders them, and creates tasks for first subscription in the queue if it has changed.
  def self.update_order_for_queue person, queue_num, new_order
    subscriptions     =person.subscriptions.where('queue_num=?',queue_num).order('order_in_queue ASC')
    old_first_in_queue=subscriptions.first
    ids               =subscriptions.map(&:id)

    new_order.each_with_index do |id, index|
      id=id.to_i
      Marketing::Subscription.find(id).update_column(:order_in_queue, index+1) if ids.include?(id)
    end

    new_first_in_queue=subscriptions.order('order_in_queue ASC').first
    unless new_first_in_queue==old_first_in_queue
      new_first_in_queue.update_column(:active,true)
      new_first_in_queue.create_tasks

      old_first_in_queue.update_column(:active,false)
      old_first_in_queue.destroy_incomplete_tasks
    end
  end

  # Fires when the model is updated. This should only happen when a task completes and marks it as inactive and dequeued.
  def activate_next_in_queue
    new_first_in_queue=person.subscriptions.where('queue_num=?',queue_num_was).order('order_in_queue ASC').first
    if new_first_in_queue.present?
      new_first_in_queue.update_column(:active,true)
      new_first_in_queue.create_tasks
    end
  end

  # Create tasks for this campaign and person. (Task creation should fire scheduling callback.)
  def create_tasks
    audit log:'status'
    self.campaign.task_builders.each do |builder|
      #make sure not to recreate a task that has already been run for this subscription
      unless self.tasks.map(&:label).include?(builder.label)
        task = builder.build(person:person, origin:self, sequenceable_id:statusable_id, sequenceable_type:statusable_type)
        task.save
      end
    end
  end

  # Removes incomplete tasks belonging to this campaign and person (complete tasks are saved for documentation)
  def destroy_incomplete_tasks
    tasks.where(completed_at:nil).where('evergreen IS NULL OR evergreen IS FALSE').destroy_all
  end

private

end
