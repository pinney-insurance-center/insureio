class Marketing::Email::SmtpServer < ActiveRecord::Base
  include OwnedStereotype

  attr_accessor :password_confirmation


  #`insecure_mode` is for backward compatibility, when an iv was not required.
  #For ay new encrypted fields, provide the iv instead.
  attr_encrypted :password, algorithm:'aes-256-cbc', mode: :single_iv_and_salt, insecure_mode: true, key:APP_CONFIG['old_encryption_key'], attribute:'crypted_password', if: (Rails.env.production? || Rails.env.test?)

  
  
  # Associations
  belongs_to :user, :foreign_key => :owner_id,:class_name => "User"

  # Validations
  validate :_password_matches_confirmation
  validate :test_connection

  # Aliases
  alias_attribute :user_name, :username # matches ActionMailer::Base.smtp_settings
  alias_attribute :domain,    :host # matches ActionMailer::Base.smtp_settings

  def authentication
    a = read_attribute :authentication
    a.blank? ? 'login' : a
  end

  def enable_starttls_auto?
    tls == ENABLE_STARTTLS_AUTO
  end

  def enable_tls?
    tls == ENABLE_TLS
  end

  # Returns a well-formatted email address representing this configuration's "from" address
  def from_addr
    return self[:from_addr] if self[:from_addr].present?
    output = username
    unless username.present? && username.include?("@")
      if host.present?
        output += "@" + host
      else
        output += "@" + address.sub(/^[^\.]+\./,'')
      end
    end
    output
  end

  def port
    read_attribute(:port) || 587
  end

  def send_test_message
    UserMailer.test_message(user, to_delivery_method).deliver
  rescue Exception => e
    errors.add(:base, "Test email could not send.<br/>#{e.message}")
    return false
  end

  def test_connection
    smtp = Net::SMTP.new address, port
    smtp.enable_starttls_auto if enable_starttls_auto?
    smtp.enable_tls           if enable_tls?
    smtp.start(host) do
      smtp.authenticate username, password, authentication
    end
  rescue Net::SMTPAuthenticationError, Exception => ex
    # I've seen a TimeoutError occur when enable_starttls_auto was called but the 
    # smtp server really only wanted enable_tls.
    errors.add(:base, "#{ex.message} -- #{ex.class}")
    return false
  end

  # formats settings as a hash suitable for the 2nd argument to Mail::Message#delivery_method
  def to_delivery_method
    {
      openssl_verify_mode: 'none',
      ca_file: APP_CONFIG['ca_cert_file'],
      enable_starttls_auto: enable_starttls_auto?, # nil can be overwritten as true, and `enable_starttls_auto=true` causes a timeout error at box561.bluehost.com:465
      tls: enable_tls?,
      address: address,
      port: port,
      domain: host,
      authentication: authentication,
      user_name: username,
      password: password
    }
  end

  private

  def _password_matches_confirmation
    errors.add(:password_confirmation, 'does not match password') if crypted_password_changed? and password != password_confirmation
  end

  ENABLE_STARTTLS_AUTO = 1
  ENABLE_TLS = 2

end
