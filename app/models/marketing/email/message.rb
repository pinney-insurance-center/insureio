class Marketing::Email::Message < Marketing::Message
  include MarketingHelper
  audit_logger_name 'marketing_message'

  attr_reader :settings
  validates :recipient, :email => true, :allow_nil => true

protected

  def filter_external_links_for_tracking markup
    page=Nokogiri::HTML(markup)
    anchors=page.css('a')
    #this pattern checks if the url is a subdomain of DR or ID, or is the
    pattern_for_internal_links="^(#{Regexp.quote(APP_CONFIG['base_url'])}|#{Regexp.quote(APP_CONFIG['id_base_url'])}|https?:\/\/.+\.insureio\.com|https?:\/\/.+\.insurancedivision\.com)"
    anchors.each do |a|
      next if a.attributes['href'].value=~Regexp.new(pattern_for_internal_links)
      url=a.attributes['href'].value
      link_text=a.text.strip
      a.attributes['href'].value="#{APP_CONFIG['base_url']}/messages/#{id}/log_and_redirect/?link_text=#{CGI.escape(link_text)}&url=#{CGI.escape(url)}"
    end
    page.to_s.html_safe
  end

  # Return Hash of params for rendering, suitable for passing to #render_body
  def custom_data_for_rendering
    new_params=super
    sender_website= sender.website || brand&.website || sender.default_brand&.website
    new_params=new_params.merge({
      privacy_policy:           privacy_policy,
      message_public_view_link: message_public_view_link(self),
      unsubscribe_link:         unsubscribe_link(self),
      tracking_pixel:           tracking_pixel(self),
      view_our_site_link:       view_our_site_link(sender_website),
      outstanding_requirements: self.case && outstanding_reqs(self.case),
      quoter_url:               quoter_url,
    })
    # Don't render unless it's needed in the template.
    # Otherwise, an unnecessary warning email could be triggered.
    new_params[:marketech_esign_url]=marketech_esign_url if template&.body =~ /\{\{\s*marketech_esign_url\s*\}\}/
    new_params
  end

  def marketech_esign_url
    url = self.case.try(:marketech_esign_url)
    if url.present?
      url
    else
      ExceptionNotifier.notify_exception ArgumentError.new("No esign_url for case##{self.case.id}. Email will be retried in 1 hour.")
      self.task.update_attributes(due_at:self.task.due_at+1.hours) if self.task
      raise Marketing::TemplateError, "No esign_url for case##{self.case.id}"
    end
  end

  def quoter_url
    if template.quoter_url_type.to_i==template.class::USE_INS_DIV_QUOTER_URL && sender.can?(:insurance_division)
      if template.insurance_division_url_page.present?
        insurance_division_url+template.insurance_division_url_page
      else
        insurance_division_url
      end
    elsif template.quoter_url_type.to_i==template.class::USE_CUSTOM_QUOTER_URL && template.custom_quoter_url.present?
      template.custom_quoter_url
    else
      quote_path=Enum::Tenant.subdomain_string("https://pinney.insureio.com/quoting/quotes/new", sender.tenant_name)
      query_str="?agent_id=#{sender.id}&brand_id=#{self.brand_id}&key=#{sender.quoter_key}&requestType=APPLICATION"
      quote_path+query_str
    end
  end

  #This method is mirrored on the front end in `updateIdLink` within `MarketingLinksCtrl`.
  def insurance_division_url(sub_path='')
    return nil unless sender.can?(:insurance_division)
    sub_path.sub!(/^\//,'') # remove initial slash to prevent double slash in href
    raise ArgumentError.new('no agent') if sender.nil?
    raise ArgumentError.new('no brand') if brand.nil?
    url =APP_CONFIG['id_base_url'].gsub(/(dev|release|test|production)/,brand.preferred_insurance_division_subdomain.to_s)
    url+="/#{brand.id}/#{sender.insurance_division_id}/"
    url+=sub_path
  end

end
