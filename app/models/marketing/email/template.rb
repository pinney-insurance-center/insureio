class Marketing::Email::Template < Marketing::Template


  # DEPRECATED. Logic to compose messages should be consolidated in the message models.
  def deliver options={}
    Marketing::Email::Message.new({
      template:self
    }.merge(options)).deliver
  end

  #Users can specify temporary quoter url options, which apply to a single delivery.
  #If none are provided, then use this template's defaults.
  attr_accessor :tmp_quoter_url_type
  attr_accessor :tmp_custom_quoter_url
  attr_accessor :tmp_insurance_division_url_page

  def quoter_url_type;             self.tmp_quoter_url_type             || self[:quoter_url_type]; end

  def custom_quoter_url;           self.tmp_custom_quoter_url           || self[:custom_quoter_url]; end

  def insurance_division_url_page; self.tmp_insurance_division_url_page || self[:insurance_division_url_page]; end

  HEADER ={
      status:
          %Q(

    ),
    marketing:
        %Q(

    )
  }

  FOOTER ={
      status:
          %Q(
    <table id="footer" width="536" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
      <th valign="top" scope="col">
        <div align="left" style="font-family: Verdana, Geneva, sans-serif; font-size: 7pt; color: #999; font-style: normal; font-weight: normal; font-variant: normal;">
          {{agent.copyright}}<br />
          {{brand_fallback_to_agent_address}}<br /><br />
          Questions? Call {{agent.phone}}
        </div>
      </th>
      <th valign="top" scope="col">
        <div align="right" style="font-family: Verdana, Geneva, sans-serif; font-size: 7pt; color: #999; font-style: normal; font-weight: normal; font-variant: normal;">
          <a href="mailto:{{ brand_specific_fallback_primary_email_for_agent }}">Contact Us</a>
        </div>
      </th>
      </tr>
    </table>
    <hr />
    ),
    marketing:
        %Q(

        )
  }

  TRACKING_PIXEL= %Q(
    {{tracking_pixel}}
  )

  USE_INS_DIV_QUOTER_URL=0
  USE_CUSTOM_QUOTER_URL=1
  USE_QUOTE_PATH_URL=2
end
