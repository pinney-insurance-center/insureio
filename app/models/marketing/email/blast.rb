class Marketing::Email::Blast < ActiveRecord::Base
  
  attr_accessor :warnings

  belongs_to :user,     class_name: "User"
  belongs_to :search,   class_name: "Reporting::Search"
  belongs_to :template, class_name: "Marketing::Template"

  has_many   :messages, class_name: "Marketing::Email::Message"

  validates  :template, presence:true
  validates  :recipients, presence: true, unless: :search_id
  validate   :has_valid_targets

  after_initialize ->{self.subject||=template.try(:subject); self.body||=template.try(:body); @warnings=[]}

  def target_email_method
    attr_name = "@target_email_method"
    instance_variable_get(attr_name) || instance_variable_set(attr_name, (
      self.template.medium=='Email' ? 'primary_email' : 'carrier_provided_email'
    ).to_s)
  end

  def deliver
    output=if targets.length <= self.class::RECIPIENTS_PER_BATCH
      send_instant
    else
      send_async
    end
    output+="<br>Warnings:#{warnings.join("<br>")}" unless warnings.empty?
    output
  end

  #The returned targets will be either cases (if they came from a search),
  #or strings/consumers/recruits (if they came from a custom list).
  def targets
    @targets||=
    begin
      @warnings=[]
      if search_id.present?
        record_ids=search.get_matching_record_ids_for_user(user)
        list=Crm::Case.where(id:record_ids[1])+Quoting::Quote.where(id:record_ids[2])
        list=filter_targets_missing_address list
        list
      elsif recipients.present?
        list=recipients.to_s.split(/\s|;|,/).compact.uniq.reject(&:blank?)
        list=filter_invalid_emails list
        list=match_to_person_where_possible list
      else
        []
      end
    end
  end

  #Gives instant feedback regarding mailing errors. Blocks application flow until finished.
  #Intended for small batches.
  def send_instant
    count = 0
    bad_addresses = []
    targets.each do |t|
      msg_opts={sender:user, blast_id:id, template:template, suppress_retries:true}
      if t.is_a?(String)
        msg_opts[:recipient]=t
      else
        msg_opts[:target]=t
      end
      self.messages.new(msg_opts)
    end
    begin
      self.messages.each do |outgoing_msg|
        if outgoing_msg.deliver
          count += 1
        else
          if outgoing_msg.failure_msgs.include?('Net::SMTPAuthenticationError')
            errors.add(:base, "SMTP Authentication Error. Have you configured your email settings correctly?")
            break
          elsif outgoing_msg.failure_msgs.include?('Net::SMTPSyntaxError')
            #This should be a very infrequent scenario,
            #since blasts validate email format for custom lists,
            #and the EmailAddress model validates format also.
            bad_address=if outgoing_msg.person
              "#{outgoing_msg.person.name} (id #{outgoing_msg.person.short_id}) #{outgoing_msg.person.send(target_email_method)}"
            else
              outgoing_msg.recipient
            end
            bad_addresses << bad_address
          else
            failed_send_msg="Unable to send to recipient #{outgoing_msg.person.try(:name)} (#{outgoing_msg.person.try(:id)})"
            if outgoing_msg.failure_msgs.present?
              failed_send_msg+=" due to #{outgoing_msg.failure_msgs}."
            else
              failed_send_msg+="."
            end
            errors.add(:base, failed_send_msg)
          end
        end
      end
    rescue => ex
      ExceptionNotifier.notify_exception(ex)
    end
    errors.add(:base, "Error with SMTP Syntax. You probably have invalid addresses for the following: " + bad_addresses.join("; ") ) unless bad_addresses.empty?
    return "Dispatched emails to #{count} of #{targets.length} recipients."
  end

  # Send asynchronously on a pseudo-randomized schedule,
  # in which every +INTERVAL_BETWEEN_BURSTS+ seconds a burst of
  # +BATCHES_AT_A_TIME+ workers are performed sometime within +BURST_DURATION+ seconds,
  # and each worker handles sending a batch of +RECIPIENTS_PER_BATCH+ emails.
  def send_async
    batches =targets.in_groups_of(self.class::RECIPIENTS_PER_BATCH).map{|g| g.compact}

    blast_initiated_at=Time.now

    batches.each_with_index do |batch, batch_num|
      delay_for_this_batch=( (batch_num/self.class::BATCHES_AT_A_TIME)*self.class::INTERVAL_BETWEEN_BURSTS+rand(1..self.class::BURST_DURATION) ).seconds.from_now(blast_initiated_at)
      batch=batch.map{|t| t.is_a?(String) ? t : [t.class.name,t.id] }#best to pass ids instead of objects to an asynchronous process
      #each worker handles a batch.
      BlastEmailWorker.perform_at delay_for_this_batch, id, batch_num, blast_initiated_at, template_id, user_id, batch
    end
    return "Scheduled #{batches.length} sets of #{self.class::RECIPIENTS_PER_BATCH} emails, which will be sent over the course of the next #{(batches.length/self.class::BATCHES_AT_A_TIME)*self.class::INTERVAL_BETWEEN_BURSTS} seconds."
  end

  #Methods for gathering aggregate data:

  def send_ratio
    messages.count.to_f / messages.sent.count
  end

  def view_ratio
    messages.sent.count.to_f / messages.viewed.count
  end

  def average_views_per_message
    view_counts=messages.viewed.pluck(:views)
    view_counts.inject{ |sum, el| sum + el }.to_f / view_counts.size
  end

  #When a blast message is shown after the fact,
  #either to a user or to a consumer through the "Having trouble viewing this message" link,
  #the subject and body need to be re-rendered.
  #The +data_for_rendering+ hash is passed by the message that is being rerendered.
  def rerender_subject data_for_rendering={}
    renderer=Marketing::RenderingService.new(data_for_rendering)
    rendered=renderer.render(subject)
    rendered
  end

  def rerender_body data_for_rendering={}, options={}
    body = self.body.dup

    # Define output Array, for ease of unshifting and pushing
    output = [body]
    header = ''
    footer = ''
    unless options[:no_boilerplate]
      #retrieve proper header and footer based on class and whether it is for a status update or for marketing
      if self.template.try(:template_purpose).try(:name) == "Status"
        header = self.template.class::HEADER[:status]
        footer = self.template.class::FOOTER[:status]
      elsif self.template.try(:template_purpose).try(:name) == "Consumer Marketing"
        header = self.template.class::HEADER[:marketing]
        footer = self.template.class::FOOTER[:marketing]
      elsif self.template.try(:template_purpose).try(:name) == "Agent Marketing"

      elsif self.template.try(:template_purpose).try(:name) == "Status"

      end

      # Add HEADER, either at {{ header }} or at TOP
      if body.match(/{{\s*header\s*}}/)
        body.gsub!(/{{\s*header\s*}}/, header)
      else
        output.unshift header
      end
      # Add FOOTER, either at {{ footer }} or at BOTTOM
      if body.match(/{{\s*footer\s*}}/)
        body.gsub!(/{{\s*footer\s*}}/, footer)
      else
        output.push footer
      end
      # Add TOP_CONTENT & BOTTOM_CONTENT (it's boilerplate)
      output.unshift Marketing::Email::Template::TOP_CONTENT
      output.push Marketing::Email::Template::BOTTOM_CONTENT
      body = output.join
    end
    renderer=Marketing::RenderingService.new(data_for_rendering)
    rendered=renderer.render(body)
    rendered.html_safe
  end

  #Define constants for use when delivering blasts
  RECIPIENTS_PER_BATCH   =20
  BATCHES_AT_A_TIME      =5
  INTERVAL_BETWEEN_BURSTS=5#measured in seconds
  BURST_DURATION         =5#measured in seconds

  LOOSE_EMAIL_VALIDATION_REGEXP=Regexp.new(/^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i)

private

  #This method takes a list of email strings and verifies their format.
  def filter_invalid_emails list
    invalid_emails=[]
    list=list.reject do |t|
      unless t=~LOOSE_EMAIL_VALIDATION_REGEXP
        invalid_emails.push(t)
        true
      end
    end
    @warnings << "The following emails are not valid: #{invalid_emails.join("; ")}" unless invalid_emails.empty?
    list
  end

  #This method takes a list of policy/opportunity objects
  #and verifies that the consumer has a deliverable address.
  def filter_targets_missing_address list
    missing=[]
    list=list.reject do |t|
      if t.consumer.try(target_email_method).nil?
        missing.push(t)
        true
      end
    end
    missing=missing.uniq#in case a search returns multiple cases for the same consumer
    unless missing.empty?
      medium_name=template.medium=='Email' ? 'an email address' : 'a mobile number with carrier'
      missing_names_and_ids=missing.map{|m| "#{m.consumer.name} (#{m.consumer.id})"}.join(', ')
      @warnings << "The following consumers lack #{medium_name}: #{missing_names_and_ids}"
    end
    list
  end

  #This method attempts to match every email address to a consumer/recruit.
  #Retain counts of ambiguous and unmatched emails (but not the emails themselves),
  #to be reported to the user.
  def match_to_person_where_possible list
    ambiguous_target_count=0
    unmatched_target_count=0
    list=list.map do |t|
      possible_matches=scope_possible_matches t
      if possible_matches.length==0
        unmatched_target_count+=1
        t
      elsif possible_matches.length>1
        ambiguous_target_count+=1
        t
      else
        possible_matches.first
      end
    end
    @warnings << " #{ambiguous_target_count} email was ambiguous because it matched multiple consumer or recruit records." if ambiguous_target_count==1
    @warnings << " #{ambiguous_target_count} emails were ambiguous because they matched multiple consumer or recruit records." if ambiguous_target_count>1
    @warnings << " #{unmatched_target_count} #{unmatched_target_count==1 ? 'email' : 'emails'} did not match any consumer or recruit record." if unmatched_target_count>0
    list
  end

  def scope_possible_matches email_string
    s=Consumer.viewable_by(user).
      joins('INNER JOIN email_addresses ON email_addresses.contactable_type="Consumer" AND email_addresses.id=consumers.primary_email_id').
      where('email_addresses.value=?',email_string).all
    s+=User.viewable_by(user).
      joins('INNER JOIN email_addresses ON email_addresses.contactable_type="User" AND  email_addresses.id=users.primary_email_id').
      where('email_addresses.value=?',email_string).
      where(is_recruit:true).all
  end

  #This method verifies that after filtering out invalid/missing emails,
  #there are actually some left.
  def has_valid_targets
    if targets.length==0
      errors.add(:base,"No messages sent because there were no valid emails to send to.")
    end
  end
end