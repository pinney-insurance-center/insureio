class ExcludedCarrier < ActiveRecord::Base
	belongs_to :widget, class_name: "Quoting::Widget"
	belongs_to :carrier

	validates :widget_id, :presence => true
	validates :carrier_id, :presence => true

  validate :unique_join

  def unique_join
    self.class.exists?(
        :widget_id => widget_id,
        :carrier_id => carrier_id
    )
  end
end