# Storage for additional case/quote data which relate only to cases that come
# through IXN or NMB
class IxnExt < ActiveRecord::Base
  serialize :data, Hash

  belongs_to :kase, class_name: 'Crm::Case', foreign_key: :case_id

  SERIALIZED_FIELDS = [] # Populated by `serialize_field`

  # The `serialize_field` helper prevents the need to explicitly perform merges
  # on `data` in order to avoid overwriting unspecified fields
  def self.serialize_field name
    SERIALIZED_FIELDS << name.to_sym

    define_method "#{name}=" do |value|
      if value.blank? && value != false
        data.delete name.to_s
      else
        data[name.to_s] = value
      end
    end

    define_method name do
      data[name.to_s]
    end
  end

  serialize_field :adb_rider
  serialize_field :adb_rider_annual
  serialize_field :adb_rider_monthly
  serialize_field :adb_rider_quarterly
  serialize_field :adb_rider_semi_annual
  serialize_field :agent_notes
  serialize_field :bankruptcy_details
  serialize_field :child_rider_annual
  serialize_field :child_rider_monthly
  serialize_field :child_rider_quarterly
  serialize_field :child_rider_semi_annual
  serialize_field :child_rider_units
  serialize_field :child_wop_rider_annual
  serialize_field :child_wop_rider_monthly
  serialize_field :child_wop_rider_quarterly
  serialize_field :child_wop_rider_semi_annual
  serialize_field :declined_why
  serialize_field :fam_w_apps
  serialize_field :heart_stroke_cancer
  serialize_field :heart_stroke_cancer_details
  serialize_field :parent_death_p60
  serialize_field :prev_addr
  serialize_field :quote_guid
  serialize_field :sibling_death_p60
  serialize_field :spouse_coverage_amt
  serialize_field :src_guid
  serialize_field :unemployed_why
  serialize_field :us_entry
  serialize_field :wop_rider_annual
  serialize_field :wop_rider_monthly
  serialize_field :wop_rider_quarterly
  serialize_field :wop_rider_semi_annual
end
