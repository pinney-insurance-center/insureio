# Service object class for executing lead distribution
class LeadDistribution
  include AuditLogger::Helpers
  audit_logger_name 'lead-distribution'

  # Constructor
  def initialize(consumer)
    @consumer = consumer
  end

  def assign
    return if @consumer.test?

    if @consumer.brand.nil?
      SystemMailer.warning(message:"No brand on consumer to be distributed: #{@consumer.id}\n\n#{@consumer.inspect}")
      return
    end

    user_rule = next_distribution_rule
    if user_rule.nil?
      audit "FAIL: No next_distribution_rule for consumer #{@consumer.id||@consumer.primary_email.try(:value)}"
      agent_id=@consumer.brand.owner_id
      #Users with limited access, or submitting to brands where they are the only member need not be concerned with this.
      if @consumer.brand.owner&.can?(:lead_distribution_config) && @consumer.brand.member_ids.length>1
        no_rule_failure_msg="This is an automated message. "+
          "Lead Distribution failed for this consumer due to the lack of relevant distribution rules. "+
          "Please add distribution rules for brand id #{@consumer.brand_id} and lead_type #{@consumer.lead_type} or specify agent explicitly going forward."
        @consumer.notes.new(critical:true,text:no_rule_failure_msg)
      end
    else
      audit "Consumer #{@consumer.id||@consumer.primary_email.try(:value)} for User #{user_rule.user_id} @ UserRule #{user_rule.id} @ Count #{user_rule.count}.#{@consumer.test? ? " !TEST!" : nil}"
      agent_id=user_rule.user_id
    end

    @consumer.agent_id=agent_id

    user_rule.increment! if user_rule

    agent_id
  end

  def self.table_name_prefix
    'lead_distribution_'
  end

  private

  # Returns a lead distribution user rule corresponding to the user who should be assigned @consumer
  def next_distribution_rule
    # Find or create UserRules
    audit "\tCursor: #{cursor.inspect}"
    candidate_rules = cursor.active_rules.select{|r| !r.off }
    # Fail if pool is empty
    if candidate_rules.empty?
      audit "\t--empty agents pool-- consumer:#{@consumer.id} test:#{!!@consumer.test?}"
      return
    else
      audit "\tCandidate Users: #{candidate_rules.map(&:user_id)}\n\tCandidate Rules: #{candidate_rules.map(&:id)}"
    end
    # Pick next elligible candidate from candidate_rules
    return cursor.next!
  rescue =>ex
    SystemMailer.warning(message:"Encountered an exception when trying to get the next distribution rule for brand #{@consumer.brand_id} and lead_type '#{@consumer.lead_type}'.")
    ExceptionNotifier.notify_exception(ex)
    nil
  end

  # Find or create cursor
  def cursor
    @cursor ||= LeadDistribution::Cursor.where(brand_id:brand_id, lead_type_id:lead_type_id).first || LeadDistribution::Cursor.create(brand_id:brand_id, lead_type_id:lead_type_id)
  end

  # Returns client's lead type id
  def lead_type_id
    @lead_type_id ||= Crm::LeadType.select(:id).find_or_create_by(text: @consumer.try(:lead_type)).id
  end

  # Returns the client's brand id
  def brand_id
    @consumer.try(:brand_id)
  end
end
