class UserSession < Authlogic::Session::Base
  #Without an explicit threshold, this will update every time current_user is checked.
  self.last_request_at_threshold = 2.minutes
end
