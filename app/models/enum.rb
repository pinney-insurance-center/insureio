module Enum
  #Data is loaded by the initializer `load_config.rb` from the file `enums.json`.
  #The enumerated classes that contain minimal custom logic are defined at the end of this file.
  #Others are defined in files that match their class name.


class EnumStereotype
  def initialize options={}
    options.each{|k,v| self.send "#{k}=", v }
  end

  def matches? hash=nil, &block
    if hash.nil?
      yield self
    else
      hash.all? { |k,v| v.is_a?(Array) ? v.include?(send(k)) : v == send(k) }
    end
  end

  def serializable_hash options={}
    output=Hash[instance_variables.map{|name| [name.to_s.gsub('@','').to_sym, instance_variable_get(name)] }]
    if options[:only]
      output.select{|k| options[:only].include?(k)}
    elsif options[:except]
      output.except(options[:except])
    else
      output
    end
  end

  def self.[](name)
    find_by_name(name) || (find_by_value(name) if respond_to?(:value))
  end

  def self.all(reload=false)
    load! if reload or @all.nil?
    @all
  end

  # Loads data for ALL enums from the enums.json file into their respective classes
  def self.load!
    enums=APP_CONFIG['enums']
    enums.each do |class_name,data|
      model = "Enum::#{class_name}".constantize
      columns = Array(data['columns']).map(&:to_sym)
      col_hash = columns.map { |c| [c,true] }.to_h # For O(1) lookup during loading
      columns << :id unless col_hash.has_key?(:id)
      model.send :attr_accessor, *columns
      # add rows to array
      all = data['records'].each_with_index.map do |row,i|
        data = Hash[ columns.zip Array(row) ]
        data[:id] ||= i + 1
        model.new( data )
      end
      # freeze data
      all.each{|row| row.freeze }
      all.freeze
      # set @all attr
      model.send :instance_variable_set, '@all', all
      # Create safety/convenience member(s) for SmallEnumStereotype
      if model < SmallEnumStereotype && col_hash.has_key?(:name)
        model.send :alias_method, :to_s, :name
      end
    end
  end

  def self.count
    all.length
  end

  def self.as_json opts={}
    self.all.as_json opts
  end

  def self.each &block
    all.each &block
  end

  def self.efind hash=nil, &block
    if hash
      all.find do |item|
        hash.all? { |k,v| item.send(k) == v }
      end
    else
      all.find &block
    end
  end

  # This does not return a scope! This is a convenience method
  # for selecting data from the collection.
  # Values in +hash+ which are Arrays will select matches whose
  # value equals any of the values in the Array.
  def self.ewhere hash=nil, &block
    all.select { |instance| instance.matches? hash, &block }
  end

  def self.find *ids
    ids.compact!
    items=[]
    all.map do |e|
      items<<e if ids.include?(e.id)
    end

    #items = all.values_at *ids.map{|i| i.to_i - 1 }
    return ids.length <= 1 ? items.first : items
  end

  def self.first limit=1
    return nil unless @all
    return limit == 1 ? all[0] : all[0...limit]
  end

  # Returns the id(s) for the record(s) that match(es) the given name(s)
  def self.id *names
    names.compact!
    ids = names.map{|name| self.find_by_name(name) }.map{|obj| obj.nil? ? 0 : obj.id }
    return names.length == 1 ? ids.first : ids
  end

  def self.last limit=1
    return limit == 1 ? all[-1] : all[-limit..-1]
  end

  def self.map &block
    all.map &block
  end

  def self.has_and_belongs_to_many assoc, options={}
    options.stringify_keys!
    foreign_key = options['foreign_key'] || "#{self.name.underscore.gsub(/\//,'_')}_ids"
    class_name = options['class_name'] || assoc.to_s.camelize
    # getter (of scope)
    define_method assoc do
      class_name.constantize.where(["#{foreign_key} REGEX ?", "(^|\D)#{self.id}(\D|$)"])
    end
  end

  def self.method_missing sym, *args, &block
    if sym.to_s =~ /^find_by_/
      fields = sym.to_s.sub('find_by_','').split('_and_')
      results = all.dup
      fields.each_with_index do |field, i|
        results.select!{|r| r.send(field) == args[i] }
      end
      return results.first
    end
    super
  end

  def self.sample
    all.sample
  end

  def self.table_name
    self.name.underscore.gsub('/','_')
  end
end

class SmallEnumStereotype < EnumStereotype; end

#Enumerated classes that contain minimal custom logic are defined below.
#################################

class AddressType < EnumStereotype; end
class BenesDesignation < SmallEnumStereotype; end
class BillType < SmallEnumStereotype; end

class BrokerDealer < EnumStereotype

  BROKER_DEALER_FILE = File.join(Rails.root, 'config/data/broker_dealers.csv')

  def self.load!
    file = File.read(BROKER_DEALER_FILE)
    broker_rows = CSV.parse(file, :headers => false)

    self.send :attr_accessor, :id, :name#make getter and setter methods
    all=[]

    broker_rows.each_with_index do |row, index|
      all << self.new({id:index+1,name:row[0]})
    end
    self.send :instance_variable_set, '@all', all
  end

end

class CaseRequirementType < EnumStereotype; end
class CaseReqResponsiblePartyType < EnumStereotype; end
class CaseReqStatus < EnumStereotype; end

class Citizenship < EnumStereotype
  def citizen?
    name == 'US Citizen'
  end
end

class CommissionLevel < EnumStereotype; end
class ContactMethod < EnumStereotype; end
class ContractStatus < EnumStereotype; end

class Country < EnumStereotype
  def to_s
    self.name
  end
end

class DiseaseType < EnumStereotype; end
class DividendType < SmallEnumStereotype; end
class EntityType < EnumStereotype; end
class LastAppliedOutcomeOption < EnumStereotype; end
class LicenseStatus < EnumStereotype; end
class LtcBenefitPeriodOption < EnumStereotype; end
class LtcHealthClassOption < EnumStereotype; end
class LtcInflationProtectionOption < EnumStereotype; end
class MaritalStatus < EnumStereotype; end
class MarketingDynamicField < EnumStereotype; end
class MarketingTemplatePurpose < EnumStereotype; end

class Membership < EnumStereotype

  def self.unpaid
    all.select{|m| 0 === m.pricing}
  end

  def permissions
    list_names = stripe_plan_name.split(/\s*\+\s*/).presence || ['Basic'] # Everyone gets Basic by default now
    list_names.map { |name| Enum::PermissionList[name].list }.flatten
  end

end

class NoteType < EnumStereotype; end

class Ownership < EnumStereotype
  alias_attribute :name, :value

  def self.allowed_for_user user
    output = [find(2)]
    output << find(3) if user.can?(:set_access_descendants)
    output << find(1) if user.can?(:set_access_global)
    return output
  end

  def self.global_id
    @global_id ||= id('global')
  end

  def self.user_id
    @user_id ||= id('user')
  end

  def self.user_and_descendants_id
    @user_and_descendants_id ||= id('user and descendants')
  end
end

class PermissionList < EnumStereotype; end
class PhoneCarrier < EnumStereotype; end
class PhoneType < EnumStereotype; end
class PlanningCat < EnumStereotype; end

class PremiumModeOption < SmallEnumStereotype; end

class ProductType < EnumStereotype
  def product_cat; ProductTypeCat.find(product_cat_id); end
end

class ProductTypeCat < EnumStereotype; end

class Purpose < EnumStereotype
  def purpose_type
    PurposeType.ewhere(id:type_id).first
  end

  def purpose_type_name
    purpose_type.try(:name) || ''
  end

  def is_for_business
    purpose_type.try(:is_for_business)
  end

  def as_json params={}
    output=super params
    output.merge({purpose_type_name:self.purpose_type_name})
  end
end

class PurposeType < EnumStereotype; end

class QuotingWidgetType < EnumStereotype

  #Get only enabled widget types.
  def self.enabled
    self.all.select{|wt| wt.enabled }
  end

  def whitelisted_carriers
    if carrier_whitelist_column.present? && carrier_whitelist_values.present?
      Carrier.where(carrier_whitelist_column=>carrier_whitelist_values).to_a
    else
      Carrier.all
    end
  end

  def quote_class
    quote_class_name.constantize
  end
end

class RelationshipType < EnumStereotype; end
class RttHealthClassOption < EnumStereotype; end

class SalesStage < EnumStereotype
  def categories
    StatusTypeCategory.ewhere(sales_stage_id:self.id)
  end
end
class ScorRegion < EnumStereotype; end
class SecurityQuestion < EnumStereotype; end
class SpiaIncomeOption < EnumStereotype; end

class State < EnumStereotype
  has_and_belongs_to_many :usage_contracts, class_name: "Usage::Contract"

  # scope get state id
  def self.get_state_id_by_name(state_name)
    where("name = ?", state_name).first.try(:id)
  end

  def self.[](name)
    super || find_by_abbrev(name)
  end

  def self.find_by_abbrev name
    name = 'NY' if name == 'NY-B' || name == 'NY-NB'
    super(name)
  end

  def to_s
    self.name
  end
end

class StatusTypeCategory < EnumStereotype
  def sales_stage
    SalesStage.find(self.sales_stage_id)
  end
end

class TagType < EnumStereotype; end
class TaskType < EnumStereotype
  def self.handled_by_system
    self.all.select{|tt| tt.handled_by_system_only }
  end
end

class Tenant < EnumStereotype

  def self.as_json opts={}
    super(opts.merge({except: :default_parent_id}))
  end

  # @return url with tenant_name (subdomain) prepended and 'pinney' removed.
  # Output will be the same as input if sender.subdomain is not present or if input is not an Insureio link.
  # For example, https://pinney.insureio.com:33/foo/bar => https://ezlife.insureio.com:33/foo/bar
  def self.subdomain_string(url, tenant_name)
    return (uri = self.subdomain_uri(url, tenant_name)) ? uri.to_s : url
  end

  # @return a URI or nil if no change is indicated.
  def self.subdomain_uri(url, tenant_name)
    tenant_name = 'pinney' if tenant_name.blank?
    uri = URI.parse(url)
    domain = uri.host.match(/(?:^|\.)(insureio.\w+)/).try(:[], 1)
    if domain
      dst_host = [tenant_name, domain].join('.')
      if uri.host != dst_host
        uri.host = dst_host
        uri
      end
    end
  end

  def logo;       "/tenants/#{self.name}/img/#{self.name}-logo.png" unless self.name=='pinney'; end
  def stylesheet; "/tenants/#{self.name}/stylesheets/branding-#{self.name}.css"; end
end

class TliHealthClassOption < EnumStereotype
  def self.parse name, tobacco=false
    # Check for Best Class/Preferred Plus
    return efind(name: 'Best Class') if name =~ /Preferred Plus/i
    # Check for name + tobacco exact match
    regex = name
    regex += ' (- )?Tobacco' unless !tobacco || name =~ /Tobacco$/i
    return found if found = efind { |item| item.name =~ /^#{regex}$/ }
    # Check lettered table ratings (IXN does this)
    if name =~ /^[A-L]$/
      regex = /^Table \d\/#{name}/
      regex = /#{regex} - Tobacco/ if tobacco
      return efind { |item| item.name =~ regex }
    end
  end
end

class TliDurationOption < EnumStereotype
  def initialize *args
    super
    self.name ||= ixn_code
  end

  def short_name
    shortened=self.name

    replace_list={
      ' Years'              =>'Y',
      'To Age '             =>'To ',
      '-Year Return of Premium' =>'Y ROP'
    }.each{|k,v| shortened=shortened.gsub(k,v)}

    return shortened
  end

  def self.for_tli_widget
    all.select{|o| o.name_for_tli_widget.present? }
  end

  def to_s
    self.name
  end

end

class TliTableRatingOption < EnumStereotype; end


class ReportingModelMapping < SmallEnumStereotype
  #These methods are needed because YAML makes it a pain to neatly define nested hashes,
  #and ActiveRecord takes a nested hash to describe association chains.
  def map_for_join
    array_to_nested_hash self.assoc_chain_for_join
  end

  def map_for_preload
    chain = assoc_chain_for_preload_if_distinct || assoc_chain_for_join
    array_to_nested_hash chain
  end

  def map_for_access
    chain = method_chain_for_access_if_distinct || assoc_chain_for_join
    array_to_nested_hash chain
  end

  def array_to_nested_hash a
    return a.to_sym if a.is_a?(String)
    return a if !a.is_a?(Array) || a.empty?
    return a[0] if a.length==1
    {a[0]=> array_to_nested_hash(a.slice(1,a.length-1)) }
  end
end

class ReportingByCaseModelMapping < ReportingModelMapping
end

class ReportingByOpportunityModelMapping < ReportingModelMapping
end

class ReportingByCaseCriteriaFields < EnumStereotype
  def model_mapping
    ReportingByCaseModelMapping.ewhere(model_key:self.model_mapping_key)
  end
  def self.serialize_for_display
    self.all.map do |g|
      attrs=g.as_json.except('id')
    end
  end
end

class ReportingByCaseShowableFieldGroups < EnumStereotype

  def model_mapping
    ReportingByCaseModelMapping.ewhere(model_key:self.model_name)
  end

  def fields
    ReportingByCaseShowableFields.ewhere(display_group_id:self.id)
  end

  def self.as_json
    self.all.map do |g|
      attrs=g.as_json.except('id','association_chain_from_case')
      fields=g.fields.as_json.map{|f| f.except('id','display_group_id','extra_assoc_to_preload') }
      attrs.merge!({'fields'=>fields})
      attrs.deep_transform_keys!{|key| key.camelize(:lower) }
    end
  end

end

class ReportingByCaseShowableFields < EnumStereotype

  def display_group
    ReportingByCaseShowableFieldGroups.find( self.display_group_id )
  end

  def display_group_name
    self.display_group.display_name
  end

  def model_key
    self.display_group.model_name
  end

  #These are necessary mostly for calculated or delegated values.
  #We could intead get delegated fields from their actual model, but
  #a less complex hash structure makes it easier for readers to follow.
  #Having an enumerable mapping for this also means less one-off code in
  #method `preload_associations`.
  def extra_map_for_preload
    return unless self.extra_assoc_to_preload
    ReportingByCaseModelMapping.ewhere(model_key:self.extra_assoc_to_preload).first&.map_for_preload
  end

end

class UsageRole < EnumStereotype
  #These roles are used for staff and task assignment.

  SYSTEM_ID                   = 1
  DEVELOPER_ID                = 2
  ADMIN_ID                    = 3
  AGENT_ID                    = 4
  INSURANCE_COORDINATOR_ID    = 5
  UNDERWRITER_ID              = 6
  APPLICATION_SPECIALIST_ID   = 7
  ADMINISTRATIVE_ASSISTANT_ID = 8
  CASE_MANAGER_ID             = 9
  MANAGER_ID                  = 10

  def taskable?
    self.class.taskable.include? self
  end

  def self.agent_id
    AGENT_ID
  end

  def self.available user
    user.super_edit ? all : all.select{|r| r.level >= 0 }
  end

  def self.taskable
    #Roles 4-9 + System User
    [1,4,5,6,7,8,9].map{|i| find(i)}
  end

end

class WebType < EnumStereotype; end

class NotFound < StandardError; end

end#end of module