module Paranoia

  def self.included(base)
    base.extend ClassMethods
    base.before_update :paranoid_log
  end

  def paranoid_delete
    self.enabled = false if respond_to?(:enabled=)
    self.active = false if respond_to?(:active=)
    self.active_or_enabled = false if respond_to?(:active_or_enabled=)
    save if persisted?
  end

  def paranoid_destroy
    # Had to run these changes outside of block because the object would be frozen otherwise
    self.enabled = false if respond_to?(:enabled=)
    self.active = false if respond_to?(:active=)
    self.active_or_enabled = false if respond_to?(:active_or_enabled=)
    save if persisted?
    run_callbacks :destroy do
    end
    return true
  end

  module ClassMethods

    def delete_all *args
      raise "Not allowed for paranoid class"
    end

    def destroy_all *args
      raise "Not allowed for paranoid class"
    end

  end

private

  def paranoid_log
    cull_changes = -> changes { changes.reject{|k,v| %w[last_request_at last_request_ip last_login_at last_login_ip perishable_token current_login_at current_login_ip failed_login_count login_count].include?(k) } }
    if changed? && cull_changes.call(changes).present?
      msg = [id, 'web:', Thread.current[:current_user_id], 'ssh:', ENV['SSH_CLIENT'], 'usr:', ENV['USER'], 'con:', !!defined?(Rails::Console), 'chg:', cull_changes.call(changes)].join("\t")
      FileUtils.mkdir_p 'log/paranoia'
      AuditLogger['paranoia/'+self.class.name.gsub('::','_')].info msg
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
  end

end