class Attachment < ActiveRecord::Base

  belongs_to       :kase,       class_name:'Crm::Case', foreign_key:'case_id'
  belongs_to       :person,     polymorphic:true
  belongs_to       :created_by, class_name:'User'
  belongs_to       :updated_by, class_name:'User'

  counter_culture  :person, column_name: proc {|a| a.person_type=='Consumer' ? 'attachments_count' : nil }

  attr_accessor :file_for_upload

  after_create :upload_file
  # Infer consumer_id if this is attached to case
  before_validation ->{ self.person ||= kase.try(:consumer) }, if: :new_record?
  before_create ->{ self.created_by_id=Thread.current[:current_user_id] }
  before_update ->{ self.updated_by_id=Thread.current[:current_user_id] }

  validates_presence_of :person

  delegate :content_type,:content_length,
    to: :s3_file, prefix: false, allow_nil:true
  alias_method :size, :content_length

  def binary_content
    s3_file.body.try(:read)
  end

  def extension
    self.file_name.try(:split,'.').try(:last) || ''
  end

  #The name except for the extension.
  def title
    parts=self.file_name.try(:split,'.')||[]
    parts[0..parts.length-2].join('.')
  end

  def default_serialization
    as_json({
      except:[:person_id,:person_type,:case_id,:file_name],
      methods:[:title,:extension,:content_type,:size,:created_by_name,:updated_by_name]
    })
  end

  def destroy
    aws_s3_client.delete_object s3_opts
    super
  end

  def upload_file
    return unless file_for_upload
    aws_s3_client.put_object s3_opts.merge(body: file_for_upload.read, content_type: file_for_upload.content_type)
  end

  def s3_file
    aws_s3_client.get_object s3_opts
  end

  CONFIG=APP_CONFIG['aws']
  BUCKET_NAME=(CONFIG['s3_bucket'] ||= 'dataraptor')+'-'+Rails.env

  private

  #This method makes use of gem `aws-sdk-s3`.
  def aws_s3_client
    @aws_s3_client ||=
    Aws::S3::Client.new({
      access_key_id:    CONFIG['s3_key_id'],
      secret_access_key:CONFIG['s3_secret'],
      region:           CONFIG['region'] ||= 'us-west-1'
    })
  end

  def s3_opts
    {
      bucket: BUCKET_NAME,
      key: "uploads/attachment/#{self.id}/#{self.file_name}",
    }
  end
end
