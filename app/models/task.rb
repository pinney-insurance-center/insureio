class Task < ActiveRecord::Base

  MAX_FAILED_ATTEMPTS = 4
  PREMIUM_APPLICABLE = %w(Crm::Case Quoting::Quote)

  attr_accessor :due_at_date, :skip_cbs_and_validation
  include ActionView::Helpers::SanitizeHelper

  audit_logger_name 'task_execution'

  

  belongs_to      :assigned_to,    class_name: "User",         :foreign_key => :assigned_to_id
  belongs_to      :completed_by,   class_name: 'User'
  belongs_to      :creator,        class_name: "User",         :foreign_key => :created_by_id
  belongs_to      :person,         polymorphic:true, inverse_of: :tasks
  belongs_to      :message,        class_name: 'Marketing::Message'
  belongs_to      :template,       class_name: 'Marketing::Template', :foreign_key => :template_id
  belongs_to      :origin,         polymorphic:true,                  inverse_of: :tasks
  belongs_to      :status,         polymorphic:true,                  foreign_key:'origin_id'
  belongs_to      :sequenceable,   polymorphic:true,                  foreign_key:'sequenceable_id'

  belongs_to_enum :task_type,      class_name: "Enum::TaskType"

  counter_culture :person,
    column_name: proc {|t| t.person_type=='Consumer' && !t.active ? 'old_tasks_count' : nil },
    column_names: {
      ["tasks.person_type='Consumer' AND tasks.active IS FALSE"] => 'old_tasks_count'
    }

  before_update   :update_sequenceable_staff_assignment,
                  ->{self.active=false if suspended || completed_at; true},
                  if: :persisted?, unless: :skip_cbs_and_validation

  before_create   :infer_tz_offset_and_state,
                  :infer_status_type_name,
                  :ensure_person!

  before_save     :infer_assigned_to_name,
                  :infer_completed_by_name, unless: :skip_cbs_and_validation

  after_save      :update_subscription,
                  ->{ schedule false, 1, true },
                  unless: :skip_cbs_and_validation


  alias_method    :status,        :origin
  alias_method    :status=,       :origin=
  alias_attribute :status_id,     :origin_id
  alias_attribute :status_type,   :origin_type
  alias_attribute :name,          :label

  # Require template for email-type tasks
  validate -> { errors.add(:template, "cannot be blank") if template.nil? }, if: :email?, unless: :skip_cbs_and_validation

  scope :joining_origin, lambda {
    joins(" LEFT JOIN crm_statuses            ON tasks.origin_type = 'Crm::Status'             AND crm_statuses.id            =tasks.origin_id")
    .joins("LEFT JOIN marketing_subscriptions ON tasks.origin_type = 'Marketing::Subscription' AND marketing_subscriptions.id =tasks.origin_id")
  }
  scope :joining_person, lambda {
    joins(" LEFT JOIN users     ON tasks.person_type = 'User'     AND users.id     =tasks.person_id")
    .joins("LEFT JOIN consumers ON tasks.person_type = 'Consumer' AND consumers.id =tasks.person_id")
  }

  scope :get_earliest_task, lambda { |sequenceable|
    where("completed_at is NULL AND origin_id = ?", sequenceable.status_id)
    .order("due_at ASC")
  }

  scope :of_type, lambda { |type_name|
    where(task_type_id: Enum::TaskType.id(type_name))
  }

  # Scope active, incomplete tasks assigned to given users
  scope :for_users, lambda { |user_ids|
    user_ids.flatten!
    user_ids.compact!
    user_ids.each_with_index{|obj,i| user_ids[i] = obj.id unless obj.is_a?(Fixnum) }
    user_ids.uniq!

    where(
      "tasks.assigned_to_id IN (?) AND tasks.task_type_id NOT IN (?)",
      user_ids,
      Enum::TaskType::handled_by_system.map(&:id)
    )
    .active
    .incomplete
  }

  # Returns all records created within the provided date range
  scope :created_in_range, lambda{ |sdate, edate|
    sdate = (sdate.to_date rescue nil)
    edate = (edate.to_date  rescue nil)
    if sdate.present? && edate.present?
      where('created_at BETWEEN ? AND ?', sdate, (edate || Date.today))
    elsif sdate.present?
      where('created_at >= ?', sdate)
    elsif edate.present?
      where('created_at <= ?', edate)
    end
  }
  # Returns all records due within the provided date range
  scope :due_in_range, lambda{ |sdate, edate|
    sdate = (sdate.to_date rescue nil)
    edate = (edate.to_date  rescue nil)
    if sdate.present? && edate.present?
      where('due_at BETWEEN ? AND ?', sdate, (edate || Date.today))
    elsif sdate.present?
      where('due_at >= ?', sdate)
    elsif edate.present?
      where('due_at <= ?', edate)
    end
  }


  # Scope active, incomplete tasks associated with statusable entities that belong to given users
  scope :unassigned_for_users, lambda { |user_ids, role_id=''|
    user_ids.flatten!
    user_ids.compact!
    user_ids.each_with_index{|obj,i| user_ids[i] = obj.id unless obj.is_a?(Fixnum) }
    user_ids.uniq!

    any_role= role_id.blank? ? 'TRUE' : 'FALSE'

    joining_person
    .where( %Q(
      tasks.assigned_to_id IS NULL AND task_type_id NOT IN (?)
      AND (#{any_role} OR tasks.role_id=?)
      AND (consumers.agent_id IN (?) OR users.parent_id IN (?) )
     ), Enum::TaskType.handled_by_system.map(&:id), role_id, user_ids, user_ids )
    .active
    .incomplete
  }

  # Scope active, incomplete tasks regardless of the ownership of their associated models.
  scope :unassigned, lambda {
    where('tasks.assigned_to_id IS NULL AND task_type_id NOT IN (?)',
      Enum::TaskType.handled_by_system.map(&:id)
    )
    .active
    .incomplete
  }

  scope :complete, lambda { where("completed_at IS NOT NULL") }
  scope :incomplete, lambda { where("completed_at IS NULL OR completed_at=?", '')}

  scope :person_name_like, lambda { |str| where('tasks.person_name like ?', "%#{str.gsub(/\s+/,'%')}%") }
  scope :label_like, lambda { |str| where 'tasks.label LIKE ?', "%#{str.gsub(/\s+/,'%')}%" }
  scope :status_type_name_like, lambda { |str| where 'tasks.status_type_name LIKE ?', "%#{str.gsub(/\s+/,'%')}%" }

  #Status type (origin type) id should be denormalized onto task records in future.
  scope :status_type_id, lambda { |st_id|
    joining_origin
    .where('tasks.origin_type="Crm::Status" AND crm_statuses.status_type_id=?',st_id)
  }

  # Scope Tasks that pertain to active people or cases
  scope :active, lambda { where(active:true) }

  # Scope Tasks that are due to be completed
  scope :due, lambda { where(completed_at:nil).where('due_at <= ?', Time.now) }

  # Scope Tasks that should be executed by the system (using 'whenever' gem)
  scope :handled_by_system, lambda { where task_type_id:Enum::TaskType.handled_by_system.map(&:id) }

  #DEPRECATED. Use `handled_by_system`.
  scope :system_tasks, lambda { SystemMailer.deprecated_method_warning; handled_by_system}

  # Scope evergreen tasks
  scope :evergreen, lambda { where(evergreen: true) }

  # Scopes auto-dialing tasks that are due
  scope :auto_dials_to_run_now, lambda {
    all
    .due
    .where(task_type_id:Enum::TaskType.id('phone dial'))
    .active
    .order(:due_at)
  }

  def person_name
    read_attribute( :person_name) || (
      write_attribute(:person_name, self.person.try(:name)) rescue nil
      begin
        self.save unless self.readonly?#so that on subsequent requests, the field will be populated.
      rescue ActiveRecord::StaleObjectError
        self.person.try(:name)
      end
      read_attribute( :person_name)
    )
  end

  def delay= days_n
    self.due_at = Time.now + (days_n).days
  end

  def editable? user
    self.assigned_to_id==user.id || user.super_edit || user.can?(:task_super_edit)
  end

  #allows us to get and set pseudo attributes due_at_human and completed_at_human
  pseudo_datetime_accessor :due_at
  pseudo_datetime_accessor :completed_at

  def active? ignore_column=false
    #Check columns on self first, so if those dont pass,
    #no other records will need to be loaded.
    return false if self[:active] == false && !ignore_column
    return false if self.suspended # here for thoroughness. tasks should never be marked active and suspended.
    return false if self.completed_at # here for thoroughness. tasks should never be marked active and completed.
    if failed_attempts >= MAX_FAILED_ATTEMPTS # here for thoroughness. tasks should never be marked active after the max failed attempts.
      SystemMailer.warning message:"Too many failed attempts for task execution. #{failed_attempts}/#{MAX_FAILED_ATTEMPTS} failed attempts for Task #{id}."
      return false
    end

    if !evergreen && origin && !origin.active
      origin_is_a_non_withdrawn_status=origin.is_a?(Crm::Status) && !origin.indicates_closed
      origin_is_a_cancelled_subscription=origin.is_a?(Marketing::Subscription)
      return false if origin_is_a_non_withdrawn_status || origin_is_a_cancelled_subscription
    end

    #Placed policies shouldn't fire tasks from previous statuses.
    relates_to_a_placed_policy=sequenceable.is_a?(Crm::Case) && sequenceable.sales_stage_id.to_i>=Enum::SalesStage.id('Placed')
    originates_from_a_previous_status=origin.is_a?(Crm::Status) && origin.try(:sales_stage_id).to_i<5

    return false if relates_to_a_placed_policy && originates_from_a_previous_status

    return false if !person.try(:active_or_enabled)

    if self.template
      purpose_name=self.template.template_purpose.try(:name)
      status_opted_out=self.person.status_unsubscribe && purpose_name=="Status"
      con_mktg_opted_out=self.person.is_a?(Consumer) && self.person.marketing_unsubscribe && purpose_name=="Consumer Marketing"
      user_mktg_opted_out=self.person.is_a?(User) && self.person.marketing_unsubscribe && purpose_name=="Agent Marketing"
      if status_opted_out || con_mktg_opted_out || user_mktg_opted_out
        return false
      end
    end

    return true
  end

  def agent
    sequenceable.try(:agent) || (person.respond_to?(:agent) && person.agent) || (person.respond_to?(:owner) && person.owner)
  end

  def agent_id
    sequenceable.try(:agent_id) || (person.respond_to?(:agent_id) && person.agent_id) || (person.respond_to?(:owner_id) && person.owner_id)
  end

  # Find a User to charge with this task.
  # Will not make assignment unless self.assigned_to is nil
  def auto_assign! role_id=nil
    return assigned_to_id if assigned_to_id
    audit log:'status'
    role_id ||= self.role_id
    return if role_id.nil?

    if role_id==Enum::UsageRole.id('agent')
      return self.assigned_to_id=agent_id
    end
    # Search StaffAssignment on Case and User
    [sequenceable, agent].each do |staff_assignment_owner|
      if self.assigned_to_id = staff_assignment_owner.try(:staff_assignment).try(:user_id_for_role_id, role_id)
        return assigned_to_id
      end
    end
    # Use system default
    self.assigned_to_id = Usage::StaffAssignment.default_user_id_for_role_id(role_id)
  end

  # Returns the case manager for the case for this Task
  def case_manager
    staff_assignment(:case_manager)
  end

  def due?
    due_at and (Time.now >= due_at)
  end

  # Returns the person to whom this task pertains
  # Person is stored as an attribute in order to prevent a stack level too deep error.
  def person
    @person ||
    @person= (association(:person).loaded? && association(:person).reader) ||
    @person= (person_type.present? && person_id.present?) ? (person_type.constantize).unscoped.find(person_id) : origin.person
  rescue => ex
    ExceptionNotifier.notify_exception ex, data: { message:"Task #{id} lacks an association with a person" }
    nil
  end

  def print_letter
    #need to implement
  end

  def send_recorded_call
    #need to implement
  end

  def crm_task_sms
    self.send_sms_text if self.try(:status).try(:statusable).try(:active) && self.get_template.present?
  end

  def send_sms_text
    begin
      Raise NotImplemented
    rescue Exception => e
      Rails.logger.error e.inspect
    end
  end

  def execute(force=false)
    return false if (completed_at || suspended) unless force
    audit "task_type -- #{task_type_id}"
    # attempt to execute email, phone, sms, or cetera
    completion = case task_type_id
    when Enum::TaskType.id('phone dial')
      _dial_phone
    when Enum::TaskType.id('email consumer'), Enum::TaskType.id('email agent')
      _send_email
    when Enum::TaskType.id('sms'), Enum::TaskType.id('sms agent')
      _send_sms
    end
    audit ['completed:', !!completion].join(' ')
    # update record
    self.is_executing = false # can't mass-assign this attribute
    if completion
      self.completed_at = Time.now
    else
      self.failed_attempts += 1
    end
    save(validate:false)
  rescue ActiveRecord::StaleObjectError => ex
    audit ex, level: :warn
    schedule true
  rescue Timeout::Error => ex
    increment_failed_attempts
    audit ex, level: :warn
    schedule true
  end

  # Called by background processes, just as rake tasks or delayed_job
  def execute_from_bg
    if !active?
      update_column :active, false
    elsif due?
      #This also checks and increments lock version, making any other copies in memory stale.
      #Beware that operations like `update_column` are not restricted by and do not update the optimistic lock.
      update_attributes is_executing: true, skip_cbs_and_validation: true
      execute
    else
      if due_at>1.day.from_now
        TaskWorker.sidekiq_options_hash['queue']=:io_tasks_w_delay_gt_1_day
        TaskWorker.perform_at due_at, id
        TaskWorker.sidekiq_options_hash['queue']=:io_tasks
      else
        TaskWorker.perform_at due_at, id
      end
    end
  rescue ActiveRecord::StaleObjectError => ex # no need to email developers
    audit body:ex, level: :error
  end

  # Returns the staff assignment for this Task's Case or Person
  # If an arg is supplied, searches for staff for given method name
  def staff_assignment(arg=nil)
    @staff_assignment ||= (status && status.case.staff_assignment) || person.staff_assignment || agent.staff_assignment
    @staff_assignment[arg]
  end

  def template_name
    template = self.get_template
    template.try(:name)
  end

  def template_name=(name)
    #need to implement
  end

  def get_template
    self.template
  end

  def self.next_auto_dial(user_or_user_id)
    where(assigned_to_id:user_or_user_id.to_i)
    .where(task_type_id:Enum::TaskType.id('phone dial'))
    .active
    .due
    .readonly(false)
    .first
  end

  # Return an array of queued, due tasks (incl. evergreen and active)
  def self.queued(user, scope=nil)
    scope ||= self.all
    scope.where(assigned_to_id:user.id).due.active.readonly(false)
  end

  def due_at_date=date
    self.due_at = DateTime.strptime(date, "%m/%d/%Y")
  end

  def due_at_date
    self.due_at.try(:strftime, "%m/%d/%Y")
  end

  def system?
    Enum::TaskType.handled_by_system.include?(task_type)
  end

  #Called by `TaskBuilder` in order to limit task execution to business hours and weekday evenings.
  def holday_or_weekend_scheduled?
    timezone   = person.try(:primary_address).try(:timezone)
    timezone ||= person.agent.try(:primary_address).try(:timezone) if person.try(:respond_to?, :agent)
    timezone ||= ActiveSupport::TimeZone.new ActiveSupport::TimeZone.find_by_zipcode(95661)
    zoned_time = timezone.at(due_at.to_f)
    return true if Holidays.on(zoned_time, :us).present?
    return true if zoned_time.saturday?
    return true if zoned_time.sunday?
    return true if zoned_time.monday? && zoned_time.hour < 9
    return true if zoned_time.friday? && zoned_time.hour >= 17
    false
  end

 def premium_applicable?
   PREMIUM_APPLICABLE.include? sequenceable_type
 end

 def annualized_premium
   return unless premium_applicable?
   if sequenceable_type.eql?('Crm::Case')
     sequenceable.try(:current_details).try(:annualized_premium)
   else
     sequenceable.try(:annualized_premium)
   end
 end

 def brand_name_of_consumer
   if person_type.eql?('Consumer')
     @person ||
     @person= (person_type.present? && person_id.present?) ? (person_type.constantize).unscoped.find(person_id) : origin.person
     @person.present? ? @person.brand_name : nil
   else
    nil
  end
 end

private

  def update_subscription
    return true if origin_type!='Marketing::Subscription'
    return true if evergreen==1 || completed_at==nil
    return true if origin.tasks.incomplete.where('evergreen IS NOT TRUE').count>0
    origin.dequeue
    return true
  end


  # Publish to Redis, which publishes to websocket for live update to users' browser
  def publish_to_redis recursion=0
    origin_is_status=origin.is_a? Crm::Status

    if origin_is_status || self.assigned_to_id.present?
      $redis.publish(APP_CONFIG['socket']['redis_channel_for_socket'], {
        type: 'task',
        recipients: [assigned_to_id, person.try(:contact).try(:owner).try(:id)].compact,
        data: as_json
      }.to_json)
    end
  rescue Redis::TimeoutError => ex
    if recursion < 1
      load 'config/initializers/redis.rb' # replace redis connection
      publish_to_redis recursion+1
    else
      ExceptionNotifier.notify_exception(ex)
    end
    return true
  rescue Redis::CannotConnectError => ex
    %x( [ -x #{APP_CONFIG['redis_executable']} ] && #{APP_CONFIG['redis_executable']} stop && #{APP_CONFIG['redis_executable']} start )
    ExceptionNotifier.notify_exception(ex)
    #Must return true because publishing to redis is much lower priority than actually creating the task.
    #If everything up to this point succeeds, then task creation should succeed.
    return true
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
    return true
  end

  # Atomically increment +failed_attempts+ while bypassing optimistic lock
  def increment_failed_attempts
    self.class.connection.execute("update #{self.class.table_name} set failed_attempts = 0 where failed_attempts is null and id = #{self.id};") if failed_attempts.nil?
    self.class.connection.execute("update #{self.class.table_name} set failed_attempts=failed_attempts+1, is_executing=false where id = #{self.id};")
  end

  def infer_assigned_to_name
    self.assigned_to_name = assigned_to.try(:short_full_name) if assigned_to_id_changed?
    return true
  end

  def infer_completed_by_name
    self.completed_by_name = completed_by.try(:short_full_name) if completed_by_id_changed?
    return true
  end

  def infer_status_type_name
    if self.origin_type=='Crm::Status' && origin
      self.status_type_name = origin.status_type_name || origin.status_type.try(:name)
    elsif self.origin_type=='Marketing::Subscription'
      self.status_type_name = origin.campaign.try(:name)
    end
  end

  def infer_tz_offset_and_state
    return unless person.present?
    self.state_id = (person.respond_to?(:state_id) && person.state_id) || person.primary_address.try(:state_id)
    self.tz_offset = person.try(:tz_offset).to_f
    return true
  end

  #Determines which person should receive the communication.
  def recipient_record
    @recipient_record ||=
     begin
      if self.task_type.try(:name) =~ /agent/
        if self.sequenceable_type=='Crm::Case'
          self.sequenceable.try(:agent)
        else
          self.person.try(:owner)
        end
      else
        self.person
      end
    end
  end

  #Determines which email address should receive the communication.
  def recipient
    @recipient ||=
    begin
      return nil unless recipient_record
      case task_type_id
      when Enum::TaskType.id('email consumer'), Enum::TaskType.id('email agent')
        recipient_record.primary_email.to_s
      when Enum::TaskType.id('sms consumer'), Enum::TaskType.id('sms agent')
        recipient_record.carrier_provided_email
      end
    rescue => ex
      ExceptionNotifier.notify_exception(ex)
      nil
    end
  end

  def schedule disregard_due_at=false, delay=1, assume_active=false
    return true if !system?
    if !assume_active && !active?
      update_column :active, false
      return true
    end
    if disregard_due_at || !scheduled || due_at_changed? || due?
      if disregard_due_at
        TaskWorker.perform_at delay.minutes.from_now, id
      elsif due?
        #A 4 second delay is long enough to ensure that execution happens after any other transactions modifying this task have completed.
        TaskWorker.perform_at 4.seconds.from_now, id
      else
        if due_at>1.day.from_now
          TaskWorker.sidekiq_options_hash['queue']=:io_tasks_w_delay_gt_1_day
          TaskWorker.perform_at due_at, id
          TaskWorker.sidekiq_options_hash['queue']=:io_tasks
        else
          TaskWorker.perform_at due_at, id
        end
      end
    end
    #If the Sidekiq queue ever gets purged, then `scheduled` should be set back to false for all active tasks.
    update_column(:scheduled, true) unless scheduled
  rescue Redis::CannotConnectError => ex
    %x( [ -x #{APP_CONFIG['redis_executable']} ] && #{APP_CONFIG['redis_executable']} stop && #{APP_CONFIG['redis_executable']} start )
    ExceptionNotifier.notify_exception(ex)
  rescue => ex
    #Do not listen only for `Redis::CannotConnectError`,
    #because a socket issue can generate other exception types.
    #Do not attempt to restart Redis for other exception types.
    #There are too many tasks created too often for that to be reasonable
    #if there is a problem that restarting the process will not solve.
    ExceptionNotifier.notify_exception(ex)
  ensure
    #Must return true because scheduling execution is much lower priority than actually creating the task.
    #Rake task `crm:execute_past_due_tasks` runs every evening and can be run manually to clean up after such issues.
    #If everything up to this point succeeds, then task creation should succeed.
    return true
  end

  def _dial_phone
    audit
    return false
  end

  # Sets person id and type if nil
  def ensure_person!
    if read_attribute(:person_name).nil?
      self.person_id   = person.id
      self.person_type = person.class.name
      self.person_name = person.full_name
      self.tenant_id   = person.tenant_id
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
  end

  # Returns a Hash of params suitable for passing to a constructor of Markting::Message
  def message_params(sending_permission=:email)
    is_marketing=(origin_type=='Marketing::Subscription')
    {
      marketing:      is_marketing,
      template:       template,

      sender:         assigned_to || agent,
      target:         sequenceable|| person,

      brand:          brand,
      recipient:      recipient
    }
  end

  def url
    case origin_type
    when 'Marketing::Subscription'
      case origin.try(:person_type)
      when 'User'
        "/users/#{origin.person_id}"
      when 'Consumer'
        "/consumers/#{origin.person_id}"
      when 'Brand'
        "/brands/#{origin.person_id}/edit"
      end
    when 'Crm::Status'
      case origin.try(:statusable_type)
      when 'Crm::Case'
        "/consumers/#{person_id}?widget=policies&case_id=#{origin.statusable_id}&task_id=#{id}&case_detail_tab=Admin"
      when 'User'
        "/users/#{person_id}"
      end
    end
  end

  def _send_email
    raw_message = nil
    raise Marketing::SMTPError, "No template for email task #{id}" unless self.template
    #Checks for existence of a message record first to avoid creating extra message records.
    #Saves early in order to preserve the association.
    self.message||= Marketing::Email::Message.create(message_params)
    update_column :message_id, message.id
    raw_message = message.deliver
  rescue Marketing::RenderingError => ex
    self.active=false
    sender = agent
    settings = sender.brand_specific_fallback_primary_email(brand)
    statusable_type=if origin && origin.statusable.is_a?(Crm::Case)
      'policy'
    elsif origin && origin.statusable.is_a?(Quoting::Quote)
      'opportunity'
    elsif origin && origin.statusable.is_a?(Marketing::Subscription)
      'marketing subscription'
    else
      origin.try(:statusable).try(:class).try(:name)
    end
    missing_info_msg="The message for task id #{self.id} (#{self.name}) failed to send to client id #{person.short_id} (#{person.name}),"+
      " related to #{statusable_type} id #{origin.try(:statusable).try(:id)}, due to missing information."+
      " The specifics of this error are as follows:\n#{ex.message}"
    SystemMailer.generic(sender.primary_email, missing_info_msg).deliver if sender.primary_email
  rescue Net::SMTPAuthenticationError => ex
    self.active=false
    sender = agent
    settings = sender.brand_specific_fallback_primary_email(brand)
    smtp_setup_msg="Your Insureio SMTP info failed to authenticate. Have you set up your email account correctly for the following?\n#{settings.to_yaml}"
    SystemMailer.generic(sender.primary_email, smtp_setup_msg).deliver if sender.primary_email
  rescue Marketing::SMTPError => ex
    self[:active] = false
    SystemMailer.warning ex, debug:self, recipients:agent.try(:primary_email)
  rescue => ex
    ExceptionNotifier.notify_exception ex, data: self.inspect
  ensure
    audit "message.deliver #{raw_message.present?}"
    raw_message
  end

  def _send_sms
    success = nil
    if !template
      SystemMailer.warning("No template for task name:#{label} id:#{id}")
    elsif !recipient.present?
      SystemMailer.warning("No mobile phone with carrier for task name:#{label} id:#{id}")
    else
      params=message_params(:sms)
      params[:from_addr]= self.task_type.try(:name) =~ /agent/ ? APP_CONFIG['email']['sender'] : APP_CONFIG['email']['consumer_facing_sender']
      self.message = Marketing::MessageMedia::Message.new( params )
      success = message.deliver
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex, data: self.inspect
  ensure
    audit "delivered? #{!!success}"
    success
  end

  # Updates sequenceable.staff_assignment if this task has been reassigned
  def update_sequenceable_staff_assignment
    if assigned_to_id_changed? && sequenceable
      sequenceable.staff_assignment || sequenceable.build_staff_assignment
      sequenceable.staff_assignment.set_user_id_for_role_id role_id, assigned_to_id
      sequenceable.staff_assignment.save
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
  end

  # @return boolean indicating whether this TaskBuilder builds a task that should send an email
  def email?
    !!task_type.try(:name).try(:match, /email/)
  end

  def brand
    person.respond_to?(:brand) ? person.brand : person.try(:default_brand)
  end

end
