class Crm::Physician < ActiveRecord::Base
  belongs_to :consumer, class_name: 'Consumer', foreign_key: :consumer_id
end
