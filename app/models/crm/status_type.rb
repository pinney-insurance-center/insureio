class Crm::StatusType < ActiveRecord::Base
  include OwnedStereotype
  
  

  belongs_to_enum :sales_stage,          class_name: "Enum::SalesStage"
  belongs_to_enum :status_type_category, class_name: "Enum::StatusTypeCategory"

  has_many        :task_builders,        class_name: "Crm::TaskBuilder", as: :owner, dependent: :destroy

  alias_attribute :category, :status_type_category
  alias_attribute :category_id, :status_type_category_id

  after_initialize ->{ self.ownership_id||=Enum::Ownership::user_and_descendants_id }#default to user and descendants
  before_create    :set_initial_sort_order

  delegate :name, to: :category, prefix: true

  #Note that all these columns are ascending.
  #Those with `-` and `DESC` are using absolute value
  #to ensure that nil (equivalent to zero) goes to the end.
  scope :sorted, lambda { order('-sales_stage_id DESC, -status_type_category_id DESC, sort_order ASC, id ASC') }
  scope :excluding_global, lambda {
    where(ownership_id:[Enum::Ownership.id('user'),Enum::Ownership.id('user and descendants')]).
    where('owner_id IS NOT NULL')
  }

  def set_initial_sort_order
    sort_order_of_last_in_cat=Crm::StatusType
      .where(status_type_category_id:self.status_type_category_id)
      .order('sort_order DESC').limit(1).pluck(:sort_order)[0].to_i
    self.sort_order=sort_order_of_last_in_cat+1
  end

  #This function allows the sort order column to be changed
  #while assuring all sort order values are unique
  #for the records in a specific category.
  def adjust_sort_order direction_operator
    if direction_operator!='+' && direction_operator!='-'
      errors.add(:base,"cannot adjust the sort order without a direction operator in the form of a '+' or a '-'")
      return false
    end
    ids_in_category=self.class
      .where(status_type_category_id:self.status_type_category_id)
      .sorted.pluck(:id)

    already_last=direction_operator=='+' && self.id==ids_in_category.last
    already_first=direction_operator=='-' && self.id==ids_in_category.first
    if already_last || already_first
      #These terms are based on the assumption that status types are displayed by ascending sort order,
      #as specified in scope `sorted`.
      #So to increase sort order is to move down the user's screen.
      direction_term=direction_operator=='+' ? 'down' : 'up'
      direction_end_term=direction_operator=='+' ? 'bottom' : 'top'
      errors.add(:base,"Cannot move further #{direction_term} from the #{direction_end_term} of the list")
      return false
    end

    current_index=ids_in_category.index(self.id)
    new_index=current_index.send(direction_operator,1)
    ids_in_category.insert(new_index, ids_in_category.delete_at(current_index))

    update_query="UPDATE crm_status_types SET sort_order=CASE id\n"
    ids_in_category.each_with_index do |id,index|
      update_query+="WHEN #{id} THEN #{index}\n"
    end
    update_query+="\nEND"
    update_query+="\nWHERE status_type_category_id=#{self.status_type_category_id}"
    ActiveRecord::Base.connection.execute(update_query)
    true
  end

  #These notes are for the purpose of self-documentation within our code as well as
  #for the benefit of users who need to manage status types.
  #In future, all code references to a specific status type should refer to a constant defined at the end of this model,
  #and they should have a note defined here.
  #The status types index should also make sure to display these.
  def note_to_users
    preset_type_entry=PRESETS.values.select{|st| st[:id].present? && st[:id]==self.id }.first
    if preset_type_entry.present?
      return preset_type_entry[:note]
    end
    nil
  end

  class Preset < Struct.new(:id, :note); end

  PRESETS={
    marketech_app_sent_esign: {id:155, note:"This status is used to determine whether a Marketech case is ready to send to the client and whether it is still editable within Marketech."},
    prospect:                 {id:266, note:"This is the fallback initial status for cases whose agent and brand both lack an initial status."},
    submit_to_app_team:       {id:307, note:"This status assigns tasks to our internal Application Team, who will complete the case.\n"+
                                            "It is used by our quoter when the full application option has been selected.\n"+
                                            "A policy must meet certain criteria before you will be allowed to assign this status to it."},
    submit_to_app_team2:      {id:646, note:"This status assigns tasks to our internal Application Team, who will complete the case.\n"+
                                            "A policy must meet certain criteria before you will be allowed to assign this status to it."},
    consumer_link:            {id:515, note:"This status indicates that the case came in through the quote path via an IO Marketing Link."},
    ezl_withdrawn:            {id:530, note:"(For EZLife Users) EZLife dashboards exclude cases in this status. The delete icon in the EZLife dashboard assigns this status."},
    referral:                 {id:545, note:"This status is used by our quoter when the referral option has been selected. Only admin users may set this status manually."},
    saved_quote:              {id:752, note:"This status is used by our quoter when the user decides to save one or more quotes seen on the 'Compare' page."},
    in_force_no_follow_ups:   {id:353, note:"This status is automatically applied to preexisting policies entered via the quote path or via a consumer's policies section. It may also be used elsewhere."},
    replaced:                 {id:785, note:"This status indicates a policy which has been replaced by a newer in force policy."},

    nmb_app_out: Preset.new(966),

    rtt_app_started:              {id:706, note:"This is the initial status for the RTT quoting process. It applies to applications which have not yet been signed by the insured."},
    rtt_app_quoted:               {id:692, note:"This status is automatically applied to RTT applications where an initial quote has been chosen, but signing has not yet taken place."},
    rtt_pending_signatures:       {id:707, note:"This status is automatically applied to RTT applications which have been signed by the insured but still need a signature from a separate owner and/or payer."},
    rtt_signer_declined:          {id:709, note:"This status is automatically triggered when an owner or payer of an RTT application declines to sign."},
    rtt_scor_submitted:           {id:695, note:"This status is automatically triggered when an RTT application has been sent for underwriting and has not yet received a decision."},
    rtt_scor_timeout:             {id:693, note:"This status is automatically triggered when an RTT application receives an underwriting response of timed out, "+
                                                "usually meaning the request was made outside SCOR/Velogica operating hours and an automated decision cannot be rendered."},
    rtt_scor_underwriter_review:  {id:696, note:"This status is automatically triggered when an RTT application receives an underwriting response indicating that it requires additional review by an underwriter."},
    rtt_scor_auto_declined:       {id:954, note:"This status is automatically triggered when an RTT application is auto-declined/disqualified without underwriting due to answers given for recent insurance apps."},
    rtt_scor_declined:            {id:694, note:"This status is automatically triggered when an RTT application receives an underwriting decision of Declined."},
    rtt_scor_approved_as_applied: {id:704, note:"This status is automatically triggered when an RTT application receives an underwriting decision of Approved with the expected rate."},
    rtt_scor_approved_ot_applied: {id:705, note:"This status is automatically triggered when an RTT application receives an underwriting decision of Approved but with a different rate than expected."},
    rtt_offer_not_taken:          {id:697, note:"This status is automatically triggered when an RTT application has received an underwriting decision but the owner did not click 'Buy' in the allotted time."},
    rtt_in_force:                 {id:698, note:"This status is automatically triggered when an RTT application has received an underwriting decision and the owner has clicked 'Buy'."},

    aw_awaiting_requirements:        {id:nil, note:"(Placeholder. We do not have a status for this yet.)"},
    aw_issued_del_req:               {id:135, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_app_submitted:                {id:158, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_approved:                     {id:129, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_approved_other_than_applied:  {id:133, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_not_taken:                    {id:144, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_reissued_case:                {id:162, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_pending_decision:             {id:717, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_in_force:                     {id:184, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_holding:                      {id:194, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_incomplete_closed:            {id:682, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_await_funds_1035_exchange:    {id:nil, note:"(Placeholder. We do not have a status for this yet.)"},
    aw_postponed:                    {id:139, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_withdrawn:                    {id:682, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_inquiry_received:             {id:nil, note:"(Placeholder. We do not have a status for this yet.)"},
    aw_declined:                     {id:141, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_in_force_with_requirements:   {id:184, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
    aw_tentative_offer:              {id:191, note:"Updates from AgencyWorks/AgencyIntegrator may automatically change a case into this status."},
  }.with_indifferent_access.freeze

  #DEPRECATED. Use the hash in future.
  #For existing code in other models,
  #this will set a capitalized constant for each status type, holding the relevant id.
  PRESETS.each do |k,v|
    next if k.to_s.match(/^rtt_/)
    self.const_set("#{k.upcase}_STATUS_TYPE_ID",v[:id])
  end

  RTT_SCOR_DECISION_STATUS_TYPE_IDS=[693,696,694,704,705].freeze
end
