class Crm::ConsumerRelationship < ActiveRecord::Base
  #This model describes a relationship between at least two consumers,
  #and optionally also a policy,
  #owned by the primary_insured consumer,
  #for which the stakeholder consumer acts as some combination of owner/payer/bene.
  #When choosing primary insured's primary contact, they will have to choose one for which there is a relationship record.

  belongs_to      :primary_insured,     class_name: "Consumer",      inverse_of: :relationships_w_self_as_primary
  belongs_to      :stakeholder,         class_name: "Consumer",      inverse_of: :relationships_w_self_as_related
  belongs_to      :crm_case,            class_name: "Crm::Case",     inverse_of: :stakeholder_relationships

  belongs_to_enum :relationship_type,   class_name: "Enum::RelationshipType"

  counter_culture :primary_insured, column_name: :relationships_w_self_as_primary_count
  counter_culture :stakeholder,     column_name: :relationships_w_self_as_related_count

  accepts_nested_attributes_for         :stakeholder

  before_validation ->{ self.primary_insured=crm_case.consumer }, if: ->{ crm_case.present? && primary_insured.blank? }

  validates :primary_insured,           presence:true
  validates :stakeholder,               presence:true
  validates :relationship_type,         presence:true
  validates :stakeholder_id,            uniqueness:{scope: :crm_case_id}, if: ->{crm_case.present? && crm_case.try(:persisted?) && stakeholder.try(:persisted?)}
  validates :percentage,                presence:true, numericality:{greater_than: 0,less_than_or_equal_to:100}, if: :is_beneficiary
  validate  :both_or_neither_of_policy_and_stake_are_defined
  validate  :stakeholder_and_pi_not_in_conflict, if: ->{crm_case.try(:persisted?) && stakeholder.try(:persisted?)}

  scope :to, lambda{|c|
    where(primary_insured_id: c.id)
  }
  scope :from, lambda{|c|
    where(stakeholder_id: c.id)
  }
  scope :to_or_from, lambda{|c|
    where('primary_insured_id=? OR stakeholder_id=?', c.id, c.id)
  }
  scope :between, lambda{|a,b|
    where('(primary_insured_id=? AND stakeholder_id=?) OR (primary_insured_id=? AND stakeholder_id=?)', a.id, b.id, b.id, a.id)
  }
  scope :of_type, lambda{|type_str_or_array|
    #This scope looks for exact matches only,
    #because there are relationships whose names are similar.
    #ie.[ 'child','grand child','step child'] or ['spouse/domestic partner','ex-spouse']
    where(relationship_type_id: Enum::RelationshipType.ewhere(name:type_str_or_array).map(&:id) )
  }

  #This is meant to simplify CSV import,
  #by allowing column names like "case1.stakeholder_relationship1.full_name".
  (Consumer.columns.map(&:name)+[:ssn,:primary_phone,:primary_email,:primary_address]).uniq.each do |f|
    next if Crm::ConsumerRelationship.columns.map(&:name).include?(f)
    self.delegate f, to: :stakeholder, allow_nil:true
    self.delegate "#{f}=", to: :stakeholder, allow_nil:true
  end

  def primary; !contingent; end

  def primary= value
    self.contingent = !value
  end

  private

  def inherit_brand_and_agent_from_pi
    return unless stakeholder && stakeholder.new_record?
    stakeholder.brand_id||=self.primary_insured.brand_id
    stakeholder.agent_id||=self.primary_insured.agent_id
  end

  def stake_is_defined
    is_owner || is_payer || is_beneficiary
  end

  def both_or_neither_of_policy_and_stake_are_defined
    if stake_is_defined && !crm_case
      errors.add(:base,'policy id must be provided in order to define a stake/role in that policy')
    elsif crm_case && !stake_is_defined
      errors.add(:base,'stake/role must be clearly defined as some combination of owner/payer/beneficiary')
    else
      return true
    end
        
  end

  def stakeholder_and_pi_not_in_conflict
    return if stakeholder_id!=primary_insured_id

    if !self.crm_case_id
      errors.add(:base,'you cannot designate a consumer as related to themself.')
    else
      errors.add(:base,'naming the primary insured as the policy owner is redundant. Only specify owner if it is someone else.') if is_owner
      errors.add(:base,'naming the primary insured as the premium payer is redundant. Only specify payer if it is someone else.') if is_payer

      is_life_ins_policy=!crm_case.product_type || crm_case.product_type.product_cat_id==Enum::ProductTypeCat.id('Life Insurance')
      errors.add(:base,'a consumer cannot be the beneficiary to their own life insurance!') if is_beneficiary && is_life_ins_policy
    end
  end

end