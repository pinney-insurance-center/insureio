class Crm::LeadType < ActiveRecord::Base
  
  has_and_belongs_to_many :brands, class_name:'Brand', join_table:'crm_lead_types_brands'
end
