class Crm::Source < ActiveRecord::Base
  

  has_and_belongs_to_many :users, class_name:'User', join_table:'crm_sources_users'

  scope :viewable, lambda{ |user|
    joins(:users).where('users.id = ?', user.id)
  }

  def editable?(user)
    false
  end

  def viewable?(user)
    return true if user.super?
    users.where('users.id = ?', user.id).limit(1).count > 0
  end
end
