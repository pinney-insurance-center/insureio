# These methods are shared between Connection and Case because
# each of these models has an :agent and a :staff_assignment.
module Crm::Accessible
  extend ActiveSupport::Concern

  def editable? user_or_id
    _accessible? user_or_id, :edit
  end
  
  def viewable? user_or_id
    _accessible? user_or_id, :view
  end

  included do
    scope :editable_by, lambda{|user_or_id|
      Crm::AccessScopeBuilder.new(self, :edit, user_or_id).to_relation
    }
    scope :viewable_by, lambda{|user_or_id|
      Crm::AccessScopeBuilder.new(self, :view, user_or_id).to_relation
    }

    #DEPRECATED. USE `editable_by` or `viewable_by`.
    scope :editables, lambda{|user_or_id| editable_by(user_or_id) }
    scope :viewables, lambda{|user_or_id| viewable_by(user_or_id) }
  end

  private

  # Access is determined in part by the permissions of the User.
  #
  # A user with super view/edit can access anything.
  #
  # An agent can access a case/consumer they own or which is owned by 
  # a family member, if the agent has permission to access that family
  # member's resources.
  #
  # A sales support user can access a case/consumer whose agent's 
  # support staff includes the user or a case/connection which has a
  # status which has a task which is assigned to the user.
  #
  # A manager can access a case/consumer whose agent is a sibling.
  def _accessible? user_or_id, edit_or_view
    # get user id
    user_id = user_or_id.to_i
    # quick response if user is self.agent
    return true if self.agent_id == user_id
    return true if self.respond_to?(:agent_of_record_id) && self.agent_id == user_id
    # get user object
    user = user_or_id.is_a?(Fixnum) ? User.find(user_or_id) : user_or_id
    return false unless user
    if user.super? || (edit_or_view==:view && user.super_view)
      return true
    elsif _accessible_through_family? user, edit_or_view
      return true
    elsif user.can?(:sales_support)
      return true if _accessible_for_sales_support?(user)
    elsif respond_to?(:brand) and brand
      return true if brand.owner_id == user_id or (user.can?(:view_brand_resources) and brand.members.where(id:user_id).limit(1).count > 0)
    end
    false
  end

  def _accessible_for_sales_support? user
    # succeeds if agent has user in staff assignment
    return true if self.try(:agent).try(:staff_assignment).try('include?', user)
    # succeeds if this person has/had a task assigned to given user
    person= (self.is_a?(Crm::Case) || self.is_a?(Quoting::Quote)) ? self.consumer : self
    Task.where(person_id:person.try(:id), person_type: person.try(:class).try(:name), assigned_to_id:user.id).limit(1).count > 0
  end

  # Returns true if the user may access the resource through a sibling, nephew, or descendant
  def _accessible_through_family? user, edit_or_view
    descendant_access, sibling_access, nephew_access =
    if edit_or_view == :edit
      [true, user.can?(:edit_siblings_resources), user.can?(:edit_nephews_resources)]
    else
      [true, user.can?(:view_siblings_resources), user.can?(:view_nephews_resources)]
    end
    # check permission to access siblings' stuff
    return true if sibling_access and self.agent.try(:parent_id) == user.parent_id
    return true if sibling_access and self.respond_to?(:agent_of_record_id) && self.agent_of_record.parent_id == user.parent_id
    # check permission to access descendants' stuff
    return true if descendant_access and self.agent.try(:descendant?,user)
    return true if descendant_access and self.respond_to?(:agent_of_record) && self.agent_of_record.try(:descendant?,user)
    # check permission to access nephews' stuff
    return true if nephew_access and self.agent.try(:nephew?,user)
    return true if nephew_access and self.respond_to?(:agent_of_record) && self.agent_of_record.try(:nephew?,user)
    # default return
    false
  end
end

class Crm::AccessScopeBuilder

  def initialize klass, mode, user_or_id
    @user  = user_or_id.is_a?(User) ? user_or_id : User.find(user_or_id)
    @mode  = mode
    @klass = klass
    @joins = []
    @arel  = nil
  end

  def to_relation
    if super? || @user.can?(:sales_support)
      @klass.where(nil)
    else
      # Scope on :brand_id
      if view? && @user.can?(:view_brand_resources)
        brand_ids=@user.owned_brand_ids+@user.brand_ids
        if brand_ids.present?
          arel_or Arel::Table.new(Consumer.table_name)[:brand_id].in(brand_ids)
          joins(:consumer) if @klass != Consumer
        end
      end
      # Scope on :agent_id
      whose_to_load = edit? ? User.whose_resources_editable_by(@user) : User.whose_resources_viewable_by(@user)
      arel_klass = @klass.columns_hash.has_key?('agent_id') ? @klass : Consumer
      arel_or arel_klass.arel_table[:agent_id].in(whose_to_load.pluck :id)
      # Return relation
      @klass.joins(@joins).where(@arel)
    end
  end

private

  ###############
  # SCOPE MODIFIERS (GENERIC)
  ###############

  def arel_or arel
    @arel = @arel ? @arel.or(arel) : arel
  end

  def joins join_value
    @joins << join_value
  end

  ###############
  # USER PERMISSION GETTERS
  ###############

  def super?
    @user.can?(:super_edit) || (@user.can?(:super_view) && view?)
  end

  # Returns whether @user can access descendants' rescources
  def descendant?
    @user.can?(view? ? :view_descendants_resources : :edit_descendants_resources)
  end

  # Returns whether @user can access siblings' rescources
  def sibling?
    @user.can?(view? ? :view_siblings_resources : :edit_siblings_resources)
  end

  # Returns whether @user can access siblings' rescources
  def nephew?
    @user.can?(view? ? :view_nephews_resources : :edit_nephews_resources)
  end

  ###############
  # ACCESS GETTERS
  ###############

  def view?
    @mode == :view
  end

  def edit?
    @mode == :edit
  end

end