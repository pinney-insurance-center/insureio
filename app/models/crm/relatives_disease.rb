class Crm::RelativesDisease < ActiveRecord::Base
  self.table_name ='quoting_relatives_diseases'

  # Associations
  belongs_to :health_info, class_name:"Crm::HealthInfo", foreign_key:"health_info_id"
end
