class Crm::HealthInfo < ActiveRecord::Base
  class Coder
    def initialize klass
      @klass = klass
    end

    def build_array objects
      Array(objects).map do |obj|
        if obj.is_a? @klass
          obj
        elsif obj.is_a? Hash
          @klass.new obj
        end
      end
    end

    def dump items
      Array(items.select(&:truthy?)).presence&.to_json # Use `try` to avoid putting 'null' in db
    end

    def load json
      build_array JSON.load(json)
    end
  end
end
