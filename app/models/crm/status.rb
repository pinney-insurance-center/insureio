class Crm::Status < ActiveRecord::Base

  

  # Associations
  belongs_to_enum :sales_stage,          class_name: "Enum::SalesStage"
  belongs_to_enum :status_type_category, class_name: "Enum::StatusTypeCategory"

  belongs_to      :statusable,    polymorphic:true
  belongs_to      :creator,       class_name: "User",            foreign_key: :created_by_id
  belongs_to      :status_type,   class_name: "Crm::StatusType", foreign_key: :status_type_id

  has_many        :tasks,         class_name: "Task",       as: :origin

  has_one         :next_task,     ->{ where(active:true, suspended:false).order('due_at ASC') }, class_name: "Task",       as: :origin

  belongs_to      :creator_name_only, ->{ select([:id,:full_name]) },   class_name: "User",    foreign_key: :created_by_id

  alias_attribute :name, :status_type_name

  validates       :status_type, presence:true
  validates       :statusable,  presence:true

  after_validation ->{ populate_self_from_type; update_statusable_from_self }, on: :create

  after_commit   :create_tasks, on: :create

  delegate       :task_builders, to: :status_type
  delegate       :color,     to: :status_type, allow_nil: true

  delegate       :editable?, to: :statusable,  allow_nil:true #this doesn't seem to work. not sure how to approach this.
  delegate       :viewable?, to: :statusable,  allow_nil:true

  delegate       :name,      to: :sales_stage,          prefix:true, allow_nil:true
  delegate       :name,      to: :status_type_category, prefix:true, allow_nil:true

  def date_set
    self.created_at.strftime('%m/%d/%Y %H:%M:%S')
  end

  #Take advantage of database index on contact id and name.
  def created_by_name
    if association(:creator).loaded?
      creator&.full_name
    elsif created_by_id#don't try to access name unless association exists.
      creator_name_only&.full_name
    end
  end

  def create_tasks
    audit log:'status'
    task_builders.each do |builder|
      is_for_opp_or_policy=(statusable.is_a?(Crm::Case) || statusable.is_a?(Quoting::Quote))
      builder_is_for_another_brand=builder.brand_id.present? && builder.brand_id != self.person.try(:brand_id)
      next if is_for_opp_or_policy && builder_is_for_another_brand
      begin
        task = builder.build origin_id:id, origin_type:'Crm::Status', person: person, sequenceable_id: statusable_id, sequenceable_type:statusable_type
        task.save
      rescue => ex
        ExceptionNotifier.notify_exception(ex)
      end
    end
  end

  def person
    if statusable.is_a? Crm::Case
      statusable.consumer || statusable.current_details&.consumer
    elsif statusable.is_a? Quoting::Quote
      statusable.consumer || statusable.crm_case&.consumer
    else
      statusable
    end
  end

private

  def populate_self_from_type
    self.status_type_name        = status_type.name
    self.sales_stage_id          = status_type.sales_stage_id
    self.indicates_closed        = status_type.indicates_closed
    self.status_type_category_id = status_type.status_type_category_id
    self.created_by_id           = Thread.current[:current_user_id]
    true
  end

  #This method handles denormalized fields and deactivating old statuses/tasks.
  def update_statusable_from_self
    attrs_to_assign={}
    if [Crm::Case, Quoting::Quote].include?(statusable.class)
      attrs_to_assign[:sales_stage_id]=[1,self.sales_stage_id.to_i,statusable.sales_stage_id].max
      attrs_to_assign[:status_type_category_id]=self.status_type_category_id
      attrs_to_assign[:active]=!self.indicates_closed if statusable.is_a?(Crm::Case)
      attrs_to_assign[:closed]= self.indicates_closed if statusable.is_a?(Quoting::Quote)
      #Only override the current state of these flags in order to set to true.
      #Never change from true back to false.
      attrs_to_assign[:is_duplicate]= true if status_type.indicates_duplicate || statusable.is_duplicate
      attrs_to_assign[:is_test]     = true if status_type.indicates_test      || statusable.is_test

      current_time=Time.now
      Enum::SalesStage.all.each do |ss|
        next if ss.id==1
        timestamp_column_name='entered_stage_'+ss.name.to_s.downcase.gsub(/\s/,'_')
        if self.sales_stage_id.to_i>=ss.id && statusable.respond_to?(timestamp_column_name) && statusable.send(timestamp_column_name).nil?
          attrs_to_assign["#{timestamp_column_name}"]= current_time
        end
      end
    end
    attrs_to_assign.delete_if{|k,v| statusable[k]==v }

    if statusable.persisted?
      #Deactivate old statuses and tasks without instantiation.
      statusable.statuses.where(active:true).update_all(active:false)
      statusable.tasks.where(active:true, evergreen:false).where('origin_id IS NOT NULL').update_all(active:false)
    end

    return true if attrs_to_assign.empty?
    if statusable.persisted?
      #Update the statusable object in the database.
      statusable.class.where(id:statusable.id).update_all(attrs_to_assign)
    end
    #Update the statusable object in memory.
    #This is necessary even if the object is persisted,
    #because its callbacks rely on having up to date data in memory.
    statusable.assign_attributes(attrs_to_assign)
    true
  end

  #This constant is used by both the case and opportunity/quote models.
  #Eventually all shared logic between those models should be moved to a module.
  REQUIREMENTS_FOR_APP_FULFILLMENT=[
    ['consumer','full_name'],
    ['consumer','ssn'],
    ['consumer','dln'],
    ['consumer','dl_state_id'],
    ['consumer','birth_country'],
    ['consumer','primary_address','street'],
    ['consumer','primary_address','city'],
    ['consumer','primary_address','state'],
    ['consumer','primary_address','zip'],
    ['consumer','phones'],
    ['consumer','emails'],
    ['consumer','preferred_contact_method_id'],
    ['consumer','financial_info','annual_income'],
    ['consumer','financial_info','net_worth_specified'],
    ['consumer','relationship_to_agent'],
    ['consumer','years_of_agent_relationship'],
    ['beneficiaries'],
    ['primary_beneficiary_percentages_must_total_to_100'],
    ['contingent_beneficiary_percentages_must_total_to_100'],
    ['temporary_insurance_must_have_answer'],
  ].freeze

end
