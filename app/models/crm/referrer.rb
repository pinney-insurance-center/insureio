class Crm::Referrer < ActiveRecord::Base
  
  has_and_belongs_to_many :brands, class_name:'Brand', join_table:'crm_referrers_brands'
end
