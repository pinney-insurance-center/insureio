class Crm::HealthInfo < ActiveRecord::Base

  has_one :consumer, class_name: "Consumer", inverse_of: :health_info

  validate :health_condition_detail_fields_are_valid

  def crimes= value
    will_change 'crimes' do
      self[:crimes] = Coder.new(Crime).build_array(value)
    end
  end

  def diseases= value
    will_change 'diseases' do
      if value.is_a? Array
        value = value.map { |d| Health::Disease::Coder.build_disease(d) }
        self[:diseases] = Health::Disease::DiseaseSet.new value
      elsif value.is_a? Hash
        self[:diseases] = Health::Disease::DiseaseSet.from_hash(value)
      else
        raise ArgumentError "expected Hash or Array; got #{value.class.name}"
      end
    end
  end

  def relatives_diseases= value
    will_change 'relatives_diseases' do
      self[:relatives_diseases] = Health::Disease::DiseaseArray.new(value)
    end
  end

  def moving_violations= value
    will_change 'moving_violations' do
      self[:moving_violations] = Coder.new(MovingViolation).build_array(value)
    end
  end

  class Tobacco < Health::Detail
    attr :cigarettes_current, :boolean
    attr :cigarettes_per_day, :integer
    attr :cigarette_last, :date
    attr :cigars_current, :boolean
    attr :cigars_per_month, :integer
    attr :cigar_last, :date
    attr :nicotine_patch_or_gum_last, :date
    attr :nicotine_patch_or_gum_current, :boolean
    attr :pipe_current, :boolean
    attr :pipe_last, :date
    attr :pipes_per_year, :integer
    attr :chewed_last, :date
    attr :chewed_current, :boolean

    def last
      [:cigarette_last, :cigar_last, :nicotine_patch_or_gum_last, :pipe_last, :chewed_last]
      .map { |field| send field }.compact.max
    end

    def products
      set = Set.new
      set.add(:cigarettes) unless [cigarettes_current, cigarettes_per_day, cigarette_last].all?(&:nil?)
      set.add(:cigars) unless [cigars_current, cigars_per_month, cigar_last].all?(&:nil?)
      set.add(:nicotine_patch_or_gum) unless [nicotine_patch_or_gum_last, nicotine_patch_or_gum_current].all?(&:nil?)
      set.add(:pipe) unless [pipe_current, pipe_last, pipes_per_year].all?(&:nil?)
      set.add(:chewed) unless [chewed_last, chewed_current].all?(&:nil?)
      set.to_a
    end
  end

  class Bp < Health::Detail
    include Health::Treatable

    attr :systolic, :integer
    attr :diastolic, :integer
    attr :control_start, :date
  end

  class Cholesterol < Health::Detail
    include Health::Treatable

    attr :date, :date, "Date of diagnosis"
    attr :level, :integer
    attr :hdl, :float
    attr :control_start, :date
  end

  class Crime < Health::Detail
    include Health::ArrayDetail

    attr :pending, :boolean, 'Is this a pending charge (as opposed to a conviction or aquittal)?'
    attr :felony, :boolean, 'Did this result in a felony conviction?'
    attr :probation_end, :date, 'If this resulted in probation, when is/was the scheduled end of your probation? (An approximate date is sufficient.)'
    attr :date, :date, 'On what date did the crime or alleged crime take place? (An approximate date is sufficient.)'
    attr :true, :boolean, "Optional. If no other fields are set on this Object, then this flag can be set to indicate that the present health concern exists even though no further detail has been provided."
  end

  class ForeignTravel < Health::Detail
    attr :when, :string
    attr :duration, :string
    attr :country_id, :integer #correlates with accessor `foreign_travel_country` on health inf
    attr :country_detail, :string
  end

  class HazardousAvocation < Health::Detail
    attr :aeronautics, :boolean
    attr :aeronautics_hang_gliding, :boolean
    attr :aeronautics_sky_diving, :boolean
    attr :aeronautics_parachuting, :boolean
    attr :aeronautics_ballooning, :boolean
    attr :aeronautics_other, :boolean
    attr :military, :boolean
    attr :racing, :boolean
    attr :racing_car, :boolean
    attr :racing_motorcycle, :boolean
    attr :racing_boat, :boolean
    attr :racing_other, :boolean
    attr :scuba_skin_diving, :boolean
    attr :scuba_skin_diving_lte_75_ft, :boolean
    attr :scuba_skin_diving_gt_75_ft, :boolean
    attr :climbing_hiking, :boolean
    attr :climbing_hiking_trail, :boolean
    attr :climbing_hiking_mountain, :boolean
    attr :climbing_hiking_rock, :boolean
    attr :flying, :boolean
    attr :flying_non_pilot_crew, :boolean
    attr :flying_student_pilot, :boolean
    attr :flying_private_pilot, :boolean
    attr :flying_private_pilot_lt_50_h, :boolean
    attr :flying_private_pilot_50_to_250, :boolean
    attr :flying_private_pilot_gt_250_h, :boolean
    attr :flying_private_pilot_lt_100_solo_h, :boolean
    attr :flying_commercial_pilot, :boolean
    attr :flying_corporate_pilot, :boolean
    attr :flying_military_pilot, :boolean
    attr :flying_test_pilot, :boolean
    attr :flying_flight_instructor, :boolean
    attr :flying_other, :boolean

    def initialize attrs_or_json = nil
      super
      # Handle deprecated field :further_detail
      if attrs_or_json
        attrs_or_json = JSON.parse(attrs_or_json) if attrs_or_json.is_a? String
        self.detail ||= attrs_or_json.with_indifferent_access.delete(:further_detail)
      end
    end
  end

  class AlcoholAbuse < Health::Disease
    attr :currently_consuming, :boolean
    attr :last_consumed_date, :date
    attr :ever_treated, :boolean
    attr :ever_relapse, :boolean
    attr :last_relapse_date, :date
  end

  class Anxiety < Health::Disease
    attr :condition_degree, :string
    attr :ever_hospitalized, :boolean
    attr :last_hospitalized_date, :date
    attr :episodes_year, :integer
    attr :medication_count, :integer
    attr :outpatient_care, :boolean
    attr :inpatient_care, :boolean
    attr :work_absence, :boolean
    attr :absence_count, :integer
    attr :ever_suicide_attempt, :boolean
    attr :last_suicide_attempt_date, :date
  end

  class Arthritis < Health::Disease
    attr :condition_degree, :string
    attr :ever_suppressants, :boolean
    attr :current_suppressants, :boolean
    attr :ever_prednisone, :boolean
    attr :current_prednisone, :boolean
    attr :ever_immunosuppressants, :boolean
    attr :currently_immunosuppressants, :boolean
    attr :good_treatment_response, :boolean
    attr :complications, :boolean
    attr :ever_surgery, :date
    attr :disabled, :boolean
  end

  class Asthma < Health::Disease
    attr :condition_degree, :string
    attr :ever_treated, :boolean
    attr :inhaled_bronchodilators, :boolean
    attr :inhaled_corticosteroids, :boolean
    attr :oral_medication_no_steroids, :boolean
    attr :oral_medication_steroids, :boolean
    attr :rescue_inhaler, :boolean
    attr :ever_hospitalized, :boolean
    attr :hospitalized_count_past_year, :integer
    attr :hospitalized_count, :integer
    attr :last_hospitalized_date, :date
    attr :episodes_year, :integer
    attr :work_absence, :boolean
    attr :absence_count, :integer
    attr :fev1, :float
    attr :ever_life_threatening, :boolean
    attr :last_life_threatening_date, :date
    attr :is_treatment_compliant, :boolean
  end

  class AtrialFibrillation < Health::Disease
    attr :condition_degree, :string
    attr :last_episode_date, :date
    attr :episode_length, :integer
    attr :self_resolve, :boolean
    attr :ever_cardiac_eval, :boolean
    attr :cardiac_eval_date, :date
    attr :cardiac_eval_result, :boolean
    attr :heart_disease, :boolean
    attr :shortness_of_breath, :boolean
    attr :current_medications, :boolean
    attr :ablation_procedure, :boolean
    attr :ablation_date, :date
    attr :ablation_result, :boolean
    attr :is_controlled, :boolean
    attr :fibrillation_type, :string

    FIB_TYPES=[:chronic,:paroxysmal]
  end

  class BreastCancer < Health::Disease
    attr :cancer_stage, :integer
    attr :condition_degree, :string
    attr :node_count, :integer
    attr :metastatis, :boolean
    attr :tumorsize, :integer
    attr :reoccurrence, :boolean
    attr :last_mammogram_date, :date
    attr :ductal_carcinoma, :boolean
    attr :lobular_carcinoma, :boolean
    attr :diagnosed_dcis, :boolean
    attr :dcis_removed, :boolean
    attr :number_of_lesions, :integer
    attr :lesion_size, :integer
    attr :comedonecrosis, :boolean
    attr :diagnosed_lcis, :boolean
    attr :lumpectomy, :boolean
    attr :mastectomy, :boolean
    attr :single_mastectomy, :boolean
    attr :double_mastectomy, :boolean
    attr :negative_sentinel_lymph_exam, :boolean
    attr :radiation_treatment, :boolean
    attr :endocrine_therapy, :boolean
    attr :endocrine_treated, :boolean
    attr :cancer_grade, :integer
  end

  class Copd < Health::Disease
    attr :condition_degree, :string
    attr :fev1, :integer
    attr :has_symptoms, :boolean
    attr :copd_severity, :string
  end

  class Crohns < Health::Disease
    attr :condition_degree, :string
    attr :stabilization_date, :date
    attr :last_attack_date, :date
    attr :ever_steroid, :boolean
    attr :currently_steroids, :boolean
    attr :steroid_stop_date, :date
    attr :ever_immuno_suppressants, :boolean
    attr :current_immuno_suppressants, :boolean
    attr :immunosuppressants_stop_date, :date
    attr :limited_to_colon, :boolean
    attr :complications, :boolean
    attr :surgery, :boolean
    attr :surgery_date, :date
    attr :weight_stable, :boolean
  end

  class Depression < Health::Disease
    attr :condition_degree, :string
    attr :ever_hospitalized, :boolean
    attr :last_hospitalized, :date
    attr :medication_count, :integer
    attr :in_psychotherapy, :boolean
    attr :responding_well, :boolean
    attr :treatment_completed, :boolean
    attr :work_absence, :boolean
    attr :absence_count, :integer
    attr :ever_suicide_attempt, :boolean
    attr :last_suicide_attempt_date, :date
  end

  class Diabetes1 < Health::Disease
    attr :last_a1_c, :float
    attr :average_a1_c, :float
    attr :complications, :boolean
    attr :is_gestational, :boolean
    attr :insulin_units, :integer
    attr :type, :string

    def xrae_blacklisted_keys
      [:type]
    end
  end

  class Diabetes2 < Health::Disease
    attr :last_a1_c, :float
    attr :average_a1_c, :float
    attr :complications, :boolean
    attr :is_gestational, :boolean
    attr :insulin_units, :integer
    attr :type, :string

    def xrae_blacklisted_keys
      [:type]
    end
  end

  class DrugAbuse < Health::Disease
    attr :currently_using, :boolean
    attr :last_used_date, :date
    attr :ever_treated, :boolean
    attr :ever_relapse, :boolean
    attr :last_relapse_date, :date
    attr :ever_convicted, :boolean
  end

  class Epilepsy < Health::Disease
    attr :condition_degree, :string
    attr :ever_treated, :boolean
    attr :ever_surgery, :boolean
    attr :surgery_date, :date
    attr :controlled_seizures, :boolean
    attr :neurological_evaluation, :boolean
    attr :neurological_normal, :boolean
    attr :caused_by_other, :boolean
    attr :last_seizure_date, :date
    attr :seizures_per_year, :integer
    attr :thirty_minute_plus, :integer
    attr :ever_medication, :boolean
    attr :seizure_type, :string

    EPILEPSY_TYPES=[
      :atonic,
      :reflex,
      :febrile,
      :myoclonic,
      :grand_mal,
      :petit_mal,
      :simple_partial,
      :complex_partial,
      :nocturnal,
    ]
  end

  class HeartMurmur < Health::Disease
    attr :condition_degree, :string
    attr :ever_echocardiogram, :boolean
    attr :valve_structures_normal, :boolean
    attr :heart_enlargement, :boolean
    attr :symptomatic, :boolean
    attr :progression, :boolean
    attr :valve_surgery, :boolean
  end

  class HepatitisC < Health::Disease
    attr :contraction_date, :date
    attr :condition_degree, :string
    attr :normal_viral_loads, :boolean
    attr :normal_viral_date, :date
    attr :normal_liver_date, :date
    attr :normal_liver_functions, :boolean
    attr :ever_treated, :boolean
    attr :treatment_start_date, :date
    attr :liver_cirrhosis, :boolean
    attr :liver_biopsy, :boolean
    attr :complications, :boolean
  end

  class IrregularHeartbeat < Health::Disease
    attr :underlying_heart_disease, :boolean
    attr :current_symptoms, :boolean
    attr :current_medications, :boolean
    attr :medication_count, :integer
    attr :atrioventricular_block, :boolean
    attr :second_degree_av_block, :boolean
    attr :third_degree_av_block_atr_disassociation, :boolean
    attr :born_with_third_degree_av_block, :boolean
    attr :pacemaker_implanted, :boolean
    attr :pacemaker_implant_date, :date
    attr :attrioventricular_junctional_rhythm, :boolean
    attr :paroxysmal_super_tachycardia, :boolean
    attr :cardiac_evaluations, :boolean
    attr :cardiac_eval_result_normal, :boolean
    attr :last_experience_symptoms, :date
    attr :symptoms_per_year, :integer
    attr :premature_atrial_complexes, :boolean
    attr :history_of_cardiovascular_disease, :boolean
    attr :premature_ventricular_contraction, :boolean
    attr :simple_pvc, :boolean
    attr :complex_pvc, :boolean
    attr :require_treatment_for_pvc, :boolean
    attr :sick_sinus_syndrome, :boolean
    attr :pacemaker_for_sick_sinus_syndrome, :boolean
    attr :pacemaker_for_sick_sinus_syndrome_implant_date, :date
    attr :history_of_fainting, :boolean
    attr :sinus_bradycardia, :boolean
    attr :pulse_rate, :integer
    attr :sinus_bradycardia_caused_by_another_condition, :boolean
    attr :sinus_bradycardia_caused_by_medication, :boolean
    attr :sinus_bradycardia_cause_unknown, :boolean
    attr :wandering_pacemaker, :boolean
    attr :cardiac_eval_for_wandering_pacemaker, :boolean
    attr :idoventricular_rhythm, :boolean
    attr :mobitz_type_1_block, :integer
    attr :mobitz_type_2_block, :integer
  end

  class ElevatedLiverFunction < Health::Disease
    attr :alt, :integer
    attr :ast, :integer
    attr :ggtp, :integer
    attr :is_stable, :boolean
    attr :is_hepatitis_negative, :boolean
    attr :is_cdt_negative, :boolean
  end

  class MultipleSclerosis < Health::Disease
    attr :condition_degree, :string
    attr :attacks_per_year, :integer
    attr :last_attack_date, :date
    attr :condition_type, :string
  end

  class OtherDisease < Health::Disease
    attr :treatment_type, :string
  end

  class OtherCancer < OtherDisease
    attr :grade, :integer
    attr :stage, :integer
  end

  class MovingViolation < Health::Detail
    include Health::ArrayDetail

    attr :accident, :boolean, 'Did this incident involve a car accident?'
    attr :date, :date, 'When did this incident occur?'
    attr :dl_suspension, :boolean, 'Did this incident result in a driver\'s license suspension?'
    attr :dl_suspension_end, :date, 'Whether past or future, when will/did your DL suspension end?'
    attr :dui_dwi, :boolean, 'Were you found to have (or did you plead guilty or no contest to a charge of) operating while under the influence of drugs or alcohol?'
    attr :reckless_driving, :boolean, 'Did this incident constitute an instance of reckless driving?'
  end

  class Parkinsons < Health::Disease
    attr :age_at_diagnosis, :integer
    attr :condition_degree, :string
    attr :live_independently, :boolean
    attr :condition_stable, :boolean
    attr :currently_disabled, :boolean
    attr :disabled_severity, :string
    attr :currently_receive_treatment, :boolean
    attr :rigidity, :boolean
    attr :rigidity_severity, :string
    attr :stable_date, :date
    attr :walking_impairment, :boolean
    attr :walking_impairment_severity, :string
    attr :mental_deterioration, :boolean
    attr :affect_fingers_only, :boolean
    attr :affect_hands_only, :boolean
    attr :affect_multiple_areas, :boolean
  end

  class ProstateCancer < Health::Disease
    attr :cancer_stage, :integer
    attr :prostatectomy, :boolean
    attr :prostatectomy_date, :date
    attr :radiation, :boolean
    attr :radiation_currently, :boolean
    attr :watchful_waiting, :boolean
    attr :gleason_score, :integer
    attr :pre_psa, :float
    attr :post_psa, :float
    attr :metastasis, :boolean
    attr :reoccurrence, :boolean
  end

  class SleepApnea < Health::Disease
    attr :condition_degree, :string
    attr :ever_treated, :boolean
    attr :treatment_start_date, :date
    attr :use_cpap, :boolean
    attr :rd_index, :integer
    attr :apnea_index, :integer
    attr :ah_index, :integer
    attr :o2_saturation, :integer
    attr :cpap_machine_complaint, :boolean
    attr :sleep_study, :boolean
    attr :sleep_study_followup, :boolean
    attr :on_oxygen, :boolean
    attr :currently_sleep_apnea, :boolean
  end

  class Stroke < Health::Disease
    attr :condition_degree, :string
    attr :last_stroke_date, :date
    attr :multiple_strokes, :boolean
  end

  class WeightReduction < Health::Disease
    attr :procedure_date, :date
    attr :prior_weight, :integer
    attr :current_weight_date, :date
    attr :any_complications, :boolean
    attr :procedure_type, :string
  end

  serialize :crimes, Coder.new(Crime)
  serialize :diseases, Health::Disease::Coder.new(true)
  serialize :moving_violations, Coder.new(MovingViolation)
  serialize :relatives_diseases, Health::Disease::Coder.new

  include Health::LegacySupport
end
