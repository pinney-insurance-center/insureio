class Crm::TaskBuilder < ActiveRecord::Base
  include Marketing::HasTemplateStereotype

  
  
  # Associations
  belongs_to      :assigned_to,    class_name:'User'
  belongs_to      :owner,          polymorphic:true
  belongs_to      :brand,          class_name:'Brand'
  belongs_to      :template,       class_name:'Marketing::Template'
  belongs_to_enum :role,           class_name:'Enum::UsageRole'
  belongs_to_enum :task_type,      class_name:'Enum::TaskType'

  alias_attribute :name,           :label
  alias_attribute :status_type,    :owner
  alias_attribute :status_type_id, :owner_id

  # Require template for email tasks
  validate -> { errors.add(:template, "can't be blank for #{task_type.name} tasks") if template.nil? }, if: :email?
  validate -> { errors.add(:role_id, "can't be blank for #{task_type.name} tasks") if role_id.nil? }, if: :email?

  def assigned_to_name
    assigned_to.try(:name)
  end

  def assignment(status)
    assignee  =status.statusable.is_a?(Crm::Case) ? status.try(:statusable).try(:agent).try(:sales_support, self.role) : nil
    assignee||=status.try(:person).try(:owner).try(:sales_support, self.role)
  end

  # Build a Task based on this TaskBuilder instance
  def build options={}
    # initialize task
    task = Task.new options
    [:assigned_to_id, :label, :task_type_id, :template_id, :evergreen, :role_id].each do |field|
      task[field] = self[field]
    end
    # calc, set due datetime
    task.due_at = Time.now + (self.delay||0).days
    if skip_holidays_and_weekends
      initial_due_at = task.due_at
      seconds_in_a_fortnight = 60 * 60 * 24 * 14
      while task.holday_or_weekend_scheduled?
        task.due_at += 1.day
        if task.due_at - initial_due_at > seconds_in_a_fortnight
          raise Exception.new("Cannot schedule task for task builder #{id} with delay #{delay} for #{initial_due_at}")
        end
      end
    end
    # try to auto-assign to user, based on role of builder (unless assignation is already make)
    if self.role && task.assigned_to_id.nil? && !task.auto_assign!(role_id) && (!task.person.respond_to?(:test?) || !task.person.try(:test?))
      # Send warning email if still no assignment is made
      warning_recipient = task.person.try(:owner).try(:primary_email).try(:value)
      data = {id:id, label:label, role_id:role_id, task_id:task.id, status_id:task.status_id, statusable_id:task.status.try(:statusable_id), statusable_type:task.status.try(:statusable_type)}
      audit log:'status', body:"Warning: no user found for assignment of task for TaskBuilder: #{data.inspect}\n\nperson: #{task.person_type} #{task.person_id}\nstatusable: #{task.status.try(:statusable_id)} #{task.status.try(:statusable_type)}\ntask: #{task.id}"
      hyperlink = if data[:status_id]
        "#{APP_CONFIG['base_url']}/crm/statuses/#{data[:status_id]}?layout=1"
      else
        ""
      end
      SystemMailer.warning("No User found for assignment of task for TaskBuilder '#{task_builder.label}' under #{task_builder.owner_type} '#{task_builder.owner.try(:name)}'\n#{data.inspect}", warning_recipient).deliver rescue Net::SMTPFatalError
    end
    # return
    task
  end

  # Explicitly define this so that it doesn't get interpreted as a call to Sidekiq
  def delay
    read_attribute :delay
  end

  # Returns true if this build a system-owned task
  def system?
    role_id.nil? and assigned_to_id.nil?
  end

  def template_name
    template.try(:name)
  end

  # @return boolean indicating whether this TaskBuilder builds a task that should send an email
  def email?
    !!task_type.try(:name).try(:match, /email/)
  end

end
