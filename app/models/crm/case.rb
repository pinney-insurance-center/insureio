class Crm::Case < ActiveRecord::Base
  include Crm::Accessible
  include Processing::CaseMethods
  include Processing::Apps::CaseMethods

  serialize :ext, Hash

  # Associations
  belongs_to_enum :benes_designation
  belongs_to_enum :bill_type
  belongs_to_enum :dividend_type
  belongs_to_enum :product_type,           class_name: "Enum::ProductType"
  belongs_to_enum :purpose,                class_name: "Enum::Purpose"
  belongs_to_enum :purpose_type,           class_name: "Enum::PurposeType"
  belongs_to_enum :sales_stage,            class_name: "Enum::SalesStage"

  belongs_to      :agent,                  class_name: "User"
  belongs_to      :agent_of_record,        class_name: "User"
  belongs_to      :consumer,               class_name: 'Consumer',  foreign_key: :consumer_id, inverse_of: :cases
  belongs_to      :consumer2,              class_name: 'Consumer',  foreign_key: :consumer_id, inverse_of: :existing_policies#necessary for validation.
  belongs_to      :replaced_by,            class_name: "Crm::Case", foreign_key: :replaced_by_id

  has_many        :quoting_details,        class_name: "Quoting::Quote", foreign_key: :case_id

  #Note: The accessor for `current_details` is overridden lower down.
  #That allows it to be treated as an association in some scenarios, and as the last item in `quoting_details` in others.
  has_one         :opportunity_details,    ->{ where sales_stage_id:Enum::SalesStage.id('Opportunity') },          class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :quoted_details,         ->{ where sales_stage_id:Enum::SalesStage.id('Opportunity') },          class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :contacted_details,      ->{ where sales_stage_id:Enum::SalesStage.id('Contacted') },            class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :a_team_details,         ->{ where sales_stage_id:Enum::SalesStage.id('App Fulfillment') },      class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :app_fulfillment_details,->{ where sales_stage_id:Enum::SalesStage.id('App Fulfillment') },      class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :submitted_details,      ->{ where sales_stage_id:Enum::SalesStage.id('Submitted To Carrier') }, class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :approved_details,       ->{ where sales_stage_id:Enum::SalesStage.id('Submitted To Carrier') }, class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :issued_details,         ->{ where sales_stage_id:Enum::SalesStage.id('Submitted To Carrier') }, class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :ixn_ext, inverse_of: :kase
  has_one         :placed_details,         ->{ where sales_stage_id:Enum::SalesStage.id('Placed') },               class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :current_details,        ->{ order 'sales_stage_id DESC, updated_at DESC' },                     class_name: "Quoting::Quote", foreign_key: :case_id
  has_one         :staff_assignment,       class_name: "Usage::StaffAssignment", as: :owner
  has_one         :next_task,              ->{ where(active:true, suspended:false).order('due_at ASC') }, class_name: "Task",   as: :sequenceable
  has_one         :status,                 ->{ where(active:true).order('created_at DESC') }, class_name: "Crm::Status", as: :statusable

  has_many        :attachments,            class_name: "Attachment",              foreign_key:'case_id'
  has_many        :app_page_snapshots,     ->{ where 'file_name LIKE "app_page_snapshot_%"'   }, class_name: "Attachment",              foreign_key:'case_id'
  has_one         :illustration_file,      ->{ where 'file_name LIKE "case_%_illustration.%"' }, class_name: "Attachment",              foreign_key:'case_id'
  has_many        :notes,                  as: :notable
  has_many        :statuses,               class_name: "Crm::Status", as: :statusable
  has_many        :tasks,                  class_name: "Task",   as: :sequenceable
  has_many        :replaced_cases,         class_name: "Crm::Case",   inverse_of: :replaced_by, foreign_key: :replaced_by_id
  has_many        :custom_requirements,    class_name: "Crm::CaseRequirement", inverse_of: :crm_case, foreign_key: :case_id
  has_many        :outstanding_requirements, ->{ where(status_id: Enum::CaseReqStatus.id("Outstanding") ) }, class_name: "Crm::CaseRequirement"

  has_many        :stakeholder_relationships, class_name: "Crm::ConsumerRelationship", foreign_key: :crm_case_id, inverse_of: :crm_case
  has_many        :stakeholders,              class_name: "Consumer",        through: :stakeholder_relationships, source: :stakeholder

  #These associations are here for reporting purposes only.
  has_one        :owner_relationship,      ->{ where is_owner:true }, class_name: "Crm::ConsumerRelationship", foreign_key: :crm_case_id, inverse_of: :crm_case
  has_one        :owner,                   class_name: "Consumer",    through: :owner_relationship, source: :stakeholder
  has_one        :payer_relationship,      ->{ where is_payer:true }, class_name: "Crm::ConsumerRelationship", foreign_key: :crm_case_id, inverse_of: :crm_case
  has_one        :payer,                   class_name: "Consumer",    through: :payer_relationship, source: :stakeholder

  #Scopes for preloading agent and brand names in reporting.
  belongs_to      :agent_name_only,            ->{ select [:id,:full_name] }, class_name: "User", foreign_key: :agent_id
  belongs_to      :agent_of_record_name_only,  ->{ select [:id,:full_name] }, class_name: "User", foreign_key: :agent_of_record_id

  counter_culture :consumer,
    column_name: proc {|c| c.sales_stage_id.to_i < Enum::SalesStage.id('Placed') ? 'pending_policies_count' : 'placed_policies_count' },
    column_names: {
      ['crm_cases.sales_stage_id< ?',Enum::SalesStage.id('Placed')]=>'pending_policies_count',
      ['crm_cases.sales_stage_id= ?',Enum::SalesStage.id('Placed')]=>'placed_policies_count',
    },
    delta_magnitude: proc {|c| c.active ? 1 : 0 }

  accepts_nested_attributes_for :ixn_ext
  accepts_nested_attributes_for :quoting_details,
                                :replaced_cases,
                                update_only: true
  accepts_nested_attributes_for :notes

  accepts_nested_attributes_for :stakeholder_relationships, allow_destroy: true

  #This temporary attribute allows important feedback like deprecation notices to be shown to the user,
  #without preventing save as `errors` would.
  attr_accessor  :warnings

  alias_attribute :client,            :consumer
  alias_attribute :insurance_exists,  :is_a_preexisting_policy
  alias_attribute :financed,          :financing

  delegate        :name,            to: :sales_stage,    prefix: true, allow_nil: true
  delegate        :name,            to: :product_type,   prefix: true, allow_nil: true

  delegate        :branding_name,
                  :brand,
                  :state,
                  :tobacco?,        to: :consumer, allow_nil: true
  delegate        :carrier_id,      to: :current_details, allow_nil: true
  delegate        :count,           to: :attachments, prefix: true, allow_nil:true
  delegate        :count,           to: :app_page_snapshots, prefix: true, allow_nil:true
  delegate        :product_category,to: :product_type, allow_nil:true

  delegate :annualized_premium, to: :current_details, allow_nil: true
  delegate :duration, to: :current_details, allow_nil: true
  delegate :duration_id, to: :current_details, allow_nil: true
  delegate :duration_category_id, to: :current_details, allow_nil: true

  # Callbacks for save/create/update
  before_validation ->{ self.consumer||=replaced_by.consumer; self.agent||=replaced_by.consumer.agent }, if: :replaced_by
  before_save   :infer_termination_date,
                :copy_quoting_details_fields,
                :set_initial_status!,
                :inherit_agent_from_consumer,
                :set_stage_field,
                :mirror_agent_change_to_staff_assignment
  before_save   ->{ self.sent_to_agency_works_at= Date.today if agency_works_id_changed? && agency_works_id.present? }
  before_create ->{ self.agent_of_record||=(agent.try(:agent_of_record) || agent) }
  after_save    :create_details_for_sales_stage
  after_update  :update_tasks
  after_update  :update_replaced_cases

  #This action should be run in its own transaction,
  #since it is potentially long running network operation,
  #and it is not essential to the integrity of the other data being saved.
  after_commit  :save_app_page_snapshot
  after_commit  :push_to_esp, if: :esp_push_pending?

  after_save    :reassoc_statuses_and_tasks_from_opportunity

  validate ->{ consumer.present? || consumer2.present? }, message: "Consumer can't be blank"
  validate  :meets_requirements_for_app_fulfillment, if: ->{ @check_app_fulfillment_requirements }
  validate ->{ Esp::Validation.new(self).valid? }, if: ->{ esp_push_pending? && sales_stage_id_changed? }
  validates :scor_guid, uniqueness: true, allow_nil:true

  ###################
  # SCOPES
  ###################

  # Scope for join with consumer and users
  scope :consumer_join, lambda {
    joins("LEFT OUTER JOIN consumers ON consumers.id = crm_cases.consumer_id").
    joins("INNER JOIN users ON users.id = consumers.agent_id OR users.id = #{self.table_name}.agent_id").
    joins('LEFT JOIN `usage_ascendants_descendants` ON `usage_ascendants_descendants`.`descendant_id` = `usage_users`.`id`')}

  scope :in_force, -> {
    where("effective_date < ?", Time.now)
    .where('termination_date IS NULL OR termination_date > ?', Time.now)
  }

  scope :for_manager, lambda { |user|
    ids = user.descendant_ids
    ids << user.id
    where("crm_cases.agent_id in (?)", ids)}

  scope :for_user, lambda { |user|
    where("crm_cases.agent_id = ?", user.id)}

  scope :person_name_like, lambda { |person_name|
    joins(:consumer).merge( Consumer.name_like(person_name) )
  }

  scope :status_type_name_like, lambda { |status|
    joins(:statuses).where('crm_statuses.active=TRUE AND status_type_name LIKE ?', "%#{status}%")
  }

  scope :status_type_id, lambda { |st_id|
    joins(:statuses).where('crm_statuses.active=TRUE AND crm_statuses.status_type_id=?',st_id)
  }

  #This scope is meant to reduce repetitive code while still not defining `status` as its own association.
  #Doubly-linked associations lead to confusing, circular callback chains and are to be avoided.
  scope :joins_current_status, lambda {
    joins(%Q(
      INNER JOIN crm_statuses AS status
      ON         status.statusable_id=#{self.table_name}.id
      AND        status.statusable_type='Crm::Case'
      AND        status.active=TRUE
    ))
  }
  scope :joining_current_status, lambda{
    :joins_current_status
  }
  scope :join_current_status, lambda{
    :joins_current_status
  }

  #DEPRECATED.
  #If you come across this scope being applied to cases,
  #either remove it, or replace it with the scope "open".
  scope :active, lambda {}

  scope :not_yet_effective, ->{ where(stage:0) }
  scope :effective, ->{ where(stage:1)}
  scope :effective_or_terminated, ->{ where(stage:[1,2]) }
  # Scopes cases which are placed and have not terminated
  scope :existing_coverage, ->{ where(sales_stage_id:Enum::SalesStage.id('Placed')).where('termination_date IS NULL OR termination_date > ?', Date.today) }

  scope :open, ->{ where(active:true) }

  scope :pending, lambda{
    where("sales_stage_id<? OR sales_stage_id IS NULL",Enum::SalesStage.id('Placed'))
  }
  scope :placed, lambda{
    where("sales_stage_id>=?",Enum::SalesStage.id('Placed'))
  }

  #Take advantage of database index on id and name.
  def agent_name
    if association(:agent).loaded? && agent.present?
      agent.full_name
    elsif agent_id
      agent_name_only.try(:full_name)
    end
  end

  def consumer_name
    consumer.full_name
  end

  #Take advantage of database index on id and name.
  def agent_of_record_name
    if association(:agent_of_record).loaded? && agent_of_record.present?
      agent_of_record.full_name
    elsif agent_of_record_id
      agent_of_record_name_only.try(:full_name)
    end
  end

  def esp_push_pending?
    !opportunity? && !ext[:esp_push] && consumer&.brand_id == Brand::NMB_ID && \
    current_details&.duration&.duration_category_id == 1
  end

  def opportunity?; sales_stage_id == 1; end

  def policy_type
    SystemMailer.deprecated_method_warning self
    self.product_type
  end

  def policy_type_id
    SystemMailer.deprecated_method_warning self
    self.product_type_id
  end

  #Set `product_type_id` on the `current_details`, and it will be copied to this record.
  def product_type_id= value
    SystemMailer.deprecated_method_warning self
    self.current_details&.product_type_id= value
  end

  def push_to_esp
    # This can be triggered by the user and gets called in an after_commit callback
    # The delay allows time for any secondary transactions triggered by callbacks to complete.
    if consumer&.brand_id == Brand::NMB_ID && Esp::Validation.new(self).valid?
      EspPushWorker.perform_at(5.seconds.from_now,id)
    end
  end

  def business_insurance
    #Setting of this columns is now DEPRECATED in favor of setting a specific purpose instead.
    if !self[:business_insurance].nil?
      if purpose
        purpose.is_for_business
      else
        purpose_type.try(:is_for_business)
      end
    else
      self[:business_insurance]
    end
  end

  def insured_is_owner?
    !stakeholder_relationships.any?{|sr| sr.is_owner }
  end
  alias_method :insured_is_owner, :insured_is_owner?

  def insured_is_payer?
    !stakeholder_relationships.any?{|sr| sr.is_payer }
  end
  alias_method :insured_is_payer, :insured_is_payer?


  #Shortcut methods:
  def quoted_details_attributes= attrs
    ss_id=Enum::SalesStage.id('Opportunity')
    attrs[:sales_stage_id]=ss_id
    self.quoting_details_attributes= [attrs]
  end

  def submitted_details_attributes= attrs
    ss_id=Enum::SalesStage.id('App Fulfillment')
    attrs[:sales_stage_id]=ss_id
    self.quoting_details_attributes= [attrs]
  end

  def approved_details_attributes= attrs
    ss_id=Enum::SalesStage.id('Submitted To Carrier')
    attrs[:sales_stage_id]=ss_id
    self.quoting_details_attributes= [attrs]
  end

  def placed_details_attributes= attrs
    ss_id=Enum::SalesStage.id('Placed')
    attrs[:sales_stage_id]=ss_id
    self.quoting_details_attributes= [attrs]
  end

  #This method accepts a file upload (`ActionDispatch::Http::UploadedFile` object).
  #This method overwrites the name before saving.
  def illustration_data= file
    extension=file.original_filename.match(/\.(\w*)/)[1]
    file.original_filename="case_#{id}_illustration.#{extension}"
    attachment = Attachment.create kase: self, person: consumer, uploader: file
  end

  def update_tasks
    return unless self.agent_id_changed?

    #make sure we're only updating tasks that were assigned the previous agent
    tasks=self.tasks.active.where(suspended:false, assigned_to_id:self.changes["agent_id"][1])
    tasks.update_all(assigned_to_id: self.agent_id, assigned_to_name: self.agent_name)
  end


  def record_type; "Policy"; end

  def last_app_page_snapshot
    self.app_page_snapshots.last
  end

  def get_nearest_staff_assignment role_name
    sa  =staff_assignment.try(role_name)
    sa||=agent.staff_assignment.try(role_name)
    role_id=Enum::UsageRole.id role_name.to_s.humanize.downcase
    user_id=Usage::StaffAssignment.new.user_id_for_role_id role_id
    sa||=User.find_by_id(user_id)
  end

  def insurance_coordinator_name
    sa=get_nearest_staff_assignment :insurance_coordinator
    sa.present? ? sa.name : ''
  end

  def application_specialist_name
    sa=get_nearest_staff_assignment :application_specialist
    sa.present? ? sa.name : ''
  end

  def case_manager
    get_nearest_staff_assignment :case_manager
  end

  #Set purpose_type_id when setting purpose_id
  def purpose_id= value
    value=value.to_i
    self[:purpose_id]=value
    self[:purpose_type_id]=Enum::Purpose.find(value).try(:type_id)
  end

  def state
    consumer.try(:state)
  end

  def agent_id
    self.read_attribute(:agent_id) || self.consumer.try(:agent_id)
  end

  #DEPRECATED. Only leaving this here because it is a field available to insert into email templates.
  #Going forward, instead, use `entered_stage_app_fulfillment` directly.
  def app_sent_at
    SystemMailer.deprecated_method_warning rescue nil
    entered_stage_app_fulfillment
  end

  def quoter_completed
    true if self.quoting_details.present?
  end

  #DEPRECATED. Use `exam_status`.
  def exam_completed
    SystemMailer.deprecated_method_warning rescue nil
    self.exam_status=~/complete/i
    end

  def exam_completed= value
    SystemMailer.deprecated_method_warning rescue nil
    nil
  end

  def inherit_agent_from_consumer
    self[:agent_id] ||= self.client.try(:agent_id)
  end

  def copy_exam(cas)
    if self.consumer === cas.consumer
      ['exam_company', 'exam_num', 'exam_status', 'exam_time'].each do |field|
        self[field] = cas[field]
      end
    end
    self.save!
  end

  def exam_date
    exam_time.try(:to_date)
  end

  def exam_scheduled?
    exam_num.present? and exam_company.present? and exam_time.present?
  end

  def exam_time_of_day
    exam_time.strftime('%l:%M %p').lstrip
  end

  def get_schedule_url(exam_company)
    if exam_company == 'SMM'
      Processing::Smm::schedule_url(self)
    else
      Processing::ExamOne::schedule_url(self)
    end
  end

  def product_cat
    Enum::ProductTypeCat.find( product_type.try(:product_cat_id) || 1 )
  end

  def product_type_name
    self.product_type.try :name
  end

  def product_type_name= name
    self.product_type = Enum::ProductType.find_by_name(name)
  end

  #This override method allows `current_details` to be retrieved either
  #through a targetted database query, or if quoting details are already in memory,
  #then from the list in memory. This is helpful when getting quotes from an external quoting engine
  #based on quoting details that are not yet saved.
  def current_details
    if association(:current_details).loaded? && !association(:quoting_details).loaded?
      association(:current_details).target
    else
      quoting_details.sort_by{|qd| qd.sales_stage_id.to_i }.last
    end
  end

  def plan_name
    return "" if self.current_details.blank?
    return "" if self.current_details.category.blank?
    return self.current_details.category.name
  end

  #DEPRECATED. Set this on the `quoting_details` record instead.
  def carrier_id= value
    SystemMailer.deprecated_method_warning self
    details=current_details || create_details_for_sales_stage
    method_name=self.persisted? ? :update_attributes : :assign_attributes
    details.send(method_name,carrier_id: value)
  end

  #DEPRECATED. Set this on the `quoting_details` record instead.
  #It will be denormalized onto the cases table in callback `copy_quoting_details_fields`.
  def face_amount= value
    SystemMailer.deprecated_method_warning self
    self[:face_amount]=value
  end

  #DEPRECATED. Set this on the `quoting_details` record instead.
  #It will be denormalized onto the cases table in callback `copy_quoting_details_fields`.
  def product_type_id= value
    SystemMailer.deprecated_method_warning self
    self[:product_type_id]=value
  end

  # Returns whether this policy is in force
  def in_force?
    return false if self.effective_date.nil?
    return false if !self.effective_date.to_date.past?
    return false if !self.termination_date.nil? && !self.termination_date.to_date.future?
    true
  end

  # Makes JSON-acceptable Hash of self and lower associations, suitable for dumping to backup file
  def json_dump
    as_json include:[
      :quoting_details, :notes, :attachments, :stakeholder_relationships, :smm_status,
      :docusign_case_data, :staff_assignment, :replaced_by_id, :premium_payer, :owner,
      statuses:{include: :tasks}
    ],methods:[:carrier_id]
  end

  def locked?
    #need to implement
  end

  # Calculate and set termination_date if able and appropriate to do so.
  def infer_termination_date
    return if     termination_date
    return unless effective_date
    return unless current_details.try(:duration)
    return unless effective_date_changed? || sales_stage_id_changed?
    if current_details.duration.years
      self.termination_date = effective_date + current_details.duration.years.years
    elsif current_details.duration.cutoff_age && consumer.birth_or_trust_date
      self.termination_date = consumer.birth_or_trust_date + approved_details.duration.cutoff_age.years
    end
  end

  #When sales stage changes, create a details record with that sales stage if one does not exist.
  #Use attributes from most current existing details record if one exists.
  def create_details_for_sales_stage ss_id=self.sales_stage_id
    return if quoting_details.any?{|qd| qd.sales_stage_id==ss_id}
    curr_attrs = current_details.try(:attributes)||{}
    qd_attrs=curr_attrs.except('id','created_at','updated_at','annual_premium','monthly_premium','quarterly_premium','semiannual_premium','sales_stage_id')
    qd = quoting_details.find_or_initialize_by sales_stage_id: ss_id
    qd.attributes = qd_attrs
    qd.save if persisted?
    qd
  end

  #This method is only for use in the `meets_requirements_for_app_fulfillment` validation.
  def beneficiaries
    stakeholder_relationships.select{|s| s.is_beneficiary? }
  end

  #This method is only for use in the `meets_requirements_for_app_fulfillment` validation.
  def primary_beneficiary_percentages_must_total_to_100
    srs=stakeholder_relationships
    p_bs=srs.select{|s| s.is_beneficiary? && !s.contingent }
    p_b_percentage=p_bs.map(&:percentage).map(&:to_i)
    p_b_percentage.sum==100
  end

  def contingent_beneficiary_percentages_must_total_to_100
    srs=stakeholder_relationships
    c_bs=srs.select{|s| s.is_beneficiary? && s.contingent }
    c_b_percentage=c_bs.map(&:percentage).map(&:to_i)
    c_bs.empty? || c_b_percentage.sum==100
  end

  alias_attribute :temporary_insurance, :bind

  def temporary_insurance_must_have_answer
    !self.temporary_insurance.nil?
  end

  #This override ensures that the key `status_type_id`,
  #if present, is always assigned last.
  def assign_attributes attrs
    attrs=attrs.with_indifferent_access
    st_id=attrs.delete :status_type_id
    super attrs
    self.status_type_id=st_id if st_id.present?
  end

  #This attribute writer allows a new status to be created through `update_attributes`,
  #which keeps the logic out of the controller.
  #Statuses should only ever be created through `update_attributes` with key `status_type_id`.
  def status_type_id= type_id=nil
    return unless type_id.present? && type_id.respond_to?(:to_i) && type_id=type_id.to_i
    type=Crm::StatusType.where(is_for_policies:true, id:type_id).first
    return unless type.present?
    return if status && status.status_type_id==type_id#don't duplicate the current status
    starting_status = status

    types_with_requirements=[
      Crm::StatusType::PRESETS[:submit_to_app_team][:id],
      Crm::StatusType::PRESETS[:submit_to_app_team2][:id]
    ]
    if types_with_requirements.include?(type_id)
      #For new records, at this point it cannot yet access the consumer object,
      #so we bypass the check until validation phase.
      should_create_status= (self.new_record? && consumer.nil?) || meets_requirements_for_app_fulfillment(true)
      @check_app_fulfillment_requirements=true#this ensures the validation runs later in the transaction, to potentially set errors
      return unless should_create_status#short-circuit the transaction if it's invalid
    end

    #Create the new details record before saving the status change,
    #so the reference to the current details record doesn't get lost,
    #in case it has changes not yet persisted to the database.
    if self.sales_stage_id.to_i<type.sales_stage_id.to_i
      create_details_for_sales_stage type.sales_stage_id.to_i
    end

    method= self.persisted? ? :create : :new
    begin
      retries ||= 0
      new_status = self.statuses.send method, status_type: type, statusable: self
    rescue => ex
      retry if (retries += 1) < 3
      ExceptionNotifier.notify_exception(ex)
    end
    new_status.valid?#trigger callbacks without saving yet.

    create_note_for_status_change(new_status.creator, starting_status)
    already_in_a_save_op=ActiveRecord::Base.connection.open_transactions > 0
    if self.persisted? && !already_in_a_save_op
      #Avoid nested saves, which would run callbacks multiple times.
      save
    end
    # Return
    new_status
  end

  def meets_requirements_for_app_fulfillment force_check=false
    return true unless @check_app_fulfillment_requirements || force_check
    @check_app_fulfillment_requirements=false#skip this method in case of nested saves
    #Check requrements
    @missing_fields=[]
    Crm::Status::REQUIREMENTS_FOR_APP_FULFILLMENT.each do |method_chain|
      obj = self
      method_chain.each{|x| obj = obj.try(x) rescue nil }
      if obj.blank?
        human_field_name=method_chain.last.gsub(/_id$/,'').gsub(/_/,' ').gsub(/dln/,'driver\'s license number').gsub(/dl state/,'driver\'s license state')
        @missing_fields << human_field_name
      end
    end
    return true if @missing_fields.blank?
    errors.add :base, "Before you can submit for processing, you will need to provide the following fields: #{@missing_fields.join(', ')}"
    return false
  end

  def create_note_for_agent_change(perpetrator, former_agent=nil)
    create_note_for_association_name_change(perpetrator, 'agent', agent, former_agent)
  end

  def create_note_for_aor_change(perpetrator, former_aor=nil)
    create_note_for_association_name_change(perpetrator, 'agent of record (AOR)', agent_of_record, former_aor)
  end

  def create_note_for_status_change(perpetrator, former_status=nil)
    create_note_for_association_name_change(perpetrator, 'status', status, former_status, note_type_id:Enum::NoteType.id('status change'))
  end

  Enum::SalesStage.all.each do |ss|
    if ss.id<Enum::SalesStage.id('Submitted To Carrier')
      ss_name=ss.name.gsub(' ','_').downcase
      next_ss_name=Enum::SalesStage.find(ss.id+1).name.gsub(' ','_').downcase
      time_method_name="time_in_#{ss_name}_stage"

      define_method(time_method_name) do |use_dhm=true, restrict_to_past_tense=false|
        end_time  =self.send("entered_stage_#{next_ss_name}")
        start_time=ss.id==1 ? self.created_at : self.send("entered_stage_#{ss_name}")
        return 'n/a' if !start_time || (restrict_to_past_tense && !end_time)
        end_time||=Time.now
        if use_dhm
          time_str=self.class.dhm_between start_time, end_time
        else
          time_str=(end_time-start_time).to_i
        end
        self.send("entered_stage_#{next_ss_name}") && use_dhm ? time_str.to_s : time_str.to_s+' (so far)'
      end
    end

    #Define accessor methods for the first status of each sales stage.
    #It's better to call `self.statuses`,filtering using rails array method `select` than to define individual associations for these,
    #because these will only be used when aggregating cases, and this way they all get preloaded at once.
    status_method_name='first_status_in_stage_'+ss.name.to_s.downcase.gsub(/\s/,'_')
    define_method status_method_name do
      self.statuses.select{|s| s.sales_stage_id.to_i>=ss.id }.first
    end
    #Allow calling functions like `kase.first_submitted_status_name`.
    delegate :created_at,:created_by_name,:name, to: status_method_name.to_sym, prefix:true, allow_nil: true
  end
  alias_method :time_until_contact,:time_in_opportunity_stage
  alias_method :time_from_contact_to_sent_stage, :time_in_contacted_stage#should remove this. just putting it in for now so things don't break.

  Enum::StatusTypeCategory.ewhere(is_for_policies:true).each do |c|
    underscored_cat_name=c.name.to_s.downcase.gsub(/[^a-z]+/,'_')
    define_method "entered_category_#{underscored_cat_name}" do
      self.statuses.select{|s| s.status_type_category_id.to_i==c.id }.first.try(:created_at)
    end
  end

  def status
    self.statuses.last
  end

private

  def _accessible? user_or_id, edit_or_view
    if sess_cons_id = Consumer.ltd_access_id
      return false if sess_cons_id != consumer_id && persisted?
    end
    super
  end

  def update_replaced_cases
    return unless self.sales_stage_id_changed? || self.effective_date_changed?
    return unless self.sales_stage_id==Enum::SalesStage.id('Placed')
    return unless self.effective_date.present?
    return unless self.replaced_cases.present?

    st_id=Crm::StatusType::PRESETS[:replaced][:id]
    replaced_cases.each do |r|
      r.update_attributes(status_type_id: st_id)
    end
  end

  #This could be called if `status_type_id=` was called on an opportunity, or
  #if an opportunity was requoted and submitted via the quote path.
  def reassoc_statuses_and_tasks_from_opportunity
    old_opp = self.quoting_details.to_a.find do |qd|
      next unless qd.persisted?
      next unless qd.sales_stage_id.to_i <= self.sales_stage_id.to_i
      next unless qd.created_at < self.created_at
      true
    end
    return unless old_opp.present?
    self.quoted_at=old_opp.created_at
    old_opp.statuses.update_all(statusable_id:self.id, statusable_type:'Crm::Case', updated_at:Time.now, active:false)
    old_opp.tasks.where(evergreen:false).update_all(active:false)
    old_opp.tasks.update_all(sequenceable_id:self.id, sequenceable_type:'Crm::Case', updated_at:Time.now)
  end

  def create_note_for_association_name_change perpetrator, association_text, new_value, former_value, note_attrs={}

    perpetrator = perpetrator.try(:name) unless perpetrator.is_a?(String)
    former_value = former_value.try(:name) unless former_value.is_a?(String)
    unless new_value.is_a?(String)
      new_value = new_value.name || new_value.id && "##{new_value.id}"
    end
    text = "#{perpetrator} changed #{association_text}"
    text += " from #{former_value}" if former_value.present?
    text += " to #{new_value || '(none)'}"
    method=self.persisted? ? :create : :new
    notes.send method, note_attrs.merge(text:text)
  end

  def mirror_agent_change_to_staff_assignment
    if agent_id_changed? && persisted?
      self.staff_assignment||self.build_staff_assignment(agent.try(:staff_assignment) && agent.staff_assignment.attributes.except('id'))
      self.staff_assignment.agent=self.agent
      self.staff_assignment.save
      consumer && self.consumer.tasks.active.where(assigned_to_id:agent_id_was).update_all(assigned_to_id:self.agent)
    end
  end

  def copy_quoting_details_fields
      first_qd=quoting_details.first
      last_qd=quoting_details.last
    if quoted_at.nil?
      self.quoted_at = first_qd.try(:created_at) || Time.now
    end
    if last_qd && (last_qd.face_amount.to_i != face_amount.to_i || last_qd.product_type_id.to_i != product_type_id.to_i)
      self[:face_amount]    =last_qd.face_amount
      self[:product_type_id]=last_qd.product_type_id
    end
    return true
  end

  # This should fire BEFORE +auto_assign_to_agent!+,
  # because if agent was not explicitly specified, they likely do not expect agent-specific settings to be used.
  # For cases to be distributed by LeadDistribution, +agent+ should be nil, and +brand+ should be used
  def set_initial_status!
    return if self.status
    if self.sales_stage_id.to_i>=Enum::SalesStage.id('Placed') || self.stage>0
      type_id=Crm::StatusType::PRESETS[:in_force_no_follow_ups][:id]
    else
      #Calling :initial_status_type vs. :initial_status_type_id so that nil is returned if the record no longer exists.
      type_id  = agent.try(:initial_status_type).try(:id)
      type_id||= consumer.try(:brand).try(:initial_status_type).try(:id)
      type_id||= Crm::StatusType::PRESETS[:prospect][:id]
    end
    self.statuses.build status_type_id:type_id
  end

  #Called when a policy enters the app fulfillment sales stage.
  #Saves a temporary pdf file to the filesystem,
  #then uploads it as an attachment to the case.
  #This should be run outside of any main save transaction,
  #since it involves a network operation.
  #The file rendering should not be delayed long tho,
  #as it is meant to provide a snapshot of the data at a particular point in time.
  def save_app_page_snapshot
    return unless self.sales_stage_id_changed?
    return unless self.sales_stage_id==Enum::SalesStage.id('App Fulfillment')

    the_html  =ApplicationController.new.render_to_string({
      file:'/quoting/quotes/_app_snapshot',
      layout:false,
      formats:[:html],
      locals: {:@case=>self,:@consumer=>self.consumer}
    })
    the_pdf   =WickedPdf.new.pdf_from_string(the_html)
    save_path =Rails.root.join('tmp',"app_page_snapshot_#{Time.now.strftime('%Y-%m-%d_%H_%M_%S')}_#{self.id}.pdf")

    File.open(save_path, 'wb'){|file| file << the_pdf }
    attachment=Attachment.create person:self.consumer, case_id:self.id, uploader:File.open(save_path)
    if attachment.persisted?
      File.delete(save_path)
    else
      error_msgs=attachment.errors.full_messages.join('. ')
      email_msg="Failed to create the application snapshot record in the database.\n"+
                "It appears that the file rendered and saved without error. Location: #{save_path}\n"+
                "The issue may have been with connecting to AWS.\n"+
                error_msgs
      SystemMailer.warning(email_msg)
      self.warnings||=[]
      warnings<< error_msgs
    end
    true#If unsuccessful, this should not rollback the save.
  rescue =>ex
    ExceptionNotifier.notify_exception(ex)
    self.warnings||=[]
    self.warnings<< "Failed to create an application snapshot. Please contact support for details."
  end

  pseudo_date_accessor :termination_date, next:'years_until_termination_date'
  pseudo_datetime_accessor :exam_time

  #Stage 2 is terminated.
  #Stage 1 is effective.
  #Stage 0 (default) is neither.
  #Only cases in stage 1 or 2 will display in the App page.
  #Only cases in stage 0 will display elsewhere.
  def set_stage_field
    if self.termination_date  && self.termination_date <  Date.today
      self.stage = 2
    elsif self.effective_date && self.effective_date < Date.today
      self.stage = 1
    end#default (enforced by the db schema) is 0.
  end

end
