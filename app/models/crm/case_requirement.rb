class Crm::CaseRequirement < ActiveRecord::Base

  belongs_to_enum :case_requirement_type,           foreign_key: :requirement_type_id
  belongs_to_enum :case_req_responsible_party_type, foreign_key: :responsible_party_type_id
  belongs_to_enum :case_req_status,                 foreign_key: :status_id

  belongs_to      :crm_case,        class_name: "Crm::Case", foreign_key: :case_id
  belongs_to      :created_by_user, class_name: "User"

  validates_presence_of :crm_case

  attr_accessor :created_by_user_or_service_name

  

  after_initialize -> { created_by_user=Thread.current[:current_user_id] if !created_by_user && !created_by_user_or_service_name }
  before_save      -> { self.completed_at||=Time.now if case_req_status.indicates_completed }

  #This transient attribute is either set by a controller or other mechanism contacting an outside service, or
  #queried from the associated user record.
  def created_by_user_or_service_name
    return @created_by_user_or_service_name if @created_by_user_or_service_name.present?

    @created_by_user_or_service_name=created_by_user.present? ? created_by_user.name : nil
  end

end
