class Crm::FinancialInfo < ActiveRecord::Base
  

  has_one :consumer, class_name: "Consumer", inverse_of: :financial_info

  before_save ->{self.bankruptcy=true}, if: :bankruptcy?
  
  alias_attribute :income, :annual_income

  def asset_total
    self.attributes.select{|k,v| k.to_s.starts_with? 'asset_'}.values.map(&:to_i).sum
  end
  
  def income_total
    asset_earned_income.to_i + asset_unearned_income.to_i
  end

  # pseudo accessor
  def liability_monthly_income
    (income || 0) / 12
  end

  #pseudo mutator saves income based on monthly income
  def liability_monthly_income= value
    new_calculated_income=value.to_f*12
    if income.to_i != new_calculated_income.to_i
      self.income = new_calculated_income
    end
  end

  def liability_total
    self.attributes.select{|k,v| k.to_s.starts_with? 'liability_'}.values.map(&:to_i).sum
  end

  def net_worth
    if net_worth_use_specified
      net_worth_specified
    else
      net_worth_calculated
    end
  end

  def net_worth_calculated
    asset_total - liability_total
  end

  #DEPRECATED
  #This method is here to accomodate the old API, which had bankruptcy store the date `bankruptcy_declared`.
  #Now this column is a boolean. This method sets the correct column based on which it looks like they are trying to send.
  def bankruptcy= value
    if value.is_a?(Date)
      self.send(:write_attribute,:bankruptcy_declared,value)
    elsif value==true || value==false
      self.send(:write_attribute,:bankruptcy,value)
    end
  end

  def bankruptcy?
    self.read_attribute(:bankruptcy) ||
    ['bankruptcy_declared','bankruptcy_discharged'].any? do |field|
      self.send(field).present?
    end
  end

end
