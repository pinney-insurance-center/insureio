class Consumer < ActiveRecord::Base

  OWNER_ID_COLUMN=:agent_id
  include ContactableStereotype
  include TaggableStereotype
  include Crm::Accessible

  ###################
  # SCOPES
  ###################
  # Default scope
  default_scope -> {
    tenant_id=Thread.current[:tenant_id] rescue nil
    current_user_acts_across_tenancies=Thread.current[:current_user_acts_across_tenancies] rescue nil
    if tenant_id && !current_user_acts_across_tenancies
      where('consumers.active_or_enabled=TRUE').
      where(tenant_id:tenant_id)
    else
      where('consumers.active_or_enabled=TRUE')
    end
  }

  #`insecure_mode` is for backward compatibility, when an iv (initialization vector) was not required.
  #For any new encrypted fields, provide the iv instead.
  attr_encrypted :dln,                 algorithm:'aes-256-cbc', insecure_mode: true,     mode: :single_iv_and_salt, key: APP_CONFIG['old_encryption_key'], if: (Rails.env.production? || Rails.env.test?)
  attr_encrypted :eft_account_num,     algorithm:'aes-256-gcm', mode: :per_attribute_iv, key: APP_CONFIG['encryption_key']
  attr_encrypted :eft_routing_num,     algorithm:'aes-256-gcm', mode: :per_attribute_iv, key: APP_CONFIG['encryption_key']
  attr_encrypted :eft_bank_name,       algorithm:'aes-256-gcm', mode: :per_attribute_iv, key: APP_CONFIG['encryption_key']

  
  #This flag triggers the create_an_opportunity callback.
  attr_accessor  :will_also_apply
  #This temporary attribute allows important feedback like deprecation notices to be shown to the user,
  #without preventing save as `errors` would.
  attr_accessor  :warnings

  after_initialize -> { self.citizenship_id=1 if self.has_attribute?(:citizenship_id) && self.citizenship_id.nil? }#makes +citizenship+ default to 'US Citizen'

  before_validation :apply_conditional_defaults
  before_validation :auto_assign_to_agent!
  before_create -> { tenant_id = brand.try(:tenant_id) if tenant_id.to_i==0}, if: :brand
  before_save    :update_case_manager_name,
                 :update_personal_completed,
                 :update_tracking_tables
  before_save    ->{self.recently_applied_for_life_ins=true if self.last_applied_for_life_ins_date || self.last_applied_for_life_ins_outcome_id}
  after_update   :note_if_agent_was_updated,
                 :handle_marketing_opt_out
  after_save     :update_tasks
  after_save     :create_a_lead, if: ->{ will_also_apply==true || will_also_apply=="1" || will_also_apply==1 }

  # Associations
  belongs_to_enum :birth_country,       class_name: "Enum::Country"
  belongs_to_enum :birth_state,         class_name: "Enum::State"
  belongs_to_enum :citizenship,         class_name: "Enum::Citizenship"
  belongs_to_enum :dl_state,            class_name: "Enum::State"
  belongs_to_enum :marital_status,      class_name: "Enum::MaritalStatus"
  belongs_to_enum :product_type,        class_name: "Enum::ProductType"
  belongs_to_enum :product_cat,         class_name: "Enum::ProductTypeCat"

  belongs_to      :agent,               class_name: "User"
  belongs_to      :agent_name_only,     ->{ select [:id,:full_name] },         class_name: "User"
  belongs_to      :brand,               class_name: "Brand",                   foreign_key: :brand_id
  belongs_to      :brand_name_only,     ->{ select [:id,:full_name] },         class_name: "Brand", foreign_key: :brand_id
  belongs_to      :financial_info,      class_name: "Crm::FinancialInfo",      dependent: :destroy, inverse_of: :consumer
  belongs_to      :health_info,         class_name: "Crm::HealthInfo",         dependent: :destroy, inverse_of: :consumer
  belongs_to      :primary_contact,     class_name: "Consumer"
  belongs_to      :status,              class_name: "Crm::Status"

  has_one         :staff_assignment,    class_name: "Usage::StaffAssignment",  as: :owner, dependent: :destroy

  has_many        :attachments,         class_name: "Attachment",              as: :person
  has_many        :recordings,          class_name: "PhoneRecording",          as: :person
  has_many        :cases,               class_name: "Crm::Case",               inverse_of: :consumer, foreign_key: :consumer_id, dependent: :destroy
  has_many        :existing_policies,   ->{ where sales_stage_id:5 },          class_name: "Crm::Case", foreign_key: :consumer_id, dependent: :destroy
  has_many        :notes,               class_name: "Note",                    as: :notable
  has_many        :physicians,          class_name: "Crm::Physician"
  has_many        :snail_mail_messages, class_name: "Marketing::SnailMail::Message", as: :target
  has_many        :tasks,               class_name: 'Task',                    as: :person
  has_many        :old_tasks,           ->{ where active:false},                class_name: 'Task', as: :person
  has_many        :subscriptions,       class_name: 'Marketing::Subscription', as: :person

  has_many        :relationships_w_self_as_primary, class_name: "Crm::ConsumerRelationship", foreign_key: :primary_insured_id, dependent: :destroy
  has_many        :relationships_w_self_as_related, class_name: "Crm::ConsumerRelationship", foreign_key: :stakeholder_id, dependent: :destroy

  has_many        :relationships,       class_name: "Crm::ConsumerRelationship", foreign_key: :primary_insured_id, dependent: :destroy

  has_many        :stakes_in_cases,     class_name: "Crm::ConsumerRelationship",   foreign_key: :stakeholder_id
  has_many        :cases_at_stake,      class_name: "Crm::Case",                   through: :stakes_in_cases, source: :crm_case

  has_many        :quoting_details,     class_name: "Quoting::Quote",  inverse_of: :consumer
  has_many        :opportunities,       ->{ where case_id:nil },       class_name: "::Quoting::Quote", inverse_of: :consumer, foreign_key: :consumer_id
  has_many        :statuses,            class_name: "Crm::Status",     as: :statusable

  # Check for the existence of tables here because certain rake tasks make use
  # of these models in contexts _before_ the database is created. Trying to
  # access the list of columns when the table doesn't exist raises an error.
  HEALTH_METHODS_TO_INCLUDE = table_exists? ? Crm::HealthInfo.columns.map(&:name).reject{|k| k.match(/_details$/) || %w[created_at updated_at id].include?(k) || Consumer.has_col?(k) }+
  ['family_diseases','family_disease_death_count']+Crm::HealthInfo::DELEGATED_KEYS : []
  FINANCIAL_METHODS_TO_INCLUDE = table_exists? ? Crm::FinancialInfo.columns.map(&:name).reject {|k| Consumer.has_col?(k) } : []

  HEALTH_METHODS_TO_INCLUDE.each do |f|
    self.delegate "#{f}?", to: :health_info, allow_nil:true
    self.delegate f,       to: :health_info, allow_nil:true
    self.delegate "#{f}=", to: :health_info, allow_nil:true
  end
  FINANCIAL_METHODS_TO_INCLUDE.each do |f|
    self.delegate f,       to: :financial_info, allow_nil:true
    self.delegate "#{f}=", to: :financial_info, allow_nil:true
  end

  delegate :tobacco?, to: :health_info,          allow_nil:true
  delegate :income,   to: :financial_info,       allow_nil:true
  delegate :income=,  to: :financial_info,       allow_nil:true
  delegate :state_id, to: :primary_address,      allow_nil: true
  delegate :state,    to: :primary_address,      allow_nil: true
  delegate :city,     to: :primary_address,      allow_nil: true
  delegate :zip,      to: :primary_address,      allow_nil: true
  delegate :name,     to: :primary_contact,      prefix: true, allow_nil: true

  accepts_nested_attributes_for :notes,
                                :relationships_w_self_as_related,
                                :relationships,
                                :statuses,
                                :subscriptions
  accepts_nested_attributes_for :cases,
                                :existing_policies,
                                :opportunities,
                                :financial_info,
                                :health_info,    allow_destroy: true, update_only:true
  accepts_nested_attributes_for :quoting_details

  attr_writer :citizenship_name

  validates :agent_id, presence: {message: 'was not explicitly provided, and could not be set via normal lead distribution.'}
  validates :agent,    presence: {message: "specified does not exist. Must specify an agent that exists."}, if: ->{ (new_record? || agent_id_changed?) && agent_id }
  validates :brand_id, presence: {message: "must be specified."}, if: ->{ new_record? || brand_id_changed? }
  validates :brand,    presence: {message: "specified does not exist. Must specify a brand that exists."}, if: ->{ (new_record? || brand_id_changed?) && brand_id }
  validate  :no_attempted_setting_of_unkown_keys

  alias_attribute :opted_out_of_marketing, :marketing_unsubscribe
  alias_attribute :opted_out_of_status,    :status_unsubscribe
  alias_attribute :owner_id, OWNER_ID_COLUMN
  alias_attribute :test,                   :test_lead

  #This is to prevent errors when users inevitably try to get/set
  #certain deprecated keys which we feel obligated to continue supporting w/out error.
  #Methods aliased to this will eventually be removed.
  def does_and_returns_nothing; nil; end
  def does_and_returns_nothing= value=nil
    SystemMailer.deprecated_method_warning
    true
  end

  def profile_id= value
    SystemMailer.deprecated_method_warning(self) rescue nil
    self.brand_id= value
  end

  alias_method :strict_birth_country=, :birth_country=
  #DEPRECATED.
  #Set `birth_country_id` or `birth_country_custom_name` explicitly instead.
  #Do not remove this function until all calls passing strings (including via API) have been removed.
  def birth_country= country
    SystemMailer.deprecated_method_warning
    if country.is_a? String
      c_id=Enum::Country.id(country)
      if c_id>0
        self.birth_country_id=c_id
      else
        self.birth_country_custom_name=country
      end
    elsif country.is_a? Integer
      self.birth_country_id=country
    else
      strict_birth_country= country
    end
  end

  #Take advantage of database index on contactable id and name.
  alias_method :agent_name, :owner_name

  #Take advantage of database index on contactable id and name.
  def brand_name
    if association(:brand).loaded? && brand
      brand.full_name
    elsif brand_id
      #Association is defined in `ContactableStereotype`.
      brand_name_only.try(:full_name)
    end
  end

  def employed?
    actively_employed || occupation.present? || occupation_description.present?
  end

  #Get last campaign queue num for this Connection
  def last_campaign_subscription_queue_num
    queue_num = Marketing::Subscription.
      select('queue_num').
      where(person_id: self).
      order('queue_num ASC').last.try(:queue_num)
    queue_num == nil ? 0 : queue_num
  end

  # Uses LeadDistribution service object to set agent on self
  def auto_assign_to_agent!
    return if self.agent_id || brand.nil? || @lead_distribution_fired#prevents an infinite loop if agent still cannot be found
    @lead_distribution_fired=true
    audit log:'lead-distribution'
    LeadDistribution.new(self).assign
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

  def has_opps_or_policies
    c=( opportunities_count.to_i + pending_policies_count.to_i + placed_policies_count.to_i )
    c>0
  end

  #Pseudo-accessors and -mutators for critical and priority notes,
  #which are now note records with flags +critical+ or +personal+.
  #Deprecated. For future code, refer directly to note records.
  def critical_note
    SystemMailer.deprecated_method_warning(self) rescue nil
    self.notes.where(critical:true).pluck(:text).join("\n\n")
  end

  def priority_note
    SystemMailer.deprecated_method_warning(self) rescue nil
    self.notes.where(personal:true).pluck(:text).join("\n\n")
  end

  def critical_note= text
    SystemMailer.deprecated_method_warning(self) rescue nil
    method=self.persisted? ? :create : :build
    self.notes.send method, critical:true, text: text
  end

  def priority_note= text
    SystemMailer.deprecated_method_warning(self) rescue nil
    method=self.persisted? ? :create : :build
    self.notes.send method, personal:true, text: text
  end

  def financial_info= value
    if value.is_a? Hash
      self.financial_info_attributes= value
    else
      super value
    end
  end

  def health_info= value
    if value.is_a? Hash
      self.health_info_attributes= value
    else
      super value
    end
  end

  #Returns a scope.
  def children
    #where other person is this person's child
    l1= relationships_w_self_as_primary.of_type(['child','step child']).pluck(:stakeholder_id)
    #where this person is other person's parent
    l2= relationships_w_self_as_related.of_type(['parent','step parent']).pluck(:primary_insured_id)

    Consumer.where(id: (l1+l2) )
  end

  def financial_info
    _supply_needed_association(:financial_info)
  end

  def health_info
    _supply_needed_association(:health_info)
  end

  #Ensure that there is a +financial_info+ record before assigning attributes.
  #Otherwise, attributes assigned to +financial_info+ are not retained.
  #In future, financial and health info need to be changed to `has_one` relationships so this won't be needed.
  def assign_attributes attrs
    #Prevent SystemStackErrors by only looking for health/financial info once,
    #even if this method is repeated.
    attrs=HashWithIndifferentAccess.new attrs
    will_assign_hi_attrs= (attrs.keys.map(&:to_s)-(HEALTH_METHODS_TO_INCLUDE+['health_info_attributes'])).length<attrs.keys.length
    will_assign_fi_attrs= (attrs.keys.map(&:to_s)-(FINANCIAL_METHODS_TO_INCLUDE+['financial_info_attributes'])).length<attrs.keys.length
    if will_assign_fi_attrs && !@skip_financial_info
      financial_info
      attrs['financial_info_attributes']||={}
      FINANCIAL_METHODS_TO_INCLUDE.each do |f|
        attrs['financial_info_attributes'][f]=attrs.delete(f) if attrs.has_key?(f)
      end
      @skip_financial_info=true
    end
    if will_assign_hi_attrs && !@skip_health_info
      health_info
      attrs['health_info_attributes']||=HashWithIndifferentAccess.new
      HEALTH_METHODS_TO_INCLUDE.each do |f|
        attrs['health_info_attributes'][f]=attrs.delete(f) if attrs.has_key?(f)
      end
      @skip_health_info=true
    end

    #so that tags get added/modified, rather than replacing the list, as would normally happen with mass-assignment.
    add_tags attrs.delete(:tags_attributes)

    super attrs
  rescue ActiveRecord::UnknownAttributeError =>ex
    #In edge rails this would just be `ex.attribute`.
    attr_name=ex.message.match(/unknown attribute\: (\w*)/)[1]
    #Because `valid?` clears errors, rather than adding errors to the object here,
    #we must collect these in an instance variable to be checked in a validation.
    @unkown_keys||=[]
    #In case a nested attributes hash contains the key causing the error,
    #so it can't be deleted.
    return false if @unkown_keys.include?(attr_name)
    @unkown_keys<< attr_name
    #Don't just stop at the first one. If there is one, there are likely more.
    #Users would probably appreciate seeing them all, versus addressing one at a time.
    assign_attributes attrs.except(attr_name.to_sym)
  end

  def copy_client(client)
    client.attributes.each do |key, val|
      self[key] = val if self[key].blank?
    end
    self.save!
  end

  def coverage_in_force
    cases.in_force.reduce(0) do |sum, kase|
      sum + kase.approved_details&.face_amount.to_i
    end
  end

  def coverage_submitted
    cases = self.cases.where("effective_date is null or effective_date > ?", Time.now)
    amount = 0
    cases.each do |cas|
      if cas.submitted_details
        amount = cas.submitted_details.face_amount
      end
    end
    amount
  end

  def editable? user
    if self.class.ltd_access_sess?
      super
    else
      super ||
      cases.where(agent_id: user&.id).limit(1).count > 0 ||
      cases_at_stake_ids.present? && Crm::Case.where(id: cases_at_stake_ids).editables(user).limit(1).count > 0
    end
  end
  alias_method :viewable?,    :editable?
  alias_method :viewable_by?, :editable?
  alias_method :editable_by?, :editable?

  def self.update_existing_or_create_new attrs
    @existing=self._find_duplicate attrs

    if @existing.nil?
      new_consumer=self.create attrs
      return new_consumer
    end

    #so that consumers cannot be reassigned to a different brand or agent via the API
    attrs.delete :brand_id
    attrs.delete :agent_id

    @existing.assign_attributes(attrs)

    @existing.note_changes @existing.health_info,  'Health Info' if @existing.health_info
    @existing.note_changes @existing,              'Consumer'
    crm_cases=@existing.cases.select{|c| c.quoted_details.try(:changed?)}
    crm_cases.each do |crm_case|
      @existing.note_changes(crm_case.quoted_details,"Quoted Details for Policy id:#{crm_case.id}") if crm_case.quoted_details
    end
    @existing.active_or_enabled=true
    @existing.save if @existing.changes.present? || @existing.cases.map(&:changes).any?(&:present?)
    return @existing
  end

  # Return first record which duplicates these attributes:
  # id || (brand && full_name && (birth_or_trust_date || email || phone) )
  def self._find_duplicate attrs
    if attrs[:id].present? && same_id=Consumer.find_by_id(attrs[:id])
      return same_id
    end
    if attrs[:brand_id] && attrs[:full_name].present?
      phones = (attrs[:phones_attributes]||[]).map{|h| h[:value].to_s.gsub(/\D/,'') if h.is_a?(Hash) }.select{|v| v.present? }
      emails = (attrs[:emails_attributes]||[]).map{|h| h[:value] if h.is_a?(Hash) }.select{|v| v.present? }
      birth  = Date.parse( attrs[:birth_or_trust_date] ).to_s(:db) rescue nil
      if birth || phones.present? || emails.present?
        query = Consumer.where(brand_id: attrs[:brand_id].to_i, full_name:attrs[:full_name])
        candidate=nil
        if birth.present?
          query_b =query.where(birth_or_trust_date:birth)
          candidate=query_b.readonly(false).first
        end
        if emails.present? && !candidate
          query_e =query.where('email_addresses.value'=>emails)
          query_e =query_e.joins(:emails)
          candidate =query_e.readonly(false).first
        end
        if phones.present? && !candidate
          query_p =query.where('phones.value'=>phones)
          query_p =query_p.joins(:phones)
          candidate =query_p.readonly(false).first
        end
      end
      return candidate
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
    nil
  end

  #Only gets called when updating an existing consumer with information
  #coming from outside the application (Lead Import API or widgets).
  #Creates a note recording the changes.
  #+old_obj+ should be an ActiveRecord object.
  def note_changes old_obj, obj_name
    old_attrs={}
    new_attrs={}
    (old_obj.try(:changes)||[]).each do |k,v|
      old_attrs[k]=v[0]
      new_attrs[k]=v[1]
    end
    if old_attrs.present?
      note_text ="#{obj_name} was changed via API\n"
      note_text+="Old Values:\n#{JSON.pretty_generate(old_attrs)}\n\n"
      note_text+="New Values:\n#{JSON.pretty_generate(new_attrs)}\n"
      self.notes.build(note_type:Enum::NoteType['api-triggered consumer update'], text: note_text )
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
    nil
  end

  def gender= value
    if value.is_a? String
      value = (value =~ /^(m|1|true)/i).present?
    end
    self[:gender] = value
  end

  def gender_string
    case self.gender
      when false
        "Female"
      when true
        "Male"
      else
        ""
    end
  end

  def spouse
    candidate1= self.relationships_w_self_as_primary.of_type(['spouse/domestic partner']).first.try(:stakeholder)
    candidate2= self.relationships_w_self_as_related.of_type(['spouse/domestic partner']).first.try(:primary_insured)
    candidate1 || candidate2
  end

  #This method may produce duplicate spouse relationships.
  #It will not overwrite another existing relationship.
  #The proper way to set spouse is by setting `relationships_attributes` instead.
  def spouse= consumer_or_attr_hash
    SystemMailer.deprecated_method_warning(self) rescue nil
    spouse_rel_type=Enum::RelationshipType.find_by_name('spouse/domestic partner')
    spouse_consumer=Consumer.create({brand_id: self.brand_id, agent_id: self.agent_id}.merge(consumer_or_attr_hash)) if consumer_or_attr_hash.is_a?(Hash)
    existing_spouse_relationship=Crm::ConsumerRelationship.where(primary_insured_id:id, stakeholder_id:spouse_consumer.id, relationship_type_id: spouse_rel_type.id )
    if existing_spouse_relationship.empty?
      Crm::ConsumerRelationship.create(primary_insured_id:self, stakeholder_id:spouse_consumer.id, relationship_type_id: spouse_rel_type )
    end
  end

  def household_income
    financial_info.try(:income_total).to_i + (spouse.try(:financial_info).try(:income_total)||financial_info.try(:spouse_income)).to_i
  end

  #The default serialization is used by the `show` action.
  #The alternate serialization options are used by the `create` action.
  def default_serialization
    output_obj=self.as_json(
      include:[:phones, :emails, :addresses, :webs, :financial_info, :staff_assignment],
      except:[#exclude encrypted cells.
        :encrypted_ssn,
        :encrypted_dln,
        :encrypted_eft_bank_name,:encrypted_eft_bank_name_iv,
        :encrypted_eft_routing_num,:encrypted_eft_routing_num_iv,
        :encrypted_eft_account_num,:encrypted_eft_account_num_iv
      ],
      methods:[
        :short_id, :person_type, :dln, :ssn,
        :brand_name, :company, :warnings,
        :attachments_count,
        :recordings_count,
        :opportunities_count,
        :pending_policies_count,
        :placed_policies_count,
        :related_count,
        :old_tasks_count,
        :primary_contact_name,
      ]
    )
    #For now, this is the easiest way to ensure all the delegated attributes are included.
    output_obj[:health_info]=health_info.as_json
    output_obj
  end

  def short_serialization
    output_obj=self.as_json(
      only:[
            :id,
            :active_or_enabled,
            :entity_type_id,
            :full_name,
            :title,
            :company,
            :birth_or_trust_date,
            :gender,
            :tenant_id,
            :trustee_name,
            :state_id,
            :preferred_contact_time,
            :preferred_contact_method_id,
            :primary_address_id,
            :primary_phone_id,
            :primary_email_id,
      ],
      methods: [:dln, :ssn, :feet, :inches, :weight, :tobacco, :warnings],
      include:[:addresses, :emails, :phones, :webs]
    )
  end

  def medium_serialization
    output_obj=default_serialization
    output_obj[:cases]=self.cases.as_json
    output_obj
  end

  # Makes JSON-acceptable Hash of self and lower associations, suitable for dumping to backup file
  def detailed_serialization
    output_obj=default_serialization
    output_obj[:notes]=self.notes.as_json
    output_obj[:cases]=self.cases.as_json(
      include:[
        :notes,:attachments,:stakeholder_relationships, :smm_status,
        :docusign_case_data, :staff_assignment,
        quoting_details:{except:[:annual_premium,:monthly_premium,:quarterly_premium,:semiannual_premium]},
        statuses:{include: :tasks}
      ]
    )
    output_obj
  end
  alias_method :as_json_debug, :detailed_serialization


  #This method returns an array with two subarrays, holding basic contact info, and strings describing relationships.
  def suggested_relations
    others_in_brand=self.class.
      where('brand_id=? AND consumers.id!=?', self.brand_id, self.id)
    contact_methods_to_preload=[]

    suggestions=HashWithIndifferentAccess.new#will use consumers as keys, arrays of strings as values

    add_suggestion=Proc.new do |c, desc|
      suggestions[c.id]||={
        id:c.id, name:c.name,
        reasons_for_suggestion:[],
      }
      suggestions[c.id][:reasons_for_suggestion]<<desc
    end

    email_values=self.emails.map(&:value).compact
    phone_values=self.phones.map(&:value).compact
    street_values=self.addresses.map(&:street).compact

    if email_values.present?
      contact_methods_to_preload<<:emails
      others_in_brand=others_in_brand.where('primary_email_id IS NOT NULL')
    end
    if phone_values.present?
      contact_methods_to_preload<<:phones
      others_in_brand=others_in_brand.where('primary_phone_id IS NOT NULL')
    end
    if street_values.present?
      contact_methods_to_preload<<:addresses
      others_in_brand=others_in_brand.where('primary_address_id IS NOT NULL')
    end

    ids_of_others_in_brand=others_in_brand.pluck(:id)

    #The groups reduce number of round trips to db server,
    #length of queries for the preloaded tables,
    #and number of records simultaneously in memory.
    ids_of_others_in_brand.in_groups_of(200) do |g|
      g.compact!
      others=self.class.where(id:g).
        select([:id,:full_name,:company,:primary_email_id,:primary_phone_id,:primary_address_id]).
        preload(contact_methods_to_preload)
      others.each do |other|
        if email_values.present?
          intersection=(other.emails.map(&:value) & email_values)
          if intersection.present?
            add_suggestion.call(other,"matching #{'email'.pluralize(intersection.length)} (#{intersection.join(',')})")
          end
        end
        if phone_values.present?
          intersection=(other.phones.map(&:value) & phone_values)
          if intersection.present?
            add_suggestion.call(other,"matching #{'phone'.pluralize(intersection.length)} (#{intersection.join(',')})")
          end
        end
        if street_values.present?
          intersection=(other.addresses.map(&:street) & street_values)
          if intersection.present?
            add_suggestion.call(other,"matching #{'street'.pluralize(intersection.length)} (#{intersection.join(',')})")
          end
        end
        if self.company.present? && other.company.present? && other.company==self.company
          add_suggestion.call(other,"matching company")
        end
      end
    end

    suggestions.values
  end

  def related_count
    relationships_w_self_as_primary_count+relationships_w_self_as_related_count
  end

  def all_relationships_w_details_as_json
    list1=self.relationships_w_self_as_primary.
      preload( stakeholder:[:phones,:emails]).
      as_json(include:{stakeholder:{only:[:id,:full_name],methods:[:primary_email,:primary_phone]}})
    list2=self.relationships_w_self_as_related.
      preload( primary_insured:[:phones,:emails]).
      as_json(include:{primary_insured:{only:[:id,:full_name],methods:[:primary_email,:primary_phone]}})
    list1 + list2
  end

  def product_type_name
    self.product_type.try :name
  end

  def product_type_name= name
    self.product_type = Enum::ProductType.find_by_name(name)
  end

  pseudo_date_accessor :relationship_to_agent_start, prev: :years_of_agent_relationship

  def birth_state_abbrev
    self.birth_state.abbrev unless self.birth_state.nil?
  end

  def birth_state_name= value
    self.birth_state = value ? Enum::State.find_by_name(value) || Enum::State.find_by_abbrev(value) : nil
  end

  def dl_state_abbrev
    self.dl_state.abbrev unless self.dl_state.nil?
  end

  def dl_state_name= value
    self.dl_state = value ? Enum::State.find_by_name(value) || Enum::State.find_by_abbrev(value)  : nil
  end

  def marital_status_name
    self.marital_status.name unless self.marital_status.nil?
  end

  def marital_status_name= value
    self.marital_status = value ? Enum::MaritalStatus.find_by_name(value) : nil
  end

  # Can be used when duplicate clients are found. Attributes & associations of
  # +other_consumer+ overwrite attributes and associations' attributes of self.
  # Return other_consumer
  def merge_to other_consumer
    def xferable_attrs rec, *except
      if rec.is_a? ActiveRecord::Base
        attrs = rec.attributes
        attrs.delete('id')
        except.each{|field| attrs.delete(field.to_s) }
        attrs
      else
        rec.to_a.map{|obj| xferable_attrs(obj, *except) }
      end
    end
    other_consumer.attributes                = attributes
    other_consumer.health_info_attributes    = xferable_attrs health_info,    :consumer_id
    other_consumer.financial_info_attributes = xferable_attrs financial_info, :consumer_id
    other_consumer.phones_attributes         = xferable_attrs phones,         :contactable_type,:contactable_id
    other_consumer.emails_attributes         = xferable_attrs emails,         :contactable_type,:contactable_id
    other_consumer.addresses_attributes      = xferable_attrs addresses,      :contactable_type,:contactable_id
    other_consumer.opportunities_attributes  = xferable_attrs opportunities,  :consumer_id
    other_consumer.cases_attributes          = xferable_attrs cases,          :consumer_id
    other_consumer.notes_attributes          = xferable_attrs notes,          :consumer_id
    return other_consumer
  end

  def citizenship_name
    self.citizenship.try(:name)
  end

  def staff_assignment
    association(:staff_assignment).target || agent.try(:staff_assignment)
  end

  def get_nearest_staff_assignment role_name
    sa  =staff_assignment.try(role_name)
    role_id=Enum::UsageRole.id role_name.to_s.humanize.downcase
    user_id=Usage::StaffAssignment.new.user_id_for_role_id role_id
    sa||=User.find_by_id(user_id)
  end

  def case_manager
    get_nearest_staff_assignment :case_manager
  end

  #Creates a query parameter string for appending to the end of a url.
  #Will be used as a dynamic field for marketing templates,
  #allowing users to pass consumer details to an embedded quoter on an outside site.
  def details_for_quoter as_url_params=true
    #prioritize details that are earlier in the sales process, and more recently created
    recent_details=self.quoting_details.not_closed.order('sales_stage_id DESC, id ASC').last
    attrs={
      io_consumer_id: self.id,
      state_id:     nil,
      birth_day:    nil, birth_month:  nil,
      birth_year:   nil, height:       nil,
      weight:       self.weight,
      gender:       self.gender_string[0].downcase,
      face_amount:  recent_details.try(:face_amount),
      duration_id:  recent_details.try(:duration_id),
      health_class: recent_details.try(:health_class).try(:value),
    }

    attrs.each{|key, value| attrs[key]||= self.send(key) if self.respond_to?(key) }
    attrs[:height]||="#{health_info.try(:feet).to_i}_#{health_info.try(:inches).to_i}"

    attrs[:birth_day  ]= attrs[:birth_day  ].present? ? ('%02i' % attrs[:birth_day  ]) : nil
    attrs[:birth_month]= attrs[:birth_month].present? ? ('%02i' % attrs[:birth_month]) : nil

    as_url_params ? "?#{attrs.to_param}" : attrs
  end

  #We want to phase out the concept of "Opportunity" records.
  #Later, we'll change this method to actually save policy records with the opportunity sales stage instead.
  #At the same time, we'll need to change any logic that references opportunities, like `saved_quote_list`.
  #Still later, we'll change existing opportunities in the database to policies for uniformity.
  alias_method :orig_opportunities_attributes=, :opportunities_attributes=
  def opportunities_attributes= array_of_attrs
    SystemMailer.deprecated_method_warning self rescue nil
    self.orig_opportunities_attributes= array_of_attrs
  end

  def saved_quotes
    self.cases.joins(:status).where('crm_statuses.status_type_id': Crm::StatusType::PRESETS[:saved_quote][:id])
  end

  #Renders markup for this consumer's saved quotes.
  def saved_quote_list
    return '<br />(There are no saved quotes to show.)<br />' if self.saved_quotes.blank?

    output=%Q(
      <table width="" height="">
    )
    self.saved_quotes.each do |q|
      url=APP_CONFIG['base_url']+'/quoting/quotes/new'
      url+= "?requestType=%s&productLine=%s&consumer_id=%d&duration_id=%d&face_amount=%d&premium_mode_id=%d&health_class_id=%d" % [
            'APPLICATION', q.product_type_id||1, self.id, q.duration_id||-1, q.face_amount||-1,
            q.premium_mode_id||0, q.health_class_id||-1
            ]
      carrier=q.current_details.carrier
      output+=%Q(
        <tr><td>
          <div style="border: 3px solid #4D5F7C; border-radius: 10px; padding: 8px; margin: 4px;">
      )
      if carrier.try(:sprite_offset).present?
        output+=%Q(
            <img src="https://pinney.insureio.com/carrier_logos/#{carrier.sprite_offset}.png" height="60p" width="160" alt="#{carrier.name}" />
        )
      else
        output+=%Q(
            Carrier: #{carrier.try(:name)}<br />
        )
      end
      output+=%Q(
            Coverage: $#{q.face_amount}<br />
            Term: #{Enum::TliDurationOption.find(q.duration_id.to_i).name }
            Premium: #{ActionController::Base.helpers.number_to_currency(q.monthly_premium)}/mo #{ActionController::Base.helpers.number_to_currency(q.annual_premium)}/year<br />
            <a href="#{url}?opp_id=#{q.id}" style="padding: 4px 12px; border-radius:4px; line-height: 30px; text-align:center; text-decoration:none; color:#ffffff; background-color:#4D5F7C; background-image: linear-gradient(to bottom, #0088cc, #0044cc); background-repeat: repeat-x;">Apply</a><br />
          </div>
        </td></tr>
      )
    end
    output+=%Q(
      </table>
    )
    output
  end

  FACTORS_REQUIRING_UNDERWRITER=[
    [:foreign_travel,     'Foreign travel'],
    [:criminal,           'Criminal history'],
    [:hazardous_avocation,'A hazardous avocation'],
    [:bankruptcy,         'Bankruptcy'],
  ].freeze

private

  def _accessible? user_or_id, edit_or_view
    if sess_cons_id = self.class.ltd_access_id
      return false if sess_cons_id != id && persisted?
    end
    super
  end

  def _supply_needed_association association_name
    assoc = association(association_name)
    unless assoc.reader
      assoc.build
      save if persisted?
    end
    assoc.reader
  end

  def apply_conditional_defaults
    if brand_id == Brand::NMB_ID
      self.preferred_contact_method_id = 1 # Phone
      self.cases.each do |kase|
        kase.bind = false if kase.bind.nil?
      end
    end
  end

  #Is triggered by the flag `will_also_apply`.
  def create_a_lead
    return true unless cases.count==0 && opportunities.count==0
    self.opportunities.create user_id:self.agent_id
  end

  #This method runs as a validation.
  #`@unkown_keys` would be set from caught `ActiveRecord::UnknownAttributeError`s.
  def no_attempted_setting_of_unkown_keys
    return true if @unkown_keys.blank?
    error_msg = "Keys #{@unkown_keys.map(&:inspect).join(',')} are not usable in the context provided. "+
      "Please refer to the documentation for a listing of accepted keys for each object type."
    errors.add(:base, error_msg)
    return false
  end

  # before_save callback
  # Move values for lead_type, referrer, source from tags to their separate fields.
  # Update tracking totals.
  def update_tracking_tables
    fields_that_used_to_be_tags=[:lead_type, :referrer, :source, :ip, :ip_address, 'quoter url', :quoter_url]
    fields_that_used_to_be_tags.each do |tag_key|
      tags.each do |tag|
        if tag =~ /^#{tag_key}[=-]/
          tags.delete(tag)
          tag_value = tag.sub /^#{tag_key}[=-]/, ''
          column_value_passed_in_as_a_tag tag_key, tag_value
        end
      end
    end

    return unless brand_id && agent_id#other validations will catch this.

    new_lead_type     =Crm::LeadType.find_or_create_by(text: self.lead_type) if self.lead_type_changed? && self.lead_type.present?
    new_referrer      =Crm::Referrer.find_or_create_by(text: self.referrer)  if self.referrer_changed?  && self.referrer.present?
    new_source        =Crm::Source.find_or_create_by(text: self.source)      if (source_changed? || agent_id_changed? || brand_id_changed?) && self.source.present?

    begin
      brand.lead_types         << new_lead_type unless new_lead_type.nil? || brand.lead_types.map(&:text).include?(         self.lead_type)
      brand.referrers          << new_referrer  unless new_referrer.nil?  || brand.referrers.map(&:text).include?(          self.referrer)
      agent.lead_sources       << new_source    unless new_source.nil?    || agent.lead_sources.map(&:text).include?(       self.source)
      brand.owner.lead_sources << new_source    unless new_source.nil?    || brand.owner.lead_sources.map(&:text).include?( self.source)
    rescue => ex
      nil#Do nothing. Not important.
    end

    return true
  end

  def note_if_agent_was_updated
    if self.agent_id_changed? and self.agent_id_was

      old_agent=User.find(self.agent_id_was).full_name
      new_agent=User.find(self.agent_id).full_name

      Note.create(text:"Consumer reassigned from Agent #{old_agent} (#{self.agent_id_was}) to Agent #{new_agent} (#{self.agent_id})",
                  notable:self,
                  creator_id:self.agent_id,
                  note_type_id:Enum::NoteType.find_by_name('transfer to AI').id,
                  confidential:0)
    end
  end

  def column_value_passed_in_as_a_tag key, value
    SystemMailer.deprecated_method_warning(self) rescue nil
    field = key.to_s.gsub(/\s/,'_')
    send(:"#{field}=", value) if value.present?
  end

  def update_case_manager_name
    if staff_assignment.nil? || (staff_assignment.nil? && agent_id_changed?)
      case_manager = staff_assignment.try(:user_id_for_role_id, Enum::UsageRole.id('case_manager')) || agent.try(:staff_assignment).try(:user_id_for_role_id, Enum::UsageRole.id('case_manager'))
      self.case_manager_name = case_manager.try(:full_name)
    end
    return true
  end

  def update_personal_completed
    personal_fields=%w[first_name last_name middle_name marital_status_id dln ssn citizenship_id birth_country birth_state_id note]
    self.personal_completed ||= personal_fields.all?{|f| self[f].present? }
    return true # if return is falsey, save will not complete
  end

  def handle_marketing_opt_out
    if opted_out_of_marketing_changed? && self.opted_out_of_marketing==true
      #delete inactive ones that have been queued (have not started yet)
      #no callbacks need be fired, since no tasks have been created
      self.subscriptions.where('(active IS NULL OR active=false) AND queue_num IS NOT NULL').destroy_all
      #set active ones to inactive and remove their queue_num (indicates they are completed)
      #should possibly trigger callbacks, to stop any further tasks from firing
      self.subscriptions.where('active IS TRUE').each{|s| s.update_attributes(active: false, queue_num: nil) }
    end
    return true
  end
end
