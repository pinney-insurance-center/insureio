# When working with 3rd-parties (e.g. RTT, IXN), we often have one-off fields
# which it behooves us to store. But these third-party fields affect such a
# small fraction of our total records, that they don't merit the inclusion of
# new columns in existing db tables. This is a generic 'extension' class which
# can be attached to a Case, Consumer, Quote, or whatever.
class Extension < ActiveRecord::Base
  belongs_to :origin, polymorphic: true, inverse_of: :extension

  serialize :data, HashWithIndifferentAccess

  delegate :[], to: :data

  # Prevent creation of blank Extension records
  before_create -> { false }, if: -> { data.blank? }
end
