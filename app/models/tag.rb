class Tag < ActiveRecord::Base
  delegate :to_s, to: :value

  belongs_to_enum :tenant
  belongs_to :person, polymorphic: true
  belongs_to :owner, class_name: :User

  validates :tenant, presence: true
  validates :person, presence: true
  validates :value, presence: true
  validates :value, uniqueness: { scope: [:tenant_id, :person_id, :person_type, :owner_id] }

  before_validation :copy_data_from_person

  def self.viewable_by user
    if Thread.current[:current_user_acts_across_tenancies]
      Tag.all
    else
      Tag.where(tenant_id: user.tenant_id)
    end
  end

  private

  def copy_data_from_person
    self.tenant_id ||= person&.tenant_id
    self.owner_id ||= person&.owner_id
  end
end