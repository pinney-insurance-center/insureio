class Address < ActiveRecord::Base

  belongs_to      :contactable, polymorphic:true
  belongs_to_enum :address_type, class_name: "Enum::AddressType"
  belongs_to_enum :state

  after_destroy   :update_contactable_after_destroy
  after_destroy   :update_tasks
  after_save      :update_contactable
  after_update    :update_tasks, if: :changed?

  alias_attribute :type_id, :address_type_id
  alias_attribute :person,  :contactable
  alias_attribute :owner,   :contactable

  delegate        :abbrev, to: :state, prefix: true, allow_nil:true

  def matches? other
    other.is_a?(Address) && [:street, :city, :state_id, :zip, :address_type_id].all?{|sym| other[sym] == self[sym] }
  end

  def update_tasks
    #only update tasks if address is primary, or we're deleting an address
    return unless contactable && (contactable.primary_address_id==id || contactable.primary_address_id.nil?)
    return unless contactable.respond_to? :tasks
    offset = contactable.primary_address_id.nil? ? 0 : contactable.tz_offset
    contactable.tasks.update_all(tz_offset:offset) if offset
    contactable.tasks.update_all(state_id:state_id) if state_id
  end

  def to_s
    "#{street}\n#{city}, #{state.try(:abbrev)} #{zip}"
  end
  alias_method :value,     :to_s

  def street= value
    return if value.blank?
    value=value.gsub(/[\u0250-\ue007<>]/,'')#remove non-latin and <> characters.
    self[:street]=value
  end

  def city= value
    return if value.blank?
    value=value.gsub(/[\u0250-\ue007<>]/,'')#remove non-latin and <> characters.
    self[:city]=value
  end

  def zip= value
    return if value.blank?
    value=value.to_s.gsub(/[^a-zA-Z\d\-]/,'')#remove all but letters, digits, and hyphens.
    self[:zip]=value
  end

  #DEPRECATED
  def value= string
    SystemMailer.deprecated_method_warning(contactable) rescue nil
    return self.street = "" if string.blank?
    string.gsub!(/[\r\n]+/,"\n")
    string.strip!
    line1 = string.match( /(.*)\n.*\z/m ) # \n delim
    line1 ||= string.match( /(.*),.*,.*\z/m ) # comma delim, with city
    line1 ||= string.match( /(.*),.*\z/m ) # comma delim w/out city
    line2 = string.match( /(([\w ]+)[, ]+)? *(\w{2})((?: +)\d{5}(-\d{4})?)?\z/m )
    if line1
      self.street = line1[1].strip
    end
    if line2
      self.city   = line2[2].try :strip
      self.state  = Enum::State.find_by_abbrev line2[3].strip
      self.zip    = line2[4].try :strip
    end
    unless line1 || line2
      self.street =string.split("\n")[0...-1].join("\n")
      self.city   ||=string.match(/\n+(.+)(\s+[a-zA-Z]{2}(\s+[0-9-]{5,})?)?\s*$/).try(:[], 1).try(:gsub,',','')
      abbr        =string.match(/\n+(?:.+\s)([a-zA-Z]{2}(-[a-zA-Z]{1,2})?)(\s[^a-zA-Z]*)?$/).try(:[], 1)
      self.state  ||=Enum::State.find_by_abbrev abbr
      self.zip    ||=string.match(/\s([0-9]{5}(-[0-9]+)?)\s*$/).try(:[], 1)
    end
  end

  def type_name= name
    self.address_type_id = Enum::AddressType.id(name)
  end


  # Use Google API and timezone gem to determine timezone, based on address
  def timezone
    if zip.present?
      ActiveSupport::TimeZone.new ActiveSupport::TimeZone.find_by_zipcode(zip) rescue nil
    end
  end

  def self.parse string
    new value:string
  end

  def state_abbreviation= abbrev_str
    self.state_id= Enum::State.all.find{|s| s.abbrev.downcase==abbrev_str.downcase }&.id
  end
  alias_method :state_abbrev=, :state_abbreviation=

  def state_name= name_str
    self.state_id= Enum::State.all.select{|s| s.name==name_str }.first.try(:id)
  end

private

  def update_contactable
    if contactable && contactable.primary_address_id.blank?
      contactable.update_column :primary_address_id, id
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

  def update_contactable_after_destroy
    if contactable && contactable.primary_address_id==id
      contactable.update_column :primary_address_id, contactable.addresses.first.try(:id)
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end
end
