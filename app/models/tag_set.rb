# A set of Strings
class TagSet < Set
  # Recurse if necessary. Return nil, string, or an array of arrays or strings
  def self.format_tag input
    if input.is_a? Hash
      ActiveSupport::Deprecation.warn('Use of Hashes for tags is deprecated. Please provide Strings or a String Array')
      input = input.with_indifferent_access
      fmt1 = [input.delete('key'), input.delete('value')].select(&:present?).join('-')
      fmt2s = input.map do |k,v|
        k = nil if k =~ /^(tag|key|value)\d+$/
        [k,v].select(&:present?).join('-')
      end
      return ([fmt1] + fmt2s).compact
    elsif input.is_a? Symbol
      return input.to_s
    elsif [String, NilClass].any? { |type| input.is_a? type }
      return input
    elsif [Array, Set].any? { |type| input.is_a? type }
      return input.map { |item| self.format_tag(item) }.select(&:present?)
    else
      raise ArgumentError.new "Illegal tag type #{input.class.name}"
    end
  end

  def initialize data=nil
    items = Array(TagSet.format_tag(data)).flatten.select(&:present?)
    super items
    pristine!
  end

  def add value
    deletions.delete?(value) || include?(value) || additions.add(value)
    super
    mark_taint!
  end
  alias_method :<<, :add

  def delete value
    additions.delete?(value) || include?(value) && deletions.add(value)
    super
    mark_taint!
  end

  def push *items
    TagSet.new(items).each do |item|
      add(item)
    end
  end

  def changed?
    !(additions.empty? && deletions.empty?)
  end

  #so that a human-readable list can be output easily for reporting.
  def list
    self.map(&:to_s).join(",\n")
  end

  def mirror_tag_set_as_tag_records person
    additions.each do |value|
      Tag.create owner_id: person.owner_id, person: person, value: value
    end
    deletions.each do |value|
      Tag.where(person: person, value: value).delete_all
    end
  end

private

  def additions
    @additions ||= Set.new
  end

  def deletions
    @deletions ||= Set.new
  end

  def mark_taint!
    if changed?
      taint
    else
      untaint
    end
  end

  def pristine!
    @deletions = Set.new
    @additions = Set.new
    untaint
  end
end