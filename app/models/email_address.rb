class EmailAddress < ActiveRecord::Base
  

  validates :value, email:true, presence:true
  
  belongs_to :contactable, polymorphic:true

  after_destroy :update_contactable_after_destroy
  after_save :update_contactable

  def matches? other
    other.is_a?(EmailAddress) && [:value].all?{|sym| other[sym] == self[sym] }
  end

  def value_present?
    !self.value.blank?
  end
  
  def to_s
    self.value || ''
  end

private

  def update_contactable
    if contactable && contactable.primary_email_id.blank?
      contactable.update_column :primary_email_id, id
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

  def update_contactable_after_destroy
    if contactable && contactable.primary_email_id==id
      contactable.update_column :primary_email_id, contactable.emails.first.try(:id)
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

end
