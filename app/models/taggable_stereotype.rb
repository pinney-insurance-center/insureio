module TaggableStereotype
  extend ActiveSupport::Concern

  included do
    extend Concerns::ModelHelpers::ClassMethods
    raise "Schema error for class #{name} with TaggableStereotype. Expected column 'tags'." unless has_col?('tags') or not table_exists?
    raise "Class #{name} with TaggableStereotype should _not_ have an association `:tags`. `:tags` is meant to refer to a serialized field on this model." unless reflect_on_association(:tags).nil?
    #This association is here for more convenient debugging and testing.
    #It is unlikely to be utilized in normal app usage.
    has_many :tag_records, class_name: "Tag", as: :person

    serialize  :tags, Coder.new
    delegate :list, to: :tags, prefix: true
    before_save ->{ @tag_set_with_change_data_preserved=tags if tags&.changed? }
    after_save :mirror_serialized_tags_as_tag_records
  end

  def tags
    write_attribute(:tags, TagSet.new(self[:tags])) unless self[:tags].is_a?(TagSet)
    self[:tags]
  end

  def tags= value
    existing = tags
    incoming = TagSet.new(value)
    #Perform comparison on arrays, to avoid accidentally modifying the actual `TagSet`s.
    deletions = existing.to_a - incoming.to_a
    additions = incoming.to_a - existing.to_a
    deletions.each { |value| existing.delete(value) }
    additions.each { |value| existing.add(value) }

    existing
  end

  alias_method :tags_attributes=, :tags=

protected

  def add_tags input
    tags.push input
    mirror_serialized_tags_as_tag_records if input.present? and persisted? and valid?
  end

  def mirror_serialized_tags_as_tag_records
    return unless @tag_set_with_change_data_preserved&.changed?
    @tag_set_with_change_data_preserved.mirror_tag_set_as_tag_records self
  end

  # This Coder class was introduced so as to handle both new and legacy tags
  # (which were serialized as Hashes)
  class Coder < ActiveRecord::Coders::YAMLColumn
    def load(yaml)
      TagSet.new(yaml.present? ? YAML::load(yaml) : nil)
    end

    def dump(obj)
      YAML.dump(obj.present? ? obj.to_a : nil)
    end
  end
end
