module Ahoy
  class Event < ActiveRecord::Base
    self.table_name = "ahoy_events"

    include ActionView::Helpers::DateHelper

    belongs_to      :visit
    belongs_to      :user,        polymorphic:true#this line is here to make Ahoy happy, since it referrs internally to user
    belongs_to      :owner,       class_name: "User"
    belongs_to      :brand,     class_name: "Brand"
    belongs_to      :message,     class_name: "Marketing::Email::Message"

    serialize       :properties, JSON

    alias_attribute :participant,      :user
    alias_attribute :participant_id,   :user_id
    alias_attribute :participant_type, :user_type

    scope           :newest_first, lambda { order('time DESC') }#because the ids are not sequential integers
    scope           :anonymous,    lambda { where(user_id:nil) }
    scope           :identifiable, lambda { where('user_id IS NOT NULL AND user_type IS NOT NULL') }
    scope           :this_month,   lambda { where('time>?',1.month.ago) }

    #A user should be able to view events for all brands of which they are members.
    #A super user should be able to view all events.
    scope :viewable_by, lambda { |user|
      s=if user.super?
        scoped
      else
        where(owner_id:user.id)
      end
      s.where('time>?',1.month.ago)
    }
    #DEPRECATED. USE `viewable_by`.
    scope :viewables, lambda {|user| viewable_by(user) }

    def as_json options={}
      options[:only]=[:time,:name, :properties,:id]
      options[:include]={
        visit:      {only:[:ip, :browser, :os, :device_type, :region, :city]},
        owner:      {only:[:id],methods:[:name,:short_id]},
        brand:    {only:[:id],methods:[:name,:short_id]},
        participant:{only:[:id],methods:[:name,:short_id]},
        message:    {only:[:id,:recipient],include:{
            template:{only:[:id,:name]},
            task:    {only:[:name,:created_at]},
            blast:   {only:[:id,:created_at]}
          }
        }
      }
      options[:methods]=[:participant_type]
      super options
    end

    def publish recursion=0
      return unless participant.present?
      $redis.publish(APP_CONFIG['socket']['redis_channel_for_socket'], {
        type: 'ahoy-event',
        recipients: ([owner_id]+brand.try(:member_ids).to_a+owner.try(:brands).to_a.map(&:member_ids).flatten).uniq,
        data: self
      }.to_json)
    rescue Redis::TimeoutError => ex
      if recursion < 1
        load 'config/initializers/redis.rb' # replace redis connection
        publish recursion+1
      else
        errors.add(:publish,"could not complete because redis timed out.")
        ExceptionNotifier.notify_exception(ex)
      end
    rescue Redis::CannotConnectError => ex
      %x( [ -x #{APP_CONFIG['redis_executable']} ] && #{APP_CONFIG['redis_executable']} stop && #{APP_CONFIG['redis_executable']} start )
      errors.add(:publish,"could not complete because it could not connect to redis.")
      ExceptionNotifier.notify_exception(ex)
    end

  end
end
