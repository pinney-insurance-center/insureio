class Ahoy::Store < ActiveRecord::Base #Ahoy::Stores::ActiveRecordStore

  # Ahoy::Event and Visit lack create/update callbacks because they should only be created/updated via this model.

  def track_visit(options)
    super do |visit|
      custom_attrs=[:participant, :participant_id, :participant_type, :owner, :owner_id, :brand, :brand_id]
      custom_attrs.each do |a|
        visit.send("#{a}=", options[a]) if options[a] && !visit.send(a)
      end
    end
  end

  def track_event(name, properties, options)
    super do |event|

      custom_attrs=[:participant, :participant_id, :participant_type, :owner, :owner_id, :brand, :brand_id, :message, :message_id]
      custom_attrs.each do |a|
        event.send("#{a}=", options[a]) if options[a] && !event.send(a)
      end

      # A visit that started out anonymous (and previous events for the same visit which happened anonymously),
      # can get a participant once it becomes associated with an event that sets a participant.
      # But also avoids overwriting a participant.
      # ie. If a consumer fills out a quoter for themselves, then fills it out for somone else,
      # keep the visit record associated with first participant).
      attrs_to_share=[:participant, :participant_id, :participant_type, :owner, :owner_id, :brand, :brand_id]
      attrs_to_share=attrs_to_share.reject{|a| event.send(a).present? }

      attrs_to_share.each do |a|
        event.visit.send("#{a}=", event.send(a) ) if !event.visit.send(a)
      end
      event.visit.save if event.visit.changed?
      event.visit.ahoy_events.anonymous.map do |e|
        attrs_to_share.each do |a|
          e.send("#{a}=", event.send(a) ) if !e.send(a)
        end
        e.save if e.changed?
      end

      event.publish
    end
  end
end
