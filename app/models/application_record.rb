class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Reflect on associations and copy their errors into this instance
  def load_errors_from_all_associations
    User.reflect_on_all_associations.each do |assoc|
      load_errors_from_association assoc.name
    end
  end

  # Reflect on one association and copy their errors into this instance
  def load_errors_from_association name
    Array(send name).each do |obj|
      unless obj.valid?
        obj.errors.full_messages.each do |msg|
          errors.add name, msg
          next
        end
      end
    end
  rescue ActiveModel::MissingAttributeError => ex # Raised on :owner_name_only and :parent_name_only
    nil # noop
  rescue StandardError => ex
    Rails.logger.warn 'Unable to reflect on association "%s": %s' % [assoc.name, ex.to_s]
  end
end
