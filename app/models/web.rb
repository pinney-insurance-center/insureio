class Web < ActiveRecord::Base
  

  belongs_to      :contactable, polymorphic:true
  belongs_to_enum :web_type, class_name: "Enum::WebType"

  validates :value, presence:true

  scope :of_type, lambda{|str|
    where(web_type_id: Enum::WebType.id(str))
  }

  def matches? other
    other.is_a?(Web) && [:value, :web_type_id].all?{|sym| other[sym] == self[sym] }
  end

  def to_s
    self.value || ''
  end

  def url
  	"#{self.web_type.try(:url_prefix)}#{self.value}"
  end

private

end
