class User < ApplicationRecord

  GENDER = { false => 'Female', true => "Male" }
  PARENT_ID_FOR_MOTORIST_GROUP=1092#Jon Chandler
  OWNER_ID_COLUMN=:parent_id

  #Apply AuthLogic functionality, including validations on login and password.
  acts_as_authentic do |u|
    #Specify crypto provider explicitly, as the default has changed.
    u.crypto_provider = Authlogic::CryptoProviders::Sha512
    #Do not apply authlogic validations if this is a recruit record.
    u.merge_validates_length_of_login_field_options(         unless: :recruit?)
    u.merge_validates_format_of_login_field_options(         allow_blank:true)
    u.validates_uniqueness_of_login_field_options(           allow_blank:true)
    u.merge_validates_confirmation_of_password_field_options(unless: :recruit?)
    u.merge_validates_length_of_password_field_options(      unless: :recruit?)
  end

  include ContactableStereotype
  include TaggableStereotype
  include Usage::UserScopes
  include Usage::HierarchyMethodsAndScopes
  include Usage::Permissions
  include Usage::PaymentRelatedMethods

  serialize :common_tags, Array

  attr_encrypted :api_key, key:APP_CONFIG['encryption_key']#non-unique key + unique iv = adequately secure

  after_save        :delayed_update_ascendant_associations
  after_save        :update_tasks
  before_create     -> { self.marketech_signature||=self.parent.try(:marketech_signature) },
                    -> { self.pinney_record=true if self.is_recruit && self.parent.try(:membership_id).to_i==Enum::Membership.id('Pinney') },
                    -> { self.parent_id||=tenant.default_parent_id }
  before_save       -> { self.tenant_id||=parent.tenant_id; }
  before_save       -> { self.agent_of_record_id=nil if self.agent_of_record_id==self.id }
  before_save       -> { reset_persistence_token }, if: -> { membership_id_changed? && self[:membership_id]==Enum::Membership.id('Disabled') }
  before_save       -> { reset_quoter_key }, if: :trigger_reset_quoter_key
  before_save       -> { reset_api_key    }, if: :trigger_reset_api_key
  before_create     -> { reset_quoter_key; reset_api_key }, unless: :recruit?
  before_validation :build_user_from_recruit,
                    :build_recruit_from_user,
                    :ensure_password,
                    :copy_login_from_email
  after_commit      -> { self.default_brand; },#creates a default brand if none was attached during user creation
                    :save_association_on_recruit,
                    :reassociate_l_and_c_from_recruit_to_user,
                    -> {
                      begin
                        NewUserWorker.perform_async(id) if (!(self.recruit?) && !skip_notification_email)
                      rescue => ex
                        ExceptionNotifier.notify_exception(ex)
                      end
                      }, on: :create
  before_save       :disable_if_recruit,
                    :set_updated_value_in_commission_level,
                    :deactivate_old_statuses,
                    :remove_payment_info_for_free_memberships
  after_update      :handle_marketing_opt_out


  attr_accessor     :skip_notification_email#to be used internally when mass uploading users.
  attr_accessor     :trigger_reset_api_key
  attr_accessor     :trigger_reset_quoter_key

  validates_presence_of   :user_account, message:'with that id does not exist.', if: :user_account_id
  validate                :user_account_must_not_be_a_recruit, if: :user_account_id
  validates               :login, email:true, if: ->{ self.login_changed? && recruit? }
  validates               :login, email:true, presence:true, uniqueness:true, if: ->{ self.login_changed? && !recruit? }
  validate                :cannot_create_circular_lineage, if: :parent_id_changed?
  validates_uniqueness_of :motorist_id, allow_nil:true
  validates_presence_of   :emails, message:'must be present in order to reset the account password if it gets lost',
    if: ->{new_record? && !recruit? }#For new records, disincluding recruit records linked to a user account record.

  # Associations
  belongs_to_enum :commission_level, class_name: "Enum::CommissionLevel"
  belongs_to_enum :membership,       class_name: "Enum::Membership"
  belongs_to_enum :tenant,           class_name: "Enum::Tenant"
  belongs_to_enum :broker_dealer,    class_name: "Enum::BrokerDealer"

  belongs_to      :primogen,                        class_name: 'User', :foreign_key => :primogen_id
  belongs_to      :aml_vendor,                      class_name: "Usage::AmlVendor"
  belongs_to      :manager,                         class_name: "User"
  belongs_to      :parent,                          class_name: "User"
  belongs_to      :parent_name_only,                ->{ select [:id,:full_name] }, class_name: "User", foreign_key: :parent_id
  belongs_to      :agent_of_record,                 class_name: "User"
  belongs_to      :sales_support_field_set,         class_name: "Usage::SalesSupportFieldSet"
  belongs_to      :support_group,                   class_name: 'User'
  belongs_to      :default_brand,                   class_name: 'Brand'
  belongs_to      :default_brand_name_only,         ->{ select [:id,:full_name] },      class_name: 'Brand', foreign_key: :default_brand_id
  belongs_to      :recruitment_brand,               class_name: 'Brand'
  belongs_to      :recruitment_brand_name_only,     ->{ select [:id,:full_name] },      class_name: 'Brand', foreign_key: :recruitment_brand_id
  belongs_to      :status,                          class_name: "Crm::Status"
  belongs_to      :user_account,                    class_name: "User",        inverse_of: :recruits, foreign_key: :user_account_id
  belongs_to      :initial_status_type,             class_name: "Crm::StatusType"

  has_many        :attachments,                       class_name: "Attachment",                        as: :person
  has_one         :photo_file,                  -> { where 'file_name LIKE "user_%_photo.%"' },        class_name: "Attachment", as: :person
  has_one         :signature_file,              -> { where 'file_name LIKE "user_%_signature.%"' },    class_name: "Attachment", as: :person
  has_many        :recordings,                        class_name: "PhoneRecording",                    as: :person
  has_many        :messages,                          class_name: "Marketing::Email::Message",         foreign_key: :user_id
  has_many        :snail_messages,                    class_name: "Marketing::Email::Message",         foreign_key: :user_id
  has_many        :snail_mail_messages,               class_name: 'Marketing::SnailMail::Message',     foreign_key: :sender_id, dependent: :destroy
  has_many        :email_messages,                    class_name: 'Marketing::Email::Message',         foreign_key: :sender_id, dependent: :destroy
  has_many        :message_media_messages,            class_name: "Marketing::MessageMedia::Message",  foreign_key: :sender_id
  has_many        :marketing_email_templates,         class_name: "Marketing::Email::Template",        foreign_key: :owner_id
  has_many        :marketing_snail_mail_templates,    class_name: "Marketing::SnailMail::Template",    foreign_key: :owner_id
  has_many        :marketing_message_media_templates, class_name: "Marketing::MessageMedia::Template", foreign_key: :owner_id
  has_many        :smtp_servers,                      class_name: "Marketing::Email::SmtpServer",      foreign_key: :owner_id

  has_many        :licenses,                     class_name: 'Usage::License',  inverse_of: :user
  has_many        :consumers,                    class_name: "Consumer",        foreign_key: :agent_id
  has_many        :contracts,                    class_name: 'Usage::Contract', inverse_of: :user
  has_many        :notes,                        class_name: "Note",            as: :notable
  has_many        :statuses,                     class_name: "Crm::Status",     as: :statusable
  has_many        :status_types,                 class_name: "Crm::StatusType"
  has_many        :owned_brands,                 class_name: "Brand",           foreign_key: :owner_id
  has_many        :children,                     class_name: "User",            foreign_key: :parent_id
  has_many        :lead_types,                   through: :brands
  has_many        :referrers,                    through: :brands
  has_many        :quoting_widgets,              class_name: "Quoting::Widget",            foreign_key: :user_id
  has_many        :user_rules,                   class_name: "LeadDistribution::UserRule", foreign_key: :user_id,  inverse_of: :user, dependent: :destroy
  has_many        :subscriptions,                class_name: 'Marketing::Subscription',    as: :person
  has_many        :campaigns,                    class_name: "Marketing::Campaign",        through: :subscriptions
  has_many        :recruits,                     class_name: "User",                       inverse_of: :user_account, foreign_key: :user_account_id
  has_many        :tasks,                        class_name: "Task",                       as: :person

  has_one         :security_answer,              class_name: "Usage::SecurityAnswer",      foreign_key: :user_id
  has_one         :docusign_cred,                class_name: "Usage::DocusignCred",        foreign_key: :user_id
  has_one         :staff_assignment,             class_name:'Usage::StaffAssignment',      as: :owner

  has_and_belongs_to_many :brands,       class_name: "Brand",  foreign_key: :user_id, join_table: 'brands_users'
  has_and_belongs_to_many :ascendants,   class_name: "User",   join_table: 'usage_ascendants_descendants', :association_foreign_key => :ascendant_id, :foreign_key => :descendant_id
  has_and_belongs_to_many :descendants,  class_name: "User",   join_table: 'usage_ascendants_descendants', :association_foreign_key => :descendant_id, :foreign_key => :ascendant_id
  has_and_belongs_to_many :lead_sources, ->{ uniq }, class_name:'Crm::Source', join_table:'crm_sources_users'

  #deprecated. use :user_rules association
  has_many :lead_distribution_rules, foreign_key: :user_id,
    class_name: "LeadDistribution::UserRule",
    inverse_of: :user,
    dependent: :destroy

  accepts_nested_attributes_for :status
  accepts_nested_attributes_for :recruits
  accepts_nested_attributes_for :statuses
  accepts_nested_attributes_for :licenses
  accepts_nested_attributes_for :contracts
  accepts_nested_attributes_for :notes

  delegate        :name,                    to: :parent, prefix: true, allow_nil:true
  delegate        :name,                    to: :tenant, prefix: true, allow_nil:true
  delegate        :name,                    to: :aml_vendor, prefix: true, allow_nil:true
  delegate        :name,      to: :agent_of_record, prefix: true, allow_nil:true
  delegate        :email,     to: :agent_of_record, prefix: true, allow_nil:true
  delegate        :phone,     to: :agent_of_record, prefix: true, allow_nil:true
  delegate        :signature, to: :agent_of_record, prefix: true, allow_nil:true
  delegate        :state, :zip, to: :primary_address, prefix: false, allow_nil:true


  alias_attribute :opted_out_of_marketing,:marketing_unsubscribe
  alias_attribute :opted_out_of_status,:status_unsubscribe
  alias_attribute :owner_id, OWNER_ID_COLUMN
  alias_attribute :payment_plan_id,       :membership_id
  alias_attribute :active_or_enabled?,    :active_or_enabled
  alias_attribute :enabled,               :active_or_enabled
  alias_attribute :active,                :active_or_enabled
  alias_attribute :agent,                 :parent
  alias_attribute :agent_id,              :parent_id
  alias_attribute :signature,             :signature_file

  # Retrieve a current_user and a contact from the access_code. Return nil if code is invalid
  def self.from_access_code access_code
    user_id, class_name, record_id, digest = Base64.decode64(access_code).split ':'
    user = User.unscoped.find_by id: user_id
    record = class_name.constantize.find(record_id) rescue nil # Record may be NilClass
    if user && user.access_digest(record) == digest
      [user, record]
    else
      [nil, nil]
    end
  end

  # Return an access code representing the contact and a user for
  # authentication. This is used instead of the numeric id as a URL query
  # string param to reference records associated with a message because a
  # match between two numeric ids may be brute-forced using timestamp-based
  # cues. `record` may be nil.
  def access_code record
    Base64.encode64 [id, record.class.name, record&.id, access_digest(record)].join ':'
  end

  def access_digest record
    Digest::SHA256.base64digest [id, record.class.name, record&.id, Rails.application.config.secret_token].join
  end

  def ascendant?(other_user)
    ascendant_ids.include?(other_user.id)
  end

  def can_log_in?
    enabled && membership_id != Enum::Membership.id('Disabled') && !recruit? && tos && !lapsed(true)
  end

  def copyright
    "&copy; #{Time.now.year}&nbsp;#{self.full_name}&nbsp; All rights reserved."
  end

  def photo_url
    f=photo_file
    if f
      #Use the `AttachmentsController#show` action,
      #which hides the secure AWS url.
      #The user's key allows authorized consumers to view the attachment.
      "/attachments/#{f.id}.#{f.extension}?key=#{quoter_key}"
    else
      "/photos/thumb/missing.png"
    end
  end
  alias_method :photo_thumb_url, :photo_url

  def signature_url
    f=signature_file
    if f
      #Use the `AttachmentsController#show` action,
      #which hides the secure AWS url.
      #The user's key allows authorized consumers to view the attachment.
      "/attachments/#{f.id}.#{f.extension}?key=#{quoter_key}"
    else
      nil
    end
  end
  alias_method :signature_thumb_url, :signature_url

  #The current front end uploader only sends png, but this is subject to change.
  def photo_data_url= data_url
    photo_file.destroy if photo_file.present?#We want to replace, not keep both.
    new_photo_file=DataUrlFile.new(data_url)
    attachment = self.attachments.create person:self, file_for_upload:new_photo_file, file_name:"user_#{id}_photo.#{new_photo_file.extension}"
  end

  def signature_data_url= data_url
    signature_file.destroy if signature_file.present?#We want to replace, not keep both.
    new_signature_file=DataUrlFile.new(data_url)
    attachment = self.attachments.create person:self, file_for_upload:new_signature_file, file_name:"user_#{id}_signature.#{new_signature_file.extension}"
  end

  # Override getter for agent_of_record
  alias_method :aor_assoc, :agent_of_record
  def agent_of_record
    if self[:agent_of_record_id].blank? || self[:agent_of_record_id]==id
      self
    else
      aor_assoc
    end
  end

  #DEPRECATED. USE `self.notes.create` OR `self.notes_attributes` DIRECTLY.
  #This is here temporarily in order to override the setter for the column `note`
  #because we do not display or make use of that column anywhere.
  def note= text
    SystemMailer.deprecated_method_warning rescue nil
    self.notes.new text:text
  end

  def aml_vendor_name= name
    self.aml_vendor=Usage::AmlVendor.where(name:name).first_or_create(name: name)
  end

  #Returns a string listing types of licenses that the user has.
  def license_types
    lines = [:corporate, :life, :health, :variable, :p_and_c]
    lics = self.licenses.select(lines).order('corporate DESC, life DESC, health DESC, variable DESC, p_and_c DESC')
    types = []
    lines.each do |line|
        types << line if lics.any?{ |lic| lic[line] }
    end
    types=types.map{|t| t.to_s.gsub('_and_','&').humanize.titleize}
    types.join(", ")
  end


  def ok_for_rtt_process?
    (can?(:motorists_rtt_process) && rtt_allowed_states.present?) ||
    can?(:motorists_rtt_process_details)
  end

  #Accessed from method `ok_for_rtt_process?`,
  #and from agent summary, and from `UsersController#show` action.
  def rtt_allowed_state_ids
    motorists_c_id=Carrier::ID_FOR_MOTORISTS
    motorists_contract=self.contracts.in_effect.where(carrier_id:motorists_c_id).first
    #single ampersand is ruby array intersection
    (motorists_contract.try(:state_ids)||[]) & Processing::Rtt::RTT_ALLOWED_STATE_IDS
  end

  def rtt_allowed_states
    Enum::State.ewhere(id:rtt_allowed_state_ids)
  end

  def user_rules_lead_types= values
    values = values.split(/\s*,\s*/) if values.is_a?(String)
    ActiveRecord::Base.transaction do
      values.each do |value|
        rule = self.user_rules.find_or_create_by_lead_type(value)
        rule.deleted = false;
        rule.save
      end
    end
    # Soft-delete tags which no longer exist
    self.user_rules.where('lead_type not in (?)', values).update_all(deleted:true)
  end

  def user_rules_lead_types
    (user_rules.enabled.try(:map, &:lead_type) || [])
  end

  def staff_assignment
    association(:staff_assignment).reader || create_staff_assignment(owner: self)
  end

  # Return an email address whose domain matches the domain of a given Profile's primary email address.
  # If no match found, fallback to user's primary email address.
  def brand_specific_fallback_primary_email brand
    # proc for extracting host from email address
    get_domain = lambda{|e| e.to_s.gsub(/^.*@/,'')}
    brand_domain = get_domain.call(brand.try(:email) || '')
    # loop through email addressses
    self.emails.each do |e|
      #return e if get_domain.call(e) == brand_domain
      if get_domain.call(e) == brand_domain
        return e.to_s
      end
    end
    primary_email.to_s
  end

  def brand_specific_fallback_primary_phone brand
    brand_first_ten_digits = brand.try(:phone).try(:value)
    self.phones.each do |p|
      return p if brand_first_ten_digits == p.value
    end
    primary_phone
  end

  def send_password_reset(email)
    reset_perishable_token!
    #Notifier.deliver_password_reset_instructions(self)
    UserMailer.password_reset(self, email).deliver
  end

  # Return array of state_ids for which this users is licensed
  def licensed_state_ids
    licenses.map{|l| l.state_id} || []
  end

  def membership_monthly_fee
    the_amount=membership.try(:pricing)||0
    raise "You shouldn't be seeking a fee for this membership level: #{membership_id}" if the_amount==0
    the_amount
  end

  # If membership is expired, it shall not be returned.
  def membership_id
    if !readonly? && lapsed?
      Enum::Membership.id('Disabled')
    else
      self[:membership_id]
    end
  end

  def lapsed(allow_grace=false)
    self[:membership_id].nil? ||
    self[:membership_id] == Enum::Membership.id('Disabled') ||
    Enum::Membership.find(self[:membership_id]).try(:pricing).to_f > 0 && !payment_due(allow_grace).try(:future?)
  end
  alias_method :lapsed?, :lapsed

  # Returns date that next payment must arrive to prevent lapse
  def payment_due(allow_grace=false)
    return Date.yesterday if self[:payment_due].nil?
    #In rare instances a user who once paid may be set to free.
    #In that case their account is valid, this method is still called,
    #and the user will have a Stripe customer id but no subscription id,
    #so we need to verify the presence of both fields before attempting to contact Stripe.
    return Date.yesterday unless self[:stripe_customer_id].present? && self[:stripe_subscription_id].present?
    if self[:payment_due].past?
      stripe_customer = Stripe::Customer.retrieve(self[:stripe_customer_id])
      stripe_subscription = stripe_customer.subscriptions.retrieve(self[:stripe_subscription_id])
      period_end_time = Time.at(stripe_subscription.current_period_end)
      self[:payment_due] = period_end_time.to_date
      save!
    end
    date = self[:payment_due]
    date += PAYMENT_GRACE_PERIOD.days if allow_grace
    date.to_date + 1
  end

  # Set payment_due 1 month from now.
  # This should never be called at the time of an upgrade. The user gets the
  # upgraded features for free for the time between now and their next
  # scheduled payment
  def update_payment_due!
    self[:payment_due] = Time.now + 1.month
    save!
  end

  def remove_payment_info_for_free_memberships
    return unless membership_id_changed? && (Enum::Membership.find(membership_id).pricing == 0)
    return unless stripe_subscribed?
    stripe_unsubscribe!
    if errors.present?
      errors.add :base, 'Failed to cancel stripe subscription.'
      return false
    end
  end

  def recruit?
    self.is_recruit
  end

  # Returns id. The purpose is so that functions that accept a User or id
  # can quickly get the id without any logic.
  def to_i
    self.id
  end

  def self.licensed_state_ids users=[], options={}
    arr = users.map{|u| u.licensed_state_ids}.flatten
    arr.uniq! unless options[:unique] == false
    arr.sort! unless options[:sort] ==  false
  end

  def self.licensed_state_ids users=[], options={}
    arr = users.map{|u| u.licensed_state_ids}.flatten
    arr.uniq! unless options[:unique] == false
    arr.sort! unless options[:sort] ==  false
  end

  def self.licensed_state_ids users=[], options={}
    arr = users.map{|u| u.licensed_state_ids}.flatten
    arr.uniq! unless options[:unique] == false
    arr.sort! unless options[:sort] ==  false
  end

  def send_recruitment_email(recipient_email,recipient_name)
    smtp = smtp_servers.first || ActionMailer::Base.smtp_settings
    UserMailer.invite_email(recipient_email, recipient_name, smtp, self).deliver
  rescue Net::SMTPFatalError => ex
    ExceptionNotifier.notify_exception(ex, data: smtp_servers.first&.inspect)
    errors.add :base, 'Email sending failed.'
    return false
  rescue Net::SMTPAuthenticationError => ex
    raise ::Marketing::SMTPAuthenticationError.new(ex, smtp, self)
    errors.add :base, 'Email authentication failed.'
    return false
  end

  def decrypt_ssn
    self.decrypt_confidential_data(self.ssn) unless self.try(:ssn).blank?
  end

  def decrypt_tin
    self.decrypt_confidential_data(self.tin) unless self.try(:tin).blank?
  end

  #Facilitate backward compatibility for records whose api keys are not yet encrypted.
  alias_method :decrypted_api_key, :api_key
  def api_key
    decrypted_api_key.present? ? decrypted_api_key : old_plaintext_api_key
  end

  # Return staff assignment users if staff assignment is not nil, other wise return current user
  def get_system_task_assignee
    unless self.staff_assignment.blank?
      return self.staff_assignment.get_staff.collect {|p| [ p.full_name, p.id ] }
    else
      return [self].collect {|p| [ p.full_name, p.id ] }
    end
  end

  def security_question_to_ask(que_attr)
    SecurityQuestion.ewhere(id: self.security_answer.try(que_attr)).first.try(:value)
  end

  def security_question_id(que_attr)
    self.security_answer.try(que_attr)
  end

  def license_info brand_id=default_brand_id
    l_nums=[]
    self.licenses.in_effect.each do |l|
      l_nums << "#{l.state.abbrev} Agent \##{l.number.ljust(10,' ')}"
    end
    brand=Brand.find_by_id(brand_id)
    company=brand.try(:company) || self.company
    name_and_company=self.company.present? ? "#{self.full_name} / #{company}" : self.full_name

    l_string ="#{name_and_company}, is a licensed life insurance agent. "
    l_string+="The following agent license numbers are provided for #{name_and_company} "
    l_string+="as required by state law:\n#{l_nums.join(', ')}\nCommercial use by others is prohibited by law. "
    l_string+='This site provides life insurance quotes. Each rate shown is a quote based on information provided '
    l_string+='by the carrier. No portion of this website may be copied, published, faxed, mailed or distributed '
    l_string+='in any manner for any purpose without prior written authorization of the owner.'
  end

  # Returns a scope for Profiles belonging to self or of which self is a member
  # If super? and strict, scope all Profiles
  def indexable_brands strict=false
    if super? and !strict
      Brand.where(enabled:true)
    else
      p_table = Brand.table_name
      ascendant_ids=self.ascendant_ids+[self.id]
      j_table = 'brands_users'
      Brand.where(enabled:true)
      .joins("join #{j_table} on #{j_table}.brand_id = #{p_table}.id")
      .where("ownership_id= ?
        OR (#{p_table}.owner_id IN (?) AND ownership_id= ?)
        OR #{p_table}.owner_id= ?
        OR #{j_table}.user_id = ?", Enum::Ownership.id('global'), ascendant_ids, Enum::Ownership.id('user and descendants'),id, id)
      .uniq
    end
  end

  def insurance_division_id
    #Simple, short, and likely hard to crack, as long as this algorithm is kept on the back end.
    ((id+2400).to_s(36)).upcase+(self.quoter_key.to_s[0..2])
  end

  def pinney?
    self[:membership_id] <= Enum::Membership.id('Pinney')
  end


  # Returns smtp config whose host matches +brand+ host. If none found, return first smtp config
  def branded_smtp_config brand
    # proc for extracting host from email address
    get_domain = lambda{|addr| addr.gsub(/^.*@/,'')}
    # get brand's domain
    brand_domain = get_domain.call( brand.try(:primary_email).try(:value) || '')
    # find matching smtp config
    self.smtp_servers.each do |config|
      return config if config.host == brand_domain
    end
    self.smtp_servers.first
  end

  def default_brand_name
    record=
      if association(:default_brand).loaded?
        default_brand
      else
        default_brand_name_only
      end
    record.try(:full_name)
  end

  alias_method :default_brand_assoc, :default_brand
  def default_brand
    return nil if self.recruit?
    return default_brand_assoc if default_brand_assoc.present?
    p||= owned_brands.first if owned_brands.present?
    p||= brands.first       if brands.present?
    p||= _build_brand_from_user
    self.default_brand=p
    #Because `after_commit, on: :create` still returns false for `persisted?`
    #and does not allow `save` or `update_column`, this will set the column:
    self.class.where(id:self.id).update_all(default_brand_id:default_brand_assoc.id)
    return p
  end

  alias_attribute :brand_id, :default_brand_id

  def default_brand_id
    return nil if self.recruit?
    self[:default_brand_id] || (self.id && default_brand.try(:id))
  end

  def _build_brand_from_user
    brand_attrs={
      name:name,
      company:self.company,
      tenant_id:self.tenant_id,
      owner_id:self.id,
      ownership_id:Enum::Ownership.id('user'),
      initial_status_type_id: self.initial_status_type_id,
    }
    new_brand=Brand.create(brand_attrs)
    [:address,:email,:phone].each do |x|#add `primary_x` associations.
      new_x=self.send("primary_#{x}").try(:dup)
      next unless new_x
      new_x.contactable=new_brand
      new_x.save
    end
    new_brand
  end

  #this attribute writer allows a new status to be created through update_attributes,
  #which keeps the logic out of the controller
  def status_type_id= type_id=nil
    if type_id.present? && type_id=type_id.to_i
      method= self.persisted? ? :create : :new
      self.status=Crm::Status.send method, status_type_id: type_id, statusable:self, created_by_id: Thread.current[:current_user_id]
    end
  end


  private

  def cannot_create_circular_lineage
    is_circular=(descendant_ids+[self.id]).include?(self.parent_id)
    errors.add(:parent_id,'is not allowed to create a circular lineage') if is_circular
  end

  #Ensure password, because it's better to create
  #a user with a secure password that no one knows
  #than for the users to enter an insecure password for expediency.
  #Eventually, we should enforce min length, etc.
  #But a big factor here is that it is not necessarily the user themselves creating the account.
  def ensure_password
    return unless self.new_record? && !changes.keys.include?('password')
    generated_pw=SecureRandom.base64(10)
    self.password||=generated_pw
    self.password_confirmation||=generated_pw
  end

  #This method should be called before saving a new user
  #and when the `trigger_reset_quoter_key` has been set.
  def reset_quoter_key
    self.quoter_key=SecureRandom.hex(16)
  end

  #This method should be called before saving a new user
  #and when the `trigger_reset_api_key` has been set.
  def reset_api_key
    self.api_key=SecureRandom.hex(16)
  end

  def user_account_must_not_be_a_recruit
    errors.add(:user_account, "must be a user, not another recruit.") if user_account.try(:recruit?)
    return user_account.try(:recruit?)
  end

  def build_user_from_recruit
    return true unless (self.new_record? && self.recruits.present?)
    recruit=recruits.first
    recruit_attrs_whitelist=%w[parent_id login full_name title company entity_type_id birth encrypted_ssn encrypted_tin assistant_name assistant_email assistant_phone relationship_manager_id time_zone docusign_id default_brand_id recruitment_brand_id]
    recruit_attrs_whitelist.each do |k|
      self.send("#{k}=",recruit.send(k)) unless self.send(k).present?
    end
    self.tags       =self.tags.merge(recruit.tags)
    self.common_tags=self.common_tags+recruit.common_tags
    if !self.password
      random_pw=(0...50).map { ('a'..'z').to_a[rand(26)] }.join
      self.attributes=self.attributes.merge({password: random_pw,password_confirmation: random_pw})
    end
    self.signature=recruit.signature if recruit.signature.present? && self.signature.blank?
    self.photo_file    =recruit.photo_file     if recruit.photo_file.present?     && self.photo_file.blank?
    #Logins should be unique, so remove the login from the recruit after adding it to the new user.
    recruit.update_column(:login,nil)
    #Preserve the recruit in an instance variable,
    #because as a has_many recruits association, it won't retain its recruit after save,
    #and we can't save this record's id on the recruit until this record is saved.
    @built_from_recruit||=recruit.reload

    recruit.addresses.each{|x| new_address=x.dup; self.addresses<< new_address }
    recruit.emails.each{|x|    new_email  =x.dup; self.emails   << new_email }
    recruit.phones.each{|x|    new_phone  =x.dup; self.phones   << new_phone }
    recruit.webs.each{|x|      new_web    =x.dup; self.webs     << new_web }
    self.emails.reject!{|e| e.value.blank?}
    self.phones.reject!{|p| p.value.blank?}
  end

  def save_association_on_recruit
    @built_from_recruit.update_attribute(:user_account_id,self.id) if defined?(@built_from_recruit)
  end

  def reassociate_l_and_c_from_recruit_to_user
    if defined?(@built_from_recruit)
      @built_from_recruit.licenses.update_all(user_id: self.id)
      @built_from_recruit.contracts.update_all(user_id: self.id)
    end
  end

  def build_recruit_from_user
    return true unless self.new_record? && self.user_account.present?
    user=user_account
    user_attrs_whitelist=%w[full_name title company entity_type_id birth encrypted_ssn encrypted_tin assistant_name assistant_email assistant_phone relationship_manager_id time_zone docusign_id]
    user_attrs_whitelist.each do |k|
      self.send("#{k}=",user.send(k)) unless self.send(k).present?
    end
    self.tags       =self.tags.merge(user.tags)
    self.common_tags=self.common_tags+user.common_tags

    self.signature = user.signature if user.signature.present? && self.signature.blank? rescue nil
    self.photo_file = user.photo_file     if user.photo_file.present?     && self.photo_file.blank? rescue nil

    self.is_recruit=true
    user.addresses.reject{|attrs|%w[street city state_id zip value].all?{ |x| attrs[x].blank?} }.
      each{|x| new_address=x.dup; self.addresses<< new_address }
    user.emails.reject{|attrs| attrs[:value].blank? }.each{|x|    new_email  =x.dup; self.emails   << new_email }
    user.phones.reject{|attrs| attrs[:value].blank? }.each{|x|    new_phone  =x.dup; self.phones   << new_phone }
    user.webs.each{|x| new_web =x.dup; self.webs     << new_web }
  end

  #If a user has role of recruit, they should not be able to login.
  def disable_if_recruit
    self.login=nil && self.password=nil if self.is_recruit
    return true
  end

  def set_updated_value_in_commission_level
    self.commission_level_updated_at = Date.today if self.commission_level_id_changed?
  end

  def handle_marketing_opt_out
    if opted_out_of_marketing_changed? && self.opted_out_of_marketing==true
      #delete inactive ones that have been queued (have not started yet)
      #no callbacks need be fired, since no tasks have been created
      self.subscriptions.where('(active IS NULL OR active=false) AND queue_num IS NOT NULL').destroy_all
      #set active ones to inactive and remove their queue_num (indicates they are completed)
      #should possibly trigger callbacks, to stop any further tasks from firing
      self.subscriptions.where('active IS TRUE').each{|s| s.update_attributes(active: false, queue_num: nil) }
    end
    return true
  end

  def deactivate_old_statuses
    if self.status_id_changed? && self.status_id.present? && self.status_id_was.present?
      self.statuses.where("id <> #{self.status_id}").each{|s| s.update_attributes(active:false)}
    end
    true
  end

  def copy_login_from_email
    return if !self.new_record?
    return if self.recruit?
    return if self.motorist_id.present?#motorist/bsb currently does not send emails in their SSO data
    self.login = emails.first&.value
  end



  SELF_EDITABLE_FIELDS = %w[
    anniversary
    assistant_address
    assistant_email
    assistant_name
    birth
    common_tags
    default_brand_id
    docusign_id
    first_name
    full_name
    initial_status_type_id
    last_name
    login
    middle_name
    name
    nickname
    password
    password_confirmation
    photo
    signature
    ssn
    tags
    time_zone
    tin
    title
    user_processes_own_business
    vici_id
  ].freeze

  #These should only be used internally by the back end,
  #and never output to the front end.
  FIELDS_TO_HIDE= %w[
    encrypted_api_key
    api_key
    old_plaintext_api_key
    crypted_password
    password_salt
    encrypted_ssn
    encrypted_tin
    perishable_token
    persistence_token
    single_access_token
    ].freeze

  PAYMENT_GRACE_PERIOD = 1

end
