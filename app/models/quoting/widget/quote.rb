class Quoting::Widget::Quote
	extend  ActiveModel::Naming
	extend  ActiveModel::Translation

	include ActiveModel::Validations
	include ActiveModel::Validations::Callbacks
	include ActiveModel::Conversion

	attr_accessor :state_id, :gender, :birthdate, :quoter

	validates :gender,    presence: true
	validates :birthdate,
	  presence: true,
	  format: { with: /\d{2}\/\d{2}\/\d{4}/} # match "03/14/1995"

	def female?
		gender == 'f'
	end
	def male?
		gender == 'm'
	end

	def dob
		return birthdate if birthdate && birthdate.is_a?(Date)
		Date.strptime(birthdate, '%m/%d/%Y') if birthdate
	end

	def age
		# http://stackoverflow.com/questions/819263/get-persons-age-in-ruby
		_dob = dob
		now = Time.now.utc.to_date
		now.year - _dob.year - ((now.month > _dob.month || (now.month == _dob.month && now.day >= _dob.day)) ? 0 : 1)
	end

	alias_method :orig_state_id=, :state_id=
	def state_id= value
		self.orig_state_id=value.to_i
	end

	def state
		Enum::State.find(state_id)
	end
	def state= s_id
	  self.state_id =
	    if s_id.is_a? Enum::State
	      s_id.id
	    else
	      s_id.to_i
	    end
	end
	def state_abbrev
		state.try(:abbrev)
	end

	# Should return an array of ["Class Name", "ClassCode"] arrays
	# for populating health class options
	def self.health_class_options
		raise "Implement in a subclass"
	end

	# Returns an object to call .process on, passing in self
	def self.result_api
		raise "Implement this in a subclass"
	end

	def results(force_reload = false)
		@results = nil if force_reload
		@results ||= self.class.result_api.process(self)
	end

	def initialize attrs = {}
		if self.class == Quoting::Widget::Quote
			raise "Abstract class"
		end

		attrs.each_pair do |key, value|
			send("#{key}=", value) if respond_to?("#{key}=")
		end

		if respond_to?(:after_initialize)
			self.after_initialize
		end
	end

	def after_initialize
		# Initialize quote defaults in a subclass
	end

	def to_partial_dir
	  File.dirname(to_partial_path)
	end

	# ActiveRecord support
	def persisted?
		false
	end
	def self.pluralize_table_names
		false
	end
end
