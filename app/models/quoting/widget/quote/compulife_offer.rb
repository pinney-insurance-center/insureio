class Quoting::Widget::Quote::CompulifeOffer
	include ActiveModel::Conversion
	attr_accessor(
		:carrier,
		:company_name,
		:product_code,
		:product_name,
		:health_code,
		:health_category,
		:period,
		:face_amount,
		:monthly_premium,
		:annual_premium,
		:policy_fee,
	)

	def company_name
		carrier.name
	end
	def compulife_code
		carrier.compulife_code
	end
	def pinney_rating?
		pinney_rating.blank?
	end

	def period_text
		Quoting::Widget::Quote::Life::Term.period_options.detect { |k,v|
		  v.to_s == self.period.to_s
		}.try(:first)
  end

  def to_json
    as_json.merge("period_text" => period_text).to_json
  end

	def initialize attrs
		attrs.each_pair do |key, value|
			send("#{key}=", value) if respond_to?("#{key}=")
		end

		self.carrier || raise("Must have a carrier")
		self.product_name || raise("Must have a product_name")
	end
end
