class Quoting::Widget::Quote::Ltc < Quoting::Widget::Quote
	attr_accessor(
		:face_amount,
		:payout_period,
		:health_class,
		:premium_mode,
		:married,
		:filing_jointly,
		:benefit_period,
		:inflation_protection,
		:elimination_period,
	)
	attr_reader :joint

	validates :face_amount,          presence: true
	validates :payout_period,        presence: true
	validates :health_class,         presence: true
	validates :premium_mode,         presence: true

	validates :benefit_period,       presence: true
	validates :inflation_protection, presence: true
	validates :elimination_period,   presence: true

	def after_initialize
		super

		if self.filing_jointly?
			@joint = Quoting::Widget::Quote::LtcJoint.new(attrs[:joint_attributes])
		else
			@joint = nil
		end

		self.face_amount   ||= 300
		self.payout_period ||= 'D'
	end

	alias_method :married?, :married
	alias_method :filing_jointly?, :filing_jointly
	alias_method :joint?, :joint

	def self.result_api
		::Ebix
	end

	def self.health_class_options
		[
			['Preferred',      'PREF'],
			['Standard',       'STD'],
			['Substandard I',  'SUB1'],
			['Substandard II', 'SUB2']
		]
	end
	def self.premium_mode_options
		[
			['Annually',   'YEAR'],
			['Semi-annl.', 'SEMI'],
			['Quarterly',  'QTR'],
			['Monthly',    'MONTH']
		]
	end
	def self.benefit_period_options
		[
		  ['1 year',  '1NB'],
		  ['2 years', '2NB'],
		  ['3 years', '3NB'],
		  ['4 years', '4NB'],
		  ['5 years', '5NB'],
		  ['6 years', '6NB'],
		  ['7 years', '7NB'],
		  ['8 years', '8NB'],
		  ['10 years','10NB'],
		  ['12 years','12NB'],
		  ['Lifetime','LIFE']
		]
	end
	def self.inflation_protection_options
		[
		  ['None','NONE'],
		  ['Periodic','PERI'],
		  ['Simple','SIMP'],
		  ['Compound','COMP'],
		  ['3%-Compound','3COMP']
		]
	end
	def self.elimination_period_options
		[
			['0 days','0'],
			['30 days','30'],
			['60 days','60'],
			['90 days','90'],
			['180 days','180'],
			['365 days','365']
		]
	end

private
	before_validation :validate_joint, if: :joint?
	def validate_joint
		if joint.invalid?
			joint.errors.each do |err|
				self.errors.add(:joint, "#{err.first} #{err.last}")
			end
		end
	end
end
