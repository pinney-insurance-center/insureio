class Quoting::Widget::Quote::LtcJoint < Quoting::Widget::Quote
	attr_accessor(
		:shared_benefit,
		:waiver_of_premium
	)

	alias_method :waiver_of_premium?, :waiver_of_premium
	alias_method :shared_benefit?, :shared_benefit
end
