class Quoting::Widget::Quote::Life::Term < Quoting::Widget::Quote
	attr_accessor(
		:face_amount,
		:period,
		:feet,
		:inches,
		:weight,
		:health_class,
		:cigarettes_period,
		:cigarettes_per_day,
		:name,
		:phone,
		:email
	)

	validates :face_amount,       presence: true, numericality: true
	validates :period,            presence: true, numericality: true
	validates :inches,            presence: true, numericality: true
	validates :weight,            presence: true, numericality: true
	validates :cigarettes_period, presence: true

	def self.result_api
		::Compulife
	end

	def smoker?
		if self.cigarettes_period.nil?
			return false
		end
		self.cigarettes_period.to_i == 0
	end

	def self.health_class_options
		[
			['Best Class', 'PP'],
			['Preferred', 'P'],
			['Standard Plus','RP'],
			['Standard','R']
		]
	end
	def self.face_amount_options
		[].tap { |amounts|
			   25_000.step(  100_000,  25_000) {|x|amounts<<x}
			  150_000.step(1_000_000,  50_000) {|x|amounts<<x}
			1_100_000.step(2_000_000, 100_000) {|x|amounts<<x}
			2_250_000.step(5_000_000, 250_000) {|x|amounts<<x}
		}
	end

	def self.cigarettes_period_options
		[
			["Never",               -1],
			["Current User",         0],
			["Within the past year", 1],
			["Over 1 year ago",      2],
			["Over 2 years ago",     3],
			["Over 3 years ago",     4],
			["Over 5 years ago",     5],
			["Over 10 years ago",    7]
		]
	end
end
