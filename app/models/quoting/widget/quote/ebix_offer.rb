class Quoting::Widget::Quote::EbixOffer
	include ActiveModel::Conversion

	attr_accessor(
		:carrier,
		:product_name,
		:product_code,
		:premium,
		:applicant_premium,
		:spouse_premium,
		:jwop,
		:swop,
		:wop,
		:sb,
		:flex,
		:acl,
		:hwep,
		:nf,
		:rop,
		:rob,
		:part,
		:has_joint
	)

	alias_method :has_joint?, :has_joint

	def company_name
		carrier.name
	end
	def naic_code
		carrier.naic_code
	end

	def initialize attrs
		attrs.each_pair do |key, value|
			send("#{key}=", value) if respond_to?("#{key}=")
		end

		self.carrier || raise("Must have a carrier")
		self.product_name || raise("Must have a product_name")
	end
end
