module Quoting::Xrae
  PERSONAL_DISEASE_IDS = Set.new([
    1, # alcohol_abuse
    2, # anxiety
    3, # arthritis
    4, # asthma
    5, # atrial_fibrillation
    6, # breast_cancer
    10, # cerebrovascular_disease
    12, # copd
    14, # crohns
    15, # depression
    16, # diabetes_1
    17, # diabetes_2
    18, # drug_abuse
    19, # epilepsy
    20, # elevated_liver_function
    23, # heart_murmur
    24, # hepatitis_c
    26, # irregular_heartbeat
    32, # other_internal_cancer
    33, # other_skin_cancer
    35, # parkinsons
    36, # prostate_cancer
    37, # sleep_apnea
    38, # stroke
    41, # weight_reduction
  ]).freeze

  RELATIVES_DISEASE_IDS = Set.new([
    6, # breast_cancer
    7, # basal_cell_carcinoma
    8, # cardiovascular_disease
    9, # cardiovascular_impairments
    10, # cerebrovascular_disease
    11, # colon_cancer
    13, # coronary_artery_disease
    16, # diabetes_1
    17, # diabetes_2
    22, # heart_attack
    25, # intestinal_cancer
    27, # kidney_disease
    28, # malignant_melanoma
    32, # other_internal_cancer
    34, # ovarian_cancer
    36, # prostate_cancer
  ]).freeze
end
