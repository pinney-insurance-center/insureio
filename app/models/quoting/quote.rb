class Quoting::Quote < ActiveRecord::Base
  include Crm::Accessible

  self.table_name = 'quoting_details'

  include HasExtension
  
  #This temporary attribute allows important feedback like deprecation notices to be shown to the user,
  #without preventing save as `errors` would.
  attr_accessor :warnings

  # Associations
  belongs_to_enum :duration,       :class_name => "Enum::TliDurationOption"
  belongs_to_enum :health_class,   :class_name => "Enum::TliHealthClassOption"
  belongs_to_enum :product_type,   :class_name => "Enum::ProductType"
  belongs_to_enum :premium_mode,   :class_name => "Enum::PremiumModeOption"
  belongs_to_enum :sales_stage,    :class_name => "Enum::SalesStage"

  belongs_to      :carrier,        class_name: "Carrier"
  belongs_to      :consumer,       class_name: "Consumer",        foreign_key: :consumer_id
  belongs_to      :crm_case,       class_name: "Crm::Case",       foreign_key:'case_id'

  has_many        :statuses,       class_name: "Crm::Status", as: :statusable
  has_many        :tasks,          class_name: "Task",   as: :sequenceable

  has_one         :next_task,      ->{ where(active:true, suspended:false).order 'due_at ASC' }, class_name: "Task",   as: :sequenceable
  has_one         :agent,          class_name: "User", through: :consumer
  has_one         :status,         ->{ where(active:true).order('created_at DESC') }, class_name: "Crm::Status", as: :statusable

  belongs_to      :creator_name_only, ->{ select [:id,:full_name] }, class_name: "User", foreign_key: :user_id

  counter_culture :consumer,
    column_name: proc {|q| q.case_id.nil? ? 'opportunities_count' : nil },
    column_names: {
      ["quoting_details.case_id IS NULL"] => 'opportunities_count'
    },
    delta_magnitude: proc {|q| !q.closed ? 1 : 0 }


  delegate :agent_name, to: :consumer, prefix:false, allow_nil:true
  delegate :case_manager, to: :consumer, allow_nil:true
  delegate :duration_category_id, to: :duration, allow_nil: true
  delegate :name, to: :product_type, prefix:true,  allow_nil:true
  delegate :staff_assignment,  to: :consumer, allow_nil:true
  delegate :tz_offset, to: :consumer, prefix:false, allow_nil:true
  delegate :years, to: :duration, allow_nil: true

  alias_attribute :modal_premium,     :planned_modal_premium
  alias_attribute :product_name,      :plan_name
  alias_attribute :policy_type,       :product_type#DEPRECATED. Only here for backward compatibility with API.
  alias_attribute :policy_type_id,    :product_type_id#DEPRECATED. Only here for backward compatibility with API.
  alias_attribute :category_id,       :duration_id#DEPRECATED. Only here for backward compatibility with API.
  alias_attribute :category,          :duration#DEPRECATED. Only here for backward compatibility with API.

  ### Set Callbacks
  after_initialize :infer_consumer!, if: :new_record?
  before_save :infer_consumer!, on: :create
  before_save :infer_premium!
  before_save :manage_inclusion_in_forecasts,                 if: ->{ sales_stage_id==1 && case_id.nil? }
  after_save :set_initial_status!,                           if: ->{ include_in_forecasts && !case_id }

  validate :must_meet_app_fulfillment_reqs, if: :@check_app_fulfillment_requirements
  validates :sales_stage_id, uniqueness: { scope: :case_id }, if: -> { case_id.present? }

  scope :not_closed,   lambda{ where(closed:false) }
  scope :active,       lambda{ not_closed }
  scope :by_type,      lambda{ order(:product_type_id) }
  scope :forecasts,    lambda{ where(sales_stage_id:1, case_id:nil, include_in_forecasts: true).joins(:consumer) }
  scope :for_users,    lambda{ |user_ids|
    joins(:consumer).where "consumers.agent_id": user_ids.flatten.compact.map { |u| u.is_a?(Fixnum) ? u : u.id }.uniq
  }
  scope :person_name_like, lambda{|str|
    joins(:consumer).merge( Consumer.name_like(str) )
  }

  #This scope is meant to reduce repetitive code while still not defining `status` as its own association.
  #Doubly-linked associations lead to confusing, circular callback chains and are to be avoided.
  scope :joins_current_status, lambda {
    joins(%Q(
      INNER JOIN crm_statuses AS status
      ON         status.statusable_id=#{self.table_name}.id
      AND        status.statusable_type='Quoting::Quote'
      AND        status.active=TRUE
    ))
  }

  #These methods should only be called from reporting, on quotes acting as opportunities.
  def record_type; "Opportunity"; end
  def current_details;     self; end
  def quoted_details;      self; end
  def opportunity_details; self; end
  def contacted_details;   nil; end
  def a_team_details;      nil; end
  def submitted_details;   nil; end
  def approved_details;    nil; end
  def agent_of_record_id;  nil; end
  def agent_of_record_name;nil; end

  ### Pseudo accessor/mutator methods

  def active; !self.closed; end
  alias_method :active?, :active

  def person_name; self.consumer.try(:name); end

  #Take advantage of database index on contact id and name.
  def created_by_name
    creator_name_only.full_name if user_id#don't try to access name unless association exists.
  end

  def as_json opts={}
    opts[:except]||=[]
    opts[:except]+=[:annual_premium,:monthly_premium,:quarterly_premium,:semiannual_premium]
    opts[:methods]||=[]
    opts[:methods]+= [:warnings]
    super opts
  end

  def annualized_premium
    return unless premium_mode && planned_modal_premium
    return planned_modal_premium if premium_mode.months.to_i == 0
    # Calculate & return sought premium
    planned_modal_premium * 12 / (premium_mode.months)
  end

  def modal_premium
    self[:planned_modal_premium] || (premium_mode && self[premium_mode.field])
  end

  def carrier_name
    self.carrier.try(:name)
  end

  def carrier_name= name
    self.carrier = Carrier.find_by_name name
  end

  def health
    self.health_class.try(:value)
  end

  def health= value
    value = "Best Class" if value == "Preferred Plus"
    h_klass = Enum::TliHealthClassOption.find_by_value(value) || Enum::TliHealthClassOption.find_by_name(value)
    self.health_class = h_klass if h_klass.present?
  end

  def self.health_class_options
    Enum::TliHealthClassOption.all.map{|x| {text:x.name, value:x.id} }
  end

  def premium_mode_name= value
    self.premium_mode = Enum::PremiumModeOption.find_by_name(value)
  end

  def premium_mode_name
    self.premium_mode.try :name
  end

  def product_type_name
    self.product_type.try :name
  end

  def product_type_name= name
    self.product_type = Enum::ProductType.find_by_name(name)
  end

  def duration_name= name_str
    opts_class=Enum::TliDurationOption
    self.duration = opts_class.find_by_name(name_str) ||
      opts_class.find_by_compulife_code(name_str) ||
      opts_class.all.select{|o| o.name&.include?(name_str.to_s) }.first
  end
  alias_method    :category_name=,    :duration_name=#DEPRECATED. Only here for backward compatibility with API.

  #when records are not yet persisted, a has_one association with consumer
  #won't work, so we can't use delegate :agent_id, to: :consumer
  def agent_id
    self.try(:agent).try(:id) || self.consumer.try(:agent_id)
  end


  module Overrides
    def premium_mode= value
      value = Enum::PremiumModeOption.find_by_name(value) if value.is_a? String
      super(value)
    end
  end
  include Overrides


  def status_type_id= type_id
    return unless type_id.present? && type_id=type_id.to_i

    type=Crm::StatusType.where(is_for_policies:true, id:type_id).first
    return unless type.present?
    return if status && status.status_type_id==type_id#don't duplicate the current status

    if self.new_record?
      begin
        retries ||= 0
        new_status=self.statuses.send :new, status_type_id: type_id, statusable:self
      rescue => ex
        retry if (retries += 1) < 3
        ExceptionNotifier.notify_exception(ex)
      end
      new_status
    else
      if type_id==Crm::StatusType::PRESETS[:submit_to_app_team][:id] || type_id==Crm::StatusType::PRESETS[:submit_to_app_team2][:id]
        @check_app_fulfillment_requirements=true
        return true
      end
      self.crm_case=Crm::Case.create(
        consumer_id:    self.consumer_id,
        quoting_details: [self], # The callback `Crm::Case#create_details_for_sales_stage` should make a copy if needed
        status_type_id: type_id,
        quoted_at:      self.created_at,
        )
      #A callback on the case will reassociate existing statuses and tasks.
    end
  end

  def temporary_insurance; nil; end#used when checking requirements for app fulfillment

  def beneficiaries; nil; end#used when checking requirements for app fulfillment

  def must_meet_app_fulfillment_reqs
    missing_fields = Crm::Status::REQUIREMENTS_FOR_APP_FULFILLMENT.select { |method_chain|
      method_chain.reduce(self) { |obj, meth| obj.try(meth) rescue nil }.blank?
    }.map { |method_chain|
      method_chain.last.gsub(/_id$/,'').gsub(/_/,' ').gsub(/dln/,'driver\'s license number').gsub(/dl state/,'driver\'s license state')
    }
    return true if missing_fields.blank?
    errors.add :base, "Before you can submit for processing, you will need to provide the following fields: #{missing_fields.join(', ')}"
    return false
  end

  #Create accessor methods like those on Crm::Case for each sales stage.
  #These methods should only ever be called for quotes that act as opportunities,
  #which should always have sales stage 1.
  Enum::SalesStage.all.each do |ss|
    if ss.id<Enum::SalesStage.id('App Fulfillment')
      ss_name=ss.name.gsub(' ','_').downcase
      next_ss_name=Enum::SalesStage.find(ss.id+1).name.gsub(' ','_').downcase
      time_method_name="time_in_#{ss_name}_stage"

      define_method(time_method_name) do |use_dhm=true, restrict_to_past_tense=false|
        end_time  =self.send("entered_stage_#{next_ss_name}") rescue nil
        start_time=ss.id==1 ? self.created_at : self.send("entered_stage_#{ss_name}")rescue nil
        return 'n/a' if !start_time || (restrict_to_past_tense && !end_time)
        end_time||=Time.now
        if use_dhm
          time_str=self.class.dhm_between start_time, end_time
        else
          time_str=(end_time-start_time).to_i
        end
        self.send("entered_stage_#{next_ss_name}") && use_dhm ? time_str.to_s : time_str.to_s+' (so far)'
      end
    end

    #Define accessor methods for the first status of each sales stage.
    #It's better to call `self.statuses`,filtering using rails array method `select` than to define individual associations for these,
    #because these will only be used when aggregating cases, and this way they all get preloaded at once.
    status_method_name='first_status_in_stage_'+ss.name.to_s.downcase.gsub(/\s/,'_')
    define_method status_method_name do
      self.statuses.select{|s| s.sales_stage_id.to_i>=ss.id }.first
    end
    #Allow calling functions like `kase.first_submitted_status_name`.
    delegate :created_at,:created_by_name,:name, to: status_method_name.to_sym, prefix:true, allow_nil: true
  end
  alias_method :time_until_contact,:time_in_opportunity_stage#should remove this. just putting it in for now so things don't break.
  alias_method :time_from_contact_to_sent_stage, :time_in_contacted_stage#should remove this. just putting it in for now so things don't break.

  Enum::StatusTypeCategory.ewhere(is_for_policies:true).each do |c|
    underscored_cat_name=c.name.to_s.downcase.gsub(/[^a-z]+/,'_')
    define_method "entered_category_#{underscored_cat_name}" do
      self.statuses.select{|s| s.status_type_category_id.to_i==c.id }.first.try(:created_at)
    end
  end

  def status
    self.statuses.last
  end

private

  def _accessible? user_or_id, edit_or_view
    if sess_cons_id = Consumer.ltd_access_id
      return false if sess_cons_id != consumer_id && persisted?
    end
    super
  end

  def infer_consumer!
    self.consumer_id ||= crm_case&.consumer_id
    self.consumer ||= crm_case&.consumer
  end

  def infer_premium!
    # Array of premiums indexed by Enum::PremiumModeOption
    premiums = [ annual_premium,
      semiannual_premium,
      quarterly_premium,
      monthly_premium,
      single_premium ]
    # Let premium mode fallback to the first premium which is non-blank
    self.premium_mode_id ||= premiums.each_with_index { |p,i| break i if p.present? }
    self.premium_mode_id ||= 1 # Default to annual
    # Find modal premium for premium_mode_id
    premiums[premium_mode_id].tap do |premium|
      self.modal_premium = premium unless premium.nil? # Don't overwrite with a nil value
    end
  end

  def set_initial_status!
    return true if self.status
    #Calling :initial_status_type vs. :initial_status_type_id so that nil is returned if the record no longer exists.
    type_id  = consumer.try(:brand).try(:initial_status_type).try(:id)
    type_id||= consumer.try(:agent).try(:initial_status_type).try(:id)
    type_id||= Crm::StatusType::PRESETS[:prospect][:id]
    self.statuses.create status_type_id:type_id, statusable:self
  end

  #If this quote is not marked for inclusion and there are no quotes being included in forecasts already, and the quote was run by a consumer, add it to forecasts.
  #If this quote is marked for inclusion and there are quotes being included in forecasts already which are incomplete,
  #unmark and close the statuses for the incomplete quotes to reduce clutter in reports and to remove duplicate tasks.
  #Incomplete quotes are generated when consumers click to get quotes from a widget or quote path.
  #They include fields like face amount and duration, but lack carrier and product info.
  def manage_inclusion_in_forecasts
    return unless consumer
    included_opps=self.consumer.opportunities.where(include_in_forecasts:true,product_type_id:self.product_type_id)
    incomplete_opps=included_opps.where(carrier_id:nil)

    if !self.include_in_forecasts && !self.user_id && included_opps.count==0
      self.include_in_forecasts=true
    elsif self.include_in_forecasts && incomplete_opps.select{|o| o!=self && o.id!=self.id }.count>0
      incomplete_opps.select{|o| o!=self && o.id!=self.id }.each do |o|
        o.statuses.update_all(active:false)
        o.update_attributes(include_in_forecasts:false)
      end
    end

    return true
  end

end
