# A widget has a polymorphic quote object, which is not a persistable record.
# The quote object represents 1 of several types of quoters such as ltc and term.
# The quote object is reponsible for communicating with quoter apis like compulife.
# Each widget type instance has a whitelist of carriers,
# in addition to each widget instance's own blacklist of carriers.
# The polymorphic quote object will only be called through the quotes controller via REST.
# Widget front end should expose public API to get quotes and request application thru REST.

class Quoting::Widget < ActiveRecord::Base
  self.table_name='quoting_widget_widgets'#TEMPORARY. Eventually this should be cleaned up with a migration.

  serialize :tags, Hash

  belongs_to :user, class_name: 'User'
  belongs_to :brand, class_name: 'Brand'

  belongs_to_enum :widget_type, class_name: "Enum::QuotingWidgetType"
  belongs_to_enum :product_cat, class_name: "Enum::ProductTypeCat"

  has_many :excluded_carriers
  has_many :blacklisted_carriers, class_name: "Carrier", :through => :excluded_carriers, foreign_key: "widget_id", source: :carrier

  validate :user_has_permission_for_widget_type

  
  attr_accessor :excluded_ids

  after_initialize :init_defaults
  def init_defaults
    return unless new_record?
    self.header_text_color ||= '#4F62E3'
    self.background_color  ||= '#ffffff'
    self.button_color      ||= '#4F62E3'
    self.text_color        ||= '#6b7c93'
    self.border_color      ||= '#3B3B3B'

    self.header_text       ||= 'Get A Free Quote For'
    self.button_text       ||= 'Compare Quotes'
    self.button2_text      ||= 'Select'
    self.button3_text      ||= 'Apply'
    self.name              ||= "Widget #{user.try(:quoting_widgets).try(:count)}"
    self.quoter_style      ||= 1
    self.widget_type_id    ||= 1
    self.show_name_field   ||= true
    self.show_phone_field  ||= true
    self.show_email_field  ||= true
  end

  before_save :update_excluded_carriers

  validates :hostname,
    presence:true,
    format: {
      with: Regexp.new(VALID_URL_PATTERN,'i'),
      message: 'must be formatted as a valid domain name with protocol'
    }

  validates :background_color,  presence: true
  validates :border_color,      presence: true
  validates :text_color,        presence: true
  validates :header_text_color, presence: true
  validates :button_color,      presence: true
  validates :header_text,       presence: true
  validates :button_text,       presence: true
  validates :name,              presence: true, uniqueness:{scope: :user_id}
  validates :user,              presence: true

  alias_method :original_brand, :brand
  def brand
    return self.original_brand || self.user.default_brand
  end

  delegate :name, to: :brand, prefix:true, allow_nil:true
  delegate :name, to: :user,    prefix:true, allow_nil:true
  delegate :rtt_allowed_states, to: :user,   prefix:false, allow_nil:true

  delegate :quote_class,         to: :widget_type

  #Cache this list while the widget is in memory.
  #Useful when looping over the list, to prevent repeat queries.
  def whitelisted_carriers
    @whitelisted_carriers ||=widget_type.whitelisted_carriers.to_a
  end

  alias_method :orig_blacklisted_carriers, :blacklisted_carriers
  def blacklisted_carriers
    @blacklisted_carriers ||=orig_blacklisted_carriers.to_a
  end

  def quoter_style_string
    case quoter_style
    when 1
      return "wide"
    when 2
      return "outlined"
    when 3
      return "flat"
    when 4
      return "rounded"
    else return nil
    end
  end

  def update_excluded_carriers
    self.blacklisted_carrier_ids = excluded_ids
  end

  def as_json(attr = {})
    super(attr).merge({:excluded_ids => blacklisted_carrier_ids})
  end

  #parses String to a Hash
  def tags= value
    if value.is_a?(Hash)
      self[:tags]=value
    elsif value.is_a?(String)
      value=value.split(',').map{|t| t.strip.split('=')}
      value_hash={}
      value.map{|k,v| value_hash[k]=v }

      self[:tags]=value_hash
    end
  end

  #formats Hash as a String
  def tags format=String
    if format==String
      self[:tags].map(&:to_a).map{|t| (t[1].present? ? t.join('=') : t[0]) }.join(', ')
    elsif format==JSON
      self[:tags].map{|k,v| "{key:'#{k}', value:'#{v}'}" }.join(",\n")
    else
      self[:tags]
    end
  end

private

  def user_has_permission_for_widget_type
    if widget_type.permission_name.present? && !user.can?(widget_type.permission_name)
      errors.add(:widget_type_id, "Owner #{user_id} lacks permission for this widget type")
    end
  end

end
