class Quoting::Xrae::Client
  include Quoting::Xrae::ValueConverterHelper
  include AuditLogger::Helpers
  audit_logger_name 'xrae_quoting'

  attr_accessor(
    :errors,
    :crm_case,
    :xrae_case,
    :case_key,
    :get_case_response,
    :update_case_response,
    :add_case_response,
    :uw_response,
    :pricing_response,
    :quotes
  )

  def initialize(crm_case)
    self.crm_case = crm_case
    self.xrae_case = self.class.crm_case_to_xrae_case(crm_case)
    if crm_case.respond_to?(:xrae_case_id) && crm_case.xrae_case_id
      self.case_key=crm_case.xrae_case_id
    end
    self.errors = ActiveModel::Errors.new(self)
  end

  # Fetch quotes from XRAE, a 3 step process:
  #   1) verify and update XRAE record if case has an `xrae_case_id` or send to XRAE to get a new id
  #   2) get an underwriting result from XRAE
  #   3) get pricing from XRAE
  # Set `self.quotes` to an array of quotes, filtered and converted by helper methods.
  # If a table-rated health class is supplied, prune results whose health class does not match.
  def get_quotes
    specified_health_class_name= crm_case.current_details.try(:health_class).try(:name)
    is_table= specified_health_class_name =~ /table/i

    if case_key
      self.get_case_response = get_case_by_key(case_key).with_indifferent_access
      unless get_case_response && get_case_response.dig(:GetCaseByKeyResponse,:GetCaseByKeyResult,:HasResult) == true
        errors.add :base, "Failed to get case for XRAE case id #{case_key}"
        return self
      end
      #caseKey is valid
      self.update_case_response= update_case
    else
      self.add_case_response = add_case.with_indifferent_access
      unless add_case_response && add_case_response.dig(:AddCaseResponse,:AddCaseResult,:Completed) == true
        errors.add :base, "Failed to submit case to XRAE"
        return self
      end
      self.case_key = add_case_response[:AddCaseResponse][:AddCaseResult][:CaseKey]
      if crm_case.respond_to?(:xrae_case_id)
        crm_case.xrae_case_id = case_key
        crm_case.save if !crm_case.new_record?
      end
    end

    self.uw_response = get_case_underwriting.with_indifferent_access
    if uw_response.dig(:GetCaseUnderwritingResponse,:GetCaseUnderwritingResult,:HasResults) != true
      uw_result_message=uw_response.dig(:GetCaseUnderwritingResponse,:GetCaseUnderwritingResult,:Message)
      errors.add :base, "Failed to get case underwriting: #{uw_result_message}"
      return self
    end

    if term?
      self.pricing_response = get_case_term_pricing_ex.with_indifferent_access
      if pricing_response.dig(:GetCaseTermPricingExResponse,:GetCaseTermPricingExResult,:HasResults) != true
        term_pricing_message=pricing_response.dig(:GetCaseTermPricingExResponse,:GetCaseTermPricingExResult,:Message)
        errors.add :base, "Failed to get case term pricing: #{term_pricing_message}"
        return self
      end
      xrae_quotes = pricing_response.dig(:GetCaseTermPricingExResponse,:GetCaseTermPricingExResult,:PricingResults,:TermPricingResult)
      self.class.prune(xrae_quotes, specified_health_class_name) if is_table
      term_length=self.class.term_length_from_id( crm_case.current_details.duration_id )
      self.quotes= self.class.xrae_term_results_to_usable_results(xrae_quotes, term_length )
      return self
    else
      self.pricing_response = get_case_ul_pricing_ex.with_indifferent_access
      if pricing_response.dig(:GetULCasePricingExResponse,:GetULCasePricingExResult,:HasResults) != true
        ul_pricing_result_message=pricing_response.dig(:GetULCasePricingExResponse,:GetULCasePricingExResult,:Message)
        errors.add :base, "Failed to get case UL pricing: #{ul_pricing_result_message}"
        return self
      end
      xrae_quotes = pricing_response.dig(:GetULCasePricingExResponse,:GetULCasePricingExResult,:PricingResults,:ULPricingResult)
      self.class.prune(xrae_quotes, specified_health_class_name) if is_table
      self.quotes =self.class.xrae_ul_results_to_usable_results(xrae_quotes)
      return self
    end
  end

  def update_case
    raise ArgumentError.new("SubjectProfile cannot be nil") if not xrae_case.has_key?(:SubjectProfile)
    raise ArgumentError.new("DateOfBirth cannot be nil") if not xrae_case[:SubjectProfile].has_key?(:DateOfBirth)
    raise ArgumentError.new("Gender cannot be nil") if not xrae_case[:SubjectProfile].has_key?(:Gender)

    message = {
      param: {
        CaseKey: case_key,
        SubjectProfile: xrae_case[:SubjectProfile]
      }
    }
    message[:param][:ConsumerName] = xrae_case[:ConsumerName] if xrae_case.has_key?(:ConsumerName)
    message[:param][:StateOfIssue] = xrae_case[:StateOfIssue] if xrae_case.has_key?(:StateOfIssue)

    return soap_request('UpdateCase', message).to_hash
  end

  # Sends a Crm::Case to XRAE. This step is preliminary to getting quotes
  # subjectProfile is a hash that contains required keys {:DateOfBirth { :Month, :Day, :Year}, :Gender ('Male'/'Female') }
  # as well as other health information
  def add_case
    raise ArgumentError.new("SubjectProfile cannot be nil"         ) if not xrae_case.has_key?(:SubjectProfile)
    raise ArgumentError.new("DateOfBirth cannot be nil"            ) if not xrae_case[:SubjectProfile].has_key?(:DateOfBirth)
    raise ArgumentError.new("Gender cannot be nil"                 ) if not xrae_case[:SubjectProfile].has_key?(:Gender)
    raise ArgumentError.new("XRAE Api key is not set in APP_CONFIG") if not APP_CONFIG.dig('xrae','api_key')

    message = {
      param: {
        AccountKey: APP_CONFIG['xrae']['api_key'],
        SubjectProfile: xrae_case[:SubjectProfile],
      } 
    }

    message[:param][:ConsumerName] = xrae_case[:ConsumerName] if xrae_case.has_key?(:ConsumerName)
    message[:param][:StateOfIssue] = xrae_case[:StateOfIssue] if xrae_case.has_key?(:StateOfIssue)

    return soap_request('AddCase', message).to_hash
  end

  # returns a target case by its key
  def get_case_by_key (key)
    raise ArgumentError.new("caseKey is nil") if key.nil?
    message = {
        caseKey: key
    }
    return soap_request('GetCaseByKey', message).to_hash
  end

  # gets quotes for a target case but also takes underwriting results as parameters - probably to narrow down a search
  # caseKey is a string, underwritingresult is the data within a GetCaseUnderwritingResult response
  def get_case_term_pricing_ex
    raise ArgumentError.new("caseKey is nil") if case_key.nil?
    raise ArgumentError.new("underwriting_results is nil") if uw_response.nil?
    message = {
      caseKey: case_key,
      results: uw_response[:GetCaseUnderwritingResponse][:GetCaseUnderwritingResult]
    }
    return soap_request('GetCaseTermPricingEx', message).to_hash
  end


  # get universal life quote
  def get_case_ul_pricing_ex
    raise ArgumentError.new("caseKey is nil") if case_key.nil?
    raise ArgumentError.new("underwriting_results is nil") if uw_response.nil?

    message = {
        caseKey: case_key,
        param: {
          SortType: 'Selected',
          PaymentMode: 'Annual'
        },
        uwResults: uw_response[:GetCaseUnderwritingResponse][:GetCaseUnderwritingResult]
    }

    return soap_request('GetULCasePricingEx', message).to_hash
  end

  def get_case_underwriting
    message = {
      caseKey: self.case_key
    }

    return soap_request('GetCaseUnderwriting', message).to_hash
  end

  def self.prune xrae_quotes, specified_health_class_name
    # Prune results whose ClassName doesn't match the table rating in `specified_health_class_name`
    if specified_health_class_name && specified_health_class_name =~ /table/i
      num           = table.match(/(^|\W)(\d)($|\W)/).try(:[], 2)
      alpha         = table.match(/(^|\W)([A-Z])($|\W)/).try(:[], 2)
      num_pattern   = Regexp.new '(^|\D)%s($|\D)' % num if num
      alpha_pattern = Regexp.new '(^|\W)%s($|\W)' % alpha if alpha
      table_pattern = /table/i
      xrae_quotes.select!{|r| r[:ClassName] =~ table_pattern && (r[:ClassName] =~ num_pattern || r[:ClassName] =~ alpha_pattern) }
    else
      xrae_quotes
    end
  end

  private

  def ns_hash input
    case input
    when Array
      input.each{|o| ns_hash o }
    when Hash
      input.keys.each do |k|
        next if k.to_s =~ /:/
        v = input.delete(k)
        input["lol0:#{k}"] = v
        ns_hash(v) if v.respond_to?(:each)
      end
    end
    input
  end

  # Provide action as a string with true casing, e.g. 'ServiceRequest', not :service_request
  def soap_request action, message
    # portType/service: XraeDirectApi
    # namespace: WSHttpBinding_XraeDirectApi
    endpoint = APP_CONFIG['xrae']['url'].sub(/\?.*$/,'') # http://dev.service.xrae.com/direct.svc

    client = Savon.client(
      endpoint: endpoint,
      ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
      ssl_verify_mode: :none,
      convert_request_keys_to: nil,
      convert_response_tags_to: :camelcase,
      soap_version: 2,
      env_namespace: :env,
      namespace_identifier: 'lol0',
      namespace: 'http://service.xrae.com/',
      namespaces: {
        'xmlns:wsa'=>'http://www.w3.org/2005/08/addressing',
        'xmlns:env'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:lol0'=>'http://service.xrae.com/'
      },
      log_level: :debug,
      log: true,
      logger: Logger.new('log/xrae_savon.log')
    )
    action_url = 'https://service.xrae.com/XraeDirectApi/'+action
    header = {'wsa:Action'=> action_url, 'wsa:To'=> endpoint}
    client.call( action,
      message:ns_hash(message),
      soap_header: header,
      soap_action: action_url
    )
  end

  def term?
    ul_match_pattern=Regexp.new 'U\/?L|Universal Life'
    is_ul=( @crm_case.current_details.duration && @crm_case.current_details.duration.name.match(ul_match_pattern) ) ||
          ( @crm_case.product_type && @crm_case.product_type.name.match(ul_match_pattern) )
    !is_ul
  end
  
end