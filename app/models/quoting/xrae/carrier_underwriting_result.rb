class Quoting::Xrae::CarrierUnderwritingResult
	def initialize (carrier_underwriting_result)
		@carrier_underwriting_result = carrier_underwriting_result
	end

	def carrier_code 
		@carrier_underwriting_result[:CarrierCode]
	end

	def carrier_name
		@carrier_underwriting_result[:CarrierName]
	end

	def naic
		@carrier_underwriting_result[:NAIC]
	end

	def product_type
		@carrier_underwriting_result[:ProductType]
	end

	def display_name
		@carrier_underwriting_result[:DisplayName]
	end

	def underwriting_class
		@carrier_underwriting_result[:UnderwritingClass]
	end

	def is_tobacco?
		@carrier_underwriting_result[:IsTabacco]
	end

	def table_rating
		@carrier_underwriting_result[:TableRating]
	end

	def sorting_rank
		@carrier_underwriting_result[:SortingRank]
	end

end