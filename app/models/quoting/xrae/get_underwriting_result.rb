class Quoting::Xrae::GetUnderWritingResult
	def initialize (get_under_writing_result = {})
		@get_under_writing_result = get_under_writing_result
	end

	def has_results?
		@get_under_writing_result[:HasResults] == 'true'
	end

	def message
		@get_under_writing_result[:Message]
	end

	def underwriting_results
		return [] if @get_under_writing_result[:UnderwritingResults].nil? || has_results? == false
		@underwriting_results ||= []
		if @underwriting_results.empty?
			@get_under_writing_result[:UnderwritingResults].each do |result|
				results.push(Quoting::Xrae::CarrierUnderwritingResult.new(result))
			end
		end
		return @underwriting_results
	end

end