class Quoting::LtcExtraFieldSet < ActiveRecord::Base
       
  # Associations
  belongs_to_enum :benefit_period,       class_name: "Enum::LtcBenefitPeriodOption"
  belongs_to_enum :health,               class_name: "Enum::LtcHealthClassOption"
  belongs_to_enum :inflation_protection, class_name: "Enum::LtcInflationProtectionOption"
end
