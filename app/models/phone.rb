class Phone < ActiveRecord::Base

  belongs_to      :contactable, polymorphic:true
  belongs_to_enum :phone_type,  class_name: "Enum::PhoneType"
  belongs_to_enum :carrier,     class_name: "Enum::PhoneCarrier"

  alias_attribute  :type_id, :phone_type_id

  validates :value, format:{with: /\A[2-9]\d{9}\Z/, message:'should be 10 digits in length, starting with a valid area code'}, presence:true
  validate  :cannot_be_blacklisted

  after_destroy     :update_contactable_after_destroy
  after_save        :update_contactable

  #Remove parentheses and hyphens, so those are not stored.
  #Leave other characters on purpose, so validation will produce the right error messages.
  def value= val
    self[:value]=val.to_s.gsub(/\(|\)|-|\s/,'')
  end

  def carrier_provided_email
    return nil unless self.carrier
    "#{value}@#{carrier.email_to_sms_domain}"
  end

  # Return phone + extension with only digital characters
  def to_d(exclude_ext=false)
    s = exclude_ext ? value : "#{value}#{ext}"
    s ||= ""
    s.gsub(/\D/, '')
  end

  def formatted_value
    match = value.to_s.gsub(/\D/, '').match(/(\d){0,1}(\d{3}){0,1}(\d{3})(\d{4})$/)
    return '' if match.nil?

    if match.captures[0].nil? && match.captures[1].present?#if it has area code but no country code
      "(#{match.captures[1]})#{match.captures[2]}-#{match.captures[3]}"
    else
      match.captures.compact.join('-')
    end
  end

  def matches? other
    other.is_a?(Phone) && [:value, :ext, :phone_type_id].all?{|sym| other[sym] == self[sym] }
  end

  # options include:
  # => :omit_type
  # => :omit_ext
  # => :separator
  def to_s options={}
    options[:omit_type]=(self.contactable_type=='User') if !options.has_key?(:omit_type)
    options[:separator] ||= ' ext. '
    match = value.to_s.gsub(/\D/, '').match(/(\d){0,1}(\d{3}){0,1}(\d{3})(\d{4})$/)
    return '' if match.nil?
    output = ""
    output += "#{phone_type.name[0].capitalize}: " unless options[:omit_type] || phone_type.nil?
    output += match.captures.compact.join('-')
    output += "#{options[:separator]}#{ext}" unless ext.blank? or options[:omit_ext]
    output
  end

  def type_name= name
    self.phone_type_id = Enum::PhoneType.id(name)
  end
  
  alias_method :click_to_call_string, :to_s

private

  def self.blacklist
    @blacklist ||= File.exists?(BLACKLIST_FILE) ? YAML.load_file(BLACKLIST_FILE) : [] rescue []
  end

  def cannot_be_blacklisted
    if self.class.blacklist.include?(to_d(true))
      errors.add(:value, "#{value} is in the system blacklist due to fraud. Please contact your vendor if you know this number to be correct")
    end
  end

def update_contactable
    if contactable && contactable.primary_phone_id.blank?
      contactable.update_column :primary_phone_id, id
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

  def update_contactable_after_destroy
    if contactable && contactable.primary_phone_id==id
      contactable.update_column :primary_phone_id, contactable.phones.first.try(:id)
    end
  rescue => ex
    ExceptionNotifier.notify_exception ex
  end

  BLACKLIST_FILE = File.join(Rails.root, 'config/phones_blacklist.yml')
end
