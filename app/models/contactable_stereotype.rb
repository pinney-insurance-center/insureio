module ContactableStereotype
  extend ActiveSupport::Concern
  #This module includes shared methods for consumers, users, recruits, and brands.
  #Much of it was previously included within the model Contact or the modules HasContactStereotype and PersonStereotype.

  included do
    extend Concerns::ModelHelpers::ClassMethods

    alias_attribute :name,:full_name

    include Paranoia
    alias_method :delete, :paranoid_delete
    alias_method :destroy,:paranoid_destroy

    #Sometimes we want to report a minor data issue back to the user,
    #but without preventing save, as an error would do.
    #If set, `warnings` should be set to an array of strings.
    attr_accessor :warnings

    #SSN and TIN should be returned formatted, but saved stripped.
    if has_col?('encrypted_tin')
        if (Rails.env.production? || Rails.env.test?)
          #`insecure_mode` is for backward compatibility, when an initialization vector was not required.
          #For any new encrypted fields, provide the iv instead.
          attr_encrypted :tin, algorithm:'aes-256-cbc', mode: :single_iv_and_salt, insecure_mode: true, key: APP_CONFIG['old_encryption_key']
        else
          alias_attribute :tin,  :encrypted_tin
        end
        alias_method :orig_tin,  :tin
        alias_method :tin,       :formatted_tin

        alias_method :orig_tin=, :tin=
        alias_method :tin=,      :stripped_tin=
    end
    if has_col?('encrypted_ssn')
        if (Rails.env.production? || Rails.env.test?)
          attr_encrypted :ssn, algorithm:'aes-256-cbc', mode: :single_iv_and_salt, insecure_mode: true, key: APP_CONFIG['old_encryption_key']
        else
          alias_attribute :ssn,  :encrypted_ssn
        end
        alias_method :orig_ssn,  :ssn
        alias_method :ssn,       :formatted_ssn

        alias_method :orig_ssn=, :ssn=
        alias_method :ssn=,      :stripped_ssn=
    end

    validates_presence_of :full_name

    belongs_to_enum :preferred_contact_method, class_name: "Enum::ContactMethod"
    belongs_to_enum :entity_type,              class_name: 'Enum::EntityType'
    belongs_to_enum :tenant,                   class_name: 'Enum::Tenant'

    belongs_to :owner,             class_name: "User",      foreign_key: self::OWNER_ID_COLUMN
    belongs_to :owner_name_only,   ->{ select [:id,:full_name] }, class_name: "User",    foreign_key: self::OWNER_ID_COLUMN

    #contact method associations
    has_many        :emails,         class_name: "EmailAddress",  as: :contactable,  dependent: :delete_all
    has_many        :addresses,      class_name: "Address",       as: :contactable,  dependent: :delete_all
    has_many        :phones,         class_name: "Phone",         as: :contactable,  dependent: :delete_all
    has_many        :webs,           class_name: "Web",           as: :contactable,  dependent: :delete_all

    has_one         :facebook,       ->{ where web_type_id: Enum::WebType.id("Facebook") }, class_name: "Web", as: :contactable
    has_one         :twitter,        ->{ where web_type_id: Enum::WebType.id("Twitter" ) }, class_name: "Web", as: :contactable
    has_one         :google_plus,    ->{ where web_type_id: Enum::WebType.id("Google+" ) }, class_name: "Web", as: :contactable
    has_one         :linked_in,      ->{ where web_type_id: Enum::WebType.id("LinkedIn") }, class_name: "Web", as: :contactable

    accepts_nested_attributes_for :emails,    allow_destroy: true, reject_if: :email_is_dup_or_blank
    accepts_nested_attributes_for :phones,    allow_destroy: true, reject_if: :phone_is_dup_or_blank
    accepts_nested_attributes_for :addresses, allow_destroy: true, reject_if: :address_is_dup_or_blank
    accepts_nested_attributes_for :webs,      allow_destroy: true, reject_if: :web_is_dup_or_blank

    after_create :set_initial_primary_contact_methods
    after_update :update_tasks, if: :changed?


    scope :name_and_id_only,lambda {
      select(["#{table_name}.id","#{table_name}.full_name"])
    }

    scope :name_like, lambda { |q|
      return scoped if q.blank?
      q = q.gsub(/[\u0250-\ue007<>]/,'')#remove non-latin and <> characters.
      q = q.gsub(/\s+/, '%')
      q = "%#{q}%"
      where( "full_name LIKE ?", q )
    }

    scope :name_or_id_like, lambda { |q|
      q_for_name = q.gsub(/[\u0250-\ue007<>]/,'')#remove non-latin and <> characters.
      q_for_name = q_for_name.gsub(/\s+/, '%')
      q_for_name = "%#{q_for_name}%"
      where("full_name LIKE ? OR id = ? OR id = ?", q_for_name, q.to_i(36), q.to_i)
    }

    scope :active_or_enabled, lambda {
      where(active_or_enabled:true)
    }

    #These delegations are for ease of reporting.
    delegate :street,:city,:state,:state_id,:zip, to: :primary_address, prefix: true, allow_nil:true

    extend ClassMethods
  end

  module ClassMethods
    def [] *args
      find_by_name *args
    end

    def find_by_name *args
      name_like( args.join(' ') ).first
    end

    def search_full_name *args
      name_like( args.join(' ') )
    end

    def short(short_id)
      find id_from_short_id(short_id)
    end

    def id_from_short_id short_id
      short_id.to_i(36)
    end

    def id_to_short_id id
      id && id.to_s(36).upcase
    end

    def short_id
      id && id.to_s(36).upcase
    end

    def active; active_or_enabled; end
  end#end of class methods

  def active; active_or_enabled; end

  #Is used in email templates
  def short_id
    id && id.to_s(36).upcase
  end

  def contact
    SystemMailer.deprecated_method_warning(self) rescue nil
    self
  end

  def as_json opts={}
    opts[:methods]||=[]
    opts[:methods]+= [:warnings]
    super opts
  end

  #For use when serializing.
  #Eventually, we will want to have AngularJS models for each type of contactable record instead.
  def person_type
    if self.respond_to?(:is_recruit) && self.is_recruit
      return 'Recruit'
    end
    self.class.name
  end
  def person_type= value
    nil
  end

  def tenant_name
    tenant.try(:name)
  end

  #Remove leading/trailing/multiple spaces.
  #Remove non-latin and <> characters.
  def full_name= value
    return unless value.present?
    value=value.strip.gsub(/\s+/,' ')
    value=value.gsub(/[\u0250-\ue007<>]/,'')
    self[:full_name]=value
  end

  #Returns first name, middle initial, last name.
  def short_full_name
    name_array=self.name.try(:split) || []
    name_array[1]=name_array[1][0] if name_array.length>2
    return name_array.join(' ')
  end

  #These name methods are used in marketing templates and communications like password resets.
  def first_name
    self.name.try(:split).try(:first) || ''
  end

  def last_name
    names = self.name.try(:split)
    names.present? && names.size >1 ? names.last : ''
  end

  def middle_name
    names = self.name.try(:split)
    names.present? && names.size > 2 ? names.second : ""
  end

  def owner_name
    record=
      if association(:owner).loaded?
        agent
      elsif association(:owner_name_only).loaded?
        owner_name_only
      elsif self.class::OWNER_ID_COLUMN==:agent_id && association(:agent_name_only).try(:loaded?)
        agent_name_only
      elsif self.class::OWNER_ID_COLUMN==:parent_id && association(:parent_name_only).try(:loaded?)
        parent_name_only
      else
        owner_name_only
      end
    record.try(:full_name)
  end

  #When displaying age in the app front end, it should be derived using front end code.
  def age is_nearest_age=false
    if birth_or_trust_date
      today = Date.today
      n_years = today.year - birth_or_trust_date.year
      if today.yday < birth_or_trust_date.yday
        n_years -= 1 unless is_nearest_age and (birth_or_trust_date.yday - today.yday) < (365 / 2)
      end
      n_years
    end
  end

  #Add formatting chars when accessing.
  #This is necessary because a string with only digits
  #is erroneously interpretted by the AngularJS front end as an integer,
  #and any leading zeros are collapsed.
  #This could be handled by padding with zeros on the front end,
  #but it happens that wherever ssn or tin are accessed, formatting does no harm.
  def formatted_ssn
    if self.entity_type==Enum::EntityType['person']
      orig_ssn.gsub(/\D/,'').insert(3,'-').insert(6,'-')
    else
      orig_ssn.gsub(/\D/,'').insert(2,'-')
    end
  rescue =>ex
    orig_ssn
  end

  def formatted_tin
    orig_tin.gsub(/\D/,'').insert(2,'-') rescue orig_tin
  end

  #strip non-digits when mutating
  def stripped_ssn= value
    self.orig_ssn= value.to_s.gsub(/\D/,'')
  end

  def stripped_tin= value
    self.orig_tin= value.to_s.gsub(/\D/,'')
  end

  def update_tasks
    if self.full_name_changed? && self.respond_to?(:assigned_tasks)
      self.person.assigned_tasks.update_all(assigned_to_name:full_name)
    end

    if self.changed.include?("primary_address_id") && self.respond_to?(:tasks)
      self.tasks.update_all(tz_offset:self.tz_offset)
    end
  end

  # Returns signed int for difference from GMT
  def tz_offset
    address = self.primary_address || self
    return nil unless address
    tz = nil
    # Get tz info based on zip code
    if address.is_a?(Address) && address.zip.present?
      zip = address.zip
      # search map for given zip. if not found, search for adjacent numbers
      if ZIP_TZ_MAP.has_key?(zip)
        tz = ZIP_TZ_MAP[zip]
      else
        trove = nil
        #Alternately seek backward, then forward, by increments of 1,
        #so that lower zip codes (which loosely correspond with lower longitude, or earlier time)
        #and nearer zip codes are both given preference, breaking out if a zip is found in the list.
        4.times{|i|
          plus_zip = (zip.to_i + 1).to_s
          minus_zip = (zip.to_i - 1).to_s
          if ZIP_TZ_MAP.has_key?(minus_zip)
            trove = ZIP_TZ_MAP[minus_zip]
            break
          elsif ZIP_TZ_MAP.has_key?(plus_zip)
            trove = ZIP_TZ_MAP[plus_zip]
            break
          end
        }
        tz = trove
      end
    end

    # Get tz info based on state if timezone find didn't work
    if address.state.present? && tz.nil?
      tz = STATE_TZ_MAP[address.state.try(:abbrev)]
    end

    if tz.respond_to?(:offset)
      return tz.offset
    end

    #if no valid timezone information is found, just return nil
    return nil
  end

  def birth_or_trust_date= value
    if value.is_a?(Date) || value.is_a?(Time)
      self[:birth_or_trust_date] = value
    elsif value.is_a?(String) && (Date.parse(value) rescue nil)
      self[:birth_or_trust_date] =Date.parse(value)
    elsif value.nil? || value.blank?
      self[:birth_or_trust_date]=nil
    else
      parsed_value=_parse_non_iso_date value
      self[:birth_or_trust_date] = parsed_value if parsed_value.is_a? Date
    end
    birth_or_trust_date
  end
  def birth; birth_or_trust_date; end
  def birth= x; self.birth_or_trust_date= x; end

  #DEPRECATED, but may remain for a while.
  #The issue it addresses is when API users don't bother reading our documentation.
  def _parse_non_iso_date value
    SystemMailer.deprecated_method_warning value rescue nil
    return nil if value.nil?
    invalid_msg='You entered an invalid birth or trust date. It should be ISO8601 format (YYYY-MM-DD), or American Date (MM/DD/YYYY).'
    alternate_patterns=[
      [/\d{1,2}\/\d{1,2}\/\d{4}/,"%m/%d/%Y"],
      [/\d{1,2}\/\d{1,2}\/\d{2}/,"%m/%d/%y"],
      [/\d{4}-\d{1,2}-\d{1,2}/,  "%Y-%m-%d"],
      [/\d{1,2}-\d{1,2}-\d{4}/,  "%m-%d-%Y"],
      [/\d{1,2}-\d{1,2}-\d{2}/,  "%m-%d-%y"],
      [/\d{6}/,                  "%m%d%Y"],
      [/\d{4}/,                  "%m%d%y"]
    ]
    matched_substring=nil
    alternate_patterns.each do |regex,format_string|
      matched_substring=value.match(regex)
      if matched_substring
        begin
          value=Date.strptime(matched_substring[0], format_string)
        rescue ArgumentError => e
          #catch validly formatted but still invalid dates
          warnings||=[]
          warnings<<invalid_msg
          value = nil
        end
        break
      end
    end
    if !matched_substring
      warnings||=[]
      warnings<<invalid_msg
      nil
    else
      value
    end
  end

  #We will continue to store and manipulate keys like `primary_x_id`, but not treat it as an association.
  #Almost everywhere that primary is needed, the entire list is loaded anyway.
  def set_initial_primary_contact_methods
    self.primary_address_id ||= addresses.first.try(:id)
    self.primary_email_id   ||= emails.first.try(:id)
    self.primary_phone_id   ||= phones.first.try(:id)
  end

  def primary_address
    addresses.select{|a| a.id==self.primary_address_id }.first ||
    addresses.first
  end
  alias_method :address, :primary_address#used only for marketing/status communications

  def primary_email
    emails.select{|a| a.id==self.primary_email_id }.first ||
    emails.first
  end
  alias_method :email, :primary_email#used only for marketing/status communications

  def primary_phone
    phones.select{|a| a.id==self.primary_phone_id }.first ||
    phones.first
  end
  alias_method :phone, :primary_phone#used only for marketing/status communications

  #Used for marketing templates
  #This returns the email that the mobile phone carrier provides for SMS via email.
  def carrier_provided_email
    mp=phones.select{|p| p.phone_type_id==Enum::PhoneType.id('mobile') && p.carrier }.first
    mp.try(:carrier_provided_email)
  end

  #Used for marketing templates
  def fax_number
    fp=phones.select{|p| p.phone_type_id==Enum::PhoneType.id('fax') }.first
    fp.try(:to_s)
  end

  #Used for marketing templates
  def website
    webs.reject{|x| x.web_type_id!= Enum::WebType.find_by_name('Website').id }.try(:first).try(:value)
  end
  def websites; webs; end
  def email_addresses; emails; end

  def email_is_dup_or_blank attrs; contact_method_is_dup_or_blank 'email', attrs; end
  def phone_is_dup_or_blank attrs; contact_method_is_dup_or_blank 'phone', attrs; end
  def address_is_dup_or_blank attrs; contact_method_is_dup_or_blank 'address', attrs; end
  def web_is_dup_or_blank attrs; contact_method_is_dup_or_blank 'web', attrs; end

  def contact_method_is_dup_or_blank assoc_name, cm_attrs
    irrelevant_keys=['id','created_at','updated_at', "#{assoc_name}_type_id"]
    cm_attrs=cm_attrs.stringify_keys.reject{|k,v| irrelevant_keys.include?(k) }

    return true if cm_attrs.all?{|k,v| v.blank? }

    matches_new_record= ->{
      #Checking `loaded?` first in order to prevent unnecessary queries.
      if self.association(assoc_name.pluralize).loaded?
        new_records=self.association(assoc_name.pluralize).reader.select{|r| r.new_record? }
      else
        new_records=self.association(assoc_name.pluralize).target
      end
      new_records.each do |r|
        r_attrs=r.attributes.reject{|k,v| irrelevant_keys.include?(k) || cm_attrs[k].blank? }

        return true if r_attrs==cm_attrs
      end
      false
    }
    matches_existing_record= ->{
      existing_records=self.association(assoc_name.pluralize).reader.select{|r| r.persisted? }
      existing_records.each do |r|
        r_attrs=r.attributes.reject{|k,v| irrelevant_keys.include?(k) || cm_attrs[k].blank? }
        return true if r_attrs==cm_attrs
      end
      false
    }
    if matches_new_record.call
      self.warnings||=[]
      self.warnings.push("#{assoc_name.capitalize} was skipped because its content duplicated another new record")
      return true
    end
    if self.persisted? && matches_existing_record.call
      self.warnings||=[]
      self.warnings.push("#{assoc_name.capitalize} was skipped because its content duplicated another existing record")
      return true
    end
  end
end
