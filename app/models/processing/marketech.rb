module Processing::Marketech
  def self.table_name_prefix
    'processing_marketech_'
  end

  CASE_PREFIX = 'DR'
  LEGACY_CASE_PREFIX = 'CLU' # used in conjunction w/ kase id to access legacy cases
end
