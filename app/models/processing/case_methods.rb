module Processing::CaseMethods
  extend ActiveSupport::Concern

  included do
    has_one  :docusign_case_data,     class_name:"Processing::Docusign::CaseDatum"
    has_one  :smm_status,             class_name:"Processing::Smm::Status",         foreign_key:'case_id'
    has_one  :marketech_dataset,      class_name:"Processing::Marketech::Dataset",  foreign_key:'case_id'

    has_many :marketech_updates,      class_name:"Processing::Marketech::Update",   foreign_key:'case_id'
    has_many :exam_one_case_data,     class_name:"Processing::ExamOne::CaseData",   foreign_key:'case_id'
    has_many :exam_one_statuses,      class_name:"Processing::ExamOne::Status",     foreign_key:'case_id'

    delegate :sendable,:editable,:loggable,:esign_url,:edit_url,:pdf_url,:log_url,
             to: :marketech_dataset, prefix: :marketech, allow_nil: true
  end

  def aw_account_key
    tenant_name=self.consumer.tenant.name rescue nil
    accounts=Processing::AgencyWorks::AW_ACCOUNTS
    accounts.has_key?(tenant_name) ? tenant_name : 'default'
  end

  def requirements
    requirements=self.custom_requirements.to_a

    if self.agency_works_id.present?
      service = Processing::AgencyWorks::CommentsService.new(self)
      service.request
      if service.error?
        self.errors.add :base, service.error
      end
      requirements+=service.requirements if service.requirements
    end

    requirements
  end

  def send_to_agency_works
    if !eligible_for_processing_with_agency_works
      return "Error: "+reasons_for_ineligibility_with_agency_works
    end
    if agency_works_id.present?
      error_msg="Error: Already sent to AgencyWorks"
      if date_processing_started(:agency_works).present?
        error_msg+="on #{ date_processing_started(:agency_works) }."
      else
        error_msg+='.'
      end
      return error_msg
    end
    Processing::AgencyWorks::SubmitService.new(self).send_to_agency_works
  end

  #Ensure presence of a +docusign_case_data+ object.
  #This allows us to use an x-editable field for +docusign_envelope_id+ in the admin area,
  #for easily linking manually entered AgencyWorks cases.
  def ensured_docusign_case_data
    self.create_docusign_case_data unless self.docusign_case_data.try(:persisted?)
    self.docusign_case_data
  end

  #Need this method because delegating the setter method apparently does not save the +docusign_case_data+ object.
  def docusign_envelope_id= value
    ensured_docusign_case_data.update_attribute :envolope_id, value
  end

  def docusign_envelope_id
    docusign_case_data.try(:envolope_id)
  end

  def marketech_carrier
    Processing::Marketech::Carrier.for_case(self).first
  end

  def marketech_signature
    self.agent.try(:agent_of_record).try(:marketech_signature)
  end
  
  def exam_one_case_datum
    self.exam_one_case_data.order(:created_at).last
  end

  def eligible_for_processing_with_scor ignore_signatures=false
    #The human readable names for these fields should be extra verbose,
    #because they will be seen by consumers.
    #Ie. "Failed to process. Driver's license number is required."
    @checked_eligibility_with_scor=true
    e=[]
    fields = {
      first_name:  consumer.first_name,
      last_name:   consumer.last_name,
      birth:       consumer.birth,
      birth_state_or_country: consumer.birth_state || consumer.birth_country,
      policy_face_amount: quoting_details.last.try(:face_amount),
      policy_duration: quoting_details.last.try(:duration),
      state:       consumer.state,
      email:       consumer.primary_email,
      address:     consumer.primary_address,
      address_zip: consumer.primary_address.try(:zip),
      address_city:consumer.primary_address.try(:city),
      phone:       consumer.primary_phone,
      drivers_license_number: consumer.dln,
      drivers_license_state: consumer.dl_state,
      consumer_signature: (ignore_signatures || date_signed_by_consumer),
      owner_signature: (ignore_signatures || insured_is_owner || date_signed_by_owner),
      payer_signature: (ignore_signatures || insured_is_payer || date_signed_by_payer),
    }
    fields.each do |k,v|
      prefix = k.to_s.humanize.capitalize
      e << "#{prefix} is required." unless v.present?
    end

    errors.add(:scor, e) if e.present?
    return e.length==0
  end

  def eligible_for_processing_with_exam_one
    @checked_eligibility_with_exam_one=true
    e=[]
    fields = {
      first_name:  consumer.first_name,
      last_name:   consumer.last_name,
      birth:       consumer.birth,
      face_amount: a_team_details.try(:face_amount),
      product_type:product_type || a_team_details.try(:product_type),
      email:       consumer.primary_email,
      address:     consumer.primary_address,
      address_state:consumer.state,
      address_zip:  consumer.primary_address.try(:zip),
      address_city: consumer.primary_address.try(:city),
    }
    fields.each do |k,v|
      prefix = k.to_s.humanize.capitalize
      e << "#{prefix} is required." unless v.present?
    end

    e << "There are no control codes available for this carrier (#{a_team_details.try(:carrier).try(:name)})." unless Processing::ExamOne::ControlCode.like(self).present?

    errors.add(:exam_one, e) if e.present?
    return e.length==0
  end


  # Returns true if this case has been sent to Marketech or Docusign
  def sent_to_vendor?
    self.docusign_case_data or self.marketech_dataset
  end

  def eligible_for_processing? service
    case service
    when :docusign
      eligible_for_processing_with_docusign
    when :agency_works
      eligible_for_processing_with_agency_works
    when :igo
     eligible_for_processing_with_igo
    when :marketech
      eligible_for_processing_with_marketech
    when :apps
      eligible_for_processing_with_apps
    when :apps_121
      eligible_for_processing_with_apps_121
    end
  end

  # Return true if given case has a Docusign carrier or already has a Docusign envelope
  def eligible_for_processing_with_docusign
    @checked_eligibility_with_docusign=true
    return true if docusign_case_data.present?
    e=[]
    if !a_team_details
      e<< 'This case lacks A-team details.'
    elsif !a_team_details.try(:carrier)
      e<< 'This case lacks a carrier.'
    elsif !Processing::Docusign::CARRIERS.any?{|c| a_team_details.carrier.name.to_s.starts_with?(c) }
      e<< "Docusign does not support this carrier (#{a_team_details.carrier.name})."
    end
    errors.add(:docusign, e) if e.present?
    return e.length==0
  end

  def eligible_for_processing_with_agency_works
    @checked_eligibility_with_agency_works=true
    e=[]
    e<< 'This case lacks App fulfillment details.' unless a_team_details.present?
    e<< 'This consumer lacks a state. Make sure their primary address is properly formed.' unless consumer.state.present? || consumer.primary_address.try(:state).present?

    errors.add(:agency_works, e) if e.present?
    return e.length==0
  end

  def eligible_for_processing_with_igo
    @checked_eligibility_with_igo=true
    e=[]
    e<< 'This consumer lacks an address.' if !consumer.try(:primary_address)
    if !a_team_details
      e<< 'This case lacks A-team details.'
    elsif !a_team_details.carrier
      e<< 'This case lacks a carrier.'
    elsif !Processing::Igo::CARRIERS.any?{|c| a_team_details.carrier.name.to_s.index(c) }
      e<< "iGo does not support this carrier (#{a_team_details.carrier.name})."
    end
    errors.add(:igo, e) if e.present?
    return e.length==0
  end

  def eligible_for_processing_with_marketech
    @checked_eligibility_with_marketech=true

    c_id =a_team_details.try(:carrier_id)
    s_id =(consumer.state || consumer.primary_address.try(:state)).try(:id)
    f_amt=a_team_details.try(:face_amount)

    e=[]
    e<< 'State must be present on client.'                                          unless s_id
    e<< 'Driver\'s license must be present on client.'                              unless consumer.dln
    e<< 'Driver\'s license state must be present on client.'                        unless consumer.dl_state_id
    e<< 'Birth must be set on client.'                                              unless consumer.birth
    e<< 'SSN must be set on client.'                                                unless consumer.ssn.present?
    e<< 'Zip must be set on client.'                                                unless consumer.primary_address.try(:zip)
    e<< 'Carrier must be present on A-team details.'                                unless c_id
    if e.present?
      errors.add(:marketech, e)
      return false
    end

    supports_carrier =Processing::Marketech::Carrier.where(carrier_id:c_id).count>0
    supports_state   =Processing::Marketech::Carrier.for_state_id(s_id).count>0
    supports_face_amt=Processing::Marketech::Carrier.for_face_amount(f_amt).count>0

    e<< 'Carrier is not supported by Marketech'                                    unless supports_carrier
    e<< 'State is not supported by Marketech'                                      unless supports_state
    e<< 'Face Amount is not supported by Marketech'                                unless supports_face_amt

    #if it's not supported by marketech, but none of the above 3 is completely unsupported, give more info
    if !marketech_carrier && e.empty?
      #find which combinations *would* work.
      supports_carrier_and_state   =Processing::Marketech::Carrier.where(carrier_id:c_id).for_state_id(s_id).count>0
      supports_carrier_and_face_amt=Processing::Marketech::Carrier.where(carrier_id:c_id).for_face_amount(f_amt).count>0
      supports_state_and_face_amt  =Processing::Marketech::Carrier.for_state_id(s_id).for_face_amount(f_amt).count>0

      e<< 'This Carrier and State combination is not supported by Marketech'       unless supports_carrier_and_state
      e<< 'This Carrier and Face Amount combination is not supported by Marketech' unless supports_carrier_and_face_amt
      e<< 'This State and Face Amount combination is not supported by Marketech'   unless supports_state_and_face_amt
      #if none of those combinations work
      e<< 'This Carrier/State/Face Amount combination is not supported by Marketech' unless e.present?
    end
    errors.add(:marketech, e) if e.present?
    return e.empty?
  end

  def eligible_for_processing_with_apps
    @checked_eligibility_with_apps=true
    e=[]
    addr=consumer.primary_address
    e<< 'Address must be present on client.'           unless addr.present?
    e<< 'Address must contain a city, state, and zip.' unless addr.nil? || ( addr.city && addr.state && addr.zip )
    e<<    'Birth must be set on client.'              unless consumer.birth_or_trust_date
    e<<    'Age must be >=60.'                         unless consumer.age.to_i>=60
    e<<    'SSN must be set on client.'                unless consumer.ssn.present?
    e<<    'Product type must be present.'             unless product_type_id || a_team_details.try(:product_type_id)
    if a_team_details
      e<<  'Carrier must be American General or AIG.' unless a_team_details.carrier.try(:name).to_s =~ /(American General|AIG)/
      e<<  'Product name must be AG-Select-A-Term.'   unless a_team_details.product_name =~ /AG.Select.A.Term/i
      e<<  'Product category must be present.'        unless a_team_details.duration
      e<<  'Face amount must be present.'             unless a_team_details.face_amount
      e<<  'Face amount must be >=$1 million.'        unless a_team_details.face_amount.to_i>=1000*1000
      e<<  'Modal premium must be present.'           unless a_team_details.modal_premium
      e<<  'Premium mode must be present.'            unless a_team_details.premium_mode
    else
      e<<  'A-team details must be present.'
    end
    errors.add(:apps, e) if e.present?
    return e.empty?
  end

  def eligible_for_processing_with_apps_121
    @checked_eligibility_with_apps_121=true
    e=[]
    requirements={
      #display name:   condition to check
      full_name:        consumer.full_name,
      ssn:              consumer.ssn,
      birth:            consumer.birth_or_trust_date,
      gender:           !consumer.gender.nil?,
      phone:            consumer.primary_phone,
      address:          consumer.primary_address,
      address_city:     consumer.primary_address.try(:city),
      address_state:    consumer.primary_address.try(:state),
      address_zip:      consumer.primary_address.try(:zip),
      face_amount:      current_details.try(:face_amount),
      eligible_carrier: current_details.try(:carrier).try(:naic_code),
      product_type:     current_details.try(:product_type),
      agent_phone:      agent.try(:phone),
      agent_email:      agent.try(:email),
    }
=begin
  #NEED TO FIGURE OUT WHAT THEY MEAN BY THIS:
  ?Policy Number
  ?Tracking ID
  APPS’ Account Number
  ?Agent code

  Agency Name (if ordering from an agency)
  Agency Code
  Agency Contact Information (phone and email)
=end
    requirements.each do |k,v|
      prefix = k.to_s.humanize.capitalize
      e << "#{prefix} is required." unless v.present?
    end
    errors.add(:apps_121, e) if e.present?
    return e.empty?

  end

  [:docusign,:agency_works,:igo,:marketech,:apps,:apps_121,:exam_one,:scor].each do |service|
    define_method "reasons_for_ineligibility_with_#{service.to_s}" do
      if !instance_variable_get("@checked_eligibility_with_#{service.to_s}")
        self.send("eligible_for_processing_with_#{service.to_s}")
      end
      ( errors[service].present? && errors[service].flatten.join("\n") )||''
    end
    define_method "date_processing_started_with_#{service.to_s}" do
      date_processing_started service
    end
  end

  def date_processing_started service
    case service
    when :docusign
      docusign_case_data.try(:created_at)
    when :agency_works
      sent_to_agency_works_at
    when :igo
      sent_to_igo
    when :marketech
      marketech_dataset.try(:created_at)||marketech_dataset.try(:updated_at) if marketech_dataset.try(:uploaded?)
    when :apps
      sent_103_to_apps_at
    when :apps_121
      sent_121_to_apps_at
    when :scor
      self[:date_processing_started_with_scor]
    end
  end
  alias_method :processing_started?, :date_processing_started

end
