require "savon"
require "builder"
# Service object for polling Velologica's SCOR App for
# an underwriting decision on a specific case.
# Currently used for the Motorists Group's RealTimeTerm quote path.
class Processing::Rtt::PollingService
  include AuditLogger::Helpers
  audit_logger_name 'rtt_process'
  include Processing::Rtt

  attr_accessor :policy,
                :scor_env,
                :start_time,
                :request_body,
                :response_body,
                :response_hash,
                :tobacco_class_name,
                :risk_class_name,
                :case_notes,
                :errors,
                :warnings,
                :underwriting_completed,
                :decision,
                :decision_reasons,
                :system_available

  def initialize(policy)
    self.errors     =ActiveModel::Errors.new(self)
    self.warnings   =[]
    self.policy     =policy
    self.scor_env   =Processing::Rtt::determine_scor_env policy
    self.start_time =Time.now
  end

  #This method makes logging the policy id simpler.
  def id
    policy.scor_guid
  end

  def check_underwriting_status
    self.request_body=compose_status_request
    self.response_hash=send_scor_soap_request self.request_body
    if self.errors.empty? && self.warnings.empty? && policy.sales_stage_id<Enum::SalesStage.id('Placed')
      update_policy_status
    end
  end

  def update_policy_status
    new_policy_status_type_id=nil
    case self.decision
    when 'ACCEPTED'
      if decision_reasons.present? && decision_reasons.include?("Policy Accepted - As Applied For")
        new_policy_status_type_id=Crm::StatusType::PRESETS[:rtt_scor_approved_as_applied][:id]
      else
        new_policy_status_type_id=Crm::StatusType::PRESETS[:rtt_scor_approved_ot_applied][:id]
      end
    when 'DECLINED'
      new_policy_status_type_id=Crm::StatusType::PRESETS[:rtt_scor_declined][:id]
    when 'REFERRED'
      new_policy_status_type_id=Crm::StatusType::PRESETS[:rtt_scor_underwriter_review][:id]
    when 'INFORMATION_REQUESTED'
      new_policy_status_type_id=Crm::StatusType::PRESETS[:rtt_scor_underwriter_review][:id]
    end
    if new_policy_status_type_id
      health_class_str = risk_class_name=='PF' ? 'Preferred ' : 'Standard '
      health_class_str+= tobacco_class_name=='SM' ? ' Tobacco' : 'Non-Tobacco'
      policy.update_attributes(status_type_id:new_policy_status_type_id)
      policy.reload.current_details.update_attributes(
        carrier_health_class:health_class_str,
        health_class_id:Enum::TliHealthClassOption.id(health_class_str)
      )
    end
  end

  def compose_status_request
    policy_id="IO_#{policy.scor_guid}"

    xml = Nokogiri::XML::Builder.new{|xml|
      xml.QDSStatusRequest(
        version:"1.0",
        :xmlns              =>"http://underwriting.qdsonline.com/statusReq",
        'xmlns:client'      =>"http://underwriting.qdsonline.com/client",
      ){
        xml.header{
          #Max length they allow for `transactionId` is 40 characters.
          xml.transactionId("IO_SCOR_#{start_time.strftime('%Y-%m-%d')}")
          xml.requestDate(start_time.iso8601)
          xml.requestingCompany{
            xml['client'].companyName('MOTORISTS LIFE INSURANCE COMPANY')
            xml['client'].companyIdentifier('MOTORISTS_LIFE')
            xml['client'].dealIdentifier1('MOTORISTS_DIRECT')
            xml['client'].password(APP_CONFIG['scor'][scor_env]["xml_password"])
          }
        }
        xml.body{
          #Can include one or more policy ids to check in a single request.
          xml.policyId(policy_id)
        }
      }
    }
    xml.to_xml
  end

=begin
    Structure of response (as observed in tests):
    {
      "QDSStatusResponse": {
        "version": "1.1",
        "xmlns:stat": "http://underwriting.qdsonline.com/status",
        "body": {
          "systemIsAvailable": "true",
          "dealExists": "true",
          "policyStatus": {
            "policyId": "IO_SCOR_f6063e64-c8a5-3507-7170-63da937eeecf",
            "policyExists": "false"
          },
          "validationErrors": {
            "validationError": "Application cannot be found."
          }
        }
      }
    }

    {
      "qds_status_response": {
        "version": "1.1",
        "xmlns:stat": "http://underwriting.qdsonline.com/status",
        "body": {
          "system_is_available": "true",
          "deal_exists": "true",
          "policy_status": {
            "policy_id": "IO_8141ab59-55e3-a235-a543-6a506d7217ca",
            "policy_exists": "true",
            "applicant_benefit_status": {
              "applicant_benefit_id": "3084667",
              "applicant_client_identifier": "IO_RTT_8531",
              "benefit_client_identifier": "8141ab59-55e3-a235-a543-6",
              "status_information": {
                "status": "COMPLETED"
              },
              "decision": "WITHDRAWN",
              "responses": {
                "response": {
                  "rule_id": "10820069",
                  "connector_response_id": "10820069",
                  "reason_code": "INVCLS",
                  "reason_description": "Invalid Risk or Smoker Class",
                  "category_code": "D",
                  "reason_source": "AP"
                }
              },
              "supporting_information": {
                "exceptions": {
                  "count": "0"
                },
                "applicant_response": {
                  "order_id": "20352146",
                  "data_type": "MIBCODEBACK",
                  "data": {
                    "type": "MIBCODEBACK",
                    "field": "N",
                    "records": {
                      "name": "mibcodes",
                      "record": {
                        "type": "mibcodebackdetail",
                        "field": "5.8.111#EN"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    Structure of response (from pages 24-25 of Velogica manual):
      {
        policy_id
        policy_exists
        applicant_benefit_status{
          applicant_benefit_id
          applicant_client_identifier
          benefit_client_identifier
          statusInformation{
            status
            statusProperty{
              key/value pairs containing additional info.
              Apparently the only currently supported key is PENDING_CAUSE with values:
                -WAITMV -waiting for MVR (motor vehicle report)
                -WAITDT - waiting on something other than MVR
            }
          }
          decision
          decisionDetails{
            property{
              name
              id
              (text node)
            }
            Properties UW_CLASS and UW_SMOKER will always be present.
            UW_CLASS indicates the approved risk class
            UW_SMOKER indicates the approved smoker class
          }
          supportingInformation
        }
      }

    Possible statuses: 'IN_PROGRESS' and 'COMPLETED'
    Possible decisions: ["ACCEPTED", "DECLINED", "REFERRED", "INFORMATION_REQUESTED", "WITHDRAWN"]
    Referred actually means they need to gather more info or get more forms from the insured.
    Withdrawn means some incompatibility between benefit and insured/bene info.
    <applicationStatus> element may include one or more <statusProperty> elements.
    For our purposes, there will only be one benefit per policy.

    We should collect useful description data in `self.case_notes`,
    which the controller can report back to the consumer,
    as well as creating note records on the policy.
=end
  def send_scor_soap_request request_body
    HTTPI.adapter = :curb
    client = Savon.client(
      #Normally endpoint and namespace are provided in a WSDL file,
      #but SCOR's docs indicate to just provide these directly.
      endpoint: APP_CONFIG['scor'][scor_env]['endpoint'],
      namespace:APP_CONFIG['scor'][scor_env]['status_namespace'],
      ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
      ssl_verify_mode: :none,
      ssl_version: :TLSv1,
      convert_request_keys_to: :none,
      headers: {'SOAPAction' => 'fetchStatus'},
      open_timeout: SOAP_SEND_TIMEOUT,
      read_timeout: SOAP_RECEIVE_TIMEOUT
    )
    op = client.operation(:fetchStatus)
    response = op.call message:{ 'content' => request_body }
    self.response_body=response.body
    content = response_body[:fetch_status_response][:fetch_status_return]
    raise "No content from SCOR SOAP response" unless content.present?
    self.response_hash=Hash.from_xml content
    self.response_hash=Processing::underscore_keys self.response_hash
    audit body:{ scor_env:scor_env, response_hash: response_hash }

    self.system_available       =response_hash.dig(:qds_status_response,:body,:system_is_available)=='true'
    self.underwriting_completed =response_hash.dig(:qds_status_response,:body,:policy_status,:applicant_benefit_status,:status_information,:status)=='COMPLETED'
    self.decision               =response_hash.dig(:qds_status_response,:body,:policy_status,:applicant_benefit_status,:decision)
    self.tobacco_class_name     =response_hash.dig(:qds_status_response,:body,:policy_status,:applicant_benefit_status,:decision_details,:property).try(:[],0)#['SM','NS']
    self.risk_class_name        =response_hash.dig(:qds_status_response,:body,:policy_status,:applicant_benefit_status,:decision_details,:property).try(:[],1)#['ST','PF']
    policy_does_not_exist       =response_hash.dig(:qds_status_response,:body,:policy_status,:policy_exists).to_s=='false'
    policy_not_found            =response_hash.dig(:qds_status_response,:body,:validation_errors,:validationError).to_s=="Application cannot be found."
    validation_error            =response_hash.dig(:qds_status_response,:body,:validation_errors,:validationError)
    status_responses            =response_hash.dig(:qds_status_response,:body,:policy_status,:applicant_benefit_status,:responses,:response)
    if status_responses.present?
      status_responses=[status_responses] if status_responses.is_a?(Hash)
      self.decision_reasons=status_responses.map{|r| r[:reason_description] }
    end

    if self.decision=='WITHDRAWN'
      if policy_does_not_exist ||policy_not_found
        self.warnings<< "Policy was never sent for processing. Send for processing first."
      elsif validation_error
        self.warnings<< validation_error
      elsif status_responses
        warning_msg=decision_reasons.join(',')
        self.warnings<< warning_msg
      end
    end
    response_hash[:qds_status_response]
  rescue Exception => ex
    app_backtrace=Rails.backtrace_cleaner.clean(ex.backtrace)
    audit level:'error', body:{
      scor_env:          scor_env, 
      message:           ex.message,
      exception_class:   ex.class.name,
      backtrace:         app_backtrace,
      raw_response_body: response_body,
      response_hash:     response_hash
    }
    ExceptionNotifier.notify_exception(ex)
    errors.add :base, ex.message
    return nil
  end


  SOAP_SEND_TIMEOUT    = 5#unit is seconds.
  SOAP_RECEIVE_TIMEOUT = 5

end