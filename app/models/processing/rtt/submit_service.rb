require "savon"
require "builder"
# Service object for sending a case to Velologica's SCOR App for underwriting
# and to Motorists Group for their documentation.
# Currently used for the Motorists Group's RealTimeTerm quote path.
class Processing::Rtt::SubmitService
  include ActiveModel
  include AuditLogger::Helpers
  audit_logger_name 'rtt_process'
  include Processing::Rtt

  attr_accessor :policy,
                :consumer,
                :rtt_answers,
                :scor_health_answers,
                :current_user,
                :scor_env,
                :start_time,
                :request_body,
                :response_body,
                :response_hash,
                :errors,
                :warnings

  def initialize policy
    self.errors              =ActiveModel::Errors.new(self)
    self.warnings            =[]
    self.policy              =policy
    self.consumer            =policy.consumer

    note_data                =Processing::Rtt.extract_rtt_and_scor_data_from_note policy
    self.rtt_answers         =note_data[:rtt_answers]
    self.scor_health_answers =note_data[:scor_health_answers]

    self.current_user        =policy.agent
    self.scor_env            =Processing::Rtt::determine_scor_env policy
    self.start_time          =Time.now
  end

  #The version defined in `ActiveSupport` does not honor options.
  #But it is important not to expose sensitive user data to the public.
  def as_json opts={}
    output=super.with_indifferent_access
    output=output.except(:current_user,:policy)
    output[:current_user_id]=current_user.try(:id)
    output[:policy_id]=policy.scor_guid
    output[:scor_env]=self.scor_env
    output
  end

  #This method makes logging the policy id simpler.
  def id
    policy.scor_guid
  end

  #This method gets called:
  #1a) when SCOR submission fails
  #    (either in `stakeholder_sign or in `save_and_sign_for_consumer`),
  #1b) or when SCOR returns an underwriting response,
  #2)  and again when the policy goes in force.
  def transfer_xml_file
    return if policy.is_test
    audit level:'info', body:"Called `transfer_xml_file` for policy #{policy.id} / #{policy.scor_guid}."
    file_content=prepare_output_for_motorists
    file_path="tmp/#{Rails.env}_#{policy.scor_guid}.xml"
    xml_file=File.write file_path, file_content
    gpg_cmd_to_issue="gpg -e -r #{APP_CONFIG['scor']['pgp_key_email']} #{file_path}"
    gpg_cmd_output=`#{gpg_cmd_to_issue}`
    if !File.exists? "#{file_path}.gpg"
      audit level:'error', body:{message:'`gpg` failed', gpg_cmd_issued:gpg_cmd_to_issue, gpg_cmd_output:gpg_cmd_output}
      gpg_error_msg="Failed to encrypt file #{file_path}.\n"+
        "Please contact agent #{policy.agent_id} #{policy.agent.full_name}."
      SystemMailer.warning(gpg_error_msg+"\n\n#{params.inspect}")
      return
    end
    port  =APP_CONFIG['scor']['ftp']['port']
    user  =APP_CONFIG['scor']['ftp']['username']
    server=APP_CONFIG['scor']['ftp']['server']
    path  =APP_CONFIG['scor']['ftp']['path']
    scp_cmd_to_issue="scp -P#{port} #{file_path}.gpg #{user}@#{server}:#{path} && echo 'success'"
    scp_cmd_output=`#{scp_cmd_to_issue}`
    if scp_cmd_output!="success\n"
      audit level:'error', body: {message:"`scp` failed.", scp_cmd_issued:scp_cmd_to_issue, scp_cmd_output:scp_cmd_output}
      scp_error_msg="Failed to transfer file #{file_path} to the RTT SFTP endpoint.\n"+
        "Please contact agent #{policy.agent_id} #{policy.agent.full_name}."
      SystemMailer.warning(scp_error_msg+"\n\n#{params.inspect}")
    else#delete the files from tmp folder
      `rm #{file_path}`
      `rm #{file_path}.gpg`
      audit "Transferred the xml file."
    end
  end

  #This method outputs a larger XML document, that includes the data in
  #SCOR XML, standard ACCORD XML, and pdf form attached inline with base64 encoding.
  #Called by `RttController#transfer_xml_file`, which drops the file in an sftp dropbox,
  #and by `RttController#app` (endpoint for viewing/verifying the output in browser).
  def prepare_output_for_motorists
    xml_body=compose_rtt_xml
    xml_body.sub '<TXLife>', '<TXLife xmlns="http://ACORD.org/Standards/Life/2">'
  end

  #This method sets policy timestamp and status, makes the request and interprets the response.
  #@returns boolean whether any errors occurred.
  def send_to_scor
    policy.date_processing_started_with_scor= Time.now
    policy.save
    policy.status_type_id= Crm::StatusType::PRESETS[:rtt_scor_submitted][:id]
    policy.save
    self.request_body=compose_scor_xml
    return false unless errors.empty?
    send_scor_soap_request(self.request_body)
    return false unless errors.empty?
    if response_hash.has_key?(:document_error)
      scor_error_msg=response_hash[:document_error].sub(/\[Location of invalid XML\:.*/,'')
      audit level:'error', body:{ scor_env:scor_env, scor_document_error: scor_error_msg }
      msg_for_support_staff="When attempting to submit RTT Policy #{policy.scor_guid} / #{policy.id}, "+
        "for consumer id #{consumer.id}, a formatting error was encountered. This is likely due to a mismatch in logic "+
        "between Insureio and SCOR which will require some investigation. Below is the error from SCOR:\n\n#{scor_error_msg}"
      SystemMailer.generic APP_CONFIG['email']['admin'], msg_for_support_staff, 'SCOR Submission Formatting Error' rescue nil
      errors.add :base, SUBMIT_ERROR_MSG
    elsif response_hash[:application_status][:status]=='DUPLICATE_APPLICATION'
      dup_warning="This policy has already been submitted to our underwriting service.\n"+
        "If you need to change any of the information already submitted to our underwriting service, "+
        "please contact your insurance agent for assistance.\n"
      self.warnings<< dup_warning
    elsif response_hash[:application_status][:status]!='SUCCESS'
      audit level:'error', body: {scor_env:scor_env, response_from_scor: response_hash }
      errors.add :base, SUBMIT_ERROR_MSG
    end
    errors.empty?
  rescue NoMethodError => ex
    app_backtrace =Rails.backtrace_cleaner.clean(ex.backtrace).join("\n")
    response_str  ="The processed response:\n#{self.response_hash}\nThe raw response:\n#{response_body}\n"
    message_str   ="Unable to interpret SCOR underwriting response for case:\n#{policy.id}\n#{response_str}"
    audit level:'error', body:{scor_env:scor_env, response_hash: response_hash, raw_response_body: response_body, error_msg: ex.message, error_backtrace: app_backtrace}
    SystemMailer.warning ex, message: message_str
    errors.add :base,     SUBMIT_ERROR_MSG
    false
  end

  private

  def scor_citizenship_code citizenship_id
    c=Enum::Citizenship.find(citizenship_id)
    if c.name.nil?
      'NC'#(not collected)
    elsif c.name=='US Citizen'
      'US'
    elsif c.name=='Permanent Resident'
      'PR'
    else
      'OT'#(other)
    end
  end

  #Built according to SCOR's documentation.
  #SCOR only wants the part required for underwriting. No beneficiary or payment data.
  def compose_scor_xml
    pi=consumer
    paa=pi.primary_address
    qda=policy.quoting_details.last
    srs=policy.stakeholder_relationships
    pi_id="IO_RTT_#{pi.id}"[0..24]
    policy_id="IO_#{policy.scor_guid}"

    xml = ::Builder::XmlMarkup.new(indent: 2, escape_attrs: true)
    xml.QDSUnderwritingRequest(
      version:"1.0",
      xmlns:"http://underwriting.qdsonline.com/request",
      'xmlns:ns2'=>"http://underwriting.qdsonline.com/client"
    ){
      xml.header{
        xml.transactionId(policy.scor_guid)
        xml.requestDate(start_time.iso8601)
        xml.requestingCompany{
          #These few elements will be namespaced under `client:`
          #using text replacement at the ened of the  `compose_scor_xml` method. 
          xml.companyName('MOTORISTS LIFE INSURANCE COMPANY')
          xml.companyIdentifier('MOTORISTS_LIFE')
          xml.dealIdentifier1('MOTORISTS_DIRECT')
          xml.password(APP_CONFIG['scor'][scor_env]["xml_password"])
        }
      }
      xml.body{
        xml.application(policyNumber:policy_id){
          xml.acceptDate(start_time.iso8601)
          xml.applicant(type:"PRIMARY", identifier:pi_id){
            xml.person(idref:pi_id)
            xml.gender( pi.gender_string.upcase )
            xml.birthDate( pi.birth_or_trust_date.iso8601 )
            xml.birthState( pi.birth_state.abbrev ) if pi.birth_state
            xml.birthCountry( (pi.birth_country||Enum::Country.find(1) ).two_char_code )
            xml.citizenship( scor_citizenship_code(pi.citizenship_id) )
            xml.height( (pi.feet*12)+pi.inches, uom:"inches" )
            xml.weight( pi.weight, uom:"lbs" )
            xml.occupation{
              xml.name(pi[:occupation])
              #Description must exist. Can be the name repeated.
              xml.description(pi.occupation_description.present? ? pi.occupation_description : pi.occupation)
            }
            dl_expired=pi.dl_expiration.present? && pi.dl_expiration.past?
            xml.driversLicense(type: (dl_expired ? '' : 'ACTIVE') ){
              xml.number(pi.dln)
              xml.state( pi.dl_state.abbrev )
            }
            xml.benefit{
              xml.effectiveDate( (policy.effective_date||1.day.from_now).iso8601 )
              xml.coverage(
                identifier:policy.scor_guid[0..24],
                benefitType:"BASE",
                plan:"TERM10",
                treaty:"MOT_01"
              ){
                xml.amount("%.2f" % qda.face_amount)
                xml.duration( qda.duration.years )
                xml.conditionalReceiptAmount( "%.2f" % (policy.conditional_binding_receipt_amount||qda.face_amount) )
                xml.isReplacementInsurance(policy.replacing ? 'true' : 'false')
                is_smoker=pi.health_info.attributes.any? do |k,v|
                  k.match(/^tobacco_(cig|pipe)/) &&
                  ( (v.is_a?(Integer) && v>0) || (!v.is_a?(Integer) && v) )
                end
                is_tobacco_user=pi.health_info.attributes.any? do |k,v|
                  k.match(/^tobacco_/) &&
                  ( (v.is_a?(Integer) && v>0) || (!v.is_a?(Integer) && v) )
                end
                xml.smokerClass(is_smoker ? 'SM' : 'NS')
                #SCOR gives no useful guidance as to whether to apply for preferred or standard,
                #and recommends a GUI dropdown. Instead, we'll count risk factors.
                multiple_risk_factors=scor_health_answers.select{|k,v| k.to_s.length==5 && v}.length>1
                xml.riskClass(multiple_risk_factors||is_tobacco_user ? 'ST' : 'PF')
              }
            }
            xml.signedMedicalAuthForm('true')
            xml.authSignedDate( start_time.strftime("%Y-%m-%d") )
            xml.applicantSignature{
              xml.date( start_time.strftime("%Y-%m-%d") )
              xml.city( paa.city[0..24] )
              xml.state( paa.state.abbrev )
            }

            xml.questionnaireResponse(
              state: paa.state.abbrev,
              questionnaireNumber: "RMM"
            ){
              scor_health_answers.each do |k,v|
                #Don't send details regarding "Other" questions.
                #These details will be sent to Motorists tho.
                next if k.to_s.match(/_detail$/)
                xml.questionResponse(questionNumber:k){
                  xml.answer(v ? 'Yes' : 'No')
                }
              end
            }
          }
          xml.agent(identifier:'Writing'){
            xml.name{
              xml.firstName(current_user.first_name)
              xml.lastName(current_user.last_name)
            }
            xml.agentNumber(current_user.id)
            relevant_l_num=current_user.licenses.in_effect.where(state_id:paa.state_id).first.try(:number)
            xml.agentLicense( relevant_l_num )
            xml.percentCommission("%.2f" % 100)
            xml.agentSignatureDate( start_time.strftime("%Y-%m-%d") )
          }
          owner_relationship=srs.select{|sr| sr.is_owner }.first
          if owner_relationship
            owner_id= SecureRandom.uuid
            xml.owner(identifier:owner_id){
              xml.person( idref:owner_id)
              xml.phone(owner.stakeholder.primary_phone.to_s.gsub(/[^\d-]/,'') )
              xml.taxType()
              xml.relationship(owner_relationship.relationship_type.name)
              #We do not have a field for ownership percentage,
              #only beneficiary percentage, so this will always be 100%.
              xml.ownershipPercent(100)
            }
          else
            xml.owner(identifier:pi_id){
              xml.person( idref:pi_id)
              xml.phone(pi.primary_phone.to_s.gsub(/[^\d-]/,'') )
              xml.taxType()
              xml.relationship('APPL')
              xml.ownershipPercent(100)
            }
          end
          xml.stateOfIssue(paa.state.abbrev)
          xml.individual(id:pi_id,identifier:pi_id){
            xml.name{
              xml.firstName(pi.first_name)
              xml.middleName(pi.middle_name)
              xml.lastName(pi.last_name)
            }
            xml.address{
              xml.addressLine1(paa.street)
              xml.city(paa.city)
              xml.state(paa.state.abbrev)
              xml.postalCode(paa.zip)
              xml.country('US')
            }
            xml.SSN( pi.orig_ssn.to_s )
          }
        }
      }
    }
    xml.target!.gsub(/(companyName|companyIdentifier|dealIdentifier1|password)>/){ |m| "ns2:#{m}" }
  rescue =>ex
    app_backtrace=Rails.backtrace_cleaner.clean(ex.backtrace)
    audit level:'error', body:{scor_env:scor_env, message: ex, backtrace: app_backtrace}
    self.errors.add :base, 'There was a problem building the request for underwriting.'
    nil
  end

  def code_for_state state_id
    #The default is the tc code Motorists Group gave us for "Other".
    Enum::State.find(state_id).try(:rtt_tc_code)||2147483647
  end

  def code_for_premium_mode premium_mode_id
    #
  end

  def years_since date_obj_or_str
    if date_obj_or_str.is_a?(String)
      date_obj=Date.strptime(date_obj_or_str)
    else
      date_obj=date_obj_or_str
    end
    (Date.today-date_obj).to_i/365
  end

  def rtt_xml_for_party_contact_methods xml, person
    pia=person.primary_address
    ppa=person.phones[0]
    spa=person.phones[1]
    pea=person.primary_email

    if pia.present?
      xml.Address{
        xml.Line1(pia.street)
        xml.City(pia.city)
        xml.AddressStateTC( pia.state.abbrev, tc:code_for_state(pia.state_id) )
        xml.Zip(pia.zip)
        if pia.county_name.present?
          xml.CountyName(pia.county_name)
        end
        if !pia.is_within_city_limits.nil?
          xml.OLifEExtension(VendorCode:"220"){
            xml.WithinCityLimits(pia.is_within_city_limits)
          }
        end
      }
    end
    if ppa.present?
      xml.Phone{
        xml.PhoneTypeCode(RTT_PHONE_TYPE_CODE_FOR_IO_PHONE_TYPE_ID[ppa.phone_type_id], tc:26)
        xml.AreaCode(ppa.value[0..2])
        xml.DialNumber(ppa.value[3..9])
      }
    end
    if spa.present?
      xml.Phone{
        xml.PhoneTypeCode(RTT_PHONE_TYPE_CODE_FOR_IO_PHONE_TYPE_ID[spa.phone_type_id], tc:27)
        xml.AreaCode(spa[:value][0..2])
        xml.DialNumber(spa[:value][3..9])
      }
    end
    if pea.present? && pea.value.present?
      xml.EMailAddress{
        xml.AddrLine(pea.value)
      }
    end
  end

  def rtt_xml_for_pi xml, person
    xml.Party(id:"Primary_Insured"){
      xml.PartyTypeCode('Person',tc:1)
      xml.GovtID(person.orig_ssn)
      pia=person.primary_address
      xml.ResidenceState( pia.state.abbrev, tc:code_for_state(pia.state_id) )
      xml.ResidenceZip(person.primary_address[:zip])
      xml.Person{
        xml.FirstName(person.first_name,tc:"X")
        xml.MiddleName(person.middle_name,tc:"X")
        xml.LastName(person.last_name,tc:"X")
        xml.MarStat(person.marital_status_id==Enum::MaritalStatus.id('Married') ? 'Married' : 'Single',tc:"X")
        xml.Gender(person.gender_string,tc:"X") #'Male' or 'Female'
        xml.BirthDate(person.birth_or_trust_date)
        xml.Age( years_since(person.birth_or_trust_date) )
        xml.Citizenship(person.citizenship_id==Enum::Citizenship.id('US Citizen') ? 'YES' : 'NO', tc:'X')
        xml.Height2{
          xml.MeasureUnits('Inches', tc:1)
          xml.MeasureValue( (person.feet*12)+person.inches )
        }
        xml.Weight2{
          xml.MeasureUnits('Pounds',tc:1)
          xml.MeasureValue()
        } #End Weight2
        xml.DriversLicenseNum(person.dln)
        xml.DriversLicenseState( person.dl_state.abbrev, tc:code_for_state(person.dl_state_id) )
      }

      rtt_xml_for_party_contact_methods xml, person

      xml.Employment{
        xml.EmploymentStatusTC(person.actively_employed, tc:'X')
        xml.Occupation(person.occupation)
      }
    }
  end

  def rtt_xml_for_agent xml, person, a_index=1
    pia=person.try(:primary_address)
    xml.Party(id:"Agent_#{a_index}"){
      xml.PartyTypeCode('Person',tc:1)
      xml.GovtID(person.orig_ssn)
      xml.ResidenceState( pia.try(:state).try(:abbrev), tc:code_for_state( pia.try(:state_id) ) )
      xml.ResidenceZip( pia.try(:zip) )
      xml.Person{
        xml.FirstName(person.first_name,tc:"X")
        xml.MiddleName(person.middle_name,tc:"X")
        xml.LastName(person.last_name,tc:"X")
      }
      rtt_xml_for_party_contact_methods xml, person
      xml.Producer{
        xml.CarrierAppointment{
          contract_number=person.contracts.where(carrier_id: Carrier::ID_FOR_MOTORISTS ).first.try(:carrier_contract_id)
          xml.CompanyProducerID(contract_number)
        }
        xml.Registration{
          xml.FirmName(person.company)
        }
      }
    }
  end

  def rtt_xml_for_other_stakeholder xml, role_name, role_code, sh_index, stakeholder_relationship
    person=stakeholder_relationship.stakeholder
    xml.Party(id:"#{role_name}_#{sh_index}"){
      xml.PartyTypeCode( person.entity_type.name,tc:1)
      xml.GovtID(person.orig_ssn)
      pia=person.primary_address
      xml.ResidenceState( pia.try(:state).try(:abbrev), tc:code_for_state(pia.try(:state_id) ) )
      xml.ResidenceZip( person.primary_address.try(:zip) )
      xml.Person{
        xml.FirstName(person.first_name,tc:"X")
        xml.MiddleName(person.middle_name,tc:"X")
        xml.LastName(person.last_name,tc:"X")
        xml.FullName(person.full_name,tc:"X")
      }
      rtt_xml_for_party_contact_methods xml, person
    }
  end

  def rtt_relation_element xml, role_name, role_code, sh_index, interest_percentage=0
    xml.Relation(
        id:"Relation_Primary_Insured_#{role_name}_#{sh_index}_#{role_code}",
        RelatedObjectID:"#{role_name}_#{sh_index}",
        OriginatingObjectID:"Primary_Insured"
    ){
      xml.InterestPercent(interest_percentage) if interest_percentage>0
      xml.OriginatingObjectType('Party',tc:6)
      xml.RelatedObjectType('Party',tc:6)
      xml.RelationRoleCode( role_name, tc:role_code)
    }
  end


  def compose_rtt_xml
    pi=consumer
    paa=pi.primary_address
    qda=policy.quoting_details.last
    srs=policy.stakeholder_relationships

    xml = ::Builder::XmlMarkup.new(indent: 2, escape_attrs: true)
    xml.TXLife {
      xml.UserAuthRequest{
        xml.UserLoginName(APP_CONFIG['scor'][scor_env]["xml_username"])
        xml.UserPswd {
          xml.CryptType('NONE')
          xml.Pswd(APP_CONFIG['scor'][scor_env]["xml_password"])
        }
      }
      xml.TXLifeRequest{
        xml.TransRefGUID('IO_RTT_'+policy.scor_guid+'TRGUID')
        xml.TransType("New Business Submission", tc: 103) #103 is the create code
        xml.TransExeTime( start_time.strftime("%H:%M:%S%z") )
        xml.OLifE{
          xml.Holding(id:"Application_Holding"){
            xml.HoldingTypeCode('Policy',tc:2)
            xml.Policy{
              #We will only be supporting the Life Insurance Product From Motorists, so these 3 fields are fixed values.
              xml.LineOfBusiness('Life',tc:1)
              xml.ProductCode(6584)
              xml.CarrierCode(5983)
              xml.PaymentMode( code_for_premium_mode(qda.premium_mode_id), tc:3)
              xml.PaymentAmt(qda.modal_premium)
              xml.PaymentMethod('EFT', tc:7)
              xml.Life{
                xml.FaceAmt(qda[:face_amount])
                xml.Coverage{
                  xml.PlanName(qda.product_name)
                  xml.LifeCovTypeCode('Term Life',tc:6)
                  xml.CovOption(id:"ABRRider"){#Always included for RTT apps.
                    xml.PlanName('Accelerated Benefit Rider')
                    xml.LifeCovOptTypeCode('Accelerated Benefit Rider',tc:2)
                  }
                  xml.LifeParticipant(PartyID:"Primary_Insured"){
                    xml.LifeParticipantRoleCode('Primary Insured', tc:1)
                    xml.UnderwritingClass(qda.carrier_health_class,tc:'X')
                    xml.UnderwritingResult{
                      map=Crm::StatusType::PRESETS
                      rtt_decision_statuses=policy.statuses.select do |s|
                        Crm::StatusType::RTT_SCOR_DECISION_STATUS_TYPE_IDS.include? s.status_type_id
                      end
                      decision_status=rtt_decision_statuses.last
                      scor_decision=case decision_status.try(:status_type_id)
                      when map[:rtt_scor_approved_as_applied][:id]
                        'ACCEPTED'
                      when map[:rtt_scor_approved_ot_applied][:id]
                        'ACCEPTED'
                      when map[:rtt_scor_declined][:id]
                        'DECLINED'
                      when map[:rtt_scor_underwriter_review][:id]
                        #May be  'REFERRED' or 'INFORMATION_REQUESTED', so it is necessary to repeat the request to SCOR.
                        service_obj=Processing::Rtt::PollingService.new policy
                        service_obj.check_underwriting_status
                        service_obj.decision
                      end
                      xml.Description(scor_decision)
                    }
                  } #End LifeParticipant
                  xml.OLifEExtension(VendorCode:"220"){
                    scor_xml=compose_scor_xml
                    #extract only the `application` node
                    scor_xml=scor_xml.match(/(<application.+<\/application>)/m)[1]
                    #add namespaces to it
                    scor_xml.sub!('<application','<application xmlns="http://underwriting.qdsonline.com/request" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"')
                    xml << scor_xml
                  }
                } #End Coverage
              } #End Life
              xml.ApplicationInfo{
                xml.CWAAmt('PAY_InitialPayment')
              } #End ApplicationInfo
            } #End Policy
          } #End Holding
          rtt_xml_for_pi xml, consumer

          rtt_xml_for_agent xml, current_user, 1
          rtt_relation_element xml, "Agent",11,1

          owners  =srs.select{|sr| sr.is_owner }
          payers  =srs.select{|sr| sr.is_payer }
          p_benes =srs.select{|sr| sr.is_beneficiary && !sr.contingent }
          c_benes =srs.select{|sr| sr.is_beneficiary &&  sr.contingent }

          owners.each_with_index do |o,i|
             rtt_xml_for_other_stakeholder xml, "Owner",8,i, o
             rtt_relation_element xml, "Owner",8,i
          end

          payers.each_with_index do |p,i|
             rtt_xml_for_other_stakeholder xml, "Payor",nil,i, p
             rtt_relation_element xml, "Payor",nil,i
          end

          p_benes.each_with_index do |b,i|
             rtt_xml_for_other_stakeholder xml, "Primary_Beneficiary",34,i, b
             rtt_relation_element xml, "Primary_Beneficiary",34,i, b.percentage
          end
          c_benes.each_with_index do |b,i|
            rtt_xml_for_other_stakeholder xml, "Contingent_Beneficiary",36,i, b
            rtt_relation_element xml, "Contingent_Beneficiary",36,i, b.percentage
          end

          xml.Relation(
            id:"Relation_Primary_Insured_Application_Holding_32",
            RelatedObjectID:"Application_Holding",
            OriginatingObjectID:"Primary_Insured"
          ){
            xml.OriginatingObjectType('Party',tc:6)
            xml.RelatedObjectType('Holding',tc:4)
            xml.RelationRoleCode('Insured',tc:32)
          } #End Relation

          xml.FormInstance(id:"Form_AgentReport-US"){
            xml.FormName('AgentReport-US')
            xml.ProviderFormNumber()
            xml.Attachment{
              xml.AttachmentBasicType('Image',tc:2)
              xml.Description()
              xml.AttachmentData(Base64.encode64( compose_rtt_pdf) )
              xml.ImageType('PDF', tc:4)
              xml.AttachmentLocation('Inline Data', tc:1)
            }
          }

        }#End OLifE
      }#End TXLifeRequest
    }#End TXLife
    xml.target!
  end

  def compose_rtt_pdf
    agent_signature, consumer_signature, owner_signature, payer_signature = compose_signatures
    contract_number=policy.agent.contracts.where(carrier_id: Carrier::ID_FOR_MOTORISTS ).first.try(:carrier_contract_id)
    html=ApplicationController.new.render_to_string(
      file: '/rtt/forms_wrapper.html',
      layout:false,
      formats:[:html],
      locals:{
        :@policy             =>policy,
        :@consumer           =>consumer,
        :@scor_health_answers=>scor_health_answers,
        :@rtt_answers        =>rtt_answers,
        :@license_number     =>policy.agent.licenses.in_effect.where(state_id: @consumer.primary_address.state_id ).first.try(:number),
        :@contract_number    =>contract_number,
        :@agent_signature    =>agent_signature,
        :@consumer_signature =>consumer_signature,
        :@owner_signature    =>owner_signature,
        :@payer_signature    =>payer_signature,
        :@is_for_pdf         =>true,
      }
    )
    pdf=WickedPdf.new.pdf_from_string html, margin:{top:'0.25in',bottom:'0.25in',left:'0.25in',right:'0.25in'}
  end

  def compose_signatures
    if !policy.date_signed_by_consumer
      agent_signature=consumer_signature='(Pending)'
    else
      agent_signature="<div class=\"consumer-data\" style=\"font-size:9px; line-height:10px;\"><em>eSigned by #{@policy.agent.full_name}</em></div>"

      consumer_signature      ="<div class=\"consumer-data\" style=\"font-size:9px; line-height:10px;\"><em>eSigned by #{@consumer.full_name}</em></div>"
    end

    if policy.insured_is_owner?
      owner_signature='(Not Applicable)'
    elsif policy.date_signed_by_owner
      owner_signature="<div class=\"consumer-data\" style=\"font-size:9px; line-height:10px;\"><em>eSigned by #{@policy.owner.full_name}</em></div>"
    else
      owner_signature='(Pending)'
    end

    if policy.insured_is_payer?
      payer_signature='(Not Applicable)'
    elsif policy.date_signed_by_payer
      owner_signature="<div class=\"consumer-data\" style=\"font-size:9px; line-height:10px;\"><em>eSigned by #{@policy.payer.full_name}</em></div>"
    else
      payer_signature='(Pending)'
    end

    [agent_signature, consumer_signature, owner_signature, payer_signature]
  end

  def send_scor_soap_request request_body
    HTTPI.adapter = :curb
    client = Savon.client(
      #Normally endpoint and namespace are provided in a WSDL file,
      #but SCOR's docs indicate to just provide these directly.
      endpoint: APP_CONFIG['scor'][scor_env]['endpoint'],
      namespace:APP_CONFIG['scor'][scor_env]['namespace'],
      ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
      ssl_verify_mode: :none,
      ssl_version: :TLSv1,
      convert_request_keys_to: :none,
      headers: {'SOAPAction' => 'underwrite'},
      open_timeout: SOAP_SEND_TIMEOUT,
      read_timeout: SOAP_RECEIVE_TIMEOUT
    )
    op = client.operation(:underwrite)
    response = op.call message:{ 'content' => request_body }
    self.response_body=response.body
    audit body:{scor_env:scor_env, raw_response_body: self.response_body }
    content = self.response_body[:underwrite_response][:underwrite_return]
    raise "No content from underwriting response" unless content.present?
    self.response_hash=Hash.from_xml content
    self.response_hash=Processing::underscore_keys self.response_hash
    audit body:{scor_env:scor_env, response_hash: self.response_hash }
    self.response_hash=self.response_hash[:qds_request_response]
=begin
    Submission receipts will contain either a `documentError` (if invalid XML) or `applicationStatus` element.
    `content[:qds_request_response][:application_status][:status]`
    Will be one of:
      -SUCCESS
      -BUSINESS_ERRORS -valid XML, but not in the expected format
      -UNABLE_TO_PROCESS -confirm valid credentials and values
      -DUPLICATE_APPLICATION -make an update request instead if that was the intent
      -INVALID_TREATY_PLAN -wrong credentials and contract
    For non-success status, will provide further detail in key `:document_error`.
=end
  rescue Curl::Err::ConnectionFailedError => ex
    audit level:'error', body:{scor_env:scor_env, exception_class:ex.class.name, message:ex.message,backtrace:ex.backtrace,request_body:request_body}
    ExceptionNotifier.notify_exception(ex)
    errors.add :base, "Couldn't connect to underwriting service."
    nil
  rescue Savon::SOAPFault => ex#Would be raised by the `op.call` method.
    fault_code = ex.to_hash[:fault][:faultcode]
    audit level:'error', body:{scor_env:scor_env, exception_class:ex.class.name, message:ex.message,backtrace:ex.backtrace,request_body:request_body}
    errors.add :base, "Couldn't connect to underwriting service."
    nil
  rescue Exception => ex
    app_backtrace=Rails.backtrace_cleaner.clean(ex.backtrace)
    audit level:'error', body:{scor_env:scor_env, exception_class:ex.class.name, message:ex.message,backtrace:app_backtrace,request_body:request_body,
      raw_response_body: response_body, response_hash: response_hash}
    ExceptionNotifier.notify_exception(ex)
    errors.add :base, SUBMIT_ERROR_MSG+ex.message
    nil
  end

end