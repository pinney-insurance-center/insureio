require "savon"

module Processing::AgencyWorks
  class AgencyWorksSubmitError < StandardError; end

  def self.table_name_prefix
    'processing_agency_work_'
  end

  include AuditLogger::Helpers
  audit_logger_name 'aw'

protected

  # This refers to +@response_hash+, which should be set on a class that includes this module
  # Returns +true+ if <tt>result_code == 'Failure'</tt>
  def error?
    [:user_auth_response, :tx_life_response].any? do |key|
      @response_hash.dig(:tx_life, key, :trans_result, :result_code) == "Failure"
    end
  end

  def warning?(response)
    response.dig(:tx_life,:tx_life_response,:trans_result,:result_info_code)=="Warning"
  end

  def confirmation_id(response)
    audit "Response hash:\n#{response.pretty_inspect}"
    response[:tx_life][:tx_life_response][:trans_result][:confirmation_id]
  end

  def send_soap_request(request_body)
    client = Savon.client(
      wsdl:File.join(Rails.root, 'lib/agency_works/AcordService.wsdl'),
      ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
      ssl_verify_mode: :none,
      ssl_version: :TLSv1_2,
      convert_request_keys_to: :none,
      headers: {'SOAPAction' => ''}, # AW no longer supports this header, but rather chokes on it <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>The given SOAPAction tXLifeOperation does not match an operation.</faultstring></soap:Fault></soap:Body></soap:Envelope>
      open_timeout: SOAP_SEND_TIMEOUT,
      read_timeout: SOAP_RECEIVE_TIMEOUT
    )
    op = client.operation(:t_x_life_operation)
    response = op.call message:{ 'content' => request_body }
    content = response.body[:t_x_life_operation_response][:return][:message]
    raise "No content from AgencyWorks SOAP response" unless content.present?
    Processing::underscore_keys Hash.from_xml content
  rescue Exception => ex
    ExceptionNotifier.notify_exception(ex)
    audit level:'error', body:{exception_class:ex.class.name, message:ex.message, backtrace:ex.backtrace[1]}
    return {tx_life:{user_auth_response:{trans_result:{result_code:"Failure"}}}}
  end

  RELATIONSHIP_TYPE_AW_CODE_MAP={
    spouse_domestic_partner:1,#Since in most states these are already legally equivalent, we just treat them as one thing.
    child:          2,
    parent:         3,
    sibling:        4,
    grand_child:    93,
    step_parent:    131,
    step_child:     94,
    trust:          69,
    creditors:      109,
    other:          2147483647
  #AgencyWorks designations that we do not utilize (here for documentation only):
  #  domestic_partner: 15,
  #  grand_parent:   92,
  #  relative:       5,
  #  friend:         14,
  }

  STATE_AW_CODE_MAP={
    "Alabama"=>1,
    "Alaska"=>2,
    "Arizona"=>4,
    "Arkansas"=>5,
    "California"=>6,
    "Colorado"=>7,
    "Connecticut"=>8,
    "Delaware"=>9,
    "District of Columbia"=>10,
    "Florida"=>12,
    "Georgia"=>13,
    "Hawaii"=>15,
    "Idaho"=>16,
    "Illinois"=>17,
    "Indiana"=>18,
    "Iowa"=>19,
    "Kansas"=>20,
    "Kentucky"=>21,
    "Louisiana"=>22,
    "Maine"=>23,
    "Maryland"=>25,
    "Massachusetts"=>26,
    "Michigan"=>27,
    "Minnesota"=>28,
    "Mississippi"=>29,
    "Missouri"=>30,
    "Montana"=>31,
    "Nebraska"=>32,
    "Nevada"=>33,
    "New Hampshire"=>34,
    "New Jersey"=>35,
    "New Mexico"=>36,
    "New York"=>37,
    "North Carolina"=>38,
    "North Dakota"=>39,
    "Ohio"=>41,
    "Oklahoma"=>42,
    "Oregon"=>43,
    "Pennsylvania"=>45,
    "Rhode Island"=>47,
    "South Carolina"=>48,
    "South Dakota"=>49,
    "Tennessee"=>50,
    "Texas"=>51,
    "Utah"=>52,
    "Vermont"=>53,
    "Virginia"=>55,
    "Washington"=>56,
    "West Virginia"=>57,
    "Wisconsin"=>58,
    "Wyoming"=>59,
    "Guam"=>14,
    "Puerto Rico"=>46,
    "Virgin Islands"=>54,
    "American Samoa"=>3
  }

  SOAP_SEND_TIMEOUT    = 20#unit is seconds.
  SOAP_RECEIVE_TIMEOUT = 20

  AW_ACCOUNTS=APP_CONFIG['agency_works']['accounts']

end
