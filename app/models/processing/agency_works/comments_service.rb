require "savon"

# Service object for polling AgencyWorks for comments
class Processing::AgencyWorks::CommentsService

  include Processing::AgencyWorks
  include ActiveModel::Validations
  include AuditLogger::Helpers
  audit_logger_name 'aw_reqs_n_comments'

  attr_accessor :error, :notes, :request_body, :response_body, :requirements

  def initialize kase
    @kase = kase
  end

  # Fetch comments from AgencyWorks for @kase
  # Set @notes : an array of Note, which equates to AW 'Comments'
  # Set @requirements : nil or an array of Crm::CaseRequirement, which equates to AW 'Case Requirements'
  # @return @notes or an empty Array
  def request
    if @kase.agency_works_id.blank?
      raise("Attempted to get AgencyWorks Case Requirements and Notes for a case that lacks an AgencyWorks Id.")
    end
    self.request_body= Processing::AgencyWorks::XmlBuilder.new.build_holding_inquiry @kase
    audit "Request body:\n#{request_body}"
    self.response_body = send_soap_request(request_body)
    audit "Response body:\n#{response_body}"
    if error?
      return []
    else
      # Set @requirements
      extract_reqs
      # Set and return @notes
      extract_notes
    end
  rescue => ex
    self.error= ex.message
    ExceptionNotifier.notify_exception(ex)
    audit level: :error, body: "#{ex.message}\n#{ex.backtrace}"
    return []
  end

  def extract_reqs
    requirement_infos = response_body.dig(:tx_life,:tx_life_response,:o_lif_e,:holding,:policy,:requirement_info)
    return unless requirement_infos
    requirement_infos = [requirement_infos] unless requirement_infos.is_a?(Array)
    @requirements = requirement_infos.map do |info|
      begin
        waived_data = info.dig(:o_lif_e_extension,:requirement_waived)
        is_waived = waived_data.is_a?(Hash) && waived_data[:tc].to_i == 1
        Crm::CaseRequirement.new({
          created_by_user_or_service_name: 'AgencyWorks',
          id:           info[:requirement_info_unique_id].to_i,
          case_id:      @kase.id,
          name:         info[:requirement_details],
          note:         info.dig(:attachment, :attachment_type) == 'General note' ? info.dig(:attachment, :attachment_data) : nil,
          ordered_at:   (Date.parse info[:requested_date] rescue Date.new),
          completed_at: (Date.parse info[:received_date] rescue Date.new),
          created_at:   (Date.parse( info.dig(:o_lif_e_extension,:requirement_create_date) ) rescue Date.new),
          requirement_type_id: Enum::CaseRequirementType.id( info[:o_lif_e_extension].try(:[], :requirement_type) ),
          responsible_party_type_id: Enum::CaseReqResponsiblePartyType.id(info[:responsible_party_type]),
          status_id: is_waived ? Enum::CaseReqStatus.id('Waived') : Enum::CaseReqStatus.id(info[:req_status]),
        })
      rescue Exception => ex
        errors.add(:requirements_parser, ex.message)
        ExceptionNotifier.notify_exception(ex, data: info)
        audit level: :error, body: "#{'='*30}\n#{ex.message}\nRequirement causing issue: #{info}\nEntire requirement list: #{requirement_infos}"
        nil # return nil
      end
    end.compact
  end

  def extract_notes
    note_threads = response_body.dig(:tx_life,:tx_life_response,:o_lif_e,:holding,:attachment)
    return [] unless note_threads
    note_threads = [note_threads] unless note_threads.is_a?(Array)
    @notes = note_threads
      .select{|nt| nt[:id] =~ /^Comment/ && nt[:attachment_basic_type] == "Text" }
      .map{|nt| extract_notes_from_thread nt }
      .flatten!#Each thread is processed into an array of notes. Flatten all threads into a single array of individual notes.
  end

  def extract_notes_from_thread note_thread
    nt= note_thread[:o_lif_e_extension]
    thread_desc= nt[:entries][:description]
    str_to_remove="table, th, td {border: 0px; margin: 0px; padding: 0px; vertical-align: middle;border-collapse:collapse;border-spacing: 0px;font-family: arial,helvetica,sans-serif;font-size:x-small;}div {padding: 0px; margin: 0px; border: 0px;}"
    confidential_flag = nt[:confidential_flag]
    # Under legacy cases, flag can be "X". Under new cases, flag should be {:tc => "0"} or {:tc => "1"}
    # "X" means true, according to Brian Zabriskie.
    is_confidential= confidential_flag == "X" || confidential_flag.is_a?(Hash) && confidential_flag[:tc] == "1"

    entry_array=nt[:entries][:entry].is_a?(Array) ? nt[:entries][:entry] : [nt[:entries][:entry]]
    notes_for_thread=entry_array.map do |n|
      note = Note.new
      n[:content]=n[:content].to_s.sub(str_to_remove,'')
      begin
        #Their format: "2017-05-09-10.37.39.023888". Our format: "2017-05-09 10:37".
        begin
          ts_pieces=n[:created].match(/(\d{4}-\d{2}-\d{2})-(\d{2}).(\d{2})/)
          formatted_timestamp="#{ts_pieces[1]} #{ts_pieces[2]}:#{ts_pieces[3]}"
          parsed_timestamp=Time.zone.parse formatted_timestamp
        rescue =>e
          parsed_timestamp=nil
        end

        note.id           = n[:id].gsub(/\D/,'').to_i
        note.text         = "RE:#{thread_desc}\n#{n[:content]}"
        note.created_at   = parsed_timestamp
        note.creator      = User.new(full_name:n[:created_by])
        note.confidential = is_confidential
      rescue Exception => ex
        ExceptionNotifier.notify_exception(ex, data: n)
        audit level: :error, body:"#{'='*30}\n#{ex.message}\nNote causing issue: #{n}\nEntire note thread: #{note_thread}"
      end
      note
    end
    notes_for_thread
  end

  def result_code
    response_body && response_body.dig(:tx_life,:tx_life_response,:trans_result,:result_code)
  end

  def error?
    if result_code =~ /Failure|Warning/
      self.error = response_body.dig(:tx_life,:tx_life_response,:trans_result,:result_info,:result_info_desc)
    end
    return self.error.present?
  end

end
