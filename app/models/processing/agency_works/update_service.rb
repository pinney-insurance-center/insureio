require "savon"

# Service object for polling AgencyWorks for status updates
class Processing::AgencyWorks::UpdateService

  include AuditLogger::Helpers
  audit_logger_name 'agency_works'

  include ::Processing::AgencyWorks

  attr_accessor :start_time,
                :status_types,
                :xml_policy_ids,
                :unmatched_ids,
                :dr_policy_ids,
                :lack_status_ids,
                :attempted_status_change_ids,
                :successful_status_change_ids,
                :attempted_details_change_ids,
                :successful_details_change_ids,
                :request_body,
                :response_hash,
                :aw_policies,
                :io_policies,
                :errors,
                :warnings

  def initialize
    self.start_time = Time.now
    self.errors                        =[]
    self.warnings                      =[]
    self.status_types                  ={}#{ 'AW status name' => <Crm::StatusType> }
    self.aw_policies                   =[]#hashes from response xml
    self.io_policies                   =[]#Insureio policy records
    self.xml_policy_ids                =[]#aw ids
    self.unmatched_ids                 =[]#aw ids
    self.dr_policy_ids                 =[]#Insureio ids
    self.lack_status_ids               =[]#Insureio ids
    self.attempted_status_change_ids   =[]#Insureio ids
    self.successful_status_change_ids  =[]#Insureio ids
    self.attempted_details_change_ids  =[]#Insureio ids
    self.successful_details_change_ids =[]#Insureio ids

    preload_status_types#populates self.status_types
  end

  #Create a Sidekiq job for each AW Status that we care about.
  #Exits early if redis is unavailable, or if too many updates are already started or queued.
  #Skips any status codes that are already started or queued.
  def self.request_all_updates
    if !$redis
      warning_msg='Cancelled updates due to a missing connection to redis.'
      @warnings= [warning_msg]
      SystemMailer.warning warning_msg
      audit warning_msg
      return
    end

    account_status_combos_to_schedule=self.determine_which_to_schedule

    AwUpdateWorker.sidekiq_options_hash['queue']=:aw_updates
    AwUpdateWorker.sidekiq_options_hash['retry']=false

    account_status_combos_to_schedule.each do |aw_account_key,status_type_code|
      #The worker calls `request_single_update` in a separate thread when the job queue allows.
      AwUpdateWorker.perform_at(6.seconds.from_now, status_type_code, aw_account_key)
    end
  end

  #Collect stats on any outstanding update jobs.
  #Log that info.
  #Return an array of account, status code combos,
  #excluding those still outstanding.
  def self.determine_which_to_schedule
    scheduled =Sidekiq::ScheduledSet.new.map{|j| j.item }.select{|j| j['class']=='AwUpdateWorker' }
    queued    =Sidekiq::Queue.new('aw_updates').map{|j| j.item }
    started   =Sidekiq::Workers.new.entries.map{|w| w[1]['payload'] }.select{|w| w['class']=="AwUpdateWorker" }
    total =scheduled.length+queued.length+started.length

    scheduled_combos =scheduled.map{|sc| sc['args'] }.uniq
    queued_combos    =queued.map{|sc|    sc['args'] }.uniq
    started_combos   =started.map{|sc|   sc['args'] }.uniq

    half_of_jobs_to_generate=ALL_ACCOUNT_STATUS_COMBOS.length/2

    if total > half_of_jobs_to_generate
      summary_of_outstanding={
        total: total,
        scheduled_combos:scheduled_combos,
        queued_combos:   queued_combos,
        started_combos:  started_combos,
      }
      warning_msg="Cancelled updates due to too many still in progress.\nHost:#{(`hostname`||'').sub("\n",'')}\n#{ summary_of_outstanding.to_json }\n"
      @warnings= [warning_msg]
      SystemMailer.warning warning_msg
      audit warning_msg
      return []
    end

    account_status_combos_to_schedule =ALL_ACCOUNT_STATUS_COMBOS

    if scheduled_combos.present?
      account_status_combos_to_schedule-=scheduled_combos
      msg="Skipping already scheduled aw account and status code combos: #{scheduled_combos}."
      audit msg
    end
    if queued_combos.present?
      account_status_combos_to_schedule-=queued_combos
      msg="Skipping already queued aw account and status code combos: #{queued_combos}."
      audit msg
    end
    if started_combos.present?
      account_status_combos_to_schedule-=started_combos
      msg="Skipping already started aw account and status code combos: #{started_combos}."
      audit msg
    end
    account_status_combos_to_schedule
  end

  # Called by `AwUpdateWorker.perform` to run in its own thread when the job queue allows.
  # Issue a single SOAP request and add policies in its response to aw_policies.
  # Update `Crm::Case` statuses according to statuses in aw_policies.
  def request_single_update status_type_code, aw_account_key
    self.class.audit_logger_name "aw_updates_for_account_#{aw_account_key}_status_#{status_type_code}"
    self.request_body = Processing::AgencyWorks::XmlBuilder.new.build_request_for_updates(status_type_code,aw_account_key)
    audit "Request: #{request_body}"
    self.response_hash = send_soap_request(request_body)
    audit body:{response: response_hash}
    audit "If no response is logged above, the request may have timed out or received an invalid response."
    if error?
      audit level:'error', body:"Error encountered while attempting to acquire case statuses from AgencyWorks."
    else
      extract_policies!
    end
    if self.aw_policies.present?
      self.xml_policy_ids = aw_policies.map{|p| p[:aw_id] }
      self.io_policies = Crm::Case.where(agency_works_id: xml_policy_ids).preload(:statuses).to_a
      aw_policies.each do |aw_policy|
        apply_update aw_policy
      end
    end
    log_totals
  rescue Exception => ex
    exception_msg="Encountered an exception: #{ex.message}\n#{ex.backtrace[1]}"
    errors << exception_msg
    audit level:'error', body:{exception_class:ex.class.name, message:ex.message, backtrace:ex.backtrace}
    ExceptionNotifier.notify_exception(ex)
  end

private

  # Extract Array of policy hashes from `response_hash`,
  # then set key `:aw_id`, which corresponds to `Crm::Case.agency_works_id`.
  def extract_policies!
    self.aw_policies=response_hash.dig(:tx_life,:tx_life_response,:o_lif_e,:holding)
    #Since arrays of one item in XML look the same as just the item, we must wrap in an array.
    self.aw_policies=[self.aw_policies] if self.aw_policies.is_a?(Hash)
    if self.aw_policies.is_a?(Array)
      self.aw_policies.map! do |aw_policy|
        aw_policy = aw_policy[:policy]#we only need the data in `policy` node.
        aw_policy[:aw_id] = aw_policy[:application_info][:tracking_id].gsub("AW_", "")
        aw_policy
      end
    else # Move on if there are no policies in +:o_lif_e+
      self.aw_policies=[]
    end
  rescue => ex
    error_msg="Error encountered while attempting to acquire case updates from AgencyWorks. #{ex.message}"
    errors << error_msg
    audit level:'error', body:{exception_class:ex.class.name, message:ex.message, backtrace:ex.backtrace, response_hash:response_hash}
    ExceptionNotifier.notify_exception(ex)
  end

  # Return nil or Fixnum representing id of Crm::StatusType
  # Called from `apply_update` once per policy.
  def extract_status_type_id aw_policy
    aw_status_name=aw_policy[:policy_status]
    insureio_status_name=aw_status_name.gsub('.','').gsub(/\W+/,' ').titleize
    if !status_types.has_key?(insureio_status_name)
      audit "Unrecognized AgencyWorks status: #{aw_status_name}. This could indicate an inadequacy in the configuration of Insureio module.", level: :warn
      return nil
    elsif status_types[insureio_status_name].nil?
      #Keys that are present and explicitly set to nil represent
      #AW statuses which we know about and choose not to mirror in Insureio.
      return nil
    end
    status_types[insureio_status_name].id
  end

  def log_totals
    execution_time = Time.now - start_time rescue -1
    lists=['xml_policy_ids', 'unmatched_ids', 'dr_policy_ids','lack_status_ids', 'attempted_status_change_ids', 'successful_status_change_ids', 'attempted_details_change_ids', 'successful_details_change_ids']
    lists.map do|l_name|
      l=self.instance_variable_get "@#{l_name}"
      l_count=l.length.to_s.rjust(4,' ')
      self.instance_variable_set "@#{l_name.sub('ids','ct')}", l_count
    end
    audit body:{
      "XML Records"=>               {count: @xml_policy_ct,                list:xml_policy_ids},
      "Unmatched Records"=>         {count: @unmatched_ct,                 list:unmatched_ids},
      "Matched Records"=>           {count: @dr_policy_ct,                 list:dr_policy_ids},
      "Records Lacking Status"=>    {count: @lack_status_ct,               list:lack_status_ids},
      "Attempted Status Changes"=>  {count: @attempted_status_change_ct,   list:attempted_status_change_ids},
      "Successful Status Changes"=> {count: @successful_status_change_ct,  list:successful_status_change_ids},
      "Attempted Details Changes"=> {count: @attempted_details_change_ct,  list:attempted_details_change_ids},
      "Successful Details Changes"=>{count: @successful_details_change_ct, list:successful_details_change_ids},
      "Execution time in minutes"=> (execution_time/60).to_s(:delimited),
      "Execution time in seconds"=> execution_time.to_s(:delimited),
    }
  end

  #Potentially changes a policy's status, plan_name, modal_premium, policy_number, and effective_date.
  def apply_update aw_policy
    io_policy = io_policies.find{|cd| cd.agency_works_id.to_i == aw_policy[:aw_id].to_i }
    if io_policy.nil?
      self.unmatched_ids << aw_policy[:aw_id]
      return
    end
    self.dr_policy_ids << io_policy.id
    # determine old and new Crm::StatusType ids
    old_status_type_id = io_policy.status.try(:status_type_id)
    new_status_type_id = extract_status_type_id(aw_policy)
    audit level:'info', body:{
      aw_policy:aw_policy[:aw_id],
      io_policy:io_policy.id,
      aw_status: aw_policy[:policy_status],
      old_io_status_type_id:io_policy.id,
      new_io_status_type_id:new_status_type_id
    }
    if new_status_type_id.nil?
      self.lack_status_ids << io_policy.id
      return
    end

    if aw_policy[:pol_number].present? && io_policy.policy_number.blank?
      io_policy.update_attributes({policy_number: aw_policy[:pol_number]})
    end
    aw_placed_date=aw_policy.dig(:application_info,:o_lif_e_extension,:placed_date)
    aw_placed_date=(aw_placed_date.present? &&
                    aw_placed_date>"0001-01-01" &&
                    Date.parse(aw_placed_date) ) rescue nil
    if aw_placed_date.present? && io_policy.effective_date.blank?
      io_policy.update_attributes(effective_date: aw_placed_date)
    end
    # update counts and Case.status if old and new Crm::StatusType ids differ
    if new_status_type_id != old_status_type_id
      self.attempted_status_change_ids << io_policy.id
      updated_st_ok=io_policy.update_attributes(status_type_id:new_status_type_id)
      self.successful_status_change_ids << io_policy.id if updated_st_ok
    end
    io_policy.reload
    cd=io_policy.current_details
    aw_details_fields={
      plan_name:     aw_policy[:plan_name],
      modal_premium: aw_policy[:payment_amt],
      premium_mode_id:  PREMIUM_MODE_NAMES[ aw_policy[:payment_mode] ],
    }
    aw_details_fields=aw_details_fields.reject{|k,v| cd.send(k)==v }
    if aw_details_fields.present?
      self.attempted_details_change_ids << io_policy.id
      updated_details_ok=cd.update_attributes(aw_details_fields)
      self.successful_details_change_ids << io_policy.id if updated_details_ok
    end
  rescue => ex
    error_msg="Error encountered while attempting to apply updated data to policy with aw id #{aw_policy[:aw_id]}."
    errors << error_msg
    audit level:'error', body:{exception_class:ex.class.name, message:ex.message, backtrace:ex.backtrace}
    ExceptionNotifier.notify_exception(ex)
  end

  #This method loads the relevant status type records into memory once per instance,
  #for quick recall, and to avoid duplicate queries to the database.
  #It populates `@status_types` as a mapping between AW status names and their corresponding records.
  def preload_status_types
    status_map=Crm::StatusType::PRESETS
    status_map.each do |key, id_and_note|
      key_match_obj=key.to_s.match(/\Aaw_(.+)/)
      next unless key_match_obj
      aw_status_name=key_match_obj[1].humanize.titleize
      self.status_types[aw_status_name]=id_and_note[:id]
    end
    records=Crm::StatusType.where(id:self.status_types.values).to_a
    self.status_types.each do |key, id|
      next if id.nil?
      record=records.find{|r| r.id==id }
      if !record
        error_msg="Failed to preload status type id #{id}, which corresponds with AgencyWorks status '#{key}'. Make sure the database is properly populated."
        errors << error_msg
        audit level: 'error', body:error_msg
      end
      self.status_types[key]=record
    end
  end

  PREMIUM_MODE_NAMES={#maps their names to our enum ids
    'Annual'      =>1,
    'Semi-annual' =>2,
    'Quarterly'   =>3,
    'Monthly'     =>4,
  }
  STATUS_TYPE_CODES = [
    'F3', # In Force With Requirements
    'F4', # Await Funds/1035 Exchange
    'F5', # Tentative Offer
    'FA', # App. Submitted
    'FB', # Approved
    'FD', # Declined
    'FF', # Not Taken
    'FH', # Holding
    'FI', # Incomplete-Closed
    'FJ', # Approved Other than Applied
    'FL', # Reissued Case
    'FM', # Conversion
    'FP', # In Force
    'FS', # Issued - Del. Req.
    'FT', # Postponed
    'FW', # Withdrawn
    'FY', # Pending Decision
    'S7', # Awaiting Requirements
    'SA', # Inquiry Received
  ]

  #The cartesian product results in tuples like [aw_account_key,status_type_code].
  ALL_ACCOUNT_STATUS_COMBOS=AW_ACCOUNTS.keys.product( STATUS_TYPE_CODES ).freeze
end