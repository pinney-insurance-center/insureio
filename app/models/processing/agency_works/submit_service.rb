require "savon"

# Service object for sending a case to AgencyWorks
class Processing::AgencyWorks::SubmitService
  
  include AuditLogger::Helpers
  audit_logger_name 'aw_submissions'

  include ::Processing::AgencyWorks

  attr_accessor :crm_case, :request_body, :response_hash

  def initialize crm_case
    self.crm_case = crm_case
  end

  def send_to_agency_works
    @request_body=Processing::AgencyWorks::XmlBuilder.new.build_new_business_submission crm_case
    @response_hash = send_soap_request @request_body
    error_or_warning_desc=@response_hash.dig(:tx_life,:user_auth_response,:trans_result,"ResultInfoDesc")
    output=""

    if error?
      audit "Error encountered while submitting to AgencyWorks: #{error_or_warning_desc}", level:'error'
      audit "The request to AgencyWorks:\n#{@request_body.pretty_inspect}", level:'error'
      audit "The response from AgencyWorks:\n#{@response_hash.pretty_inspect}", level:'error'
      output="Error submitting to AgencyWorks. Please contact support for assistance."
    else
      if warning?(@response_hash)
        audit level:'warn', body:"Warning encountered while submitting to AgencyWorks: #{error_or_warning_desc}"
        audit level:'warn', body:"The response from AgencyWorks:\n#{@response_hash.pretty_inspect}"
      end
      #probably not worth presenting warnings to agent, since they're not even acknowledged/handled in CLU1
      #probably still worth logging
      agency_works_id=confirmation_id(@response_hash)
      crm_case.notes.create(creator_id:Thread.current[:current_user_id],text:"This case was imported into AW on #{Time.now}",note_type_id:3)
      crm_case.agency_works_id=agency_works_id
      crm_case.save
      output="Successfully submitted to AgencyWorks."
    end
    output
  rescue NoMethodError => ex
    consumer = crm_case.try(:consumer)
    SystemMailer.warning ex, message:"Unable to build AgencyWorks submission XML for case: #{crm_case.id}\nclient: ##{consumer.try(:id)} - #{consumer.try(:name)}"
    # Return response string
    'Error: unable to build AgencyWorks XML for case. Admin has been emailed.'
  end
end