require "builder"

class Processing::AgencyWorks::XmlBuilder

  #"Holding Search" fetches updates to details and status for a set of policies.
  def build_request_for_updates status_type, aw_account_key
    build_wrapping_xml "Holding Search", 302, aw_account_key do |xml|
      xml.MaxRecords("2500")
      xml.Criteria {
        xml.ObjectType("Policy", :tc => 18)
        xml.PropertyName("AsOfDate")
        xml.PropertyValue((Time.now).strftime("%Y-%m-%d"))
        xml.Operation(:tc => 6)
      }
      xml.Criteria {
        xml.ObjectType("Policy", :tc => 18)
        xml.PropertyName("PolicyStatus")
        xml.PropertyValue(status_type)
        xml.Operation(:tc => 1)
      }
    end
  end

  #"Holding Inquiry" fetches requirements and notes for a single policy.
  def build_holding_inquiry kase
    build_wrapping_xml "Holding Inquiry", 203, kase.aw_account_key do |xml|
      xml.Holding(id: "Holding_1") {
        xml.Policy {
          xml.ApplicationInfo {
            xml.TrackingID("AW_#{kase.agency_works_id}")
          }
        }
      }
    end
  end

  #Based on CLU1 code. Moved from deleted model `Processing::AgencyWorks::CaseData`.
  def build_new_business_submission crm_case#based on CLU1 code
    #to shorten long lines:
    c=crm_case.consumer

    sds=crm_case.stakeholder_relationships.preload(:stakeholder)
    #The reason for only using one of each here is historical.
    #That's just how the code worked prior and no users have complained.
    #At some point we should review Agency Integrator's documentation,
    #to see if they do in fact allow multiple owners,
    #and multiple primary/contingent beneficiaries.
    pb=sds.select{|sd| sd.is_beneficiary && !sd.contingent}.first
    cb=sds.select{|sd| sd.is_beneficiary && sd.contingent}.first
    owner=sds.select{|sd| sd.is_owner }.first

    build_wrapping_xml "New Business Submission", 103, crm_case.aw_account_key do |xml|
      xml.Holding(:DataRep => "Full", id: "Holding_#{crm_case.id}") {
        xml.HoldingTypeCode("Policy", tc: 2)
        xml.Policy(id: crm_case.id) {
          xml.PolNumber("")
          xml.LineOfBusiness("Life", tc: 1)
          xml.PolicyStatus("Pending", tc: 8)
          xml.StatusChangeDate(crm_case.status.created_at.strftime("%Y-%m-%d")) unless crm_case.status.nil?
          premium_mode_name=crm_case.a_team_details.premium_mode.try(:name)
          premium_mode_code = case premium_mode_name
            when "Annually",nil then 1
            when 'Semi-annl.'   then 2#this line inferred from the fact that Annually is 1 and Monthly is 4.
            when 'Quarterly'    then 3#this line inferred from the fact that Annually is 1 and Monthly is 4.
            when "Monthly"      then 4
            else nil
          end
          xml.PaymentMode(premium_mode_name, tc: premium_mode_code)
          xml.PaymentAmt(crm_case.a_team_details.modal_premium.to_s)
          xml.IssueAge()
          xml.Life {
            xml.FaceAmt(crm_case.a_team_details.face_amount)
          }
          xml.ApplicationInfo {
            xml.SignedDate()
            xml.SubmissionDate(Time.now.strftime("%Y-%m-%d"))
            state=(c.state || c.primary_address.try(:state))
            xml.ApplicationJurisdiction( state.try(:abbrev), tc: Processing::AgencyWorks::STATE_AW_CODE_MAP[state.try(:name)])
          }
          xml.Coverage {
            unless pb.nil?
              xml.LifeParticipant(            id:"LP_#{pb.id}") {
                xml.LifeParticipantRoleCode(  "Primary Beneficiary", tc: 7)
                xml.ParticipantName(          pb.stakeholder.try(:name))
                pri_relation, pri_relation_tc=aw_rel_code(pb.relationship_type.try(:name)||'other')
                xml.BeneficiaryRoleCode(      pri_relation, tc: pri_relation_tc)
              }
            end
            unless cb.nil?
              xml.LifeParticipant(            id:"LP_#{cb.id}") {
                xml.LifeParticipantRoleCode(  "Contingent Beneficiary", tc: 9)
                xml.ParticipantName(          cb.stakeholder.try(:name))
                con_relation, con_relation_tc=aw_rel_code(cb.relationship_type.try(:name)||'other')
                xml.BeneficiaryRoleCode(      cb, tc: con_relation_tc)
              }
            end
            xml.LifeParticipant(            id:"LP_#{c.id}") {
              xml.LifeParticipantRoleCode(  "Primary Insured", tc: 1)
              if crm_case.a_team_details.health_class && crm_case.a_team_details.health_class.name.include?("Table ")
                xml.TempTableRating(          crm_case.a_team_details.health_class.name.split(' ')[1])
              end
              xml.UnderwritingClass(        crm_case.a_team_details.carrier_health_class) if crm_case.a_team_details.carrier_health_class
            }
            unless owner.nil?
              xml.LifeParticipant(            id:"LP_#{crm_case.id}") {
                xml.LifeParticipantRoleCode(  "Owner", tc: 18)
                xml.ParticipantName(          owner.stakeholder.name)
              }
            end
          }
          xml.OLifEExtension {
            unless pb.nil?
              xml.PrimaryBeneficiaryName(   pb.stakeholder.try(:name))
              pri_relation, pri_relation_tc=aw_rel_code(pb.relationship_type.try(:name)||'other')
              xml.PrimaryBeneficiaryRole(   pri_relation)
            end
            unless cb.nil?
              xml.ContingentBeneficiaryName(cb.stakeholder.try(:name))
              con_relation, con_relation_tc=aw_rel_code(cb.relationship_type.try(:name)||'other')
              xml.ContingentBeneficiaryRole(con_relation)
            end
            unless owner.nil?
              xml.OwnerName(owner.stakeholder.name)
              xml.OwnerRole(owner.relationship_type.try(:name)||'other')
            end
            xml.CustomFields {
              xml.CustomField(crm_case.agent.full_name, :Name => "WS Agent")
            }
            xml.Codes {
              xml.Code(:Name => c.brand.full_name, :Family_Name => "Lead Source")
              xml.Code(:Name => crm_case.agent.full_name, :Family_Name => "WS Callers")
            }
          }
        } #End Policy
      } #End Holding
      unless owner.nil?
        xml.Party(id: "Owner_#{crm_case.id}") {
          xml.PartyTypeCode("Person", tc: 1)
          xml.FullName(owner.stakeholder.name)
          xml.Address(id: "Owner_#{crm_case.id}") {
            owner_a=owner.stakeholder.address
            xml.Line1(owner_a.try(:street).try(:split,"\n").try(:[], 0))
            xml.Line2(owner_a.try(:street).try(:split,"\n").try(:[], 1))
            xml.City( owner_a.try(:city) )
            unless owner_a.try(:state).nil?
              xml.AddressState(owner_a.state.abbrev)
              xml.AddressStateTC(owner_a.state.abbrev, tc: Processing::AgencyWorks::STATE_AW_CODE_MAP[owner.stakeholder.state.name]) 
            end
            xml.Zip(owner_a.zip)
          }
        }
        xml.Relation(               id: "Rel_Case_Owner_1",
                                    :OriginatingObjectID => "Holding_#{crm_case.id}",
                                    :RelatedObjectID => "Owner_#{crm_case.id}") {
          xml.OriginatingObjectType("Holding", tc: 4)
          xml.RelatedObjectType(    "Party", tc: 6)
          xml.RelationRoleCode(     "Owner", tc: 8)
        }
      end
      #According to project heads, "Organization" is a custom field
      #used only by the Pinney sales team and not critical for minimal operation.
      if crm_case.aw_account_key=='default'
        xml.Party(id: "Organization_#{c.lead_type}") {
          xml.PartyTypeCode("Org", tc: 2)
          xml.Organization {
            org_code = case c.tenant.name
              when 'ezlife' then 16
              when 'pjp'    then 4
              when 'taber'  then 7
              when 'ebi'    then 8
              when 'amg'    then 9
              else
                3 #Wholesale
            end
            xml.OrgCode(org_code)
          }
        }
        xml.Relation(                id: "Rel_Case_Org_1",
                                    :OriginatingObjectID => "Holding_#{crm_case.id}",
                                    :RelatedObjectID => "Organization_#{c.lead_type}") {
          xml.OriginatingObjectType("Holding", tc: 4)
          xml.RelatedObjectType(    "Organization", tc: 116)
          xml.RelationRoleCode(     "Agent of Agency", tc: 120)
        }
      end
      xml.Party(id: "Insured_#{c.id}") {
        xml.PartyTypeCode("Person", tc: 1)
        xml.GovtID(c.ssn)
        xml.Person {
          xml.FirstName(          c.first_name)
          xml.MiddleName(         c.middle_name)
          xml.LastName(           c.last_name)
          xml.Gender(             c.gender, tc: (c.gender_string == "Male" ? 1 : 2))
          xml.BirthDate(          c.birth.strftime("%Y-%m-%d")) unless c.birth.nil?
          xml.SmokerStat(         c.health_info.tobacco? ? "Nonsmoker" : "Tobacco User", tc: c.health_info.tobacco? ? 1 : 3) unless c.health_info.nil?
          xml.DriversLicenseNum(  c.dln)
          xml.DriversLicenseState(c.dl_state.abbrev, tc: Processing::AgencyWorks::STATE_AW_CODE_MAP[c.dl_state.name]) unless c.dl_state.nil?
        }

        a=c.primary_address
        xml.Address(          id: "Add_#{c.id}") {
          xml.AddressTypeCode("Residence", tc: 1)
          al1,al2,al3=        c.address.value.split("\n")
          xml.Line1(          al1)
          xml.Line2(          al2)
          xml.Line3(          al3)
          xml.City(           a.city)
          xml.AddressStateTC( a.state.abbrev, tc: Processing::AgencyWorks::STATE_AW_CODE_MAP[a.state.name])
          xml.Zip(            a.zip)
        } unless              a.blank?

        wp=c.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work') }.first
        xml.Phone(          id: "phn_#{c.id.to_s + wp.value.to_s}") {
          xml.PhoneTypecode("Business", tc: 2)
          xml.AreaCode(     wp.value[0..2])
          xml.DialNumber(   wp.value[3..9])
        } unless            wp.blank?

        hp=c.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home') }.first
        xml.Phone(          id: "phn_#{c.id.to_s + hp.value.to_s}") {
          xml.PhoneTypecode("Home", tc: 1)
          xml.AreaCode(     hp.value[0..2])
          xml.DialNumber(   hp.value[3..9])
        } unless            hp.blank?

        mp=c.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('mobile') }.first
        xml.Phone(          id: "phn_#{c.id.to_s + mp.value.to_s}") {
          xml.PhoneTypecode("Cell", tc: 12)
          xml.AreaCode(     mp.value[0..2])
          xml.DialNumber(   mp.value[3..9])
        } unless            mp.blank?

        xml.EMailAddress {
          xml.EmailType(    "Personal", tc: 2)
          xml.AddrLine(     c.primary_email.value)
        } unless            c.primary_email.blank?
      }
      xml.Relation(               id: "Rel_Case_insured_1",
                                  :OriginatingObjectID => "Holding_#{crm_case.id}",
                                  :RelatedObjectID => "Insured_#{c.id}") {
        xml.OriginatingObjectType("Holding", tc: 4)
        xml.RelatedObjectType(    "Person",  tc: 115)
        xml.RelationRoleCode(     "Insured", tc: 32)
      }
      xml.Party(          id: "Agent_#{crm_case.agent_id}") {
        xml.PartyTypeCode("Person", tc: 1)
        xml.Person {
          xml.FirstName(  crm_case.agent.first_name)
          xml.LastName(   crm_case.agent.last_name)
        }
      }
      xml.Relation(               id: "Rel_Case_Agent_1",
                                  :OriginatingObjectID => "Holding_#{crm_case.id}",
                                  :RelatedObjectID => "Agent_#{crm_case.agent_id}") {
        xml.OriginatingObjectType("Holding",      tc: 4)
        xml.RelatedObjectType(    "Person",       tc: 115)
        xml.RelationRoleCode(     "PrimaryAgent", tc: 37)
        xml.VolumeSharePct(       "100.00")
      }
    end
  end

  #1203 is supposedly the update code. This is based on a prior dev's note.
  #We should consult their documentation before attempting to implement.
  #The method skeleton is here for reference only.
  # def def build_policy_update_submission crm_case
  #   build_wrapping_xml "New Business Submission", 1203, kase.aw_account_key, true do |xml|
  #     #...
  #   end
  # end

protected

  def build_wrapping_xml trans_type_name, trans_type_code, aw_account_key
    account=Processing::AgencyWorks::AW_ACCOUNTS[aw_account_key]
    xml = Builder::XmlMarkup.new(:indent => 2,:escape_attrs => true)
    xml.instruct!
    xml.TXLife {
      xml.UserAuthRequest {
        xml.UserLoginName(account["username"])
        xml.UserPswd {
          xml.CryptType
          xml.Pswd(account["password"])
        }
        xml.VendorApp {
          xml.VendorName("", VendorCode: 1145)
          xml.AppName("AW")
          xml.AppVer("1.1.00")
        }
      }
      xml.TXLifeRequest {
        # xml.TransRefGUID()
        xml.TransType(trans_type_name, :tc => trans_type_code)
        xml.TransExeDate(Time.now.strftime("%Y-%m-%d"))
        # Using DateTime instead of Time because of the %z parameter
        xml.TransExeTime(ActiveSupport::TimeZone.new('UTC').now.strftime("%H:%M:%S%z"))
        if trans_type_code==103#if submitting vs getting notes/reqs/updates
          xml.PendingResponseOK("TRUE", tc: 1)
        else
          xml.InquiryLevel("Objects", :tc => 1)
        end
        xml.OLifE {
          yield xml
        }
      }
    }
    xml.target!
  end

  def aw_rel_code rel_string
    Processing::AgencyWorks::RELATIONSHIP_TYPE_AW_CODE_MAP.each do |k,v|
      return {k.to_s => v} if rel_string.to_s.gsub(/\W/,'_')==k.to_s
    end
    return {"other" => 2147483647}
  end

end
