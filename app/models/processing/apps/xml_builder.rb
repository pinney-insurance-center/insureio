require 'spreadsheet'
# This module is built with the assumption (given in the original requirements)
# that the primary insured is also the owner and the payer. AIG supports individual
#parties for each of these three roles. This module could be modified to  make use of that support.
class Processing::Apps::XmlBuilder

  TABLES_SPREADSHEET = "#{Rails.root}/lib/APPS/AIG  DATA MAP ACORD 2.21_AIG 8.0.0 Updated 10-29-12.xls"
  CARRIER_CODE_SPREADSHEET= File.join Rails.root, 'lib/APPS/ag_carrier_codes.xls'
  LIFE_PRODUCT_CAT        = Enum::ProductTypeCat.id('Life Insurance')
  ANNUITY_PRODUCT_CAT     = Enum::ProductTypeCat.id('Annuities')
  DISABILITY_PRODUCT_CAT  = Enum::ProductTypeCat.id('Disability')
  PERSON_GENRE = Enum::EntityType.id('person')
  TRUST_GENRE  = Enum::EntityType.id('trust')
  CONFIG       = APP_CONFIG['apps']
  APPLICATION_HOLDING = 'Holding_insured'
  JAN_AIG_PRODUCER_ID = '7YV29'
  PINNEY_AIG_PRODUCER_ID= 'X0442'
  JAN_PINNEY = Struct.new(:first_name, :middle_name, :last_name, :birth, :gender, :ssn, :id).new 'Russel', 'Jan', 'Pinney'
  INSURED_PARTY         = 'Party_insured'
  APPS_PARTY            = 'Party_APPS'
  EMPLOYER_PARTY        = 'Party_employer'
  PRIMARY_AGENT_PARTY   = "Party_primary_agent"
  ADDITIONAL_AGENT_PARTY = "Party_additional_agent"
  REQUIREMENT_ACCT_NUM  = 3127 # value for Holding.Policy.RequirementInfo.RequirementAcctNum; signifies: AMERICAN GENERAL - PINNEY INSURANCE - ExamRight
  AG_CARRIER_CODE       = 60488
  AG_CARRIER_ID         = Carrier.where('name LIKE "American General%"').first.try(:id)

  def initialize(kase)
    @kase                  = kase
    @details               = @kase.current_details
    @consumer              = @kase.consumer
    @dt                    = Time.now
    @datatables            = Spreadsheet.open TABLES_SPREADSHEET
    @carrier_code_table    = Spreadsheet.open(CARRIER_CODE_SPREADSHEET).worksheet('AG Co-Carrier for Existing Ins')
  end

  #Full process, called AppRight on the front end.
  #Only available for AG, which we fill as the carrier by default.
  def submit_new_business_xml xml_opts={}
    # prep
    @relations_procs = []
    transaction_type_code = ['New Business Submission', tc:103]
    # build
    buffer  = ""
    builder = Builder::XmlMarkup.new xml_opts.merge(target:buffer)
    builder.TXLife(
      "xmlns"=>"http://ACORD.org/Standards/Life/2",
      "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
      "xsi:schemaLocation"=>"http://ACORD.org/Standards/Life/2 file:///J:\SHARE\ACORD\XMLife216\TXLife2.16.00.xsd"
    ) { |txl|
      txl.TXLifeRequest( PrimaryObjectID:APPLICATION_HOLDING ) { |req|
        req.TransRefGUID  guid
        req.TransType     *transaction_type_code
        req.TransExeDate  format_date @dt
        req.TransExeTime  format_time @dt
        req.TransMode     'Original', tc:2
        req.TestIndicator *bool_val(!Rails.env.production?)
        req.OLifE { |ol|
          ol.comment! 'Source info'
          ol.SourceInfo { |source|
            source.CreationDate   format_date @kase.created_at
            source.CreationTime   format_time @kase.created_at
            source.SourceInfoName "PINNEY"
          }
          # GroupingInfo (USED FOR HOUSEHOLD INCOME)
          ol.comment! 'Grouping used for Estimated Household income'
          ol.Grouping( id:label('Grouping', @consumer) ) { |g|
            g.GroupingTypeCode  'Household', tc:1
            g.Household         { |hh| hh.EstIncome @consumer.household_income.to_i }
            add_relation label('Grouping',@consumer), INSURED_PARTY, ['Household', tc:18]
          }
          ol.comment! 'SOUGHT COVERAGE @kase'
          # HOLDING (Primary Holding, represents Application for Policy being applied for)
          ol.Holding( id:APPLICATION_HOLDING ) { |holding|
            holding.HoldingSysKey holding_sys_key
            holding.HoldingTypeCode  'Policy', tc:2
            holding.HoldingStatus    'Proposed', tc:3
            add_organization_party(@details.carrier)
            holding.Policy( id:INSURED_PARTY, CarrierPartyID:label('Party', @details.carrier) ) { |policy|
              policy.LineOfBusiness  'Individual' # individual
              policy.ProductType     *product_type
              policy.ProductCode     'AGLSELTERM'
              policy.CarrierCode     AG_CARRIER_CODE
              policy.PolNumber       pol_number
              policy.PlanName        plan_name_for_app_right
              policy.LtliaInd        *bool_val(true)
              policy.PaymentMode     *payment_mode(@kase)
              policy.PaymentAmt      payment_amt @kase
              policy.PaymentMethod "Pre-authorized Check", tc:26
              policy.FinancialActivity(id:"FinancialActivity_EFT") { |fin|
                fin.FinActivityType "Premium Payment", tc:1
                fin.PaymentMode     *payment_mode(@kase)
                fin.Payment(id:"Payment_1") { |payment|
                  payment.PaymentMethod "Pre-authorized Check", tc:26
                  payment.MonthlyPaymentAmt(currency @details.monthly_premium) if @details.try(:monthly_premium)
                  payment.AnnualPaymentAmt(currency @details.annual_premium) if @details.try(:annual_premium)
                  payment.QuarterlyPaymentAmt(currency @details.quarterly_premium) if @details.try(:quarterly_premium)
                  payment.SemiAnnualPaymentAmt(currency @details.semiannual_premium) if @details.try(:semiannual_premium)
                }
              }
              policy.Life { |life|
                life.FaceAmt      face_amount @kase
                life.Coverage( id:label('Coverage', @kase) ) { |cov|
                  cov.IndicatorCode   'Base', tc:1
                  cov.DurationDesign  duration @kase
                  cov.CovOption       { |opt|
                    opt.ClassName                 class_name
                    opt.PermTableRatingAlphaCode  table_rating_alpha_code if table_rating_alpha_code
                  }
                }
                life.Coverage { |coverage|
                  coverage.LifeCovTypeCode        'Waiver of Planned Premium', tc:21
                  coverage.IndicatorCode             'Rider', tc:2
                } if waiver_of_premium?
              }
              policy.ApplicationInfo { |inf|
                inf.TrackingID                pol_number
                inf.ApplicationJurisdiction   *state
                inf.ReplacementInd            *bool_val(@kase.replaced_by)
              }
              policy.RequirementInfo( FulfillerPartyID:APPS_PARTY ) { |rin|
                rin.ReqCode            'Diagnose', tc:535
                rin.ReqStatus          'Received', tc:7
                rin.RequirementAcctNum REQUIREMENT_ACCT_NUM
              }
              policy.Guarantee { |guarantee|
                guarantee.PayToYear       duration
                guarantee.AnnualPremAmt   @details.annualized_premium
                guarantee.CumulativePremiumAmt 0
                guarantee.CashValue       0
                guarantee.DeathBenefitAmt @details.face_amount
              } if @consumer.state.name == 'Florida'
            }
            holding.Attachment { |attachment|
              attachment.AttachmentBasicType  'Text Only', tc:1
              attachment.Description          'Details and Explanations'
              attachment.AttachmentData       @consumer.priority_note
              attachment.AttachmentType       'General Note', tc:14
            } if @consumer.priority_note.present?
          }
          ol.comment! '/ SOUGHT COVERAGE'
          # HOLDING (One Holding to represent each Existing Insurance Coverage) && HOLDING (One Holding to represent each Existing DI Insurance Coverage)
          @kase.existing_coverages.each{ |kase|
            ol.comment! 'EXISTING COVERAGE'
            holding_for_existing_coverage(ol, kase, 103)
            ol.comment! '/ EXISTING COVERAGE'
          }
          ol.comment! 'PRIMARY INSURED / OWNER / PAYER'
          # Party for Primary Insured
          ol.Party( id:INSURED_PARTY ) { |par|
            par.PartyTypeCode   'Person', tc:1
            par.GovtID          client.ssn.to_s
            par.EstNetWorth     client.financial_info.try(:net_worth) || 0
            par.Person { |psn|
              psn.FirstName           client.first_name
              psn.MiddleName          client.middle_name.try(:[], 0) if client.middle_name.present?
              psn.LastName            client.last_name
              psn.Suffix              client.suffix if client.suffix.present?
              # psn.Occupation          client.occupation || 'unknown' This field is commented b/c AppRight has a set of legal strings, which don't map to our anything-goes field
              psn.BirthDate           format_date client.birth
              psn.Gender              *gender
              psn.BirthJurisdictionTC *state(client.birth_state) if client.birth_state
              psn.EstSalary           client.financial_info.try(:asset_earned_income).to_i
              psn.NoDriversLicenseInd *bool_val(client.dln.blank?)
              psn.DriversLicenseNum   client.dln if client.dln.present?
              psn.DriversLicenseState *state(client.dl_state) if client.dl_state
              psn.BirthCountry        *birth_country
              psn.OLifEExtension { |ext|
                ext.PersonExtension { |pext|
                  pext.ImmigrationInd *bool_val(client.citizenship.try(:citizen?))
                }
              }
            } # /Person
            # Build addresses
            @consumer.addresses.each do |address|
              build_address(par, address, address == @consumer.primary_address)
            end
            # Build phones
            @consumer.phones.each do |phone|
              build_phone(par, phone, phone == @consumer.primary_phone)
            end
            # Build emails
            @consumer.emails.each do |email|
              build_email(par, email, email == @consumer.primary_email)
            end
            # Risk
            par.Risk { |ris|
              ris.TobaccoInd                  *bool_val(@consumer.tobacco?)
              ris.ExistingInsuranceInd        *bool_val(@consumer.cases.any? &:in_force?)
              ris.BankruptcyInd               *bool_val(@consumer.financial_info.try(:bankruptcy).present?)
              ris.SubstanceUsage { |sub|
                sub.TobaccoType       'Cigarettes', tc:1
                sub.SubstanceAmt      "#{client.health_info.cigarettes_per_day * 30} per month" if client.health_info.cigarettes_per_day
                sub.SubstanceEndDate  format_date(client.health_info.last_cigarette)
              } if client.health_info.last_cigarette
              ris.SubstanceUsage { |sub|
                sub.TobaccoType       'Cigars', tc:2
                sub.SubstanceAmt      "#{client.health_info.cigars_per_month} per month" if client.health_info.cigars_per_month
                sub.SubstanceEndDate  format_date(client.health_info.last_cigar)
              } if client.health_info.last_cigar
              ris.SubstanceUsage { |sub|
                sub.TobaccoType       'Pipe', tc:4
                sub.SubstanceEndDate  format_date(client.health_info.last_pipe)
              } if client.health_info.last_pipe
              ris.SubstanceUsage { |sub|
                sub.TobaccoType       'Chewing Tobacco', tc:8
                sub.SubstanceEndDate  format_date(client.health_info.last_tobacco_chewed)
              } if client.health_info.last_tobacco_chewed
              ris.SubstanceUsage { |sub|
                sub.TobaccoType       'Other Nicotine', tc:4096
                sub.SubstanceEndDate  format_date(client.health_info.last_nicotine_patch_or_gum)
              } if client.health_info.last_nicotine_patch_or_gum
              ris.LifeStyleActivity { |lsa|
                lsa.LifeStyleActivityType 'Foreign Travel', tc:29
                lsa.ActivityFrequency 1
                lsa.ForeignTravel { |travel|
                  travel.OLifEExtension('VendorCode'=>124){ |ole|
                    ole.ForeignTravelExtension { |fte|
                      fte.TravelPurpose
                    }
                  }
                }
              } if true # HARDCODED. was: @kase.consumer.health_info.foreign_travel
              ris.CriminalConviction { |con|
                con.CrimeDescription
              } if client.health_info.try(:criminal)
              @consumer.cases.active.effective.each do |kase|
                ris.ExistingLifeInsuranceCoverage {|cov|
                  cov.CompanyName   kase.approved_details.carrier.try(:name)
                  cov.Placed        *bool_val(kase.effective_date)
                  cov.AppliedAmt    face_amount(kase)
                  add_relation APPLICATION_HOLDING, label('Holding', kase), ['Replaced by', tc:64] do |rel|
                    rel.RelationDescription 'External Replacement', tc:1000700002
                  end
                } if kase.approved_details.present? && kase.approved_details.product_type_id == Enum::ProductType.id('Term')
              end
              ris.OLifEExtension( ExtensionCode:'RiskExtension' ) { |oex|
                oex.RiskExtension { |rex|
                  rex.HazardousActivityInd            *bool_val(client.health_info.try(:hazardous_avocation))
                  rex.ViolationsInd                   *moving_violations
                  rex.CriminalConvictionInd           *bool_val(client.health_info.try(:criminal))
                  rex.Bankruptcy( id:"Bankruptcy_1") { |br| # Can occur up to 4 timesgin
                    br.FilingPartialDate        format_date(client.financial_info.bankruptcy_declared) if client.financial_info.bankruptcy_declared
                    br.FilingStatusPartialDate  format_date(client.financial_info.bankruptcy_discharged) if client.financial_info.bankruptcy_discharged
                    br.BankruptcyDischargeInd   *bool_val(client.financial_info.bankruptcy_discharged)
                  } if client.financial_info.try(:bankruptcy)
                }
              }
            } # /Risk
            if @consumer.company.present? # /Employment
              par.Employment( id:"Employment_1", EmployerPartyID:EMPLOYER_PARTY ) { |emp|
                emp.EmployerName        @consumer.company
                emp.EmploymentDuties    @consumer.occupation if @consumer.occupation.present?
                # Add party & relation for employer
                add_party EMPLOYER_PARTY do |party|
                  party.comment!      'EMPLOYER'
                  party.PartyTypeCode   'Organization', tc:2
                  party.FullName        @consumer.company
                end
                add_relation INSURED_PARTY, EMPLOYER_PARTY, ['Employer', tc:7]
              }
            end
            par.KeyedValue { |kv|
              kv.KeyName 'AgentAdverse'
              kv.KeyValue 'False'
            }
            # Add relations to Holding_1
            add_relation APPLICATION_HOLDING, INSURED_PARTY, ['Primary Insured', tc:32]
            add_relation APPLICATION_HOLDING, INSURED_PARTY, ['Owner', tc:8]
            add_relation APPLICATION_HOLDING, INSURED_PARTY, ['Payer', tc:31]
          } # /Party 1
          ol.comment! '/ PRIMARY INSURED / OWNER / PAYER'
          add_beneficiaries
          # Agents
          add_agents
          # Children
          @consumer.children.each do |child|
            add_person_party child, label('Party', child)
            add_relation INSURED_PARTY, label('Party', @consumer.spouse), ['Child', tc:2] do |rel|
              desc = child.gender == FEMALE ? ['Daughter', tc:6] : ['Son', tc:5]
              rel.RelationDescription *desc
            end
          end
          if @consumer.spouse
            add_person_party @consumer.spouse, label('Party', @consumer.spouse)
            add_relation INSURED_PARTY, label('Party', @consumer.spouse), ['Spouse', tc:1] do |rel|
              desc = @consumer.spouse.gender == FEMALE ? ['Wife', tc:2] : ['Husband', tc:1]
              rel.RelationDescription *desc
            end
          end
          # All parties from +@parties_procs+
          @parties.each do |party_id, party_proc|
            ol.Party(id:party_id) { |party|
              party_proc.call(party)
            }
          end
          ol.comment! 'APPS PARTY (FULFILLER)'
          ol.Party( id:APPS_PARTY ) { |par|
            par.PartyTypeCode 'Organization', tc:2
            par.Organization { |org|
              org.AbbrName 'APPS'
              org.OrgCode '01100'
            }
          }
          ol.comment! '/ APPS PARTY (FULFILLER)'
          # All relations from +@relations_procs+
          ol.comment! 'RELATIONS'
          @relations_procs.each do |rp|
            rp.call(ol)
          end
          ol.comment! '/ RELATIONS'
        } # /OLifE
      } # /TXLifeRequest
    } # /TxLife (/root)
    # return
    buffer
  end

  #The exam/requirements ordering process, more limited.
  #Just called APPS on the front end.
  #Available for multiple carriers, so we need to fill in the appropriate carrier codes.
  def order_requirements_xml xml_opts={}
    #prep
    @relations_procs = []
    transaction_type_code = ['General Requirement Order Request', tc:121]
    # build
    buffer  = ""
    builder = Builder::XmlMarkup.new xml_opts.merge(target:buffer)
    builder.TXLife(
      "xmlns"=>"http://ACORD.org/Standards/Life/2",
      "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
      "xsi:schemaLocation"=>"http://ACORD.org/Standards/Life/2 file:///J:\SHARE\ACORD\XMLife216\TXLife2.16.00.xsd"
    ) { |txl|
      txl.TXLifeRequest( PrimaryObjectID:APPLICATION_HOLDING ) { |req|
        req.TransRefGUID  guid
        req.TransType     *transaction_type_code
        req.TransExeDate  format_date @dt
        req.TransExeTime  format_time @dt
        req.TransMode     'Original', tc:2
        req.TestIndicator *bool_val(!Rails.env.production?)
        req.OLifE { |ol|
          #requires Party > Person, Address, Phone, BirthDate for insured
          #requires Party > Person, Address, Phone, Email, Producer for agent
          #requires Holding > Policy
          #optional Party > Person, Address, Phone, Email for agency
          #
          ol.Holding( id:APPLICATION_HOLDING ) { |holding|
            holding.HoldingSysKey holding_sys_key
            holding.HoldingTypeCode  'Policy', tc:2
            holding.HoldingStatus    'Proposed', tc:3
            add_organization_party(@details.carrier)
            holding.Policy( id:INSURED_PARTY, CarrierPartyID:label('Party', @details.carrier) ) { |policy|
              policy.LineOfBusiness  'Individual' # individual
              policy.ProductType     *product_type
              policy.ProductCode     'AGLSELTERM'
              policy.CarrierCode     carrier_code @details.carrier, 121
              policy.PolNumber       pol_number
              policy.PlanName        @details.product_name
              policy.LtliaInd        *bool_val(true)
              policy.PaymentMode     *payment_mode(@kase)
              policy.PaymentAmt      payment_amt @kase
              policy.PaymentMethod "Pre-authorized Check", tc:26
              policy.FinancialActivity(id:"FinancialActivity_EFT") { |fin|
                fin.FinActivityType "Premium Payment", tc:1
                fin.PaymentMode     *payment_mode(@kase)
                fin.Payment(id:"Payment_1") { |payment|
                  payment.PaymentMethod "Pre-authorized Check", tc:26
                  payment.MonthlyPaymentAmt(currency @details.monthly_premium) if @details.try(:monthly_premium)
                  payment.AnnualPaymentAmt(currency @details.annual_premium) if @details.try(:annual_premium)
                  payment.QuarterlyPaymentAmt(currency @details.quarterly_premium) if @details.try(:quarterly_premium)
                  payment.SemiAnnualPaymentAmt(currency @details.semiannual_premium) if @details.try(:semiannual_premium)
                }
              }
              policy.Life { |life|
                life.FaceAmt      face_amount @kase
                life.Coverage( id:label('Coverage', @kase) ) { |cov|
                  cov.IndicatorCode   'Base', tc:1
                  cov.DurationDesign  duration @kase
                  cov.CovOption       { |opt|
                    opt.ClassName                 class_name
                    opt.PermTableRatingAlphaCode  table_rating_alpha_code if table_rating_alpha_code
                  }
                }
                life.Coverage { |coverage|
                  coverage.LifeCovTypeCode        'Waiver of Planned Premium', tc:21
                  coverage.IndicatorCode             'Rider', tc:2
                } if waiver_of_premium?
              }
              policy.ApplicationInfo { |inf|
                inf.TrackingID                pol_number
                inf.ApplicationJurisdiction   *state
                inf.ReplacementInd            *bool_val(@kase.replaced_by)
              }
              policy.RequirementInfo( FulfillerPartyID:APPS_PARTY ) { |rin|
                rin.ReqCode            'Diagnose', tc:535
                rin.ReqStatus          'Received', tc:7
                rin.RequirementAcctNum REQUIREMENT_ACCT_NUM
              }
              policy.Guarantee { |guarantee|
                guarantee.PayToYear       duration
                guarantee.AnnualPremAmt   @details.annualized_premium
                guarantee.CumulativePremiumAmt 0
                guarantee.CashValue       0
                guarantee.DeathBenefitAmt @details.face_amount
              } if @consumer.state.name == 'Florida'
            }
          } # /Holding
          ol.comment! 'PRIMARY INSURED / OWNER / PAYER'
          # Party for Primary Insured
          ol.Party( id:INSURED_PARTY ) { |par|
            par.PartyTypeCode   'Person', tc:1
            par.GovtID          @consumer.ssn.to_s
            par.Person { |psn|
              psn.FirstName           @consumer.first_name
              psn.LastName            @consumer.last_name
              psn.BirthDate           format_date @consumer.birth_or_trust_date
              psn.Gender              *gender
            } # /Person
            # Build addresses
            @consumer.addresses.each do |address|
              build_address(par, address, address.id == @consumer.primary_address_id)
            end
            # Build phones
            @consumer.phones.each do |phone|
              build_phone(par, phone, phone.id == @consumer.primary_phone_id)
            end
            # Build emails
            @consumer.emails.each do |email|
              build_email(par, email, email.id == @consumer.primary_email_id)
            end
            # Add relations to Holding_1
            add_relation APPLICATION_HOLDING, INSURED_PARTY, ['Primary Insured', tc:32]
          } # /Party
          add_agents
          # All parties from +@parties_procs+
          @parties.each do |party_id, party_proc|
            ol.Party(id:party_id) { |party|
              party_proc.call(party)
            }
          end
          # All relations from +@relations_procs+
          ol.comment! 'RELATIONS'
          @relations_procs.each do |rp|
            rp.call(ol)
          end
          ol.comment! '/ RELATIONS'
        } # /OLifE
      } # /TXLifeRequest
    } # /TxLife (/root)
    # return
    buffer
  end

  #If exam/requirements were ordered via APPS,
  #then status and results can be gathered from APPS.
  #This method will be called from a controller action that APPS must post to.
  def interpret_reqs_and_status_updates_xml
    #
  end

private

  def add_agents
    agent_of_record         = @kase.agent_of_record
    agent_of_record_is_self = agent_of_record.id == @kase.agent.id
    producer_id             = agent_of_record.contracts.where(carrier_id:AG_CARRIER_ID).first.try(:carrier_contract_id)
    if producer_id.nil?
      agent_of_record         = JAN_PINNEY
      producer_id             = JAN_AIG_PRODUCER_ID
      agent_of_record_is_self = false
    end
    # Add primary agent party
    add_person_party(agent_of_record, PRIMARY_AGENT_PARTY) do |party|
      party.Producer {|pro|
        pro.CarrierAppointment {|car| # appointment
          car.CompanyProducerID producer_id
          car.StateLicense      *state
        }
      }
      agent_of_record.phones.each do |phone|
        build_phone(party, phone, nil)
      end if agent_of_record.is_a?(User)
    end
    # Add primary agent relationship
    add_relation APPLICATION_HOLDING, PRIMARY_AGENT_PARTY, ['Primary Writing Agent', tc:37] do |builder|
      builder.InterestPercent agent_of_record_is_self ? 100 : 50
    end
    # Add additional agent party (if indicated)
    unless agent_of_record_is_self
      add_person_party @kase.agent, ADDITIONAL_AGENT_PARTY do |party|
        @kase.agent.phones.each do |phone|
          build_phone(party, phone, nil)
        end
        @kase.agent.emails.each do |email|
          build_email(party, email, nil)
        end
      end
      # Add additional agent relationship
      add_relation APPLICATION_HOLDING, ADDITIONAL_AGENT_PARTY, ['Additional Writing Agent', tc:52] do |builder|
        builder.InterestPercent 50
      end
    end
  end

  def add_beneficiaries
    benes=@kase.stakeholder_relationships.select{|b| b.is_beneficiary && b.stakeholder.entity_type_id == PERSON_GENRE }
    # We do not transmit beneficiaries which are not people because our system
    # cannot map legal values for American general's EntityType field. Melissa
    # Harley instructed us to omit Organization-type beneficiaries (even if
    # the percentage interest among beneficiaries does not add up to 100
    # because of this) and let the a-team enter the data for these
    # beneficiaries manually in AppRight.
    benes.each do |bene|
      party_id = label('Party_Beneficiary' + (bene.contingent ? 'Contingent' : 'Primary') + (true ? 'Person' : 'Organization'), bene)

      add_person_party(bene.stakeholder, party_id)

      # Add relation to policy
      role = bene.contingent ? ['Contingent Beneficiary', tc:36] : ['Primary Beneficiary', tc:34]
      add_relation APPLICATION_HOLDING, party_id, role do |rel|
        rel.InterestPercent           bene.percentage
        rel.BeneficiaryDesignation    'Named', tc:1
      end
      #Add relation to insured
      bene_rel = bene.relationship_type.try(:name).to_s.gsub(/\W/,'').downcase
      rel_desc = @datatables.worksheet('RelationDescription Table').find{|row| row[1].gsub(/\W/,'').downcase == bene_rel }
      if rel_desc
        add_relation INSURED_PARTY, party_id, ['Unknown', tc:0] do |rel|
          rel.RelationDescription rel_desc[1], tc:rel_desc[0].to_i
        end
      end
    end
  end

  def add_party id, &block
    @parties ||= {}
    @parties[id] ||= block
  end

  # Add party-building proc for Person to @parties
  def add_person_party person, id, &block
    add_party id do |party|
      party.PartyTypeCode   'Person', tc:1
      party.GovtID           person.ssn.to_s if person.ssn.present?
      party.Person { |psn|
        psn.FirstName     person.first_name
        psn.MiddleName    person.middle_name if person.middle_name.present?
        psn.LastName      person.last_name if person.last_name.present?
        psn.BirthDate     format_date(person.birth) if person.birth
      }
      yield(party) if block
    end
  end

  # Add party-building proc for Organization to @parties
  def add_organization_party org, id=nil, &block
    id ||= label('Party', org)
    add_party id do |party|
      party.PartyTypeCode     'Organization', tc:2
      party.FullName          org.name
      if org.respond_to?(:tin) && org.tin.present?
        party.GovtID          org.tin.to_s
      elsif org.respond_to?(:ssn) && org.ssn.present?
        party.GovtID          org.ssn.to_s
      end
      party.Organization { |organization|
        organization.EstabDate   format_date(org.birth)
      } if org.respond_to?(:birth)
      yield(party) if block
    end
  end

  # Requires that the ids are prefixed with the name of their ObjectType, e.g. 'Party_n'
  def add_relation originating_id, related_id, relation_role_args, &block
    @relation_ct ||= 0
    @relations_procs ||= []
    @relations_procs << -> xml_builder {
      xml_builder.Relation( id:"Relation_#{@relation_ct += 1}", OriginatingObjectID:originating_id, RelatedObjectID:related_id ) { |rel|
        rel.OriginatingObjectType   *object_type( originating_id.sub(/_.*$/,'') )
        rel.RelatedObjectType       *object_type( related_id.sub(/_.*$/,'') )
        rel.RelationRoleCode        *relation_role_args
        yield(rel) if block
      }
    }
  end

  def birth_country
    bc_name= @consumer.birth_country.try(:name) || @consumer.birth_country_custom_name
    return ['Unknown', tc:0] unless bc_name.present?
    row = find_data_from_spreadsheet 'Nation Table', bc_name
    return ['Unknown', tc:0] unless row.present?
    if row[2].try :include?, 'DEPRECATED'
      match = row[2].match(/use .*?(\d+)\W*(.*)$/i)
      [match[2], tc:match[1]]
    else
      [row[1], tc:row[0].to_i]
    end
  end

  def bool_val truth
    truth ? ['True', tc:1] : ['False', tc:0]
  end

  def build_address builder, addr, primary=true
    type_name = case addr.address_type.try(:name)
    when 'home';    'Residence'
    when 'work';    'Business'
    when 'mailing'; 'Mailing'
    else;           'Residence'
    end
    addr_type = find_data_from_spreadsheet('AddressTypeCode Table', type_name)
    builder.Address { |node|
      node.AddressTypeCode  addr_type[1], tc:addr_type[0].to_i
      node.Line1            addr.street
      node.City             addr.city
      node.AddressStateTC   *state(addr.state)
      node.Zip              addr.zip
      node.PrefAddr         *bool_val(primary)
    }
  end

  def build_email builder, email, primary=true
    builder.EmailAddress { |node|
      node.AddrLine       email.value
      node.PrefEMailAddr  *bool_val(primary) unless primary.nil?
    }
  end

  def build_phone builder, phone, primary=true
    phone_type = case phone.phone_type.try(:name)
    when 'home'
      ['Home', tc:1]
    when 'work'
      ['Business', tc:2]
    when 'mobile'
      ['Mobile', tc:12]
    else
      ['Business', tc:2]
    end
    builder.Phone { |node|
      node.PhoneTypeCode  *phone_type
      node.AreaCode       phone.value[0..2]
      node.DialNumber     phone.value[3..-1]
      node.PrefPhone      *bool_val(primary) unless primary.nil?
    } if phone.value.present?
  end

  def carrier_code carrier, request_type
    if request_type==103
      table=@carrier_code_table.map{|c| {id: c[3].to_i, code: c[1].strip} }
    elsif request_type==121
      table=Carrier.where('naic_code IS NOT NULL').map{|c| {id: c[:id], code: c[:naic_code]} }
    end

    row = table.find{|r| r[:id] == carrier.id }
    row[:code]
  end

  # Name for rate class
  def class_name
    case @kase.current_details.health_class.value
    when 'PP'
      @kase.tobacco? ? 'CLASS 5' : 'CLASS 1'
    when 'P'
      @kase.tobacco? ? 'CLASS 5' : 'CLASS 2'
    when 'RP'
      @kase.tobacco? ? 'CLASS 6' : 'CLASS 3'
    when 'R'
      @kase.tobacco? ? 'CLASS 6' : 'CLASS 4'
    else # table ratings
      @kase.tobacco? ? 'CLASS 8' : 'CLASS 7'
    end
  end

  def client
    @kase.consumer
  end

  # Formats number as "%.2f". Input can be Float or Fixnum
  def currency number
    "%.2f" % number
  end

  def duration kase=nil
    kase ||= @kase
    kase.current_details.duration.years
  rescue=>ex
    error "no duration for kase #{kase.id}"
    raise ex
  end

  def error msg
    @kase.errors.add :base, msg
  end

  def face_amount(kase)
    kase.current_details.face_amount.to_i
  rescue=>ex
    error "no face amount for kase #{kase.id}"
    raise ex
  end

  def find_data_from_spreadsheet worksheet, val, col=1
    @datatables.worksheet(worksheet).find{|row| row[col] =~ Regexp.new(val, Regexp::IGNORECASE) } if val
  end

  def format_date date
    date = date.utc if date.respond_to?(:utc)
    (date||Date.today).strftime '%Y-%m-%d'
  end

  def format_time dt
    (dt||Time.now).utc.strftime '%H:%M:%S'
  end

  def gender
    client.gender == FEMALE ? ['F', tc:2] : ['M', tc:1]
  end

  def guid
    @dt.utc.strftime('%Y%m%d%H%M%S') + "_" + "DR" + "_" + holding_sys_key.to_s
  end

  def holding_for_existing_coverage builder, kase, request_type
    return unless kase.in_force?
    # Require carrier
    carrier = kase.current_details.try(:carrier)
    return if carrier.nil?
    # Compute holding id
    holding_label_prefix = "Holding_#{ 'Non' unless kase.replaced_by_id == @kase.id }Replace"
    holding_id = label(holding_label_prefix, kase)
    # Add carrier to parties
    add_party label('Party', carrier) do |party|
      party.comment!      'CARRIER'
      party.PartyTypeCode 'Organization', tc:2
      party.FullName      carrier.name
      party.Carrier       { |car|
        car.NAICCode      carrier.naic_code
      } if carrier.naic_code
    end
    # Add relation for holding (kase)
    if kase.replaced_by_id == @kase.id
      add_relation APPLICATION_HOLDING, holding_id, ['Replaced By', tc:64]
      add_relation holding_id, INSURED_PARTY, ['Insured, tc:32']
    else
      add_relation INSURED_PARTY, holding_id, ['Existing Insurance', tc:1012400001]
    end
    # Add Holding to builder
    builder.Holding( id:holding_id, DataRep:'ReadOnly' ) { |hold|
      hold.HoldingTypeCode 'Policy', tc:2
      hold.Policy( id:label('Policy', kase) ) { |pol|
        pol.PolNumber         kase.id
        pol.CarrierCode       carrier_code kase.current_details.carrier, request_type
        pol.LineOfBusiness    'Individual'
        pol.IssueDate         kase.effective_date.to_s(:db)
        if kase.current_details.try(:product_type)
          pol.ProductType       *product_type(kase.current_details.product_type_name)
        end
        if kase.product_cat.id == LIFE_PRODUCT_CAT
          pol.Life { |life|
            life.FaceAmt kase.current_details.face_amount
          }
        end
        if kase.product_cat.id == DISABILITY_PRODUCT_CAT
          pol.DisabilityHealth { |dh|
            dh.IncomeAmtCov kase.current_details.face_amount
          }
        end
      }
    }
  end

  def holding_sys_key
    @kase.id
  end

  def immigration_status
    row = find_data_from_spreadsheet('ImmigrationStatus Table', 'Other')
    [row[1], tc:row[0].to_i]
  end

  def object_type name
    row = find_data_from_spreadsheet 'ObjectType Table', name
    [name, tc:row[0].to_i]
  end

  def label prefix, obj, &block
    @labels ||= {}
    @labels[prefix] ||= []
    unless i = @labels[prefix].index(obj)
      i = @labels[prefix].length
      @labels[prefix] << obj
    end
    "#{prefix}_#{i+1}"
  end

  def marital_status
    data = find_data_from_spreadsheet 'MarStat Table', client.marital_status.try(:name)
    data ? [data[1], tc:data[0].to_i] : ['Unknown', tc:0]
  end

  def moving_violations
    truth = client.health_info.try(:moving_violation_total).to_i > 0
    bool_val(truth)
  end

  def payment_amt kase
    sprintf "%0.02f", @details.modal_premium
  rescue => ex
    error "no modal premium for kase #{kase.id}"
    raise ex
  end

  def payment_mode kase
    id = case @details.premium_mode.value
    when 'YEAR';  1
    when 'SEMI';  2
    when 'QTR';   3
    when 'MONTH'; 4
    else;         9 # single payment
    end
    row = @datatables.worksheet('PaymentMode Table').find{|r| r[0] == id }
    return [row[1], tc:row[0].to_i]
  rescue => ex
    error "no premium mode for kase #{kase.id}"
    raise ex
  end

  def plan_name_for_app_right
    raise "wrong plan for AppRight (#{@details.product_name})" unless @details.product_name =~ /AG.Select.A.Term/i
    "Select-A-Term #{@details.duration.years}"
  end

  def pol_number
    @kase.apps_id
  end

  def product_type product_type_name=nil
    case product_type_name || @details.product_type.name
    when 'Term'
      ['Term', tc:2]
    when 'Universal Life'
      ['Universal Life', tc:3]
    when 'Variable Life'
      ['Variable Universal Life', tc:4]
    when 'Accidental'
      ['Accident and Health', tc:25]
    else
      ['Term', tc:2]
    end
  rescue=>ex
    error "No product type name for #{product_type_name}"
    raise ex
  end

  def purpose
    sanitized_reason = @kase.reason.try(:gsub, /\W/, '')
    row = @datatables.worksheet('Purpose Table').find{|r| r[1].gsub(/\W/,'').downcase == sanitized_reason }
    row ? [row[1], tc:row[0].to_i] : ['Unknown', tc:0]
  end

  def state dr_state=nil
    dr_state ||= @consumer.state
    clean_state_name = dr_state.name.sub(/\s*\(.*?\)/,'') # Remove "(Non_Bus)" suffix
    row = find_data_from_spreadsheet 'State Table', clean_state_name
    [dr_state.abbrev.sub(/\W.*/,''), tc:row[0].to_i]
  end

  def table_rating_alpha_code
    value = @kase.current_details.health_class.value
    if value =~ /^Table/
      value.sub(/\d\//,'').sub(/ - Tobacco$/,'')
    end
  end

  def tobacco
    @kase.tobacco? ? ['Smoker', tc:2] : ['Non-Smoker', tc:1]
  end


  def underwriting_class
    text = @kase.current_details.health_class.name
    case @kase.current_details.health_class.value
    when 'PP', 'P' # Preferred Plus || Preferred
      [text, tc:2]
    when 'RP' # Standard Plus
      [text, tc:6]
    when 'R' # Standard
      [text, tc:1]
    else # Table ratings
      [text, tc:3]
    end
  end

  def underwriting_subclass
    ['Best', tc:1] if @kase.current_details.health_class_id == Enum::TliHealthClassOption.id('Best Class')
  end

  def waiver_of_premium?
    false # This feature is not in use but is supported by APPS
  end

end
