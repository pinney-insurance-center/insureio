module Processing::Apps::CaseMethods
  extend ActiveSupport::Concern

  included do
    #These values are likely account-specific, so if/when we allow organizations or users
    #to use their own APPS accounts thru Insureio, we will need to move these, and
    #implement logic for fetching the appropriate values as needed.
    APPS_ID_PREFIX       = Rails.env.production? ? 'YME' : 'YTP'
    APPS_ID_MIN          = 1_889_749
    APPS_ID_MAX          = 1_894_999

    after_update :apps_id_in_range? if :apps_id_offset_changed?
  end

  #For convenience and conservation of db storage,
  #we store a unique numeric offset for use with APPS/AppRight.
  #What we send to them is the concatenation of the environment prefix and the full number value.
  def apps_numeric_id
    return nil if self[:apps_id_offset].nil?
    @apps_numeric_id||= (APPS_ID_MIN + self[:apps_id_offset] - 1)
  end

  # Compose the id which will identify this case in AppRight's database.
  def apps_id
    raise errors[:apps_id].first unless apps_numeric_id.nil? || apps_id_in_range?
    "#{ APPS_ID_PREFIX }#{ apps_numeric_id }"
  end

  # Fetch cases which are in force
  def existing_coverages
    consumer.cases.
      where('effective_date IS NOT NULL AND sales_stage_id>=? AND id != ?',Enum::SalesStage.id('Placed'), self.id)
  end

  def apps_id_in_range?
    return nil if apps_numeric_id.nil?
    if apps_numeric_id > APPS_ID_MAX
      errors.add :id, "APPS_ID_MAX exceeded (#{i} vs #{APPS_ID_MAX})"
      ExceptionNotifier.notify_exception ArgumentError.new(errors[:id].first)
      return false
    end

    send_range_warnings_if_needed
    true
  end

  def send_range_warnings_if_needed
    halfway_thru_block        = (APPS_ID_MAX+APPS_ID_MIN)/2
    three_quarters_thru_block = (3*APPS_ID_MAX+APPS_ID_MIN)/4
    seven_eighths_thru_block  = (7*APPS_ID_MAX+APPS_ID_MIN)/8
    fifty_from_block_max      = APPS_ID_MAX - 50

    human_range_msgs={
      halfway_thru_block:        '1/2 of allotted block of AppRight import IDs used up.',
      three_quarters_thru_block: '3/4 of allotted block of AppRight import IDs used up.',
      seven_eighths_thru_block:  '7/8 of allotted block of AppRight import IDs used up.',
      fifty_from_block_max:      'Down to < 50 allotted AppRight import IDs!',
    }
    specifics_msg="#{apps_numeric_id} out of #{APPS_ID_MAX}."
    plan_ahead_msg='Time to start planning for a new block. May be time to request a new block.'
    time_to_req_msg='Time to request a new block from Fulks, Chris <Chris.Fulks@aglife.com>'

    if fifty_from_block_max <= apps_numeric_id
      msg=fifty_from_block_max_msg+specifics_msg+time_to_req_msg
    elsif seven_eighths_thru_block <= apps_numeric_id
      msg=seven_eighths_thru_block+specifics_msg+time_to_req_msg
    elsif three_quarters_thru_block == apps_numeric_id
      msg=three_quarters_thru_block+specifics_msg+plan_ahead_msg
    elsif halfway_thru_block == apps_numeric_id
      msg=halfway_thru_block+specifics_msg+plan_ahead_msg
    end
    if msg
      SystemMailer.generic APP_CONFIG['email']['developers'], msg
    end
  end

end
