class Processing::Apps::UploadService
  include AuditLogger::Helpers

  attr_accessor :request_body, :response_body, :response_hash, :success

  def initialize(kase)
    @kase=kase
    @xml_builder_obj=Processing::Apps::XmlBuilder.new(@kase)
  end

  def submit_new_business
    process_name='submit_new_business'
    next_apps_id_offset=get_next_id_offset
    @kase.update_attributes(apps_id_offset: next_apps_id_offset)
    @request_body=@xml_builder_obj.send("#{process_name}_xml")
    make_soap_request process_name
    if success
      @kase.update_attributes(sent_103_to_apps_at: Date.today)
    end
    return success
  end

  def order_requirements
    process_name='order_requirements'
    next_apps_id_offset=get_next_id_offset
    @kase.update_attributes(apps_id_offset: next_apps_id_offset)
    @request_body=@xml_builder_obj.send("#{process_name}_xml")
    make_soap_request process_name
    if success
      @kase.update_attributes(sent_121_to_apps_at: Date.today)
    end
    return success
  end

  def get_next_id_offset
    last_apps_id_offset=Crm::Case.where('apps_id_offset IS NOT NULL').maximum(:apps_id_offset)||0
    last_apps_id_offset+1
  end

  def make_soap_request process_name
    # See http://savonrb.com/version3/getting-started.html for Savon usage
    wsdl_remote_url_or_relative_file_path=APP_CONFIG['apps']['wsdl']
    wsdl_uri=wsdl_remote_url_or_relative_file_path.match(/http/) ? wsdl_remote_url_or_relative_file_path : File.join(Rails.root, wsdl_remote_url_or_relative_file_path)
    client = Savon.client(
      wsdl: wsdl_uri,
      convert_request_keys_to: :camelcase,
      ssl_ca_cert_file: APP_CONFIG['ca_cert_file'],
      ssl_version: :TLSv1
    )
    operation = client.operation :apps_acord
    audit log:"apps_#{process_name}log", body: "Request:\n\t"+operation.build(message:{AcordXML: request_body }).to_s
    response = operation.call(message:{AcordXML: request_body })
    @response_body=response.body
    audit log:"apps_#{process_name}.log", body:"Response:\n\t"+@response_body.to_s
    @response_hash=response.to_hash
    @success= response_hash.dig(:apps_acord_response, :apps_acord_result) == "1" rescue false
    return success
  end

end
