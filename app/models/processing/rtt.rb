#This module should hold logic shared across the `::SubmitService` and `::PollingService` classes.
module Processing::Rtt
  include AuditLogger::Helpers
  audit_logger_name 'rtt_process'

  def self.determine_scor_env policy
    (Rails.env=='production' && !policy.is_test) ? 'production' : 'test'
  end

  #The note referred to here is overwritten each time
  #the consumer progresses to a new subset of questions.
  #In future we may want to replace this with a serialized field on the policy model.
  #@returns Hash with keys :rtt_answers and :scor_answers.
  def self.extract_rtt_and_scor_data_from_note policy
    rtt_and_scor_data_note=policy.consumer.notes.
      select{|n| n.text.include?(RTT_NOTE_PREFIX) }.last
    rtt_and_scor_data=rtt_and_scor_data_note.text.split(RTT_NOTE_PREFIX)[1]
    rtt_and_scor_data=JSON.parse rtt_and_scor_data
    rtt_and_scor_data=HashWithIndifferentAccess.new rtt_and_scor_data
  end

  def self.serialize_saved_data_for_rtt_consumer_resume_link policy
    output_obj=policy.consumer.as_json(
      only:[
            :id,
            :full_name,
            :company,
            :birth_or_trust_date,
            :gender,
            :dl_state_id,
            :birth_country_id,
            :birth_state_id,
            :occupation,
            :marital_status_id,
            :citizenship_id
      ],
      methods: [:dln, :ssn, :warnings],
      include:[:addresses, :emails, :phones, :health_info]
    )
    output_obj[:gender]=policy.consumer.gender_string[0].downcase
    output_obj[:cases_attributes      ]=[policy.as_json]
    output_obj[:health_info_attributes]=output_obj.delete :health_info
    output_obj[:addresses_attributes  ]=output_obj.delete :addresses
    output_obj[:emails_attributes     ]=output_obj.delete :emails
    output_obj[:phones_attributes     ]=output_obj.delete :phones

    note_data=self.extract_rtt_and_scor_data_from_note policy
    output_obj[:rtt_answers        ]=note_data[:rtt_answers]
    output_obj[:scor_health_answers]=note_data[:scor_health_answers]
    output_obj
  end

  def self.deliver_resume_app_email policy
    audit level:'info', body:"Called `deliver_resume_app_email` for policy #{policy.id} / #{policy.scor_guid}."
    _compose_and_deliver policy, policy.consumer, 'consumer'
  end

  def self.deliver_stakeholder_emails policy
    audit level:'info', body:"Called `deliver_stakeholder_emails` for policy #{policy.id} / #{policy.scor_guid}."
    if policy.owner==policy.payer
      _compose_and_deliver policy, policy.owner, 'owner_payer'
    else
      if policy.owner
        _compose_and_deliver policy, policy.owner, 'owner'
      end
      if policy.payer
        _compose_and_deliver policy, policy.payer, 'payer'
      end
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
    return false
  end

  def self._compose_and_deliver policy, recipient, role_name
    query_params={
      agent_id:  policy.agent_id,
      key:       policy.agent.quoter_key,
      brand_id:  policy.consumer.brand_id,
      id:        policy.scor_guid,
      person_id: recipient.id
    }
    query_params[:resume_app]=true if role_name=='consumer'
    query_str=query_params.to_query
    link_url=''
    msg_template_path=''
    subject_line=''
    if role_name=='consumer'
      link_url=CONSUMER_RESUME_LINK_PATH
      msg_template_path=CONSUMER_RESUME_MSG_TEMPLATE_PATH
      subject_line="You Can Still Resume Your Application For Life Insurance"
    else
      link_url=STAKEHOLDER_SIGN_LINK_PATH
      msg_template_path=STAKEHOLDER_SIGN_MSG_TEMPLATE_PATH.sub('{{role_name}}',role_name)
      subject_line="Your input is required for a life insurance application for #{policy.consumer.full_name}"
    end
    link_url+='?'+query_str
    msg_view_url=APP_CONFIG['base_url']+msg_template_path+'?'+query_str
    audit level:'info', body:{message:'Composing message.',link_url:link_url,msg_view_url:msg_view_url}
    msg_body=ApplicationController.new.render_to_string(
      file: msg_template_path,
      layout:false,
      formats:[:html],
      locals:{
        :@policy      =>policy,
        :@stakeholder =>recipient,
        :@msg_view_url=>msg_view_url,
        :@link_url   =>link_url
      }
    )
    user_smtp_config=policy.agent.try(:branded_smtp_config,policy.consumer.brand)
    user_delivery_method=user_smtp_config.try(:to_delivery_method)
    user_from_addr=user_smtp_config.try(:from_addr)
    smtp_config_to_use=(user_delivery_method || ActionMailer::Base.smtp_settings)
    from_addr_to_use=(user_from_addr||APP_CONFIG['email']['consumer_facing_sender'])

    audit level:'info',body:{
      message:'Sending message.',
      link_url:link_url,msg_view_url:msg_view_url,
      smtp_config:smtp_config_to_use.as_json.except('password'),
      from_addr:from_addr_to_use
    }
    mail = Mail.new({
      from:    from_addr_to_use,
      to:      recipient.email,
      subject: subject_line,
      body:    msg_body
    })
    mail.content_type 'text/html; charset=UTF-8'
    mail.delivery_method :smtp, smtp_config_to_use
    message = mail.deliver
  rescue => ex
    audit level:'error', body:{ guid:policy.scor_guid, error_class:ex.class.name, message:ex.message, backtrace:ex.backtrace}
    policy.errors.add(:base,"Failed to deliver email. #{ex.class.name} - #{ex.message}")
    return false
  end

  #these are ERB templates.
  CONSUMER_RESUME_MSG_TEMPLATE_PATH="/rtt/msg_resume_app.html"
  STAKEHOLDER_SIGN_MSG_TEMPLATE_PATH="/rtt/msg_signature_required_{{role_name}}.html"

  CONSUMER_RESUME_LINK_PATH="#{APP_CONFIG['base_url']}/rtt/"
  STAKEHOLDER_SIGN_LINK_PATH="#{APP_CONFIG['base_url']}/rtt/stakeholder_complete_and_sign.html"

  SUBMIT_ERROR_MSG='Error submitting to underwriting service. Please contact your insurance agent for assistance.'

  SOAP_SEND_TIMEOUT    = 5#unit is seconds.
  SOAP_RECEIVE_TIMEOUT = 5

  RTT_PHONE_TYPE_CODE_FOR_IO_PHONE_TYPE_ID={
    #keys should be Insureio phone type ids.
  }

  UNMATCHED_SCOR_COUNTRIES=[
    ["Ivory Coast",                         "09_01_01_25"],
    ["Laos",                                "09_01_02_15"],
    ["Tibet",                               "09_01_02_28"],
    ["Bonaire",                             "09_01_03_08"],
    ["Curacao",                             "09_01_03_12"],
    ["Monserrat",                           "09_01_03_21"],
    ["St. John",                            "09_01_03_25"],
    ["St. Martin",                          "09_01_03_28"],
    ["St. Thomas",                          "09_01_03_30"],
    ["England",                             "09_01_05_14"],
    ["Madeira",                             "09_01_05_30"],
    ["Scotland",                            "09_01_05_41"],
    ["Wales",                               "09_01_05_51"],
    ["Yugoslavia",                          "09_01_05_52"],
    ["US - Protectorates, etc.",            "09_01_07_04"],
    ["Northern Marianas",                   "09_01_08_15"],

    ["Other Country (Africa)",              "09_01_01_56"],
    ["Other Country (Asia)",                "09_01_02_32"],
    ["Other Country (Caribbean)",           "09_01_03_36"],
    ["Other Country (Central America)",     "09_01_04_08"],
    ["Other Country (Europe)",              "09_01_05_53"],
    ["Other Country (Middle East)",         "09_01_06_16"],
    ["Other Country (North America)",       "09_01_07_05"],
    ["Other Country (Australia / Oceania)", "09_01_08_23"],
    ["Other Country (South America)",       "09_01_09_14"]
  ]

  #This list would normally also include Florida (id 10), but it is currently not allowed due to a temporary filing issue.
  RTT_ALLOWED_STATE_IDS=[11, 14, 15, 16, 18, 23, 24, 28, 37, 40, 42, 44, 48, 50, 51]

  RTT_NOTE_PREFIX="Autosaved RTT and SCOR health question answers:\n".freeze
end
