module Processing::Docusign
	CARRIERS = "American General, Americo, Banner, Genworth, ING, John Hancock, Lincoln Financial, Met Life, Mutual of Omaha, Protective, SBLI, Transamerica, United of Omaha, William Penn".split(',')

	def self.table_name_prefix
		"processing_docusign_"
	end

	# No longer used. This was for the previous Docusign implementation, when Dataraptor was supposed to interface with Docusign's API
	def self.eligible_to_send?(kase)
		#first, check if user has DocuSign credentials
		kase.errors.add :docusign, "Agent lacks docusign credentials" if kase.agent.docusign_cred.nil?
		# Require email on client
		consumer = kase.consumer
		kase.errors.add :docusign, "Consumer lacks email" unless consumer.email.present?
		# Require state and carrier on kase
		carrier, state = kase_carrier_and_state(kase)
		kase.errors.add :docusign, "Case specifies no carrier" unless carrier
		kase.errors.add :docusign, "Case specifies no state" unless state
		# Require template for state & carrier on Docusign server
		unless kase.try(:agent).try(:docusign_cred).try(:set?)
			kase.errors.add :docusign, "Agent lacks Docusign credentials"
			return false
		end
		api = Api::Client.new(kase)
		account = first_account( api )
		if account.nil?
			kase.errors.add :docusign, "No Account information was returned from Docusign."
		else
			kase.errors.add :docusign, "Account lacks template for carrier and state" unless account.template_for?(carrier, state)
		end
		# return
		kase.errors[:docusign].empty?
	end

	def self.send_to_docusign(kase)
		carrier, state = kase_carrier_and_state(kase)
		account = first_account( Api::Client.new(kase) )
		template = account.template_for(carrier, state) || raise("No template found for #{carrier.name} in #{state.abbrev}. Aborting.")
		builder = Api::Envelope::Builder.from_template(template)
		builder.recievers << {
			name: kase.consumer.full_name,
			email: kase.consumer.email,
			role_name: "Signer 1"
		}
		builder.email_subject = "Please sign on Docusign"
		builder.send!
	end

private
	def self.first_account(api)
		api.accounts.first
	end

	def self.kase_carrier_and_state(kase)
		carrier = kase.current_details.try(:carrier)
		state = kase.state
		return [carrier, state]
	end
end
