class Processing::Docusign::Api::Account
	attr_accessor :api, :name, :accountId, :baseUrl, :isDefault, :userName, :userId, :email, :siteDescription
	def initialize(opts)
		opts.each_pair do |k,v|
			self.send("#{k}=",v)
		end

		self.is_default = (opts["isDefault"] == "true" ? true : false)
	end

	alias_attribute :id, :accountId
	alias_attribute :base_url, :baseUrl
	alias_attribute :is_default, :isDefault
	alias_attribute :username, :userName
	alias_attribute :site_description, :siteDescription

	def templates(force_reload = false)
		api.templates(self, force_reload)
	end

	def template_for(carrier, state)
		# carrier_name is going to be the entire carrier's name
		# The assumption here is that the templates' carrier_names will
		# contain a part of carrier_name, so that's how we'll match against it.
		# State abbreviation should be exact.
		carrier_name = carrier.name.downcase
		state_abbrev = state.abbrev

		templates.each do |template|
			if( (template.state_abbrev == state_abbrev) &&
				  (carrier_name.include?(template.carrier_name.downcase)) )
				return template
			end
		end
		nil
	end

	def template_for?(carrier, state)
		!!template_for(carrier, state)
	end

	def marshal_dump
		[@api, @name, @accountId, @baseUrl, @isDefault, @userName, @userId, @email, @siteDescription]
	end

	def marshal_load array
		@api, @name, @accountId, @baseUrl, @isDefault, @userName, @userId, @email, @siteDescription = array
	end

	def ==(other)
		other.is_a?(self.class) && other.id == self.id
	end
end
