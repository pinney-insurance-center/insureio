require_relative Rails.root.join("app/models/processing/docusign/api/middlewares")
require_relative Rails.root.join("app/models/processing/docusign/api/account")
require_relative Rails.root.join("app/models/processing/docusign/api/template")

module Processing::Docusign::Api
	class Client
		CONFIG   = APP_CONFIG['docusign']
		API_BASE = "restapi/v2"
		API_URL  = "#{CONFIG[:server]}/#{API_BASE}"

		def initialize(kase)
			@creds ||= {
				email:			kase.agent.docusign_cred.email,#CONFIG[:email],
				password:		kase.agent.docusign_cred.password,#CONFIG[:password],
				integrator_key: CONFIG[:integrator_key]
			}

			@templates ||= {}
		rescue NoMethodError => ex
			SystemMailer.warning(ex)
		end

		def has_credentials?
			@creds && @creds[:email] && @creds[:password]
		end

		# Retrieves a hash of the login information for the user
		def login_information
			@login_information ||= conn.get("#{API_BASE}/login_information").body
		end

		# Lists all the accounts available to this user
		def accounts(force_reload = false)
			Rails.cache.fetch("docapi/#{@creds[:email]}/accounts/", force: force_reload, expires_in: 5.minutes) do
				login_information
				@login_information["loginAccounts"]||=[]
				@login_information["loginAccounts"].map do |account_params|
					Processing::Docusign::Api::Account.new(account_params.merge(api: self))
				end
			end
		end

		# Returns an array of all the templates that an account has access to
		def templates(account, force_reload = false)
			Rails.cache.fetch("docapi/#{@creds[:email]}/accounts/#{account.id}/", force: force_reload, expires_in: 5.minutes) do
				conn.
				get("#{account.base_url}/templates").
				body["envelopeTemplates"].
				map do |template_params|
					Processing::Docusign::Api::Template.new(
						template_params.merge(api: self, accountId: account.id)
					)
				end
			end
		end

		def create_envelope_from_template(template, opts)
			opts = opts.merge({
				templateId: template.id
			})

			account = template.account

			conn.post("#{account.base_url}/envelopes") do |req|
				req['Content-Type'] = 'application/json'
				req['Content-Disposition'] = 'form-data'
				req.body = opts.to_json
			end
		end

		def marshal_dump
			[@creds]
		end

		def marshal_load array
			@creds = array[0]
		end

	private
		def conn
			ssl_opts= API_URL.include?('demo') ? {verify: false} : {}

			@conn ||= Faraday.new(url: CONFIG[:server], ssl: ssl_opts) do |builder|
				builder.use Processing::Docusign::Api::Middlewares::AcceptJsonMiddlware
				builder.use Processing::Docusign::Api::Middlewares::DSAuthMiddleware, @creds
				builder.use Faraday::Response::Logger unless Rails.env.test?

				# If you're using something like Fiddler2 for debugging HTTP requests,
				# set proxy information here
				# if Rails.env.development?
				# if true
				# 	builder.proxy 'http://localhost:8910'
				# end

				# Note to future devs: adapter needs to be specified after all other
				# middleware, or that middleware won't be used.
				builder.adapter Faraday.default_adapter
			end
		end
	end
end
