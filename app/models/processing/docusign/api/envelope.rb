class Processing::Docusign::Api::Envelope
	attr_accessor :api, :envelopeId, :uri, :statusDateTime, :status, :template, :account
	alias_attribute :id, :envelopeId
	alias_attribute :status_date_time, :statusDateTime

	def initialize(opts)
		opts.each_pair do |k,v|
			self.send("#{k}=",v)
		end
	end

	class Builder
		include ActiveModel::Validations

		attr_accessor :template, :emailSubject, :emailBlurb, :templateRoles, :status
		alias_attribute :email_subject, :emailSubject
		alias_attribute :recievers, :templateRoles
		alias_attribute :email_blurb, :emailBlurb

		validates_presence_of :template, :email_subject, :status
		validate :has_good_recievers

		def self.from_template(template, opts = {})
			new({template: template}.merge(opts))
		end

		def initialize(opts)
			opts.each_pair do |k,v|
				self.send("#{k}=",v)
			end

			self.recievers ||= []
			self.status ||= "sent"
		end

		def send!
			raise("Builder not valid! Errors: #{errors.full_messages.join(', ')}") unless valid?
			api = template.api
			response = api.create_envelope_from_template(template, {
				emailSubject: emailSubject,
				emailBlurb: emailBlurb,
				templateRoles: cleanTemplateRoles,
				status: status
			})
			if(response.status != 201) # 201 created
				raise("Wasn't able to create the envelope: #{response.body}")
			else
				return Processing::Docusign::Api::Envelope.new(response.body)
			end
		end

	private
		# Maps recievers from snake_case into the camelCase
		# format which Docusign expects
		def cleanTemplateRoles
			recievers.map do |tr|
				{
					name: tr[:name],
					email: tr[:email],
					roleName: tr[:roleName] || tr[:role_name]
				}
			end
		end

		def has_good_recievers
			if recievers.blank? || recievers.empty?
				self.errors.add(:recievers, "needs at least one member (email, name, role_name)")
				return
			end
			recievers.each do |r|
				!r[:email].blank? || self.errors.add(:recievers, "member is missing :email (#{r[:email]}, #{r[:name]}, #{r[:role_name]})")
				!r[:name].blank? || self.errors.add(:recievers, "member is missing :name")
				!r[:role_name].blank? || self.errors.add(:recievers, "member is missing :role_name")
			end
		end

	end
end
