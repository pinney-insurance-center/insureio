class Processing::Docusign::Api::Template
	attr_accessor(
		:api,
		:accountId,
		:templateId,
		:name,
		:shared,
		:password,
		:description,
		:lastModified,
		:pageCount,
		:uri,
		:folderName,
		:folderUri)

	alias_attribute :id, :templateId
	alias_attribute :last_modified, :lastModified
	alias_attribute :page_count, :pageCount
	alias_attribute :folder_name, :folderName
	alias_attribute :folder_uri, :folderUri

	def initialize(opts)
		opts.each_pair do |k,v|
			self.send("#{k}=",v) if respond_to?("#{k}=")
		end
		self.shared = (opts["shared"] == "true" ? true : false)
	end

	def shared?
		shared
	end

	def state_abbrev
		name.split(" ").first
	end
	def state
		s = Enum::State.ewhere(abbrev: state_abbrev).first
		if(s.nil?)
			AuditLogger['auto_template_resolution'].
			error("A state wasn't found for the state abbreviation #{abbrev} (full template name: #{name})")
		end
		s
	end

	def account
		@account ||= api.accounts.find { |a| a.id == accountId }
	end

	def carrier_name
		name.split(" ")[1]
	end
	def carrier
		carriers = Carrier.where("name like ?", "%#{carrier_name}%")
		if(carriers.length > 1)
			AuditLogger['auto_template_resolution'].
			warn("Name #{carrier_name} resulted in a match for more than one carrier: #{carriers.map(&:name)} (full template name: #{name})")
		end
		if(carriers.length == 0)
			AuditLogger['auto_template_resolution'].
			error("Name #{carrier_name} resulted in no matches for a carrier. (full template name: #{name})")
			return nil
		end
		carriers.first
	end

	def marshal_dump
		[@api, @accountId, @templateId, @name, @shared, @password, @description, @lastModified, @pageCount, @uri, @folderName, @folderUri]
	end

	def marshal_load array
		@api, @accountId, @templateId, @name, @shared, @password, @description, @lastModified, @pageCount, @uri, @folderName, @folderUri = array
	end
end
