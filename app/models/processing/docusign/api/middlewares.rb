# Insert application/json accept header into requests
# and decode JSON responses
module Processing::Docusign::Api::Middlewares
	class AcceptJsonMiddlware
		def initialize(app)
			@app = app
		end

		def call(env)
			env[:request_headers]["Accept"] = "application/json"
			@app.call(env).on_complete do |e|
				on_complete(e)
			end
		end

	private
		def on_complete(env)
			Rails.logger.info "The response from DocuSign:\n#{env.pretty_inspect}\n"
			env[:body] = JSON.parse(env[:body])
		end
	end

	# Insert authenticaion headers into requests
	class DSAuthMiddleware
		def initialize(app, creds)
			@app = app
			@email = creds[:email]
			@password = creds[:password]
			@integrator_key = creds[:integrator_key]
		end

		def call(env)
			env[:request_headers]['X-DocuSign-Authentication'] = {
				"Username" => @email,
				"Password" => @password,
				"IntegratorKey" => @integrator_key
			}.to_xml(root:"DocuSignCredentials", skip_instruct:true, indent:0).
			gsub(">\n<",'><').#no linebreaks between tags
			gsub("&amp;",'&')#yes, DocuSign expects output in a format directly violating the XML spec.
							 #ampersand characters (and probably others) need to be sent unescaped.

			Rails.logger.info "The request to DocuSign: #{env.pretty_inspect}\n"
			@app.call(env)
		end
	end

	# Enforce that all GET requests are succesful; or throw an exception
	class GetReqSuccessEnforcerMiddleware
		class GetFailureException < Exception; end;

		def initialize(app)
			@app = app
		end
		def call(env)
			if env[:method] == :get
				@app.call(env).on_complete do |environment|
					on_complete(environment)
				end
			end
		end

	private
		def on_complete(env)
			if env[:status] != 200
				raise GetFailureException.new("API GET request to #{env[:url]} failed with status: #{env[:status]}, " +
				"body: #{env[:body]}")
			end
		end
	end
end
