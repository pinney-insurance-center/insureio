# Corresponds to MarketechEvent in CLU
class Processing::Marketech::Update < ActiveRecord::Base
  
  
  alias_attribute :caseID, :case_id

  after_create :set_esign_code_if_available
  after_create :set_marketech_status
  after_create :complete_marketech_task_if_available

  CLIENT = 'client'
  AGENT = 'agent'

  APP_UPLOADED = 1
  APP_READY = 2
  APP_COMPLETE_NO_ESIGN = 3         # :event_code=>6, :party => "agent"
  APP_COMPLETE_ESIGN_READY = 4      # :event_code=>1, :party => "client"
  CLIENT_LINK_CLICKED = 5           # :event_code=>2, :party => "client"
  CLIENT_AUTHENTICATED = 6          # :event_code=>3, :party => "client"
  AGENT_LINK_CLICKED = 7            # :event_code=>2, :party => "agent"
  AGENT_AUTHENTICATED = 8           # :event_code=>3, :party => "agent"
  ESIGN_COMPLETED = 9               # :event_code=>4, :party => "client"
  ESIGN_CANCELLED = 10              # :event_code=>5, :party => "client"
  APP_PRINTED = 11
  CLIENT_CALLED = 12
  
  def self.build params
    new params.select{|k,v| %w[caseID event party code].include?(k.to_s) }
  end

  def status_id
    case party
    when AGENT
      case event
      when 2; AGENT_LINK_CLICKED
      when 3; AGENT_AUTHENTICATED
      when 6; APP_COMPLETE_NO_ESIGN
      end
    when CLIENT
      case event
      when 1; APP_COMPLETE_ESIGN_READY
      when 2; CLIENT_LINK_CLICKED
      when 3; CLIENT_AUTHENTICATED
      when 4; ESIGN_COMPLETED
      when 5; ESIGN_CANCELLED
      end
    end  
  end

  # Set the Task to complete
  def complete_marketech_task_if_available
    if dataset
      case status_id
      when CLIENT_LINK_CLICKED
        marketech_tasks.where(label:'Consumer Link Clicked').update_all completed_at:Time.now
      when CLIENT_AUTHENTICATED
        marketech_tasks.where(label:'Consumer Authenticated').update_all completed_at:Time.now
      when APP_COMPLETE_ESIGN_READY
        marketech_tasks.where(label:'App Complete - eSign Ready').update_all completed_at:Time.now
      when ESIGN_COMPLETED
        dataset.update_attributes esign_complete:true
        marketech_tasks.where(label:'esignature Completed').update_all completed_at:Time.now
        email_tasks.update_all(suspended:true,active:false)
        create_save_app_task
      when ESIGN_CANCELLED
        marketech_tasks.where(label:'esignature Cancelled').update_all completed_at:Time.now
      end
    end
  rescue => ex
    ExceptionNotifier.notify_exception(ex)
  end

  # Fetch Marketech::Dataset that belongs to kase
  def dataset
    @dataset ||= Processing::Marketech::Dataset.where(case_id:normalized_case_id).first
  end

  def esign_cancelled?
    return event == 5 && party == CLIENT
  end

  def esign_completed?
    return event == 4 && party == CLIENT
  end

  # Returns an integer case_id that points to a Crm::Case
  def normalized_case_id
    self.case_id.sub(/^(#{Processing::Marketech::CASE_PREFIX})|(#{Processing::Marketech::LEGACY_CASE_PREFIX})/,'').to_i
  end

  # If this event represents an esignature request, find the Marketech::Dataset
  # for this case and set its :esign_code
  # Also create a task on the current status (whatever it be) for the user in
  # charge to change the Status (as soon as the user is ready)
  def set_esign_code_if_available
    return unless code.present? && dataset && dataset.esign_code != code
    dataset.update_attributes esign_code:code
    begin
      dataset.kase.notes.create(text:"Marketech esign code updated: #{code}")
      task_attrs={
        delay:0,
        task_type_id:Enum::TaskType::id('to-do'),
        label:'Change Status to "App Sent - eSign"',
        sequenceable:dataset.kase,
        origin: dataset.kase.status,
        person: dataset.kase.consumer,
      }
      Task.create(task_attrs)
    rescue =>ex
      ExceptionNotifier.notify_exception(ex)
      File.write File.join(Rails.root, 'log', "#{__method__.to_s}-last-rescue.txt"), JSON.dump([dataset&.id, code, as_json])
      true#do not cancel saving the update if task or note creation fails.
    end
  end

  def set_marketech_status
    dataset.update_attributes status_id:status_id
  end

  # Returns scope for Marketech tasks on case's current status
  def marketech_tasks
    dataset.kase.status.tasks.where(task_type_id:Enum::TaskType::id('Marketech'))
  end

  def email_tasks
    dataset.kase.status.tasks.where(task_type_id:Enum::TaskType::id('email consumer'))
  end

private

  def create_save_app_task
    task = Task.new({
      task_type_id: Enum::TaskType.id('to-do'),
      person: dataset.kase.consumer,
      due_at: Time.now,
      label: 'Save E-Signed App to T-drive',
      origin: dataset.kase.status,
      sequenceable: dataset.kase,
      role_id: Enum::UsageRole.id('application specialist'),
      active: true
    })
    task.auto_assign!
    task.save!
  end

end
