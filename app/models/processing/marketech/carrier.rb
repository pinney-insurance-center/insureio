class Processing::Marketech::Carrier < ActiveRecord::Base

  belongs_to :carrier, class_name: '::Carrier', foreign_key:'carrier_id'

  validates :carrier_id, presence:true

  scope :for_case, lambda{|kase|
    state_id = kase.consumer.try(:state_id) || kase.consumer.try(:primary_address).try(:state_id)
    face_amount = kase.a_team_details.try(:face_amount)
    carrier_id = kase.a_team_details.try(:carrier_id)
    where(carrier_id:carrier_id).for_state_id(state_id).for_face_amount(face_amount).order('esign DESC')
  }

  scope :for_face_amount, lambda{|face_amount|
    where([%q(
      face_amount is null
      OR (face_amount_operator = ">"  and ? >  face_amount )
      OR (face_amount_operator = ">=" and ? >= face_amount )
      OR (face_amount_operator = "<"  and ? <  face_amount )
      OR (face_amount_operator = "<=" and ? <= face_amount )
      ), face_amount, face_amount, face_amount, face_amount])
  }

  scope :for_state_id, lambda{|state_id| where(state_id.nil? ? 'FALSE' : ['state_id_mask & ?', (1<<state_id.to_i)]) }

  def face_amount_id= value
    self.face_amount = case value
    when 1;   50000
    when 3;   100000
    when 6;   250000
    when 11;  500000
    when 50;  25000
    when 51;  5000
    when 64;  105000
    else;     raise 'unknown face amount id'
    end
  end

  def naic= naic_code
    self.carrier_id = Carrier.find_by_naic_code(naic_code).id
  end

  def state_id_blacklist
    (1..Enum::State.count).to_a.select{|i| state_id_mask << i unless state_id_supported?(i) }
  end

  def state_id_supported? i
    i = int_or_id(i)
    state_id_mask & (1<<i) > 0
  end

  def state_id_whitelist
    (1..Enum::State.count).to_a.select{|i| state_id_mask << i if state_id_supported?(i) }
  end

  def add_state_to_blacklist i
    i = int_or_id(i)
    self.state_id_mask &= ~(1<<i)
  end

  def add_state_to_whitelist i
    i = int_or_id(i)
    self.state_id_mask |= (1<<i)
  end

  def whitelist_all_states
    (1..Enum::State.count).each{|i| add_state_to_whitelist(i)}
  end

  def blacklist_all_states
    (1..Enum::State.count).each{|i| add_state_to_blacklist(i)}
  end

  def self.export_csv
    column_headings=["Carrier Name","Carrier Id","Marketech Product Code","Esign?","Face Amount Limit (Optional)"]
    column_headings+=Enum::State.all.map(&:abbrev)

    CSV.generate do |csv|
      csv << column_headings
      all.each do |c|
        row=[]
        row << c.carrier.name
        row << c.carrier_id
        row << c.code
        row << (c.esign ? 'y' : 'n')
        row << c.face_amount_operator.to_s.rjust(2, ' ')+c.face_amount.to_s
        Enum::State.all.each do |s|
          row << (c.state_id_supported?(s.id) ? 'y' : 'n')
        end
        csv << row
      end
    end
  end

 #Either returns a list of failures, or nil if successful.
  def self.import_csv csv
    count     =0
    return_val={successes:[],failures:[]}

    CSV.parse(csv, headers:true) do |row|
      count+=1
      row=row.to_hash
      attrs={}
      attrs[:carrier_id]          =row["Carrier Id"]
      attrs[:code]                =row["Marketech Product Code"]
      attrs[:esign]               =row["Esign?"]=='y'
      attrs[:face_amount_operator]=row["Face Amount Limit (Optional)"].match(/(>|<|=)*/)
      attrs[:face_amount]         =row["Face Amount Limit (Optional)"].match(/\d*/)
      attrs[:state_id_mask]       =0
      states=row.except("Carrier Name","Carrier Id","Marketech Product Code","Esign?","Face Amount Limit (Optional)")
      mtech_carrier=new(attrs)
      states.each{|k,v| mtech_carrier.add_state_to_whitelist(Enum::State.find_by_abbrev(k).try(:id).to_i) if v=~/y/i }
      if mtech_carrier.valid?
        return_val[:successes] << mtech_carrier
      else
        return_val[:failures] << mtech_carrier
      end
    end
    if return_val[:failures].empty?
      old_rules=self.where('id <= ?',self.maximum(:id).to_i)
      return_val[:successes].each{|s| s.save}
      old_rules.destroy_all
    end
    return return_val
  end

private

  def int_or_id i
    i.is_a?(Fixnum) ? i : i.id
  end
end
