require 'builder'

class Processing::Marketech::Dataset < ActiveRecord::Base
  

  belongs_to :kase, class_name:'Crm::Case', foreign_key:'case_id'
  belongs_to :latest_update, class_name:'Processing::Marketech::Update', foreign_key:'update_id'

  APP_UPLOADED = 1
  APP_READY = 2
  APP_COMPLETE_NO_ESIGN = 3         # :event_code=>6, :party => "agent"
  APP_COMPLETE_ESIGN_READY = 4      # :event_code=>1, :party => "client"
  CLIENT_LINK_CLICKED = 5           # :event_code=>2, :party => "client"
  CLIENT_AUTHENTICATED = 6          # :event_code=>3, :party => "client"
  AGENT_LINK_CLICKED = 7            # :event_code=>2, :party => "agent"
  AGENT_AUTHENTICATED = 8           # :event_code=>3, :party => "agent"
  ESIGN_COMPLETED = 9               # :event_code=>4, :party => "client"
  ESIGN_CANCELLED = 10              # :event_code=>5, :party => "client"
  APP_PRINTED = 11
  CLIENT_CALLED = 12


  # Return true if an 'edit' request is possible
  def editable?
    uploaded? and not ([ ESIGN_COMPLETED ].include?(status_id) || esign_completed?)
  end
  alias_method :editable,:editable?

  def edit_url
    url_for 'start'
  end

  # Return true if case's current status is 'Marketech' and the status has a completed task of type 'Marketech'
  def esign_completed?
    esign_complete and kase.status.status_type_id == Crm::StatusType::PRESETS[:marketech_app_sent_esign][:id]
  end

  def esign_url
    return unless esign_code.present?
    url_for('auth') + "&code=#{self.esign_code}"
  end

  # Return true if a 'log' request is possible
  def loggable?
    uploaded? and [ ESIGN_COMPLETED, ESIGN_CANCELLED, APP_PRINTED, CLIENT_CALLED ].include?(status_id)
  end
  alias_method :loggable,:loggable?

  def log_url
    url_for('retrieve') + '&format=log'
  end

  # Returns kase.id with a prefix, used to identify legacy Marketech records (where Marketech's xmlID was not used)
  def prefixed_case_id
    self.class.prefixed_case_id(self.case_id)
  end

  def pdf_url
    url_for('retrieve') + '&format=pdf'
  end

  def self.prefixed_case_id case_id
    "#{Processing::Marketech::CASE_PREFIX}#{case_id}"
  end

  # Return true if an email is ready to be sent to agent and client
  def sendable?
    uploaded? \
    and not [ ESIGN_COMPLETED, APP_PRINTED, CLIENT_CALLED ].include?(status_id) \
    and kase.status.status_type_id == Crm::StatusType::PRESETS[:marketech_app_sent_esign][:id] \
    and not (esign_code.blank? or esign_completed?)
  end
  alias_method :sendable,:sendable?

  # Return false if a 'prepare' request is possible
  def uploaded?
    status_id and status_id > 0
  end

private

  def url_for action
    APP_CONFIG['marketech']['url_base'] + APP_CONFIG['marketech']['path_start'] + '/' + action + '.do?' + url_query_string
  end

  # Return either xmlID=_ or siteKey=_&caseID=_
  def url_query_string
    xml_id ? "xmlID=#{xml_id}" : "siteKey=#{APP_CONFIG['marketech']['legacy_site_key']}&caseID=#{prefixed_case_id}"
  end

end
