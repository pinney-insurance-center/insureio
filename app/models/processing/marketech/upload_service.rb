class Processing::Marketech::UploadService
  include AuditLogger::Helpers
  audit_logger_name 'marketech'

  # Takes an argument of Processing::Marketech::Dataset
  def initialize(mdata)
    @marketech_dataset = mdata
  end

  # Send to Marketech
  def upload(user)
    audit
    # send to Marketech
    uri = URI::parse APP_CONFIG['marketech']['url_base'] + APP_CONFIG['marketech']['path_start'] + '/prepare.do'
    xml = to_ffxml
    audit level:'debug', body:uri.to_s
    audit level:'debug', body:xml.to_s
    res = Net::HTTP.start uri.host, uri.port, nil, nil, nil, nil, use_ssl:true do |http|
      # http.use_ssl = true
      req = Net::HTTP::Post.new uri.path
      req.content_type = 'application/xml'
      req.body = xml
      http.request req
    end
    if res.body =~ /^OK/
      # update status_id, set xml_id]
      xml_id = res.body.match(/^OK (\S+)/)[1]
      @marketech_dataset.update_attributes status_id:Processing::Marketech::Dataset::APP_UPLOADED, xml_id:xml_id
      # create crm note
      Note.create note_type_id:Enum::NoteType.id('status change'), notable:kase, text:"App uploaded to Marketech by #{user.name}"
      audit 'OK'
    else
      audit body:'FAIL', level:'error'
      audit body:res.body, level:'error' rescue nil
      raise "Bad response from Marketech:\n#{res.body}"
    end
  end

private

  #Define shorthand accessor methods for the most frequently referenced records.
  def consumer;    @consumer ||= kase.consumer;     end
  def kase;        @kase   ||= @marketech_dataset.kase; end
  def id;          @marketech_dataset.id;               end
  def term_length; submitted(:duration, :name).try :[], 0..1; end#1st 2 digits of category name
  def contract_id; kase.agent.contracts.where(carrier_id:kase.a_team_details.carrier_id).first.try(:carrier_contract_id); end
  def gender;      consumer.gender == MALE ? 'male' : 'female'; end

  def format_state state
    state && state.abbrev[0..1]
  end

  def frequency_mqsa
    case submitted(:premium_mode, :value)
    when 'YEAR'; 3
    when 'SEMI'; 2
    when 'QTR'; 1
    when 'MONTH'; 0
    end
  end

  def description_code person
    #Marketech supports two other owner types that we do not: [1:partnership, 4:other]
    case person.entity_type.try(:name)
    when 'person';  0
    when 'trust';   3
    when 'company'; 2
    end
  end

  #Try a sequence of method calls starting from the `a_team_details` object.
  def submitted(*methods)
    obj = kase.a_team_details
    methods.each{|x| obj = obj.try(x) }
    obj
  end

  # Construct an xml document suitable for body in POST request to Marketech's prepare.do action
  def to_ffxml
    xml = ::Builder::XmlMarkup.new
    xml.ffxml do
      xml.siteKey APP_CONFIG['marketech']['site_key']
      xml.caseID  @marketech_dataset.prefixed_case_id
      xml.policy  kase.marketech_carrier.code
      xml.state   format_state(consumer.state)
      xml.common do
        # config options, drawn from <FFXML Options Pinney.xlsx>
        xml.sdf true, context:'config', name:'useClickWrap'
        xml.sdf 120,  context:'config', name:'daysToExpire'

        #The following code is comented out instead of removed (against our normal policy)
        #because agents want auto-signature behavior stopped immediately, but
        #they will want it back later once we (or Marketech) figure out how to size signatures correctly.

        #The `savedAgentSignature` is an identifying string of the form 'PY__PINNEY__JAN' which
        #should correspond to a #tiff image we've sent to Marketech, which they store in their database.
        # if kase.marketech_signature.present?
        #   xml.sdf kase.marketech_signature, context:'config', name:'savedAgentSignature'
        # end

        # options drawn from <xmlsdf.xlsx>
        #Each tuple holds Marketech's context and name, and Insureio's stored value.
        c_h_phone=consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home') }.first
        c_w_phone=consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work') }.first
        c_w_address=consumer.addresses.select{|a| a.address_type_id==Enum::AddressType.id('work') }.first
        a_w_address=kase.agent.addresses.select{|a| a.address_type_id==Enum::AddressType.id('work') }.first
        map=[
          #consumer info
          ['generic',        'name_first',           consumer.first_name],
          ['generic',        'name_last',            consumer.last_name],
          ['generic',        'name_middle',          consumer.middle_name],
          ['generic',        'gender',               gender],
          ['generic',        'ssn',                  consumer.ssn.gsub(/\D/,'')],
          ['generic_birth',  'date_mdy',             consumer.birth.to_s],
          ['generic',        'birth_state',          format_state(consumer.birth_state)],
          ['generic',        'birth_country',        consumer.birth_country],
          ['generic',        'marital_status',       consumer.marital_status.try(:name)],
          ['generic',        'driver_license_state', format_state(consumer.dl_state)],
          ['generic',        'driver_license_number',consumer.dln],
          ['generic_citizen','yesno_01',             consumer.citizenship.citizen? ? 0 : 1],
          ['generic',        'type_of_visa',         consumer.citizenship.try(:name)],
          ['generic_work',   'company_name',         consumer.company],
          ['generic',        'occupation',           consumer.occupation],
          ['generic',        'income',               consumer.financial_info.try(:income).to_i],
          ['generic',        'assets',               consumer.financial_info.try(:assets).to_i],
          ['generic',        'liabilities',          consumer.financial_info.try(:liabilities).to_i],
          ['generic',        'net_worth',            consumer.financial_info.try(:net_worth).to_i],
          ['generic',        'height_inches',        consumer.health_info.try(:inches)],
          ['generic',        'height_feet',          consumer.health_info.try(:feet)],
          ['generic',        'weight',               consumer.health_info.try(:weight)],
          ['generic',        'travel_yn',            consumer.health_info.foreign_travel],
          #['generic',        'travel_reason',        consumer.health_info.travel_reason],#not yet something we store
          #['generic',        'travel_frequency',     consumer.health_info.travel_frequency],#not yet something we store
          ['generic',        'travel_duration',      consumer.health_info.travel_duration],
          ['generic',        'travel_when',          consumer.health_info.travel_when],
          ['generic',        'travel_where',         Enum::Country.find(consumer.health_info.foreign_travel_country_id).try(:name)||consumer.health_info.foreign_travel_country_detail],
          #consumer contact info
          ['generic',        'best_place_to_call',   consumer.primary_phone.to_s],
          ['generic',        'best_time_to_call',    consumer.preferred_contact_time],
          ['generic',        'email',                consumer.email],
          ['generic_home',   'address_street',       consumer.primary_address.try(:street)],
          ['generic_home',   'address_city',         consumer.primary_address.try(:city)],
          ['generic_home',   'address_state',        format_state(consumer.primary_address.try(:state))],
          ['generic_home',   'address_zip',          consumer.primary_address.try(:zip)],
          ['generic_home',   'phone',                c_h_phone.try(:value)],
          ['generic_work',   'phone',                c_w_phone.try(:value)],
          ['generic_work',   'phone_ext',            c_w_phone.try(:ext)],
          ['generic_work',   'address_street',       c_w_address.try(:street)],
          ['generic_work',   'address_city',         c_w_address.try(:city)],
          ['generic_work',   'address_state',        format_state(c_w_address.try(:state))],
          ['generic_work',   'address_zip',          c_w_address.try(:zip)],
          #policy info
          ['generic',        'term_length',          term_length],
          ['generic',        'policy_class',         submitted(:carrier_health_class)],
          ['generic',        'death_benefit',        submitted(:face_amount).to_s],
          ['generic',        'insurance_policy',     submitted(:plan_name)],
          ['generic',        'insurance_company',    submitted(:carrier_name)],
          ['generic',        'annual_premium',       submitted(:annualized_premium)],
          ['generic',        'frequency_mqsa',       frequency_mqsa],
          ['generic',        'payment_amount',       submitted(:modal_premium).to_s],
          ['generic',        'product_type',         submitted(:product_type, :marketech_code)],
          ['generic',        'product_type_text',    submitted(:product_type, :name)],
          ['generic',        'binding_yn',           kase.bind ? 0 : 1],
          #agent info
          ['generic',        'how_long_known',       "#{consumer.years_of_agent_relationship.to_i} years"],
          ['agent1',         'agent_number',         contract_id],
          ['agent1',         'email',                kase.agent.primary_email.to_s],
          ['agent1',         'firm_name',            kase.agent.company],
          ['agent1',         'name_full',            kase.agent.name],
          ['agent1',         'address_street',       a_w_address.try(:street)],
          ['agent1',         'address_city',         a_w_address.try(:city)],
          ['agent1',         'address_state',        format_state(a_w_address.try(:state))],
          ['agent1',         'address_zip',          a_w_address.try(:zip)],
          #other parties
          ['owner',          'applicant_yn',         kase.insured_is_owner],
          ['payor',          'applicant_yn',         kase.insured_is_payer],
        ]

        if consumer.health_info.try(:tobacco?)
          map+=[
            ['generic',         'tobacco_yn',          0],
            #cigarettes
            ['tobacco1',        'tobacco_yn',          consumer.health_info.cigarettes_current ? 0 : 1],
            ['tobacco1',        'tobacco_amount',      "#{consumer.health_info.cigarettes_per_day.to_i}  per day"],
            ['tobacco1',        'tobacco_last_use',    consumer.health_info.last_cigarette.to_s],
            #cigars
            ['tobacco2',        'tobacco_yn',          consumer.health_info.cigars_current ? 0 : 1],
            ['tobacco2',        'tobacco_amount',      "#{consumer.health_info.cigars_per_month.to_i} per month"],
            ['tobacco2',        'tobacco_last_use',    consumer.health_info.last_cigar.to_s],
            #chew
            ['tobacco3',        'tobacco_yn',          consumer.health_info.tobacco_chewed_current ? 0 : 1],
            #['tobacco3',        'tobacco_amount',      consumer.health_info.tobacco_chewed_per_day.to_i+' per day'],#not yet something we store
            ['tobacco3',        'tobacco_last_use',    consumer.health_info.last_tobacco_chewed.to_s],
            #pipes
            ['tobacco4',        'tobacco_yn',          consumer.health_info.pipe_current ? 0 : 1],
            ['tobacco4',        'tobacco_amount',      "#{consumer.health_info.pipes_per_year.to_i} per year"],
            ['tobacco4',        'tobacco_last_use',    consumer.health_info.last_pipe.to_s],
            #other
            ['tobacco5',        'tobacco_yn',          consumer.health_info.nicotine_patch_or_gum_current ? 0 : 1],
            #['tobacco5',        'tobacco_amount',      consumer.health_info.nicotine_patch_or_gum_per_day.to_i+' per day'],#not yet something we store
            ['tobacco5',        'tobacco_last_use',    consumer.health_info.last_nicotine_patch_or_gum.to_s],
          ]
        else
          map+=[
            ['generic',         'tobacco_yn',          1]
          ]
        end

        srs=kase.stakeholder_relationships.preload(stakeholder:[:addresses])
        owner=srs.select{|sr| sr.is_owner }.first
        payer=srs.select{|sr| sr.is_payer }.first

        if owner
          map+=[
            ['owner','description',   description_code(owner.stakeholder)],
            ['owner','relationship',  owner.relationship_type.try(:name)],
            ['owner','name_full',     owner.stakeholder.name],
            ['owner','email',         owner.stakeholder.primary_email.to_s],
            ['owner','phone',         owner.stakeholder.primary_phone.to_s],
            ['owner','address_street',owner.stakeholder.primary_address.try(:street)],
            ['owner','address_city',  owner.stakeholder.primary_address.try(:city)],
            ['owner','address_state', format_state(owner.stakeholder.primary_address.try(:state))],
            ['owner','address_zip',   owner.stakeholder.primary_address.try(:zip)],
          ]
        end
        if payer
          map+=[
            ['payor','description',   description_code(payer.stakeholder)],
            ['payor','relationship',  payer.relationship_type.try(:name)],
            ['payor','name_full',     payer.stakeholder.name],
            ['payor','email',         payer.stakeholder.primary_email.to_s],
            ['payor','phone',         payer.stakeholder.primary_phone.to_s],
            ['payor','address_street',payer.stakeholder.primary_address.try(:street)],
            ['payor','address_city',  payer.stakeholder.primary_address.try(:city)],
            ['payor','address_state', format_state(payer.stakeholder.primary_address.try(:state))],
            ['payor','address_zip',   payer.stakeholder.primary_address.try(:zip)],
          ]
        end

        existing_policies=consumer.cases.where(is_a_preexisting_policy:true)
        map+=[['generic','existing_yn',existing_policies.length>1 ? 0 : 1]]
        existing_idx=0
        existing_policies.each do |existing|
          map+=[
            ['repl'+existing_idx.to_s,'amount',       existing.face_amount],
            ['repl'+existing_idx.to_s,'company',      existing.current_details.carrier.try(:name)],
            ['repl'+existing_idx.to_s,'policy_number',existing.policy_number],
            ['repl'+existing_idx.to_s,'replacing_yn', existing.replaced_by_id==kase.id],
            ['repl'+existing_idx.to_s,'type',         existing.product_type.try(:name)],
            ['repl'+existing_idx.to_s,'issue_year',   existing.effective_date],
          ]
          existing_idx+=1
        end

        benes=srs.select{|sr| sr.is_beneficiary }

        # Marketech allows 4 primary beneficiaries and 3 contingent beneficiaries.
        # Contingent beneficiaries have the same fields as primary beneficiaries EXCEPT for +information+
        primary_beneficiary_idx = 0
        contingent_beneficiary_idx = 0
        benes.each do |bene|
          break if (!bene.contingent && primary_beneficiary_idx>3) || (bene.contingent && contingent_beneficiary_idx>2)
          context = bene.contingent ? "cbene#{contingent_beneficiary_idx+=1}" : "bene#{primary_beneficiary_idx+=1}"
          map+=[
            [context+'_birth','date_mdy',        bene.stakeholder.birth.try(:strftime, '%m/%d/%Y')],
            [context,         'name_full',       bene.stakeholder.name],
            [context,         'relationship',    bene.relationship_type.try(:name)],
            [context,         'share',           bene.percentage],
            [context,         'ssn',             bene.stakeholder.ssn],
            [context,         'address_combined',bene.stakeholder.primary_address.try(:to_s)],
          ]
        end

        map.each do |the_context, the_name, the_value|
          xml.sdf(the_value, context:the_context, name:the_name)
        end
      end
    end
  end

end