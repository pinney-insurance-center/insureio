require "active_support/core_ext"

module Processing
  class ExamOne
    class CompanyNotSupportedError < StandardError; end
    class InvalidInformationError < StandardError; end
    class Or01SendError < StandardError; end
    include AuditLogger::Helpers
    audit_logger_name 'exam_one' # Used for AuditLogger::Helpers

    def self.table_name_prefix
      "processing_exam_one_"
    end

    def self.build_and_send_or01(crm_case)
      case_datum=crm_case.exam_one_case_datum || Processing::ExamOne::CaseData.create(case_id:crm_case.id)

      return unless crm_case.eligible_for_processing_with_exam_one
      (crm_case.errors[:exam_one] << "Control code is required") && return unless crm_case.exam_one_case_datum.try(:control_code)
      return unless Or01.new(crm_case).send
      (crm_case.errors[:exam_one] << "Could not update case datum") && return unless case_datum.update_attributes(info_sent: true)
      return true
    end

    def self.get_statuses
      audit
      Processing::ExamOne::PollService.new.run
    end    

    def self.schedule_url(crm_case)
      consumer = crm_case.consumer
      params = {
        control_code: crm_case.exam_one_case_datum.control_code.try(:paramed),
        control_num:  crm_case.id,
        fname:        consumer.first_name,
        lname:        consumer.last_name,
        addr:         consumer.primary_address.try(:value),
        city:         consumer.primary_address.try(:city),
        state:        consumer.primary_address.try(:state).try(:abbrev)[0..1],
        zip:          consumer.primary_address.try(:zip),
        gender:       consumer.gender == MALE ? 0 : 1,
        email:        consumer.primary_email.try(:value)
      }
      error_msg=''
      params.each{|k,v| error_msg+="#{k.to_s.humanize} is blank.\n" if v.blank? }

      crm_case.errors.add( :exam_one, "Cannot generate ExamOne Schedule URL.\n#{error_msg}") if params.any?{|k,v| v.blank? }
      APP_CONFIG['exam_one']["web_url"] + params.to_query
    end

  end
end
