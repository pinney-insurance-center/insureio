module Processing
  class Igo
    class UserDataXmlBuilder
      def build_xml(agent)
        xml = Builder::XmlMarkup.new(indent: 2, escape_attrs: true)
        xml.UserData {
          xml.Data(agent.first_name, Name: "FirstName")
          xml.Data(agent.last_name, Name: "LastName")
          xml.Data(agent.primary_phone.try(:value), Name: "Phone")
        }
        xml.target!
      end
    end
  end
end
