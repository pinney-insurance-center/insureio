module Processing
  class Igo
    class CarrierXmlBuilder
      def add_carrier_xml(crm_case)
        xml = Builder::XmlMarkup.new(indent: 2, escape_attrs: true)

        current_details=crm_case.current_details

        carrier_product=Processing::Igo::CarrierProduct.where(
          dr_carrier_id:      current_details.carrier_id
          ).first

        cp_mappings={
          carrier_id:     "CarrierID",
          product_id:     "ProductID",
          product_type_id:"ProductTypeID"
        }
        Processing.build_xml_for(xml, carrier_product, cp_mappings) if !carrier_product.nil?

        
        qd_mappings={
          face_amount:      "FaceAmount",
          premium_mode_name:"POL_PaymentMethod",
          modal_premium:    "POL_PremiumAmount"
        }
        Processing.build_xml_for(xml, current_details, qd_mappings) if !current_details.nil?

        xml.target!
      end

    end
  end
end
