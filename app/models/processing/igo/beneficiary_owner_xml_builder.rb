module Processing
  class Igo
    class BeneficiaryOwnerXmlBuilder
      def add_beneficiary_owner_xml(crm_case)
        xml = Builder::XmlMarkup.new(indent: 2, escape_attrs: true)
        pb_mappings={
          first_name:       "PB_FirstName",
          middle_name:      "PB_MiddleName",
          last_name:        "PB_LastName",
          trustee:          "PB_EntityName",
          relationship_name:"PB_RelationType",
          percentage:       "PB_Share",
          ssn:              "PB_SSN"
        }
        owner_mappings={
          first_name:       "OWN_Ind_FirstName",
          middle_name:      "OWN_Ind_MiddleName",
          last_name:        "OWN_Ind_LastName",
          ssn:              "OWN_Ind_SSN",
          relationship_name:"OWN_Ind_Relationship"
        }

        @PBCounter = 1
        sds=crm_case.stakeholder_relationships.preload(stakeholder:[:contact])
        pbs=sds.select{|sd| sd.is_beneficiary && !sd.contingent }
        owner=sds.select{|sd| sd.is_owner }.first

        pbs.each do |bene|
          #Processing.build_xml_for(xml, primary_beneficiary, pb_mappings)
          xml.Data(bene.stakeholder.first_name,  Name: "PB_FirstName__" +    @PBCounter.to_s) if bene.stakeholder.first_name.present?
          xml.Data(bene.stakeholder.middle_name, Name: "PB_MiddleName__" +   @PBCounter.to_s) if bene.stakeholder.middle_name.present?
          xml.Data(bene.stakeholder.last_name,   Name: "PB_LastName__" +     @PBCounter.to_s) if bene.stakeholder.last_name.present?
          xml.Data(bene.stakeholder.trustee_name,Name: "PB_EntityName__" +   @PBCounter.to_s) if bene.stakeholder.trustee_name.present?
          xml.Data(bene.relationship_name,       Name: "PB_RelationType__" + @PBCounter.to_s) if bene.relationship_name.present?
          xml.Data(bene.percentage,              Name: "PB_Share__" +        @PBCounter.to_s) if bene.percentage.present?
          xml.Data(bene.stakeholder.ssn,         Name: "PB_SSN__" +          @PBCounter.to_s) if bene.stakeholder.ssn.present?
          xml.Data(bene.stakeholder.birth.strftime("%m/%d/%Y"), Name: "PB_DOB__" + @PBCounter.to_s) if bene.stakeholder.birth.present?
          @PBCounter += 1
        end

        if !owner.nil?
          Processing.build_xml_for(xml, owner, owner_mappings)

          xml.Data(owner.nil? ? "Yes" : "No", Name: "PIOwnerInd")

          xml.Data(owner.stakeholder.primary_phone, Name: "OWN_ Ind_PHONE") if owner.stakeholder.primary_phone.present?
        end

        xml.target!
      end

    end
  end
end
