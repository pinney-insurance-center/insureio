module Processing
  class Igo
    class PrimaryInsuredXmlBuilder
      def add_primary_insured_xml(consumer)
        xml = Builder::XmlMarkup.new(indent: 2, escape_attrs: true)

        consumer_mappings={
          first_name:         "PIFirstName",
          last_name:          "PILastName",
          middle_name:        "PIMiddleName",
          company:            "PIEMP_Name",
          birth_country:      "PIBirthCountry",
          occupation:         "PIEmp_Occupation",
          birth_state_abbrev: "PIBirthState",
          dl_state_abbrev:    "PIDLicense",
          marital_status_name:"PIMStatus",
          citizenship_name:   "PIFNDCitizenCountry",
          inches:             "MED_Q1_HtIn",
          feet:               "MED_Q1_HtFt",
          weight:             "MED_Q1_Wt",
          #was part of `contact_mapping`:
         }

        Processing::build_xml_for xml, consumer, consumer_mappings

         if !consumer.state.nil?
          xml.Data(consumer.state.abbrev,     Name: "PIADDR_State")
          xml.Data(consumer.state.abbrev,     Name: "StateID")
        end

        birth_state_abbrev=consumer.try(:birth_state).try(:abbrev)

        xml.Data(consumer.try(:gender) ? "Male" : "Female", Name: "PIGender")

        xml.Data(consumer.birth_or_trust_date.strftime("%m/%d/%Y"),   Name: "PIDOB") if !consumer.birth_or_trust_date.nil?
        xml.Data("Yes", Name: "PICitizen")
         # xml.Data(consumer.try(:ssn), Name: "PISSN")
         # xml.Data(consumer.try(:dln), Name: "PIDLicenseNo")

        xml.Data("Individual", Name: "AgntType")
        xml.Data("Individual", Name: "PIPhysicianType")
        xml.Data("Yes", Name: "PIEmployed")
        xml.Data("Yes", Name: "NonMed_PhysInd")
        xml.Data("Yes", Name: "PIDriversLicense_YesNo")

        address=consumer.primary_address
        if address.present?
          xml.Data(consumer.address.street, Name: "PIADDR_Street")
          xml.Data(consumer.address.city, Name: "PIADDR_City")
          xml.Data(consumer.address.zip, Name: "PIADDR_Zip")
        end

        home_phone=consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home') }.first
        work_phone=consumer.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work') }.first

        xml.Data( home_phone.try(:value).to_s.gsub(/\D/,''), Name: "PIPhone_Home")     if home_phone
        xml.Data( work_phone.try(:value).to_s.gsub(/\D/,''), Name: "PIPhone_Work")     if work_phone
        xml.Data( work_phone.try(:ext).to_s,                 Name: "PIPhone_Work_EXT") if work_phone

        email_value=consumer.email.try(:value)
        xml.Data( email_value, Name: "PIEmail") if email_value.present?

        financial_info=consumer.financial_info
        if !financial_info.nil?
          xml.Data(financial_info.income,   Name: "PIAnnualEarnedIncome") if !financial_info.income.nil?
        end

        xml.Data(consumer.tobacco? ? "Yes" : "No", Name: "PITobaccoInfo")

        xml.target!
      end

    end
  end
end
