require "net/ftp"

class Processing::ExamOne::PollService
  include AuditLogger::Helpers
  audit_logger_name 'exam_one' # Used for AuditLogger::Helpers

  def run
    delete_done_files if Rails.env.production?
    decrypt_and_save_files
    delete_local_old_files
  end

private

  def decrypt_and_save_files
    ftp.nlst("*.PGP").each do |filename|
      audit "decrypt and save: #{filename}"
      # download, decrypt file
      ftp.getbinaryfile(filename, full_file_path(filename))
      path_of_decrypted_file = decrypt_file(ftp, filename)
      # parse, save data to db
      Processing::ExamOne::Or01Parser.parse_and_save_statuses(path_of_decrypted_file)
      # set file as 'done' on server
      ftp.rename(filename, "#{filename}.DONE") if Rails.env.production?
    end
  end

  # Decrypts a file from ExamOne. This requires that the decryption key be in your keyring and that the key's passphrase be in APP_CONFIG['exam_one]['passphrase']
  # Returns path of output file (decrypted).
  def decrypt_file(ftp, filename)
    input_file = full_file_path(filename)
    output_file = input_file + ".out"
    %x[echo "#{APP_CONFIG['exam_one']['passphrase']}" | gpg --passphrase-fd 0 --batch --decrypt --output "#{output_file}" "#{input_file}"]
    output_file
  end


  def delete_done_files
    files_to_delete = ftp.nlst("#{time_ago_to_delete}*.DONE")
    files_to_delete.each do | filename |
      ftp.delete(filename)
    end
  end

  def ftp
    @ftp ||= 
    begin
      conn = Net::FTP.new("xfer.labone.com")
      conn.login("pinney", "YbjV8dl1")
      conn.passive = true
      conn.chdir("outgoing")
      conn
    end
  end

  def full_file_path(filename)
    "#{Rails.root}/tmp/#{filename}"
  end

  def delete_local_old_files
    filepath = "#{Rails.root}/tmp/#{time_ago_to_delete}"
    if Rails.env.development?
      %x[del #{filepath}*.PGP]
      %x[del #{filepath}*.out]
    else
      %x[rm -f #{filepath}*.PGP]
      %x[rm -f #{filepath}*.out]
    end
  end

  def time_ago_to_delete
    (Time.now - 1.month).strftime("%Y%m%d")
  end

end