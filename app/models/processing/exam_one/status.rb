class Processing::ExamOne::Status < ActiveRecord::Base
  

  belongs_to :case, class_name: "Crm::Case"
  belongs_to :consumer, class_name: "Consumer", foreign_key: :consumer_id
end
