class Processing::ExamOne::CaseData < ActiveRecord::Base

  belongs_to :kase,         class_name: 'Crm::Case'
  belongs_to :control_code, class_name: 'Processing::ExamOne::ControlCode'

end
