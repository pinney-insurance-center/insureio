# This module should be included in Processing::ExamOne::Or01
module Processing
  class ExamOne
    module FieldFormatter

      def birth
        client.birth.strftime("%Y%m%d")
      end

      def contact_method
        case client.try(:preferred_contact_method).try(:name)
        when 'home phone'
          "HM"
        when 'work phone'
          "WK"
        when 'mobile phone'
          "MO"
        else
          "  "
        end
      end

      def full_name
        [ stringf(30, client.last_name),
          stringf(20, client.first_name),
          stringf(20, client.middle_name),
          stringf(3, client.suffix)
        ].join
      end

      def gender
        if client.gender.nil?
          " "
        elsif client.gender == MALE
          "M"
        else
          "F"
        end
      end

      def marital_status
        output = client.marital_status.try(:name).try(:[],0) || " "
        raise "Bad Marital Status for client #{client.id}, #{output}" unless "SMWD ".include?(output)
        output
      end

      def product_type_id
        name = (kase.product_type.try(:name) || kase.a_team_details.try(:product_type_name)).to_s
        value = name =~ /disability/i ? 0 : 1
        stringf(2, value)
      end

      # Tracy Meyer says our office always sets this to a value that indicates unknown so that the examiner will test for it.
      # Erik's original code returned either 'Y' or 'N'.
      def tobacco
        " "
      end

      def ssn
        stringf(9, client.ssn.gsub(/\D/,''), true)
      end

      def stringf(length, text=nil, strict=false)
        text = text.to_s
        text = text.gsub(/\n/,'')
        text = text.gsub(/[ ()-]/,'') if strict
        text = text.slice(0...length) if text.length > length
        text.ljust(length)
      end

    end
  end
end
