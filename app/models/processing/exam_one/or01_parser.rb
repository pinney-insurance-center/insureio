require "active_support/core_ext"

class Processing::ExamOne::Or01Parser
  include AuditLogger::Helpers
  audit_logger_name 'exam_one' # Used for AuditLogger::Helpers

  def initialize(text)
    @text = text
  end

  def policy_number
    @policy_number ||= begin
      line = @text.match(/\nPI01.*/).try :[], 0
      line and line[17..44].to_i
    end
  end

  # Parse statuses, save to db
  def save_statuses
    statuses.each do |status|
      if Processing::ExamOne::Status.exists?(status.attributes.reject{|k,v| v.blank?})
        audit "**SKIPPING** #{status.attributes.reject{|k,v| v.blank?}.inspect}"
      elsif status.save
        audit "**SAVED** status id:#{status.id}"
      else
        audit level: :error, body:["**ERROR** Could not save status for policy:#{status.case_id}",status.errors.full_messages]
      end
    end
  end

  def self.parse_and_save_statuses(filepath)
    texts = File::read(filepath).split(/(?:^|\n)OR01/)
    texts.each do |text|
      parser = self.new(text)
      # save status if policy number in OR01 matches a case in the db
      parser.save_statuses if Crm::Case.exists?(parser.policy_number)
    end
  end

private

  def statuses
    @statuses ||= @text.scan(/\nST.*/).map do |line|
      Processing::ExamOne::Status.new({
        case_id: policy_number,
        exam_one_status_id: line[24..33].to_i,
        description: line[34..108].strip,
        completed_at: ActiveSupport::TimeZone.new('UTC').parse(line[111..123])
      })
    end.uniq{|status| status.attributes }
  end      

end
