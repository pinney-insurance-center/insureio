# Replaces or01_parser, or01_sender, or01_validator

class Processing::ExamOne::Or01
  include Processing::ExamOne::FieldFormatter
  include AuditLogger::Helpers
  audit_logger_name 'exam_one'

  attr_reader :kase

  def initialize(kase)
    @kase = kase
  end

  # Send OR01 to ExamOne. Return false if not successful. If not successful, set errors on kase
  def send
    client = Savon.client wsdl:APP_CONFIG['exam_one']['wsdl']
    op = client.operation(:deliver_exam_one_content)
    message = {
      :username => APP_CONFIG['exam_one']["username"],
      :password => APP_CONFIG['exam_one']["password"],
      :destinationID => "C1",
      :payload => self.to_s 
    }
    @response = op.call message:message
    
    if result.present? && result.try(:[],:response_code).present? && result[:response_code] == '1'
      return true
    else # handle errors
      audit level:'error', body:["Bad response when sending OR01 to ExamOne", '--response--', result.try(:[],:response_code_text), '--payload--', self.to_s]
      kase.errors.add :base, result.try(:[],:response_code_text)
      return false
    end
  end

  # Return meaningful part of response body
  def result
    @response and Processing::parse_and_format(@response.body[:deliver_exam_one_content_response][:deliver_exam_one_content_result])[:return_values]
  end

  # Returns OR01 string suitable for sending to ExamOne
  def to_s
    [ order_record,
      order_notes,
      applicant_record_1,
      applicant_record_2,
      applicant_record_3,
      applicant_record_4,
      carrier_info,
      policy_info,
      exam_appointment,
      total
    ].join("\n")
  end

private

  def applicant_record_1
    [ "AN01",
      full_name,
      birth,
      ssn,
      gender,
      marital_status,
      tobacco,
      stringf(3, client.age),
      stringf(4),
      contact_method,
      stringf(23)
    ].join
  end

  def applicant_record_2
    [ "AN02",
      stringf(40, client.primary_address),
      stringf(17, client.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('home')}.first.try(:value), true),
      stringf(17, client.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work')}.first.try(:value), true),
      stringf(5,  client.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('work')}.first.try(:ext), true),
      stringf(25),
      stringf(17, client.phones.select{|p| p.phone_type_id==Enum::PhoneType.id('mobile')}.first.try(:value), true),
      stringf(7)
    ].join
  end

  def applicant_record_3
    [ "AN03",
      stringf(40, client.primary_address.try(:value).to_s[40..80]),
      stringf(40, client.primary_address.try(:value).to_s[80..120]),
      stringf(27, client.primary_address.try(:city)),
      stringf(2,  client.primary_address.state.try(:abbrev)),
      stringf(10, client.primary_address.try(:zip)),
      stringf(9)
    ].join
  end

  def applicant_record_4
    [ "AN04",
      stringf(2,  client.dl_state.try(:abbrev)),
      stringf(22, client.dln),
      stringf(4),
      stringf(30, client.occupation),
      stringf(70, client.primary_email.try(:value))
    ].join
  end

  def carrier_info
    [ "CO01PI01",
      stringf(10, control_code),
      stringf(50, kase.a_team_details.try(:carrier).try(:name)),
      stringf(64)
    ].join
  end

  def client
    @client ||= @kase.consumer
  end

  def control_code
    @control_code ||= begin
      kase.exam_one_case_datum.control_code.subsidiary
    end
  end

  def exam_appointment
    [ "SE01",
      stringf(14, kase.exam_date.try(:strftime, "%Y%m%d%H%M%S")),
      stringf(114)
    ].join
  end

  def order_notes
    if client.priority_note.present?
      [ "ON01",
        stringf(90, client.priority_note),
        stringf(38)
      ].join
    else
      ""
    end
  end

  def order_record
    [ "OR01V01OO",
      stringf(30, @kase.id),
      stringf(20, "PIN1"),
      stringf(9),
      APP_CONFIG['exam_one']["global_site_id"],
      stringf(54, "00000000")
    ].join
  end

  def policy_info
    sds=kase.stakeholder_relationships.preload(stakeholder:[:contact])
    pbs=sds.select{|sd| sd.is_beneficiary && !sd.contingent }
    [ "PI01",
      product_type_id,
      stringf(11, kase.a_team_details.try(:face_amount).to_s.rjust(11, "0")),
      stringf(28, Processing::AgencyWorks::CaseData.where(case_id:kase.id).first.try(:agency_works_id)),
      stringf(50, pbs.first.try(:name)),
      stringf(6,  pbs.first.try(:relationship)),
      stringf(3,  tobacco),
      stringf(8,  kase.created_at.strftime('%Y%m%d')),
      stringf(20)
    ].join
  end

  def total
    [ "TOTL",
      stringf(10, 1),
      stringf(10, 10),
      stringf(108)
    ].join
  end

end