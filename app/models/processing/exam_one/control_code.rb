class Processing::ExamOne::ControlCode < ActiveRecord::Base

  alias_attribute :control_code, :paramed

  # Scopes records whose :name shares the same first word as the Crm::Case's carrier name
  scope :like, lambda{ |kase|
    carrier_name = kase.current_details.try(:carrier_name)
    matcher = carrier_name && carrier_name.gsub(/\s.*/, '%')
    where 'name LIKE ?', matcher
  }

  def state_abbrev= abbrev
    self.state_id = Enum::State.find_by_abbrev(abbrev).id
  end

end
