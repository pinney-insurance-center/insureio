require "net/http"
require 'rexml/document'

module Processing
  class Igo
    CARRIERS = ["Assurity", "AXA", "Genworth", "ING", "John Hancock", "Lincoln Benefit", "Lincoln Financial", "Minnesota Life", "Mutual of Omaha", "Nationwide", "Principal", "Prudential", "Transamerica", "United of Omaha"]

    def post_xml(crm_case, consumer)
      uri = URI.parse(APP_CONFIG['igo']["xml_web_service_url"])
      xml = build_xml(crm_case, consumer)
      response = Net::HTTP.post_form(uri, {"x" => xml, "user" => crm_case.try(:agent).try(:first_name) + crm_case.try(:agent).try(:last_name) + crm_case.try(:agent).try(:id).to_s})
      AuditLogger["igo"].info crm_case.id
      AuditLogger["igo"].info xml
      AuditLogger["igo"].info response.body
      response.body
    end

    def self.get_saml(crm_case, consumer)
        @igo_response= Processing::Igo.new.post_xml(crm_case, consumer)
        document = REXML::Document.new(@igo_response)
        text = document.root.text
    end

    private

    def build_xml(crm_case, consumer)
      xml = Builder::XmlMarkup.new(indent: 2, escape_attrs: true)
      xml.iGoApplicationData {
        xml << client_data_xml_builder.build_xml(crm_case, consumer)
        xml << user_data_xml_builder.build_xml(crm_case.agent_of_record)
        xml << session_data_xml_builder.build_xml
      }
      xml.target!
    end

    def client_data_xml_builder
      @client_data_xml_builder ||= ClientDataXmlBuilder.new
    end

    def user_data_xml_builder
      @user_data_xml_builder ||= UserDataXmlBuilder.new
    end

    def session_data_xml_builder
      @session_data_xml_builder ||= SessionDataXmlBuilder.new
    end
  end
end
