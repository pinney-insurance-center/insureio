module Processing
  class Smm
    def self.table_name_prefix
      'processing_smm_'
    end

    def schedule_url(kase, consumer=nil)
      web_url params(kase, consumer || kase.consumer)
    end

    private

    def params(crm_case, consumer)
      smm_carrier      = crm_case.a_team_details.try(:carrier).try(:smm_carrier)
      phone_collection = consumer.phones.sort_by{|p| p.phone_type_id.to_i }
      primary_phone    = phone_collection.first
      secondary_phone  = phone_collection.second if phone_collection.count > 1
      {
        cid:            smm_carrier.try(:smm_code),
        first_name:     (consumer.first_name),
        last_name:      (consumer.last_name),
        appphone:       primary_phone.to_s,
        appaltphone:    secondary_phone.to_s,
        email:          consumer.email.to_s,
        ssn:            consumer.ssn ? consumer.ssn.tr('-', '') : '',
        addr:           (consumer.primary_address.street),
        city:           consumer.primary_address.city,
        state:          two_char_state_abbrev(consumer),
        zip:            consumer.primary_address.zip,
        gender:         consumer.gender == MALE ? 'male' : 'female',
        dob:            consumer.birth.try(:strftime, "%m/%d/%Y"),
        crm_casenumber: crm_case.id,
        ref:            crm_case.id,
        faceamt:        crm_case.a_team_details.try(:face_amount),
        entityid:       APP_CONFIG['smm']['entity_id'],
        entitytype:     "2",
        noaddtolist:    true,
        test:           !Rails.env.production?
      }
    end

    def web_url(params)
      url = APP_CONFIG['smm']["web_url"]
      url + params.to_param
    end

    def two_char_state_abbrev(consumer)
      state=consumer.state || consumer.primary_address.state
      state.abbrev[0..1] if state
    end
  end
end
