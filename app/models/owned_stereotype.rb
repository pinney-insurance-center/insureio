module OwnedStereotype
  extend ActiveSupport::Concern
  extend Concerns::ModelHelpers::ClassMethods

  included do

    belongs_to :owner, class_name:'User' unless defined?(self::OWNER_ID_COLUMN)
    belongs_to :owner_name_only, ->{ select [:id,:full_name] }, class_name:'User', foreign_key: :owner_id unless defined?(self::OWNER_ID_COLUMN)
    belongs_to_enum :ownership, class_name:'Enum::Ownership'

    alias_attribute(:owner_id, self::OWNER_ID_COLUMN) if defined?(self::OWNER_ID_COLUMN) && !has_col?('owner_id')

    scope :owned_by_user, lambda { |user| where(owner_id:user.id) }

    scope :editable_by, lambda { |user|
      if user.can?(:super_edit)
        where(true)
      else
        where("owner_id=? OR owner_id IN (?)",
          user.id,
          User.editable_by(user).select(:id)
        )
      end
    }
    #DEPRECATED! USE `editable_by`.
    scope :editables, lambda {|user|
      SystemMailer.deprecated_method_warning rescue nil
      editable_by(user)
    }

    scope :viewable_by, lambda { |user|
      if user.can?(:super_edit) || user.can?(:super_view)
        where(true)
      else
        where("owner_id IS NULL 
          OR (ownership_id IS NULL)
          OR (ownership_id = ?)
          OR (owner_id = ?)
          OR (ownership_id = ? AND owner_id IN (?) )",
          Enum::Ownership::global_id,
          user.id,
          Enum::Ownership::user_and_descendants_id,
          user.ascendant_ids
        )
      end
    }
    #DEPRECATED! USE `viewable_by`.
    scope :viewables, lambda {|user|
      SystemMailer.deprecated_method_warning rescue nil
      viewable_by(user)
    }
  end

  def editable?(user)
    user.can?(:super_edit) or
    user.id == self.owner_id or
    self.owner.try(:editable?, user)
  end

  def viewable?(user)
    user.can?(:super_edit) or
    user.can?(:super_view) or
    owner_id.nil? or
    ownership_id.nil? or
    owner_id == user.id or
    ownership_id == Enum::Ownership::global_id or
    (ownership_id == Enum::Ownership::user_and_descendants_id and user.ascendant_ids.include?(owner_id))
  end

  def owner_name
    record=if association(:owner).loaded?
      agent
    else
      owner_name_only
    end
    record.try(:full_name)
  end

  module ClassMethods
    def ownership_is_always value
      value = 'user and descendants' if value ==  'descendants'
      @ownership = value
      # force ownership_id
      after_initialize {|obj| obj.ownership_id = Enum::Ownership.id(self.class.instance_variable_get('@ownership')); }
      attr_accessor :ownership_id unless column_names.include?('ownership_id')
      # overwrite viewable_by scope
      viewable_by_proc = case value
      when 'global'
        lambda{|user| where(nil) }
      when 'user'
        lambda{|user| where('owner_id IS NULL OR owner_id = ?', user.try(:id)) }
      when 'user and descendants'
        lambda{|user| where('owner_id IS NULL OR owner_id = ? OR owner_id IN (?)', user.try(:id), user.try(:ascendant_ids)) }
      end
      scope :viewable_by, viewable_by_proc
    end
  end
end
