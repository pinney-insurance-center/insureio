module QuotingHelper

  # Used in app_snapshot.html.erb.
  # Outputs a label and input, nesting calls to fields_for if 'form' arg is an Array.
  # e.g. <%= nested_form_field nil, [builder, :approved_details], :collection_select, :my_model_id, MyModel.all, :id, :name %>
  # Make label_text nil for just the input tag, no wrapping elements.
  def nested_form_field label_text, form, method, field, *args
    if form.is_a? Array
      if form.length > 1
        form.shift.fields_for form.first do |builder|
          form[0] = builder
          nested_form_field label_text, form, method, field, *args
        end
      else
        nested_form_field label_text, form.first, method, field, *args
      end
    else
      if form==:view_context#so that one can use methods like +check_box_tag+ that require no form element
        method_call=eval("#{method}(#{([field]+args).to_s.gsub(/\[|\]/,'')})")
      else
        method_call=form.send method, field, *args
      end
      if label_text.nil?
        method_call
      else
        %Q(<div class='row'>
        <div class='col-md-4'>#{label_text}</div>
        #{ method_call }
        </div>).html_safe
      end
    end
  end

  #Used in app_snapshot.html.erb:
  def nested_text_field label_text, form, field, *args
    nested_form_field label_text, form, :text_field, field, *args
  end

end