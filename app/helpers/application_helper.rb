module ApplicationHelper

  #not sure why this helper is here.
  #the default formatting can be (and is) set in environment.rb
  def strf_date(date_obj)
    date_obj.strftime("%m/%d/%y") rescue nil
  end
  alias_method :strfdate, :strf_date

  def strf_dt(date_time)
    date_time && date_time.strftime("%m/%d/%y %I:%M %P") rescue nil
  end

  # Wraps the +link_to+ helper but supplies +onclick+ directive to open a popup.
  # Default size is 700x700. This and other options can be overridden by supplying
  # parameters +:popup+, +:height+, & +:width+.
  def link_to_popup *args, &block
    options = args.extract_options!
    height  = options.delete(:height) || 700
    width   = options.delete(:width)  || 700
    popup   = options.delete(:popup)
    options[:onclick] = "window.open(this.href,'DRPopup','height=#{height},width=#{width},#{popup}'); return false;"
    link_to *args, options, &block
  end

  def user_email(message)
    unless message.user.blank?
      unless message.user.contact.blank?
        message.user.emails.blank? ? nil : message.user.emails.first
      end
    end
  end

  def consumer_email(message)
    unless message.consumer.blank?
      unless message.consumer.contact.blank?
        message.consumer.emails.blank? ? nil : message.consumer.emails.first
      end
    end
  end

  def receiver_emails(message)
    emails = []
    consumer_email = consumer_email(message)
    user_email = user_email(message)
    emails << '<p>'+"#{consumer_email.value}" unless consumer_email.try(:value).blank?
    emails << "#{user_email.value}" unless user_email.try(:value).blank?
    emails << "#{message.recipient}"+'</p>' unless message.recipient.blank?
    return emails.join(',')
  end

  def stylesheet_link_tag_for_tenant tenant
    stylesheet_link_tag(tenant.stylesheet || tenant.name) if tenant
  end

end