module Quoting::Xrae::ValueConverterHelper
  module ClassMethods

    #converts a Crm::Case object into an XraeDirect SOAP case object
    def crm_case_to_xrae_case crm_case
      health_info = crm_case.consumer.health_info
      saved = crm_case.persisted?
      uw_profile = 
      {
        SubjectProfile: {
          DateOfBirth: {
            Month: crm_case.consumer.birth.month,
            Day: crm_case.consumer.birth.day,
            Year: crm_case.consumer.birth.year
          },
          Gender: crm_case.consumer.gender ? 'Male' : 'Female',
          PhysicalBuild: {
            HeightFeet: health_info.feet,
            HeightInches: health_info.inches,
            WeightPounds: health_info.weight
          },
          PolicyDetails: {
            FaceAmount: crm_case.current_details.face_amount
          },
        },
        StateOfIssue: (crm_case.consumer.state||crm_case.consumer.primary_address.try(:state)).try(:abbrev).try(:sub,/-.*$/,'')
      }

      duration_id = crm_case.current_details.duration_id
      if duration_id
        uw_profile[:SubjectProfile][:PolicyDetails][:ProductType] = duration_id < 7 ? 'Term' : 'Perm'
      end

      if health_info.tobacco? == true
        tb_profile= uw_profile[:SubjectProfile][:TobaccoUse] = 
        {
          EverUse: true
        }

        if health_info.tobacco_cigarettes_current.nil? == false
          c1_profile= tb_profile[:CigaretteUse] = 
          {
            CurrentlyUse: health_info.tobacco_cigarettes_current
          }

          if health_info.tobacco_cigarettes_current == true
            c1_profile[:CigarettesPerDay] = health_info.tobacco_cigarettes_per_day || 0
          elsif health_info.tobacco_cigarette_last
            date_obj=ensure_date health_info.tobacco_cigarette_last
            c1_profile[:LastCigaretteDate] = 
            {
              Month: date_obj.month,
              Year: date_obj.year
            }
          end
        end
        if health_info.tobacco_cigars_current.nil? == false
          c2_profile=tb_profile[:CigarUse] = 
          {
            CurrentlyUse: health_info.tobacco_cigars_current
          }
          if health_info.cigars_current == true
            c2_profile[:CigarsPerYear] = health_info.tobacco_cigars_per_month || 0
          elsif health_info.tobacco_cigar_last
            date_obj=ensure_date health_info.health_info.tobacco_cigar_last
            c2_profile[:LastCigarDate] = 
            {
              Month: date_obj.month,
              Year: date_obj.year
            }
          end
        end
        if health_info.tobacco_pipe_current.nil? == false
          pipes_profile= tb_profile[:PipeUse] = 
          {
            CurrentlyUse: health_info.tobacco_pipe_current
          }
          if health_info.tobacco_pipe_current
            pipes_profile[:PipesPerYear] = health_info.tobacco_pipes_per_year || 0
          elsif health_info.tobacco_pipe_last
            date_obj=ensure_date health_info.tobacco_pipe_last
            pipes_profile[:LastPipeDate] = 
            {
              Month: date_obj.month,
              Year: date_obj.year
            }
          end
        end
        if health_info.tobacco_chewed_current.nil? == false
          tb_profile[:ChewSnuffUse] = 
          {
            CurrentlyUse: health_info.tobacco_chewed_current
          }
          if health_info.tobacco_chewed_current == false and health_info.tobacco_chewed_last.nil? == false
            date_obj=ensure_date health_info.tobacco_chewed_last
            tb_profile[:ChewSnuffUse][:LastChewDate] = 
            {
              Month: date_obj.month,
              Year: date_obj.year
            }
          end
        end
        if health_info.tobacco_nicotine_patch_or_gum_current.nil? == false
          tb_profile[:NicotineGumUse] = 
          {
            CurrentlyUse: health_info.tobacco_nicotine_patch_or_gum_current
          }
          if health_info.tobacco_nicotine_patch_or_gum_current == false and health_info.tobacco_nicotine_patch_or_gum_last.nil? == false
            date_obj=ensure_date health_info.tobacco_nicotine_patch_or_gum_last
            tb_profile[:NicotineGumuse][:LastGumDate] =
            {
              Month: date_obj.month,
              Year: date_obj.year
            }
          end
        end
      end

      if (health_info.bp_systolic != nil and health_info.bp_systolic != -1) or (health_info.bp_diastolic != nil and health_info.bp_diastolic != -1)
        uw_profile[:SubjectProfile][:BloodPressure] = 
        {
          SystolicCurrent: health_info.bp_systolic,
          DiastolicCurrent: health_info.bp_diastolic
        }
      end

      if (health_info.cholesterol_level != nil and health_info.cholesterol_level != -1) or (health_info.cholesterol_hdl != nil and health_info.cholesterol_hdl != -1)
        uw_profile[:SubjectProfile][:Cholesterol] =
        {
          TotalValue: health_info.cholesterol_level,
          HDLValue: health_info.cholesterol_hdl.to_i
        }
      end

      uw_profile[:SubjectProfile][:MedicalConditions] = { }

      #map an Insureio health condition name to an associated medical condition in xrae
      mapping={
        diabetes_1:                  :DiabetesType1,
        diabetes_2:                  :DiabetesType2,
        anxiety:                     :Anxiety,
        depression:                  :Depression,
        epilepsy:                    :Epilepsy,
        parkinsons:                  :Parkinsons,
        asthma:                      :Asthma,
        copd:                        :COPD,
        sleep_apnea:                 :SleepApnea,
        cancer_breast:               :CancerBreast,
        cancer_prostate:             :CancerProstate,
        cancer_skin:                 :CancerSkin,
        crohns:                      :Crohns,
        weight_reduction:            :WeightReduction,
        atrial_fibrillation:         :AtrialFibrillation,
        heart_murmur:                :HeartMurmur,
        irregular_heartbeat:         :IrregularHeartbeat,
        stroke:                      :Stroke,
        alcohol_abuse:               :AlcoholAbuse,
        drug_abuse:                  :DrugAbuse,
        elevated_liver_function:     :LFT,
        hepatitis_c:                 :HepatitisTypeC,
        arthritis:                   :ArthritisRheumatoid
      }

      mapping.each do |condition_field,xrae_med_cond|
        next unless health_info.send("#{condition_field}?")
        details_obj=health_info.send("#{condition_field}_details")
        filtered_details=details_obj.as_json
        filtered_details.delete_if{|k,v| details_obj.xrae_blacklisted_keys.include?(k.to_sym) || [:errors,:changed_attributes].include?(k.to_sym) }
        next unless filtered_details.present?
        xrae_attrs=xrae_condition_attributes filtered_details
        uw_profile[:SubjectProfile][:MedicalConditions][xrae_med_cond]=xrae_attrs
      end

      if [:other_internal_cancer, :other_skin_cancer].any? { |d| health_info.diseases[d].truthy? }
        uw_profile[:SubjectProfile][:MedicalConditions][:CancerOther] = { }
      end

      if health_info.heart_attack
        uw_profile[:SubjectProfile][:MedicalConditions][:CoronaryArtery] = { }#:MyocardialInfarction => true 
      end

      if health_info.cerebrovascular_disease.truthy?
        uw_profile[:SubjectProfile][:MedicalConditions][:Cerebrovascular] = { }
      end

      if health_info.hazardous_avocation
        uw_profile[:SubjectProfile][:HazardousAvocations] = { }
      end

      has_violations = health_info.moving_violations.any? do |v|
        (v.date&.<= 5.years.ago) && (v.dui_dwi || v.reckless_driving || v.dl_suspension)
      end
      if has_violations
        uw_profile[:SubjectProfile][:DrivingHistory] = { ConvictionExists: true }
      end

      swoop = Proc.new { |k, v| v.delete_if(&swoop) if v.kind_of?(Hash);  v.nil? }
      uw_profile.delete_if &swoop

      return uw_profile
    end

    def ensure_date date_or_string
      if date_or_string.is_a?(Date)
        date_or_string
      else
        Date.parse date_or_string
      end
    end

    def xrae_condition_attributes attrs
      result = Hash.new
      attrs.each { |k,v| 
        if(v.is_a?(Time) or v.is_a?(Date))
          v = {Month: v.try(:month), Year: v.try(:year)}
        end
        if(v.is_a?(String) and v.blank?)
          v = nil
        end
        result[k.to_s.camelize.to_sym] = v if (v.nil? == false)
      }
      return result
    end

    def xrae_rating_to_health_class(underwriting_class, table_rating, is_tobacco)
      table_rating = table_rating.to_i
      if table_rating == 0
        case underwriting_class
        when "SuperPreferred"
          return "PP"
        when "Preferred"
          return is_tobacco == true ? "Preferred Tobacco" : "P"
        when "StandardPlus"
          return "RP"
        when "Standard"
          return is_tobacco == true ? "Standard Tobacco" : "R"
        else
          return "R"
        end
      elsif table_rating.to_i <= 8
        i = table_rating.to_i
        a = 'A'.ord - 1 + i
        return ["Table #{i}/#{a}", is_tobacco.presence && ' - Tobacco'].compact.join
      else
        return "R"
      end
    end

    #Returns an array of hashes.
    def xrae_term_results_to_usable_results pricing_results, term_years = nil
      results = []
      pricing_results.sort!{|x,y| x[:ModalAnnual].to_f <=> y[:ModalAnnual].to_f}
      pricing_results.each_with_index do |pr, i|
        carrier = Carrier.find_by_naic_code(pr[:NAIC])
        health_code=xrae_rating_to_health_class(pr[:UnderwritingClass], pr[:TableRating], pr[:IsTobacco] == 'true')
        next unless term_years.nil? or term_years == pr[:TermYears].to_i
        result_hash={
          id:                  i,
          naic_code:           pr[:NAIC],
          carrier:             carrier,
          company_name:        pr[:CarrierName],
          product_name:        pr[:ProductName],
          annual_premium:      pr[:ModalAnnual],
          semi_annual_premium: pr[:ModalSemi],
          quarterly_premium:   pr[:ModalQuarterly],
          monthly_premium:     pr[:ModalMonthly],
          health_category:     pr[:UnderwritingClass] + '-' + pr[:ClassName],
          health_code:         health_code,
          term_length:         pr[:TermYears],
          display:             true,
        }
        results.push result_hash
      end
      return results
    end

    #Returns an array of hashes.
    def xrae_ul_results_to_usable_results pricing_results
      pricing_results.sort!{|x,y| x[:Premium][:Annual].to_f <=> y[:Premium][:Annual].to_f}
      results = []
      pricing_results.each_with_index do |pr, i|
        carrier = Carrier.find_by_naic_code(pr[:CompanyNAIC])
        result_hash={
          id:                i,
          naic_code:         pr[:CompanyNAIC],
          carrier:           carrier,
          company_name:      pr[:CompanyName],
          product_name:      pr[:ProductName],
          annual_premium:    pr[:Premium][:Annual],
          quarterly_premium: pr[:Premium][:Quarterly],
          monthly_premium:   pr[:Premium][:Monthly],
          health_category:   pr[:ClassTranslation],
          display:           true,
        }
        results.push result_hash
      end
      return results
    end

    #use this to convert terms from the select box to actual values usable by xrae
    def term_length_from_id term_select_id 
      case term_select_id
      when 1
        return 10
      when 2
        return 15
      when 3
        return 20
      when 4
        return 25
      when 5 
        return 30
      else
        return nil
      end
    end

  end
  
  module InstanceMethods
    
  end
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end