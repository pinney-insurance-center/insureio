module MarketingHelper
#Methods used for rendering marketing templates.

  def outstanding_reqs kase
    markup='<ul>'
    return '' unless kase.is_a?(Crm::Case)
    kase.outstanding_requirements.each do |req|
      markup+="<li>"+
      "Requirement: #{req.name} #{req.note}<br>\n"+
      "Required Of: #{req.case_req_responsible_party_type&.name || '-'}<br>\n"+
      "Ordered: #{req.ordered_at || '-'}<br>\n"+
      "Received: #{req.completed_at || '-'}<br>\n"+
      "Type: #{req.case_requirement_type&.name || '-'}"+
      "</li>"
    end
    markup+='</ul>'
  end

  def privacy_policy
    '<a href="https://insurancedivision.com/uploads/InsuranceDivisionPrivacyandDataSecurityPolicy.pdf">Privacy and Data Security Policy</a>'
  end

  def tracking_pixel message
    "<img src=\"#{base_url_for message}/messages/#{message.id}/tracking_pixel\" width=\"1\" height=\"1\" />"
  end

  # Bulk mailings sometimes include recipients for whome there is no person record, whence `recipient_email`
  def unsubscribe_link message
    is_for_status = "Status" == message.template&.template_purpose&.name
    from_what = is_for_status ? 'status' : 'marketing'
    unsub_url = "#{base_url_for message}/contacts/unsubscribe_from_#{from_what}_step_one?#{query_string message}"
    "<a href=\"#{unsub_url}\">Click to unsubscribe</a><br />"
  end

  # Bulk mailings sometimes include recipients for whome there is no person record, whence `recipient_email`
  def message_public_view_link message
    url = "#{base_url_for message}/messages/#{message.id}?#{query_string message}"
    "Having trouble viewing this email? <a href=\"#{url}\">Click here</a>"
  end

  def view_our_site_link url
    url.present? ? "<a href=\"#{url}\">Visit our Site</a>" : ''
  end

  private

  def base_url_for message
    APP_CONFIG['base_url'].sub(/app|pinney/, message.sender.tenant_name)
  end

  def query_string message
    {
      ltd_access_code: message.sender&.access_code(message.person), # For ltd_auth
      message_id: message.id, # For cross_reference_identity_for_message_links
      person_type: message.person&.class&.name, # For cross_reference_identity_for_message_links
      person_id: message.person&.id, # For cross_reference_identity_for_message_links
      recipient: message.recipient, # For cross_reference_identity_for_message_links
      key: message.sender&.quoter_key, # For cross_reference_identity_for_message_links
    }.select { |_,v| v.present? }.to_query
  end
end
