angular.module('dr.brands', ['DrBulletins'])
.controller("BrandMgmtCtrl", ['$scope', '$resource','$rootScope','$http','$location','UserModel','PaginatedBrandListModel','requestSanitizer','bulletinService',
  function ($scope, $resource,$rootScope,$http,$location,UserModel,PaginatedBrandListModel,requestSanitizer,bulletinService) {
    $scope.init=function(){
      $scope.brandCtrlVars={};
      if($scope.person){ $scope.relevantUser= $scope.person;}
      $scope.getBrands();
      ($scope.person||$scope.currentUser).$promise.then(function(){ $scope.getCurrentDefaultProfile(); });
      $scope.brandCtrlVars.showSection='list';//can be 'list' or 'form'
      //Check the url for a brand id. If provided, open the edit form for that brand.
      var brandId=$location.search.brand_id;
      if(brandId){
        var specifiedProfile=null;
        for(var i in $scope.brands){
          if($scope.brands[i].id==brandId){
            specifiedProfile=$scope.brands[i];
            break;
          }
        }
        if(specifiedProfile){
          $scope.editProfile(specifiedProfile);
        }else{
          $http.get('brands/'+brandId+'.json')
          .success(function(data, status, headers, config){
            $scope.editProfile(data);
          })
          .error(function(data, status, headers, config){
            $rootScope.addBulletin({text:'Failed to load the branding brand.', klass:'danger', id:transactionId});
          });
        }
      }
    }
    $scope.getBrands=function(){
      $scope.PaginatedBrandList = new PaginatedBrandListModel(
        $scope, $resource,
        '/brands.json?paginate=true'+( $scope.relevantUser ? '&user_id='+$scope.relevantUser.id : '' )
      );
    }
    $scope.getProfileNamesForMenu=function(searchStr){
      if(searchStr.length<=2){return; }
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading brands for menu.', id:transactionId});
      $http.get('brands.json',{params: {name:searchStr,'select[]':['id','name']} })
      .success(function(data, status, headers, config){
        $scope.brandCtrlVars.brandNamesForMenu=data;
        bulletinService.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load brands for menu.', klass:'danger', id:transactionId});
      });
    }

    $scope.getCurrentDefaultProfile=function(){
      var transactionId = bulletinService.randomNonZeroId(),
          brandId=($scope.relevantUser||$scope.currentUser).default_brand_id;
      bulletinService.add({text:'Loading current default brand name.', id:transactionId});
      $http.get('brands/'+brandId+'.json')
      .success(function(data, status, headers, config){
        $scope.brandCtrlVars.currentDefaultProfile={id:data.id,full_name:data.full_name};
        $scope.resetDefaultProfileMenu();
        bulletinService.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load current default brand name.', klass:'danger', id:transactionId});
      });
    }

    $scope.resetDefaultProfileMenu=function(){
      $scope.brandCtrlVars.tmpDefaultProfile=$scope.brandCtrlVars.currentDefaultProfile;
    }

    $scope.editProfile=function(brand){
      if($scope.currentUser.can('edit_initial_status_type_id')){
        $scope.getPolicyStatusTypes();
      }
      $scope.brandCtrlVars.showSection='form';
      $scope.activeProfile=brand;
      if(brand.id){
        $.each(['emails','phones','addresses','webs'],function(cIndex,contactMethod){
          //Convert keys like 'phones' to keys like 'phones_attributes'
          //in order to work smoothly with rails.
          $scope.activeProfile[contactMethod+'_attributes']=$scope.activeProfile[contactMethod]||[];
          delete $scope.activeProfile[contactMethod];
        });
      }
      $scope.contact=$scope.activeProfile;//so the contact method templates work.
    }

    $scope.cancelEdit=function(){
      $scope.getBrands();
      $scope.brandCtrlVars.showSection='list';
      $('#branding-edit').modal('hide');
      $scope.activeProfile={};
      $scope.contact={};
    }

    $scope.submitProfileForm=function(){
      var transactionId = bulletinService.randomNonZeroId(),
          httpMethod=$scope.activeProfile.id ? 'put' : 'post',
          brandCopy=$.extend(true,{},$scope.activeProfile),
          attrsToRemove=['created_at','updated_at','logo_url','header_url','initial_status_type','preferred_subdomain_set_already'];
      bulletinService.add({text:'Saving brand.', id:transactionId});

      requestSanitizer.clean(brandCopy,attrsToRemove);

      if(!brandCopy.preferred_insurance_division_subdomain){
        //don't save blank values
        delete brandCopy.preferred_insurance_division_subdomain;
      }
      $.each(['emails','phones','addresses','webs'],function(cIndex,contactMethod){
        var cM=brandCopy[contactMethod+'_attributes'];
        if(cM==null || cM && cM.length==0) delete brandCopy[contactMethod+'_attributes'];
      });

      $http[httpMethod]('/brands'+($scope.activeProfile.id ? ('/'+$scope.activeProfile.id) : '')+'.json',{brand:brandCopy} )
      .success(function(data, status, headers, config){
        $scope.getBrands();
        $('#branding-edit').modal('hide');
        $scope.brandCtrlVars.showSection='list';
        $scope.activeProfile={};
        $scope.contact={};
        bulletinService.add({text:'Successfully saved brand.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.addBulletin({text:'Failed to save brand.'+(data.errors||''), klass:'danger', id:transactionId});
      });
      return false;
    }

    $scope.removeProfile=function(id){
      if(!confirm('Are you sure you want to permanently destroy this brand?')){return;}
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Destroying brand.', id:transactionId});
      $http.delete('/brands/'+id+'.json')
      .success(function(data, status, headers, config){
        var brandIndex=null;
        $.each($scope.brands,function(index,s){
          if(s.id==id){ brandIndex=index; }
        });
        if(brandIndex!=null){ $scope.brands.splice(brandIndex,1); }
        bulletinService.add({text:'Successfully destroyed brand.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.addBulletin({text:'Failed to destroy brand.', klass:'danger', id:transactionId});
      });
    }

    $scope.keepProfileLogo = function (dataUrl, fileName) {
      $scope.activeProfile.logo_data_url=dataUrl;
      $('#logo-upload-modal').modal('hide');
    }

    $scope.keepProfileHeader = function (dataUrl, fileName) {
      $scope.activeProfile.header_data_url=dataUrl;
      $('#header-upload-modal').modal('hide');
    }

    $scope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller("ProfileMembershipsCtrl",['$scope','$rootScope','UserModel','$http','$resource','bulletinService',
  function ($scope,$rootScope,UserModel,$http,$resource,bulletinService) {
    $scope.init=function(){
      $scope.brandMembershipVars={};
      $scope.brandMembershipVars.agents=[];
      $scope.brandMembershipVars.brands=[];
      //
    }
    $scope.searchProfiles=function(searchStr){
      if(searchStr.length<3){ return; }
      $scope.brandsForSearch = $resource('/brands.json').query( {name:searchStr} );
    }
    $scope.getProfileMembers=function(p){
      var transactionId=$rootScope.randomNonZeroId();
      $rootScope.addBulletin({text:'Loading memberships for brand '+p.name+'.', id:transactionId});
      $http.get('/brands/'+p.id+'/member_ids.json?')
      .success(function(data, status, headers, config){
        p.memberIds = {};
        for(i in data){
          p.memberIds[data[i]]=1;
        }
        $rootScope.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to load memberships for brand '+p.name+'. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.selectAllForProfile=function(brand){
      for(var i in $scope.agents){
        if($scope.agents[i].showInTable){
          brand.memberIds[$scope.agents[i].id]=1;
        }
      }
    }
    $scope.selectAllForAgent=function(agent){
      for(var i in $scope.brands){
        if($scope.brands[i].showInTable){
          $scope.brands[i].memberIds[agent.id]=1;
        }
      }
    }
    $scope.sendUpdates=function(){
      $scope.saving=true;//This flag keeps the form from being resubmitted before it's finished.

      var joins=[],
          ps=$scope.brandMembershipVars.brands,
          as=$scope.brandMembershipVars.agents;
      //In future, since we keep looping over agent and brand lists,
      //looking for ones marked showInTable, the potential speed increase may make it worthwhile to maintain separate arrays
      //of pointers to the relevant agents and brands.
      for(var pIndex in ps){
        var p=ps[pIndex];
        for(var aIndex in as){
          var a=as[aIndex],
              isAgentAMemberOfProfile=p.memberIds[a.id];
          if( (typeof isAgentAMemberOfProfile)!='undefined'){
            joins.push([a.id, p.id, isAgentAMemberOfProfile ]);
          }
        }
      }
      if(!joins.length){ $scope.saving=false; return; }//Skip the request if none are marked.

      //It doesn't matter if existing joins are included as well as new ones, since the duplicates are ignored on the back end.
      var transactionId=$rootScope.randomNonZeroId();
      $rootScope.addBulletin({text:'Saving assignments.', id:transactionId});
      $http.post('/brands/update_assignments.json',{joins:joins})
      .success(function(data, status, headers, config){
        $scope.saving=false;
        $rootScope.addBulletin({text:'Successfully saved assignments.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        $scope.saving=false;
        $rootScope.addBulletin({text:'Failed to save assignments. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.init();
  }
])
.provider('PaginatedBrandListModel', function () {
  this.$get = ["$resource", '$rootScope', 'bulletinService', function ($resource, $rootScope, bulletinService) {
    function PaginatedBrandListModel($scope, $resource, url) {
      this.scope    = $scope;
      this.resource = $resource;
      this.url      = url;
      this.options  = new Object;
      this.get();
    }

    Object.defineProperties(PaginatedBrandListModel.prototype, {
      get: {
        writable: false,
        value: function () {
          var self = this,
              transactionId = bulletinService.randomNonZeroId();
          bulletinService.add({text:'Loading brand list.', id:transactionId});
          var response = this.resource(this.url).get(this.options);
          response.$promise.then(
            function (data) {//success
              self.scope.brands  = data.brands;
              self.page   = data.page;
              self.pageCt = data.page_count;
              if(self.scope.brandCtrlVars) self.scope.resetDefaultProfileMenu();
              bulletinService.removeBulletinById(transactionId);
              self.scope.brands.forEach(function(b){
                $rootScope.setPointersToPrimaryContactMethods(b);
              });
            }, function (xhr) {//error
              var msg;
              try { msg = xhr.data.error; } catch (ex) {}
              bulletinService.add({text:'Failed to load brand list. '+(msg||''), klass:'danger', id:transactionId});
            }
          );
          return response;
        }
      },
      pageChangeCallback: {
        get: function () {
          return this.setPage;
        }
      },
      pageChangeCallee: {
        value: true,
      },
      setPage: {
        writable: false,
        value: function (page) {
          this.options.page = page;
          this.get();
        }
      }
    });

    return PaginatedBrandListModel;
  }]
})
;
