angular.module('dr.controllers')
.controller("PaymentInfoCtrl", ['$scope', '$rootScope', '$resource', '$http', '$filter', '$uibModal',
  function ($scope, $rootScope, $resource, $http, $filter, $uibModal) {
    $scope.init=function(){
      $scope.paymentInfoVars={};
      $scope.paymentInfoVars.mId=$rootScope.currentUser.membership_id;
      $scope.paymentInfoVars.email=$rootScope.currentUser.$primaryEmail.value;
      $scope.paymentInfoVars.subscribed=$rootScope.currentUser.stripe_subscribed;
      $scope.paymentInfoVars.token==$rootScope.currentUser.stripe_customer_token

      var paymentInfo = new Object;

      $.when(
        $.getScript("https://checkout.stripe.com/checkout.js"),
        $.Deferred(function( deferred ){
            $( deferred.resolve );
       })
      ).done(function(){
        /* Handles credit card form for Stripe, once the Stripe checkout script is loaded. */
        $scope.paymentInfoVars.stripeNewCustomerHandler = StripeCheckout.configure({
          key: STRIPE_KEY,
          name:'Insureio',
          token: $scope.stripeNewCustomerCb
        });
        $scope.paymentInfoVars.stripeNewPaymentSourceHandler = StripeCheckout.configure({
          key: STRIPE_KEY,
          name:'Insureio',
          token: $scope.stripeNewPaymentSourceCb
        });
      });
    }

    $scope.openPromoCodeModal=function(){
      $scope.enrollModal = $uibModal.open({
          templateUrl: 'promo_code_modal.html',/* this refers to the id of an inline template. */
          controller : 'promoCodeModalCtrl',
          scope      : $scope
      });
    }

    $scope.checkOutWithStripe=function(tmpMId){
      $scope.paymentInfoVars.mId=tmpMId;
      if ($scope.paymentInfoVars.subscribed == true) {
        $scope.updatePlan();
      }else {
        $scope.createNewStripeCustomer();
      }
    }

    $scope.updatePlan=function(){
      var couponMsg=$scope.paymentInfoVars.couponCode ? ' with coupon code' : '',
          result = confirm('Are you sure you want to update plan'+couponMsg+'?');
      if (!result) { return; }
      //This function does not go through the `stripeHandler` first
      //because it does not need to create a token and a stripe customer.
      //The server retrieves the stripe customer from the stored `stripe_customer_id`.
      $http.post('/usage/stripe/update_plan.json',
        {membership_id: $scope.paymentInfoVars.mId, coupon_code:$scope.paymentInfoVars.couponCode}
      )
      .success(function(data, status, headers, config){
        var couponMsg=$scope.paymentInfoVars.couponCode ? ' with coupon code' : '';
        $rootScope.addBulletin({text:'Your payment info and plan'+couponMsg+' have been successfully updated.'});
      })
      .error(function(data, status, headers, config){
        var couponMsg=$scope.paymentInfoVars.couponCode ? 'Check the spelling and capitalization in your coupon code.' : '';
        var errorMsg='Your plan may not have been properly updated.'+couponMsg+' If this issue persists, please contact support. ';
        $rootScope.addBulletin({text:errorMsg + (data.errors||'') });
      });
    }

    $scope.createNewStripeCustomer=function(){
      $scope.paymentInfoVars.stripeNewCustomerHandler.open({
        key: STRIPE_KEY,
        description: $scope.membershipPlan().description,
        amount: $scope.membershipPlan().pricing*100,//Stripe API uses cents, so $15 is passed to them as 1500.
        email: $scope.paymentInfoVars.email
      });
    }

    $scope.updatePaymentSource=function(){
      $scope.paymentInfoVars.stripeNewPaymentSourceHandler.open({
        key: STRIPE_KEY,
        panelLabel:'Add or Update Card',
        email: $scope.paymentInfoVars.email
      });
    }

    //This callback gets run after calls to the stripe handler,
    //so the generated token and other pertinent info can be stored by our app.
    $scope.stripeNewCustomerCb=function(args) {
      $http.post('/usage/stripe/create_customer.json',
        {
          stripeToken:args,
          membership_id:$scope.paymentInfoVars.mId,
          coupon_code: $scope.paymentInfoVars.couponCode
        }
      )
      .success(function(data, status, headers, config){
        $scope.paymentInfoVars.subscribed=$rootScope.currentUser.stripe_subscribed=true;
        var couponMsg=$scope.paymentInfoVars.couponCode ? ' with coupon code' : '';
        $rootScope.addBulletin({text:'Your payment info and plan'+couponMsg+' have been successfully updated.'});
      })
      .error(function(data, status, headers, config){
        var couponMsg=$scope.paymentInfoVars.couponCode ? 'Check the spelling and capitalization in your coupon code.' : '';
        var errorMsg='Your payment info was updated, but your plan may not have been set.'+couponMsg+' If this issue persists, please contact support.';
        $rootScope.addBulletin({text:errorMsg + (data.errors||''), klass:'danger' });
      });
    }

    //This callback gets run after calls to the stripe handler,
    //so the generated token and other pertinent info can be stored by our app.
    $scope.stripeNewPaymentSourceCb=function(args) {
      $http.post('/usage/stripe/update_payment_source.json',
        {stripeToken:args}
      )
      .success(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Your payment info and plan have been successfully updated.'});
      })
      .error(function(data, status, headers, config){
        var errorMsg='Your payment info was updated, but your plan may not have been set. If this issue persists, please contact support.';
        $rootScope.addBulletin({text:errorMsg + (data.errors||''), klass:'danger' });
      });
    }

    $scope.unsubscribeFromStripe=function() {
      if (!confirm("Are you sure you want to unsubscribe?")) { return; }
       $http.get('/usage/stripe/unsubscribe.json')      
      .success(function(data, status, headers, config){
        $scope.paymentInfoVars.subscribed=$rootScope.currentUser.stripe_subscribed=false;
        $rootScope.addBulletin({text:'Your payment info and plan have been successfully updated.'});
      })
      .error(function(data, status, headers, config){
        var errorMsg='Error unsubscribing.';
        $rootScope.addBulletin({text:errorMsg + (data.errors||''), klass:'danger' });
      });
    }


    $scope.membershipPlan=function(){
      return $filter('whereKeyMatchesValue')( MEMBERSHIPS, 'id', ($scope.paymentInfoVars.mId||0) )
    }

    $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller('promoCodeModalCtrl', ['$scope', '$rootScope', '$http', '$uibModalInstance',
  function ($scope, $rootScope, $http, $uibModalInstance) {
    $scope.setPromoCode=function(){
      $uibModalInstance.close();
      if($scope.paymentInfoVars.subscribed){
        $scope.checkOutWithStripe($scope.paymentInfoVars.mId);
      }else{
        var bulletinText='Your promo code is ready to be applied to a payment plan. '+
          'It has not yet been checked for validity. '+
          'Next, please select a payment plan.';
        $rootScope.addBulletin({text:bulletinText, klass:'success'});
      }
    }
  }
])
;
