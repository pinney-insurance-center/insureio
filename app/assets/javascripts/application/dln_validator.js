

window.validateDriversLicenseNumber=function(stateAbbrev, dln, consumer){
  var stateDlnPatterns={
        /* Each pattern string is either a function name or an escaped partial (missing flags and start/end markers) regex. */
        AK: {patterns:["\\d{1,7}"],                                                         human:"1 to 7 digits"},
        AL: {patterns:["\\d{7}",                    "\\d{1,7}"],                            human:"1 to 7 digits"},
        AR: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        AZ: {patterns:["[A-Z]{1}\\d{8}",            "[A-Z]{2}\\d{3,6}",   "\\d{9}"],        human:"(1 letters then 8 digits) OR (2 letters then 3 to 6 digits) OR (9 digits)"},
        CA: {patterns:["[A-Z]{1}\\d{7}"],                                                   human:"1 letters then 7 digits"},
        CO: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        CT: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        DC: {patterns:["matchesSSN",               "\\d{7}"],                               human:"SSN OR 7 digits"},
        DE: {patterns:["\\d{1,7}"],                                                         human:"1 to 7 digits"},
        FL: {patterns:["[A-Z]{1}\\d{12}"],                                                  human:"1 letter then 12 digits"},
        GA: {patterns:["\\d{7,9}",                  "matchesSSN"],                          human:"SSN OR 7 to 9 digits"},
        HI: {patterns:["(\\d|H){1}\\d{8}"],                                                 human:"(9 digits) OR (1 letter H then 8 digits)"},
        IA: {patterns:["\\d{9}",                    "\\d{3}[A-Z]{2}\\d{4}"],                human:"(9 digits) OR (3 digits then 2 letters then 4 digits)"},
        ID: {patterns:["[A-Z]{2}\\d{6}[A-Z]{1}",    "\\d{9}"],                              human:"(9 digits) OR (2 letters then 6 digits then 1 letter)"},
        IL: {patterns:["[A-Z]{1}\\d{11}"],                                                  human:"1 letter then 11 digits"},
        IN: {patterns:["\\d{10}",                   "matchesSSN"],                          human:"SSN OR 10 digits"},
        KS: {patterns:["matchesSSN",               "[A-Z]{1}\\d{8}"],                       human:"SSN OR (1 letter then 8 digits)"},
        KY: {patterns:["matchesSSN",               "[A-Z]{1}\\d{8}"],                       human:"SSN OR (1 letter then 8 digits)"},
        LA: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        MA: {patterns:["[A-Z]{1}\\d{8}",            "matchesSSN"],                          human:"SSN OR (1 letter then 8 digits)"},
        MD: {patterns:["[A-Z]{1}\\d{12}"],                                                  human:"1 letter then 12 digits"},
        ME: {patterns:["validForME"],                                                       human:"7 digits if under 21 OR 7 digits followed by 1 letter X"},
        MI: {patterns:["[A-Z]{1}(\\d{10}|\\d{12})"],                                        human:"1 letter then 10 OR 12 digits"},
        MN: {patterns:["[A-Z]{1}\\d{12}"],                                                  human:"1 letter then 12 digits"},
        MO: {patterns:["[A-Z]{1}\\d{5,9}",          "\\d{9}",             "matchesSSN"],    human:"SSN OR (1 letter then 5 to 9 digits) OR (9 digits)"},
        MS: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        MT: {patterns:["matchesSSN",               "\\d{13}"],                              human:"SSN OR  13 digits"},
        NC: {patterns:["\\d{1,8}"],                                                         human:"1 to 8 digits"},
        ND: {patterns:["matchesSSN",               "(9){1}\\d{9}",        "[A-Z]{3}\\d{6}"],human:"SSN OR (1 digit 9 then 8 digits) OR (3 letters then 6 digits)"},
        NE: {patterns:["(A|B|C|E|G|H|V){1}\\d{3,8}"],                                       human:"1 letter (A, B, C, E, G, H, V) then 3 to 8 digits"},
        NH: {patterns:["\\d{2}[A-Z]{3}\\d{5}"],                                             human:"2 digits then 3 letters then 5 digits"},
        NJ: {patterns:["[A-Z]{1}\\d{14}"],                                                  human:"1 letter then 14 digits"},
        NM: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        NV: {patterns:["validForNV"],                                                       human:"(10 digits then last 2 digits of birth year) OR (10 digits)"},
        NY: {patterns:["\\d{9}"],                                                           human:"9 digits"},
        OH: {patterns:["matchesSSN",               "[A-Z]{2}\\d{6}"],                       human:"SSN OR (2 letters then 6 digits)"},
        OK: {patterns:["\\d{9}",                    "([A-Z]|\\d){1}\\d{9}"],                human:"(9 digits) OR (1 letter/digit character then 9 digits)"},
        OR: {patterns:["\\d{1,7}"],                                                         human:"1 to 7 digits"},
        PA: {patterns:["\\d{8}"],                                                           human:"8 digits"},
        RI: {patterns:["\\d{7}",                    "(V){1}\\d{6}"],                        human:"(7 digits) OR (1 letter V and 6 digits for disabled veterans)"},
        SC: {patterns:["\\d{6,9}"],                                                         human:"6 to 9 digits"},
        SD: {patterns:["\\d{8}",                    "matchesSSN"],                          human:"SSN OR 8 digits"},
        TN: {patterns:["\\d{7,9}"],                                                         human:"7 to 9 digits"},
        TX: {patterns:["(0|1|2|3){1}\\d{8}"],                                               human:"1 digit from the set (0, 1, 2, 3) then 7 digits"},
        UT: {patterns:["\\d{4,10}"],                                                        human:"4 to 10 digits"},
        VA: {patterns:["matchesSSN",               "[A-Z]{1}\\d{8}"],                       human:"SSN OR (1 letter then 8 digits)"},
        VT: {patterns:["\\d{8}",                    "\\d{7}(A){1}"],                        human:"(8 digits) OR (7 digits then one letter A)"},
        WA: {patterns:["validForWA"],                                                       human:"5 letters (last name) then 1 letter (first name) 1 letter (middle name) then 3 digits then 2 letter/digit characters"},
        WI: {patterns:["[A-Z]{1}\\d{13}"],                                                  human:"1 letter then 13 digits"},
        WV: {patterns:["(0|A|B|C|D|E|F|S)\\d{6}",   "X{1,2}\\d{5}"],                        human:"(1 character from the set (0, A, B, C, D, E, F, S) then 6 digits) OR (1 to 2 of the letter X then 5 digits)"},
        WY: {patterns:["\\d{10}",                   "matchesSSN"],                          human:"SSN OR 10 digits"}
      },
      helperFunctions={
        matchesSSN:function(){
          return dln==consumer.ssn;
        },
        validForWA:function(){
          var name      =consumer.full_name.split(' '),
              last      =name[name.length-1].substring(0, 5),
              first     =name[0].substring(0, 1),
              middle    =name[1].substring(0, 1),
              patternStr='^'+last+first+middle+'\\d{3}[A-Z0-9]{2}$',
              r         =new RegExp(patternStr, i);

          return dln.match(r);
        },
        validForME:function(){
          return dln.match(/^\d{7}X?$/);
        },
        validForNV:function(){
          var birthYear =consumer.birth_or_trust_date.substring(8,2),//assumes a string in MM/DD/YYYY date format.
              patternStr='^\\d{10}('+birthYear+')?$',
              r         =new RegExp(patternStr, i);

          return dln.match(r);
        }
      },
      patternTuple=stateDlnPatterns[stateAbbrev]['patterns'],
      patternMatched=false,
      errorMsg='';

  if(!stateAbbrev || !stateDlnPatterns[stateAbbrev]){
    return [false, "You must select a  driver's license state before entering driver's license number in order for DLN to be validated properly."]
  }else if(!dln){
    return [false,""];
  }

  for (var i in  patternTuple){
    var patternString=patternTuple[i];
    if(!patternString || patternMatched){
      continue;
    }
    var functionNames=Object.keys(helperFunctions),
        patternStringIsAFunctionName=functionNames.includes(patternString);
    if( patternStringIsAFunctionName ){
      var functionName=patternString;

      if( helperFunctions[functionName](dln) ){
        patternMatched=true;
        break;
      }
    }else{//patternString is a regex
      var r=new RegExp('^'+patternString+'$', 'i');
      if( dln.match(r) ){
        patternMatched=true;
        break;
      }
    }
  };
  if(!patternMatched){
     errorMsg="Driver's licenses in "+stateAbbrev+" must be "+stateDlnPatterns[stateAbbrev]['human']+".";
  }
  return [patternMatched, errorMsg];
}
