angular.module('io.util', ['ioConstants'])
.service('Util', [ '$location', 'ioConstantsObject', function ($location, ioConstantsObject) {
  /* Find an instance (or a field on an instance) of an Enum */
  this.enum = (enumName, id, field) => {
    if (!this.ENUMS) this.ENUMS = {};
    /* Build a map from id => object */
    if (!this.ENUMS[enumName]) {
      this.ENUMS[enumName] = ioConstantsObject[enumName].reduce((map, obj) => {
        map[obj.id] = obj;
        return map;
      }, {});
    }
    const obj = this.ENUMS[enumName][id];
    return field ? (obj && obj[field]) : obj;
  };

  this.firstName = (name) => {
    return !!name && name.split(/\s+/)[0];
  };

  /* Return YYYY/MM/DD string. (Not using toLocaleStirng with 'jp-JA' because
  /* node doesn't ship with support for locales by default. It silently
  /* disregards the locale and uses whatever region is its default.) */
  this.formatDate = (date) => {
    if (typeof date != 'object') date = new Date(date);
    return `${date.getFullYear()}/${date.toLocaleString('en-US', { month: '2-digit', day: '2-digit' })}`
  };

  this.genderString = (gender) => {
    if (gender == null) return;
    if (typeof gender === 'string') gender = parseInt(gender);
    return !!gender == ioConstantsObject.FEMALE ? 'Female' : 'Male';
  };

  /* Check String or Date for validity as date */
  this.invalidDate = (value) => {
    let date = value instanceof Date ? value : new Date(value);
    return isNaN(date.valueOf());
  };

  this.lastName = (name) => {
    if (!name) return;
    let names = name.split(/\s+/);
    if (names.length > 2) return names.slice(2, 99).join(' ');
    else if (names.length > 1) return names.pop();
  };

  this.middleName = (name) => {
    if (!name) return;
    let names = name.split(/\s+/);
    if (names.length > 2) return names[1];
  }

  /* Update query string in window url (`window.location.search`) without re-navigating browser */
  this.updateUrlParams = (newParams, doClearOld) => {
    Object.keys(newParams).forEach(key => newParams[key] == null && delete newParams[key]);
    let params = doClearOld ? newParams : Object.assign($location.search(), newParams);
    let queryString = new URLSearchParams(params).toString();
    let newUrl = window.location.href.replace(/\?.*|$/, `?${queryString}`);
    window.history.pushState({}, null, newUrl);
  }

  /* Return a date `n` years in the past */
  this.yearsAgo = (n) => {
    let d = new Date();
    let fpart = n % 1;
    let ipart = Math.floor(n);
    d.setYear( d.getFullYear() - ipart );
    return new Date(d - (fpart * 1000 * 60 * 60 * 24 * 365));
  };

  /* Return a float for number of years arg `date` occurs in the past */
  this.yearsSince = (date) => {
    if (!date) return undefined; /* Return `undefined` instead of `null` b/c the former will return `false` for both `>` and `<` */
    var then = new Date(date);
    if (this.invalidDate(then)) return undefined; /* Return `undefined` instead of `null` b/c the former will return `false` for both `>` and `<` */
    var now = new Date();
    var years = now.getYear() - then.getYear();
    now.setYear(0);
    then.setYear(0);
    years += (now - then) / 1000 / 60 / 60 / 24 / 365; /* This contains a residual leap-year error. But said error is minimized by the previous steps. */
    return years;
  };
}])
;
