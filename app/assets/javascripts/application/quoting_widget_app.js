//create new ngApp
var widgetApp = angular.module("WidgetApp", ['RestfulResource',  'ui.bootstrap']);
// we include RestfulResource - a modified version of ngResource which uses PUT/UPDATE requests for updating

//angular directive for binding with a colorpicker control
widgetApp.directive('colorpicker', function(){
  return {
    require: '?ngModel',
    link: function (scope, elem, attrs, ngModel) {
      elem.spectrum();
      if (!ngModel) return;
      ngModel.$render = function () {
        elem.spectrum('set', ngModel.$viewValue || '#fff');
      };
      elem.on('change', function () {
        if(!scope.$$phase)
            scope.$apply(function () {
              ngModel.$setViewValue(elem.val());
            });
      });
    }
  }
})

//factory for our Widget rails objects using RestfulResource
widgetApp.factory("Widget", ['Resource', function ($resource) {
    var Widgets = $resource('/quoting/widgets/:id.json',
        {
            id: '@id',
        });
    Widgets.prototype.isCarrierBlacklisted = function(id)
    {
        return this.excluded_ids.indexOf(id) != -1
    };
    Widgets.prototype.toggleCarrier = function(id)
    {
        if(this.isCarrierBlacklisted(id))
        {
            //remove
            var idx = this.excluded_ids.indexOf(id);
            this.excluded_ids.splice(idx,1);
        }
        else
        {
            this.excluded_ids.push(id);
        }
    };
    return Widgets;
}]);

//our ngController for our widgets.
widgetApp.controller("WidgetCtrl", function ($scope, bulletinService, $location, $resource, $http, $timeout, $uibModal, Widget){
    $scope.init=function(){
      $scope.editWidget = null;
      $scope.widgetBaseUrl =$location.$$protocol+'://'+$location.$$host;
      $scope.widgetBaseUrl+=($location.$$port ? ':' : '')+$location.$$port;
      $scope.widgetBaseUrl+='/quoting/widgets/';
      for(let i in WIDGET_TYPES){//TLI, LTC, CAP, RTT
        let abbrev=WIDGET_TYPES[i].abbrev.toUpperCase(),
            id    =WIDGET_TYPES[i].id
        $scope['ID_FOR_WIDGET_TYPE_'+abbrev]=id;
      }

      $scope.widgets = Widget.query();
      $scope.getBrands();
    }

    $scope.getBrands = function (event, select) {
      $scope.brands = [];
      var transactionId=$rootScope.randomNonZeroId();
      bulletinService.add({text:'Loading brand options...',id:transactionId});
      $scope.brands = $resource('/brands/autocomplete.json').query();
      $scope.brands.$promise.then(function(){
        $rootScope.removeBulletinById(transactionId);
        $scope.brands.sort(function(a,b) {return a.name == b.name ? 0 : a.name < b.name ? -1 : 1});
      }, function(){
        bulletinService.add({text:'Brand options failed to load.', klass:'danger', id:transactionId});
      });
    }

    $scope.htmlSnippet= function(id){
      if(!id){ return ''; }
      var text='<script src="'+$scope.widgetBaseUrl+id+'.js" id="dr-quoter-widget-'+id+'-script" defer></script>';
      return text;
    }

    $scope.openEditModal = function(index){
      if( (typeof index)!='undefined' && index!=null){
        $scope.editWidget = angular.copy($scope.widgets[index]);
      }else{
        $scope.editWidget = new Widget({
          header_text_color: '#4F62E3',
          background_color:  '#FFFFFF',
          border_color:      '#4F62E3',
          button_color:      '#4F62E3',
          text_color:        '#000000',
          header_text:       'Get A Free Quote',
          button_text:       'Compare Quotes',
          widget_type_id:    1,
          quoter_style:      1,
          excluded_ids:      [],
          show_name_field:   true,
          show_phone_field:  true,
          show_email_field:  true,
          tags:              '',
          agent_split:       '',
          brand_id:        $rootScope.currentUser.default_brand_id,
        });
      }

      $uibModal.open({
        windowClass: 'super-size',
        templateUrl: '/widgets/edit_modal.html',
        controller : 'widgetFormModalCtrl',
        scope      : $scope||$rootScope
      });
    };

    $scope.openShowModal = function(index){
      $scope.selectedWidget = $scope.widgets[index];
      $uibModal.open({
        size       : 'lg',
        templateUrl: '/widgets/modal_for_show.html',
        controller : 'widgetFormModalCtrl',
        scope      : $scope||$rootScope
      });
    }

    $scope.openTosModal = function(index){
      $scope.selectedWidget = $scope.widgets[index];
      $uibModal.open({
        template: '\
        <div class="modal-header">\
          <a type="button" class="close" ng-click="$close()" data-dismiss="modal">×</a>\
          <h4 class="modal-title">Insureio Quoting Widget Terms of Service</h4>\
        </div>\
        <div class="modal-body" id="edit-widget">\
          <ng-include src="\'/rtt/widget_tos.html\'"></ng-include>\
        </div>\
        <div class="modal-footer">\
          <a class="btn btn-primary pull-right" ng-click="$close()">Continue</a>\
        </div>',
        controller : 'widgetTosModalCtrl',
        scope      : $scope||$rootScope
      });
    }

    $scope.destroyWidget = function(index){
      widget = $scope.widgets[index]
      widget.$delete(
        function() {
          $scope.widgets.splice(index,1);
        },
        function() {
          bulletinService.add({text:'Failed to destroy widget', klass:'danger'});
        }
      );
    };

    //This function re-applies inline styles whenever AngularJS ngIf directive adds widget elements to the dom.
    //Would probably benefit from changing this to a watcher later.
    $scope.updatePreviewStyles = function(){
      $timeout(
        function(){
          if(!$scope.editWidget){ return; }
          $.each(Object.keys($scope.editWidget),function(i,k){
            $scope.updatePreviewStyle(k);
          });
        },250);
    }

    $scope.updatePreviewStyle= function(field){
      var value=$scope.editWidget[field];

      switch(field){
        case 'background_color':
          $('#edit-widget .drQuoter .quoter-inner-container').css('background-color',value);
          break;
        case 'text_color':
          $('#edit-widget .drQuoter .quoter-inner-container label').css('color',value);
          break;
        case 'button_color':
          $('#edit-widget .drQuoter .widget-btn').css('background-color',value);
          break;
        case 'header_text_color':
          $('#edit-widget .drQuoter h1').css('color',value);
          $('#edit-widget .quoter-inner-container .brand-logo div').css('color',value);
          $('#edit-widget .quoter-inner-container .brand-logo div span').css('color',value);
          break;
        case 'border_color':
          $('#edit-widget .drQuoter .quoter-inner-container').css('border-color',value);
          $('#edit-widget .drQuoter hr').css('border-color',value);
          break;
        case 'show_name_field':
          $('#edit-widget label[for="dr-widget-name"]').css('display',value ? 'block' : 'none');
          $('#edit-widget input[name="name"]').css('display',value ? 'block' : 'none');
          break;
        case 'show_phone_field':
          $('#edit-widget label[for="dr-widget-phone"]').css('display',value ? 'block' : 'none');
          $('#edit-widget input[name="phone"]').css('display',value ? 'block' : 'none');
          break;
        case 'show_email_field':
          $('#edit-widget label[for="dr-widget-email"]').css('display',value ? 'block' : 'none');
          $('#edit-widget input[name="email"]').css('display',value ? 'block' : 'none');
          break;
      }
    }

    $scope.getCurrentUser().$promise.then(function () {
      $scope.init();
    });
})
.controller("widgetFormModalCtrl",
  function ($scope, bulletinService, $location, $resource, $http, $uibModalInstance, Widget, requestSanitizer){
    $scope.modalVars={};
    $scope.modalVars.rttTerms=false;

    $scope.updatePreviewStyles();

    $scope.saveWidget = function(){
      if(!$scope.editWidget) { return; }
      var newObject = $scope.editWidget.id == null,
          transactionId=$rootScope.randomNonZeroId();
      requestSanitizer.clean($scope.editWidget,['created_at','updated_at','user_name','brand_name']);

      bulletinService.add({text:'Saving widget.', klass:'info', isBusy:true, id:transactionId});
      $scope.editWidget.$save(
        function(item, response) {//success
            bulletinService.add({text:'Successfully saved widget.', klass:'success', id:transactionId});
            if(newObject){
              $scope.widgets.push($scope.editWidget);
              $scope.openShowModal($scope.widgets.length-1);
            } else {
              $scope.$parent.widgets = Widget.query();
            }
            $scope.editWidget = null;
            $uibModalInstance.dismiss('close');
        },
        function(item, response){ //error
            try{msg=item.data.errors;}catch(e){msg="Failed to save widget";}
            bulletinService.add({text:msg, klass:'danger', id:transactionId});
        }
      );
    };

  }
)
.controller("widgetTosModalCtrl",
  function ($scope, bulletinService, $location, $resource, $http, $uibModalInstance, Widget){
    //
  }
);