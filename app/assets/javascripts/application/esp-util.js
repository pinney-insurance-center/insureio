/* This file holds tools for working with the underwriting service ESP. */

angular.module('io.espUtil', ['io.util', 'DrBulletins'])
.service('EspUtil', ['$http', 'Util', 'bulletinService',
function ($http, Util, bulletinService) {

  this.downloadDocument = (kase, which, doBuild) => {
    let [promise, xId] = doBuild ? generateDocument(kase, which) : getSignedUrl(kase, which);
    promise.then((response) => {
      bulletinService.add({klass: 'success', text: `Opening download for ${which}...`, id: xId, ttl: 3000});
      /* Download target from the app server instead of from AWS b/c AWS does
      /* not set content-disposition header, and chrome is not honouring the
      /* <a> 'download' attribute without it. */
      let a = document.createElement('a');
      a.style.display = 'none';
      a.download = true;
      a.href = `/ixn/${kase.id}/download?type=${which}`;
      document.body.appendChild(a);
      a.click();
      a.remove();
    });
  }

  this.openDocument = (kase, which) => {
    let [promise, xId] = getSignedUrl(kase, which);
    promise.then((response) => {
      bulletinService.add({klass: 'success', text: `Received signed URL for ${which}`, id: xId});
      window.open(response.data.url, '_blank');
    });
  }

  this.sendKase = (kase) => {
    let xId = bulletinService.randomNonZeroId();
    bulletinService.add({klass: 'info', ttl: 256 * 1000 * 1000, text: 'Preparing documents for ESP. This could take a minute. You\'ll be notified of completion if you remain on this page.', id: xId});
    return $http({url: `/ixn/${kase.id}/esp.json`, method: 'PUT',
      headers: {'Content-type': 'application/json'},
    }).then(
    (response) => {
      bulletinService.add({klass: 'success', text: 'Sent to ESP', id: xId, ttl: 6000});
      Object.values(FIELD_FOR_EXT).forEach(key => { kase.ext[key] = true });
    },
    (response) => {
      bulletinService.add({klass: 'danger', text: `Unable to send to ESP. ${response.data.errors}`, id: xId});
    });
  };

  function getSignedUrl (kase, which) {
    if (!kase.ext[FIELD_FOR_EXT[which]]) {
      bulletinService.add({klass: 'warning', text: 'This case has not been sent to ESP, so this document has not been created'});
      return [Promise.reject(), null];
    }
    let xId = bulletinService.randomNonZeroId();
    bulletinService.add({klass: 'info', text: `Requesting signed URL for ${which} resource...`, id: xId});
    let promise = $http.get(`/ixn/${kase.id}/${which}`)
    .catch(response => { bulletinService.add({klass: 'danger', text: `No URL returned for ${which}. ${response.data.errors}`, id: xId}) });
    return [promise, xId];
  }

  function generateDocument (kase, which) {
    let xId = bulletinService.randomNonZeroId();
    bulletinService.add({klass: 'info', ttl: 256 * 1000 * 1000, text: 'Preparing documents. This could take a minute. You\'ll be notified of completion if you remain on this page.', id: xId});
    let promise = $http({url: `/ixn/${kase.id}/generate_pdf.json`, method: 'PUT', headers: {'Content-type': 'application/json'} })
    .then(response => { kase.ext[FIELD_FOR_EXT[which]] = true })
    .catch(response => { bulletinService.add({klass: 'danger', text: `Unable to prepare documents. ${response.data.errors}`, id: xId}) });
    return [promise, xId];
  }

  const FIELD_FOR_EXT = {pdf: 'nmb_pdf_saved', xml: 'esp_push'};
}]);
