angular.module('DrFilters', [])
/* Format a currency only if its value is greater than zero */
.filter('currencyGt0', ['currencyFilter', (currencyFilter) => {
  return (value, dummyChar) => {
    if (isNaN(value) || value == 0) return;
    return currencyFilter(value);
  };
}])
/*
  Get fieldName from an item in any enum declared on $rootScope.
  E.g.
    {{ 2 | enumValue : 'PHONE_TYPES' }} // => work
    {{ 3 | enumValue : 'STATES' : 'abbrev' }} // => AZ
*/
.filter('enumValue', ['$rootScope', function ($rootScope) {
  return function (itemId, enumName, fieldName) {
    if (itemId!==null && typeof(itemId)!=undefined) {//allow 0 (since the gender constant has 0 and 1 as keys)
      if (!fieldName) fieldName = 'name';
      var items = $rootScope[enumName];
      if (!items)
        throw("No value defined for $rootScope." + enumName);
      var item;
      if (items[itemId - 1] && (items[itemId - 1].id == itemId || items[itemId - 1].value == itemId) )
        item = items[itemId - 1];
      else if (items[itemId] && (items[itemId].id == itemId || items[itemId].value == itemId) )
        item = items[itemId];
      else {
        for (var i in items)
          if (items[i].id === itemId || items[i].value === itemId) {
            item = items[i];
            break;
          }
      }
      if (item)
        return item[fieldName];
    }
  }
}])
//This filter was borrowed from https://gist.github.com/cmmartin/341b017194bac09ffa1a

// REQUIRES:
// moment.js - https://github.com/moment/momentjs.com

// USAGE:
// {{ someDate | moment: [any moment function] : [param1] : [param2] : [param n]

// EXAMPLES:
// {{ someDate | moment: 'format': 'MMM DD, YYYY' }}
// {{ someDate | moment: 'fromNow' }}

// To call multiple moment functions, you can chain them.
// For example, this converts to UTC and then formats...
// {{ someDate | moment: 'utc' | moment: 'format': 'MMM DD, YYYY' }}
.filter('moment', function () {
  return function (input, momentFn /*, param1, param2, etc... */) {
    if( !input ){ return input; }
    var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
    return momentObj[momentFn].apply(momentObj, args);
  };
})//end of borrowed code
//This filter provides a shorthand notation for using moment
//to convert to standard US date format, falling back to a blank response
//in situations where the input value may be blank, null, or undefined.
.filter('formatOptionalDateAsUS',function ($filter) {
  return function (input) {
    var formattedDate='';
    if( (typeof(input)=='string' && input.length) || (input instanceof Date) ){
      formattedDate=$filter('moment')(input,'format','M/D/YYYY');
    }
    return formattedDate;
  };
})
.filter('formatAsYearsAgo', function () {
  return function (inputDate) {
    if( !inputDate ){ return inputDate; }
    var today=moment( new Date() );
    return today.diff(inputDate, 'years');
  };
})
//This filter will display today, tomorrow, yesterday then display the actual date as 'M/D/Y at h:mm a'.
//Default preposition is 'at', but any other can be passed in as the second parameter.
.filter('formatForTimeAgo', function () {
  return function timeTodayDateElse(date, preposition){
    if(!date){ return date; }
    preposition=preposition|| 'at'
    moment.locale('en', {
      'calendar' : {
        'lastDay'  : '[Yesterday] [\n][ '+preposition+' ] h:mm a',
        'sameDay'  : '[Today] [\n][ '+preposition+' ] h:mm a',
        'nextDay'  : '[Tomorrow] [\n][ '+preposition+' ] h:mm a',
        'lastWeek' : 'M/D/YY [\n]['+preposition+'] h:mm a',
        'nextWeek' : 'M/D/YY [\n]['+preposition+'] h:mm a',
        'sameElse' : 'M/D/YY [\n]['+preposition+'] h:mm a'
       }
    });
    return moment(date).calendar();
  }
})
.filter("whereKeyMatchesValue", function () {
  return function (objList, key, valueToMatch) {
    return $.grep(objList||[],function(obj,index){ return typeof(obj)=='object' && obj[key]==valueToMatch; });
  }
})
.filter("whereKeyContainsValue", function () {
  return function (objList, key, valueToMatch) {
    const lwrCase = typeof valueToMatch === 'string' && valueToMatch.toLowerCase();
    return $.grep(objList||[], obj => typeof obj === 'object' && (obj[key]||'').toLowerCase().indexOf(lwrCase) >= 0);
  }
})
.filter('caseNotesOnly', function () {
  return function (notes) {
    var output = [];
    for (var i in notes) {
      if (notes[i].notable_type === 'Crm::Case')
        output.push(notes[i]);
    }
    return output;
  };
})
.filter('ceil', function () {
  return Math.ceil;
})
.filter('titleCase', function() {
  return function(input) {
    input = input || '';
    return input.replace(/\w\S*/g,
      function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  };
})
// Format a phone object as a string, e.g. 1 (800)789-1234 ext 432
.filter("drFormatPhone", function ($filter) {
  return function (phoneObj) {
    if (!phoneObj  || !phoneObj.value){
      return null;
    }
    var stringBuilder=$filter('drFormatPhoneNum')(phoneObj.value);
    if (phoneObj.ext && phoneObj.ext.length)
      stringBuilder += ' ext '+phoneObj.ext;
    return stringBuilder && stringBuilder.trim();
  }
})
//Format a phone number as a string. e.g. (800)789-1234
.filter("drFormatPhoneNum", function () {
  return function (phoneNum) {
    var matches = /(\d{3})(\d{3})(\d{4})/.exec(phoneNum);
    if (matches == null)
      return null;
    var stringBuilder = '('+matches[1]+')'+matches[2]+'-'+matches[3];
    return stringBuilder;
  }
})
//Format an address as a single line string.
.filter("drFormatAddress", ['enumValueFilter', function(enumValueFilter){
  return function(addrObj,multiLine){
    if(addrObj == null){ return null; }
    var lineBreakSymbol=multiLine ? "\n" : '<span style="opacity:0.5">&#8627;</span> ',
        addrString='';
    if(addrObj.street  ){ addrString+=addrObj.street.replace(/[\u0250-\ue007<>]/g,'').replace(/\n/m,lineBreakSymbol)+lineBreakSymbol; }
    if(addrObj.city    ){ addrString+=addrObj.city.replace(/[\u0250-\ue007<>]/g,'')+', '; }
    if(addrObj.state_id){ addrString+=enumValueFilter(addrObj.state_id,'STATES','abbrev'); }
    if(addrObj.zip     ){ addrString+=addrObj.zip; }
    return addrString;
  }
}])

//1e{pad} is a number expressed using scientific notation and it means 10 to the 5th power since maximum usa
//zip code is 99970 so in your case 1e5 (aka 100000) should do it, but it is dynamic function so we can reuse it
.filter("drfixedlen", function () {
  return function (zip, pad) {
    if (!zip) return null;
    pad = typeof pad !== 'undefined' ? pad : 5;
    return(eval('1e' + pad)+''+ zip).slice(-pad);
  }
})
//format phone number and strip non-numeric characters from string
  .filter("drstripPhone", function () {
    return function (phone) {
      if (!phone) return null;
      if( (typeof phone)=='object' ){ phone=phone.value; }
      return phone.replace(/\D/g,'');
    }
  })
.filter('drRelativizeTz', function ($rootScope) {
  return function (timezoneOffset) {
    if (!timezoneOffset) return '-';
     // Pick a date when DST is NOT in effect because the backend stores tz_offset without any DST info (or even specifying the TZ itself)
    if (!$rootScope.TIMEZONE_OFFSET) $rootScope.TIMEZONE_OFFSET = new Date(1999,1,1).getTimezoneOffset() / -60;
    //returning timezonoffset to deal with timezon like 5.5,13.5
    return timezoneOffset - $rootScope.TIMEZONE_OFFSET;
  }
})
.filter("drFormatSsn", function () {
  return function (ssn) {
    if (!ssn) return null;
    var matches = ssn.toString().replace(/\D/g,'').match(/(\d{3})(\d{2})(\d{4})/);
    if (matches == null || matches.length!=4) return ssn;//if it doesn't match, just show what's there.
    var stringBuilder = matches[1]+'-'+matches[2]+'-'+matches[3];
    return stringBuilder;
  }
})
.filter("drFormatTin", function () {
  return function (tin) {
    if (!tin) return null;
    var matches = tin.toString().replace(/\D/g,'').match(/(\d{2})(\d{7})/);
    if (matches == null || matches.length!=3) return tin;//if it doesn't match, just show what's there.
    var stringBuilder = matches[1]+'-'+matches[2];
    return stringBuilder;
  }
})
.filter('drShortId', function(){
  return function (numericId){
    return (numericId||'').toString(36).toUpperCase()
  }
})
/*
  Takes a Rails errors object and returns an array of strings
*/
.filter('errorStrings', function () {
  return function (errors) {
    var output = [];
    for (var key in errors) {
      errors[key].forEach(function (value) {
        output.push(key+' '+value);
      });
    }
    return output;
  }
})
.filter('firstName', function () {
  return function (fullName) {
    if (fullName) return fullName.split(/\s+/)[0];
  }
})
.filter('lastName', function () {
  return function (fullName) {
    if (fullName) {
      var names = fullName.split(/\s+/);
      if (names.length > 1)
        return names[names.length - 1];
    }
  }
})
.filter('notesByFlag', function () {
  return function (notes, flag) {
    var output = [];
    for (var i in notes) {
      if (isNaN(i)) continue;
      var note = notes[i];
      if (!flag
      || note[flag]//The most intuitive way to use this filter is to pass the name of the flag.
      || (flag==1 && note.critical)
      || (flag==2 && note.personal)
      || (flag==3 && note.confidential))
        output.push(note);
    }
    return output;
  };
})
.filter('regex', function () {
  return function (string, pattern, replaceWith) {
    if (!string) return;
    return string.replace(new RegExp(pattern, 'g'), replaceWith||'');
  }
})


function find (array, id, field) {
  if (id)
    if (array[id - 1] && array[id - 1].id == id)
      return array[id - 1][field];
    else if
      (array[id] && array[id].id == id)
      return array[id][field];
    else
      for (var i in array)
        if (array[i].id === id)
          return array[i][field];
}
