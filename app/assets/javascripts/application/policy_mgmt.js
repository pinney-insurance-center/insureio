angular.module("PolicyMgmtApp", ['RestfulResource',  'ui.bootstrap', 'io.util', 'io.espUtil', 'io.nmbUtil', 'ioConstants'])
.factory("Policy", ['Resource', function ($resource) {
    return $resource('/crm/cases/:id.json', { id: '@id', consumer_id:'@consumer_id'});
}])
.controller("PolicyCtrl", ['$scope','$rootScope','$resource','$http','$filter','$q','$uibModal','$location', '$timeout','Policy','requestSanitizer','bulletinService','Util','EspUtil','NmbUtil','ioConstantsObject',
  function($scope, $rootScope, $resource, $http, $filter, $q, $uibModal, $location,$timeout,Policy,requestSanitizer,bulletinService,Util,EspUtil,NmbUtil,constants) {
    $scope.GLOBAL_ID=1;
    $scope.USER_ID=2;
    $scope.USERS_ID=3;
    $scope.salesStages = constants.SALES_STAGES;
    $scope.placedStageId = constants.SALES_STAGE_ID_FOR_PLACED;
    $scope.appFulfillmentStageId = constants.SALES_STAGE_ID_FOR_APP_F;

    $scope.init=function (){
      $scope.consumerId=$scope.person && $scope.person.id;
      if(!$scope.stateVars){//if not already defined in parent scope
        $scope.stateVars={};
      }
      if (!$scope.stateVars.activePolicy) $scope.stateVars.activePolicy = {};
      if (!$scope.policyFilterStr) $scope.policyFilterStr = 'Open';
      if (!$scope.activeDetailsTab) $scope.activeDetailsTab = '';
      if (!$scope.activeSubsectionTab) $scope.activeSubsectionTab = '';
      if (!$scope.stateVars.activeSalesStage) $scope.stateVars.activeSalesStage = 0;

      $scope.newPlacedPolicyTemplate={
        consumer_id:$scope.consumerId,
        sales_stage_id:$scope.placedStageId,
        is_a_preexisting_policy: true,
        bind: false,
        quoting_details_attributes:[
          {sales_stage_id:$scope.placedStageId, user_id:$scope.currentUser.id}
        ]
      };
      $scope.newPlacedPolicy=$.extend(true,{},$scope.newPlacedPolicyTemplate);
      $scope.newOpportunityTemplate={
        sales_stage_id:1,
        include_in_forecasts:false,
        user_id:$scope.currentUser.id
      };
      $scope.newOpportunity     =$.extend(true,{},$scope.newOpportunityTemplate);
      //Defining form objects as properties of this object makes them accessible from this scope.
      $scope.formHolder         ={};
      var url = $location.absUrl().split('?')[0];
      //Load only for dashboard page.
      if ( (typeof $scope.consumerMgmtVars)=='undefined' ){
       $scope.consumerMgmtVars={};
       $scope.consumerMgmtVars.consumerId=$scope.task.person_id;
       $scope.consumerMgmtVars.scope=$scope;//This gets set in `ConsumerManagementCtrl` otherwise.
      }else{
       $scope.getPolicyStatusTypes();
      }

      //At some point front end code for tasks should be decoupled enough from consumers and policies
      //that this entire function will never be run in a user view,
      //and this check will no longer be needed.
      if(($scope.person.person_type) != 'Consumer'){ return ; }

      if($scope.consumerMgmtVars.scope.policies){
        $scope.consumerMgmtVars.scope.policies.$promise.then(function(){
          //After the policies view has been opened, and policies have loaded,
          //if it is the first time loading policies since loading the client management view,
          //then open the policy specified in the url.
          if(window.policyMgmtListAlreadyLoadedOnce) return;
          window.policyMgmtListAlreadyLoadedOnce=true;
          var policyParam=location.search.match(/(policy|opportunity)_id\=([^&]*)/);
          if(policyParam && policyParam[2] && $scope.stateVars){
            $scope.setActivePolicyByTypeAndId( policyParam[1], parseInt(policyParam[2]) );
          }
        });
      }

      /* Load Cases */
      $scope.getPolicies(true)
      .$promise.then(resource => {
        /* If query string specifies a kase, open it */
        let kaseId = $location.search().case_id;
        if (!$scope.stateVars) $scope.stateVars = {};
        if (!!$scope.stateVars.activePolicy && !isNaN(kaseId)) {
          let kase = resource.data.find(k => k.id == kaseId) || {};
          $scope.setActiveSubsectionTab('Policy Details');
          $scope.setActivePolicy(kase);
        }
      });
    }

    $scope.getPolicies = function (reload,scope) {
      if(scope){ $scope=scope; }
      if (!reload && $scope.policies){ return $scope.policies; }
      if (!$scope.consumerId) $scope.consumerId = $scope.consumerMgmtVars.consumerId;
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading policy list.', id:transactionId});

      $scope.consumerMgmtVars.scope.policies=Policy.get(
        {consumer_id:$scope.consumerId},//params
        function(response){//success
          bulletinService.removeBulletinById(transactionId);
          //Update the counts in Policy tab in #consumer-tabs.
          var countsToUpdate=['opportunities_count','pending_policies_count','placed_policies_count'],
              personObj=$scope.person || $scope.task.person;
          countsToUpdate.forEach(function(countKey) {
            personObj[countKey]=response[countKey];
          });
          //create a pointer to the current status          
          for(let i in response.data){
            let p=response.data[i];
            if(p.statuses.length>0){
              p.$currentStatus=p.statuses[p.statuses.length-1];
            }
          }
        },
        function(response){//fail
          var msg="Failed to load policy list. ";
          try{msg+=response.data.msg;}catch(e){console.log(e);}
          bulletinService.add({text:msg, klass:'danger', id:transactionId});
        }
      );
      return $scope.policies;
    };

    $scope.policyListTrackingFunction=function(p){//considers policy type and id
      return $scope.policyType(p)+p.id;
    }

    $scope.getStatus=function(policy){
      var str=(policy.$currentStatus && policy.$currentStatus.status_type_name) || 'n/a'
      return str;
    }

    var path = $location.path();
    if ( !path.includes("/consumers") && !path.includes("/users") ){
      Object.defineProperty($scope, 'tasks', { // In use by Consumer.TasksCtrl and PolicyCtrl
        value: new Array(2),
        configurable: false,
      });

      Object.defineProperties($scope.tasks, {
        addCurrentTask: {
          configurable: false,
          value: function (task) {
            if (this[1] != undefined){
             this[1].push(task);
             this.formatTask(task);
            }
          }
        },
      });
    }

    $scope.replacingPolicyIsInForce=function(){
      var aP =$scope.stateVars.activePolicy,
          rId=aP && aP.replaced_by_id,
          replacementPolicy,
          today=moment(),
          isInForce=false;
      if(!rId){ return isInForce; }
      for(let i in $scope.policies.data){
        let p=$scope.policies.data[i];
        if(p.id==rId){
          replacementPolicy=p;
          break;
        }
      }
      if( replacementPolicy &&
          replacementPolicy.sales_stage_id==SALES_STAGE_ID_FOR_PLACED &&
          replacementPolicy.effective_date &&
          moment(replacementPolicy.effective_date)<=today
        ){
        isInForce=true;
      }
      return isInForce;
    }

    $scope.requoteLinkUrl=function(policy){
      var linkUrl='/quoting/quotes/new?consumer_id='+$scope.person.id,
          type=$scope.policyType(policy);
      if( type=='Opportunity' ){ return ''; }
      linkUrl+='&kase_id='+policy.id+'&quote_id='+policy.quoting_details[0].id;
      return linkUrl;
    }

    $scope.policyType=function(policy){//considers existance of key 'include_in_forecasts', and value of key 'sales stage'
      if(policy.typeStr) return policy.typeStr;

      var typeStr='';
      if('include_in_forecasts' in policy){
        typeStr='Opportunity'
      }else if(policy.sales_stage_id>=$scope.placedStageId){
        typeStr='Placed'
      }else{
        typeStr='Pending'
      }
      policy.typeStr=typeStr;
      return typeStr;
    }

    $scope.policyProductType=function(policy){
      var str           ='',
          currentDetails=$scope.currentDetails(policy),
          carrierId     =currentDetails ? currentDetails.carrier_id : policy.carrier_id,
          planName      =currentDetails ? currentDetails.plan_name  : policy.plan_name,
          productTypeId =currentDetails ? currentDetails.product_type_id : policy.product_type_id;
      if(carrierId){
        str =$filter('enumValue')(carrierId, 'CARRIERS', 'short_name');
        str+=' - '
        str+=planName||''
      }else if(productTypeId){//incomplete quotes from widgets will not have a carrier or product name, but will have a product type
        str =$filter('enumValue')(productTypeId,'PRODUCT_TYPES');
      }
      return str;
    }

    $scope.productTypeId = kase => {
      let durationId = $scope.currentDetails(kase).duration_id;
      return constants.DURATIONS[durationId] && constants.DURATIONS[durationId].duration_category_id;
    }

    $scope.statusTypeGroupName=function(item){
      return item.$groupName;
    }
    $scope.getRequirements=function(){
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading requirements.', id:transactionId});
      $http.get('/crm/cases/'+$scope.stateVars.activePolicy.id+'/requirements.json')
      .success(function(data, status, headers, config){
        $scope.requirements = data;
        bulletinService.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load requirements. '+(data.errors||''), klass:'danger', id:transactionId});
      });
      return true;
    }

    //This function gets the names and companies of agents viewable to the current user.
    //Is used to populate staff assignment fields.
    $scope.getAgents=function(search){
      if(search.length <= 2){ return; }
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading agents.', id:transactionId});
      $http.get('/users.json?full_name='+(search||'')+'&no_pagination=true&select[]=id&select[]=full_name&select[]=company')
      .success(function(data, status, headers, config){
        $scope.agents = data;
        bulletinService.removeBulletinById(transactionId);//just remove the loading message if successful.
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load agents. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    //This function gets names of agents in a specific brand.
    //Is used to populate the agent and agent of record fields.
    $scope.refreshAgentsInProfile=function(search){
      if(search.length <= 2){ return; }
      $http.get('/users.json?full_name='+search+'&brand_id='+$scope.person.brand_id+'&&select[]=id&select[]=full_name&select[]=company')
      .success(function(data, status, headers, config){
        $scope.agentsInProfile=data.concat([$scope.stateVars.activePolicy.agent]);
      });
    }

    $scope.updateAgentForPolicy=function(){
      var successFn=function(){
        $scope.stateVars.activePolicy.agent_id=$scope.stateVars.tmpAgentId;
        $scope.stateVars.activePolicy.agent_name=$scope.stateVars.tmpAgentName;
        $scope.stateVars.showAgentForm=false;
        $('.modal.in').modal('hide');
      };
      $scope.updateRecordSingleField('/crm/cases/'+$scope.stateVars.activePolicy.id+'.json','agent for policy','agent_id',$scope.stateVars.tmpAgentId,successFn);
    }
    $scope.updateAgentOfRecordForPolicy=function(){
      var successFn=function(){
        $scope.stateVars.activePolicy.agent_of_record_id=$scope.stateVars.tmpAgentOfRecordId;
        $scope.stateVars.activePolicy.agent_of_record_name=$scope.stateVars.tmpAgentOfRecordName;
        $scope.stateVars.showAgentOfRecordForm=false;
        $('.modal.in').modal('hide');
      };
      $scope.updateRecordSingleField('/crm/cases/'+$scope.stateVars.activePolicy.id+'.json','agent of record for policy','agent_of_record_id',$scope.stateVars.tmpAgentOfRecordId,successFn);
    }
    $scope.loadInvolvedParties=function(){
      var transactionId = bulletinService.randomNonZeroId();
      if (!$scope.stateVars.activePolicy || !$scope.stateVars.activePolicy.id) return;
      bulletinService.add({text:'Loading involved parties.', id:transactionId});
      $scope.involvedParties=$resource('/crm/cases/'+$scope.stateVars.activePolicy.id+'/involved_parties.json').get({},
        function(data, status, headers, config){//success
          $rootScope.convertInputs(data);
          data.stakeholder_relationships_attributes=$scope.formatStakeholders(data.stakeholder_relationships);
          delete data.stakeholder_relationships;
          $.extend(true, $scope.stateVars.activePolicy, data );

          $scope.checkOutstandingReqsForAppFulfillment();
          bulletinService.removeBulletinById(transactionId);
        },
        function(data, status, headers, config){//error
          bulletinService.add({text:'Failed to load involved parties. '+(data.errors||''), klass:'danger', id:transactionId});
        }
      );
      return $scope.involvedParties;
    }
    $scope.formatStakeholders = function (stakeholderRelationshipsList) {
      var formatter=function(b){
        b.stakeholder_attributes=b.stakeholder;
        delete b.stakeholder;
        $rootScope.setPointersToPrimaryContactMethods(b.stakeholder_attributes);
        $.each(['emails','phones','addresses'],function(cIndex,contact_method){
          //Convert keys like 'phones' to keys like 'phones_attributes'
          //so that submitting the beneficiary forms works smoothly with rails.
          b.stakeholder_attributes[contact_method+'_attributes']=b.stakeholder_attributes[contact_method]||[];
          delete b.stakeholder_attributes[contact_method];
        });
      }

      $.each(stakeholderRelationshipsList||[], function(index, s){
        formatter(s);
      });
      return stakeholderRelationshipsList;
    }

    $scope.newStakeholderRelationshipTemplate={
      contingent:false,
      stakeholder_attributes:{
        emails_attributes:[],phones_attributes:[],addresses_attributes:[],entity_type_id:1
      }
    };

    //Applies to new placed policy form.
    $scope.addStakeholder=function(policy) {
      policy.stakeholder_relationships_attributes=[];
      var sRAs=policy.stakeholder_relationships_attributes,
          brandId=($scope.person.brand_id||$scope.currentUser.default_brand_id),
          agentId=$scope.currentUser.id,
          newstakeholderRelationship=$.extend(true,{},$scope.newStakeholderRelationshipTemplate);

      newstakeholderRelationship.stakeholder_attributes.brand_id=brandId;
      newstakeholderRelationship.stakeholder_attributes.agent_id=agentId;

      sRAs.push(newstakeholderRelationship);
    }
    $scope.initSubsection=function(){
      switch($scope.activeSubsectionTab){
        case 'Policy Details':
          if (!$scope.stateVars.activePolicy.id) $scope.stateVars.activePolicy.id = $location.search().case_id;
          $scope.loadInvolvedParties();
          $scope.getStaffAssignment($scope.stateVars.activePolicy);
          break;
        case 'Reqs and Notes':
          $scope.getRequirements();
          break;
      }
    }

    $scope.openAttachments=function(){
      $scope.consumerMgmtVars.activeTab='Documents';
    }

    $scope.roleFieldForRoleName=function(str){//needed because AngularJS doesn't like regular expressions in directives.
      return str.toLowerCase().replace(/\s/g,'_')+'_id';
    }

    $scope.sendConsumerResumeAppLink=function(){
      if( !$scope.currentUser.can('motorists_rtt_process_details') || !$scope.stateVars.activePolicy.scor_guid){
        return;
      }
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Sending resume app link to consumer.', id:transactionId});
      $http.put('/rtt/'+$scope.stateVars.activePolicy.scor_guid+'/deliver_resume_app_email.json')
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully sent resume app link to consumer.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to send resume app link to consumer. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.forceScorUnderwriting=function(){
      if( !$scope.currentUser.can('motorists_rtt_process_details') || $scope.stateVars.activePolicy.date_processing_started_with_scor ){
        return;
      }
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Sending to SCOR for underwriting.', id:transactionId});
      $http.put('/rtt/'+$scope.stateVars.activePolicy.scor_guid+'/force_send_for_underwriting.json')
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully sent to SCOR for underwriting.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to send to SCOR for underwriting. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.forceRttXmlTransmission=function(){
      if( !$scope.currentUser.can('motorists_rtt_process_details') ){
        return;
      }
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Sending to Encova for record keeping.', id:transactionId});
      $http.put('/rtt/'+$scope.stateVars.activePolicy.scor_guid+'/force_transfer_xml.json')
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully sent to Encova for record keeping.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to send to Encova for record keeping.. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.openPastStatusesModal=function(){
      $uibModal.open({
        templateUrl: '/crm/cases/modal_for_past_statuses.html',
        controller : 'pastStatusesModalCtrl',
        scope      : $scope
      });
    }

    $scope.openReqNoteDetailModal=function(note){
      $uibModal.open({
        resolve: { note: function () { return note } },
        templateUrl: 'req_note_detail_modal.html',//defined within template `subsection_for_reqs_and_notes.html`
        controller : 'reqNoteDetailModalCtrl',
        scope      : $scope
      });
    }

    $scope.openNewRequirementModal=function(note){
      $uibModal.open({
        templateUrl: '/crm/cases/modal_for_new_req.html',
        controller : 'newRequirementModalCtrl',
        scope      : $scope
      });
    }

    $scope.openStaffAssignmentModal=function(){
      $uibModal.open({
        resolve: { tasks: function () { return $scope.tasks } },
        templateUrl: '/crm/cases/modal_for_staff_assignment.html',
        controller : 'staffAssignmentCtrl',
        scope      : $scope
      });
    }
    $scope.openSpecialHandlingModal=function(){
      $uibModal.open({
        templateUrl: '/crm/cases/modal_for_special_handling.html',
        controller : 'specialHandlingModalCtrl',
        scope      : $scope
      });
    }
    $scope.openstakeholderRelationshipModal=function(stakeholderRelationshipObj){
      $uibModal.open({
        resolve:  { stakeholderRelationship:function () {
                      return stakeholderRelationshipObj;
                    }
                  },
        templateUrl: '/crm/cases/modal_for_stakeholder.html',
        controller : 'stakeholderRelationshipModalCtrl',
        scope      : $scope
      });
    }
    $scope.openAddPolicyModal=function(selectedOption,hideRecordTypeOptions){
      $uibModal.open({
        resolve    : {
                      recordTypeToAdd:      function(){return selectedOption},
                      hideRecordTypeOptions:function(){return hideRecordTypeOptions}
                     },
        templateUrl: '/crm/cases/modal_for_add_policy.html',
        controller : 'addPolicyModalCtrl',
        scope      : $scope
      });
    }
    $scope.openPlacedPolicyInfoModal=function(){
      $uibModal.open({
        templateUrl: '/crm/cases/modal_for_placed_policy_info.html',
        controller : 'placedPolicyInfoModalCtrl',
        scope      : $scope
      });
    }

    $scope.openEditQuotingDetailsModal=function(){
      var qD=$scope.detailsForStage($scope.stateVars.activeSalesStage)[0];
      $uibModal.open({
        resolve    : {details: function(){return qD} },
        templateUrl: 'modal_for_edit_quoting_details.html',//defined inline within subsection_details.html.
        controller : 'editQuotingDetailsModalCtrl',
        scope      : $scope
      });
    }

    $scope.openEditPlacedPolicyModal=function(policy){
      $uibModal.open({
        resolve    : {policy: function(){return policy} },
        templateUrl: '/crm/cases/modal_for_edit_placed_policy.html',
        controller : 'editPlacedPolicyModalCtrl',
        scope      : $scope
      });
    }

    $scope.canGetEspPdf= () => {
      let aP=$scope.stateVars.activePolicy,
          qD=aP.quoting_details[aP.quoting_details.length-1],
          idForWholeLifeDurationOption=17;
      return (
        aP.ext.nmb_pdf_saved ||
        (qD.duration_id==idForWholeLifeDurationOption && !$scope.checkOutstandingReqsForAppFulfillment() )
      );
    }

    $scope.openEspPdf = (...args) => { EspUtil.downloadDocument($scope.stateVars.activePolicy, 'pdf', ...args) }
    $scope.openEspXml = () => { EspUtil.openDocument($scope.stateVars.activePolicy, 'xml') }

    $scope.openExamModal=function(data){
      if(data && data.object && data.object.exam_company){ $scope.stateVars.activePolicy.exam_company=data.object.exam_company; }
      $uibModal.open({
        templateUrl: '/crm/cases/modal_for_exam.html',
        controller : 'examModalCtrl',
        scope      : $scope
      });
    }

    $scope.sendKaseToEsp = () => {
      if ($scope.checkOutstandingReqsForAppFulfillment()) {
        let reqs = $scope.stateVars.outstandingReqsForAppFulfillment.join(', ');
        bulletinService.add({klass: 'danger', ttl: 64000, text: `Case not transmitted. Outstanding requirements: ${reqs}`});
      }
      else if (1 != $scope.productTypeId($scope.stateVars.activePolicy))
        bulletinService.add({klass: 'danger', text: `Transmission to ESP is supported only for Term Life products`});
      else
        EspUtil.sendKase($scope.stateVars.activePolicy);
    }

    $scope.openScorUnderwritingResponseModal=function(data){
      $uibModal.open({
        templateUrl: 'modal_for_scor_underwriting_response.html',//template is within policy.html
        controller : 'scorUnderwritingResponseModalCtrl',
        scope      : $scope
      });
    }

    $scope.setActivePolicyByTypeAndId=function(type,id){
      var policy={};
      $.each($scope.policies.data,function(index,p){
        var isPolicy=!('include_in_forecasts' in p);
        if( p.id==id && ( (isPolicy && type=='policy') || (!isPolicy && type=='opportunity') ) ){
          policy=p;
        }
      });
      $scope.setActivePolicy(policy);
    }

    $scope.setActivePolicy=function(policy){
      var policyWasAlreadyActive= $scope.stateVars.activePolicy==policy;
      $scope.stateVars.activePolicy=( policyWasAlreadyActive ? {} : policy );//set global var for easy reference
      Util.updateUrlParams({case_id: policy.id});
      if( $.isEmptyObject($scope.stateVars.activePolicy) ){ return; }
      $scope.checkOutstandingReqsForAppFulfillment();
      if( !('include_in_forecasts' in $scope.stateVars.activePolicy) ){
        //An opportunity will not have quoting details associations and no subsections to show.
        $scope.openCurrentDetails();
        $scope.openSubsection();
        $scope.initSubsection();
      }
    }

    //This function is necessary because legacy policies will not necessarily have a
    //sales_stage_id that matches the sales_stage_id of their current details record.
    $scope.openCurrentDetails=function(){
      var currentSalesStage=0;
      $.each($scope.stateVars.activePolicy.quoting_details,function(index,qD){
        if(qD.sales_stage_id>currentSalesStage) currentSalesStage=qD.sales_stage_id;
      });
      $scope.setActiveSalesStage(currentSalesStage);
    }
    $scope.currentDetails=function(policy){
      var currentDetails = {};
      if(policy.typeStr=='Opportunity'){
        currentDetails=policy;
      } else if (policy.quoting_details && policy.quoting_details.length) {
        currentDetails = policy.quoting_details.reduce((qdA, qdB) => {
          if (qdB.sales_stage_id < qdA.sales_stage_id)
            return qdA;
          if (qdB.sales_stage_id > qdA.sales_stage_id)
            return qdB;
          return (qdA.updated_at > qdB.updated_at) ? qdA : qdB;
        }, { sales_stage_id: 0, updated_at: new Date(null) });
      }
      return currentDetails;
    }
    $scope.healthClass=function(policy){
      return  $scope.currentDetails(policy).carrier_health_class ||
              Util.enum('HEALTH_CLASSES', $scope.currentDetails(policy).health_class_id, 'name');
    }
    $scope.setActiveSalesStage=function(x){ $scope.stateVars.activeSalesStage=parseInt(x);}
    $scope.openSubsection=function(){
      if($scope.activeSubsectionTab){ return; }
      $scope.setActiveSubsectionTab( 'Policy Details' );
    }
    $scope.setActiveSubsectionTab=function(tabText){
      $scope.activeSubsectionTab=tabText;
      $scope.initSubsection();
    }
    $scope.setPolicyFilterStr=function(str){
      $scope.policyFilterStr=str;
    }
    $scope.activeDetailsTabClass=function(stageId){
      return $scope.activeDetailsTab==stageId ? 'active' : '';
    }
    $scope.activeDetailsTabAnchorClass=function(stageId){
      var detailsForStage=$scope.detailsForStage(stageId);
      return detailsForStage.length ? '' : 'j-disabled';
    }
    $scope.stageHasDetails=function(salesStageObj){
      return $scope.detailsForStage(salesStageObj.id).length>0;
    }
    $scope.detailsForStage=function(stageId){
      var quotes = ($scope.stateVars.activePolicy.quoting_details || [])
      .sort(function (a, b) { return new Date(b.updated_at) - new Date(a.updated_at) });
      var qD=$.grep(quotes, function (quote) { return quote.sales_stage_id == stageId });
      return qD;
    }
    $scope.activeSalesStageTabClass=function(stageId){
      return $scope.stateVars.activeSalesStage==stageId ? 'active' : '';
    }
    $scope.activeSubsectionTabClass=function(tabText){
      return $scope.activeSubsectionTab==tabText ? 'active' : '';
    }
    $scope.currentStatusTypeIsInStatusTypes=function(){
      if(!$rootScope.policyStatusTypes || !$rootScope.policyStatusTypes.length || !$scope.stateVars.activePolicy.status){ return false; }
      var returnVal=false;
      $.each($rootScope.policyStatusTypes,function(index,sT){
        if(sT.id==$scope.stateVars.activePolicy.status.status_type_id){
          returnVal=true;
          return false;//equivalent to break
        }
      });
      return returnVal;
    }
    $scope.ixnId = () => {
      let quote = $scope.stateVars.activePolicy.quoting_details
      .sort((a,b) => ((b.sales_stage_id||0) - (a.sales_stage_id||0)))
      .find(quote => (quote.ext && quote.ext.ixn_guid));
      return quote && quote.ext.ixn_guid;
    }

    $scope.yearsKnown=function(){
      var startDate=($scope.person || {}).relationship_to_agent_start,
          years;
      if(!startDate ){ return '-';}

      years=moment().diff(startDate, 'years');

      if( (typeof $scope.person.years_of_agent_relationship)=='undefined' ){
        $scope.person.years_of_agent_relationship=years;
      }
      return years.toString();
    }

    $scope.updateYearsKnown=function(){
      $scope.person.relationship_to_agent_start=moment().subtract($scope.person.years_of_agent_relationship, 'years').format('MM/DD/YYYY');
    }
    $scope.quotedByName=function(qD){
      var text='';
      if(!qD.user_id){text="The Consumer"}
      else if(qD.user_id==$scope.currentUser.id){text="You"}
      else{text="Another Agent"}
      return text;
    }
    $scope.getDaysSince=function(dateStr){
      var today  =moment(),
          thatDay=moment(dateStr);
      return today.diff(thatDay,'days');
    }
    $scope.riderNames=function(){
      var names      =[],
          aP         =$scope.stateVars.activePolicy,
          riderFields=['child','waiver_of_premium','disability_income','critical_or_chronic_illness','ltc','ad_and_d'],
          humanize   =function(str){return str.replace('ad_and_d','AD&D').replace('disability_income','DI').replace(/_/g,' ').replace(' or ','/');};

      $.each(riderFields,function(index,r){
        if(aP['has_rider_'+r]){
          var str='';
          str=humanize(r).replace('ltc','LTC').replace('ad ','AD ');
          if(r=='child') str+=' ('+aP['has_rider_child']+')';//add the number of child riders
          if(aP['rider_'+r+'_amount']) str+=' ($'+aP['rider_'+r+'_amount'].toLocaleString()+')';//add amount
          names.push(str);
        }
      });
      return names.join(', ')||'n/a';
    }
    $scope.isLifeAndLacksPB=function(){
      var aP=$scope.stateVars.activePolicy,
          sR=aP.stakeholder_relationships_attributes,
          prodTypeIsBlank=!aP.product_type_id,
          lacksPB=true;
      let prodTypeIsLife = !!$scope.PRODUCT_TYPES.find(o => {
        return aP.product_type_id == o.id && 1 == o.product_cat_id
      });

      if(!prodTypeIsBlank && !prodTypeIsLife){ return false; }

      for(var i in sR){
        if(sR[i].is_beneficiary && !sR[i].contingent){
          lacksPB=false;
          break;
        }
      }
      return lacksPB;
    }

    $scope.ownerCount=function(){
      var aP=$scope.stateVars.activePolicy,
          sR=aP.stakeholder_relationships_attributes,
          count=0;
      for(var i in sR){
        if(sR[i].is_owner){
          count+=1;
        }
      }
      return count;
    }
    $scope.payerCount=function(){
      var aP=$scope.stateVars.activePolicy,
          sR=aP.stakeholder_relationships_attributes,
          count=0;
      for(var i in sR){
        if(sR[i].is_payer){
          count+=1;
        }
      }
      return count;
    }

    //Called by `$scope.checkOutstandingReqsForAppFulfillment`.
    $scope.beneficiaries=function(onlyContingent){
      var aP=$scope.stateVars.activePolicy,
          sR=aP.stakeholder_relationships_attributes||[],
          list=[];
      for(var i in sR){
        if( sR[i].is_beneficiary && (!onlyContingent || sR[i].contingent) ){
          list.push(sR[i]);
        }
      }
      return list;
    }

    $scope.benPercentageTotal=function(contingent){
      var aP=$scope.stateVars.activePolicy,
          sR=aP.stakeholder_relationships_attributes,
          total=0;
      for(var i in sR){
        if(sR[i].is_beneficiary && (!!sR[i].contingent)==(!!contingent) ){
          total+=sR[i].percentage;
        }
      }
      return total;
    }


    $scope.$on('triggerCheckOutstandingReqsForAppFulfillment', function(event) {
      if($scope.stateVars.activePolicy && $scope.involvedParties && $scope.involvedParties.$resolved){
        $scope.checkOutstandingReqsForAppFulfillment();
      }
    });

    //Returns true/false.
    //Sets key `stateVars.outstandingReqsForAppFulfillment`.
    //Should be called from the success callback within `$scope.loadInvolvedParties`.
    $scope.checkOutstandingReqsForAppFulfillment = function (bulletinOnFail) {
      var p=$scope.person,
          a=(p.$primaryAddress||{}),
          f=(p.financial_info||p.financial_info_attributes||{}),
          aP=$scope.stateVars.activePolicy,
          reqs={//This list is based off of the one in `Crm::Status::REQUIREMENTS_FOR_APP_FULFILLMENT`.
            'dob':                                                 p.birth_or_trust_date,
            'full_name':                                           p.full_name,
            'ssn':                                                 p.ssn,
            'complete address':                                    (a && a.street && a.city && a.state_id && a.zip),
            'phones':                                              p.$primaryPhone,
            'emails':                                              p.$primaryEmail,
            'preferred contact method':                            p.preferred_contact_method_id,
            'birth country':                                       (p.birth_country_id || p.birth_country_custom_name),
            "drivers' license #":                                  p.dln,
            "drivers' license state":                              p.dl_state_id,
            'annual income':                                       (f.annual_income!=undefined && f.annual_income!==null),
            'net worth specified':                                 (f.net_worth_specified!=undefined && f.net_worth_specified!==null),
            'relationship to agent':                               p.relationship_to_agent,
            'years of agent relationship':                         p.relationship_to_agent_start,
            'beneficiaries':                                       $scope.beneficiaries()[0],
            'primary beneficiary percentages must total to 100':   $scope.benPercentageTotal(false)==100,
            'contingent beneficiary percentages must total to 100':($scope.beneficiaries(true).length==0) || $scope.benPercentageTotal(true)==100,
            'temporary insurance must have answer':                (aP.bind!=undefined && aP.bind!==null),
          },
          outstanding=[];
      /* Remove certain requirements for NMB cases */
      if (NmbUtil.isNMB($scope.person)) {
        delete reqs['birth country'];
        delete reqs['relationship to agent'];
        delete reqs['years of agent relationship'];
      }
      for(let humanReqName in reqs){
        let requirementMet=reqs[humanReqName];
        if(!requirementMet){
          outstanding.push(humanReqName);
        }
      }
      $scope.stateVars.outstandingReqsForAppFulfillment=outstanding;
      let fail = !!outstanding.length;
      if (fail && bulletinOnFail)
        bulletinService.add({klass: 'warning', text: `Outstanding requirements: ${outstanding.join('. ')}`});
      return fail;
    }

    $scope.updateQuoteInclusion=function(details){
      var transactionId = bulletinService.randomNonZeroId(),
          strFragment='to '+(details.include_in_forecasts ? '':'not ')+'be included in forecasts.';

      bulletinService.add({text:'Updating quote '+strFragment, id:transactionId});

      $http.put('/quoting/quotes/'+details.id+'.json',{key:'include_in_forecasts',value:details.include_in_forecasts})
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully updated quote '+strFragment, klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to update quote '+strFragment, klass:'danger', id:transactionId});
      });
    }
    $scope.updateQuotingDetails=function(details){
      var transactionId =bulletinService.randomNonZeroId(),
          salesStageName=(Util.enum('SALES_STAGES', details.sales_stage_id, 'name')||'').toLowerCase(),
          url           ='/quoting/quotes/'+details.id+'.json',
          data          ={quoting_quote: $.extend(true,{},details) },
          blackList     =['created_at','updated','typeStr','status','statuses','next_task'];

      requestSanitizer.clean(data.quoting_quote,blackList,true);

      bulletinService.add({text:'Updating '+salesStageName+' details.', id:transactionId});

      //In future, this should go through a RESTful update action on the Quoting::QuotesController.
      $http.put(url, data)
      .success(function(data, status, headers, config){
        $('.modal.in').modal('hide');
        bulletinService.add({text:'Successfully updated details.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to update details. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.updateRelationshipToMatchType=function(person){
      //Relying on these linkages is brittle, and looping through
      //relationship types doing a string comparison on the names would be even worse.
      //Since these represent almost identical information,
      //the only difference being that one is more granular than the other,
      //hopefully in future we can make them a single field.
      if(person.entity_type_id==2){//if this is a trust
        person.relationship_id=21;
      }else if(person.entity_type_id==3){//if this is a company
        person.relationship_id=8;
      }
    }
    $scope.destroyStakeholder=function(objFromForm){
      if(!confirm('Are you sure you want to permanently destroy this policy stakeholder?')) return;
      var aP           =$scope.stateVars.activePolicy,
          id           =objFromForm.id,
          personName   =objFromForm.stakeholder_attributes.full_name,
          kase         ={id:aP.id,stakeholder_relationships_attributes:[{id:id,_destroy:'1'}]},
          transactionId = bulletinService.randomNonZeroId();

      bulletinService.add({text:'Destroying stakeholder '+personName+'.', id:transactionId});

      $http.put('/crm/cases/'+aP.id+'.json',{crm_case:kase})
      .success(function(data, status, headers, config){
        var indexToRemove;
        $.each(aP.stakeholder_relationships_attributes,function(index,b){
          if(b.id==personId){indexToRemove=index; return false;}
        });
        aP.stakeholder_relationships_attributes.splice(indexToRemove,1);

        $scope.loadInvolvedParties();
        $('.modal.in').modal('hide');

        bulletinService.add({text:'Successfully destroyed stakeholder '+personName+'.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to destroy stakeholder '+personName+'. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.updateActivePolicy=function(keysToUpdate,wordsForBulletin,$uibModalInstance){
      var p   =$scope.stateVars.activePolicy,
          kase={},
          transactionId = bulletinService.randomNonZeroId();

      bulletinService.add({text:'Saving '+wordsForBulletin+'.', id:transactionId});

      $.each(keysToUpdate,function(index,key){//copy keys to kase
        kase[key]=p[key];
      });
      $http.put('/crm/cases/'+p.id+'.json',{crm_case:kase})
      .success(function(data, status, headers, config){
        $scope.loadInvolvedParties();
        $('.modal.in').modal('hide');
        $uibModalInstance.close();
        bulletinService.add({text:'Successfully saved '+wordsForBulletin+'.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to save '+wordsForBulletin+'. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    //This function arises out of the design decision to represent an integer `replaced_by_id` as a boolean (checkbox).
    $scope.addCheckboxStateToPlacedPolicies=function(){
      var ps=$scope.policyList('Placed'),
          aP=$scope.stateVars.activePolicy;
      for(var i in ps){
        ps[i].replacementCheckboxState=(ps[i].replaced_by_id && ps[i].replaced_by_id==aP.id);
      }
    }
    $scope.addReplacement=function(replaced){
      //send put request to update action for replaced

      var transactionId  = bulletinService.randomNonZeroId(),
          newReplacedById= replaced.replacementCheckboxState ? $scope.stateVars.activePolicy.id : null;

      bulletinService.add({text:'Saving replacement.', id:transactionId});

      $http.put('/crm/cases/'+replaced.id+'.json',{ crm_case:{replaced_by_id:newReplacedById} })
      .success(function(data, status, headers, config){
        replaced.replaced_by_id=newReplacedById;
        bulletinService.add({text:'Successfully saved replacement.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to save replacement. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.updatePolicyStatus=function(statusTypeId){
      var transactionId = bulletinService.randomNonZeroId(),
          policy       =$scope.stateVars.activePolicy;
          model        =('include_in_forecasts' in policy) ? 'Quoting::Quote' : 'Crm::Case';

      $('#modal-edit-status-details').modal('hide');
      bulletinService.add({text:'Updating policy status.', id:transactionId});

      $http.post('/crm/statuses.json',{statusable_id:policy.id,statusable_type:model, status_type_id: statusTypeId})
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully updated policy status.', klass:'success', id:transactionId});

        if(!policy.statuses) policy.statuses=[];
        policy.statuses.push(data);
        policy.status=data;

        $scope.getPolicies(true);
        $scope.refreshCurrentTasks();//defined in ConsumerManagementCtrl
      })
      .error(function(data, status, headers, config){
        policy.status_type_id=policy.status ? policy.status.status_type_id : null;
        var errorMsg='Failed to update policy status. '+(data.errors||'');
        bulletinService.add({text:errorMsg, klass:'danger', id:transactionId});
        if( errorMsg.match(/Before you can submit for processing/) ){
          alert(errorMsg);
        }
      });
    }
    $scope.displayNextTaskType=function(policy){
      var str='',
          nextTask=policy.next_task;
      if(nextTask && nextTask.task_type_id){
        str+=$scope.findEnumItem('TASK_TYPES', nextTask.task_type_id ).name;
      }
      return str;
    }
    $scope.displayNextTask=function(policy){
      var str='';
      if(policy.next_task){
        var nextTask=policy.next_task;
        str+=nextTask.label+' ';
        str+=$filter('formatOptionalDateAsUS')(nextTask.due_at);
      }else{str='n/a'}
      return str;
    }
    $scope.canProcessWith=function(service){
      var aP          =$scope.stateVars.activePolicy,
          isEligible  =aP['eligible_for_processing_with_'+service],
          wasStarted  =aP['date_processing_started_with_'+service],
          isInProgress=aP['$'+service+'InProgress'];
      return isEligible && !wasStarted && !isInProgress;
    }
    $scope.processingBtnTitle=function(service){
      var aP          =$scope.stateVars.activePolicy,
          isEligible  =aP['eligible_for_processing_with_'+service],
          reasons     =aP['reasons_for_ineligibility_with_'+service],
          wasStarted  =aP['date_processing_started_with_'+service],
          isInProgress=aP['$'+service+'InProgress'],
          str         ='';
      if(!isEligible){
        str+=reasons;
      }else if(wasStarted){
        str+='Processing was already started on '+wasStarted+'.';
      }else if(isInProgress){
        str+='Submission is in progress. Please wait.';
      }else{
        str+='Click to submit'
      }
      return str;
    }
    $scope.updateProcessingTimeStamp=function(serviceName){
      $scope.stateVars.activePolicy['date_processing_started_with_'+serviceName]=moment().format('MM/DD/YYYY hh:mm a');
    }
    $scope.updateAgencyWorksTimeStamp=function(){$scope.updateProcessingTimeStamp('agency_works');}

    $scope.submitForProcessing=function(serviceName){
      var transactionId   =bulletinService.randomNonZeroId(),
          serviceNameHuman=serviceName.replace(/_/g,' '),
          httpMethod      ='post',
          aP              =$scope.stateVars.activePolicy,
          pId             =aP.id,
          url             ='/processing/'+pId+'/submit_to_'+serviceName+'.json',
          successCb       =null;

      //Will disable button while in progress to prevent redundant submissions.
      aP['$'+serviceName+'InProgress']=true;

      switch(serviceName) {
        case 'agency_works':
          successCb=function(data){
            $scope.stateVars.activePolicy.agency_works_id=data.agency_works_id;
          }
          break;
        case 'docusign':
          successCb=function(data){
            $scope.stateVars.activePolicy.docusign_envelope_id=data.envelope_id;
          }
          break;
        case 'igo':
          successCb=function(data){
            window.igoWindow=window.open('about:blank', 'iGo Submission');
            window.igoWindow.document.write(data.markup);
            $('#igo-hidden-form',igoWindow.document).submit();
          }
          break;
        case 'app_assist':
          //Does not send case data. Just obscures our login to AppAssist.
          successCb=function(data){
            window.appAssistWindow=window.open('about:blank', 'AppAssist Submission');
            window.appAssistWindow.document.write(data.markup);
          }
          break;
        case 'marketech':
          successCb=function(data){
            $.extend($scope.stateVars.activePolicy,data.object);
            var url=$scope.stateVars.activePolicy.marketech_edit_url;
            window.marketechWindow=window.open(url, 'Edit Marketech Policy');
          }
          break;
        default:
          break;
      }
      bulletinService.add({text:'Submitting policy to '+serviceNameHuman+'.', klass:'success', id:transactionId});

      $http.post(url)
      .success(function(data, status, headers, config){
        if(typeof(successCb)=='function'){ successCb(data); }
        aP['$'+serviceName+'InProgress']=false;
        $scope.updateProcessingTimeStamp(serviceName);
        bulletinService.add({text:'Successfully submitted policy to '+serviceNameHuman+'.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        aP['$'+serviceName+'InProgress']=false;
        bulletinService.add({text:'Failed to submit policy to '+serviceNameHuman+'.'+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    //Initialize the policy section only after the current user and
    //the relevant person are loaded.
    $scope.getCurrentUser().$promise.then(function(){
      if($scope.person){
       $scope.person.$promise.then(function(){
         if($scope.person.id){
           $scope.init();
         }else{
           console.warn('Cannot prepare the policies section without a consumer.\nWill retry in 2s.');
           $timeout($scope.init, 2000);
         }
       });
      } 
    });
  }
])
.controller('pastStatusesModalCtrl', function ($scope, $http, $uibModalInstance) {
  //This is the only modal in the Policies GUI that is for reference only, and does not facilitate editing.
  $scope.policy=$scope.stateVars.activePolicy;

  $scope.statusChangedByName=function(s){
    var text='';
    if(!s.created_by_id){text="n/a"}
    else if(s.created_by_id==$scope.currentUser.id){text="You"}
    else{text="Another Agent"}
    return text;
  }

})
.controller('newRequirementModalCtrl', function ($scope, $http, $resource, $q, $uibModalInstance, requestSanitizer, bulletinService) {
  $scope.RequirementModel=$resource('/crm/requirements/:id.json',{id:'@id'}, {//options
      update: {
        method: 'put', isArray: false,
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},data);
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          dataCopy=JSON.stringify({requirement: dataCopy});
          return dataCopy;
        }
      },
      create: {
        method: 'post',
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},data);
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          dataCopy=JSON.stringify({requirement: dataCopy});
          return dataCopy;
        }
      }
    }
  );
  $scope.newRequirement=new $scope.RequirementModel({
    status_id:1,
    case_id: $scope.stateVars.activePolicy.id
  });

  $scope.save=function(){
    var transactionId   =bulletinService.randomNonZeroId();
    bulletinService.add({text:'Saving requirement.', id:transactionId});

    $scope.newRequirement.$create(
      function(data){//success
        bulletinService.add({text:'Successfully saved requirement.', id:transactionId});
        $uibModalInstance.close();
        $scope.requirements.push($scope.newRequirement);
      },
      function(item, response){//error
          var msg='';
          try{msg=item.data.msg;}catch(e){}
        bulletinService.add({klass:'danger', text:'Failed to save requirement. "'+msg});
      }
    );
  }

})
.controller('reqNoteDetailModalCtrl', function ($scope, $http, $uibModalInstance, note) {
  $scope.note=note;
})
.controller('staffAssignmentCtrl', function ($scope, $http, $uibModalInstance, bulletinService) {
  $scope.staffAssignmentFormObjTemplate={owner_id:$scope.stateVars.activePolicy.id,owner_type:'Crm::Case'};
  $scope.init=function(){
    $scope.formHolder.staffAssignmentForm={}
    $scope.setStaffAssignmentFormObj();
  }

  $scope.setStaffAssignmentFormObj=function(){
    if( typeof($scope.formHolder.staffAssignmentForm.$setPristine) =='function' ){
      $scope.formHolder.staffAssignmentForm.$setPristine();
    }
    if($scope.stateVars.activePolicyStaffAssignment){
      $scope.sA=$.extend(true,{},$scope.stateVars.activePolicyStaffAssignment);
    }else{
      $scope.sA=$.extend(true,{},$scope.staffAssignmentFormObjTemplate);
    }
  }

  $scope.saveStaffAssignment = () => {
    const transactionId = bulletinService.randomNonZeroId();
    const staff = $scope.sA;
    const copy = Object.keys(staff).reduce((out, key) => {
      /* `null` instead of `undefined` allows user to clear a role using the form */
      if (!/_name$/.test(key)) out[key] = staff[key] || null;
      return out;
    }, {});
    const httpMethod = staff.id ? 'put' : 'post';
    const url = staff.id ? `/usage/staff_assignments/${staff.id}.json` : '/usage/staff_assignments.json';
    bulletinService.add({text:'Saving staff assignment.', id:transactionId});
    $http[httpMethod](url, {usage_staff_assignment:copy})
    .success(function(data, status, headers, config){
      $scope.stateVars.activePolicyStaffAssignment = staff;
      $uibModalInstance.close();
      bulletinService.add({text:'Successfully saved staff assignment.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to save staff assignment. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.init();
})
.controller('specialHandlingModalCtrl', function ($scope, $http, $uibModalInstance) {
  $scope.policy=$scope.stateVars.activePolicy;

  $scope.saveSpecialHandling=function(){
    var whiteListedAttrs=[
          'bind',
          'business_insurance',
          'collateral_assignment',
          'has_rider_child',
          'has_rider_waiver_of_premium',
          'has_rider_disability_income',
          'has_rider_critical_or_chronic_illness',
          'has_rider_ltc',
          'has_rider_ad_and_d',
          'agent_id',
          'autoloan_premium',
          'dividend_type_id',
        ];
    $scope.updateActivePolicy(whiteListedAttrs,'policy special handling',$uibModalInstance);
  }
})
.controller('stakeholderRelationshipModalCtrl', function ($scope, $http, $uibModalInstance, stakeholderRelationship) {
  $scope.$uibModalInstance=$uibModalInstance;
  $scope.stakeholderRelationship=stakeholderRelationship;
})
.controller('stakeholderRelationshipFormCtrl', function ($scope, $http, requestSanitizer, bulletinService) {
  $scope.init=function(){
    $scope.origStakeholderRelationship=($scope.stakeholderRelationship || $scope.newStakeholderRelationshipTemplate);
    $scope.primaryInsured=$scope.person;//preserve this value, before overriding it.
    $scope.stateVars.addHow=($scope.origStakeholderRelationship.id) ? 'existing' : 'new';
    $scope.setSDFormObj();
    if($scope.primaryInsured.id){
      //Since this controller is used in the placed policy form and quote path, 
      //the PI may not always be persisted.
      $scope.getExistingRelationsToPI();
    }
  }

  $scope.searchConsumers=function(searchString){
    if(searchString.length < 3){ return; }//min length prevents overly broad queries.
    var paramsObj = {
          search_by: 'name_or_id',
          search_term: searchString,
          'model[]':['Consumer'],
          'select[]':['full_name', 'person_id', 'primary_email_id', 'primary_phone_id']
        };
    var transactionId = bulletinService.randomNonZeroId();
    bulletinService.add({text:'Searching consumers.', id:transactionId});
    $http.get('/contacts.json',{params:paramsObj})
    .success(function(data, status, headers, config){
      $scope.consumersForSearch = data;
      bulletinService.removeBulletinById(transactionId);//just remove the loading message if successful.
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to search consumers. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.getExistingRelationsToPI = function () {
    var transactionId = bulletinService.randomNonZeroId();
    bulletinService.add({text:'Loading existing relations.', id:transactionId});
    $http.get('/consumers/'+$scope.primaryInsured.id+'/related.json',{params:{strict:true}})
    .success(function(data, status, headers, config){
      for(var i in data){
        data[i].$groupName='Existing Relations To PI';
        data[i].full_name=(data[i].primary_insured||data[i].stakeholder).full_name;
      }
      $scope.existingRelationsToPI = data;
      bulletinService.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to load existing relations. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.consumerGroupName=function(item){
    return item.$groupName;
  }
  $scope.setSDFormObj=function(){
    if($scope.overwriteOrigSR){
      $scope.stakeholderRelationship=$scope.origStakeholderRelationship;
    }else{
      $scope.stakeholderRelationship=$.extend(true,{},$scope.origStakeholderRelationship);
    }
    $scope.person=$scope.stakeholderRelationship.stakeholder_attributes;
    $scope.contact=$scope.person;
  }
  //Called by the menu, when a selection is made.
  $scope.setExistingConsumerAsStakeholder=function(obj){
    var otherPersonId,
        relationshipTypeId,
        pIId=$scope.primaryInsured.id,
        sR=$scope.stakeholderRelationship;
    //if( Object.keys(obj).indexOf()>-1 )
    if(obj.primary_insured_id && obj.primary_insured_id==pIId){//obj is a relationship record
      otherPersonId=obj.stakeholder_id;
      relationshipTypeId=obj.relationship_type_id;
    }else if(obj.stakeholder_id && obj.stakeholder_id==pIId){//obj is a relationship record
      otherPersonId=obj.primary_insured_id;
      relationshipTypeId=null;
    }else{//obj is a consumer record
      otherPersonId=obj.id;
      relationshipTypeId=null;
    }
    sR.stakeholder_id=otherPersonId;
    sR.relationship_type_id=relationshipTypeId;
  }
  $scope.isStakeholderRelationshipValid=function(){
    $scope.attemptedSave=true;
    $scope.formErrorCt=0;
    var sR=$scope.stakeholderRelationship;

    //This validates any single fields marked required, and any fields with a pattern.
    if(!$scope.formHolder.sRForm.$valid){ $scope.formErrorCt++; }

    if(!sR.is_beneficiary && !sR.is_owner && !sR.is_payer){ $scope.formErrorCt++; }

    if($scope.stateVars.addHow=='existing' && !sR.stakeholder_id){
      $scope.formErrorCt++;
    }
    return $scope.formErrorCt==0;//valid <-> no errors
  }
  $scope.productTypeId = policy => {
    let durationId = $scope.currentDetails(policy).duration_id;
    return window.DURATIONS[durationId].duration_category_id;
  }
  $scope.createOrUpdateStakeholder=function(objFromForm){
    var aP       =$scope.stateVars.activePolicy,
        sR       =$.extend(true,{}, objFromForm),
        person   =sR.stakeholder_attributes,
        name     =person.full_name,
        brandId=($scope.primaryInsured.brand_id || $scope.currentUser.default_brand_id),
        agentId  =$scope.currentUser.id,
        kase     ={id:aP.id, stakeholder_relationships_attributes:[sR]},
        transactionId = bulletinService.randomNonZeroId();

    if($scope.stateVars.addHow=='existing'){
      delete sR.stakeholder_attributes;
    }else{
      delete sR.stakeholder_id;
      person.brand_id=brandId;
      person.agent_id=agentId;
    }

    requestSanitizer.clean(sR, null, true);

    bulletinService.add({text:'Saving stakeholder '+name+'.', id:transactionId});

    $http.put('/crm/cases/'+$scope.stateVars.activePolicy.id+'.json',{crm_case:kase})
    .success(function(data, status, headers, config){
      bulletinService.add({text:'Successfully saved stakeholder '+name+'.', klass:'success', id:transactionId});
      $scope.loadInvolvedParties();
      if($scope.$uibModalInstance) $scope.$uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to save stakeholder '+name+'. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.init();
})
.controller('addPolicyModalCtrl', function ($scope, $uibModalInstance, recordTypeToAdd, hideRecordTypeOptions) {
  $scope.modalState={
    recordTypeToAdd      : recordTypeToAdd,
    hideRecordTypeOptions: hideRecordTypeOptions
  };
  //Defining this here versus directly in the select element
  //allows this to be referenced from more than one place (like maybe the modal header).
  $scope.recordTypeOpts=[
    {id:'enter placed',name:'Manually Enter a Placed Policy'},
    {id:'enter quote', name:'Manually Add a New Opportunity (Quote)'},
    {id:'run quote',   name:'Run a Quote Via the Quote Path'}
  ];
  //This controller delegates all of its important functions to addPlacedPolicyFormCtrl and addOpportunityFormCtrl.
  $scope.$uibModalInstance=$uibModalInstance;
  //Since buttons are in a different subscope than the form they work on,
  //they need a reference to this parent scope.
  $scope.modalScope=$scope;
})
.controller('addPlacedPolicyFormCtrl', ['$scope', '$http', 'requestSanitizer', 'bulletinService', function ($scope, $http, requestSanitizer, bulletinService) {//pertains to a form within addPolicyModalCtrl

  $scope.clearNewPlacedPolicyFormObj=function(){
    $scope.modalScope.policy =$.extend(true,{},$scope.newPlacedPolicyTemplate);
    $scope.modalScope.details=$scope.modalScope.policy.quoting_details_attributes[0];
    if($scope.formHolder.newPlacedPolicyForm) $scope.formHolder.newPlacedPolicyForm.$setPristine();
  }
  $scope.clearNewPlacedPolicyFormObj();

  $scope.createPlacedPolicy=function(){
    //send post request to '/crm/cases/' with the object from the form
    //on success, clear the form, push the resulting object onto placed policies
    var transactionId = bulletinService.randomNonZeroId(),
        policyCopy=$.extend(true, {}, $scope.modalScope.policy);
    requestSanitizer.clean(policyCopy,null,true);

    bulletinService.add({text:'Saving placed policy.', id:transactionId});

    $http.post('/crm/cases.json',{crm_case:policyCopy, consumer_id:$scope.consumerId})
    .success(function(data, status, headers, config){
      $scope.person.placed_count+=1;

      $scope.clearNewPlacedPolicyFormObj();
      bulletinService.add({text:'Successfully saved placed policy.', klass:'success', id:transactionId});
      $scope.$uibModalInstance.close();
      //It is important to start this follow-up request after closing the modal,
      //otherwise the DOM gets rearranged and the modal may stay in place, confusing the user.
      $scope.getPolicies(true);
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to save placed policy. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
}])
.controller('addOpportunityFormCtrl', ['$scope', '$http', 'bulletinService', function ($scope, $http, bulletinService) {//pertains to a form within addPolicyModalCtrl

  $scope.clearNewOpportunityFormObj=function(){
    $scope.modalScope.details=$.extend(true,{},$scope.newOpportunityTemplate);
    if($scope.formHolder.newOpportunityForm) $scope.formHolder.newOpportunityForm.$setPristine();
  }
  $scope.clearNewOpportunityFormObj();

  $scope.createOpportunity=function(){
    var transactionId = bulletinService.randomNonZeroId();

    bulletinService.add({text:'Saving opportunity.', id:transactionId});

    $http.put('/consumers/'+$scope.consumerId+'.json',{consumer:{opportunities_attributes:[$scope.modalScope.details]}})
    .success(function(data, status, headers, config){
      $scope.person.opportunities_count+=1;

      $scope.getPolicies(true);

      $scope.clearNewOpportunityFormObj();

      bulletinService.add({text:'Successfully saved opportunity.', klass:'success', id:transactionId});
      $scope.$uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to save opportunity. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
}])
.controller('placedPolicyInfoModalCtrl', function ($scope, $uibModalInstance) {
  $scope.policy=$scope.stateVars.activePolicy;
  $scope.savePlacedPolicyInfo=function(){
    $scope.updateActivePolicy(['policy_number','effective_date','termination_date'],'placed policy info',$uibModalInstance);
  }
})
.controller('editQuotingDetailsModalCtrl', function ($scope, $uibModalInstance, details, $http) {
  $scope.details=details
})
.controller('editPlacedPolicyModalCtrl', function ($scope, $uibModalInstance, policy, $http, bulletinService) {
  //Preserve a reference to the original so it can be modified on success.
  $scope.origPolicy=policy;

  //Deep duplicate into a new object for modifying within the form.
  $scope.resetEditPlacedPolicyFormObj=function(){
    $scope.policy=$.extend(true, {}, $scope.origPolicy);
    $scope.policy.quoting_details_attributes=$scope.policy.quoting_details;
    delete $scope.policy.quoting_details;
    $scope.details=$scope.policy.quoting_details_attributes[$scope.policy.quoting_details_attributes.length-1]||{};
    if($scope.formHolder.editPlacedPolicyForm) $scope.formHolder.editPlacedPolicyForm.$setPristine();
  }
  $scope.resetEditPlacedPolicyFormObj();

  $scope.updatePlacedPolicy=function(){
    var transactionId = bulletinService.randomNonZeroId(),
        sanitizedPolicy=$.extend(true, {}, $scope.policy);

    delete sanitizedPolicy.next_task;
    delete sanitizedPolicy.status;
    //may still need to strip a few keys...
    bulletinService.add({text:'Updating placed policy #'+$scope.policy.id+'.', id:transactionId});

    $http.put('/crm/cases/'+$scope.policy.id+'.json',{crm_case:sanitizedPolicy})
    .success(function(data, status, headers, config){
      //Merge the returned object into the actual policy.
      $.extend(true, $scope.origPolicy, data.object);
      bulletinService.add({text:'Successfully updated placed policy #'+$scope.policy.id+'.', klass:'success', id:transactionId});
      $uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to update placed policy #'+$scope.policy.id+'. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
})
.controller('examModalCtrl', function ($scope, $uibModalInstance, $http, bulletinService) {
  $scope.policy=$scope.stateVars.activePolicy;
  $scope.examModalVars={};

  $scope.getExamOneControlCodes=function(){
    var transactionId = bulletinService.randomNonZeroId();

    bulletinService.add({text:'Loading relevant control codes.', id:transactionId});

    $http.get('/processing/'+$scope.stateVars.activePolicy.id+'/exam_one_control_codes.json')
    .success(function(data, status, headers, config){
      $scope.controlCodes=data;
      bulletinService.removeBulletinById(transactionId);//only show a message to indicate loading or failure.
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to load control codes. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  if($scope.policy.exam_company=='ExamOne'){
    $scope.examModalVars.controlCodeId=null;
    $scope.getExamOneControlCodes();
  }

  $scope.examStatusUrl=function(){
    if($scope.policy.exam_company=='SMM'){
      return 'https://www.smminsurance.com/order/'+$scope.policy.exam_num;
    }else if($scope.policy.exam_company=='ExamOne'){
      return 'https://services.examone.com/examone/orders/OrderDetails.aspx?app_id='+$scope.policy.exam_num;
    }
  }
  $scope.eOScheduledNotSubmitted=function(){
    //When a policy is scheduled, an exam_one_case_data record is created.
    //When it is submitted, the `info_sent` boolean is set to true.
    var eod=$scope.policy.exam_one_case_data;
    return eod.length && !(eod[eod.length-1]||{}).info_sent;
  }
  $scope.eOScheduleTitle=function(){
    if($scope.policy.exam_one_case_data.length){
      return 'This case has already been scheduled for ExamOne.';
    }else if($scope.examModalVars.controlCodeId){
      return '';
    }else{
      return 'Must select a control code before you can schedule.';
    }
  }
  $scope.eOSubmitTitle=function(){
    var eod=$scope.policy.exam_one_case_data;
    if( (eod[eod.length-1]||{}).info_sent ){
      return 'This case has already been submitted to ExamOne.';
    }else if(eod){
      return '';
    }else{
      return 'Must schedule before you can submit.';
    }
  }
  $scope.eOSubmitText=function(){
    var eod=$scope.policy.exam_one_case_data;
    if( (eod[eod.length-1]||{}).info_sent ){
      return 'Submitted';
    }else if(eod){
      return 'Submit';
    }else{
      return 'Ineligible';
    }
  }
  $scope.scheduleWithExamOne=function(){
    var transactionId = bulletinService.randomNonZeroId();

    bulletinService.add({text:'Opening ExamOne Scheduler.', id:transactionId});

    $http.post('/processing/'+$scope.stateVars.activePolicy.id+'/schedule_with_exam_one.json',
      {case_datum:{control_code_id:$scope.examModalVars.controlCodeId}}
    )
    .success(function(data, status, headers, config){
      $scope.stateVars.activePolicy.exam_one_case_data=data.exam_one_case_data;
      window.open(data.url,'exam_one_window');
      bulletinService.removeBulletinById(transactionId);//only show a message to indicate loading or failure.
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to open ExamOne Scheduler. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.submitToExamOne=function(){
    var transactionId = bulletinService.randomNonZeroId();

    bulletinService.add({text:'Submitting to ExamOne.', id:transactionId});

    $http.post('/processing/'+$scope.stateVars.activePolicy.id+'/submit_to_exam_one.json')
    .success(function(data, status, headers, config){
      //There should only be one case data at this point in the process, so the first is also the last.
      $scope.stateVars.activePolicy.exam_one_case_data[0].info_sent=true;
      bulletinService.add({text:'Successfully submitted to ExamOne.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to submit to ExamOne. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.scheduleAndSubmitWithSMM=function(){
    var transactionId = bulletinService.randomNonZeroId();

    bulletinService.add({text:'Submitting to SMM.', id:transactionId});

    $http.post('/processing/'+$scope.stateVars.activePolicy.id+'/submit_to_smm.json')
    .success(function(data, status, headers, config){
      $scope.stateVars.activePolicy.smm_status=data.smm_status;
      window.open(data.url,'smm_window');
      bulletinService.add({text:'Successfully submitted to SMM.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      bulletinService.add({text:'Failed to submit to SMM. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

})
.controller('scorUnderwritingResponseModalCtrl', function ($scope, $uibModalInstance, $http, bulletinService) {
      var transactionId = bulletinService.randomNonZeroId();
      $http.get('/rtt/'+$scope.stateVars.activePolicy.scor_guid+'/full_scor_underwriting_response.json')
      .success( function (data, status, headers, config){
        $scope.responseObj=JSON.stringify(data,null,'    ');
      }).error( function (data, status, headers, config){
        var failureMsg='Failed to load SCOR underwriting response.';
        $scope.responseObj=failureMsg+data.errors
      });
})
/* Format a currency and duration */
.filter('flatAmtAndYrs', ['currencyFilter', (currencyFilter) => {
  return (quote) => {
    const amt = currencyFilter(quote.flat_amt);
    return amt ? `${amt} @ ${quote.flat_yrs || '?'} yrs` : '-';
  };
}])
;