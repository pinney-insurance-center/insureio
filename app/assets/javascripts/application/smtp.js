insureioNg.controller('SMTPCtrl', ["$scope", "$http", "$uibModal", "bulletinService",
  function($scope, $http, $uibModal, bulletinService) {
  $scope.smtpSectionStateVars={};
  $scope.smtpSectionStateVars.showAdvanced=false;
  $scope.accountDefaults={host:"", address:"", port:587, tls:1, authentication:'login'};
  $scope.preconfiguredServices=[
    {image_url:'/images/office.png',  name: 'Office 365',    settings:{address: 'smtp.office365.com',    tls:2}},
    {image_url:'/images/outlook.png', name: 'Outlook',       settings:{address: 'smtp-mail.outlook.com', host: 'outlook.com', tls:2}},
    {image_url:'/images/hotmail.png', name: 'Hotmail',       settings:{address: 'smtp.live.com',         host: 'hotmail.com', port: 465}},
    {image_url:'/images/gmail.png',   name: 'Gmail',         settings:{address: 'smtp.gmail.com',        host: 'gmail.com',   port: 465, tls:2}},
    {image_url:'/images/yahoo.png',   name: 'Yahoo',         settings:{address: 'smtp.mail.yahoo.com',   host: 'yahoo.com',   port: 465, tls:2}},
    {image_url:'/images/other.png',   name: 'Custom / Other',settings:{}}
  ];

  $scope.getExistingAccounts=function(){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading existing email accounts.', id:transactionId});
    $http.get('/smtp_servers.json?id='+$scope.person.id)
    .success(function(data, status, headers, config){
      $scope.existingAccounts = data;
      $rootScope.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load existing email accounts. '+(data.errors||''), klass:'danger', id:transactionId});
    })
    .finally(function(){
      $scope.existingAccountsResolved=true;
    });
  }
  $scope.getExistingAccounts();

  $scope.openForm=function(account){
    if(!account){ account=$scope.accountDefaults; }
    $scope.smtpSectionStateVars.activeAccount=account;

    $uibModal.open({
      templateUrl: '/smtp_servers/modal_for_form.html',
      controller : 'smtpModalCtrl',
      scope      : $scope
    });
  }

}])
.controller('smtpModalCtrl', ["$scope", "$http", "$uibModalInstance", "bulletinService",
  function($scope, $http, $uibModalInstance, bulletinService) {
  $scope.account=$.extend(true,{},$scope.smtpSectionStateVars.activeAccount);
  $scope.smtpSectionStateVars.service=$scope.account.id ? $scope.preconfiguredServices[5] : null;

  $scope.prefillAccountSettings=function(){
    var service=$scope.smtpSectionStateVars.service;
    $.extend(true,$scope.account,$scope.accountDefaults, service.settings);
    if(service.name=='Custom / Other'){ $scope.smtpSectionStateVars.showAdvanced=true; }
  }

  $scope.testConnection=function(account,successFn){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Testing email account.', id:transactionId});
    $http.post('/smtp_servers/test_connection.json',{marketing_email_smtp_server:account})
    .success(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Successfully connected to email account.'+
        ' We recommended that you also send a test email from Insureio before sending any business-critical emails.', klass:'success', id:transactionId});
      if(!account) account=data.object;
      if( typeof(successFn)=='function' ) successFn();
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Could not connect to email account. Your email service returned these errors: '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.save=function(account){
    $scope.testConnection(account,function(){/*success cB. fail cB is defined in `$scope.testConnection` above.*/
      var resetView=function(){
        $scope.smtpSectionStateVars.showAdvanced=false;
      }
      if(account.id){//update
        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text:'Testing and', id:transactionId});
        $http.put('/smtp_servers/'+account.id+'.json',{marketing_email_smtp_server:account})
        .success(function(data, status, headers, config){
          $rootScope.addBulletin({text:'Successfully saved email account.'+
        ' We recommended that you also send a test email from Insureio before sending any business-critical emails.', klass:'success', id:transactionId});
          var index=null;
          for(var i=0, l=$scope.existingAccounts.length; i<l; i++){
            if($scope.existingAccounts[i].id==account.id){
              index=i; break;
            }
          }
          if(index!=null) $scope.existingAccounts[index]=account;
          resetView();
        })
        .error(function(data, status, headers, config){
          $rootScope.addBulletin({text:'Failed to save email account. '+(data.errors||''), klass:'danger', id:transactionId});
        });
      }else{//create
        $scope.createRecord({marketing_email_smtp_server:account},null,$scope.existingAccounts,'/smtp_servers.json','email account',resetView)
      }
    }); 
  }

}]);