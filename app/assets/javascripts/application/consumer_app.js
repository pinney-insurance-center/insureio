(function(){

	angular.module('dr.consumer', ['DrBulletins'])
	.controller('Consumer.FinanceCtrl', ['$scope', '$resource', function ($scope, $resource) {
		$scope.calc = new Calculator($scope);
	}])
	.controller('Consumer.TasksCtrl', ['$scope', '$resource', 'Task', 'bulletinService', function ($scope, $resource, Task, bulletinService) {
		$scope.settings = new Object;
		$scope.tasks = new Object;
		$scope.CURRENT = 1; // active = 1
		$scope.OLD = 0; // active = 0
		// $scope.tasks is defined on ConsumerManagementCtrl
		$scope.init=function(){
			if ($scope.person.$personType == 'Consumer'){
				var scope = $scope;
				while (scope && !scope.hasOwnProperty('getPolicies')) scope = scope.$parent;
				if (scope && scope.hasOwnProperty('getPolicies')) {
					$scope.$watch('policies', function (newVal, oldVal) {
						if (newVal && newVal.$promise) newVal.$promise.then(function () { applyRelatedToLabelToTasks(); });
					});
				} else{
					console.error('Cannot find parent scope which owns fn getPolicies!');
				}
			}
			$scope.showTasks($scope.CURRENT);
		}
		$scope.showTasks = function (index, forceRefresh) {
			if (!$scope.tasks[index] ||  forceRefresh)
				fetchTasks(index);
			$scope.visibleTasks = $scope.tasks[index];
		}

		//Both this scope and `Dashboard.WidgetTableCtrl` listen for and respond to this event.
		$scope.$on('refreshCurrentTasks', function(e) {
			$scope.showTasks($scope.CURRENT,true);
		});

		$scope.$on('addTaskIfRelevant', function(e,theTask) {
			$scope.addTaskIfRelevant(theTask);
		});

		$scope.addTaskIfRelevant=function(theTask){
			var personTypeForTaskAssociations=$scope.person.$personType.replace('Recruit','User'),
					relevantTaskList=$scope.tasks[theTask.active],
					olderVersionOfTheTask;
			if(theTask.person_id!=$scope.person.id || theTask.person_type!=personTypeForTaskAssociations){
				return;//task does not pertain to this person.
			}
			for(let i in relevantTaskList){
				if(relevantTaskList[i].id==theTask.id){
					olderVersionOfTheTask=relevantTaskList[i];
					break;
				}
			}
			if(olderVersionOfTheTask){
				$.extend(true,olderVersionOfTheTask,theTask);//update task
			}else{
				relevantTaskList.push(theTask);//add task
			}
		}

		$scope.saveTask = function (task) {
			var transactionId = bulletinService.randomNonZeroId();
			bulletinService.add({text:'Updating task #'+task.id+'...', id:transactionId});
			var personBaseUrl=task.person_type=='User' ? '/users/' : '/consumers/';
			return Task.update({id:task.id, crm_task:task}, function () {
				task.$editDueAt = false;
				$.get(personBaseUrl+$scope.person.id+'/marketing_history_and_to_do',{container:'#history_and_to_do'});
				bulletinService.add({klass:'success', text:'Task #'+task.id+' updated', id:transactionId});
			}, function () {
				bulletinService.add({klass:'danger', text:'Failed to update task #'+task.id, id:transactionId});
			});
		}
		$scope.completeTask = function (task) {
			task.completed_at = new Date;
			$scope.saveTask(task).$promise.then(
				function () {//on success
					var indexInStartingList;
					for(let i in $scope.tasks[$scope.CURRENT]) {
						if ($scope.tasks[$scope.CURRENT][i].id == task.id) {
							indexInStartingList = i;
							break;
						}
					}
					$scope.tasks[$scope.CURRENT].splice(indexInStartingList,1);
					if($scope.tasks[$scope.OLD]){
						$scope.tasks[$scope.OLD].splice(0,0,task);
					}else{
						$scope.person.old_tasks_count+=1;
					}
    			$scope.$broadcast('refreshMarketingSection');
				},
				function () {//on failure
					task.$done = false;
					task.completed_at=null;
				});
		}
		//User-suspended tasks can be un-suspended, but completed tasks stay completed.
		$scope.suspendOrUnsuspendTask = function (task) {
			$scope.saveTask(task).$promise.then(
				function () {//on success
					var startingList, 
					    endingList,
					    indexInStartingList;
					if(task.suspended){
						startingList=$scope.tasks[$scope.CURRENT];
						endingList  =$scope.tasks[$scope.OLD];
					}else{
						startingList=$scope.tasks[$scope.OLD];
						endingList  =$scope.tasks[$scope.CURRENT];
					}
					for(let i in startingList){
						if(startingList[i].id==task.id){
							indexInStartingList=i;
							break;
						}
					}
					startingList.splice(indexInStartingList,1);
					if(endingList){
						endingList.splice(0,0,task);
					}else if(task.suspended){
						$scope.person.old_tasks_count+=1;
					}
				},
				function () {//on failure
					//Revert to what it was before the ngChange directive.
					task.suspended=!task.suspended;
				});
		}
		$scope.updateTaskAssignee = function (newAssignee, $select, scope) {
			var task = scope.modelHolder;
			task.assigned_to_id = newAssignee.id;
			task.assigned_to_name = newAssignee.full_name;
			$scope.saveTask(task).$promise.then(data => {
				delete task.$editAssignee;
				/* A new staff assignment could have been created by the Rails app */
				if ($scope.stateVars.activePolicy) $scope.$emit('refreshStaffAssignment');
			});
		}
		function fetchTasks (active) {
			var personType=$scope.person.$personType.replace('Recruit','User');
			$scope.tasks[active] = Task
			.query({active:active, person_type: personType , person_id: $scope.person.id},
				function (data) {
					applyRelatedToLabelToTasks($scope.tasks[active]);
					// Sort by due date
					$scope.tasks[active].sort(function (a,b) {
						if (!a.due_at) return 1;
						else if (!b.due_at) return -1;
						else {
							var da = new Date(a.due_at);
							var db = new Date(b.due_at);
							var diff = da - db;
							if (isNaN(diff)) return (isNaN(da.getYear()) ? 1 : -1);
							else return diff;
						}
					});
				}
				);
		}
		Object.defineProperties($scope.tasks, {
			formatTask: {
				configurable: true,
				value: function (task) {
					formatTask(task, getMapForPolicyIdToPolicyOrder($scope.visibleTasks));
				}
			},
		});
		function getMapForPolicyIdToPolicyOrder (tasks) {
			var policyIds_o = new Object;
			if ($scope.policies && $scope.policies.data && $scope.policies.data.length) {
				var policies = $scope.policies.data;
				// Build map of policy id to policy order, based on policies in scope
				for (let i = 0; i < policies.length; i++) {
					var type = ('include_in_forecasts' in policies[i]) ? 'opp' : 'pol'; // use text that will sort opportunities above cases
					var key  = type + policies[i].id;
					policyIds_o[key] = i + 1;
				}
			} else {
				// Build map of policy id to policy order, based on tasks
				for (let i = 0; i < tasks.length; i++) {
					var klass = tasks[i].sequenceable_type;
					if (klass == "Crm::Case" || klass == "Quoting::Quote") {
						// hold id in map obj
						var type = (klass == 'Quoting::Quote') ? 'opp' : 'pol'; // use text that will sort opportunities above cases
						var key  = type + tasks[i].sequenceable_id;
						if (!policyIds_o[key]) policyIds_o[key] = true;
					} else {
						// set $relatedTo on non-Policy tasks. This is a failsafe. It shouldn't actually happen.
						if (klass && tasks[i].sequenceable_id)
							tasks[i].$relatedTo = klass + ' ID:' + tasks[i].sequenceable_id;
						else
							tasks[i].$relatedTo = tasks[i].origin_type + ' ID:' + tasks[i].origin_id;
					}
				}
				// Sort ids and set cardinality of keys in policyIds_o
				var sortedPolicyIds_a = Object.keys(policyIds_o).sort();
				for (let i = 0; i < sortedPolicyIds_a.length; i++) {
					let id = sortedPolicyIds_a[i];
					policyIds_o[id] = i + 1;
				}
			}
			return policyIds_o;
		}
		function formatTask(task, policyIds_o, selectedTaskId) {
			// set _colourClass on task, based on due date
			Task.setColourClass(task, selectedTaskId);
			// set $done on task
			if (task.completed_at) task.$done = true;
			// set $relatedTo
			if (task.sequenceable_id && task.sequenceable_type) {
				var klass = task.sequenceable_type;
				var type  = (klass == 'Quoting::Quote') ? 'opp' : 'pol';
				var key   = type + task.sequenceable_id;
				var suffix = policyIds_o[key] ? '#' + policyIds_o[key] : 'ID:' + task.sequenceable_id;
				task.$relatedTo = 'Policy ' + suffix;
			} else {
				task.$relatedTo = task.origin_type + ' ID:' + task.origin_id;
			}
		}
		$scope.relatedToLabel = function (task) {
			var label = '';
			var type = task.sequenceable_type;
			switch (type) {
				case 'Quoting::Quote':
				label = "Opportunity";
				break;
				case 'Crm::Case':
				label = "Policy";
				break;
				case 'User':
				label = "User";
				break;
				case 'Consumer':
				label = 'Campaign';
				break;
				case 'Crm::Status':
				label = 'Status';
				break;
				case 'Marketing::Subscriptions':
				label = 'Marketing';
				default:
				label = type.replace(/\:\:/g , ' ')
			}
			return label;
		}
		$scope.relatedToId = function (task) {
			return task.sequenceable_type == 'Consumer' ? task.origin_id : task.sequenceable_id;
		}
		$scope.relatedToClass = function (task) {
			var className = '';
			if  (task.sequenceable_type == 'Quoting::Quote') {
				className = 'label-info';
			} else if (task.sequenceable_type = 'Crm::Case') {
				className = 'label-success';
			}
			return className;
		}
		function applyRelatedToLabelToTasks (tasks) {
			if (!tasks) tasks = $scope.visibleTasks;
			var policyIds_o = getMapForPolicyIdToPolicyOrder(tasks);
			// Get selected task id from query string
			var selectedTaskId;
			var selectedTaskIdMatch = /(\?|&)task_id=(\d+)/.exec(window.location.search);
			if (selectedTaskIdMatch) selectedTaskId = parseInt(selectedTaskIdMatch[2]);
			// set $relatedTo on Policy tasks
			for (var i = 0; i < tasks.length; i++) {
				formatTask(tasks[i], policyIds_o, selectedTaskId);
			}
		}

		$scope.init();
	}])

	var Calculator = function (scope) { // constructor function
		this.$scope = scope;
	};
	Object.defineProperties(Calculator.prototype, {
		// Add Object to calculator.children
		addChild: {
			value: function () {
				this.children || (this.children = []);
				this.children.push(new Object);
			},
			writable: false,
			configurable: false,
		},
		calcCollegeCost: {
			value: function (child) {
				var total=0;
				if (child) {
					// calc cost for just one child
					child.age || (child.age = 18);
					var index0 = parseInt(child.collegeType);
					if (index0 == 1) index0 += parseInt(child.collegeResidence);
					if (isNaN(index0)) return 0;
					var index1 = parseInt(child.collegeFees);
					if (isNaN(index1)) return 0;
					var annualCost = this.collegeCosts[index0][index1];
					var yearsOfCollege = child.collegeType == 0 ? 2 : 4;
					var yearsTilCollege = child.age < 18 ? 18 - child.age : 0;
					for (var i = yearsTilCollege; i < yearsTilCollege + yearsOfCollege; i++) {
						total += annualCost * Math.pow(1+DEFAULT_INFLATION, i);
					}
					return total;
				} else {
					// calc total cost for all children
					if (!this.children) return 0;
					var calculator = this;
					total = this.children.reduce(function(previousVal, currentVal, index, array){
						return previousVal + calculator.calcCollegeCost(currentVal);
					}, 0);
					return Math.round(total, 0);
				}
			},
			writable: false,
			configurable: false,
		},
		calcIncome: {
			value: function () {
				this.$scope.finance.annual_income = (this.$scope.finance.asset_earned_income||0) + (this.$scope.finance.asset_unearned_income||0);
			},
			writable: false,
			configurable: false,
		},
		calcAssets: {
			value: function () {
				this.$scope.finance.assets = [
				this.$scope.finance.asset_checking,
				this.$scope.finance.asset_savings,
				this.$scope.finance.asset_pension,
				this.$scope.finance.asset_investments,
				this.$scope.finance.asset_home_equity,
				this.$scope.finance.asset_real_estate,
				this.$scope.finance.asset_life_insurance
				].reduce(function(oldVal, curVal){
					return oldVal + (curVal || 0);
				}, 0);
			},
			writable: false,
			configurable: false,
		},
		calcLiabilities: {
			value: function () {
				this.$scope.finance.liabilities = [
				this.$scope.finance.liability_final_expense,
				this.$scope.finance.liability_other,
				this.$scope.finance.liability_auto,
				this.$scope.finance.liability_credit,
				this.$scope.finance.liability_student_loans,
				this.$scope.finance.liability_personal_loans,
				this.$scope.finance.liability_mortgage_1,
				this.$scope.finance.liability_mortgage_2,
				this.$scope.finance.liability_heloc,
				this.$scope.finance.liability_education,
				].reduce(function(oldVal, curVal){
					return oldVal + (curVal || 0);
				}, 0);
			},
			writable: false,
			configurable: false,
		},
		collegeCosts: {
			value: [
			  [3131,  10550], // public 2yr
			  [8655,  17860], // public 4yr in-state
			  [21706, 30911], // public 4yr out-of-state
			  [29056, 39518]  // private 4yr
			  ],
			  writable: false,
			  configurable: false,
			},
		recommendedCoverageAmount: {
			value: function () {
				var total = 0;
				total += (this.$scope.finance.annual_income || 0) * (this.$scope.finance.liability_years_income_needed || 0);
				total -= (this.$scope.finance.assets || 0);
				total += (this.$scope.finance.liabilities || 0);
				return total;
			},
			writable: false,
			configurable: false,
		},
		// Remove Object from calculator.children
		removeChild: {
			value: function (child) {
				this.children.splice(this.children.indexOf(child), 1);
			},
			writable: false,
			configurable: false,
		},
	});
	var UPDATE_ACTION = {'update':{method:'PUT'}};

})();