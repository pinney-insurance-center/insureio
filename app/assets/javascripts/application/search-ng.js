angular.module("SearchApp", ['RestfulResource', 'ui.bootstrap','easypiechart'])
.factory("Search", ['Resource', function ($resource) {
    return $resource('/reporting/searches/:id.json', { id: '@id'});
}])
.controller("searchCtrl", ['$scope', '$compile', '$interval', '$rootScope', '$location', '$http', 'Search', 'requestSanitizer', 'bulletinService',
  function ($scope, $compile, $interval, $rootScope, $location, $http, Search, requestSanitizer, bulletinService) {
  $scope.init=function(){
    console.log('ran init block.');
    $scope.resetSearchCtrlVars();
    $scope.newSearchTemplate={
      criteria:{},
      ownership_id:2,
      marketing_visible:true,
      client_visible:true,
      case_visible:true,
      owner_visible:true,
      status_visible:true,
      fields_visible:true,
    };
    $scope.summaryChartOptions = {
        /*animate:{
            duration:0,
            enabled:false
        },*/
        barcolor:'#808080',
        scalecolor:'#c0c0c0',
        lineWidth:10,
        lineCap:'butt'
    };
    $scope.getSearches();
    $scope.STAFF_ASSIGNMENT_ROLES=[
      {text:"Any",                     value:null},
      {text:"Administrative Assistant",value:"administrative_assistant_id"},
      {text:"Case Manager",            value:"case_manager_id"},
      {text:"Manager",                 value:"manager_id"},
      {text:"Policy Specialist",       value:"policy_specialist_id"},
      {text:"Application Specialist",  value:"application_specialist_id"},
      {text:"Underwriter",             value:"underwriter_id"},
      {text:"Insurance Coordinator",   value:"insurance_coordinator_id"},
    ];
    if($rootScope.immediatelyTriggerNewForm){
      $scope.showForm($scope.newSearch(),true,true);
      $rootScope.immediatelyTriggerNewForm=false;
    }
    $scope.fieldsToShowDisplayObject=$scope.REPORTING_BY_CASE_FIELDS_TO_SHOW;
  }

  $scope.POLICY_TYPES_W_NULL_OPTION=[{id:null,name:''}].concat(PRODUCT_TYPES);
  $scope.CARRIERS_W_NULL_OPTION    =[{id:null,name:''}].concat(CARRIERS);
  $scope.SALES_STAGES_W_NULL_OPTION=[{id:null,name:'No Stage'}].concat(SALES_STAGES);
  $scope.CATEGORIES_W_NULL_OPTION  =[{id:null,name:'No Category',sales_stage_id:null,is_for_policies:true}].concat($scope.STATUS_CATEGORIES);

  //Create display names for sales stage menu.
  for(var i in $scope.SALES_STAGES_W_NULL_OPTION){
    var sSObj=$scope.SALES_STAGES_W_NULL_OPTION[i];
    sSObj.displayName='';
    sSObj.displayName+=sSObj.id || '-';
    sSObj.displayName+=' - ';
    sSObj.displayName+=sSObj.name;
  }

  $scope.resetSearchCtrlVars=function(){
    $scope.searchCtrlVars={};
    $scope.searchCtrlVars.activeSection='list';
    $scope.searchCtrlVars.activeSearch={};
    $scope.readStateFromUrl();
  }

  $scope.readStateFromUrl=function(){
    var params=$location.search(),
        filter=params.ownership_filter;

    if(filter && filter!='All'){
      $scope.searchCtrlVars.ownershipFilter=filter;
    }
  }

  $scope.setOwnershipFilter=function(str){
    if($scope.searchCtrlVars.ownershipFilter==str){ return; }
    $scope.searchCtrlVars.ownershipFilter=str;
    $location.search({ownership_filter:str});
    $scope.getSearches();
  }

  $scope.newSearch=function(){ return $.extend(true,{},$scope.newSearchTemplate); }
  $scope.duplicateSearch=function(search){
    $scope.searchCtrlVars.activeSearch=$.extend(true, {}, search);
    $scope.searchCtrlVars.activeSearch.name+=' (Copy)';
    delete $scope.searchCtrlVars.activeSearch.id;
    delete $scope.searchCtrlVars.activeSearch.owner_id;
    delete $scope.searchCtrlVars.activeSearch.created_at;
    delete $scope.searchCtrlVars.activeSearch.updated_at;
    $scope.showForm($scope.searchCtrlVars.activeSearch);
  }

  $scope.resetActiveSearch=function(){
    if($scope.searchCtrlVars.activeSearch.id){
      //find it in available searches, and clone that
      for(var i in $scope.searches){
        if($scope.searches[i].id==$scope.searchCtrlVars.activeSearch.id){
          $scope.searchCtrlVars.activeSearch=$.extend(true,{},$scope.searches[id]);
          delete $scope.searchCtrlVars.activeSearch.$$hashKey;
        }
      }
    }else{
      $scope.searchCtrlVars.activeSearch=$scope.newSearch();
    }
  }

  $scope.showResults=function(search){
    $scope.searchCtrlVars.activeSearch=search;
    $scope.searchCtrlVars.activeSection='results';
    search.$restrictedByFields=$scope.getRelevantModelFieldPairs(search.criteria);
    search.$showsFields=$scope.getRelevantModelFieldPairs(search.fields_to_show);

    var transactionId=$rootScope.randomNonZeroId(),
        searchCopy   =$.extend(true,{},$scope.searchCtrlVars.activeSearch);

    requestSanitizer.clean(searchCopy,[],true);
    $scope.compact(searchCopy.fields_to_show);

    $rootScope.addBulletin({text:'Loading report results.', id:transactionId, ttl: 10*60*1000});

    $scope.showLastReportingBulletin=false;
    $scope.showReportLoadingMsg=true;
    $scope.startedLoadingReport=moment();
    $scope.reportLoadTime=0;
    var reportLoadTimeUpdater=$interval(function(){
      $scope.reportLoadTime+=1;
    },1000);

    //Loading an erb view here. will eventually replace with an ngInclude and a static AngularJS template.
    $http.post('/reporting/searches/results.json',{reporting_search:searchCopy})
    .success(function(data, status, headers, config){
      $.extend($scope.searchCtrlVars,data);
      $rootScope.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      $scope.showLastReportingBulletin=true;
      $rootScope.addBulletin({text:'Failed to load search results. '+(data.errors||''), klass:'danger', id:transactionId});
    }).finally(function(){
      $scope.showReportLoadingMsg=false;
      $interval.cancel(reportLoadTimeUpdater);
    });
  }
  //This mapping is used by function `$scope.getMenuOptions` below.
  $scope.menuOptionLists={
    agents:    {url:'/users.json?no_pagination=true&select[]=id&select[]=full_name&select[]=company'},
    brands:  {url:'/brands.json?just_the_basics=true'},
    lead_types:{url:'/crm/lead_types.json'},
    referrers: {url:'/crm/referrers.json'},
    sources:   {url:'/crm/sources.json'},
    status_types: {url:'/crm/status_types.json'},
  };
  $scope.findMenuOptionKeyByUrl=function(url){
    var x;
    for(let key in $scope.menuOptionLists){
      if($scope.menuOptionLists[key].url==url){ x=key; }
    }
    return x;
  }
  $scope.getMenuOptions=function(){
    for(let key in $scope.menuOptionLists){
      var resourceName      =key,
          humanResourceName =key.replace(/_/g,' '),
          url               =$scope.menuOptionLists[key].url;

      $rootScope.addBulletin({text:'Loading '+humanResourceName+' list for criteria menus.', klass:'success', id:url});

      $http.get(url)
      .success(function(data, status, headers, config){
        var resourceName    =$scope.findMenuOptionKeyByUrl(config.url),
          humanResourceName =resourceName.replace(/_/g,' ');
        $scope[resourceName]=data;
        $rootScope.removeBulletinById(config.url);
      })
      .error(function(data, status, headers, config){
        var resourceName    =$scope.findMenuOptionKeyByUrl(config.url),
          humanResourceName =resourceName.replace(/_/g,' ');
        $rootScope.addBulletin({text:'Failed to load '+humanResourceName+' list for criteria menus.', klass:'danger', id:config.url});
      });
    }

  }

  $scope.searchTags = function (term) {
    if (term.length >= 3) {
      var xactionId = $rootScope.randomNonZeroId();
      return $http.get('/tags.json?q='+term)
      .success(function (data, status) {
        if (data.length == 0)
          $rootScope.addBulletin({text: 'No tags matching '+term, klass: 'warning', id: xactionId});
        $scope.tags = data;
      })
      .error(function (data, status) {
        $rootScope.addBulletin({text: 'Failed to load tags. ' + data.errors, klass: 'danger', id: xactionId});
      });
    } else {
      $scope.tags = [];
    }
  };

  //This function is used to output natural looking lists
  //of criteria fields and fields to show.
  $scope.getRelevantModelFieldPairs=function(obj){
    var outputList=[];
    for(let modelName in obj){
      if( (typeof obj[modelName])=='object' ){
        for(let fieldName in obj[modelName]){
          if( !obj[modelName][fieldName] ){ continue; }
          var displayName=modelName+' - '+fieldName.replace(/_(min|max)/,'_range').replace(/_/g,' ');
          if( outputList.indexOf(displayName)>-1 ){ continue; }
          outputList.push(displayName);
        }
      }
    }
    return outputList;
  }
  $scope.showForm=function(search){
    if($scope.searchCtrlVars.activeSearch.id==search.id && $scope.searchCtrlVars.activeSection=='form'){ return; }

    $scope.getMenuOptions();
    $scope.searchCtrlVars.activeSearch=$.extend(true,{},search);

    $scope.searchCtrlVars.activeSection='form';

    if(!$scope.searchCtrlVars.activeSearch.criteria.case){ $scope.searchCtrlVars.activeSearch.criteria.case={}; }
    $scope.initCurrentOrPrev();
  }

  $scope.destroySearch=function(search){
  if(!confirm('Are you sure you want to permanently destroy this search?')){return;}
    $scope.searchCtrlVars.activeSection='list';
    $scope.searchCtrlVars.activeSearch={};

    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Destroying search.', id:transactionId});

    $http.delete('/reporting/searches/'+search.id+'.json')
    .success(function(data, status, headers, config){
      var searchIndex=null;
      $.each($scope.searches,function(index,s){
        if(s.id==search.id){ searchIndex=index; }
      });
      if(searchIndex!=null){ $scope.searches.splice(searchIndex,1); }
      $rootScope.addBulletin({text:'Successfully destroyed search.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to destroy search.', klass:'danger', id:transactionId});
    });
  }

  $scope.getSearches=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        params={},
        filter=$scope.searchCtrlVars.ownershipFilter;
    $rootScope.addBulletin({text:'Loading available reports.', id:transactionId});

    switch(filter){
      case 'mine':
        params.owner_id=$rootScope.currentUser.id;
        break;
      case 'shared':
        params['ownership_id[]']=[3,2];//user or user and descendants
        break;
      case 'global':
        params['ownership_id[]']=[1];//global
        break;
    }

    $scope.searches=Search.query(
      params,
      function(searches){//success
        $rootScope.removeBulletinById(transactionId);
        for(var i in searches){
          searches[i].$ownerName=searches[i].owner_name;
          delete searches[i].owner_name;
          $scope.convertStringsToDigits(searches[i]);
        }
        return searches;
      },
      function(response){//fail
        $rootScope.addBulletin({text:'Failed to load available reports.', klass:'danger', id:transactionId});
      }
    );
  }

  $scope.convertStringsToDigits=function(obj){
    for(var key in obj){
      if( (typeof obj[key])=='object' ){
        $scope.convertStringsToDigits(obj[key]);
      }else if(obj[key]=='0'){
        obj[key]=0;
      }else if(obj[key]=='1'){
        obj[key]=1;
      }
    }
    return obj;
  }

  //This method is needed because AngularJS won't let us perform certain string manipulations directly within the view.
  $scope.concatStringsForMethodName=function(array){
    return array.join('').replace(/\s/g,'_').toLowerCase();
  }
  //So that when the `sales_stage_id` criterion is changed, the `first_x_min` and `first_x_max` criteria get cleared.
  //This way, if sales stage is set to "Submitted", the only date range fields will relate to the change into Submitted.
  $scope.clearSalesStageFirstXFields=function(){
    for(let key in $scope.searchCtrlVars.activeSearch.criteria.case ){
      if( key.match(/^first_(quoted|sent_to_app_team|submitted|approved|issued)*_(min|max)$/) ){
        delete $scope.searchCtrlVars.activeSearch.criteria.case[key];
      }
    }
  }
  //This function is used by the Fields To Show" box.
  $scope.clearFieldsForModel=function(fieldGroup){
    if(!fieldGroup.showFields){//When showFields is toggled to off, turn off all the fields for that box.
      for(i in fieldGroup.fields){
        var field=fieldGroup.fields[i];
        if($scope.searchCtrlVars.activeSearch.fields_to_show[field.model]){
          $scope.searchCtrlVars.activeSearch.fields_to_show[field.model][field.method]=0;
        }
      }
    }
  }
  /*The tree of possible criteria that get set though the status section:
  -Current/prev MUST be specified for subsequent fields to appear.
  -Sales Stage does NOT need to be specified for subsequent fields to appear (because some categories don't specfy sales stage).
    -Sales Stage id is denormalized to the case, is sequential, and never moves backwards, so it can always be checked on the case.
    -What varies is whether we are checking `sales_stage_id` or `sales_stage_id_min`.
    -The field name may be interpretted by the search model code as a `==` or `>=` comparison on the `sales_stage_id` column.
  -Date ranges do NOT need to be specified for subsequent fields to appear.
    -Date range comparisons may be on the case (for sales stage timestamps), on the status (`created_at` column), or on all statuses (`created_at` column.
  -Category MUST be specified for subsequent fields to appear.
    -Category is checked on the case (for the current status), or on all statuses.
  -Status type is checked on individual statuses.
  */

  $scope.updateStatusFields=function(){
    var aS=$scope.searchCtrlVars.activeSearch;
    if(aS.$currentOrPrev=='current'){
      aS.$salesStageCriterionField='sales_stage_id';
      aS.$stCategoryCriterionModel='case';
      aS.$stCriterionModel='status';
      if(!aS.criteria[aS.$stCriterionModel]){ aS.criteria[aS.$stCriterionModel]={}; }
    }else{
      aS.$salesStageCriterionField='sales_stage_id_min';
      aS.$stCategoryCriterionModel='statuses';
      aS.$stCriterionModel='statuses';
    }
  }

  $scope.updateSTSalesStageTimestampField=function() {//called when `sales_stage_id` changes
    var prefix='entered_stage_',
        aS    =$scope.searchCtrlVars.activeSearch,
        ssId  =aS.criteria.case[aS.$salesStageCriterionField];
    if(!ssId){
      prefix=null;
    }else if(ssId==1){
      prefix="created_at";
    }else{
      for(var i in SALES_STAGES){
        if(SALES_STAGES[i].id==ssId){
          prefix+=SALES_STAGES[i].name.replace(/ /g,'_').toLowerCase();
          break;
        }
      } 
    }
    aS.$stSalesStageTimestampField=prefix;
  }
  $scope.clearStatusCriteria=function(){//called when `$currentOrPrev` changes
    var aSD=$scope.searchCtrlVars.activeSearch.criteria;
    delete aSD.case.sales_stage_id;
    delete aSD.case.sales_stage_id_min;
    delete aSD.case.status_type_category_id;
    Object.keys(aSD.case).forEach(function(key){//delete timestamp restrictions.
      if( key.match(/^entered_stage_/) ){
        delete aSD.case[key];
      }
    });
    delete aSD.status;
    delete aSD.statuses;
  }
  $scope.clearSSDependentStatusCriteria=function(){//called when `sales_stage_id` changes
    var aSD=$scope.searchCtrlVars.activeSearch.criteria;
    delete aSD.case.status_type_category_id;
    Object.keys(aSD.case).forEach(function(key){//delete timestamp restrictions.
      if( key.match(/^entered_stage_/) ){
        delete aSD.case[key];
      }
    });
    delete aSD.status;
    delete aSD.statuses;
  }
  $scope.initCurrentOrPrev=function(){
    var aS =$scope.searchCtrlVars.activeSearch,
        aSD=aS.criteria;
    if(aSD.case.sales_stage_id || aSD.case.status_type_category_id){
      aS.$currentOrPrev='current';
    }else if(aSD.case.sales_stage_id_min || aSD.statuses && aSD.statuses.status_type_category_id){
      aS.$currentOrPrev='all';
    }else{
      //When neither is true, leave it blank.
    }
    $scope.updateStatusFields();
  }

  $scope.collapseForm=function(){
    var aS=$scope.searchCtrlVars.activeSearch;
    aS.marketing_visible=false;
    aS.client_visible   =false;
    aS.case_visible     =false;
    aS.owner_visible    =false;
    aS.status_visible   =false;
    aS.fields_visible   =false;
  }

  $scope.generateCSVString=function(){
    var regExpForCharsThatRequireQuotes=/(,|\n)/g,
        csvOutputString='',
        headings=$scope.searchCtrlVars.headings,
        headingRowForOutput=[],
        rows=$scope.searchCtrlVars.records;

    for(var i in headings){
      var h =headings[i],
          h0=h[0].replace(/"/g,"''"),//model name
          h1=h[1].replace(/"/g,"''"),//field name
          headingCell=h0+' - '+h1;
      if( headingCell.match(regExpForCharsThatRequireQuotes) ){
        headingCell='"'+headingCell+'"';
      }
      headingRowForOutput.push(headingCell);
    }
    csvOutputString+=headingRowForOutput.join(',')+'\n';

    for(var rowIdx in rows){
      var row=rows[rowIdx],
          sanitizedRow=[];
      for(var cellIdx in row){
        if( isNaN(cellIdx) ){ continue; }//angular adds its own keys to objects, including arrays.
        var dataCell=String(row[cellIdx]||'').replace(/"/g,"''");

        if( dataCell.match(regExpForCharsThatRequireQuotes) ){
          dataCell='"'+dataCell+'"';
        }
        sanitizedRow.push(dataCell);
      }
      csvOutputString+=sanitizedRow.join(',')+'\n';
    }

    return csvOutputString;
  }

  $scope.downloadCSV=function(){
    var csvString=$scope.generateCSVString(),
        sanitizedReportName=($scope.searchCtrlVars.activeSearch.name||'').replace(/\W/g,'_'),
        exportTimeStamp=moment().format('YYYY-MM-DD_hh_mm');
    $('#csv-export-link').remove();//Remove element if it exists already.
    //Add and click the export link.
    var a     =document.createElement('a');
    a.id      ='csv-export-link';
    a.href    ='data:attachment/csv,'+encodeURIComponent(csvString);
    a.target  ='_blank';
    a.download=sanitizedReportName+'_'+exportTimeStamp+'_results.csv';
    document.body.appendChild(a);
    a.click();
  }

  $scope.saveSearch=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        httpMethod   =$scope.searchCtrlVars.activeSearch.id ? 'put' : 'post',
        searchCopy   =$.extend(true,{},$scope.searchCtrlVars.activeSearch);

    requestSanitizer.clean(searchCopy,[],true);
    $scope.compact(searchCopy.fields_to_show);

    $rootScope.addBulletin({text:'Saving search.', id:transactionId});

    $http({method:httpMethod,
      url:'/reporting/searches'+($scope.searchCtrlVars.activeSearch.id ? ('/'+$scope.searchCtrlVars.activeSearch.id) : '')+'.json',
      data:{reporting_search:searchCopy} })
    .success(function(data, status, headers, config){
      $scope.getSearches();
      $scope.searchCtrlVars.activeSection='list';
      $rootScope.addBulletin({text:'Successfully saved search.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save search.'+(data.errors||''), klass:'danger', id:transactionId});
    });
    return false;
  }
  //This mimicks the back end compact function,
  //so that only relevant keys are sent to the server.
  //It makes reviewing and debugging requests easier.
  //Only apply this function to a copy.
  $scope.compact=function(obj){
    for(let k in obj){
      for(let k2 in obj[k]){
        if(!obj[k][k2]){ delete obj[k][k2]; }
      }
      if(!Object.keys(obj[k]).length){ delete obj[k]; }
    }
  }

  $rootScope.getCurrentUser().$promise.then(function(){
    $scope.init();
  });
}])
.directive('fieldsToShowCheckbox', [function () {
    return {
      restrict: 'E',
      scope: { modelName: '=', fieldName: '=', displayText: '=', speed: '='},
      template: '\
        <div class="checkbox checkbox-{{speed || \'primary\'}}">\
          <input id="{{modelName}}-{{fieldName}}-{{$parent.$index}}"\
            type="checkbox"\
            ng-model="$parent.searchCtrlVars.activeSearch.fields_to_show[modelName][fieldName]"\
            ng-true-value="1"\
            ng-false-value="null" />\
          <label for="{{fieldName}}-{{$parent.$index}}">\
            <i class="speed-indicator {{speed}}"></i>\
            {{displayText}}\
          </label>\
        </div>',
      link: function (scope, elem, attrs) {
        //Ensure that the needed object structure exists, so it can be modified by clicks on the checkbox.
        var aS=scope.$parent.searchCtrlVars.activeSearch;
        if(!aS.fields_to_show){ aS.fields_to_show={}; }
        var fTS=aS.fields_to_show;
        if(!fTS[scope.modelName]){ fTS[scope.modelName]={}; }
        var fTSModel=fTS[scope.modelName];
        if(!fTSModel[scope.fieldName] || fTSModel[scope.fieldName]==0){ fTSModel[scope.fieldName]=null; }
        
      }
    }
  }])
.directive('reportingDateRangePicker', [function () {
    return {
      restrict: 'E',
      scope: { modelName: '=', minMethod: '=', maxMethod: '='},
      templateUrl: '/reporting/searches/date_range_picker.html',
      link: function (scope, elem, attrs) {
        scope.rangeOpts=[
          ['Ever',             'ages ago',             'today'],
          //This RegEx needs to tolerate 8 digits with or without slashes,
          //because ui-mask briefly sets the value that way, then adds the slashes. 
          ['Date Range',       /^(\d\d\/?\d\d\/?\d{4})?$/, /^(\d\d\/?\d\d\/?\d{4})?$/],//allows blank
          ['Today',            'today',                'today'],
          ['Yesterday',        'yesterday',            'yesterday'],
          ['Last 7 Days',      '7 days ago',           'today'],
          ['Last 30 Days',     '30 days ago',          'today'],
          ['Last 90 Days',     '90 days ago',          'today'],
          ['Previous Week',    'start of last week',   'end of last week'],
          ['Previous Month',   'start of last month',  'end of last month'],
          ['Previous Quarter', 'start of last quarter','end of last quarter'],
          ['Week To Date',     'start of this week',   'today'],
          ['Month To Date',    'start of this month',  'today'],
          ['Quarter To Date',  'start of this quarter','today'],
          ['Year To Date',     'start of this year',   'today']
        ];

        //This function is important because when the reference to `searchCtrlVars.activeSearch` changes,
        //we want our references to nested objects to also change.
        scope.updateModel=function(){
          var aS=scope.$parent.searchCtrlVars.activeSearch;
          if(scope.model!==aS.criteria[scope.modelName]){//check object identity
            if(!aS.criteria[scope.modelName]){
              aS.criteria[scope.modelName]={}//ensure that object exists
            }
            scope.model=aS.criteria[scope.modelName];//set pointer to correct object
          }
        }
        scope.updateModel();

        //This function is used as the condition for the ngIf directive
        //which adds/removes the `drDatetimePicker`s to/from the DOM.
        //Updating the model at the beginning of the function ensures
        //that the `drDatetimePicker`s are instantiated with the correct model.
        scope.selectedOpt=function(){
          scope.updateModel();
          var opt=scope.rangeOpts[0];
          if( (typeof scope.model[scope.minMethod])=='undefined' && (typeof scope.model[scope.maxMethod])=='undefined' ){
            return opt;//So 'Ever' is initially selected, but after a selection is made, 'Date Range' is the default.
          }
          for(var i=1; i<scope.rangeOpts.length; i++){
            if( scope.model[scope.minMethod].match(scope.rangeOpts[i][1]) &&
                scope.model[scope.maxMethod].match(scope.rangeOpts[i][2]) ){
              opt=scope.rangeOpts[i];
            }
          }
          return opt;
        }

        scope.selectAnOpt=function(opt){
          if(opt[0]=='Date Range'){
            var today=moment().format('MM/DD/YYYY');
            scope.model[scope.minMethod]=today;
            scope.model[scope.maxMethod]=today;
          }else{
            scope.model[scope.minMethod]=opt[1];
            scope.model[scope.maxMethod]=opt[2];
          }
        }

      }
    }
  }]);;