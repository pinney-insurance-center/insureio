
function bindDragNDropHandlers(name_of_page, attrs_to_store){
  //this function relies on a common naming standard,
  //with boxes of class widget and handles of class drag_handle
  $('.widget .summary .drag_handle').bind({
    dragstart: function(e){handleDragStart(e.originalEvent)},
    dragend:   function(e){handleDragEnd(  e.originalEvent)}
  });
  $('.widget .summary').bind({
    dragenter: function(e){handleDragEnter(e.originalEvent)},
    dragover:  function(e){handleDragOver( e.originalEvent)},
    dragleave: function(e){handleDragLeave(e.originalEvent)},
    drop:      function(e){handleDragDrop( e.originalEvent, name_of_page, attrs_to_store)}
  });
}

function handleDragStart(e) {
  var $this=$(e.target).closest('.widget');
  console.log('started dragging '+$this.attr('id'));
  $this.addClass('being_dragged');

  drag_src_id = $this.attr('id');

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/plain', $this.attr('id') );
}
function handleDragEnd(e) {
  var $this=$(e.target).closest('.widget');
  console.log('stopped dragging '+$this.attr('id'));
  $this.removeClass('being_dragged');
}
function handleDragOver(e) {
  e.preventDefault();
  var $this=$(e.target).closest('.widget');
  //in theory, +handleDragEnter+ should add this, and +handleDragLeave+ should remove it,
  //but when you +dragenter+ a child element, +dragleave+ is triggered on the parent element.
  $this.addClass('drag_enter');
  return false;
}
function handleDragEnter(e) {
  e.preventDefault();
  var $this=$(e.target).closest('.widget');
  console.log('entered '+$this.attr('id'));
  $this.addClass('drag_enter');
  return false;
}
function handleDragLeave(e) {
  var $this=$(e.target).closest('.widget');
  console.log('left '+$this.attr('id'));
  $this.removeClass('drag_enter');
}
function handleDragDrop(e, name_of_page, attrs_to_store) {
  if (e.stopPropagation) {e.stopPropagation();}
  var $this=$(e.target).closest('.widget'),
     src_id=e.dataTransfer.getData('text/plain');

  console.log('dropped '+src_id+' onto '+$this.attr('id'));

  if ( src_id != $this.attr('id') ) {
    updateDomAfterDnD($('#'+src_id),$this);
    //should be implemented separately on each page that uses reorderable widgets,
    //since they may be structured differently
  }

  $this.removeClass('drag_enter');
  
  updateWidgetList(name_of_page, attrs_to_store);
}

function updateWidgetList(name_of_page, attrs_to_store){
  var tmp_list=[];
  $('.widget').each(function(){
    var $this         =$(this),
        tmp_list_item =[];

    $.each( attrs_to_store, function(i,v){
      //we need the passed in list vs. looping through data attributes because we need to preserve attribute order
      //console.log("i="+i+" and v="+v);
      tmp_list_item.push( $this.data(v) );
    });
    tmp_list.push(tmp_list_item);
  });
  widget_list=tmp_list;
  //all the replaces below are necessary because Javascript's native encodeURI() function is not consistently implemented
  document.cookie=name_of_page+'_widgets='+JSON.stringify(widget_list).replace(/,/g,'%2C').replace(/\[/g,'%5B').replace(/\]/g,'%5D');
}