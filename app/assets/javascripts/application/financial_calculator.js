function santitizeNumber(input, defaultVal) {
  if (typeof input == "number") return input;
  return (input && input.replace(/[^0-9.]+/g,'')) || defaultVal || 0; 
}
function forceFloat(input, defaultVal) { return parseFloat( santitizeNumber(input) ); }
function forceInt(input, defaultVal) { return parseInt( santitizeNumber(input) ); }

var FinancialCalculator = function(scope, options){
  this.scope = scope || {};
  this.children = this.scope.children || [];
  this.educationMode = 'auto';
  this.inflation = DEFAULT_INFLATION;
  for (let x in options) { this[x] = options[x]; }
  this.spouseMarginalTaxRate || (this.spouseMarginalTaxRate = this.spouseRecTaxRate());
};

FinancialCalculator.prototype = {
  /* Totals and subtotals */
  incomeTotal: function(){ return this.sum([this.earnedIncome, this.unearnedIncome]) },
  familyTotal: function(){
    var total = this.incomePresentValue() + this.sum([this.finalExpense, this.educationTotal(), this.other]) - this.spouseIncomePresentValue();
    return total;
  },
  assetsTotal: function(){
    return this.sum([this.checking, this.savings, this.pension, this.investments, this.homeEquity, this.realEstate, this.lifeInsurance]);
  },
  mortgageTotal: function(){
    return this.sum([this.mortgage1, this.mortgage2, this.heloc])
  },
  otherDebtsTotal: function(){
    return this.sum([this.auto, this.credit, this.studentLoans, this.personalLoans])
  },
  netWorth:function(){
      return this.assetsTotal()-this.otherDebtsTotal()-this.mortgageTotal()
    },
  educationTotal: function() {
    this.education || (this.education = 0);
    if (this.educationMode !== 'manual') {
      var calc = this;
      var total = this.children.reduce(function(prev,next){
        return prev + calc.educationForOne(next);
      },0);
      this.education = Math.round(total * 100) / 100.0;
    }
    return this.education;
  },
  /* Sums final expense, other debts, mortgage, education -- not income */
  needsTotal: function() {
    return this.sum([this.finalExpense, this.otherDebtsTotal(), this.mortgageTotal(), this.educationTotal(), this.other]);
  },
  /* Income */
  annualIncome: function() {
    return 12 * forceFloat(this.monthlyIncome);
  },
  spouseAnnualIncome: function() {
    return forceFloat(this.spouseIncome) * (1 - forceFloat(this.spouseMarginalTaxRate));
  },
  spouseRecTaxRate: function() {
    var inc = forceFloat(this.spouseIncome);
    for (max in FinancialCalculator.taxBrackets) {
      if (inc <= max) return FinancialCalculator.taxBrackets[max];
    }
    return 0.396;
  },
  // Calculate "present value" of income needs, based on annualIncome()
  incomePresentValue: function() { /* Calculate future lost income, less future spouse income */
    return FinancialCalculator.presentValue(this.annualIncome(), this.getInterest(), this.getInflation(), this.getYearsToWork());
  },
  // Calculate "present value" of spouse's income
  spouseIncomePresentValue: function() {
    return FinancialCalculator.presentValue(this.spouseAnnualIncome(), 0, this.getInflation(), this.getSpouseYearsToWork());
  },
  /* Education for single child */
  educationForOne: function(child) {
    child.education || (child.education = 0);
    if (child.age && child.fees && child.collegeType) {
      var costsIndexA;
      var costsIndexB = child.fees == "0" ? 0 : 1;
      var yearsOfCollege;
      switch(child.collegeType)
      {
      case "pub2":
        costsIndexA = 0;
        yearsOfCollege = 2;
        break;
      case "pvt4":
        costsIndexA = 3;
        yearsOfCollege = 4;
        break;
      case "pub4":
        costsIndexA = child.residence == "instate" ?  1 : 2;
        yearsOfCollege = 4;
        break;
      default:
        costsIndexA = 2;
        yearsOfCollege = 4;
        break;
      }
      var age = forceInt(child.age, 8);
      var cost = FinancialCalculator.educationCosts[costsIndexA][costsIndexB];
      var yearsTilCollege = age < 18 ? 18 - age : 0;
      var costAtYearX = cost;
      var inflation = 1 + (this.inflation || DEFAULT_INFLATION);
      var sum = 0;
      for (var i=0; i < yearsTilCollege + yearsOfCollege; i++) {
        if (i >= yearsTilCollege) { sum += costAtYearX; }
        costAtYearX *= inflation;
      }
      child.education = sum;
    }
    return child.education;
  },
  /* Accessors */
  getInflation: function() { return forceFloat(this.inflation); },
  getInterest: function() { return forceFloat(this.interest); },
  getYearsToWork: function() { return forceInt(this.yearsOfIncome); },
  getSpouseYearsToWork: function() { return forceInt(this.spouseYearsToWork); },
  /* Reporting */
  runReport: function() {
    this.report = new FinancialCalculatorReport(this);
    this.faceAmount = this.report.bottomLine > 0 ? this.report.bottomLine : 0;
    setTimeout(function(){ $('#financial .currency').formatCurrency(); }, 0);
  },
  rmReport: function(){ /* Hides the report and scrolls user to top of page */
    delete this.report;
    this.report = null;
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  },
  /* Helper functions which do not rely on data in FinancialCalculator instance */
  sum: function(args){return args.reduce(function(prev, next){return forceFloat(prev) + forceFloat(next);} ) }
};

/* Args for needs are essentially: money, interest, inflation, years */
FinancialCalculator.presentValue = function(p, r, g, y) {
  if (r == g)
    return p;
  var x = Math.pow( (1+g)/(1+r), y);
  return p * (1-x) / (r-g);
}
FinancialCalculator.educationCosts = [
  [3131,  10550], // public 2yr
  [8655,  17860], // public 4yr in-state
  [21706, 30911], // public 4yr out-of-state
  [29056, 39518]  // private 4yr
]
FinancialCalculator.taxBrackets = {
  18150:0.10,
  73800:0.15,
  148850:0.25,
  226850:0.28,
  405100:0.33,
  457600:0.35
}

var FinancialCalculatorReport = function(financialCalculator) {
  var f = financialCalculator;
  var ff = forceFloat;
  this.annualIncome   = f.annualIncome();
  this.yearsToWork    = f.getYearsToWork();
  this.education      = f.educationTotal();
  this.mortgage       = f.mortgageTotal();
  this.debts          = f.otherDebtsTotal();
  this.finalExpense   = ff(f.finalExpense);
  this.lifeInsurance  = ff(f.lifeInsurance);
  this.lumpSumNeeds   = f.needsTotal();
  this.incomePv       = f.incomePresentValue();
  var assetReduction  = this.lifeInsurance + ff(f.realEstate) + ff(f.residence);
  this.assets         = f.assetsTotal() - assetReduction;
  this.spousePv       = f.spouseIncomePresentValue();
  this.bottomLine     = this.incomePv + this.lumpSumNeeds - this.assets - this.lifeInsurance - this.spousePv;
  // calculate years table
  this.currentYear = new Date().getFullYear();
  this.years = [ new FinancialCalculatorReport.Year(f.spouseAnnualIncome(), this.annualIncome, this.assets) ];
  var interest = f.getInterest();
  var inflation = f.getInflation();
  for (var i = 0; i < this.yearsToWork; i++) {
    var y = this.years[i];
    var s = y.spouseIncome * (1+inflation);
    var d = y.desiredIncome * (1+inflation);
    var a = y.assets * (1+interest);
    this.years.push(new FinancialCalculatorReport.Year( s, d, a ));
  }
}

FinancialCalculatorReport.Year = function(spouseIncome, desiredIncome, assets){
  this.spouseIncome = spouseIncome;
  this.desiredIncome = desiredIncome;
  this.surplus = spouseIncome - desiredIncome;
  this.assets = assets + this.surplus;
  if (this.assets < 0) this.assets = 0;
  this.needs = (this.surplus + this.assets) * -1;
  if (this.needs < 0) this.needs = 0;
}