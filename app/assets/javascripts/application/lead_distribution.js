
angular.module('drLeadDistribution', [])
.factory('leadDistStateObj', function () {
  //Creating a blank object here allows the `init` function in the controller to set state vars,
  //as well as allowing each filter to import the object only once and avoid passing it in each filter call.
  return {
    criteria:{}
  };
})
.factory('LeadDistRuleModel', function () {
  function Rule (obj, scope) {
    angular.extend(this, obj);
    var r=this;
    //set fixed properties
    r.brand=scope.brands.filter(function(brand){ return brand.id==r.brand_id })[0];
    r.leadType=scope.leadTypes.filter(function(leadType){ return leadType.id==r.lead_type_id })[0];
    r.agentName=r.user_name;
    r.brandName=r.brand && r.brand.full_name;
  }
  //set dynamic properties
  Object.defineProperties(Rule.prototype, {
    leadTypeText: { get:function(){ return this.leadType && this.leadType.text } },
    on: {
      get:function(){return !this.off},
      set:function(val){this.off = !val}
    },
    quickSet: {
      get:function(){ return this._quickSet },
      set:function(val){
        Object.defineProperty(this, '_quickSet', {value:parseInt(val), configurable:true});
        this.sunday = this.monday = this.tuesday = this.wednesday = this.thursday = this.friday = this.saturday = this.quickSet;
      }
    }
  });
  return Rule;
})
.controller('LeadDistCtrl', ['$scope', '$http', '$sce', '$q', '$filter', '$location', '$resource', '$uibModal', 'leadDistStateObj', 'LeadDistRuleModel', 'bulletinService',
function ($scope, $http, $sce, $q, $filter, $location, $resource, $uibModal, stateObj, LeadDistRuleModel, bulletinService) {

  /* Make a string representing identifiers for all lead-dist rules in the @args array */
  function formatLeadDistUserRules (arrayOfRules) {
    return arrayOfRules.map(function (record) {
      return [record.id,
        record.brandName || record.brand && record.brand.full_name,
        record.leadType && record.leadType.text,
        record.agentName]
      .filter(function (value) { return !!value })
      .join(' - ');
    }).join('\n');
  }

  $scope.init=function(){
    $scope.checkSwitcherConfig = {
      on_state_content: '<span class="fa fa-check"></span>',
      off_state_content: '<span class="fa fa-times"></span>'
    }
    $scope.stateObj=stateObj;
    $scope.filter = { can: true }; /* Show/hide rules based on agent permission */
    const criteria = $scope.criteria = stateObj.criteria;
    criteria.agentId = $location.search().agent_id;
    criteria.brandId = $location.search().brand_id;
    criteria.leadTypeId = $location.search().lead_type_id;
    $scope.newCursor = {};
    $scope.getJoinTables();
    $scope.joinTables.$promise.then(() => {
      if (!criteria.brand && criteria.brandId) criteria.brand = $scope.brands.find(x => x.id == criteria.brandId);
      if (!criteria.leadType && criteria.leadTypeId) criteria.leadType = $scope.leadTypes.find(x => x.id == criteria.leadTypeId);
      $scope.getLeadDistributionRules({});
    });
    if (!criteria.agent && criteria.agentId) {
      criteria.agent = $resource(`/users/${criteria.agentId}.json`).get();
      criteria.agent.$promise.then(() => { criteria.agent.name = criteria.agent.full_name });
    }
  }

  $scope.getJoinTables=function(){
    //This response will contain info about linkages between users, brands, and lead types.
    //It will also contain distribution cursor records, which consist only of lead_type_id, brand_id, rule_id.
    $scope.joinTables=$scope.stateObj.joinTables=$resource('/lead_distribution/cursors/join_tables.json').get({},
      function(data, status, headers, config){//success
        $scope.brands=$scope.joinTables.brands;
        $scope.leadTypes=$scope.joinTables.lead_types;
      },
      function(data){//error
        $scope.joinTables.statusCode=data.status;
      }
    );
  }

  $scope.createCursor = function () {
    const cur = $scope.newCursor;
    if (!(cur.brand && cur.brand.id)) return alert('Please enter brand');
    if (!(cur.lead_type_text && cur.lead_type_text.length > 0)) return alert('Please enter lead type');
    const transactionId = $scope.randomNonZeroId();
    bulletinService.add({ text:'Creating combination.', id:transactionId });
    $http.post('/lead_distribution/cursors.json', { brand_id: cur.brand.id, lead_type_text: cur.lead_type_text })
    .success((data, status, headers, config) => {
      bulletinService.add({ text:'Created combination.', klass:'success', id:transactionId });
      $scope.getJoinTables();
      $scope.getAgents();
      if ($scope.criteria.agent || $scope.criteria.brand || $scope.criteria.leadType)
        $scope.getLeadDistributionRules();
      $scope.newCursor = {};
    })
    .error(function(data, status, headers, config){
      var err = data && data.errors ? data.errors : 'Error: '+status;
      if (typeof err === 'object')
        err = Object.keys(text).reduce((arr, key) => {
          let val = text[key].join(', ');
          arr.push(`${key} ${val}`);
          return arr;
        }, []).join('. ');
      bulletinService.add({ text:err, klass:'danger', id:transactionId });
    });
  }

  $scope.enabledLabel = () => {
    switch ($scope.filter && $scope.filter.can) {
      case true: return 'Enabled';
      case false: return 'Disabled';
      default: return 'All';
    }
  }

  $scope.selectAgentCb = function (agent, model) {
    // set 'on' on agent
    if( agent.hasOwnProperty('on') ){ return; }
    Object.defineProperty(agent, '_on', {value:!agent.temporary_suspension, configurable:true} );
    Object.defineProperty(agent, 'on', {
      get:function(){ return this._on },
      set:function(val){ Object.defineProperty(this, '_on', {value:val, configurable:true}) }
    });
    // get lead distribution rules
    $scope.getLeadDistributionRules({agent:agent});
  }
  $scope.selectLeadTypeCb = function (leadType, model) {
    $scope.getLeadDistributionRules({leadType:leadType});
  }
  $scope.selectBrandCb = function (brand, model) {
    $scope.getLeadDistributionRules({brand:brand});
  }
  /* Mutate all rules according to attrs */
  $scope.setAll = (attrs) => {
    $scope.rules.forEach(rule => Object.assign(rule, attrs));
  }
  $scope.getAgents = function (searchStr) {
    if( $scope.agents || searchStr.length<2 ){ return $scope.agents; }
    var transactionId=$scope.randomNonZeroId();
    $scope.agents=$resource('/users/agents.json').query(
      {name:searchStr},
      function(data, status, headers, config){//success
        $scope.agents = data.sort($scope.sortFnFactory('name'));
      },
      function(data){//error
        $scope.agents.statusCode=data.status;
        $scope.addBulletin({text:'Failed to load agents. '+(data.errors||''), klass:'danger', id:transactionId});
      }
    );
    return $scope.agents;
  }
  $scope.lowerPriority=function(cursorId){
    $http.get('/lead_distribution/cursors/'+cursorId+'/reset_counts.json')
    .success(function(data, status, headers, config){
      $scope.getLeadDistributionRules({});
    });
  }
  $scope.clearAgent = () => { $scope.clearCriterion('agent', 'agentId') }
  $scope.clearBrand = () => { $scope.clearCriterion('brand', 'brandId') }
  $scope.clearLeadType = () => { $scope.clearCriterion('leadType', 'leadTypeId') }
  $scope.clearCriterion = (...keys) => {
    keys.forEach(key => {delete $scope.criteria[key]});
    $scope.getLeadDistributionRules({});
  }
  $scope.getLeadDistributionRules = function (opts) {
    opts = angular.extend($scope.criteria, opts);
    const params = {
      include_permission_flag: 1,
      include_user_name: 1,
      lead_type_id: opts.leadType && opts.leadType.id,
      brand_id: opts.brand && opts.brand.id || opts.brandId,
      agent_id: opts.agent && opts.agent.id || opts.agentId,
    };
    for (let key in params) if (!params[key]) delete params[key];
    /* Update browser location */
    const queryStr = Object.keys(params).map(k => `${k}=${params[k]}`).join('&');
    const loc = `${window.location.pathname}?${queryStr.slice(1)}`;
    window.history.pushState({}, '', loc);
    const paramsPresent = ['lead_type_id', 'brand_id', 'agent_id'].reduce((isPresent, key) => isPresent || !!params[key], false);
    if (!paramsPresent) return;
    // fetch rules for `opts`
    $scope.rules = $resource('/lead_distribution/user_rules.json').query(params);
    $scope.rules.$promise.then(data => {
      const cursorMins = $scope.minCountInGroup = {};
      $scope.rules.forEach((rule, i) => {
        cursorMins[rule.cursor_id] = Math.min(rule.count||0, cursorMins[rule.cursor_id]||0);
        $scope.rules[i] = new LeadDistRuleModel($scope.rules[i], $scope);
      });
      $scope.sortRules();
    });
  }

  $scope.openNewDistGroupModal = () => {
    /* Bulletins may appear within the modal for feedback purposes. Other
    /* modals clear existing bulletins, so that behaviour is repeated here. */
    bulletinService.clear();
    $uibModal.open({
      resolve: {},
      templateUrl: '/lead_distribution/user_rules/modal_for_brand_lead_combo.html',
      scope: $scope
    });
  }

  // Update all displayed rules in backend
  $scope.saveSettings = function () {
    var displayedRules = $filter('filterRules')($scope.rules, $scope.criteria, $scope.filter),
        transactionId  = $scope.randomNonZeroId();
    $scope.addBulletin({text:'Saving changes to lead distribution settings.', id:transactionId});
    $http.put('/lead_distribution/user_rules/update_multiple.json', {rules:displayedRules})
    .success(function(data, status, headers, config){
      $scope.addBulletin({text:'Successfully saved changes to lead distribution settings.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      if (status == 422) {
        console.error(data);
        var errText = [];
        if (data.error)
          errText.append(data.error);
        if (data.failures)
          errText.append('Failed to save '+data.failures.length+' records:\n'+formatLeadDistUserRules(data.failures));
        if (errText.length)
          $scope.addBulletin({text:errText.join('\n'), klass:'danger', id:transactionId});
      }
    })
    .finally(function(data, status, headers, config){
      if (data.deleted) {
        let msg = ('Removed '+data.deleted.length+' records which are no longer valid:\n'+formatLeadDistUserRules(data.deleted));
        var deletedIdMap = {};
        data.deleted.forEach(function (deletedRecord) { deletedIdMap[deletedRecord.id] = deletedRecord });
        $scope.rules = $scope.rules.filter(function (rule) { return deletedIdMap.hasOwnProperty(rule.id) });
        if ($scope.lastOpenBulletin && $scope.lastOpenBulletin.id == transactionId) {
          $scope.lastOpenBulletin.text += '\n. . . . .\n' + msg;
          $scope.$apply();
        } else
          $scope.addBulletin({text: msg, klass:'notice', id: transactionId});
      }
    });
  }

  // Tell back end to toggle criteria.agent's temporary_suspension
  $scope.updateAgentSuspension = function () {
    var transactionId= $scope.randomNonZeroId();
    $scope.addBulletin({text:'Updating agent suspension.', id:transactionId});
    $http.put('/users/'+$scope.criteria.agent.id+'/update.json',
      {temporary_suspension: !$scope.criteria.agent.on })
    .success(function(data, status, headers, config){
      $scope.addBulletin({text:'Suspension updated on agent '+$scope.criteria.agent.id+'.',klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $scope.addBulletin({text:'Failed to update suspension on agent '+$scope.criteria.agent.id+'.',klass:'danger', id:transactionId});
    });
  }

  // Returns agent from $scope.agents matching `id`
  $scope.findById = function (array, id) {
    for (var i in array)
      if (array[i].id == id)
        return array[i];
  }

  $scope.trustAsHtml = function(value) {
    return $sce.trustAsHtml(value);
  }

  // Returns a dummy, already-resolved promise
  $scope.resolvedPromiseFactory = function () {
    var deferred = $q.defer();
    deferred.resolve();
    return deferred.promise;
  }

  $scope.showSortBtn = function (key, reverse) {
    return true;
  }

  $scope.sortRules = function (sortKey, reverse) {
    $scope.rules.sort(function(a,b){
      var key = sortKey || 'agentName';
      if (a[key] < b[key])
        return reverse ? 1 : -1;
      if (a[key] > b[key])
        return reverse ? -1 : 1;
      return 0;
    });
  }

  $scope.sortFnFactory = function (key) {
    return function (a,b) {
      if (a[key] > b[key])
        return 1;
      if (a[key] < b[key])
        return -1;
      return 0;
    };
  }

  $scope.init();
}])
.directive('drSorter', function(){
  return {
    restrict: 'E',
    transclude: true,
    scope: { key: '@', reverse: '=drSortReverse' },
    template: '<a ng-click="$parent.sortRules(key, reverse)" ng-show="$parent.showSortBtn(key, reverse)" ng-transclude></a>'
  }
})
.filter('havingAssociation', function(){
  return function (arrayToFilter, foreignObjId, joinTable, foreignIdKeyOrIndex, ownIdKeyOrIndex) {
    // Let N=joinTable.length and M=arrayToFilter.length.
    // idsWithAssoc.length will likely be N/2 or smaller,
    // because users managing lead dist are unlikely to be a member of >50% of brands visible to them,
    // and it is unlikely that a lead type is used among nultiple brands.
    // Approx # of operations: 0.1N*M.
    if(!arrayToFilter){ return []; }
    var idsWithAssoc=[],
        objsWithAssoc;
    joinTable.forEach(function(r){
      if(r[foreignIdKeyOrIndex]==foreignObjId){
        idsWithAssoc.push(r[ownIdKeyOrIndex]);
      }
    });
    objsWithAssoc=arrayToFilter.filter(function(o){ return idsWithAssoc.includes(o.id); })
    return objsWithAssoc;
  }
})
.filter('agentsBySelectedBrand', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (agentsArray) {
      if(!stateObj.criteria.brand){ return agentsArray; }
      return havingAssociationFilter(agentsArray, stateObj.criteria.brand.id, stateObj.joinTables.brands_users, 0, 1);
    }
  }])
.filter('agentsBySelectedLeadType', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (agentsArray) {
      if(!stateObj.criteria.leadType){ return agentsArray; }
      var brandsArray=stateObj.joinTables.brands,
          brandsForSelectedLeadType,
          agentsForBrandList;
      brandsForSelectedLeadType=havingAssociationFilter(brandsArray, stateObj.criteria.leadType.id, stateObj.joinTables.lead_types_brands, 0, 1),
      agentsForBrandList = (agentsArray||[]).filter(function(agent){
        var isMatch;
        for(var i=0; i<stateObj.joinTables.brands_users.length; i++){
          var row=stateObj.joinTables.brands_users[i];
          if(row[1]==agent.id){
            isMatch=true;
            break;
          }
        }
        return isMatch;
      });
      return agentsForBrandList;
    }
  }])
.filter('agentsBySelections', function (agentsBySelectedBrandFilter, agentsBySelectedLeadTypeFilter) {
  return function (agentsArray) {
    var aA1=agentsBySelectedBrandFilter(agentsArray),
        aA2=agentsBySelectedLeadTypeFilter(aA1);
    return aA2;
  }
})
.filter('leadTypesBySelectedBrand', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (leadTypesArray) {
      if(!stateObj.criteria.brand){ return leadTypesArray; }
      return havingAssociationFilter(leadTypesArray, stateObj.criteria.brand.id, stateObj.joinTables.lead_types_brands, 0, 1);
    }
  }])
.filter('leadTypesBySelectedAgent', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (leadTypesArray) {
      if (!leadTypesArray) return [];
      if(!stateObj.criteria.agent){ return leadTypesArray; }
      var brandsArray=stateObj.joinTables.brands,
          brandsForSelectedAgent,
          leadTypesForBrandList;
      brandsForSelectedAgent=havingAssociationFilter(brandsArray, stateObj.criteria.agent.id, stateObj.joinTables.brands_users, 1, 0);
      leadTypesForBrandList=leadTypesArray.filter(function(leadType){
        var isMatch;
        for(var i=0; i<stateObj.joinTables.lead_types_brands.length; i++){
          var row=stateObj.joinTables.lead_types_brands[i];
          if(row[0]==leadType.id){
            isMatch=true;
            break;
          }
        }
        return isMatch;
      });
      return leadTypesForBrandList;
    }
  }])
.filter('leadTypesBySelections', function (leadTypesBySelectedBrandFilter, leadTypesBySelectedAgentFilter) {
  return function (leadTypesArray) {
    var lTA1=leadTypesBySelectedBrandFilter(leadTypesArray),
        lTA2=leadTypesBySelectedAgentFilter(lTA1);
    return lTA2;
  }
})
.filter('brandsBySelectedAgent', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (brandsArray) {
      if(!stateObj.criteria.agent){ return brandsArray; }
      return havingAssociationFilter(brandsArray, stateObj.criteria.agent.id, stateObj.joinTables.brands_users, 1, 0);
    }
  }])
.filter('brandsBySelectedLeadType', ['leadDistStateObj','havingAssociationFilter', function (stateObj, havingAssociationFilter) {
    return function (brandsArray) {
      if(!stateObj.criteria.leadType){ return brandsArray; }
      return havingAssociationFilter(brandsArray, stateObj.criteria.leadType.id, stateObj.joinTables.lead_types_brands, 0, 1);

    }
  }])
.filter('brandsBySelections', function (brandsBySelectedAgentFilter,brandsBySelectedLeadTypeFilter) {
  return function (brandsArray) {
    var bA1=brandsBySelectedLeadTypeFilter(brandsArray),
        bA2=brandsBySelectedAgentFilter(bA1);
    return bA2;
  }
})
.filter('filterRules', function () {
  return function (array, serverCriteria, browserCriteria) {
    const {agentId, brandId, leadTypeId, ...server} = serverCriteria;
    const {can, ...browser} = browserCriteria;
    return array ? array.filter(rule => {
      if (agentId && agentId != rule.user_id) return false;
      if (brandId && brandId != rule.brand_id) return false;
      if (leadTypeId && leadTypeId != rule.lead_type_id) return false;
      if (can != null && can == rule.lacks_permission) return false;
      return true;
    }) : [];
  }
})
;
