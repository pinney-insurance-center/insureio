  
const maxRecentConsumerCount=15;

angular.module('ConsumerManagementApp', ['DrFilters', 'DrModels'])
.controller('ConsumerManagementCtrl', ['$scope', '$resource', 'ConsumerModel', '$uibModal', '$http', '$controller', '$filter', '$location', 'Policy','$timeout', 'bulletinService',
  function ($scope, $resource, ConsumerModel, $uibModal, $http, $controller, $filter, $location, Policy, $timeout, bulletinService) {
    $scope.init=function(){
      $scope.consumerMgmtVars = { rightSidebarIsClosed: window.innerWidth < 1024 };
      window.addEventListener('resize', evt => {
        $scope.consumerMgmtVars.rightSidebarIsClosed = window.innerWidth < 1024;
      });
      $scope.consumerMgmtVars.consumerId=$location.path().match(/\/consumers\/(\d+)/)[1];
      $scope.getConsumer();
      $scope.consumerMgmtVars.activeTab=$location.search()['active_tab'] || 'Policy';

      //Store a reference to this scope, so that calls to `getPolicies`
      //that take place within subscopes will still store policies on this scope.
      $scope.consumerMgmtVars.scope=$scope;
      //Create an instance of `PolicyCtrl` which shares this scope,
      //so this controller can share its functions.
      $controller('PolicyCtrl',{$scope : $scope});
      $scope.getSubscriptions($scope.consumerMgmtVars.consumerId,'Consumer', $scope);
      $scope.setConsumerMgmtPendingReqWatcher();
    }

    //When a nonidemotent request completes,
    //refresh the requirements list.
    //Why put this here?
    // 1) Different models are updated thru different endpoints,
    //    so we can't just set an interceptor on one resource to catch all data changes.
    // 2) We don't want this to keep running once they navigate away from this scope.
    // 3) Angular makes it impossible to cleanly add and remove global http callbacks at runtime.
    $scope.setConsumerMgmtPendingReqWatcher=function(){
      var unregister=$scope.$watch(
        function(){//what to watch
          var reqs=$http.pendingRequests,
              relevantReqs=reqs.filter(function(req){
                return req.method.match(/(put|post|patch|delete)/i);
              });
          return relevantReqs.length;
        },
        function(newVal,oldVal,scope){//what to do
          if(!$scope.consumerMgmtVars){
            unregister();//stop watching
          }
          if( newVal<oldVal ){//when a req completes
            $timeout(function(){
              var policyIsOpen=$scope.consumerMgmtVars.activeTab=='Policy' && $scope.stateVars.activePolicy.id;
              if(policyIsOpen){
                $scope.checkOutstandingReqsForAppFulfillment();//defined in `PolicyCtrl`
              }
            },1);
          }
        }
      );
    }

    $scope.setActiveTab=function(tabName){
      $scope.consumerMgmtVars.activeTab=tabName;
      $scope.setTemplateUrlForActiveTab();
      $location.search({active_tab:tabName});
    }

    $scope.setTemplateUrlForActiveTab=function(){
      var url;
      switch($scope.consumerMgmtVars.activeTab) {
        case 'Policy':     url='/crm/cases/container.html'; break;
        case 'Financial':  url='/quoter/finance.html'; break;
        case 'Health':     url='/quoter/underwriting.html'; break;
        case 'Marketing':  url='/contacts/marketing.html'; break;
        case 'Related':    url='/consumers/related/container.html'; break;
        case 'Files':      url='/attachments/container.html'; break;
      }
      $scope.templateUrlForActiveTab=url;
      return url;
    }

    $scope.getConsumer=function(reload){
      //Calling the variable 'person' allows more of the same templates to be shared between user and consumer views.
      if($scope.person && !reload){ return $scope.person; }
      $scope.person=ConsumerModel.get({id: $scope.consumerMgmtVars.consumerId});
      $scope.person.$promise.then(function(){
        $scope.refreshSpouse();
        $scope.refreshCurrentTasks();
        $scope.updateRecentConsumers();
        $scope.$broadcast('refreshNotes');
      })
      return $scope.person;
    }

    /* getStaffAssignment was hoisted from policy_mgmt.js */
    $scope.getStaffAssignment = policy => {
      if (!policy) policy = $scope.stateVars.activePolicy;
      if (!policy) return;
      const xid = bulletinService.randomNonZeroId();
      let params;
      if ('policy_number' in policy)
        params = `owner_id=${policy.id}&owner_type=Crm%3A%3ACase`;
      else
        params = `owner_id=${policy.consumer_id}&owner_type=Consumer`;
      bulletinService.add({text:'Loading staff assignment.', id:xid});
      $http.get(`/usage/staff_assignments.json?${params}`).success(data => {
        $scope.stateVars.activePolicyStaffAssignment = data;
        bulletinService.removeBulletinById(xid);
      }).error(data => {
        bulletinService.add({text:`Failed to load staff assignment. ${data.errors||''}`, klass:'danger', id:xid});
      });
    }
    $scope.$on('refreshStaffAssignment', evt => $scope.getStaffAssignment());

    $scope.updateRecentConsumers=function(){
      var recents  =JSON.parse(localStorage.recentConsumers || "[]"),
          lineBreakSymbol='<span style="opacity:0.5">&#8627;</span> ',
          consumer ={
            id:         $scope.person.id,
            short:      $scope.person.short_id,
            name:       $scope.person.full_name,
            phone:      ($scope.person.$primaryPhone||{}).value,
            email:      ($scope.person.$primaryEmail||{}).value,
            addr:       $filter('drFormatAddress')($scope.person.$primaryAddress),
            time:       moment()
          };

      //remove any earlier entry for the consumer that is to be added
      var oldIndex=-1;
      $.each(recents,function(index,oldConsumerEntry){
        if( oldConsumerEntry.id==consumer.id ){
          oldIndex=index;
        }
      });
      if(oldIndex!=-1){recents.splice(oldIndex,1)};

      //enforce max count
      if(recents.length>=maxRecentConsumerCount){
        recents.pop();
      }

      recents.unshift(consumer)
      localStorage.recentConsumers=JSON.stringify(recents);
    }

    $scope.refreshSpouse=function(){
      $scope.clientSpouse=$resource('/consumers/:id/spouse.json').get({id:$scope.consumerMgmtVars.consumerId});
    }

    Object.defineProperty($scope, 'tasks', { // In use by Consumer.TasksCtrl and PolicyCtrl
      value: new Array(2),
      configurable: true,
    });
    Object.defineProperties($scope.tasks, {
      addCurrentTask: {
        configurable: true,
        value: function (task) {
          this[1].push(task);
          this.formatTask(task);
       }
      },
    });


    // Written by Maurice; moved by Markham
    $scope.policyList=function(filterStr){
      var unifiedList=($scope.policies && $scope.policies.data) || [],
          filteredUnifiedList=[],
          placedStageId=$scope.SALES_STAGE_ID_FOR_PLACED;

      if(!filterStr){filterStr='All'}

      switch(filterStr){
        case 'Placed':
          $.each(unifiedList, function(index,p){
            if(p.sales_stage_id==placedStageId) filteredUnifiedList.push(p);
          });
          break;
        case 'Pending':
          $.each(unifiedList, function(index,p){
            if( p.sales_stage_id<placedStageId && !('include_in_forecasts' in p) && ('active' in p && p.active) ) filteredUnifiedList.push(p);
          });
          break;
        case 'Closed':
          $.each(unifiedList, function(index,p){
            if( ('closed' in p && p.closed) || ('active' in p && !p.active) ) filteredUnifiedList.push(p);
          });
          break;
        case 'All':
          filteredUnifiedList=unifiedList;
          break;
        case 'Open':
          $.each(unifiedList, function(index,p){
            if( p.sales_stage_id<placedStageId && (('closed' in p && !p.closed) || ('active' in p && p.active)) ) filteredUnifiedList.push(p);
          });
          break;
      }

        return filteredUnifiedList;
    };

    $scope.openContactMethodsModal=function(){
      //Bulletins will show within the modal for feedback purposes.
      //So existing bulletins should expire before the modal opens.
      bulletinService.clear();
      $uibModal.open({
        resolve    : {},
        templateUrl: '/contacts/modal_for_contact_methods.html',
        controller : 'contactMethodsModalCtrl',
        scope      : $scope
      });
    }

    $scope.openEmploymentInfoModal=function(){
      //Bulletins will show within the modal for feedback purposes.
      //So existing bulletins should expire before the modal opens.
      bulletinService.clear();
      $uibModal.open({
        resolve    : {},
        templateUrl: '/contacts/modal_for_employment_info.html',
        controller : 'employmentInfoModalCtrl',
        scope      : $scope
      });
    }

    $scope.openPersonalInfoModal=function(){
      //Bulletins will show within the modal for feedback purposes.
      //So existing bulletins should expire before the modal opens.
      bulletinService.clear();
      $uibModal.open({
        resolve    : {},
        templateUrl: '/contacts/modal_for_personal_info.html',
        controller : 'personalInfoModalCtrl',
        scope      : $scope
      });
    }

    $scope.openTrackingDataModal=function(){
      //Bulletins will show within the modal for feedback purposes.
      //So existing bulletins should expire before the modal opens.
      bulletinService.clear();
      $uibModal.open({
        resolve    : {},
        templateUrl: '/consumers/modal_for_tracking_data.html',
        controller : 'trackingDataModalCtrl',
        scope      : $scope
      });
    }

    $scope.getCurrentUser().$promise.then(function(){
      $scope.init();
    });
}])
.controller('employmentInfoModalCtrl', ['$scope', '$http', '$q', '$uibModal',
  function ($scope, $http, $q, $uibModal) {
    //
  }
])
.controller('personalInfoModalCtrl', ['$scope', '$http', '$q', '$uibModal',
  function ($scope, $http, $q, $uibModal) {
    //
  }
])
.controller('trackingDataModalCtrl', ['$scope', '$http', '$q', '$uibModal', 'bulletinService',
  function ($scope, $http, $q, $uibModal, bulletinService) {

    $scope.person.$tmpBrand={id:$scope.person.brand_id, name:$scope.person.brand_name}

    $scope.getBrands=function(){
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading brands.', id:transactionId});

      $http.post('/users/member_brand_names.json',{ids:[$scope.currentUser.id]})
      .success(function(data, status, headers, config){
        $rootScope.removeBulletinById(transactionId);
        $scope.brands=data[$rootScope.currentUser.id];
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load brands.'+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.confirmAndUpdate=function(msg,fieldName,newValue){
      if( !confirm(msg) ){return; }
      var saveCb=function(){
        $scope.person[fieldName]=newValue;
        if(fieldName=='brand_id'){
          $scope.person.brand_name=$scope.person.$tmpBrand.name;
        }
      }
      $scope.person.$updateWithCustomParams({key: fieldName, value: newValue},saveCb);
    }

  }
])
//This controller is used in User Management as well.
.controller('contactMethodsModalCtrl',
  function ($scope, $http, $q) {
    $scope.contactMethodFormObjTemplate={
      clearAndCloseForm:function(){
        var name=this.name,
            nameCapped=name.charAt(0).toUpperCase() + name.substr(1);
        this.model={
          person_id:$scope.person.id,
          person_type:$scope.person.$personType.replace('Recruit','User')
        };
        this.model[name]={};
        this.show=false;
      },
      submitForm:function(){
        var namePlural  =this.name+(this.name.charAt(this.name.length-1)=='s' ? 'es' : 's' ),
            parentObject=$scope.person[namePlural],
            url         ='/'+namePlural+'.json',
            objName     =this.name,
            obj         =this,
            successFn   =function(){obj.clearAndCloseForm();};

        if(this.name=='web'){
          objName=this.model.web_type_id==1 ? 'website' : 'social media account';
        }
        $scope.createRecord(this.model, null, parentObject, url, objName, successFn );
      },
      setThePrimary:function(objId){
        var objName  =this.name,
            keyToSet ='primary_'+objName+'_id',
            successFn=function(){
                        $scope.person[keyToSet]=objId;
                        $rootScope.setPointersToPrimaryContactMethods($scope.person);
                      };

        $scope.person.$updateWithCustomParams({key: keyToSet, value: objId},successFn);
      }
    }
    var contactMethodModels=['email','phone','address','web']

    for(var i in contactMethodModels){
      $scope[contactMethodModels[i]+'FormObj']=$.extend({name:contactMethodModels[i]}, $scope.contactMethodFormObjTemplate);
      $scope[contactMethodModels[i]+'FormObj'].clearAndCloseForm();
    }
})
.controller('RelatedConsumersCtrl', ['$scope', '$rootScope', '$http', 'bulletinService',
  function ($scope, $rootScope, $http, bulletinService) {
    $scope.init=function (){
      $scope.getExistingRelations();
      $scope.getSuggestedRelations();
      $scope.relatedConsumerVars={}
    }
    //Existing relationships are returned from the server with
    //details about the relationship itself as well as details about the other person,
    //under one of two keys: `stakeholder` or `primary_insured`.
    $scope.getExistingRelations = function () {
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading existing relations.', id:transactionId});
      $http.get('/consumers/'+$scope.consumerMgmtVars.consumerId+'/related.json')
      .success(function(data, status, headers, config){
        $scope.existingRelations = data;
        //Establish which direction the relationship goes.
        //Because "A is B's parent" is not the same as "B is A's parent".
        for(let i in $scope.existingRelations){
          let rel=$scope.existingRelations[i];
          if(rel.primary_insured_id==$scope.person.id){
            //key `stakeholder` is already defined.
            rel.otherPerson=rel.stakeholder;
            rel.primary_insured=$scope.person;
          }else{
            //key `primary_insured` is already defined.
            rel.otherPerson=rel.primary_insured;
            rel.stakeholder=$scope.person;
          }
        }
        $scope.person.related_count=$scope.existingRelations.length;
        $scope.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load existing relations. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.getSuggestedRelations = function () {
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading suggested relations.', id:transactionId});
      $http.get('/consumers/'+$scope.consumerMgmtVars.consumerId+'/suggested_relations.json')
      .success(function(data, status, headers, config){
        $scope.suggestedRelations = data;
        $scope.removeBulletinById(transactionId);
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to load suggested relations. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.searchConsumers=function(searchString){
      if(searchString.length < 3){ return; }//min length prevents overly broad queries.
      var paramsObj={no_pagination:true,
            text:'full_name',
            search_term:searchString,
            search_by:'name_or_id',
            'model[]':['Consumer'],
            'select[]':['full_name', 'id']
          };

      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Searching consumers.', id:transactionId});
      $http.get('/contacts.json',{params:paramsObj})
      .success(function(data, status, headers, config){
        $scope.consumersForSearch = data;
        $scope.removeBulletinById(transactionId);//just remove the loading message if successful.
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to search consumers. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.relateConsumer=function(existingConsumerId,relationshipObj){
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Saving relationship.', id:transactionId});

      relationshipObj.stakeholder_id=existingConsumerId;
      if(!existingConsumerId){
        var attrs=relationshipObj.stakeholder_attributes
        attrs.agent_id=$rootScope.currentUser.id;
        attrs.brand_id=$scope.person.brand_id;
      }
      $http.put('/consumers/'+$scope.consumerMgmtVars.consumerId,{consumer:{relationships_attributes:[relationshipObj]}})
      .success(function(data, status, headers, config){
        bulletinService.add({text:'Successfully saved relationship.', klass:'success', id:transactionId});
        //clear the forms
        $scope.newRelationToExisting={};
        $scope.newRelationToNew={};
        $scope.relatedConsumerVars.searchedConsumer={};
        $scope.relatedConsumerVars.addHow='';//close the form view
        $scope.getSuggestedRelations();
        $scope.getExistingRelations();
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to save relationship. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
    $scope.setPrimaryContact=function (id,name){
      if ($scope.person.primary_contact_id == id){ id=null; name=null; }
      var transactionId = bulletinService.randomNonZeroId(),
          newPCName=name;
      bulletinService.add({text:'Saving primary contact.', id:transactionId});
      $http.put('/consumers/'+$scope.consumerMgmtVars.consumerId,{consumer:{primary_contact_id:id}})
      .success(function(data, status, headers, config){
        $scope.person.primary_contact_id=id;
        $scope.person.$primary_contact_name=newPCName;
        bulletinService.add({text:'Successfully saved primary contact.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to save primary contact. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.deleteRelation=function(id){
      if(!confirm('Are you sure?')) return;
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Deleting relationship.', id:transactionId});
      $http.delete('/crm/consumer_relationships/'+id+'.json')
      .success(function(data, status, headers, config){
        var deletedRelationIndex=null;
        $.each($scope.existingRelations,function(index,r){
          if(id==r.id){
            deletedRelationIndex=index;
            return false;
          }
        });
        if(deletedRelationIndex!=null) $scope.existingRelations.splice(deletedRelationIndex,1);
        $rootScope.person.related_count=$scope.existingRelations.length;
        $scope.refreshSpouse();
        bulletinService.add({text:'Successfully deleted relationship.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to delete relationship. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.init();
  }])
  ;
