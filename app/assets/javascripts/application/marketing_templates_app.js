angular.module('dr.marketing', ['ui.bootstrap'])
.controller("MarketingTemplatesCtrl", ['$scope', '$rootScope', '$element', '$location', '$http', '$resource', '$uibModal',
  function ($scope, $rootScope, $element, $location, $http, $resource, $uibModal) {

    $scope.init=function(){
      $scope.MARKETING_TEMPLATE_MEDIA=$scope.MARKETING_TEMPLATE_MEDIA.concat('Any');
      $scope.MARKETING_TEMPLATE_PURPOSES=$scope.MARKETING_TEMPLATE_PURPOSES.concat({id:null,name:'Any'});
      $scope.PLANNING_CATS=$scope.PLANNING_CATS.concat({id:null,name:'Any'});
      $scope.PRODUCT_CATS=$scope.PRODUCT_CATS.concat({id:null,name:'Any'});

      $scope.mktgTemplatesVars={
        pageChangeCallback: function () {//used for drPaginationLinks
          $scope.getTemplates();
        },
        pageChangeCallee: true,//used for drPaginationLinks
        rightSidebarIsClosed: false,
      };

      var vanitySubdomain=$rootScope.currentUser.full_name.split(/\s/)[0].toLowerCase(),
          obscuredId=($rootScope.currentUser.id+2400).toString(36);
      $scope.mktgTemplatesVars.insDivBaseURL='https://'+vanitySubdomain+'.insurancedivision.com/'+obscuredId+'/';
      $scope.mktgTemplatesVars.iOBaseUrl='https://'+$location.$$host;

      $scope.searchFields=['medium', 'purpose_id', 'product_category_id', 'planning_category_id','name','page'];
      $scope.readStateFromUrl();
      $scope.getTemplates();
    }

    $scope.readStateFromUrl=function(){
      var params=$location.search();
      for(var i in $scope.searchFields){
        var f=$scope.searchFields[i],
            isDefined=((typeof params[f])!='undefined');
        $scope.mktgTemplatesVars[f]=isDefined ? params[f] : null;
        if(['medium','name'].indexOf(f)==-1 && isDefined){
          $scope.mktgTemplatesVars[f]=parseInt($scope.mktgTemplatesVars[f]);
        }else if(f=='medium' && !isDefined){
          $scope.mktgTemplatesVars[f]='Any';
        }
      }
      if(!$scope.mktgTemplatesVars['page']){
        $scope.mktgTemplatesVars['page']=1;
      }
    }

    $scope.cloneTemplate=function(template){
      var newTemplate=$.extend(true,{},template),
          keyBlacklist=['id','created_at','updated_at','thumb_url'];
      for(var i in keyBlacklist){
        delete newTemplate[ keyBlacklist[i] ];
      }
      newTemplate.owner_id=$rootScope.currentUser.id;
      newTemplate.name=newTemplate.name+'(Copy)';
      $scope.openTemplateForm(newTemplate)
    }

    $scope.openTemplateForm=function(template,medium){
      if(!medium){
        if(template){
          medium=template.medium||'Email';
        }else{
          medium='Email'
        }
      }
      if(!template){
        $scope.origTemplate={
          enabled:true,
          quoter_url_type:2,
          ownership_id:2,
          medium:medium,
          body:''
        };
      }else{
        $scope.origTemplate=template;
      }
      $scope.template=$.extend(true,{},$scope.origTemplate);//work on a copy so form can be reset
      $uibModal.open({
        templateUrl: '/marketing/templates/modal_form_for_'+medium.toLowerCase()+'.html',
        controller : 'MarketingTemplateFormModalCtrl',
        windowClass: 'super-size',
        scope      : $scope
      });
    }

    $scope.openSendEmailModal=function(template){
      $scope.template=template;
      $uibModal.open({
        templateUrl: '/marketing/templates/modal_send_email.html',
        controller : 'MarketingTemplateSendEmailModalCtrl',
        scope      : $scope
      });
    }

    $scope.openSendSmsModal=function(template){
      $scope.template=template;
      $uibModal.open({
        templateUrl: '/marketing/templates/modal_send_sms.html',
        controller : 'MarketingTemplateSendEmailModalCtrl',
        scope      : $scope
      });
    }

    $scope.getTemplates=function(resetPageNum){
      var params = {paginate:true};
      //When filters are changed, page should be reset.
      if(resetPageNum){ $scope.mktgTemplatesVars.page=1 }
      for(var i in $scope.searchFields){
        var fieldName=$scope.searchFields[i];
        params[fieldName]=$scope.mktgTemplatesVars[fieldName];
        $location.search(fieldName,$scope.mktgTemplatesVars[fieldName]);
      }
      $scope.mktgTemplatesVars.templatesLoading=true;

      $rootScope.getTemplateOptions(params);
      $rootScope.templateOptions.$promise.then(function(){
        $scope.templates = $rootScope.templateOptions.templates;
        $scope.mktgTemplatesVars.page=$rootScope.templateOptions.page;
        $scope.mktgTemplatesVars.pageCt=$rootScope.templateOptions.page_count;
        $scope.mktgTemplatesVars.templatesLoading=false;
      })
    }

    $scope.getSearches=function(nameConstraint){
      if(nameConstraint.length<3){ return; }
      $scope.searches=$resource('/reporting/searches.json').query({name:nameConstraint});
    }

    $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller("MarketingTemplateFormModalCtrl", ['$scope', '$rootScope', '$timeout', '$location', '$http', '$resource', '$filter', '$uibModalInstance', 'requestSanitizer', 'bulletinService',
  function ($scope, $rootScope, $timeout, $location, $http, $resource, $filter, $uibModalInstance, requestSanitizer, bulletinService) {
    $scope.init=function(){
      $scope.mktgTemplateFormVars={
        enabledTemplatePurposes:[],
      };

      for(var i in $scope.MARKETING_TEMPLATE_PURPOSES){
        var p=$scope.MARKETING_TEMPLATE_PURPOSES[i]
        if( !p.permission || $rootScope.currentUser.can( p.permission ) ){
          $scope.mktgTemplateFormVars.enabledTemplatePurposes.push( p );
        }
      }
      //This options object is used by the `ui-tinymce` directive.
      $scope.tinymceOptions={
        theme:    "modern",
        plugins:  "media image fullscreen preview code link",
        toolbar1: "undo redo | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image"+
                  " | bold italic | styleselect | fontselect fontsizeselect"+
                  " | help | code | fullscreen preview"
      };

      $scope.MARKETING_DYNAMIC_FIELDS=$scope.MARKETING_DYNAMIC_FIELDS.slice();//make a copy

      if($scope.template.medium=='SMS'){
        var idxOfFieldToRemove;
        $scope.MARKETING_DYNAMIC_FIELDS.forEach(function(field,idx){
          if(field.method_chain=='quoter_url') idxOfFieldToRemove=idx;
        });
        $scope.MARKETING_DYNAMIC_FIELDS.splice(idxOfFieldToRemove,1);
      }
      if( !$rootScope.currentUser.can('motorists_rtt_process') ){
        var idxOfFieldToRemove;
        $scope.MARKETING_DYNAMIC_FIELDS.forEach(function(field,idx){
          if(field.method_chain=='rtt_quote_url') idxOfFieldToRemove=idx;
        });
        $scope.MARKETING_DYNAMIC_FIELDS.splice(idxOfFieldToRemove,1);
      }

      $scope.MARKETING_DYNAMIC_FIELDS=$scope.MARKETING_DYNAMIC_FIELDS.map(function(field){
        field.grouping=$filter('titleCase')( field.grouping );
        if(field.expected_arg_types) debugger
        var argTypes=(field.expected_arg_types||[]).join(', '),
            separator=argTypes.length ? '|' : '';
        field.displayText='{{'+argTypes+separator+field.method_chain+'}}';
        return field;
      });

    }

    $scope.charCountColor=function(){
      var l=$scope.template.body.length,
          color=(l>0   && l<=140 && 'green') ||
                (l>140 && l<=160 && 'goldenrod') ||
                ('red');
      return color;
    }
    $scope.messageCount=function(){
      //Messages over 160 chars get divided into separate messages of 153 chars each.
      return Math.ceil($scope.template.body.length/153);
    }
    $scope.isCharCountOutOfRange=function(){
      var l=$scope.template.body.length;
      return l<1 || l>918;
    }

    $scope.insertDynamicFieldAndResetMenu=function(){
      var bodyInput = $('#marketing_email_template_body');

      $scope.insertAtCursor(bodyInput[0], $scope.mktgTemplateFormVars.dynamicFieldSelection);
      $scope.mktgTemplateFormVars.dynamicFieldSelection=null;
    }

    $scope.insertAtCursor=function(myField, myValue) {
      //IE support
      if (document.selection) {
          myField.focus();
          sel = document.selection.createRange();
          sel.text = myValue;
      }
      //MOZILLA and others
      else if (myField.selectionStart || myField.selectionStart == '0') {
          var startPos = myField.selectionStart;
          var endPos = myField.selectionEnd;
          myField.value = myField.value.substring(0, startPos)
                  + myValue
                  + myField.value.substring(endPos, myField.value.length);
      } else {
          myField.value += myValue;
      }
    }

    $scope.saveTemplate=function(template){
      var transactionId=$rootScope.randomNonZeroId(),
          idString=template.id ? '/'+template.id : '',
          httpMethod=template.id ? 'put' : 'post',
          sanitizedCopy=$.extend(true,{},template),
          keyBlacklist=['created_at','updated','thumb_url','medium'];

      if(template.id){
        keyBlacklist=keyBlacklist.concat(['id','type']);
      }
      requestSanitizer.clean(sanitizedCopy,keyBlacklist,true,true);

      $rootScope.addBulletin({text:'Saving template '+template.name+'.', id:transactionId});
      $http[httpMethod]('/marketing/templates'+idString+'.json', {medium:template.medium, marketing_template:sanitizedCopy })
      .success(function(data, status, headers, config){
        $uibModalInstance.close();
        $rootScope.addBulletin({text:'Successfully saved template.', klass:'success', id:transactionId});
        $scope.getTemplates();
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to save template. '+(data.errors||data.error||''), klass:'danger', id:transactionId});
      })
    }

    $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller("MarketingTemplateSendEmailModalCtrl", ['$scope', '$rootScope', '$location', '$http', '$resource', '$uibModalInstance', 'bulletinService',
  function ($scope, $rootScope, $location, $http, $resource, $uibModalInstance, bulletinService) {
    //Initially show the form with the options coded into the template, but allow a different selection.
    $scope.mktgTemplatesVars.quoter_url_type             =$scope.template.quoter_url_type;
    $scope.mktgTemplatesVars.custom_quoter_url           =$scope.template.custom_quoter_url;
    $scope.mktgTemplatesVars.insurance_division_url_page =$scope.template.insurance_division_url_page;

    $scope.sendEmail=function(confirmed){
      var transactionId=$scope.randomNonZeroId(),
          template=$scope.template,
          sndOpt=$scope.mktgTemplatesVars.sendingOption,
          paramsObj={
            medium:                     template.medium||'Email',
            sending_option:             sndOpt,
            quoter_url_type:            $scope.mktgTemplatesVars.quoter_url_type,
            custom_quoter_url:          $scope.mktgTemplatesVars.custom_quoter_url,
            insurance_division_url_page:$scope.mktgTemplatesVars.insurance_division_url_page
          },
          url='/marketing/templates/'+template.id+(confirmed ? '/deliver.json' : '/gauge_size_of_delivery.json');

      $scope.addBulletin({text:'Preparing to send email(s).', id:transactionId});

      if(sndOpt=='self'){
        if(!$rootScope.currentUser.$primaryEmail){//fallback
          alert('You must have a primary email set to use this option.');
          return;
        }
      }else if(sndOpt=='custom'){
        paramsObj.recipients=$scope.mktgTemplatesVars.customRecipientEmailList;
      }else if(sndOpt=='blast'){
        paramsObj.search_id=$scope.mktgTemplatesVars.searchId;
      }else{//fallback
        alert('You must choose a sending option before attempting to send.');
        return;
      }
      $http.post(url,paramsObj)
      .success(function (data, status, headers, config) {
        if(!confirmed && data.requires_confirmation){
          var confirmMsg='You are about to send out '+data.size+' emails.\nAre you sure you want to do this?';
          var warnMsg='You are attempting to send out '+data.size+' emails.\nTo send a bulk email to 500 or more recipients, '+
                      'you must first enable Bulk Sending in your email configuration (My Account / Email Configuration).';
          $rootScope.removeBulletinById(transactionId);
          if( !data.user_has_bulk_send_capable_account ){
            alert(warnMsg);
            return;
          }
          if( confirm(confirmMsg) ){
            $scope.sendEmail(true);
          }
        }else{//Either confirmed already or does not require confirmation.
          $rootScope.addBulletin({text: data.msg, klass: 'success', id: transactionId});
        }
      })
      .error(function (data, status, headers, config) {
        $rootScope.addBulletin({text: 'Failed to send email. ' + (data.errors || data.msg || ''), klass: 'error', id: transactionId});
      });
    }

    $scope.currentUserMobilePhoneWithCarrier=function(){
      var u=$rootScope.currentUser.contact;
      for(var i in u.phones){
        var p=u.phones[i];
        if(p.phone_type_id==MOBILE_PHONE_TYPE_ID && p.carrier_id){
          return p;
        }
      }
    }

  }
])
.controller("MarketingTemplateSendSmsModalCtrl", ['$scope', '$rootScope', '$location', '$http', '$resource', '$uibModalInstance',
  function ($scope, $rootScope, $location, $http, $resource, $uibModalInstance) {

    $scope.currentUserMobilePhoneWithCarrier=function(){
      var u=$rootScope.currentUser.contact;
      for(var i in u.phones){
        var p=u.phones[i];
        if(p.phone_type_id==MOBILE_PHONE_TYPE_ID && p.carrier_id){
          return p;
        }
      }
    }

    $scope.sendSms=function(){
      //
    }

  }
]);
