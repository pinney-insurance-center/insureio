/* This file holds tools for working with the carrier NMB. */

angular.module('io.nmbUtil', ['io.util', 'DrBulletins','ioConstants'])
.service('NmbUtil', ['$resource', 'Util', 'bulletinService','ioConstantsObject', function ($resource, Util, bulletinService, ioConstantsObject) {
  /* Should the current page load use the NMB quote path? */
  this.isNMB = (consumer) => {
    if (consumer.id) /* For saved records, this module ought only to be used for the NMB brand */
      return consumer.brand_id == ioConstantsObject.BRAND_ID_FOR_NMB; /* const is set in constants.js.erb */
    else /* For new records, this module ought only to be used for the NMB tenant */
      return /^nmb\./.test(document.location.host)
  }

  this.modifyQuoterPath = (quoter, consumer) => {
    /* Add ALQ to end of path */
    quoter.setRequestType(ioConstantsObject.APPLICATION_REQUEST);
    quoter.removePanesFromPath(['COMPARE', 'APPLICATION']);
    quoter.path.push({ label: 'ALQ', name: 'ALQ', viewRates: true, disabled: 'disabled' });
    /* Default citizenship, which is requisite to use ALQ */
    if (!consumer.citizenship_id) consumer.citizenship_id = ioConstantsObject.CITIZENSHIP_US_CITIZEN_ID;
  };

  this.modifyQuoterDurationOptions = (quoter) => {
    const durations_map = ioConstantsObject.DURATIONS.reduce((map, obj) => {
      map[obj.id] = obj;
      return map;
    }, {});
    /* Kludge to use TliDurationOption 17 for IXN's Guaranteed Issue Whole Life. */
    durations_map[17] = Object.assign({}, durations_map[17], {
      name: 'Guaranteed Issue Whole Life',
      ixn_code: 'Guaranteed Issue Whole Life',
    });
    quoter.durations = Object.keys(NMB_PRODUCTS).map((id) => {
      return durations_map[id];
    });
  };

  this.loadAlq = (consumer, kase, quote, opts) => {
    try {
      let alq = new this.Alq(consumer, kase, quote, opts);
      alq.load();
      return alq;
    } catch (ex) {
      console.warn(`Exception in loadAlq: ${ex}`);
      if (ex instanceof String)
        bulletinService.add({ klass: 'danger', text: ex.valueOf() });
      else
        throw ex
      return false;
    }
  };

  this.Alq = class { /* util for IXN's "Agency Life Quoter" */
    constructor (consumer, kase, quote, opts) {
      this.consumer = consumer;
      this.finance = consumer.financial_info_attributes;
      this.health = consumer.health_info_attributes;
      this.kase = kase;
      this.quote = quote;
      this.useSandbox = opts && opts.useSandbox;
      if (this.kase && this.kase.id)
        this.involvedParties = $resource(`/crm/cases/${this.kase.id}/involved_parties.json`).get();
      else
        this.involvedParties = {$promise: Promise.resolve()};
    }

    load () {
      return this.involvedParties.$promise.then(() => {
        window.IXN_QUOTER_CONFIG = this.useSandbox ? this.sandboxConfig() : this.config();
        let domain = this.useSandbox ? 'sand-alq.ixn.tech' : 'alq.ixn.tech';
        addDOMElement('link', { href: `https://${domain}/css/reset.css`, rel: 'stylesheet', type: "text/css" });
        addDOMElement('script', { src: `https://${domain}/js/alq.widget.js`, type: "text/javascript" });
      });
    }

    /* Contains an ID for a sandbox ALQ for experimenting */
    sandboxConfig () {
      return Object.assign(this.config(), { id: 'e15d1c0a-cf93-44b7-9463-be17af7e6d8c' });
    }

    config () {
      this.analyze();
      let obj = {
        id: '387467d1-8b41-4f4f-afd5-80c6b61f8c80', /* ID for our _NMB_ ALQ config */
        carrier_ids: [1800], /* IXN uses this id to identify NMB */
        product_ids: Object.values(NMB_PRODUCTS).map(x => x[0]),
        product_types: Object.values(NMB_PRODUCTS).map(x => x[1]),
        agent: {},
        client: {
          first_name: Util.firstName(this.consumer.full_name),
          last_name: Util.lastName(this.consumer.full_name),
        },
        quote_data: this.quote_data(),
        quote_params: this.quote_params(),
      };
      return obj;
    }

    quote_data () {
      let address = this.consumer.$primaryAddressAttributes;
      let state_id = address && address.state_id;
      let birth = this.consumer.birth_or_trust_date && new Date(Date.parse(this.consumer.birth_or_trust_date));
      let quoteData = {
        gender: Util.genderString(this.consumer.gender),
        state: Util.enum('STATES', state_id, 'abbrev'),
        dob_day: birth.toLocaleString('en-US', { day: '2-digit' }),
        dob_month: birth.toLocaleString('en-US', { month: '2-digit' }),
        dob_year: birth.toLocaleString('en-US', { year: 'numeric' }),
        tobacco: this.isTobacco,
        face_amount: this.quote.face_amount,
        child_rider_units: this.extGet('child_rider_units') || 0,
        health_categories: ixnHealthClassNames(this.klass),
        product_types: [this._products()[1]],
        carrier_ids: [1800], /* IXN uses this id to identify NMB */
      };
      ['adb_rider',
      'adb_rider_annual',
      'adb_rider_monthly',
      'adb_rider_quarterly',
      'adb_rider_semi_annual',
      'child_rider_annual',
      'child_rider_monthly',
      'child_rider_quarterly',
      'child_rider_semi_annual',
      'child_wop_rider_annual',
      'child_wop_rider_monthly',
      'child_wop_rider_quarterly',
      'child_wop_rider_semi_annual',
      'wop_rider_annual',
      'wop_rider_monthly',
      'wop_rider_quarterly',
      'wop_rider_semi_annual',
      ].forEach(extKey => quoteData[extKey] = this.extGet(extKey));
      return quoteData;
    }

    quote_params () {
      let middleName = Util.middleName(this.consumer.full_name);
      let address = this.consumer.$primaryAddressAttributes;
      let phones = this.consumer.phones_attributes || [];
      let maritalStatusName = Util.enum('MARITAL_STATUSES', this.consumer.marital_status_id, 'name');
      let isEmployed = this.consumer.actively_employed || !!( (this.consumer.occupation || this.consumer.occupation_description || '').trim() );
      /* Return */
      let outObj = {
        insureio_consumer_id: this.consumer.id,
        insureio_case_id: this.kase && this.kase.id,
        insureio_quote_id: this.quote && this.quote.id,
        insureio_health_class_id: insureioHealthClassId(this.klass),
        middle_initial: middleName && middleName[0],
        address: address && address.street,
        city: address && address.city,
        zip: address && address.zip,
        time_at_address: this.consumer.years_at_address && this.consumer.years_at_address.toFixed(0),
        previous_address: this.extGet('prev_address'),
        phone_primary: phones[0] && phones[0].value,
        phone_secondary: phones[1] && phones[1].value,
        email: this.consumer.$primaryEmailAttributes && this.consumer.$primaryEmailAttributes.value,
        ssn: this.consumer.ssn && this.consumer.ssn.replace(/\D/g, ''),
        citizenship_dl_number: this.consumer.dln,
        citizenship_dl_state: Util.enum('STATES', this.consumer.dl_state_id, 'abbrev'),
        citizenship_permanent_resident: this.consumer.citizenship_id == 2,
        citizenship_id_number: this.consumer.citizenship_noncitizen_registration_number,
        citizenship_date_of_entry: this.extGet('nmb_us_entry_date'),
        tobacco_last_used: this.analysis.tobacco.last,
        tobacco_products_used: Object.keys(this.analysis.tobacco.typesUsed).join(','),
        height_ft: this.health.feet,
        height_in: this.health.inches || "0", /* IXN treats zero as null */
        weight: this.health.weight,
        marital_status: maritalStatusName && maritalStatusName.toLowerCase(),
        employment_employed: isEmployed ? 'yes' : 'no',
        employment_occupation: this.consumer.occupation || this.consumer.occupation_description,
        employment_annual_income: this.finance.annual_income,
        employment_unemployment_reason: this.extGet('unemployment_reason'),
        billing_frequency: this._premiumMode(),
        referrer: 'Insureio.com',
        /* Diseases */
        diagnosed_heart_condition_stroke_cancer: this.analysis.disease.hasHeartStrokeCancer ? 'yes' : 'no',
        diagnosed_heart_condition_stroke_cancer_details: arr2Str(this.analysis.disease.heartStrokeCancerDetail),
        /* Driving history */
        operating_under_influence: this.analysis.movingViolation.dui ? 'yes' : 'no',
        operating_under_influence_details: arr2Str(this.analysis.movingViolation.duiDetails),
        moving_violation_suspended_revoked_license_last_3_years: this.analysis.movingViolation.suspended ? 'yes' : 'no',
        moving_violation_suspended_revoked_license_last_3_years_details: arr2Str(this.analysis.movingViolation.suspensionDetails),
        /* Criminal history */
        convicted_of_felony: this.analysis.crime.felony ? 'yes' : 'no',
        convicted_of_felony_details: arr2Str(this.analysis.crime.felonyDetails),
        pending_felony_charge: this.analysis.crime.felonyPending ? 'yes' : 'no',
        on_probation: this.analysis.crime.probation ? 'yes' : 'no',
        /* Family history */
        prior_to_60_parent_death: this.analysis.parentDeath ? 'yes' : 'no',
        prior_to_60_parent_death_details: arr2Str(this.analysis.parentDeath),
        prior_to_60_sibling_death: this.analysis.siblingDeath ? 'yes' : 'no',
        prior_to_60_sibling_death_details: arr2Str(this.analysis.siblingDeath),
        /* Finance */
        recent_bankruptcy: this.analysis.recentBankruptcy ? 'yes' : 'no',
        recent_bankruptcy_details: this.analysis.recentBankruptcy,
        /* Travel */
        international_travel: this.analysis.travel || this.health.foreign_travel ? 'yes' : 'no',
        international_travel_details: this.analysis.travel || null,
        /* Misc */
        agent_years_knowing_pi: this.consumer.relationship_to_agent_start,
        purpose_of_insurance: this.kase && Util.enum('PURPOSES', this.kase.purpose_id, 'name'),
        insurance_on_pi_spouse: this.extGet('nmb_spouse_coverage_amt'),
        family_member_names_with_application: this.extGet('nmb_family_w_apps'),
      };
      /* Beneficiaries */
      this.beneficiaries().forEach(bene => { Object.assign(outObj, bene) });
      /* IXN wants an array of { name: ..., value: ... } */
      return Object.entries(outObj).map((tuple) => { return { name: tuple[0], value: tuple[1] } });
    }

    _premiumMode () {
      let id = this.quote.premium_mode_id;
      switch (id) {
        case 1: return 'annual';
        case 2: return 'semi_annual';
        case 3: return 'quarterly';
        case 4: return 'monthly';
        case null: return null;
        throw new Error(`Unrecognized premium_mode_id ${id} for NMB`);
      }
    }

    _products () {
      let id = this.quote.duration_id;
      if (id == null) return null;
      let out = NMB_PRODUCTS[id];
      if (out == null) throw new Error(`Unrecognized duration_id ${id} for NMB products`);
      return out;
    }

    /* Ensure that `this.klass` is no better than `newBest` */
    limitRate (newBestRate) {
      this.klass = Math.max(newBestRate, this.klass);
      if (newBestRate == RATINGS.STD_TOBACCO) this.isTobacco = true;
    }

    extGet (key) { return this.kase && this.kase.ext && this.kase.ext[key] }

    /* Compute NMB health class and a few items used in `this.quote_params` */
    analyze () {
      /* Validate consumer presence */
      if (!this.consumer.$resolved) throw new String('Awaiting records. Please try again in 2 seconds.');
      /* Validate Citizenship */
      if (this.consumer.citizenship_id != ioConstantsObject.CITIZENSHIP_US_CITIZEN_ID)
        { throw new String('Products from this carrier are available only to U.S. citizens.') }
      /* Validate Birth */
      if (Util.invalidDate(this.consumer.birth_or_trust_date))
        { throw new String('Birth must be a date.') }
      this.age = Util.yearsSince(this.consumer.birth_or_trust_date);
      this.analysis = {};
      this.isTobacco = false;
      this.klass = RATINGS.SUPER;
      /* Arbitrary rule from NMB: rating is no better than STD for low-face-amt cases */
      if (this.quote.face_amount < (100*1000)) this.limitRate(RATINGS.STD);
      /* Compile tobacco data for ALQ */
      this.analysis.tobacco = { typesUsed: {} };
      if (this.health.tobacco) {
        this.analysis.tobacco = Object.keys(this.health).reduce((out, key) => {
          let keyMatch = /(cigarette|cigar|pipe|chewed|patch_or_gum)/.exec(key);
          if (!keyMatch) return out; /* Consider only "tobacco" fields */
          let type = keyMatch[1];
          let value = this.health[key];
          if (/last/.test(key) && value) {
            let date = new Date(value);
            if (!out.last || out.last < date) out.last = date;
            let yearsSince = Util.yearsSince(date);
            if (yearsSince < 1) { this.limitRate(RATINGS.STD_TOBACCO) }
            if (yearsSince < 2) { this.limitRate(RATINGS.STD) }
            if (yearsSince < 3) { this.limitRate(RATINGS.PREFERRED) }
            if (yearsSince <= 7) out.typesUsed[type] = true;
          } else if (/current/.test(key) && value) {
            out.typesUsed[type] = true;
            this.limitRate(RATINGS.STD_TOBACCO);
          }
          return out;
        }, this.analysis.tobacco);
      }
      /* Cholesterol */
      if (this.health.cholesterol) {
        if (this.health.cholesterol_level > 300) { this.limitRate(RATINGS.T1) }
        if (this.health.cholesterol_level > 270) { this.limitRate(RATINGS.STD) }
        if (this.health.cholesterol_level > 240) { this.limitRate(RATINGS.PREFERRED) }
        /* HDL ratio */
        if (this.health.cholesterol_hdl > 7) { this.limitRate(RATINGS.T1) }
        if (this.health.cholesterol_hdl > 5.5) { this.limitRate(RATINGS.STD) }
        if (this.health.cholesterol_hdl > 5) { this.limitRate(RATINGS.PREFERRED) }
      }
      /* Blood pressure */
      if (this.health.bp) {
        if (this.age < 61) {
          if (this.health.bp_systolic > 150) { this.limitRate(RATINGS.T1) }
          if (this.health.bp_systolic > 145) { this.limitRate(RATINGS.STD) }
          if (this.health.bp_systolic > 140) { this.limitRate(RATINGS.PREFERRED) }
          if (this.health.bp_diastolic > 90) { this.limitRate(RATINGS.T1) }
          if (this.health.bp_diastolic > 85) { this.limitRate(RATINGS.PREFERRED) }
        } {
          if (this.health.bp_systolic > 155) { this.limitRate(RATINGS.T1) }
          if (this.health.bp_systolic > 150) { this.limitRate(RATINGS.STD) }
          if (this.health.bp_systolic > 145) { this.limitRate(RATINGS.PREFERRED) }
          if (this.health.bp_diastolic > 90) { this.limitRate(RATINGS.T1) }
        }
      }
      /* Compile family history for ALQ */
      let familyHistory = this.health.relatives_diseases.reduce((out, disease) => {
        let deathAge = disease.death_age === null ? undefined : disease.death_age; /* Use `undefined` instead of `null` b/c `null` has a numeric value */
        if (deathAge) out.nKilled += 1;
        if (deathAge < 60) { /* `undefined` will return false for any comparison */
          let details = null;
          if (!out[disease.relationship_type_id]) out[disease.relationship_type_id] = [];
          for (let key in disease) {
            if (key == 'true') continue;
            let value = disease[key];
            if (value != null) out[disease.relationship_type_id].push(value.toString());
          }
        }
        return out;
      }, { death: {}, nKilled: 0 });
      this.analysis.parentDeath = familyHistory.death[ioConstantsObject.RELATIONSHIP_TYPE_ID_PARENT];
      this.analysis.siblingDeath = familyHistory.death[ioConstantsObject.RELATIONSHIP_TYPE_ID_SIBLING];
      if (this.age < 65) { /* For NMB health class, disregard if PI is 65 or older */
        if (familyHistory.nKilled > 1) { this.limitRate(RATINGS.T1) }
        if (familyHistory.nKilled > 0) { this.limitRate(RATINGS.STD) }
      }
      /* Compile disease-related data for ALQ */
      this.analysis.disease = Object.keys(this.health.diseases).reduce((out, key) => {
        let value = this.health.diseases[key];
        let match = /stroke|cardio|heart|cancer|noma/.test(key);
        if (match && value && key != 'basal_cell_carcinoma') {
          let {type_id, ...attrs} = value;
          if (Object.keys(attrs).length) {
            out.hasHeartStrokeCancer = true;
            delete attrs.true;
            if (/cancer|noma/.test(key)) this.limitRate(RATINGS.STD); /* Cancer */
            if (Object.keys(attrs).length) out.heartStrokeCancerDetail.push(`${key}:${JSON.stringify(attrs)}`);
          }
        }
        return out;
      }, { heartStrokeCancerDetail: [] });
      /* Compile driving history */
      this.analysis.movingViolation = this.health.moving_violations.reduce((out, incident) => {
        let yearsSince = Util.yearsSince(incident.date);
        if (yearsSince < 3) out.ct += 1;
        if (incident.dui_dwi) {
          out.dui = true;
          incident.date && out.duiDetails.push(incident.date.toISOString());
          incident.detail && out.duiDetails.push(incident.detail);
          if (yearsSince < 3) this.limitRate(RATINGS.T1);
          if (yearsSince < 5) this.limitRate(RATINGS.STD);
          if (yearsSince < 7) this.limitRate(RATINGS.PREFERRED);
        }
        if (incident.dl_suspension && yearsSince < 3) {
          out.suspended = true;
          out.suspensionDetails.push(incident.date.toISOString());
          incident.detail && out.suspensionDetails.push(incident.detail);
        }
        return out;
      }, { duiDetails: [], suspensionDetails: [], ct: 0 });
      if (this.analysis.movingViolation.ct > 3) this.limitRate(RATINGS.T1);
      if (this.analysis.movingViolation.ct > 2) this.limitRate(RATINGS.STD);
      /* Alcohol / substance abuse */
      ['alcohol_abuse', 'drug_abuse'].forEach((abuse) => {
        let disease = this.health.diseases[abuse];
        if (!disease || !disease.treatment_end_date) return;
        let sinceTreatment = Util.yearsSince(disease.treatment_end_date);
        if (sinceTreatment < 5) { this.limitRate(RATINGS.T1) }
        if (sinceTreatment < 7) { this.limitRate(RATINGS.STD) }
        if (sinceTreatment < 10) { this.limitRate(RATINGS.PREFERRED) }
      });
      /* Compile criminal data for ALQ */
      let now = new Date();
      this.analysis.crime = this.health.crimes.reduce((out, event) => {
        if (event.felony) {
          out.felony = true;
          event.date && out.felonyDetails.push(event.date);
          event.detail && out.felonyDetails.push(event.detail);
          if (event.pending) out.felonyPending = true;
          if (Util.yearsSince(event.date) < 10) this.limitRate(RATINGS.STD);
        }
        if (event.probation_end && new Date(event.probation_end) > now)
          out.probation = true;
        return out;
      }, { felonyDetails: [] });
      /* Body build */
      let gender = this.consumer.gender;
      if (gender instanceof String) gender = JSON.parse(gender);
      if (gender != ioConstantsObject.MALE && gender != ioConstantsObject.FEMALE) {
        throw new String('Gender must be set');
      }
      if (!this.health.feet) throw new String('Height (feet) must be set')
      if (!this.health.weight) throw new String('Weight must be set')
      var height = Math.round(this.health.feet * 12 + this.health.inches);
      var weightIdx = height - 56;
      for (var klass = RATINGS.SUPER; klass <= RATINGS.STD; klass ++) {
        var weightMax = MAX_WEIGHTS[!!gender][klass][weightIdx];
        if (this.health.weight > weightMax) { this.limitRate(1 + klass) }
      }
      /* Check minimum weight if PI is age 65+ */
      if (this.age >= 65) {
        var weightMin = MIN_WEIGHTS[!!gender][weightIdx];
        if (this.health.weight < weightMin) { this.limitRate(RATINGS.T1) }
      }
      /* Compile financial info for ALQ */
      let yearsSinceBankruptcy = Util.yearsSince(this.finance.bankruptcy_declared);
      this.analysis.recentBankruptcy = yearsSinceBankruptcy < 5 && this.finance.bankruptcy_declared;
      /* Compile travel info for ALQ */
      this.health.travel_country = this.health.travel_country_id && Util.enum('COUNTRIES', health.travel_country_id, 'name');
      this.analysis.travel = ['duration', 'when', 'country_id', 'country_detail']
      .reduce((out, name) => {
        let key = `travel_${name}`
        let value = this.health[key];
        if (value) out.push(`${name}: ${value}`);
        return out;
      }, []).join("\t");
      delete this.health.travel_country;
      /* Whole-life applications are all STANDARD rate, but we still want the
      /* foregoing health analysis because it provides data which we send to
      /* integration partners */
      if (this.quote.duration_id == 17) this.klass = RATINGS.STD;
    }

    /* Get primary or contingent beneficiaries */
    beneficiaries () {
      let stakeholders = this.involvedParties.stakeholder_relationships;
      if (!stakeholders) return [];
      let number = { primary: 1, contingent: 1 };
      return stakeholders
      .filter(s => s.is_beneficiary)
      .map(s => {
        /* Labels */
        let bene = s.stakeholder;
        let label = s.contingent ? 'contingent' : 'primary';
        let i = number[label];
        number[label]++;
        /* Names */
        let middleName = Util.middleName(bene.full_name);
        let middleInitial = middleName && middleName[0];
        /* Return object which can be merged into quote params */
        return {
          [`${label}_beneficiary_${i}_first_name`]: Util.firstName(bene.full_name),
          [`${label}_beneficiary_${i}_middle_initial`]: middleInitial,
          [`${label}_beneficiary_${i}_last_name`]: Util.lastName(bene.full_name),
          [`${label}_beneficiary_${i}_gender`]: Util.genderString(bene.gender),
          [`${label}_beneficiary_${i}_date_of_birth`]: formatDate(bene.birth_or_trust_date),
          [`${label}_beneficiary_${i}_relationship`]: relationshipName(s.relationship_type_id, bene.gender),
          [`${label}_beneficiary_${i}_percentage`]: s.percentage,
        }
      });
    }
  }

  function addDOMElement (tag, attrs) {
    /* Remove matching existing element if any */
    let selector = Object.keys(attrs).reduce((out, key) => {
      return out + `[${key}="${attrs[key]}"]`;
    }, tag);
    let elem = document.querySelector(selector);
    if (elem) elem.parentNode.removeChild(elem);
    /* Add new element */
    elem = document.createElement(tag);
    for (let key in attrs) { elem[key] = attrs[key] }
    document.body.appendChild(elem);
  }

  function arr2Str (array, delim) {
    return array && array.map((x) => { return x.toString() }).join(delim||'\t') || null
  }

  function formatDate (date) {
    return date && date.toLocaleString('en-US', {day: '2-digit', month: '2-digit', year: 'numeric'});
  }

  function isDiseaseTruthy (disease) {
    if (!disease) return false;
    for (let field in disease) {
      if (field != 'type_id' && disease[field]) return true;
    }
    return false;
  }

  /* Return id for Insureio health class*/
  function insureioHealthClassId (klass) {
    switch (klass) {
      case RATINGS.SUPER: return 1;
      case RATINGS.PREFERRED: return 2;
      case RATINGS.STD: return 4;
      case RATINGS.STD_TOBACCO: return 6;
      case RATINGS.T1: return 7;
    }
    throw new Error('nmb-util.js ERR. Class is ' + klass);
  }

  /* Translate int-type klass into an IXN string */
  function ixnHealthClassNames (klass) {
    switch (klass) {
      case RATINGS.SUPER: return ['Preferred Plus'];
      case RATINGS.PREFERRED: return ['Preferred'];
      case RATINGS.STD: return ['Standard'];
      case RATINGS.STD_TOBACCO: return ['Standard'];
      case RATINGS.T1: return TABLE_NAMES;
    }
    throw new Error('nmb-util.js ERR. Class is ' + klass);
  }

  /* Translate int-type klass into an NMB string */
  function nmbHealthClassName (klass) {
    switch (klass) {
      case RATINGS.SUPER: return 'Super Preferred No Tobacco';
      case RATINGS.PREFERRED: return 'Preferred No Tobacco';
      case RATINGS.STD: return 'Standard No Tobacco';
      case RATINGS.STD_TOBACCO: return 'Standard Tobacco';
      case RATINGS.T1: return 'Table (need to contact underwriter)';
    }
    throw new Error('nmb-util.js ERR. Class is ' + klass);
  }

  /* Translate Insureio relationship to IXN relationship */
  function relationshipName (relationshipTypeId, gender=MALE) {
    switch (relationshipTypeId) {
      case 1:
      case 9:
        return gender == ioConstantsObject.MALE ? 'father' : 'mother';
      case 2:
      case 8:
        return gender == ioConstantsObject.MALE ? 'brother' : 'sister';
      case 3: return 'spouse';
      case 4:
      case 5:
      case 11:
      case 12:
      case 13:
        return 'other';
      case 6:
      case 10:
        return 'child';
      case 7: return 'grandchild';
    }
  }

  /* Map IO>TliDurationId => [IXN>productId, IXN>productType] */
  const NMB_PRODUCTS = {
    2: [2440, '15 Year Term'],
    3: [2441, '20 Year Term'],
    5: [2442, '30 Year Term'],
    17: [2445, 'Whole Life'],
  }

  /* An array of the table names used by IXN */
  let TABLE_NAMES = []; /* cf. https://github.com/ienetwork/code_examples/blob/master/agency_life_quoter/advanced.html */
  for (let x = 'A'; x <= 'P'; x = String.fromCharCode(1 + x.charCodeAt(0)))
    { TABLE_NAMES.push(`Table ${x}`) }

  const RATINGS = {
    SUPER:       0, /* Super Preferred No Tobacco */
    PREFERRED:   1, /* Preferred No Tobacco */
    STD:         2, /* Standard No Tobacco */
    STD_TOBACCO: 3, /* Standard Tobacco */
    T1:          4,
  };

  const MIN_WEIGHTS_MALE = Object.freeze([83,85,88,92,96,99,103,107,110,114,117,121,123,128,130,134,137,142,145,149,152,157,161,165,169]);
  const MIN_WEIGHTS_FEMALE = Object.freeze([82,84,86,89,92,95,98,101,105,108,111,115,118,122,125,129,132,136,140,144,148,151,155,159,164]);
  const MAX_WEIGHTS_MALE_SUPER = Object.freeze([136,139,142,148,156,159,163,168,173,177,182,189,195,200,205,210,215,220,226,232,238,245,252,259,266]); /* Super Preferred No Tobacco */
  const MAX_WEIGHTS_MALE_PREFERRED = Object.freeze([140,145,150,155,160,164,169,174,179,184,189,195,200,206,211,217,223,229,235,241,247,253,259,265,271]); /* Preferred No Tobacco */
  const MAX_WEIGHTS_MALE_STD = Object.freeze([160,166,172,178,184,190,196,202,208,214,220,227,233,239,244,250,256,263,269,276,283,289,296,303,310]); /* Standard No Tobacco */
  const MAX_WEIGHTS_FEMALE_SUPER = Object.freeze([121,124,127,133,141,144,148,153,158,162,167,174,180,185,190,195,200,205,211,217,223,230,237,244,251]); /* Super Preferred No Tobacco */
  const MAX_WEIGHTS_FEMALE_PREFERRED = Object.freeze([136,140,144,149,154,159,164,169,174,179,184,189,195,200,205,210,216,222,228,233,239,245,250,256,262]); /* Preferred No Tobacco */
  const MAX_WEIGHTS_FEMALE_STD = Object.freeze([155,159,164,169,175,181,187,193,197,202,206,210,215,220,225,230,236,242,248,254,260,267,274,281,287]); /* Standard No Tobacco */
  /* Minimum weight requirements for age 65+, starting at a height of 56", ending at a height of 80" */
  const MIN_WEIGHTS = {};
  MIN_WEIGHTS[ioConstantsObject.MALE] = MIN_WEIGHTS_MALE;
  MIN_WEIGHTS[ioConstantsObject.FEMALE] = MIN_WEIGHTS_FEMALE;
  /* Maximum weight requirements, starting at a height of 56", ending at a
  /* height of 80". Organize into nested objects in order to reduce the amount
  /* of logic that needs to be performed in `chooseHealthClass` */
  const MAX_WEIGHTS = {};
  MAX_WEIGHTS[ioConstantsObject.MALE] = {};
  MAX_WEIGHTS[ioConstantsObject.MALE][RATINGS.SUPER] = MAX_WEIGHTS_MALE_SUPER;
  MAX_WEIGHTS[ioConstantsObject.MALE][RATINGS.PREFERRED] = MAX_WEIGHTS_MALE_PREFERRED;
  MAX_WEIGHTS[ioConstantsObject.MALE][RATINGS.STD] = MAX_WEIGHTS_MALE_STD;
  MAX_WEIGHTS[ioConstantsObject.FEMALE] = {};
  MAX_WEIGHTS[ioConstantsObject.FEMALE][RATINGS.SUPER] = MAX_WEIGHTS_FEMALE_SUPER;
  MAX_WEIGHTS[ioConstantsObject.FEMALE][RATINGS.PREFERRED] = MAX_WEIGHTS_FEMALE_PREFERRED;
  MAX_WEIGHTS[ioConstantsObject.FEMALE][RATINGS.STD] = MAX_WEIGHTS_FEMALE_STD;
}])
;
