angular.module('DrModels', ['ioConstants', 'ioRequestSanitizer'])
.provider('FinanceModel', function () {
  this.$get = ['$resource', function ($resource) {
    var FinanceModel = $resource('/crm/financial_infos/:id.json');
    Object.defineProperties(FinanceModel.prototype, {
      getNetWorth: {
        value: function () {
          if (this.net_worth_use_specified)
            return this.net_worth_specified;
          else {
            var netWorth = 0;
            netWorth += this.assets || 0;
            netWorth -= this.liabilities || 0;
            return netWorth;
          }
        }
      },
    });
    return FinanceModel;
  }];
})
.provider('UserModel', function UserModelProvider () {
  this.$get = ["$resource", '$rootScope', 'ioConstantsObject', 'requestSanitizer', function UserModelFactory ($resource, $rootScope, ioConstantsObject, requestSanitizer) {
    var UserModel = $resource('/users/:id.json',{id:'@id'}, {
      get: {
        method: 'GET',
        transformResponse: function(dataString, headers,statusCode) {
          var obj = dataString ? JSON.parse(dataString) : {};
          obj.$headers=headers();
          obj.$statusCode=statusCode;
          obj.$personType= obj.is_recruit ? 'Recruit' : 'User';
          obj.$agentOfRecord={id:obj.agent_of_record_id, full_name:obj.agent_of_record_name};
          if( !obj.id ){ return obj; }//When not logged in, "/users/0.json" returns an empty object.
          //Other dates can stay in ISO8601 format,
          //but this one uses a different type of date picker.
          if(obj.birth_or_trust_date){
            obj.birth_or_trust_date= moment(obj.birth_or_trust_date).format('MM/DD/YYYY');
          }
          $rootScope.setPointersToPrimaryContactMethods(obj);
          convertInputs(obj);
          return obj;
        }
      },
      getApiKey:{
        method:'GET', isArray: false,
        url:'/users/show_my_api_key.json'
      },
      getRecruitForUser:{
        method:'GET', isArray: false,
        url:'/users/:user_id/recruit_for_user.json',
        transformResponse: function(dataString, headers,statusCode) {
          var obj = dataString ? JSON.parse(dataString) : {};
          obj.$personType= 'Recruit';
          obj.$headers=headers();
          obj.$statusCode=statusCode;
          convertInputs(obj);
          return obj;
        }
      },
      update: {
        method: 'put', isArray: false,
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},{user:data});
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          return JSON.stringify(dataCopy);
        }
      },
      updateWithCustomParams: {
        method: 'put', isArray: false
      },
      create: {
        method: 'post',
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},{user:data});
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          return JSON.stringify(dataCopy);
        }
      }
    });
    Object.defineProperties(UserModel.prototype, {
      //Override instance functions to call the class functions instead.
      //This keeps the calling user object intact and returns a new object.
      $getApiKey:{value:function(successCb,errorCb){
        var id=this.id,
            password=this.password;
        return UserModel.getApiKey(
          {password:password},
          function(data){
            if((typeof successCb)=='function') successCb(data);
          },
          function(data){
            if((typeof errorCb)=='function') errorCb(data);
          }
        );
      }},
      $getRecruitForUser:{value:function(successCb,errorCb){
        var userId=this.id;
        return UserModel.getRecruitForUser(
          {user_id:userId},
          function(data){
            if((typeof successCb)=='function') successCb(data);
          },
          function(data){
            if((typeof errorCb)=='function') errorCb(data);
          }
        );
      }},
      $updateWithCustomParams:{value:function(params,successCb,errorCb){
        params=$.extend(params,{id:this.id});
        return UserModel.updateWithCustomParams(params,
          function(data){
            if((typeof successCb)=='function') successCb(data);
          },
          function(data){
            if((typeof errorCb)=='function') errorCb(data);
          }
        );
      }},
      // Get or set a single permission
      can: {
        value: function (permissionName, newVal, strict) {
          /* When no user is logged in, do not try to get or set permissions */
          if (this.id == null || !this.$resolved) { return false }
          var i = ioConstantsObject.PERMISSIONS.indexOf(permissionName);
          if (i < 0){
            console.error('Unknown user permission "'+permissionName+'"');
            return false;
          }
          if (newVal==undefined || newVal == null){
            // JS can't do bitwise read on bits higher than the 32nd for some reason,
            // so we need to chop everything down to 32 bits.
            var fieldName   = 'permissions_' + Math.floor(i / 32); // how many 32-bit sequences we need to chop off
            var permissions = this[fieldName];
            if (permissions == null){
              console.error('Unknown permissions field '+fieldName+'. This indicates a problem with permission '+permissionName+'.');
              return false;
            }
            i = i % 32;
            if (permissions & (1<<i)) return true;
            if (!strict && permissionName != 'super_edit' && this.can('super_edit')) return true;
            if (!strict && permissionName != 'super_view' && /view_/.test(permissionName) && this.can('super_view')) return true;
            return false;
          } else if (newVal) {
            this.permissions |= i;//set bit to true by ORing with the bitmask
          } else {
            this.permissions &= ~i;//set bit to false by ANDing with the bitmask
          }
        }
      }
    });
    return UserModel;
  }];
}) // end provider for UserModel
.provider('ConsumerModel', function ConsumerModelProvider () {
  this.$get = ["$resource", '$rootScope', 'requestSanitizer', function ConsumerModelFactory ($resource, $rootScope, requestSanitizer) {
    var ConsumerModel = $resource('/consumers/:id.json',{id:'@id'}, {
      get: {
        method: 'GET',
        transformResponse: function(dataString, headers,statusCode) {
          var obj = dataString ? JSON.parse(dataString) : {};
          obj.$headers=headers();
          obj.$statusCode=statusCode;
          obj.$personType= 'Consumer';
          //Other dates can stay in ISO8601 format,
          //but this one uses a different type of date picker.
          if(obj.birth_or_trust_date){
            obj.birth_or_trust_date= moment(obj.birth_or_trust_date).format('MM/DD/YYYY');
          }
          obj.$primary_contact_name=obj.primary_contact_name;
          delete obj.primary_contact_name;
          obj.financial_info_attributes=obj.financial_info||{};
          delete obj.financial_info;
          obj.health_info_attributes=obj.health_info||{};
          delete obj.health_info;
          obj.health_info_attributes.family_diseases_attributes=obj.health_info_attributes.family_diseases||[];
          delete obj.health_info_attributes.family_diseases;
          $rootScope.setPointersToPrimaryContactMethods(obj);
          convertInputs(obj);
          return obj;
        }
      },
      update: {
        method: 'put', isArray: false,
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},{consumer: data});
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          return JSON.stringify(dataCopy);
        }
      },
      updateWithCustomParams: {
        method: 'put', isArray: false
      },
      create: {
        method: 'post',
        transformRequest: function (data){
          var dataCopy=$.extend(true,{},{consumer: data});
          requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
          return JSON.stringify(dataCopy);
        }
      }
    });
    Object.defineProperties(ConsumerModel.prototype, {
      $updateWithCustomParams:{
        value:function(params,successCb,errorCb){
          params=$.extend(params,{id:this.id});
          //Override instance function to call the class function instead.
          return ConsumerModel.updateWithCustomParams(params,
            function(data){
              if((typeof successCb)=='function') successCb(data);
            },
            function(data){
              if((typeof errorCb)=='function') errorCb(data);
            }
          );
        }
      }
    });
    return ConsumerModel;
  }];
}) // end provider for ConsumerModel
;
