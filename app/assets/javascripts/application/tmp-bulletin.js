angular.module('tmpBulletin', [
  'DrBulletins',
])
.run(['bulletinService', function (bulletinService) {
  const now = new Date();
  /* Do not show bulletin if it has been seen in the last 24 hours */
  const oneDayAgo = now -  24 * 60 * 60 * 1000;
  const LOCAL_STORAGE_KEY = 'tmpBulletinTimestamp';
  const timeString = localStorage.getItem(LOCAL_STORAGE_KEY);
  if (timeString && new Date(timeString) > oneDayAgo) return;
  /* Determine the message text */
  let text = null;
  if (now < new Date(2020, 7, 6))
    text = 'ATTN: Insureio has scheduled downtime for maintenance for 1-2 hours on 6 August 2020 at 9:00 pm PDT';
  /* Display bulletin */
  if (text) {
    localStorage.setItem(LOCAL_STORAGE_KEY, new Date().toISOString());
    bulletinService.add({klass: 'warning', text: text});
  }
}]);
