
  angular.module("noteApp", ['RestfulResource',  'ui.bootstrap'])
.factory("Note", ['Resource','requestSanitizer',
  function ($resource,requestSanitizer) {
      return $resource('/notes/:id.json',
        { id: '@id', notable_type:'@notable_type', notable_id:'@notable_id', note_type_name:'@note_type'},
        {
          update: {
            method: 'put', isArray: false,
            transformRequest: function (data){
              var dataCopy=$.extend(true,{},{note: data});
              requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
              return JSON.stringify(dataCopy);
            }
          },
          updateWithCustomParams: {
            method: 'put', isArray: false
          },
          create: {
            method: 'post',
            transformRequest: function (data){
              var dataCopy=$.extend(true,{},{note: data});
              requestSanitizer.clean(dataCopy,['created_at','updated_at'],true);
              return JSON.stringify(dataCopy);
            }
          }
        }
      );
  }
])
  .controller("NoteCtrl", ['$scope','$rootScope','bulletinService','$resource','$http','Note', '$window',
    function($scope, $rootScope, bulletinService, $resource, $http, Note, $window) {

      $scope.init=function(type, id, noteTypeName, suppressAlert){
        $scope.noteCtrlScope=$scope;
        if(!$scope.notableType  && type)           $scope.notableType =type;
        if(!$scope.notableId    && id  )           $scope.notableId   =id;
        if(!$scope.noteTypeName && noteTypeName  ) $scope.noteTypeName=noteTypeName;

        switch($scope.notableType){
          case 'Consumer':
            $scope.noteableTypeHuman='Consumer';
            break;
          case 'User':
            $scope.noteableTypeHuman='Recruit';//only recruits get notes, not users.
            break;
          case 'Crm::Case':
            $scope.noteableTypeHuman='Policy';
            break;
        }

        if($scope.notableType && $scope.notableId){
          $scope.loadNotes('all',!suppressAlert);

          $scope.initNewNote();
      }else{
        console.error('Notes section was initialized without sufficient data to load or display notes.');
        }
      }

      $scope.$on('refreshNotes', function(e, args) {
          $scope.init();
      });

      $scope.loadNotes=function(filter,alert){
        $scope.noteFilter=filter||'all';
        var transactionId='/notes.json';
        $scope.ajaxStatusIsLoading=true;
        bulletinService.add({text:'Loading notes.',klass:'info',icon:'fa fa-refresh fa-spin',id:transactionId, isBusy:true});
        $scope.notes=Note.query(
          {
            notable_type  :$scope.notableType,
            notable_id    :$scope.notableId,
            note_type_name:$scope.noteTypeName,
            note_filter   :$scope.noteFilter
          },
          function(notes){
            if(alert){ $scope.alertForCriticalNotes(); }
            $scope.ajaxStatusIsLoading=false;
            $rootScope.removeBulletinById(transactionId);
            return processNotes(notes);
          },
          function(response ){
            try{msg=response.data.errors;}catch(e){msg="Failed to load notes.";}
            $scope.ajaxStatusIsLoading=false;
            bulletinService.add({text:msg,klass:'danger',id:transactionId});
          }
        );
      }
      $scope.isActive=function(filter){//helper function for setting the class on filter links
        return $scope.noteFilter==filter ? 'active' : ''
      }
      $scope.isForConsumer=function(){
        return $scope.notableType=='Consumer'
      }

      $scope.initNewNote=function(){
        $scope.noteCtrlScope.newNote=new Note({
          notable_type  :$scope.notableType,
          notable_id    :$scope.notableId,
          note_type_name:$scope.noteTypeName,
          creator_id    :$rootScope.currentUser.id,
          text          :'',
          confidential  :false,
          critical      :false,
          personal      :false
        });
      }

      $scope.saveNewNote=function(){
        var transactionId='Saving new note.';
        $scope.ajaxStatusIsLoading=true;
        bulletinService.add({text:transactionId, klass:'info', icon:'fa fa-floppy-o fa-spin', isBusy:true, id:transactionId});
      $scope.noteCtrlScope.newNote.$create(
          function(responseBody) {//success
            $scope.ajaxStatusIsLoading=false;
            bulletinService.add({text:"Successfully created note.", klass:'success', id:transactionId});

            $scope.noteCtrlScope.newNote.id          =responseBody.object.id;
            $scope.noteCtrlScope.newNote.removable   =true;
            $scope.noteCtrlScope.newNote.expanded    =false;
            $scope.noteCtrlScope.newNote.class       ='from-current-user'
            $scope.noteCtrlScope.newNote.creator_name="(You)"
            /*$scope.newNote.created_at  ="(Just Now)"*/

            $scope.notes.unshift($scope.noteCtrlScope.newNote);
            $scope.initNewNote();
          },
          function(item, response){//error
            try{msg=item.data.errors;}catch(e){msg="Failed to created note";}
            $scope.ajaxStatusIsLoading=false;
            bulletinService.add({text:msg, klass:'danger', id:transactionId});
          }
        );
      }

      $scope.destroyTopNote=function(){
        $scope.notes[0].$delete(
          function(){//success
            $scope.notes.splice(0,1);
          },
          function(){//error
            alert('Failed to destroy note.');
          });
      }

      $scope.removeCriticalFlag=function(note_id){
      if( !confirm("Remove critical flag on this note?") ){ return; }

      var relevantNote     =new Note({id:note_id,critical:false}),
            transactionId  ='Removing critical flag from note '+note_id+'.';

        $scope.ajaxStatusIsLoading=true;
        bulletinService.add({text:transactionId, klass:'info', icon:'fa fa-floppy-o fa-spin', id:transactionId});
        relevantNote.$update(
          function(item){//success
            $scope.ajaxStatusIsLoading=false;
            bulletinService.add({text:item.msg, klass:'notice', id:transactionId});
            $scope.loadNotes($scope.noteFilter);
          },
          function(item){//error
            try{msg=item.data.errors;}catch(e){msg="Failed to remove critical flag from note";}
            $scope.ajaxStatusIsLoading=false;
            bulletinService.add({text:msg, klass:'danger', id:transactionId});
          }
        );
      }

      $scope.alertForCriticalNotes=function() {
        var alertText='';
        $.each($scope.notes,function(index,note){
          if(note.critical){ alertText+="\n\n"+note.text; }
        });
        if(alertText.length){ alert(alertText); }
      }

      $scope.noteClass=function(n) {
        var str='';
        if(n.critical) str+='critical ';
        if(n.confidential) str+='confidential ';
        if(n.personal) str+='personal ';
        return str;
      }

      $scope.toggleAllNotesOpen=function(){
        for(var i in $scope.notes){
          $scope.notes[i].$expanded=true;
        }
      }
      
      $scope.toggleAllNotesClosed=function(){
        for(var i in $scope.notes){
          $scope.notes[i].$expanded=false;
        }
      }
      
      $scope.setFocus=function(){ //This auto-focuses the notes slide out textarea when toggling it open
        var el=angular.element('.notes-input');
        if (el)
          el.focus();
      }

      $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });

    }
  ]);

  function processNotes(notes){
    //This call to `convertInputs` is necessary because AngularJS does not seem to reliably call the function.
    //Luckily, it is written to be idempotent.
    $rootScope.convertInputs(notes);
    $.each(notes,function(index,n){
      processNote(n);
    });
  }

  function processNote(n){
    n.removable   =false;
    n.expanded    =false;
    n.creator_name=n.creator_name ? n.creator_name : '(System)'

    n.title       =n.text.split("\n")[0].substring(0,79);
    n.title      +=n.title==n.text ? '' : '...'//add ellipses if text has been truncated
    n.class       ='from-'+(n.creator_name==$rootScope.getCurrentUser().full_name ? 'current' : 'other')+'-user'
  }
