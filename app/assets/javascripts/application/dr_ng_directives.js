
angular.module('dr.directives', [])
/*
  `drDatetimePicker` wraps `datetimepicker` directive, which wraps the `uib-datepicker-popup` directive.
  https://github.com/zhaber/angular-js-bootstrap-datetimepicker
  https://github.com/angular-ui/bootstrap/tree/master/src/datepickerPopup
  Why not use `uib-datepicker-popup` directly for dates? B/c the new version requires a JS date object,
  whereas `datetimepicker` can work with strings. Another solution may be more universal filtering
  whenever objects are sent/recieved from the server.
*/
.directive('drDatetimePicker', ['uibDateParser','$compile', function (uibDateParser,$compile) {
  var dtTemplate='\
        <div class="datetimepicker">\
          <datetimepicker\
            id="{{id}}"\
            ng-model="model"\
            placeholder="mm/dd/yyyy"\
            date-format="MM/dd/yyyy"\
            datepicker-append-to-body="!preventAppendToBody"\
            date-opened="dateOpened"\
            date-ng-click="doNothing($event)"\
            hidden-time="dateOnly"\
            date-options="{}">\
          </datetimepicker>\
          <div class="btn btn-default"\
            ng-click="dateOpened=!dateOpened">\
            <i class="fa fa-calendar"></i>\
          </div>\
        </div>',
      dTemplate='\
        <input type="text"\
          id="{{id}}"\
          name="{{name}}"\
          ng-model="model"\
          ng-required="required"\
          class="form-control"\
          placeholder="mm/dd/yyyy"\
          ui-mask="99/99/9999"\
          model-view-value="true">';

  return {
    restrict: 'E',
    scope: { model: '=', dateOnly: '=', preventAppendToBody: '=', required: '=', id: '@', modelName: '@model', name: '@' },
    link: function (scope, element, attrs, model) {
      scope.doNothing=function(event){
        event.preventDefault();
        event.stopPropagation();
      }
      scope.datePickerOptions={};
      scope.element=element;
      if (!attrs.id) { scope.id = scope.modelName.replace(/\./g, '-{{$id}}-') }
      element.html( scope.dateOnly ? dTemplate : dtTemplate );
      element.replaceWith( $compile( element.html() )(scope) );
    }
  }
}])
/*
  Relies on library ImageResizeCropCanvas in vendor/assets, downloaded from http://tympanus.net/codrops/2014/10/30/resizing-cropping-images-canvas/

  Note: The library script uses selector '.modal.in .overlay' to find the element that it uses to determine crop boundary.
*/
.directive('drImageEditor', [function () {
  return {
    restrict: 'E',
    template: '<div class="modal-body">\
                <div ng-show="isFileReaderSupported" style="min-height:400px;transition:all .4s;position:relative">\
                  <!-- No NG binding support for file input. Therefore, we attach behaviour in the link fn. -->\
                  <input type=file ng-hide="file">\
                  <div ng-show="file">\
                    <img style="max-width:900px; max-height:900px;">\
                    <div class="overlay" style="width:{{cropWidth||200}}px; height:{{cropHeight||200}}px;"><div class="overlay-inner"></div></div>\
                  </div>\
                </div>\
                <span ng-hide="isFileReaderSupported">Your browser does not support FileReader, so image uploads are disabled.</span>\
              </div>\
              <div class="modal-footer">\
                <a class="btn pull-left" data-dismiss="modal" aria-label="Close" ng-click="fullyClearFile()">{{backBtnTxt||\'Cancel\'}}</a>\
                <a class="btn btn-primary" ng-click="triggerSaveImage()">{{saveBtnTxt||\'Save\'}}</a>\
              </div>',
    controller: ['$scope', function ($scope) {
      $scope.file = null;
      $scope.isFileReaderSupported = !!FileReader;
      function getFile () {
        var elem = $scope.$fileInput.get(0); // $scope.$fileInput should be set in directive's link function
        return elem.files && elem.files.length && elem.files[0];
      }
      $scope.readFileInput = function () {
        if (FileReader && ($scope.file = getFile())) {
          var reader = new FileReader();
          reader.onload = function () {
            $scope.$imageElem.attr('src', reader.result); // $scope.$fileInput should be set in directive's link function
          }
          var image = $scope.$imageElem.get(0);
          image.onload = function () {
            image.onload = null; // `delete` wouldn't garbage collect this fast enough, apparently
            $scope.$apply(); // This fn gets triggered by a non-ng event
            $scope.resizeableImage = new ResizeableImage($scope.$imageElem); // provided by ImageResizeCropCanvas
          }
          reader.readAsDataURL($scope.file);
        }
      }
      $scope.triggerSaveImage = function () {
        var dataUrl = $scope.resizeableImage.crop();
        $scope.saveFn(dataUrl, $scope.file.name);
      }
      $scope.fullyClearFile=function(){
        $scope.file=null;
        $scope.$fileInput.val('');
      }
    }],
    scope: {
      'saveFn': '=',
      'backBtnTxt': '=',
      'saveBtnTxt': '=',
      'cropWidth': '=',
      'cropHeight': '=',
    },
    link: function (scope, elem, attrs) {
      // No NG binding support for file input. Therefore, we attach behaviour with an onchange attribute.
      scope.$imageElem = elem.find('img');
      scope.$fileInput = elem.find('input[type=file]');
      scope.$fileInput.on('change', scope.readFileInput);
    }
  }
}])
.directive('ioSectionHeader', [function () {
  return {
    restrict: 'E',
    transclude: true,
    scope: { subHeading:'='},
    template: "\
      <div class=\"left\">\
        <h3 style=\"margin-bottom:0;\">\
          <i ng-class=\"SECTION_ICONS[ activeSectionName ] ||''\"></i>\
          {{activeSectionName}}\
        </h3>\
        <h2>{{subHeading || activePageName || '' }}</h2>\
      </div>\
      <div class=\"right\">\
        <div class=\"actions\">\
          <ng-transclude></ng-transclude>\
        </div>\
      </div>",
    link: function(scope, elem, attrs){
      //
    }
  };
}])
.directive('drPaginationLinks', [function () {
  return {
    restrict: 'E',
    link: function (scope, element, attrs) {
      scope.options = scope.$parent.$eval(attrs.options);
      scope.getPaginationTuples = function () {
        if (!scope.options) return;
        var current = scope.options.page;
        var max = scope.options.pageCt;
        var linkNumbers = new Array;
        var i;
        for (i = 1;                      i <= Math.min(3, max);         i++) linkNumbers.push(i);
        for (i = Math.max(i, current-2); i <= Math.min(max, current+3); i++) linkNumbers.push(i);
        for (i = Math.max(i, max-3);     i <= max;                      i++) linkNumbers.push(i);
        scope.paginationTuples = new Array;
        for (var j = 0; j < linkNumbers.length; j++) {
          var linkNumber = linkNumbers[j]
          var needsEllipsis = (j > 0) && linkNumber > 1+linkNumbers[j-1];
          scope.paginationTuples.push([linkNumber, needsEllipsis]);
        }
        return scope.paginationTuples;
      }
      scope.$watch('options.pageCt', function (oldVal, newVal) {scope.getPaginationTuples()});
      scope.$watch('options.page',   function (oldVal, newVal) {scope.getPaginationTuples()});
      scope.setPage = function (pageNumber) {
        // set page
        if (pageNumber < 1)
          scope.options.page = 1;
        else if (pageNumber > scope.options.pageCt)
          scope.options.page = scope.options.pageCt;
        else
          scope.options.page = pageNumber;
        // trigger callback
        var callee;
        if (typeof scope.options.pageChangeCallee === 'object')
          callee = scope.options.pageChangeCallee;
        else if (scope.options.pageChangeCallee)
          callee = scope.options;
        else
          callee = scope;
        scope.options.pageChangeCallback.call(callee, scope.options.page);
      };
    },
    template: '<a ng-click="setPage(options.page-1)" ng-class="options.page === 1 && \'disabled\'">&larr; Previous</a> \
    <span ng-repeat="tup in paginationTuples"> \
      <span ng-show="tup[1]">&#8230;</span> \
      <a ng-click="setPage(tup[0])" ng-class="{\'highlight\': tup[0] == options.page}">{{tup[0]}}</a> \
    </span> \
    <a ng-click="setPage(options.page+1)" ng-class="options.page === options.pageCt && \'disabled\'">Next &rarr;</a>',
  }
}])
.directive('drUserSelect', ['$http', function ($http) {
  function refreshUsersForUiSelect (select) {
    if (select.search && select.search.length) {
      select.users = [];
      $http.get('/contacts.json',
        {
          params: {
            person_type:'User',
            exclude_recruits:true,
            'select[]':['id','full_name','person_id','person_type'],
            search_by:'name_or_id',
            search_term:select.search
          }
        }
      )
      .success(function (data, status, headers, config) {
        data.forEach(function (user) {
          user._sid = user.id.toString(36).toUpperCase();
        });
        select.users = data;
      });
    }
  }
  return {
    restrict: 'EA',
    template: ' \
    <ui-select ng-model="modelHolder._user" on-select="onSelectFn($item, $select)"> \
      <ui-select-match data-placeholder="{{placeholder || \'User Name or Id\'}}">({{$select.selected._sid}}) {{$select.selected.full_name}}</ui-select-match> \
      <ui-select-choices class="text-left" repeat="user in $select.users" refresh="refreshFn($select)" refresh-delay="{{refreshDelay || 100}}"> \
        ({{user._sid}}) {{user.full_name}} \
      </ui-select-choices> \
    </ui-select>',
    link: function (scope, elem, attrs) {
      if (!attrs.onSelectFn) {
        var html = elem.toArray().map(function (e) { return e.outerHTML }).join("<!-- DELIMITER -->");
        throw("No on-select-fn attribute given for directive dr-user-select: "+html);
      }
      scope.onSelectFn = function ($item, $select) { scope.$parent.$eval(attrs.onSelectFn)($item, $select, scope) }
      scope.refreshFn = scope.$parent.$eval(attrs.refreshFn) || refreshUsersForUiSelect;
      scope.modelHolder = scope.$parent.$eval(attrs.modelHolder) || scope;
      scope.refreshDelay = attrs.refreshDelay;
      scope.placeholder = attrs.placeholder;
    },
    scope: {
      refreshDelay: '@',
      placeholder: '@',
    },
  };
}])
.directive('drLastBulletin',['$rootScope',function($rootScope){
  return {
    restrict: 'E',
    scope:    {'persist': '=',},
    template: '<div class="alert" ng-class="classFn()" style="white-space:pre-line;" ng-cloak>\
                <i ng-class="$root.lastOpenBulletin.icon"></i><span ng-bind="$root.lastOpenBulletin.text"></span>\
              </div>',
    link: function (scope, elem, attrs) {
      scope.classFn=function(){
        var classStr='alert';
        classStr+=' '+((scope.$root.lastOpenBulletin||{}).klass || 'alert-info');
        if( !(scope.$root.lastOpenBulletin || scope.persist) ){ classStr+=' unopaque';}
        if(!scope.persist){ classStr+=' ng-transition'; }
        return classStr;
      }
    }
  }

}])
.directive('drSocialIcons', [function () {
  return {
    restrict: 'E',
    template: "\
    <div ng-repeat=\"r in records\" ng-if=\"r.name!='Website'\">\
      <a ng-if=\"r.value\" ng-href=\"{{r.url_prefix}}{{r.value}}\" target=\"_blank\", title=\"Click to go to {{person.full_name}}'s {{r.name}} brand.\">\
        <i class=\"{{r.icon_class}}\"></i>\
      </a>\
      <i ng-if=\"!r.value\" class=\"{{r.icon_class}}\"></i>\
    </div>",
    link: function(scope, elem, attrs){
      WEB_TYPES_copy=$.extend(true,[],WEB_TYPES);
      scope.records=[];
      //Adding them to the variable this way because their order in the enum is not the order we want to display them in.
      scope.records.splice(0,0,WEB_TYPES_copy[2],WEB_TYPES_copy[3],WEB_TYPES_copy[4],WEB_TYPES_copy[1]);
      //Most of the info we need is in the enum. Next step is just to get the values for each.
      //If in future our list of social media services stays relatively fixed,
      //social media screennames should probably be individual columns on the person record to avoid this logic.
      //The wrapping within a `$promise.then()` callback is necessary because this linker may run before the person is loaded.
      scope.person.$promise.then(function(){
        if(scope.person.webs && scope.person.webs.length){
          for(var i in scope.records){
            for(var j in scope.person.webs){
              if(scope.person.webs[j].web_type_id==scope.records[i].id){
                scope.records[i].value=scope.person.webs[j].value;
                break;
              }
            }
          }
        }
      });
      
    }
  }
}])
.directive('drNotes',['$rootScope',function($rootScope){
  return {
    restrict: 'E',
    scope: { notableType: '=', notableId: '=', noteTypeName: '=', suppressAlert: '=', inlineNewNoteForm: '='},
    template: '<div id="consumer-notes" class="grid grid--column" ng-controller="NoteCtrl" ng-include="\'/notes/list.html\'"></div>',
    link: function (scope, elem, attrs, controllers) {
      //
    }
  }

}])
//This provider is used by the similarly named directive below.
.provider('DrCurrencyFormatter', function DrCurrencyFormatterProvider () {
  this.$get = ["$resource", '$rootScope', function DrCurrencyFormatterFactory ($resource, $rootScope){
    function DrCurrencyFormatter (readonly) {}
    Object.defineProperties(DrCurrencyFormatter.prototype, {
      run: {
        configurable: false,
        value: function (input, readonly) {
          if (!input) return input;
          if (typeof input == "number") input += "";
          input = input.replace(/[^\d.]/g,'');
          var decimalMatch = /^(\d+)(\.\d*){0,1}/.exec(input);
          if (!decimalMatch) {
            console.error('No decimal match for text "'+input+'"');
            return input;
          }
          var intPart = this.formatIntPart(decimalMatch[1]);
          var decimalPart = decimalMatch[2];
          if (readonly) {
            // Make decimalPart exactly 2 digits long
            if (!decimalPart || !decimalPart.length)
              decimalPart = '.00';
            while (decimalPart.length < 3)
              decimalPart += '0';
            if (decimalPart.length < 3)
              decimalPart = decimalPart.slice(0,3);
          }
          // Return
          return intPart + (decimalPart||'');
        }
      },
      runForReadonly: {
        configurable: false,
        value: function (input) {
          return this.run(input, true)
        }
      },
      runForReadWrite: {
        configurable: false,
        value: function (input) {
          return this.run(input, false);
        }
      },
      formatIntPart: {
        configurable: false,
        value: function (input) {
          // Count triplets in the pre-decimal-point text for insertion of commas
          var preTripletLen = input.length % 3;
          var tripletCount = Math.floor(input.length / 3);
          var pattern = "";
          if (preTripletLen)
            pattern += "(\\d{"+preTripletLen+"})";
          for (var i = 0; i < tripletCount; i ++)
            pattern += "(\\d{3})";
          var match = new RegExp(pattern).exec(input);
          // Add dollar sign and commas to input text
          if (!match) {
            console.error("No match for regex /"+pattern+"/ on text '"+input+"'");
            return input;
          }
          match.shift();
          return "$" + match.join(",");
        }
      },
    });
    return DrCurrencyFormatter;
  }];
}) // end provider for DrCurrencyFormatter.
// Used for formatting currency within a text input. This allows the model to hold a number
// while the input holds a formatted string.
.directive("drFormatCurrency",['DrCurrencyFormatter', function (DrCurrencyFormatter) {
  function formatR (input) {
    return new DrCurrencyFormatter().runForReadonly(input);
  }
  function formatRW (input) {
    return new DrCurrencyFormatter().runForReadWrite(input);
  }
  function parseR (input) {
    if (input && typeof input === "string")
      input = parseInt(input.replace(/\D/g, '')) || 0;
    return input;
  }
  function parseRW (input) {
    if (input && typeof input === "string")
      input = parseFloat(input.replace(/[^\d.]/g, '')) || 0;
    return input;
  }
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, elem, attrs, ngModel) {
      var readonly = attrs.ngReadonly && scope.$eval(attrs.ngReadonly);
      var formatFn = readonly ? formatR : formatRW;
      ngModel.$formatters.push(formatFn);
      if (readonly) {
        ngModel.$parsers.push(function (input) {
          elem[0].value = formatFn(input);
          return parseR(input);
        });
      } else {
        ngModel.$parsers.push(function (input) {
          elem[0].value = formatFn(input);
          return parseRW(input);
        });
      }
    }
  };
}])
;
