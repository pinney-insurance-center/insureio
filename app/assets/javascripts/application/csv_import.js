angular.module('dr.csvImport',['ui.bootstrap'])
.controller('CsvImportCtrl',['$scope','$rootScope','$upload','$http','requestSanitizer','bulletinService',
  function($scope, $rootScope, $upload, $http, requestSanitizer, bulletinService){
    $scope.stateVars={rowsPerBatch:10};

    $scope.csvFileReadCallback = function (csvFileReader) {
      var str=csvFileReader.result;
      var unicodeCharLocation = $scope.searchForUnicode(str);
      if(unicodeCharLocation >= 0)
        { $scope.pushError({row:'all', message: 'The CSV file contains disallowed characters starting at char '+unicodeCharLocation+'. Please restrict your inputs to ASCII characters.' }); }
      str=str.replace(/\r\n/g, "\n");//change DOS line endings to *nix line endings
      str=str.replace(/\n$/, "");//remove trailing line break
      $scope.stateVars.csvFileContent = str;

      var csvParseData=Papa.parse($scope.stateVars.csvFileContent,{header:true});
      if (!(csvParseData.data && csvParseData.data.length)) {
        $scope.pushError({row: 'all', message: 'Cannot process CSV with no data rows'});
        return;
      }
      for (var key of Object.keys(csvParseData)) {
        if (csvParseData[key] == '' || csvParseData[key] == null)
          delete csvParseData[key]
      }
      //add properties: [meta, errors, data] to stateVars.
      angular.extend($scope.stateVars,csvParseData);
      $scope.stateVars.batchLength=Math.ceil($scope.stateVars.data.length/$scope.stateVars.rowsPerBatch);
      if(!($scope.stateVars.errors && $scope.stateVars.errors.length)){
        $scope.stateVars.nestedData=$scope.convertRowsToNestedStructures($scope.stateVars.data);
        $scope.validateNestedData();
      }
      $scope.$apply();//necessary because this function is called by a non-angular callback function.
    }

    $scope.searchForUnicode=function(str){
      let match = /[^\u0000-\u00ff]/.exec(str);
      return match ? match.index : -1;
    }

    $scope.pushError=function(obj){
      $scope.stateVars = $scope.stateVars || {};
      $scope.stateVars.errors = $scope.stateVars.errors || [];
      $scope.stateVars.errors.push(obj);
    }

    $scope.readAndParseFile=function(){
      var keysToClear=['csvFileContent','meta','errors','data','nestedData','batchLength'];
      for(var i in keysToClear){
        delete $scope.stateVars[ keysToClear[i] ];
      }
      if(!$scope.stateVars.csvFile || !$scope.stateVars.csvFile.length){
        return;
      }
      //Observed different MIME types in Chrome on Ubuntu, on Win7, and on Win10.
      //Best to cover all possibilities, while not letting through obvious wrong types like images.
      var acceptableMimeTypeStrings=['','text/csv','text/plain','application/vnd.ms-excel'],
          fileMimeType=$scope.stateVars.csvFile[0].type,
          hasAcceptableMimeType=acceptableMimeTypeStrings.indexOf(fileMimeType)>-1,
          fileNameMatchObj=$scope.stateVars.csvFile[0].name.match(/.(\w+)$/),
          fileNameExtension=fileNameMatchObj && fileNameMatchObj[1],
          hasAcceptableFileNameExtension=fileNameExtension && fileNameExtension=='csv';
      if(!hasAcceptableMimeType || !hasAcceptableFileNameExtension){
        var formatMsg='This does not appear to be a CSV file.\n'+
                      'The file name extension is "'+fileNameExtension+'" and your OS says the file type is "'+fileMimeType+'".\n'+
                      'Please open your file in a text editor to be sure.\n'+
                      'It should look like plain text, with commas between cells and line breaks between rows.\n'+
                      'If the file contents look correct, then rename the file to end with ".csv".';
        $scope.pushError({row:'all', message: formatMsg });
        return;
      }      
      //When finished, `readAsText` will trigger the `onload` event,
      //which will execute the callback defined above.
      var csvFileReader = new FileReader();
      csvFileReader.onload = function () { $scope.csvFileReadCallback(csvFileReader) }
      csvFileReader.readAsText($scope.stateVars.csvFile[0],"UTF-8");
    }

    $scope.convertRowsToNestedStructures=function(rows){
      var nestedObjects=[];
      for(var i in rows){
        var row=rows[i];
        nestedObjects[i]={};
        for(var key in row){
          var methodChain=$scope.extractMethodChainFromString(key);
          $scope.followMethodChainToSetKey(nestedObjects[i], methodChain, row[key]);
        }
        requestSanitizer.clean(nestedObjects[i],[],true);
      }
      return nestedObjects;
    }

    $scope.extractMethodChainFromString=function(key){
      //Remove all but letters, digits, dots, and underscores from keys.
      var tmpChain=key.replace(/[^\w\.]/g, '').split('.'),
          methodChain=[];
      for(var m in tmpChain){
        var matches=tmpChain[m].match(/(\D+)(\d+)/);
        if(matches){
          methodChain=methodChain.concat([ matches[1], parseInt(matches[2]) ]);
        }else{
          methodChain.push(tmpChain[m]);
        }
      }
      return methodChain;
    }

    $scope.followMethodChainToSetKey=function(obj, methodChain, v){
      var nextObj;
      if(methodChain.length>1){
        if( Number.isInteger(methodChain[0]) ){//key is an index
          if(!obj[ methodChain[0] ]){
            obj[ methodChain[0] ]={};
          }
          nextObj=obj[ methodChain[0] ];
        }else{//key is a key
          if( Number.isInteger(methodChain[1]) ){//value is an array
            var pluralized;
            switch(methodChain[0][ methodChain[0].length-1 ]){
              case 's':
                pluralized=methodChain[0].replace(/s$/,'ses');
                break;
              case 'y':
                pluralized=methodChain[0].replace(/y$/,'ies');
                break;
              default:
                pluralized=methodChain[0]+'s';
            };
            if( methodChain[0].match(/_details$/) ){
              //These are the only associated model names that are already plural
              //and therefor don't conform to the above pluralization rules.
              //This list will need reevaluating if/when future models and associations are added.
              pluralized=methodChain[0];
            }
            if(!obj[ pluralized+"_attributes" ]){
              obj[ pluralized+"_attributes" ]=[];
            }
            nextObj=obj[ pluralized+"_attributes" ];
          }else{//value is an object
            if(!obj[ methodChain[0]+"_attributes" ]){
              obj[ methodChain[0]+"_attributes" ]={};
            }
            nextObj=obj[ methodChain[0]+"_attributes" ];
          }
        }
        methodChain.shift();//like `pop`, but from beginning of array.
        $scope.followMethodChainToSetKey(nextObj, methodChain, v);
      }else{//key is an attribute
        if( v.match(/^[\d,\.]+$/) ){//value is a string representing a number
          v=v.replace(/,/g,'');//remove commas so rails doesn't choke
        }else if(v==''){//value is a blank string
          return obj;//return without setting this key
        }
        obj[ methodChain[0] ]=v;
      }
      return obj;
    }

    $scope.validateNestedData=function(){
      for(var i in $scope.stateVars.nestedData){
        var c=$scope.stateVars.nestedData[i],
            errors=[];
        if(c.profile_id){
          errors.push('The key `profile_id` is no longer supported. Use key `brand_id`.');
        }
        if(!c.agent_id && !c.brand_id){
          errors.push('Either an agent_id or a brand_id is required, but missing from this row.');
        }
        if(!c.full_name){
          errors.push('Full name is required, but missing from this row.');
        }
        for(var key in c){
          var keyIsId=!!key.match(/\w(_id)$/),
              v=c[key];
          if(keyIsId){
            if( !v || isNaN(v) || !parseInt(v) ){
              errors.push('Key '+key+' must be numeric and nonzero and a real id.');
            }
          }
        }
        if(errors.length){
          $scope.pushError({row: i, message: errors.join(' ') });
        }
      }
    }

    $scope.uploadNextCsvBatch=function(){
      if(!$scope.stateVars.batchIdx){
        $scope.stateVars.batchIdx=0;
        $scope.stateVars.status='started';
        $scope.stateVars.successes=[];
        $scope.stateVars.failures=[];
        $scope.stateVars.transactionId=$rootScope.randomNonZeroId();
      }
      var transactionId=$scope.stateVars.transactionId,
          batchLowerBound=$scope.stateVars.batchIdx*$scope.stateVars.rowsPerBatch,
          paddedBatchUpperBound=($scope.stateVars.batchIdx+1)*$scope.stateVars.rowsPerBatch,
          batchUpperBound=Math.min(paddedBatchUpperBound, $scope.stateVars.nestedData.length),
          thisBatch=$scope.stateVars.nestedData.slice(batchLowerBound,batchUpperBound);
      $http.post('/consumers/batch_import.json',{rows:thisBatch,offset:batchLowerBound})
      .success(function(data){
        $rootScope.addBulletin({text:'Uploaded batch '+($scope.stateVars.batchIdx+1)+'.', klass:'success', id:transactionId});
        $scope.stateVars.successes=$scope.stateVars.successes.concat(data.successes);
        $scope.stateVars.failures=$scope.stateVars.failures.concat(data.failures);
        if($scope.stateVars.batchIdx+1<$scope.stateVars.batchLength){
          $scope.stateVars.batchIdx++;
          $scope.uploadNextCsvBatch();
        }else{
          $scope.stateVars.status='success';
        }
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to upload batch '+$scope.stateVars.batchIdx+'. '+(data.errors||''), klass:'danger', id:transactionId});
        $scope.stateVars.status='error';
      });
    }

  }
]);
