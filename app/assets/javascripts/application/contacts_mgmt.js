angular.module('ContactsMgmtApp', ['RestfulResource', 'ui.bootstrap'])
.config(['$locationProvider',
  function($locationProvider) {
      //Allows us to use standard url params (`/some/path?key=value`)
      //vs. Angular style (`/some/path#?key=value`).
      $locationProvider.html5Mode({
        enabled:true,
        rewriteLinks : false
      });
    }
])
.factory("Contact", ['Resource', function ($resource) {
  //Angular will collapse the `/`s if no `actionName` or `id` is provided.
  return $resource('/contacts/:id/:actionName.json', {id:'@id'});
}])
.controller("ContactsMgmtCtrl",
  ['$scope','$rootScope','$resource','$http','$uibModal','$location','$filter','User','Contact','bulletinService',
  function ($scope,$rootScope,$resource,$http,$uibModal,$location,$filter,User,Contact,bulletinService) {
  $scope.canCreateRecruits=true;

  $scope.init=function(){
    $scope.scopeVars={};
    $scope.scopeVars.newUserTemplate={
      addresses_attributes:[
        {address_type_id:2,$typeName:'Work'}
      ],
      phones_attributes:[
        {phone_type_id:2}
      ],
      emails_attributes:[{}]
    };
    $scope.scopeVars.searchCriteria={
      pageChangeCallback: function () {//used for drPaginationLinks
        $scope.getContacts();
      },
      pageChangeCallee: true,//used for drPaginationLinks
    };
    $scope.scopeVars.searchCriteriaDefaults={
      person_type:'Consumer',
      name_starting_with:'Any',
      search_term:'',
      search_by:'name',
      page:1,
    };
    $scope.relevantKeys=Object.keys($scope.scopeVars.searchCriteriaDefaults);
    $scope.readStateFromUrl();
    if($rootScope.immediatelyTriggerNewForm){
      $scope.openNewConsumerModal();
      $rootScope.immediatelyTriggerNewForm=false;
    }
  };
  //This function populates `$scope.scopeVars.searchCriteria` from the url if possible,
  //otherwise from defaults. It then calls `$scope.getContacts()`
  //if non-default values were detected.
  $scope.readStateFromUrl=function(){
    var urlParams  =$location.search(),
        defaults   =$scope.scopeVars.searchCriteriaDefaults,
        workingData=$scope.scopeVars.searchCriteria;

    $scope.copyRelevantKeys(defaults, workingData);
    $scope.copyRelevantKeys(urlParams, workingData);

    $scope.getContacts();
  }
  $scope.copyRelevantKeys=function(fromObj,toObj){
    for (var i in $scope.relevantKeys){
      var key=$scope.relevantKeys[i];
      if( fromObj[key] ){//ignore nulls, blanks, zeros.
        toObj[key]=fromObj[key];
      }
    }
  }
  $scope.resetState=function(){
    $scope.scopeVars.searchCriteria=$.extend(true,{},$scope.scopeVars.searchCriteriaDefaults);
    $scope.contacts=[];
    $scope.writeStateToUrl();
  }
  //This function records application state in the browser's history,
  //making the user's search bookmarkable and repeatable via the back button,
  //and (I think) listing it as the referrer for subsequent requests (advantageous for troubleshooting).
  $scope.writeStateToUrl=function(){
    var stateObj={};
    $scope.copyRelevantKeys($scope.scopeVars.searchCriteria, stateObj);
    $location.search(stateObj);
  }
  //On success, this function calls `writeStateToUrl`.
  $scope.getContacts=function(){
    const xid = bulletinService.randomNonZeroId();
    const paramsObj = {
      addresses: true,
      emails: true,
      paginate: true,
      phones: true,
    };
    $scope.copyRelevantKeys($scope.scopeVars.searchCriteria, paramsObj);
    bulletinService.add({text:'Loading contacts.', id:xid});

    $scope.contactsRequest=Contact.get(
      paramsObj,
      function(data){//success
        bulletinService.add({text:'Loaded contacts. Processing for display.', id:xid});
        $scope.contacts=data.records;
        $scope.scopeVars.searchCriteria.page=data.page;
        $scope.scopeVars.searchCriteria.pageCt=data.page_count;
        for(var i in $scope.contacts){
          $scope.formatContactData( $scope.contacts[i] );
          $rootScope.setPointersToPrimaryContactMethods( $scope.contacts[i] );
        }
        $scope.writeStateToUrl();

        var isSuper=$rootScope.currentUser.can('super_edit');
        $scope.contacts.forEach(function(c){
          if(!isSuper){ c.$isEditable=!!c.is_editable; }
          delete c.is_editable;
        });

        if($scope.scopeVars.searchCriteria.person_type=='User'){
          var userIds=[]
          $scope.contacts.forEach(function(u){
            if(u.person_type=='User')
              userIds.push(u.id);
          });
          if(userIds.length){
            $scope.getBrandMemberships(userIds);
          }
        }

        bulletinService.removeBulletinById(xid);
      },
      function(response){//error
        bulletinService.add({text:'Failed to load contacts. '+(response.errors||''), klass:'danger', id:xid});
      }
    );
  }
  $scope.getBrandMemberships=function(userIds){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading brand memberships.', id:transactionId});

    $scope.scopeVars.brandMembershipsRequest=$http.post('/users/member_brand_names.json',{ids:userIds})
    .success(function(data, status, headers, config){
      $rootScope.removeBulletinById(transactionId);
      for(var uId in data){
        for(var i in $scope.contacts){
          var c=$scope.contacts[i];
          if(c.person_type=='User' && c.person_id==uId){
            c.$memberBrands=data[uId];
          }
        }
      }
    })
    .error(function(data){
      var msg='Failed to load brand memberships.'
      if(data){
        msg+=data.msg||data.error||data.errors;
      }
      $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
    });
  }

  $scope.personTypeBtnClass=function(str){
    return $scope.scopeVars.searchCriteria['person_type']==str ? 'active' : '';
  }

  $scope.togglePersonType=function(str){
    $scope.scopeVars.searchCriteria['person_type']=str;
    $scope.getContacts();
  }

  $scope.formatContactData=function(contact){
    var str=contact.person_type||contact.$personType;
    contact.$ownerName=contact.owner_name;
    delete contact.owner_name;

    switch(true){
      case (/(User|Recruit)/).test(str):
        //in future, these will be further differentiated and have separate urls.
        contact.$url='/users/'+contact.id;
        contact.$contactUpdateUrl='/users/'+contact.id+'.json';
        contact.$ownerIdFieldName='parent_id';
        contact.$ownerId=contact[contact.$ownerIdFieldName];
        contact.$humanOwnerFieldName='Parent';
        contact.$humanPersonType='user';
        //fall through
      case str=='Recruit':
        contact.$typeStr='Recruit';
        contact.$iconClass='fa fa-black-tie';
        break;
      case str=='User':
        contact.$typeStr='User';
        contact.$iconClass='fa fa-user';
        break;
      case str=='Brand':
        contact.$typeStr='Brand';
        contact.$iconClass='fa fa-building';
        contact.$url='/brands/'+contact.id;
        contact.$contactUpdateUrl='/brands/'+contact.id+'.json';
        contact.$ownerIdFieldName='owner_id';
        contact.$ownerId=contact[contact.$ownerIdFieldName];
        contact.$humanOwnerFieldName='Owner';
        contact.$humanPersonType='brand';
        break;
      case str=='Consumer':
        contact.$typeStr='Consumer';
        contact.$iconClass='fa fa-folder-open';
        contact.$url='/consumers/'+contact.id;
        contact.$contactUpdateUrl='/consumers/'+contact.id+'.json';
        contact.$ownerIdFieldName='agent_id';
        contact.$ownerId=contact[contact.$ownerIdFieldName];
        contact.$humanOwnerFieldName='Agent';
        contact.$humanPersonType='consumer';
        break;
    }
  }

  $scope.openNewConsumerModal=function(){
    $uibModal.open({
      templateUrl: '/consumers/new_consumer_modal.html',
      controller : 'newConsumerModalCtrl',/*This controller is defined in `UserHierarchyMgmtApp`.*/
      windowClass: '',
      scope      : $scope
    });
  }
  $scope.openNewUserModal=function(parentUser){
    $uibModal.open({
      resolve    : { parentUser: function () { return parentUser || $rootScope.getCurrentUser(); } },
      templateUrl: '/users/new_user_modal.html',
      controller : 'newUserModalCtrl',/*This controller is defined in `UserHierarchyMgmtApp`.*/
      windowClass: '',
      scope      : $scope
    });
  }
  $scope.openEditManagerModal=function(contact){
    $uibModal.open({
      resolve    : { contact: function () { return contact; } },
      templateUrl: '/contacts/modal_for_manager.html',
      controller : 'editManagerModalCtrl',
      scope      : $scope
    });
  }
  $scope.openUserStaffAssignmentModal=function(contact){
    $uibModal.open({
      resolve    : { contact: function () { return contact; } },
      templateUrl: '/contacts/modal_for_staff_assignment.html',
      controller : 'userStaffAssignmentModalCtrl',
      scope      : $scope
    });
  }
  $scope.deactivateUser=function(userId){
    var warningText='Deactivating this user account removes them from contact management, '+
      'lead distribution, and agent/staff assignment menus, immediately logs them out, and prevents them logging in. '+
      'It will NOT delete any descendant users, or brands/consumers/policies/tasks assigned to them. To continue, please '+
      'confirm that you have removed or reassigned all items assigned to this user.';

    if( !confirm(warningText) ){ return; }

    var transactionId=$rootScope.randomNonZeroId(),
        disabledMembershipId=$filter('whereKeyMatchesValue')( MEMBERSHIPS,'name','Disabled' )[0].id;
    $rootScope.addBulletin({text:'Deactivating user account.', id:transactionId});

    User.update({id:userId},
      {
        user:{
          active:false,
          membership_id:disabledMembershipId
        }
      },
      function(data){//success
        $rootScope.addBulletin({text:'Successfully deactivated user account.', klass:'success', id:transactionId});
        $scope.getContacts();
      },
      function(response){//error
        var msg='Failed to deactivate user account.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
  }

  $rootScope.getCurrentUser().$promise.then(function(){
    $scope.init();
  });
}])
.controller('newConsumerModalCtrl', ['$scope','$filter','$http','$uibModalInstance','requestSanitizer','bulletinService',
  function ($scope, $filter, $http, $uibModalInstance, requestSanitizer, bulletinService) {
  $scope.formHolder={};
  $scope.GLOBAL_ID=1;
  $scope.USER_ID=2;
  $scope.USERS_ID=3;
  $rootScope.getPolicyStatusTypes();
  $scope.statusTypeGroupName=function(item){
    return item.$groupName;
  }
  $rootScope.getUserStatusTypes();
  $scope.userStatusTypeGroupName=function(item){
    return item.groupName;
  }   
  $scope.addPolicyStatus=function(item){
    $scope.status_params = [{status_type_id: item.id, status_type_name: item.name, status_type_category_id: item.status_type_category_id,
                             statusable_type: 'Consumer'}];
  } 
  $scope.addUserStatus=function(item){
    $scope.status_params = [{status_type_id: item.id, status_type_name: item.name, status_type_category_id: item.status_type_category_id,
                             statusable_type: 'User'}];
  }   
  $scope.newConsumerTemplate={
    agent_id:$rootScope.currentUser.id,
    brand_id:$rootScope.currentUser.default_brand_id,
    addresses_attributes:[{}],
    phones_attributes:[{}],
    emails_attributes:[{}],
    tags:{}
  };

  $scope.newConsumer=$.extend(true,{},$scope.newConsumerTemplate);
  $scope.contact=$scope.newConsumer;//don't use this variable! want to phase this out.
  $scope.person=$scope.newConsumer;//for tags to work
  $scope.resetNewConsumer=function(){
    $scope.newConsumer=$.extend(true,{},$scope.newConsumerTemplate);
    $scope.contact=$scope.newConsumer;//don't use this variable! want to phase this out.
    $scope.formHolder.newConsumerForm.$setPristine();
  }
  $scope.saveNewConsumer=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        newConsumerSanitizedCopy=$.extend(true,{},$scope.newConsumer),
        params={consumer: newConsumerSanitizedCopy};

    newConsumerSanitizedCopy.statuses_attributes = $scope.status_params;
    requestSanitizer.clean(newConsumerSanitizedCopy, null, true);

    $rootScope.addBulletin({text:'Saving new consumer.', id:transactionId});

    $http.post('/consumers.json',params)
    .success(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Successfully saved new consumer with id '+data.id+'.', klass:'success', id:transactionId});
      
      $uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save new consumer.'+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.getBrands=function(){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading brands.', id:transactionId});

    $http.post('/users/member_brand_names.json',{ids:[$rootScope.currentUser.id]})
    .success(function(data, status, headers, config){
      $rootScope.removeBulletinById(transactionId);
      $scope.brands=data[$rootScope.currentUser.id];
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load brands.'+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.getBrands();
}])
.controller("editManagerModalCtrl",
  function ($scope,$rootScope,$resource,$http,$uibModalInstance,contact) {

  $scope.init=function(){
    $scope.contact=contact;
    $scope.resetManager();
  }

  //This function gets names of agents in a specific brand.
  $scope.refreshAgentsInProfile=function(search){
    if(search.length <= 2){ return; }
    var paramsObj={
      full_name:search,
      brand_id:contact.brand_id,
      'select[]':['id','full_name','company']
    };
    $http.get('/users.json', {params:paramsObj})
    .success(function(data, status, headers, config){
      $scope.agentsInProfile=data;
    });
  }

  $scope.saveManagerChange=function(){
    var successCb=function(){//`updateRecordSingleField`` takes care of the pending/success/fail bulletins.
          contact[contact.$ownerIdFieldName]=$scope.scopeVars.tmpAgentId;
          contact.$ownerName=$scope.scopeVars.tmpAgentName;
          $uibModalInstance.close();
        };

    $rootScope.updateRecordSingleField(
      contact.$contactUpdateUrl,
      contact.$humanPersonType,
      contact.$ownerIdFieldName,
      $scope.scopeVars.tmpAgentId,
      successCb
    );
  }

  $scope.resetManager=function(){
    $scope.scopeVars.tmpAgentId=$scope.contact[$scope.contact.$ownerIdFieldName];
    $scope.scopeVars.tmpAgentName=$scope.contact.$ownerName;
    if($scope.scopeVars.editManagerForm){
      $scope.scopeVars.editManagerForm.$setPristine();
    }
  }
  $scope.init();
})
.controller("userStaffAssignmentModalCtrl", ['$scope','$rootScope','$resource','$http','$uibModalInstance','contact','bulletinService',
  function ($scope,$rootScope,$resource,$http,$uibModalInstance,contact,bulletinService) {
  $scope.contact=contact;
  $scope.formHolder={};
  $sAModalScope=$scope;
  $scope.stateVars=$scope.scopeVars;
  $scope.getUserStaffAssignment=function(){
    var transactionId=$rootScope.randomNonZeroId(),
      paramsObj={owner_id:contact.person_id, owner_type:contact.person_type};

    $rootScope.addBulletin({text:'Loading staff assignment.', id:transactionId});
    $http.get('/usage/staff_assignments.json', {params:paramsObj})
    .success(function(data, status, headers, config){
      $scope.scopeVars.activeStaffAssignment = data;
      $scope.setStaffAssignmentFormObj();
      $rootScope.removeBulletinById(transactionId);//just remove the loading message if successful.
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load staff assignment. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.setStaffAssignmentFormObj=function(){
    if( (typeof ($scope.formHolder.staffAssignmentForm||{}).$setPristine) =='function' ){
      $scope.formHolder.staffAssignmentForm.$setPristine();
    }
    $sAModalScope.sA=$.extend(true,{},$scope.scopeVars.activeStaffAssignment);
  }
  $scope.saveStaffAssignment=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        sACopy       =$.extend(true,{},$scope.sA),
        httpMethod   =sACopy.id ? 'put' : 'post',
        url          =sACopy.id ? '/usage/staff_assignments/'+sACopy.id+'.json' : '/usage/staff_assignments.json';
    for(var k in sACopy){//don't submit with name fields.
      if(k.match(/_name$/)){ delete sACopy[k];}
    }
    $rootScope.addBulletin({text:'Saving staff assignment.', id:transactionId});
    $http[httpMethod](url,{usage_staff_assignment:sACopy})
    .success(function(data, status, headers, config){
      $uibModalInstance.close();
      delete $scope.scopeVars.activeStaffAssignment;
      $rootScope.addBulletin({text:'Successfully saved staff assignment.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save staff assignment. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.getUserStaffAssignment();
}]);
