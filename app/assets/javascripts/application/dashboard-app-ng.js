var UPDATE_ACTION = {'update':{method:'PUT'}};

angular.module("DashboardApp", ['RestfulResource',  'ui.bootstrap','ui.bootstrap.contextMenu'])
.factory("Dashboard.Event", ['$resource', function ($resource) {
  return $resource('/ahoy/events/:id.json', { id: '@id' });
}])
.factory("Task", ['$resource', function ($resource) {
   var Task = $resource('/tasks/:id.json', {id:'@id'}, {update:{method:'PUT'}});
   Object.defineProperties(Task.prototype, {
     setColourClass: {
       configurable: false,
       value: function () {
         Task.setColourClass(this);
       }
     }
   });
   Object.defineProperties(Task, {
     setColourClass: {
       configurable: false,
       value: function (task, selectedTaskId) {
         if (task.completed_at) {
           task._colourClass = 'green'; //Tasks that are completed
         }
         else {
           var today = new Date();
           try {
             var dueDate = new Date(task.due_at);
             if (dueDate.getDate() == today.getDate() && dueDate.getMonth() == today.getMonth() && dueDate.getYear() == today.getYear())
               task._colourClass = 'green'; //Tasks with todays deadline
             else if (dueDate > today)
               task._colourClass = 'grey'; //Tasks with later deadline
             else
               task._colourClass = 'red'; //Tasks past due
           } catch (ex) {
             console.error(ex);
           }
         }
         if (selectedTaskId && task.id == selectedTaskId) task._colourClass += ' selected-task';
       }
     }
   });
   return Task;
}])
.controller("Dashboard.EventCtrl", ['$scope','bulletinService','Dashboard.Event','moment',
  function($scope,bulletinService,Event,moment) {
    window.socketFn.gotEvent= function(message) {
      console.log('Received event from socket:',message.data);
      var eventList=$('#events-widget .list-events, #consumer-events .list-events'),
          $scope=null;
      if(!eventList.length){return;}//only proceed if there is an open list of events in the dom.
      $scope=angular.element(eventList).scope();
      $scope.pushEvent(message.data);
    }

    $scope.pushEvent=function(newEvent){
      //Events are shown in reverse-chronological order,
      //so unless an event is being updated, it should go to the top of the list.
      //If the event being updated is older than the oldest in the list, it should not be added.
      var added    =false,
          lastEvent=$scope.events[$scope.events.length-1];

      if($scope.events.length && newEvent.time<lastEvent.time){console.log('Event falls outside the scope of this list.');return;}

      //If this event list only pertains to a single participant,
      //then pushed events not pertaining to that participant should be ignored.
      var filterByParticipant=$scope.person.id && newEvent.participant;
      if(filterByParticipant){
        var participantMatches=newEvent.participant_type==($scope.consumerMgmtVars ? 'Consumer' : 'User') && newEvent.participant.id==$scope.person.id;
        if(!participantMatches){
          return;
        }
      }
      processEvents([newEvent], filterByParticipant );

      //attempt to replace any event with same id
      for(var i=0; i<$scope.events.length;i++){
        if($scope.events[i].id==newEvent.id){
          $scope.events[i]=newEvent;
          added=true;
          console.log('Inserted event at index '+i+' in the list.');
          break;
        }
      }
      if(!added){
        $scope.events.unshift(newEvent);
        console.log('Inserted event at the top of the list.');
      }
      $scope.$apply();//this refreshes the ngRepeat div.
    };

    var transactionId='/ahoy/events.json';
    bulletinService.add({text:'Loading events.',klass:'info',icon:'fa fa-refresh fa fa-spin',id:transactionId, isBusy:true});
    params=$scope.person.id ? {participant_id: $scope.person.id, participant_type: ($scope.consumerMgmtVars ? 'Consumer' : 'User')} : {};
    $scope.events = Event.query(
      params,
      function(events){
        bulletinService.add({text:'Events loaded.',klass:'success',id:transactionId});
        return processEvents(events,true)
      },
      function(response){
        try{msg=response.data.msg;}catch(e){msg="Failed to load events.";}
        bulletinService.add({text:msg,klass:'danger',id:transactionId});
      }
    );
  }
])
.controller("Dashboard.WidgetsCtrl", ['$scope', '$sce', '$resource', '$uibModal', '$rootScope', '$location', 'DASHBOARD_WIDGETS', 'Task',
  function($scope, $sce, $resource, $uibModal, $rootScope, $location, DASHBOARD_WIDGETS, Task) {

    $scope.init=function(){
      $scope.widgetCtrlVars=[];
      $scope.widgetCtrlVars.activeWidgetName='Recently Viewed';

      $scope.enabledWidgets = [];
      $scope.buildEnabledWidgetsList();

      $scope.readStateFromUrl();
    }

    $scope.buildEnabledWidgetsList=function(){
      var availableWidgets = DASHBOARD_WIDGETS.slice(0); // shallow clone the array

      for (let i in availableWidgets) {
        var widget =availableWidgets[i],
            fullyFunctionalWidget= new WidgetTable($scope, $resource, Task, widget);
        if (!widget.permission || $scope.can(widget.permission, true)) {
          $scope.enabledWidgets.push(fullyFunctionalWidget);
        }
      }
      $scope.recentConsumers=[];
      try {
        $scope.recentConsumers = JSON.parse(localStorage.recentConsumers||'[]');
      } catch (ex) {
        console.log('Error retrieving recent consumers from localStorage:',ex);
      }
      for (let i in $scope.recentConsumers){
        $scope.recentConsumers[i].addr = $sce.trustAsHtml($scope.recentConsumers[i].addr);
      }
    }

    $scope.readStateFromUrl=function(){
      var params=$location.search(),
          w=params.widget || localStorage.lastActiveWidget;
      if(w && w!='Recently Viewed'){
        $scope.widgetCtrlVars.activeWidgetName=w;
      }
    }

    $scope.setActiveWidget=function(str){
      if($scope.widgetCtrlVars.activeWidgetName==str){ return; }
      $scope.widgetCtrlVars.activeWidgetName=str;
      $location.search({widget:str});
      localStorage.lastActiveWidget=str;
    }

    $scope.openChangeTaskStatusModal = function (task,tableId,tableScope,self) {
      $uibModal.open({
        controller: 'Dashboard.changeTaskStatusCtrl',
        templateUrl: '/crm/cases/modal_for_change_status_task.html',
        windowClass: 'thin-modal',
        $scope: $scope,
        resolve: {
          task: function () {return task},
          tableScope: function () {return tableScope},
          tableId: function () {return tableId},
          self: function () {return self}
        }
      });
    }

    $scope.userStatusTypeGroupName=function(item){
       return item.groupName;
    }

    $scope.getCurrentUser().$promise.then(function () {
      $scope.init();
    });
  }
])
.controller("Dashboard.WidgetTableCtrl", ['$scope', '$resource', function ($scope, $resource) {
  $scope.tableId = $scope.widget.keyPrefix;
  $scope.widget.scope=$scope;
  $scope.widget.setDateFilterWatchers();
  $scope.widget.get();
  $scope.widget.getStatusOptions();

  //Both this scope and `Consumer.TasksCtrl` listen for and respond to this message.
  $scope.$on('refreshCurrentTasks', function(e) {
    $scope.widget.get();
  });

  $scope.withdrawTask=function(task){
    if( !confirm('Are you sure you want to close this task?') ){ return; }
    $resource('/tasks/:id/inactive_task.json', {id:task.id}, UPDATE_ACTION).update(
      function (data) {
        $scope.addBulletin({klass:'success', text:'Task closed.'});
        $scope.widget.get();
        if( typeof(successFn)=='function' ){ successFn(); }
      },
      function () {
        $scope.addBulletin({klass:'danger', text:'Failed to modify Task.'});
      }
    );
  }
  $scope.withdrawEZLCase=function(kaseId){
    if( !confirm('Are you sure you want to close this case?') ){ return; }
    $resource('/crm/cases/:id.json', {id:kaseId}, UPDATE_ACTION).update(
      {crm_case:{status_type_id:$scope.EZL_WITHDRAWN_STATUS_TYPE_ID}},
      function (data) {
        $scope.addBulletin({klass:'success', text:'Case closed.'});
        $scope.widget.get();
        if( typeof(successFn)=='function' ){ successFn(); }
      },
      function () {
        $scope.addBulletin({klass:'danger', text:'Failed to modify case.'});
      }
    );
  }

  $scope.setColourClass=function (task, selectedTaskId) {
    if (task.completed_at) {
       task._colourClass = 'grey';//Tasks that are completed
    }
    else {
      var today = new Date(),
          dueDate;
      try {
        dueDate = new Date(task.due_at);
      }catch (ex) {
        console.error(ex);
        return;
      }
      if( dueDate.getDate()  == today.getDate() &&
          dueDate.getMonth() == today.getMonth() &&
          dueDate.getYear()  == today.getYear()
      ){
        task._colourClass = 'green';//Tasks with todays deadline
      }else if (dueDate > today){
        task._colourClass = 'grey';//Tasks with later deadline
      }else{
        task._colourClass = 'red';//Tasks past due
      }
    }
    if (selectedTaskId && task.id == selectedTaskId){ task._colourClass += ' selected-task'; }
  }

  $scope.urlForTask=function(task){
    var urlStr ='';

    if(task.person_type === 'Consumer'){
      urlStr= '/consumers/'+task.person_id;
    }else if(task.person_type === 'User'){
      urlStr= '/users/'+task.person_id;
    }
    if (task.origin_type === 'Crm::Status'){
      if (task.sequenceable_type === 'Quoting::Quote'){
        urlStr+='/?active_tab=Policy';
        urlStr+="&opportunity_id="+task.sequenceable_id;
      }else if(task.sequenceable_type === 'Crm::Case'){
        urlStr+='/?active_tab=Policy';
        urlStr+="&policy_id="+task.sequenceable_id;
      }else if(task.sequenceable_type === 'User'){
        urlStr+='/?active_tab=Follow Up';
      }
      urlStr+='&task_id='+task.id;
    }else if(task.origin_type === 'Marketing::Subscription'){
      urlStr+='/?active_tab=Marketing';
    }
    return urlStr;
  }

}])
.controller('Dashboard.contextMenu', [
  '$scope','$window','$timeout','$filter',
  function ($scope,$window, $timeout, $filter) {


    var p       =$scope.task.person;
    $scope.menuOptions = [
      ['Call',
        function ($scope) {//action
          $window.location = 'tel:' + p.$primaryPhone.value
        },
        function($scope){//enabled?
          return !!p.$primaryPhone;
        }
      ],
      ['Email',
        function ($scope) {//action
          $window.location = 'mailto:' + p.$primaryEmail.value
        },
        function($scope){//enabled?
          return !!p.$primaryEmail;
        }
      ],
      ['Change Status',
        function ($scope) {
          $scope.openChangeTaskStatusModal($scope.task, this.scope, this.widget);need
        },
        function($scope){//enabled?
          return $scope.task.origin_type=='Crm::Status';
        }
      ],
      ['Add Task',function ($scope){
        $scope.openNewTaskModal(p, this.scope);
      }]
    ];

  }
])
.controller('Dashboard.ezlContextMenu', [
  '$scope','$window','$timeout',
  function ($scope,$window, $timeout) {

    $scope.ezlMenuOptions = [
      ['Remove', function($scope){
        $scope.withdrawEZLCase($scope.kase.id);
      }]
    ];

  }
])
.controller("Dashboard.changeTaskStatusCtrl",['$scope','$resource','$filter','$http','$uibModalInstance','task','tableId','self','DASHBOARD_WIDGETS', '$rootScope', 'tableScope',
  function ($scope, $resource, $filter, $http,$uibModalInstance,task,tableId,self,DASHBOARD_WIDGETS, $rootScope, tableScope){
    if (task.status){
      $scope.status_type_id = task.status.status_type_id;
      $scope.status_type_name = task.status.status_type_name;
      $scope.currentStatusType={id:$scope.status_type_id,name:$scope.status_type_name};
    }

    if(task.sequenceable_type=='User'){
      $rootScope.getUserStatusTypes();
      $scope.statusTypes=$rootScope.userStatusTypes;
    }else{
      $rootScope.getPolicyStatusTypes();
      $scope.statusTypes=$rootScope.policyStatusTypes;
    }
    $scope.statusTypeGroupName=function(item){
      return item.$groupName;
    }
    $scope.resetTmpStatusTypeId=function(){
        $scope.tmpStatusTypeId=$scope.status_type_id;
      }
    $scope.resetTmpStatusTypeId();

    $scope.updateStatus=function(statusTypeId){
      var transactionId=$rootScope.randomNonZeroId(),
        sequenceable_id=task.sequenceable_id,
        model          =task.sequenceable_type;

       $rootScope.addBulletin({text:'Updating status.', id:transactionId});

       $http.post('/crm/statuses.json',{statusable_id: sequenceable_id, statusable_type: model, status_type_id: statusTypeId, task_id: task.id})
       .success(function(data, status, headers, config){
         $rootScope.addBulletin({text:'Successfully updated status.', klass:'success', id:transactionId});
         $rootScope.$broadcast('refreshCurrentTasks');
         $uibModalInstance.close();
       })
       .error(function(data, status, headers, config){
         $rootScope.$broadcast('refreshCurrentTasks');
         $rootScope.addBulletin({text:'Failed to update status. '+(data.errors||''), klass:'danger', id:transactionId});
       });
    }
  }
])
.controller("ProductionSummaryCtrl", ['$scope', '$http', '$uibModal',
  function($scope, $http, $uibModal) {

    ProductionSummaryStateModel=function(){
      this.page  =1;
      this.pageCt=1;
      this.userId=$scope.userScopeVars.agentId
      this.order_column='last_updated';
      this.descending=true;
      this.get();
    }
    Object.defineProperties(ProductionSummaryStateModel.prototype, {
      pageChangeCallback: { // used for directive drPaginationLinks
        get: function () {
          return this.setPage;
        },
        call: function(callee, page) {
          this.page = page;
          $scope.LoadWithPageOptions();
          return this.setPage;
        }
      },
      pageChangeCallee: {   // used for directive drPaginationLinks
        value: true,
      },
      setPage: {
        writable: false,
        value: function (page) {
          this.page = page;
          this.get();
        }
      },
      sortColumns:{
        value: function(column) {
          if (this.order_column == order_column) {
            this.descending = !this.descending;
          }
          else {
            this.order_column = order_column;
            this.descending = false;
          }
          this.setPage(1);
        }
      },
      get: {
        writable: false,
        value: function(){
          var self=this,
              transactionId=$rootScope.randomNonZeroId();

          $rootScope.addBulletin({text:'Loading production summary.', id:transactionId});

          $http.get( '/reporting/production_summaries.json', {params:self} )
          .success(function(data, status, headers, config){
            $rootScope.removeBulletinById(transactionId);
            $scope.cases=data.open_cases;
            self.pageCt=data.page_count;
          })
          .error(function(data, status, headers, config){
            var errorMsg='Error loading production summary.';
            $rootScope.addBulletin({text:errorMsg + (data.errors||''), klass:'danger', id:transactionId});
          });
        }
      }
    });

    $scope.init = function(){
      $scope.widget=new ProductionSummaryStateModel;
      $scope.sort_columns=$scope.widget.sortColumns;
    }

    $scope.userScopeVars.agent.$promise.then(function () {
      $scope.init();
    });
  }
])
.constant("moment", moment)
.constant('WidgetTable', WidgetTable)
;

//This object constructor encapsulates all table functionality.
//This includes sorting, filtering, querying, and data/callbacks for directive `drPaginationLinks`.
//Functions that act on a single task or case should be defined in the appropriate controller.
function WidgetTable($scope, $resource, Task, widgetTemplate){
  this.scope     = $scope;
  this.resource  = $resource;
  this.Task      = Task;
  for(let key in widgetTemplate){
    this[key]    =widgetTemplate[key];
  }
  if( this.label.match(/(opportunities|pending|unassigned)/i) ){
    this.columnHeadings=[
      {optName:'person_name',     humanName:'Consumer Name'},
      {humanName:'Type'},
      {optName:'tz_offset',       humanName:'State / Time'},
      {optName:'status_type_name',humanName:'Status'},//filtering is by status id, but sorting is by status name.
      {optName:'label',           humanName:'Follow Up'},
      {optName:'due_at',          humanName:'Due Date'},
      {humanName:'Premium'},
      {humanName:'Assigned Brand'}
    ];
    if( this.label.match(/(team)/i) ){
      this.columnHeadings.splice(7,0,{optName:'assigned_to_name',humanName:'Assigned User'});
    }
  }
  this.keyPrefix =this.label.replace(/\s+/g,'-').toLowerCase();
  this.options   = new Object;
  this.dueOptions=['All','Current'];
  if( this.keyPrefix.match(/opportunities/) ){
    this.dueOptions.push('New Lead Only');
  }
  this.optionKeys=['due','order_column','order_direction','updated_at_gte','updated_at_lte','due_at_gte','due_at_lte','status_type_id','label','person_type','person_id','person_name','include_tests','assigned_to_id','assigned_to_name'];

  //load all option keys from `localStorage`
  for(var i in this.optionKeys){
    var key=this.optionKeys[i];
    this.options[key]=localStorage.getItem(this.keyPrefix+'-'+key);
  }
  this.options.due=this.options.due || this.dueOptions[0];//default to "All"
}

Object.defineProperties(WidgetTable.prototype, {
  getSuccessCb:{
    writable: false,
    value: function (data) {
      var self=this;
      if(self.keyPrefix.match(/(tasks|opportunities)/)){
        self.scope.tasks = data.tasks;
        self.scope.tasks.forEach(function(t,idx){
          self.scope.tasks[idx]=t=new self.Task(t);
          t.setColourClass();
          $rootScope.setPointersToPrimaryContactMethods(t.person);
        });
      }else{
        self.scope.cases = data.cases;
        if( self.keyPrefix.match(/rtt/) ){
          self.scope.cases.forEach(function(t){
            t.$status=t.statuses[t.statuses.length-1];
          });
        }
      }
      self.count  = data.count;
      self.page   = data.page;         // used for directive drPaginationLinks
      self.pageCt = data.page_count;   // used for directive drPaginationLinks
    }
  },
  get: {
    writable: false,
    value: function () {
      var self = this;
      this.response = this.resource(this.url).get(this.options);
      this.response.$promise.then(function (data) {
        self.getSuccessCb(data);
      }, function (xhr) {
        var msg='';
        try { msg = xhr.data.errors; } catch (ex) {}
        self.scope.addBulletin({klass:'danger', text:'Failed to get data for table "'+self.label+'". '+msg});
        if(xhr.data.count && xhr.data.page && xhr.data.pageCt && xhr.data.cases || xhr.data.tasks ){
          self.getSuccessCb(xhr.data);
        }
      });
      return this.response;
    }
  },
  setDateFilterWatchers:{
    writable: false,
    value: function () {
      var dateFilterKeys=['created_at_gte','created_at_lte','updated_at_gte','updated_at_lte','due_at_gte','due_at_lte'],
          scope=this.scope;
      for(let i in dateFilterKeys){
        var key=dateFilterKeys[i];
        scope.$watch('widget.options.'+key, function(newValue,oldValue){
          if (oldValue != newValue && ( !newValue || (newValue instanceof Date) ) ){
            scope.widget.updateLocalStorage(key);
          }
        });
      }
    }
  },
  pageChangeCallback: { // used for directive drPaginationLinks
    get: function () {
      return this.setPage;
    }
  },
  pageChangeCallee: {   // used for directive drPaginationLinks
    value: true,
  },
  refreshLabelOptions: {
    writable: false,
    value: function (search) {
      if (search.length > 2)
        this.labelOptions = this.resource("/tasks/follow_up_names.json").query({q:search});
    }
  },
  refreshAssignedUserOptions:{
    writable: false,
    value: function (search) {
      if (search.length > 2)
        this.assignedUserOptions = this.resource("/contacts.json").query({person_type:'User',search_by:'full_name',search_term:search,exclude_recruits:true});
    }
  },
  setAssignedUser:{
    writable: false,
    value: function (userObj) {
      if(!userObj){
        userObj={id:'',full_name:''}
      }
      this.updateLocalStorage('assigned_to_name',userObj.full_name, true);
      this.updateLocalStorage('assigned_to_id',  userObj.id);
    }
  },
  refreshPersonOptions: {
    writable: false,
    value: function (search) {
      if (search.length > 2)
        this.personOptions = this.resource("/contacts.json").query({search_by:'full_name',search_term:search});
    }
  },
  setPerson:{
    writable: false,
    value: function (personObj) {
      if(!personObj){
        personObj={person_type:'',id:'',full_name:''}
      }
      this.updateLocalStorage('person_name',personObj.full_name,   true);
      this.updateLocalStorage('person_type',personObj.person_type, true);
      this.updateLocalStorage('person_id',  personObj.id);
    }
  },
  updateLocalStorage: {//Make `localStorage` match `options`.
    writable: false,
    value: function (key,value,suppressDataRefresh){//value argument is optional
      var updatedValue;
      if( (typeof value)!='undefined' ){
        updatedValue=value||'';
        //If value is passed in explitly, it is because the model is not up-to-date yet.
        //So must set the model here, to ensure that table data is refreshed correctly.
        this.options[key]=value;
      }else{
        updatedValue=this.options[key]||'';
      }
      localStorage.setItem(this.keyPrefix+'-'+key, updatedValue);
      if(!suppressDataRefresh){ this.get(); }
    }
  },
  getStatusOptions: {
    writable: false,
    value: function () {
      var includeUserTypes=!this.label.match(/business/i) && $rootScope.currentUser.can('user_status_rules_gui'),
          widgetObj=this;
      $rootScope.getPolicyStatusTypes();
      if(includeUserTypes){
        $rootScope.getUserStatusTypes();
        $rootScope.policyStatusTypes.$promise.then(function(){
          $rootScope.userStatusTypes.$promise.then(function(){
            widgetObj.statusOptions=$rootScope.policyStatusTypes.concat($rootScope.userStatusTypes);
          });
        });
        for(var i in this.statusOptions){
          if ( (typeof this.statusOptions[i])=='undefined' ) {         
            this.statusOptions.splice(i, 1);
            i--;
          }
        }
      }else{
        this.statusOptions=$rootScope.policyStatusTypes;
      }
    }
  },
  setDueScope: {//for task tables only
    writable: false,
    value: function (filter) {
      this.options.due = filter;
      localStorage.setItem(this.keyPrefix+'-due', filter||'All');
      this.get();
    }
  },
  setTestInclusionScope: {//for rtt table only
    writable: false,
    value: function (filter) {
      this.options.include_tests = filter;
      localStorage.setItem(this.keyPrefix+'-include_tests', filter||'All');
      this.get();
    }
  },
  sort: {
    writable: false,
    value: function (key) {
      if (!this.options.order_column || this.options.order_column==key) {
        this.options.order_direction = this.options.order_direction == "DESC" ? "ASC" : "DESC"
      }
      this.options.order_column = key;
      this.updateLocalStorage('order_column');
      this.updateLocalStorage('order_direction');
      this.get();
    }
  },
  setPage: {
    writable: false,
    value: function (page) {
      this.options.page = page;
      this.get();
    }
  }
});


//Processes the JSON before rendering.
//Does not show fields for people when
//the query is already scoped to pertain to a single person.
function processEvents(events, remove_people){
 $.each(events,function(index,e){

   if( typeof(remove_people)!='undefined' && remove_people ){
     delete e.participant;
     delete e.owner;
     delete e.brand;
   }
   e.expanded=false;

   var theMoment=moment(e.time,'YYYY-MM-DDTHH:mm:ssZ');

   e.date=theMoment.format("M/D/YY");

   e.time_ago=deriveTimeAgoFromEventTime(theMoment);

   e.icon=deriveIconFromEventName(e.name);

   e.participant_type_human=e.participant_type=='Consumer' ? 'Consumer': 'Recruit';
 })
}

//Derives a FontAwsome icon class from the name of the event.
function deriveIconFromEventName(n){
  switch(true){
    case /Email/.test(n):
      return "fa fa-envelope-o";
    case /Initiated.*Unsubscribe/.test(n):
      return "fa fa-exclamation";
    case /Completed.*Unsubscribe/.test(n):
      return "fa fa-exclamation-triangle";
    case /Widget/.test(n):
      return "fa fa-cog";
    case /External Link/.test(n):
      return "fa fa-external-link-square";
    default:
      return "fa fa-circle-o";
  }
}

function deriveTimeAgoFromEventTime(time){
  var timeUnit='minutes'
      timeAgo =moment().diff(time,timeUnit);

  if(timeAgo>=60){
    timeUnit='hours';
    timeAgo=moment().diff(time,timeUnit);
  }
  if(timeAgo>=24){
    timeUnit='days';
    timeAgo=moment().diff(time,timeUnit);
  }

  return timeAgo+' '+timeUnit+' ago';
}
