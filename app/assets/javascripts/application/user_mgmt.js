
angular.module('dr.users', [])
.controller("User.PersonalCtrl", [
  '$scope', '$rootScope', '$q', '$http', '$filter','$uibModal','$resource','$location','$timeout','$sce','bulletinService','UserModel',
  function ($scope, $rootScope, $q, $http, $filter, $uibModal, $resource, $location, $timeout, $sce, bulletinService, UserModel) {
    $scope.init=function(){
      $scope.userScopeVars={};//Object properties are mutable from within child scopes.
      $scope.userScopeVars.scope=$scope;
      $scope.GLOBAL_ID=1;
      $scope.USER_ID=2;
      $scope.USERS_ID=3;
      window.$location=$location;
      var matchedPath=$location.$$path.match(/users\/(\d+)/),
          idString=matchedPath ? matchedPath[1] : '0',
          uId=parseInt( idString ) || $rootScope.currentUser.id;
      //This scope may be spawned by navigation to a url for a user or for a recruit.
      //If for the current user, no other records will be loaded.
      //If for a user other than the current user, then recruit will also be loaded.
      //If for a recruit, then the user (if it exists) will also be loaded.
      //Manager view operates on the recruit, and agent view operates on the user.

      if( uId==$rootScope.currentUser.id ){
        $scope.userScopeVars.agentId=$rootScope.currentUser.id;
        $scope.userScopeVars.agent  =$rootScope.currentUser;
        $scope.userScopeVars.managerOrAgentView='agent';
        $scope.person=$scope.userScopeVars.agent;
      }else{
        //get the specified user/recruit
        $scope.person=UserModel.get({id:uId});
      }
      $scope.person.$promise.then( $scope.initialLoadOfPersonCb );

      $rootScope.getUserStatusTypes();
    }

    $scope.initialLoadOfPersonCb=function(){
      var usv=$scope.userScopeVars;

      if( $scope.person.is_recruit ){
        usv.managerOrAgentView='manager';
        usv.recruitId=$scope.person.id;
        usv.recruit  =$scope.person;
        usv.agentId  =$scope.person.user_account_id;
        if( $scope.person.user_account_id ){
          usv.agent=UserModel.get({id: $scope.person.user_account_id });
        }else{
          usv.agent=$q.defer();
          usv.agent.resolve();
        }
      }else{//person is user
        usv.managerOrAgentView='agent';
        usv.agentId=$scope.person.id;
        usv.agent  =$scope.person;
        if($scope.person!=$rootScope.currentUser){
          usv.recruit=UserModel.getRecruitForUser(
            {user_id:$scope.person.id},
            function(data){//success
              data.$personType='Recruit';
              $scope.userScopeVars.recruitId=$scope.userScopeVars.recruit.id;
            }
          );
        }
      }

      var sectionName=$location.search().selected_tab;
      if( sectionName && sectionName!='Personal' ){
        var matchingSections=$scope.userGuiSections.filter(function(s, idx){ return s.name==sectionName });
        if(matchingSections){
          $scope.openSection( matchingSections[0] );
        }
      }

      /*if(!$rootScope.hasTakenTour() && $scope.userScopeVars.agentId==$rootScope.currentUser.id){
        $timeout(function(){ $scope.tour.start(); },1000);
        $rootScope.indicateTourWasTaken();
      }*/

    }

    $scope.updatePassword = function() {
      var msg="You must re-enter your password to verify your identity before proceeding with this change.",
          password = prompt(msg);
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Updating password', id:transactionId});
  
      var data = {
        password: password,
        'user': {
          'password': $scope.userScopeVars.password,
          'password_confirmation':$scope.userScopeVars.password_confirmation
        }
      };
      $http({
        method  : 'PUT',
        url     : "/users/" + $scope.userScopeVars.agentId + ".json",
        data    : data
      }).success(
      function(data, status, headers, config) {
        bulletinService.add({text:'Successfully updated your password.', klass:'success', id:transactionId});                          flare
        $scope.modalInstance.close();
      }).error(function(data, status, headers, config){
        $scope.addBulletin({text: 'Failed to update your password. '+(data.error||data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.updateUserStatus=function(statusTypeId){
      var transactionId = bulletinService.randomNonZeroId();
      $('#modal-edit-status-details').modal('hide');
      bulletinService.add({text:'Updating user status.', id:transactionId});

      $http.post('/crm/statuses.json',{statusable_id:$scope.person.id,statusable_type:'User', status_type_id: statusTypeId.id})
      .success(function(data, status, headers, config){
        $scope.refreshCurrentTasks();
        bulletinService.add({text:'Successfully updated user status.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        bulletinService.add({text:'Failed to update user status. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    //Points the variable person at the correct person object based on the menu value.
    $scope.setPerson=function(){
      var key=$scope.userScopeVars.managerOrAgentView=='agent' ? 'agent' : 'recruit';
      $scope.person=$scope.userScopeVars[key];
      $scope.person.$promise.then(function(){
        if($scope.userScopeVars.managerOrAgentView=='recruit'){
          $scope.getSubscriptions($scope.userScopeVars.recruitId, 'User',$scope);
          $rootScope.getPolicyStatusTypes();
        }
      });
    }

    $scope.setAgent=function(){
      $scope.agent = $scope.userScopeVars['agent'];
    }

    $scope.isAgentView    =function(){ return $scope.userScopeVars.managerOrAgentView=='agent'; }
    $scope.isManagerView  =function(){ return $scope.userScopeVars.managerOrAgentView=='manager'; }
    $scope.isMyAccountView=function(){ return $scope.userScopeVars.agentId==$rootScope.currentUser.id; }

    //This list is looped over to create the navigation in the user personal view.
    $scope.userGuiSections=[
      {
        shouldBeShown:function(){ return ($rootScope.can('user_gui_production') && $scope.isManagerView() ); },
        url:          function(){ return '/reporting/production_summary.html'; },
        name:         'Production',//the part passed in the url
        displayText:  'Production',
      },
      {
        shouldBeShown:function(){ return ( $rootScope.can('marketing') && $scope.isManagerView() );},
        url:          function(){ return '/contacts/marketing.html'; },
        name:         'Marketing',
        displayText:  'Marketing',
      },
      {
        shouldBeShown:function(){ return ( $rootScope.can('brand') &&
            ($scope.isAgentView() || $scope.isMyAccountView() ) ); },
        url:          function(){ return '/brands/gui.html'; },
        name:         'Brands',
        displayText:  'Brands',
      },
      {
        shouldBeShown:function(){ return ( $rootScope.can('submission_options') && $scope.isAgentView() ); },
        url:          function(){ return '/users/submission_options.html'; },
        name:         'Submission',
        displayText:  'Submission Options',
      },
      {
        shouldBeShown:function(){ return (
            ( !$scope.isMyAccountView() && $rootScope.can('user_gui_l_and_c') ) ||
            ( $scope.isMyAccountView() && $rootScope.can('my_account_view_licenses') || $rootScope.can('my_account_view_contracts') )
          );
        },
        url:          function(){ return '/users/l_and_c.html'; },
        name:         'Licensing',
        displayText:  'Licensing & Contracting',
      },
      {
        shouldBeShown:function(){ return ( $rootScope.can('email') && $scope.isAgentView() && $scope.isMyAccountView() ); },
        url:          function(){ return '/smtp_servers/container.html'; },
        name:         'Email',
        displayText:  'Email Configuration',
      },
      {
        shouldBeShown:function(){
          return (
            ( $rootScope.can('user_gui_permissions') && !$scope.isMyAccountView() && $scope.isManagerView() && $scope.userScopeVars.agent) ||
            ( $rootScope.can('view_own_permissions') && $scope.isMyAccountView() )
          );
        },
        url:          function(){ return '/users/permissions.html'; },
        name:         'Permissions',
        displayText:  'Permissions',
      },
      {
        shouldBeShown:function(){
          return (
              $scope.isMyAccountView() && $scope.isAgentView()
            );
        },
        url:          function(){ return '/users/payment_info.html?no_layout=true'; },
        name:         'Payment',
        displayText:  'Payment Info',
      },
    ];
    $scope.userGuiSectionNames=[
      'Production',
      'Marketing',
      'Brands',
      'Submission Options',
      'Licensing & Contracting',
      'Email Configuration',
      'Permissions',
      'Payment Info'
    ];

    $scope.openSection=function(sectionObj){
      if( !sectionObj.shouldBeShown() ){ return; }
      $scope.userScopeVars.selectedTab=sectionObj.name;
      $scope.userScopeVars.selectedTabUrl=sectionObj.url();
      $location.search({selected_tab:sectionObj.name});
    }

    $scope.openContactMethodsModal=function(){
      //Bulletins will show within the modal for feedback purposes.
      //So existing bulletins should expire before the modal opens.
      bulletinService.clear();
      $uibModal.open({
        resolve    : {},
        templateUrl: '/contacts/modal_for_contact_methods.html',
        controller : 'contactMethodsModalCtrl',
        scope      : $scope
      });
    }

    $scope.saveUserPhoto = function (dataUrl, filename) {
      var transactionId = bulletinService.randomNonZeroId();
      $scope.addBulletin({text:'Uploading photo.', id:transactionId});
      $http.put('/users/'+$scope.person.id+'.json', {user:{photo_data_url:dataUrl}})
      .success(function(data, status, headers, config){
        $('#photo-preview').attr('src',dataUrl);
        $('.modal.in').modal('hide');
        $scope.addBulletin({text:'Successfully uploaded photo.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to upload photo. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }

    Object.defineProperty($scope, 'tasks', { // In use by Consumer.TasksCtrl and PolicyCtrl
      value: new Array(2),
      configurable: false,
    });
    Object.defineProperties($scope.tasks, {
      addCurrentTask: {
        configurable: false,
        value: function (task) {
          this[1].push(task);
          this.formatTask(task);
        }
      },
    });

    $scope.openAccountLinkModal=function(){
      $scope.modalInstance = $uibModal.open({
        resolve: { tasks: function () { return $scope.tasks } },
        templateUrl: '/users/modal_for_account_link.html',
        controller : 'userAccountLinkCtrl',
        scope      : $scope
      });
    }

    $scope.openAccountInfoModal=function(){
      $scope.modalInstance = $uibModal.open({
        resolve: { tasks: function () { return $scope.tasks } },
        templateUrl: '/users/modal_for_account_info.html',
        controller : 'userAccountInfoCtrl',
        scope      : $scope
      });
    }

    $scope.getApiKey= function(){
      if($scope.userScopeVars.apiKeySaving){ return; }
      var msg="You must re-enter your password to verify your identity before proceeding.",
            password = prompt(msg);
      UserModel.getApiKey(
        {
          id: $scope.person.id,
          password: password
        },
        function(data){//success
          var msg='Below is your API Key.\n\nPlease protect this information as much as you would your password, since it acts as your password for API requests.';
          prompt(msg, data.api_key);
        },
        function(response){//error
          bulletinService.add({text:'Failed to retrieve API Key. '+(response.data.error||response.data.errors||''), klass:'danger'});
          $scope.userScopeVars.apiKeySaving=false;
        }
      );
    }

    $scope.resetApiKey=function(){
      var msg="You must re-enter your password to verify your identity before proceeding with this change.",
          password = prompt(msg),
          transactionId=bulletinService.randomNonZeroId();
      $scope.userScopeVars.apiKeySaving=true;
      bulletinService.add({text:'Updating API Key...',id:transactionId})
      UserModel.updateWithCustomParams(
        {
          id: $scope.person.id,
          user:{ trigger_reset_api_key: true },
          password:password
        },
        function(data){//success
          $scope.userScopeVars.apiKeySaving=false;
          bulletinService.add({text:'Updated API Key.', klass:'success',id:transactionId});
          var msg='Below is your API Key.\n\nPlease protect this information as much as you would your password, since it acts as your password for API requests.';
          prompt(msg, data.api_key);
        },
        function(response){//error
          bulletinService.add({text:'Update failed. '+(response.data.error||response.data.errors||''), klass:'danger',id:transactionId});
          $scope.userScopeVars.apiKeySaving=false;
        }
      );
    }

    $scope.userStatusTypeGroupName=function(item){
      return item.groupName;
    }

    $scope.askConfirmation = function(person){
      var check = confirm("Warning! This action cannot be undone. Are you sure you want to convert this account?"); 
      if (check == true) { 
        $scope.createUserAccount(person)
      }else { 
        return false; 
      }
    }
 
    $scope.createUserAccount = function(person){
      UserModel.create(
        { recruits_attributes:[{id: person.id}] },
        function(data, status, headers, config) {//success
          $uibModalInstance.close();
          $scope.showLastReportingBulletin=true;
          bulletinService.add({text:'Successfully created user account from recruit.', klass:'success'});
        },
        function(data, status, headers, config){//error
          bulletinService.add({text:'User account failed to create.', klass:'danger'});
        }
      );
    }

    $scope.extractInitialsFromName=function(name){ // Retrieves the first letter of a users first/last name
      var nameArray=name.split(' '),
          first=nameArray[0],
          last=nameArray.length>1 ? nameArray[nameArray.length-1] : '',
          initials=(first[0]+(last[0]||'') ).toUpperCase();
      return initials;
    }

    $scope.hexColorCodeFromQuoterKey=function(user){ // Assigns a color to users based on users quoter_key
      var codeAsInt=parseInt(user.quoter_key,36),
          codeAsHex=codeAsInt.toString(16),
          code=codeAsHex.slice(0,6);
      return code;
    }

    //Please keep the following line at the end of the code block, so it only runs after all scope functions have been defined.
    $scope.getCurrentUser().$promise.then(function(){ $scope.init(); });

  }
])
.controller('userAccountLinkCtrl', [
'$scope', '$http', '$uibModalInstance', '$q', '$uibModal', 'bulletinService',
function ($scope, $http, $uibModalInstance, $q, $uibModal, bulletinService) {
  $scope.link = {};
  
  $scope.linkAccount = function(person){
    if(!$scope.link.manualUserId){ return; }
    var transactionId = bulletinService.randomNonZeroId();
    bulletinService.add({text:'User account is being linked.', id:transactionId});

    $http({
      method  : 'PUT',
      url     : '/users/'+person.id+'.json',
      data    : { user:{ user_account_id: $scope.link.manualUserId }}
    }).success(function(data, status, headers, config) {
      $uibModalInstance.close();
      $scope.showLastReportingBulletin=true;
      bulletinService.add({text:'User account has been successfully linked', klass:'success', id: transactionId});
    }).error(function(data, status, headers, config){
      bulletinService.add({text:'User account linking failed.', klass:'danger', id: transactionId});
    });
  }
}])
.controller('userAccountInfoCtrl', function ($scope, $http, $uibModalInstance, $q, $uibModal) {
  //
});

$(document).on('change', 'input.field_set_premium_limit', function() {
  var params_data = { premium_limit: $(this).val() };
  url = "/users/"+$(this).data('id')+"/update.json"
  fire_ajax(url, params_data, 'put');
});
