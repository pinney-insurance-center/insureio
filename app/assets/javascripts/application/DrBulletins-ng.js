angular.module('DrBulletins', [])
.run(['$rootScope','bulletinService', function ($rootScope,bulletinService){
  $rootScope.lastOpenBulletin=null;//the bulletin to be replaced by new non-error bulletins as they arrive
  $rootScope.openErrorBulletins=[];//error bulletins that have not yet been explicitly dismissed by the user
  $rootScope.bulletins = [];//the full list, to be displayed in the modal
  $rootScope.busyRequests = [];//a list of ids for bulletins with the isBusy flag set to true
  $rootScope.setToggleNavBarRight = function(val){
    $rootScope.toggleNavBarRight = val;
  }

  $rootScope.compareToggleNavBarRight = function(val){
    return $rootScope.toggleNavBarRight == val;
  }

  $rootScope.addBulletin=function (opts, applyScope) {
    console.warn('`$rootScope.addBulletin` is DEPRECATED. Call `bulletinService.add` instead.');
    bulletinService.add(opts);
    if (applyScope) this.$apply();
  }

  $rootScope.randomNonZeroId = function(){
    console.warn('`$rootScope.randomNonZeroId` is DEPRECATED. Call `bulletinService.randomNonZeroId` instead.');
    return bulletinService.randomNonZeroId();
  }

  $rootScope.removeBulletinById= function(id){
    console.warn('`$rootScope.removeBulletinById` is DEPRECATED. Call `bulletinService.removeBulletinById` instead.');
    bulletinService.removeBulletinById(id);
  }
}])
.factory('bulletinService', ["$rootScope", function ($rootScope) {
  function Bulletin (attrs) {
    jQuery.extend(this, attrs);
  }
  Bulletin.prototype.startRemovalTimer = function () {
    delete this.isBeingReadByUser;
    var self = this;
    if(self.removalTimer) clearTimeout(self.removalTimer);
    self.removalTimer=setTimeout(function(){
      if (!self.isBeingReadByUser && !self.isBusy && !(self.klass=='alert-danger') ) {
        self.remove();
        $rootScope.$apply();
      }
    }, this.ttl||16000);
  }
  Bulletin.prototype.remove = function () {
    let self = this;
    $rootScope.bulletins = $rootScope.bulletins.filter(bull => bull !== self);
    $rootScope.lastOpenBulletin = $rootScope.bulletins.slice(-1)[0];
  }
  Bulletin.prototype.removeFromBusyRequests = function () {
    var i = $rootScope.busyRequests.indexOf(this.id);
    if (i >= 0) {
      $rootScope.busyRequests.splice(i, 1);
    }
  }
  Bulletin.prototype.removeFromOpenErrorBulletins = function () {
    var i = $rootScope.openErrorBulletins.indexOf(this);
    if (i >= 0) {
      $rootScope.openErrorBulletins.splice(i, 1);
    }
  }
  return {
    //This function will generate fairly reliably unique ids.
    //The random number prevents collisions even when several transactions are started nearly simultaneously
    //(as is the case when a template has several included templates and is populated via a json request),
    //and the timestamp component prevents collisions if the same random number is generated later.
    //It is unlikely that so many requests will be started at once that they could have both the same timestamp and random number.
    randomNonZeroId: function(){
      return ( Math.random()|| this.randomNonZeroId() )+'_'+(new Date()).toISOString();
    },
    add: function (opts) {
      if( (typeof opts)!='object' ){ return; }
      /*
      opts should include:
        -text
        -klass (optional) {info|danger|success|warning}
        -icon  (optional, can be any icon class from FontAwesome)
        -id    (optional. allows managing a transaction with the server start to finish)
        -ttl   (optional, measured in milliseconds)
      */
      //default to 'info'
      opts.klass= !(opts.klass) ? 'info' : opts.klass;
      //the bootstrap classes all start with alert, like 'alert', 'alert-danger', etc.
      opts.klass= opts.klass.match(/alert/) ? opts.klass : 'alert-'+opts.klass;
      if(!opts.icon){
        switch(opts.klass){
          case 'alert-info':      opts.icon='fa fa-spinner fa-pulse';     break;
          case 'alert-danger':    opts.icon='fa fa-ban';                  break;
          case 'alert-success':   opts.icon='fa fa-check-circle';         break;
          case 'alert-warning':   opts.icon='fa fa-exclamation-triangle'; break;
        }
      }
      if(opts.klass=='alert-success' && opts.text==''){
        opts.ttl= opts.ttl ? opts.ttl : 9000;//make blank success bulletins default to 3 seconds instead of 12.
      }
      else if(opts.klass=='alert-success'){
        opts.ttl= opts.ttl ? opts.ttl : 12000;//make success bulletins default to 6 seconds instead of 12.
      }
      let hasSpinnerIcon=(opts.icon||'').indexOf('fa fa-spinner')>-1;
      if( hasSpinnerIcon || (opts.isBusy && opts.id) ){
        $rootScope.busyRequests.push(opts.id);
      }
      opts.text=!!(opts.text) ? String(opts.text) : '';//ensure it's a string.
      opts.text=opts.text.replace(/<br\s*\/{0,1}>/g,'\n');//line breaks are the only markup anticipated in buletins.
      opts.timestamp=moment().format('h:mm a');

      $.each($rootScope.bulletins,function(index,b){
        //If other bulletin has the same transaction id, remove it.
        //For url ids, don't consider the parameters.
        if(opts.id && b.id && b.id.toString().split(/\?/)[0]==opts.id.toString().split(/\?/)[0]){
          $rootScope.bulletins.splice(index,1);
          var i=$rootScope.busyRequests.indexOf(opts.id);
          if(i >= 0) $rootScope.busyRequests.splice(i,1);
          return false;//break out of the loop
        }
      });

      var bulletin = new Bulletin(new Bulletin(opts));
      $rootScope.bulletins.push(bulletin);
      $rootScope.lastOpenBulletin=bulletin;
      bulletin.startRemovalTimer();
      $rootScope.setToggleNavBarRight('LastOpenNotification');
      return bulletin;
    },
    removeBulletinById: function (id) {
      var indexToRemove=null;
      for(var i in $rootScope.bulletins){
        if($rootScope.bulletins[i].id==id){
          indexToRemove=i;
          break;
        }
      }
      $rootScope.bulletins.splice(indexToRemove,1);
      $rootScope.lastOpenBulletin = $rootScope.bulletins.slice(-1)[0];
    },
    clear: function () {
      $rootScope.bulletins = [];
      delete $rootScope.lastOpenBulletin;
    },
  };
}])
.filter('reverse', function() {//works on a copy, preserving the original order or the array
  return function(items) {
    return items.slice().reverse();
  };
})
.filter('noSpin', function() {
  return function(item) {
    if(typeof item === 'undefined' || item==null) { return; }
    return item.toString().replace(/(fa-pulse)/g,'');
  }
})
;