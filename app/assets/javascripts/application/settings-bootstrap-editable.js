// set defaults for X-editable Bootstrap
$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.showbuttons = false;
$.fn.editable.defaults.emptytext = 'empty';
$.fn.editable.defaults.onblur = 'submit';
$.fn.editable.defaults.toggle = 'click';
$.fn.editable.defaults.ajaxOptions = {type:'put'};
$.fn.editable.defaults.error=function(xhr, status, error) {eval(xhr.responseText);return '(See message at top of screen.)';}

/* send AJAX data as params['editable'][field] = value
  This should be implemented as $.fn.editable.defaults.params after compatibility
  has been assured for all instances of X-editable in our application. This entails
  altering the #update methods and all controllers that responds to X-editable requests
  as follows:
  => @obj.update_attributes(params[obj_name] || params[:editable])

  Until that time, this function can be used manually in any editable() call as
  follows:
  => $(selector).editable({params:window.config.xEditableParams})
*/
if (typeof(window.config) != 'object') window.config = {};
window.config.xEditableParams = function(params){
  var data = {};
  data[params.name] = params.value;
  return {editable:data, pk:data.pk};
}

/* alias chain $.fn.editable
  this chain produces the behaviour that focus fires a click */
$.fn._chainedEditable = $.fn.editable;
$.fn.editable = function(option){
	this.on('focus',function(){
		$(this).mousedown();
	});
	this._chainedEditable(option);
};
$.fn.editable.defaults = $.fn._chainedEditable.defaults;