insureioNg.controller('LAndCCtrl', ["$scope", "$http", "$uibModal", "bulletinService",
  function($scope, $http, $uibModal, bulletinService) {
  $scope.init=function(){
    $scope.lCStateVars={};
    $scope.person.$commission_level_updated_at=new Date($scope.person.commission_level_updated_at);
    $scope.getLicenses();
    $scope.getContracts();
  }

  $scope.getLicenses=function(){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading licenses.', id:transactionId});
    $http.get('/usage/licenses.json?user_id='+$scope.person.id)
    .success(function(data, status, headers, config){
      $scope.licenses = data;
      $rootScope.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load licenses. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.getContracts=function(){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading contracts.', id:transactionId});
    $http.get('/usage/contracts.json?user_id='+$scope.person.id)
    .success(function(data, status, headers, config){
      $scope.contracts = data;
      $rootScope.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load contracts. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.openLicenseForm=function(license){
    $scope.lCStateVars.activeLicense= license || {};

    $uibModal.open({
      templateUrl: '/l_and_c/modal_for_license_form.html',
      controller : 'licenseModalCtrl',
      scope      : $scope
    });
  }

  $scope.openContractForm=function(contract){
    $scope.lCStateVars.activeContract= contract || {};

    $uibModal.open({
      templateUrl: '/l_and_c/modal_for_contract_form.html',
      controller : 'contractModalCtrl',
      scope      : $scope
    });
  }

  //This object and function are needed for the xeditable form to properly invoke the uibootstrap popup picker.
  $scope.dtPickerOpened = {};
  $scope.dtPickerOpen = function($event, elementOpened) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.dtPickerOpened[elementOpened] = !$scope.dtPickerOpened[elementOpened];
  };

  $scope.person.$promise.then(function(){ $scope.init(); });
}])
.controller('licenseModalCtrl', ["$scope", "$http", "$uibModalInstance", 'requestSanitizer', 'bulletinService',
  function($scope, $http, $uibModalInstance, requestSanitizer, bulletinService) {
  $scope.license=$.extend(true,{},$scope.lCStateVars.activeLicense);
  //

  $scope.saveLicense=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        lCopy        =$.extend(true,{user_id:$scope.person.id},$scope.license);
        httpMethod   =lCopy.id ? 'put' : 'post',
        url          =lCopy.id ? '/usage/licenses/'+lCopy.id+'.json' : '/usage/licenses.json',
        params       ={usage_license: requestSanitizer.clean(lCopy) };

    $rootScope.addBulletin({text:'Saving license.', id:transactionId});

    $http[httpMethod](url, params)
    .success(function(data, status, headers, config){
      $scope.getLicenses();
      $uibModalInstance.close();
      $rootScope.addBulletin({text:'Successfully saved license.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save license. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

}])
.controller('contractModalCtrl', ["$scope", "$http", "$uibModalInstance", 'requestSanitizer', 'bulletinService',
  function($scope, $http, $uibModalInstance, requestSanitizer, bulletinService) {
  $scope.contract=$.extend(true,{},$scope.lCStateVars.activeContract);
  $scope.allStates=[];
  for(var i in STATES){ $scope.allStates.push(i); }
  //

  $scope.saveContract=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        cCopy        =$.extend(true,{user_id:$scope.person.id},$scope.contract);
        httpMethod   =cCopy.id ? 'put' : 'post',
        url          =cCopy.id ? '/usage/contracts/'+cCopy.id+'.json' : '/usage/contracts.json'
        params       ={usage_contract: requestSanitizer.clean(cCopy) };

    $rootScope.addBulletin({text:'Saving contract.', id:transactionId});

    $http[httpMethod](url, params)
    .success(function(data, status, headers, config){
      $scope.getContracts();
      $uibModalInstance.close();
      $rootScope.addBulletin({text:'Successfully saved contract.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save contract. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

}]);