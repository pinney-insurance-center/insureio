angular.module('DrStatusTypeMgmtApp', ['RestfulResource', 'ui.bootstrap'])
.controller("StatusTypeMgmtCtrl", ['$scope','$rootScope','UserModel','$resource','$http','$uibModal','$filter','bulletinService',
function ($scope,$rootScope,UserModel,$resource,$http,$uibModal,$filter,bulletinService) {
  $scope.GLOBAL_ID=1;
  $scope.USER_ID=2;
  $scope.USERS_ID=3;

  $scope.init=function(){
    $scope.scopeVars={};
    $scope.scopeVars.forPolicies=!window.location.search.match(/for_users=true/);
    $scope.scopeVars.newStatusTypeTemplate={
      ownership_id:3,/*user & descendants*/
      owner_id:$rootScope.currentUser.id,
      $owner:{
        id:$rootScope.currentUser.id,
        full_name:$rootScope.currentUser.full_name
      },
      status_type_category_id:($scope.scopeVars.forPolicies ? 1 : 16),
      is_for_policies:$scope.scopeVars.forPolicies,
      color:'#fff',
    };
    $scope.scopeVars.newTaskBuilderTemplate={
      owner_type: "Crm::StatusType",
    };
    if($scope.scopeVars.forPolicies){
      $scope.salesStages=$scope.SALES_STAGES;
      $scope.categories=$filter('whereKeyMatchesValue')($scope.STATUS_CATEGORIES,'is_for_policies', true);
    }else{
      //When sales stages are added for users/recruits,
      //those will be their own enum class.
      $scope.salesStages=[];
      $scope.categories=$filter('whereKeyMatchesValue')($scope.STATUS_CATEGORIES,'is_for_policies', false);
    }
    $scope.salesStages.push({id:null,name:'No Stage Indicated'});
    $scope.categories.push( {id:null,name:'No Category Indicated'});
    //Add `salesStageName` to each category, for use in the select menu.
    for(var i in $scope.categories){
      var cat=$scope.categories[i];
      for(var j in $scope.salesStages){
        var ss=$scope.salesStages[j];
        if(cat.sales_stage_id==ss.id){
          cat.salesStageName=(ss.id ? 'Stage '+ss.id+': ' : '')+ss.name;
          break;
        }
        if(cat.salesStageName){ next; }
      }
    }

    $scope.getStatusTypes(!$scope.scopeVars.forPolicies).$promise.then($scope.getStatusTypesCallback);
  }

  $scope.getStatusTypesCallback=function(){
    var stResourceName=$scope.scopeVars.forPolicies ? 'policyStatusTypes' : 'userStatusTypes';
    //Keep a reference to `$rootScope.userStatusTypes` or `$rootScope.policyStatusTypes`
    //within this scope with a short name for clarity.
    $scope.statusTypes=$rootScope[stResourceName];
    $scope.formatOwnerNames();
  }

  $scope.refreshStatusTypesForSingleCategory=function(categoryId){
    var transactionId=$rootScope.randomNonZeroId(),
        url='/crm/status_types.json';
    if(!params){ params={}; }
    params.include_owner_name=true;
    params.status_type_category_id=categoryId;
    if(!$scope.scopeVars.forPolicies){ params.for_users=true; }

    $rootScope.addBulletin({text:'Reloading status rules for this category.', id:transactionId});

    $scope.statusTypesRequest=$http.get(url,{params:params})
    .success(function(data, status, headers, config){
      $rootScope.removeBulletinById(transactionId);
      //Remove the records that are being refreshed.
      var indicesToRemove=[];
      for(var i in $scope.statusTypes){//collect records in specified category
        if($scope.statusTypes[i].status_type_category_id==params.status_type_category_id){
          indicesToRemove.push(i);
        }
      }
      for(var i in indicesToRemove){//remove each record in specified category
        var index=indicesToRemove[indicesToRemove.length-1-i];//work backwards from the end of the list
        $scope.statusTypes.splice(index,1);
      }
      //Add the new data to the array, changing it in place, so we don't lose the reference.
      Array.prototype.push.apply($scope.statusTypes, data);

      $scope.formatOwnerNames();
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to reload status rules for this category. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.formatOwnerNames=function(){
    for(var i in $scope.statusTypes){
      $scope.statusTypes[i].$owner={full_name:$scope.statusTypes[i].owner_name};
      delete $scope.statusTypes[i].owner_name;
    }
  }
  $scope.adjustSortOrder=function(statusType,direction){
    //call controller action of same name.
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Adjusting sort order.', id:transactionId});

    $http.post('/crm/status_types/'+statusType.id+'/adjust_sort_order.json',{direction:direction})
    .success(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Successfully adjusted sort order.', klass:'success', id:transactionId});
      var oldIndex=$scope.statusTypes.indexOf(statusType),
          newIndex=direction=='+' ? oldIndex+1 : oldIndex-1;
      $scope.statusTypes.splice(oldIndex,1);//remove from list
      $scope.statusTypes.splice(newIndex,0,statusType);//add back to list
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to adjust sort order. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
  $scope.openEditModal=function(statusType){
    if(!statusType){ statusType=$scope.scopeVars.newStatusTypeTemplate; }
    $uibModal.open({
      resolve: { origStatusType: function () { return statusType; } },
      templateUrl: '/crm/status_types/edit_modal.html',
      controller : 'editModalCtrl',
      scope      : $scope
    });
  }
  $scope.openStatusForNewPoliciesModal=function(statusType){
    $uibModal.open({
      templateUrl: '/crm/status_types/status_for_new_policies_modal.html',
      controller : 'statusForNewPoliciesModalCtrl',
      scope      : $scope,
      resolved   : {}
    });
  }
  $scope.getTasksForStatus=function(statusType){
    if(statusType){ $scope.scopeVars.activeStatusType=statusType; }
    $rootScope.$broadcast('fetchTaskBuilders');//broadcast event is caught in `Sequence.TaskBuildersCtrl`
    $scope.scopeVars.showTasks=true;
  }

  //`$rootScope.currentUser` is essential to this view,
  //so nothing should be initialized until it is resolved.
  $rootScope.getCurrentUser().$promise.then(function(){
    $scope.init();
  });
}])
.controller('statusForNewPoliciesModalCtrl', ['$scope', '$http', '$q', '$uibModal',
  function ($scope, $http, $q, $uibModal) {
    //
  }
])
.controller("editModalCtrl", ['$scope','$rootScope','UserModel','$http','$uibModal','$uibModalInstance','$filter','$resource','origStatusType','requestSanitizer','bulletinService',
function ($scope,$rootScope,UserModel,$http,$uibModal,$uibModalInstance,$filter,$resource,origStatusType,requestSanitizer,bulletinService) {
  //Work on a copy, so the form can be reset,
  //and so unsaved changes are not reflected in the page when the modal closes.
  $scope.statusType=$.extend(true,{},origStatusType);
  $scope.resetEditForm=function(){
    $scope.statusType=$.extend(true,{},origStatusType);
    $scope.scopeVars.editForm.$setPristine();
  }
  $scope.updateSSFromCat=function(catId){
    var selectedCat=$filter('whereKeyMatchesValue')($scope.categories, 'id', catId)[0];
    $scope.statusType.sales_stage_id=selectedCat.sales_stage_id;
  }
  $scope.getOwnerOptions = function (searchTerm) {
    if (searchTerm && searchTerm.length) {
      $scope.ownerOptions = $resource('/contacts.json',
        {person_type:'User',exclude_recruits:true,'select[]':['id','full_name'],search_by:'name_or_id'}
      ).query({search_term: searchTerm});
    }
  }
  $scope.saveChanges=function(statusType){
    var url='/crm/status_types'+(statusType.id ? '/'+statusType.id+'.json' : '.json'),
        saveMethod=statusType.id ? 'put' : 'post',
        transactionId=$rootScope.randomNonZeroId(),
        attrBlackList=['note_to_users'],
        statusTypeCopy=$.extend(true,{},statusType);

    requestSanitizer.clean(statusTypeCopy,attrBlackList,true);

    $rootScope.addBulletin({text:'Saving status rule "'+statusType.name+'".', id:transactionId});

    $http[saveMethod](url,{crm_status_type: statusTypeCopy})
    .success(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Successfully saved status rule "'+statusType.name+'".', klass:'success', id:transactionId});
      if(statusType.id){
        $scope.statusTypes.splice($scope.statusTypes.indexOf(origStatusType),1);//remove from list
        //Reload just this category. Difficult to determine new sort order without reloading them.
        $scope.refreshStatusTypesForSingleCategory(statusType.status_type_category_id);
      }else{
        statusType.id=data.object.id;
        statusType.$owner={full_name:$rootScope.currentUser.full_name};
        var indexOfLastInCategory=0;
        for(var i in $scope.statusTypes){
          if($scope.statusTypes[i].status_type_category_id==statusType.status_type_category_id){
            indexOfLastInCategory=i;
          }
        }
        $scope.statusTypes.splice(indexOfLastInCategory+1,0,statusType);
      }

      $uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save status rule "'+statusType.name+'". '+(data.errors||''), klass:'danger', id:transactionId});
    });

  }
}]);
