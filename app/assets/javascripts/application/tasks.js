angular.module('TasksApp',['ui.bootstrap'])
.controller('newTaskModalCtrl', ['$scope', '$http', '$location', '$uibModalInstance', '$controller', '$q', '$timeout', 'pId', 'pType', 'pName', 'requestSanitizer', 'bulletinService',
  function ($scope, $http,  $location, $uibModalInstance, $controller, $q, $timeout, pId, pType, pName, requestSanitizer, bulletinService) {

    var today = new Date();
    today.setHours(today.getHours()+6);

    $scope.task={
      due_at: today,
      $assigned_to: {id: $scope.currentUser.id, full_name: $scope.currentUser.full_name},
      assigned_to_id: $scope.currentUser.id,
    };

    if(pId && pType){
      $scope.task.person_id=pId;
      if(pType=='Recruit'){ pType='User'; }//on the back end, recruits are still a subset of users.
      $scope.task.person_type=pType;
      $scope.task.$person={ person_id: pId, person_type: pType, full_name: pName };
    }

    $scope.searchTaskableContacts=function(searchString){
      if(searchString.length < 3){ return; }//min length prevents overly broad queries.
      var contacts=$scope.searchContacts(searchString);
      $scope.taskableContacts=[];
      contacts.$promise.then(function(){
        for(var i in contacts){
          if( contacts[i].person_type=='Consumer' ||
              contacts[i].person_type=='User' && contacts[i].role=='recruit'
          ){
            $scope.taskableContacts.push( contacts[i] );
          }
        }
      });
    }

    $scope.selectPersonForTask=function(p){
      $scope.task.person_id=p.person_id;
      $scope.task.person_type=p.person_type;
    }

    $scope.getPoliciesOrSubscriptions=function(relatesTo){
      if (relatesTo == 'marketing'){
        $scope.task.sequenceable_type = $scope.task.person_type;
        $scope.task.sequenceable_id   = $scope.task.person_id;
        $scope.task.origin_type       = 'Marketing::Subscription';
        $scope.task.origin_id         = null;
        $scope.getSubscriptions($scope.task.person_id, $scope.task.person_type, $scope, true);
      }else if(relatesTo == 'policy'){
        $scope.task.sequenceable_type = 'Crm::Case';
        $scope.task.sequenceable_id   = null;
        $scope.task.origin_type       = 'Crm::Status';
        $scope.task.origin_id         = null;
        $scope.consumerMgmtVars={consumerId: pId, scope: $scope};
        //Create an instance of `PolicyCtrl` which shares this scope,
        //so this controller can share its functions.
        $controller('PolicyCtrl',{$scope : $scope});
        //Store a reference to this scope, so that calls to `getPolicies`
        //that take place within subscopes will still store policies on this scope.
        $scope.consumerMgmtVars.scope=$scope;
        try{
          $scope.policies=$scope.getPolicies(true, $scope);
          $scope.policies.$promise.then(function(){//scrape them for product_type, face_amount, and health_class.

            if (!$scope.policies.data.length){ return; }

            $.each($scope.policies.data, function(index, policy) {
              //Keys starting with $ will be removed using `requestSanitizer.clean`, so they don't cause errors on save.
              policy.$productType = ($scope.policyProductType(policy) || 'n/a');
              policy.$faceAmount  = $scope.currentDetails(policy).face_amount ;
              policy.$healthClass = $scope.healthClass(policy) ;
            });
          });
        }catch(e){
          console.error('There was a problem trying to get policies.', e);
        }
      }else{
          $scope.task.sequenceable_type = $scope.task.person_type;
          $scope.task.sequenceable_id   = $scope.task.person_id;
          $scope.task.origin_type       = null;
          $scope.task.origin_id         = null;
      }
    }

    $scope.createNewTask=function(isValid){
      if(!isValid){
        $('#new_crm_task .ng-invalid').addClass("error_fields");
        return false;
      }

      var transactionId=$rootScope.randomNonZeroId(),
          taskCopy=$.extend(true,{},$scope.task),
          params={crm_task:taskCopy};

      requestSanitizer.clean(taskCopy);

      $rootScope.addBulletin({text:'Saving new task.', klass:'info', id:transactionId});
      
      $http.post('/tasks.json',params)
      .success(function(data, status, headers, config){
        var transactionId=$rootScope.randomNonZeroId()
        $rootScope.addBulletin({text:'Successfully saved new task.', klass:'success', id:transactionId});
        $rootScope.$broadcast('refreshCurrentTasks');
        $uibModalInstance.close();
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to save new task.'+(data.errors||''), klass:'danger', id:transactionId});
      });
    }//end of `$scope.createNewTask`

  }
]);
