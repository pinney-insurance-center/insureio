
//for each association type, add listeners for add/update
$.each(['address','web','phone','email-address'], function(i,assoc){
  var underscored       =assoc.replace('-','_');
  var underscored_plural=underscored.replace(/s$/,'se').replace(/$/,'s');

  $(document).on('click','#add-'+assoc+'.ajax',function(){//the link that shows the form
    addContactInfo($(this),underscored_plural);
  });
  $(document).on('click','#cancel-'+assoc+'-form',function(){//the link that cancels/removes the form
  	$(this).parent().remove();
  });
  $(document).on('click','#submit-'+assoc+'-form',function(){//the link that shows the form
    $(this).parent().submit();
  });
  $(document).on('ajax:success', '#new_'+underscored, function(e){//the form
  	updateViews();
  });
});

var personType= function(){
  console.error('FUNCTION `personType` IS DEPRECATED! CHECK FOR `$scope.consumerMgmtVars` INSTEAD.');
	if       ( $('#usage-user-id').length>0 ){
		return "User";
	}else if(  $('#crm-connection-id').length>0 ){
		return "Consumer";
	}else if(  $('#usage-brand-id').length>0 ){
		return "Brand";
	}else if(  $('#person-type').length>0 ){
    return $('#person-type').val();
  }
}
var personId= function(){
  console.error('FUNCTION `personId` IS DEPRECATED! CALL `$scope.person.id` INSTEAD.');
	if       ( $('#usage-user-id').length>0 ){
		return $('#usage-user-id').val();
	}else if(  $('#crm-connection-id').length>0 ){
		return $('#crm-connection-id').val();
	}else if(  $('#usage-brand-id').length>0 ){
		return $('#crm-brand-id').val();
	}else if(  $('#person-id').length>0 ){
    return $('#person-id').val();
  }
}

var contactId= function(){
  console.error('FUNCTION `contactId` IS DEPRECATED! CALL `$scope.person.id` INSTEAD.');
  if( $('#contact-id').length>0 ){
    return $('#contact-id').val();
  }
}
