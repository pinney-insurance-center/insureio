formatDatetime = function(dt) {
	if (dt === null || typeof(dt) !== "object")
			{return '';}
	var meridian = dt.getHours() < 12 ? 'AM' : 'PM';
	var hour = dt.getHours() % 12;
	if (hour == 0) {hour = 12;}
	var month = 1 + dt.getMonth();
	return 	zpad(month, 2) + '/' + zpad(dt.getDate(), 2) + '/'
					+ zpad(dt.getFullYear(), 4) + ' '
					+ zpad(hour, 2) + ':' + zpad(dt.getMinutes(), 2) + ' ' + meridian;
}

pad = function(text, length, separator) {
	if (separator == null)
		{separator = ' ';}
	text = text.toString();
	if (text.length < length) {
			padding = new Array(1 + length - text.length).join(separator);
			text = padding + text;
	}
	return text;
}

zpad = function(text, length) {
	return pad(text, length, '0');
}