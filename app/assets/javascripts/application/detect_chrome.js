//This script is needed in case our dependencies and/or our app code cannot be run.
//When the angular app runs, it will set `$scope.chromeDetected`.
function isChrome(){
  return window.navigator.userAgent.indexOf('Chrome')>-1;
}
function isIE10OrOlder(){
  return window.navigator.appVersion.indexOf('MSIE')>-1;
}

document.addEventListener('DOMContentLoaded', function(){
  if( !isChrome() ){
    var banner=document.getElementById('browser-recommendation-banner');
    var htmlString='\
        <div class="notification-container">\
          <span class="alert-icon">\
            <i class="fa fa-exclamation-circle"></i>\
          </span>\
          <strong>Alert:</strong>'+
          (isIE10OrOlder() ? '<a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads">Your browser is out of date!</a><br>' : '')+
          '\
          For security and compatibility, we highly recommend using the <a href="https://www.google.com/chrome/">Google Chrome browser</a>.\
          <a class="close" onclick="$rootScope.dismissedBrowserWarning=true; $rootScope.$apply();">\
            <i class="fa fa-times" style="position: absolute; top: 10px; right: 50px;"></i>\
          </a>\
        </div>\
    ';
    banner.innerHTML=htmlString;
    banner.setAttribute('style','display:block;');
  }
});

angular.module('detectChrome',[])
.run(['$rootScope',function($rootScope){
  //This module expects the html page to have already defined plain JS function `isChrome`
  //for browsers that support JS but not our app's dependencies or our app code itself.
  //This module just mirrors the value in a scope variable that can be accessed from the view
  //so that the browser compatibility banner can be displayed if needed.
  $rootScope.chromeDetected=window.isChrome();
}]);
