/*
  A place for miscellaneous Angular JS controllers,
  small enough not to justify separate files or separate modules,
  and not readily fitting into any existing modules.
  The rule of thumb for length used so far is <200 lines.
*/

if (typeof Array.prototype.remove !== 'function') {
  Object.defineProperty(Array.prototype, 'remove', {
    value: function (obj) {
      var i = this.indexOf(obj);
      if (i >= 0) this.splice(i, 1);
      return i >= 0;
    }
  });
}


angular.module('dr.controllers',['ui.bootstrap'])
.controller('DrToolCtrl', function ($scope, $uibModal) {

  $scope.open = function (url) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: url,
      controller: 'ModalInstanceCtrl',
      windowClass: 'modal-overflow-override',
    });
}})
.controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', 'bulletinService',
  function ($scope, $uibModalInstance, bulletinService) {
  $scope.close = function () {
    $uibModalInstance.dismiss('close');
  };
}])
.controller('SuggestionFormCtrl', ['$scope', '$http', 'bulletinService',
  function ($scope, $http, bulletinService) {
  $scope.submitSuggestion = function () {
    var transactionId=$rootScope.randomNonZeroId();

    $http('/help/post_suggestion')
    .success(function(data, status, headers, config){
      $scope.addBulletin({text:'Successfully sent your suggestion.', klass:'success', id:transactionId});
      $scope.suggestion='';
    })
    .error(function(data, status, headers, config){
      $scope.addBulletin({text:'Failed to send your suggestion. ' + (data.errors||''), klass:'danger', id:transactionId});
    });

  };
}])
.controller("SequencesCtrl", ['$scope','$rootScope','$location', '$resource', '$http', '$uibModal', '$timeout','requestSanitizer','bulletinService',
  function ($scope, $rootScope, $location, $resource, $http, $uibModal, $timeout, requestSanitizer, bulletinService) {
  var Sequence = $resource('/marketing/campaigns/:id.json', {id:'@id'}, {update: { method: 'PUT', isArray: false }}); // Per Ryan, in future, Campaign and StatusType should be merged into single entity
  var Search = $resource('/reporting/searches.json', { });

  $scope.init=function(){
    $scope.PLANNING_CATS=$rootScope.PLANNING_CATS.concat({id:null,name:'Any'});
    $scope.PRODUCT_CATS=$rootScope.PRODUCT_CATS.concat({id:null,name:'Any'});

    $scope.sequencesCtrlVars={
      pageChangeCallback: function () {//used for drPaginationLinks
        $scope.getSequences();
      },
      pageChangeCallee: true,//used for drPaginationLinks
    };

    $scope.searchFields=['product_category_id', 'planning_category_id','name','page','per_page'];
    $scope.readStateFromUrl();
    $scope.newSequence = new Sequence({
      owner_id: $scope.currentUser.id,
      $owner:{id: $scope.currentUser.id, full_name: $scope.currentUser.full_name}
    });
    $scope.getSequences();
    if($rootScope.immediatelyTriggerNewForm){
      $scope.toggleNewCampaignForm();
      $rootScope.immediatelyTriggerNewForm=false;
    }
    getSearches();
  }

  $scope.readStateFromUrl=function(){
    var params=$location.search();
    for(let i in $scope.searchFields){
      var f=$scope.searchFields[i],
          isDefined=((typeof params[f])!='undefined');
      $scope.sequencesCtrlVars[f]=isDefined ? params[f] : null;
      if(['name'].indexOf(f)==-1 && isDefined){
        $scope.sequencesCtrlVars[f]=parseInt($scope.sequencesCtrlVars[f]);
      }
    }
    if(!$scope.sequencesCtrlVars['page']){
      $scope.sequencesCtrlVars['page']=1;
    }
  }

  $scope.toggleNewCampaignForm=function(){
    $scope.toggle($scope.newSequence, true);
    $scope.sequencesCtrlVars.sequenceList.$promise.then(function(){
      $timeout(function(){
        var $content=$('#main-container [ng-controller] >.section-container>.section-content'),
          height=$content.height();
        $content.scrollTop(height);//scrolls to bottom, where the new campaign form is.
      }, 10);
    });
  };

  function getSearches () {
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading available searches.',klass:'info',icon:'fa fa-refresh fa fa-spin', id:transactionId});

    $scope.searches=Search.query(
      {},//params
      function(searches){//success
        $rootScope.removeBulletinById(transactionId);
        return searches;
      },
      function(response){//fail
        $rootScope.addBulletin({text:'Failed to load available searches.', klass:'danger', id:transactionId});
      }
    );
  }

  $scope.getSequences=function (resetPageNum) {
    var transactionId=$scope.randomNonZeroId(),
    params={paginate:true};
    //When filters are changed, page should be reset.
    if(resetPageNum){ $scope.sequencesCtrlVars.page=1 }
    for(let i in $scope.searchFields){
      var fieldName=$scope.searchFields[i];
      params[fieldName]=$scope.sequencesCtrlVars[fieldName];
      $location.search(fieldName,$scope.sequencesCtrlVars[fieldName]);
    }
    $scope.addBulletin({text:'Loading campaigns.', id:transactionId});

    $scope.sequencesCtrlVars.sequenceList=Sequence.get(params,
      $scope.buildNgResourceSuccessCallback('Loaded Campaigns',
      function (data, responseHeaders) {
        $scope.sequences=data.campaigns;
        $scope.sequencesCtrlVars.page = data.current_page;
        $scope.sequencesCtrlVars.pageCt = data.total_pages;
      }
    ));
    return $scope.sequencesCtrlVars.sequenceList;
  }

  $scope.saveSequence = function (sequence) {
    var url= sequence.id ? '/marketing/campaigns/'+sequence.id+'.json' : '/marketing/campaigns.json',
        httpMethod = sequence.id ? 'put' : 'post',
        attrs = $.extend({owner_id:$scope.getCurrentUser().id},sequence),
        transactionId= $rootScope.randomNonZeroId();

    requestSanitizer.clean(attrs,['created_at','updated_at','owner_name','_edit']);
    $rootScope.addBulletin({text:'Saving campaign.', id:transactionId});

    $http[httpMethod](url,{marketing_campaign:attrs})
    .success(function(data, status, headers, config){
      $scope.addBulletin({text:'Successfully saved campaign.', klass:'success', id:transactionId});
        if (!sequence.id) {
          $scope.newSequence = new Sequence(); // resets new form
          $scope.sequences.push(data); // adds the saved sequence to the list
        }
        else {
          $scope.toggle(sequence, '_edit', false);//closes edit form
        }
    })
    .error(function(data, status, headers, config){
      $scope.addBulletin({text:'Failed to save campaign. ' + (data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.destroySequence = function (sequence,index) {
    $rootScope.destroyRecord(index,$scope.sequences,'/marketing/campaigns/'+sequence.id+'.json','campaign');
  }

  $scope.openEnrollConsumerModal=function(sequence){
    $scope.enrollModal = $uibModal.open({
        resolve: { sequence: function () { return sequence } },
        templateUrl: '/marketing/campaigns/modal_for_enroll_consumers.html',
        controller : 'enrollConsumersModalCtrl',
        scope      : $scope
    });
  }

  $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });
}
])
.controller('enrollConsumersModalCtrl', ['$scope', '$http', '$uibModalInstance', 'sequence', 'bulletinService',
  function ($scope, $http, $uibModalInstance, sequence, bulletinService) {
    $scope.sequence = sequence;

    $scope.getConsumers = function(sequence, reporting_search_id, callback){
      if (sequence.checked == 0 || sequence.checked == null)
        return;
      var transactionId=$scope.randomNonZeroId();
      $rootScope.addBulletin({text:'Acquiring consumers list.',klass:'info',icon:'fa fa-refresh fa fa-spin', id:transactionId});
      if ($scope.consumers == undefined)
        $scope.consumers = {};
      $http.get(('/reporting/searches/' + reporting_search_id + '/get_consumers_for_subscription.json'), {})
      .success(function(data, status, headers, config){
        $scope.consumers.consumer_summary = data;
        $scope.addBulletin({text:'Successfully gathered consumers list.', klass:'success', id:transactionId});
        callback($scope.consumers.consumer_summary, null)
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to gather consumers list.' + (data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.buildConsumerList = function(sequence, reporting_search_id){
      $scope.getConsumers(sequence, reporting_search_id, $scope.updateFailedList);
     }

    $scope.enrollConsumers = function(sequence){
      $scope.subscribeConsumers(sequence, $scope.consumers.consumer_summary.filtered_consumer_ids,$scope.updateFailedList);
    }

    $scope.subscribeConsumers = function(sequence, filtered_consumer_ids, callback){
      if (sequence.checked == 0 || sequence.checked == null)
        return;
      if($scope.subscriptionInProgress){ return; }
      $scope.subscriptionInProgress=true;
      var transactionId=$scope.randomNonZeroId();
      $rootScope.addBulletin({text:'Processing consumers list.',klass:'info',icon:'fa fa-refresh fa fa-spin', id:transactionId});
      $http.post(('/marketing/campaigns/' + sequence.id + '/subscribe_consumers.json'),
        {filtered_consumer_ids: filtered_consumer_ids, created_via_report_id: (sequence.reporting_search||{}).id}
      )
      .success(function(data, status, headers, config){
        $scope.consumers.enroll_summary = data;
        $scope.subscriptionInProgress=false;
        $scope.addBulletin({text:'Successfully subscribed consumers.', klass:'success', id:transactionId});
        callback($scope.consumers.consumer_summary, $scope.consumers.enroll_summary)
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to process consumers list.' + (data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.updateFailedList = function(consumer_summary, enroll_summary){
      var consumer_data = consumer_summary.consumer_data,
          failed_list={};
        $.each(consumer_summary.skipped_ids, function(index, id){
        failed_list[id]={}
        failed_list[id]['name']=consumer_data[id]['name']
        failed_list[id]['message'] = "Consumers has unsubscribed from marketing notifications"
      });
      if (enroll_summary && enroll_summary['unsuccessful']){
        $.each(enroll_summary['unsuccessful'], function(id, message){
          failed_list[id]={}
          failed_list[id]['name']=consumer_data[id]['name'];
          failed_list[id]['message'] = message;
        });
      }
      $scope.consumers.failed_count = Object.keys(failed_list).length;
      $scope.consumers.failed_list = failed_list;
    }

  }
])
.controller("Sequence.TaskBuildersCtrl", ['$scope','$resource','$filter','bulletinService',
  function ($scope, $resource, $filter, bulletinService) {
    var TaskBuilder = $resource('/crm/task_builders/:id.json?', {id:'@id'}, {update: { method: 'PUT', isArray: false }});
    $scope.newBuilder = new TaskBuilder();
    $scope.TO_DO_TASK_TYPE_ID=$filter('whereKeyMatchesValue')($scope.TASK_TYPES,'name','to-do')[0].id;

    $scope.$on('fetchTaskBuilders', function(e) {
      $scope.fetchTaskBuilders();
    });
    $scope.fetchTaskBuilders= function () {
      if( typeof($scope.sequence)!='undefined' ){
        //This controller resides within `SequencesCtrl`.
        $scope.ownerType='Marketing::Campaign';
        $scope.ownerId=$scope.sequence.id;
      }else{
        //This controller resides within `StatusTypeMgmtCtrl`.
        $scope.ownerType='Crm::StatusType';
        $scope.ownerId=$scope.scopeVars.activeStatusType.id;
      }
      $scope.taskBuilders = TaskBuilder.query({owner_id:$scope.ownerId, owner_type:$scope.ownerType, includes:1},
        $scope.buildNgResourceSuccessCallback('Loaded TaskBuilders', function (data) {
          // create fake attributes so that the autocomplete will show the initially selected names (if any)
          for (var i in $scope.taskBuilders) {
            var builder = $scope.taskBuilders[i];
            builder._assigned_to = {full_name:builder.assigned_to_name};
            builder._template = {name:builder.template_name};
          }
          sort();
        })
      );
    }
    function sort () {
      $scope.taskBuilders.sort(function (a, b) {
        return (a.delay||0) - (b.delay||0);
      });
    }
    $scope.toggleTaskBuilders = function () {
      $scope.$parent.toggle($scope.sequence);
      if ($scope.isToggled($scope.sequence) && !$scope.taskBuilders)
        $scope.fetchTaskBuilders();
    }

    $scope.saveBuilder = function (builder) {
      var fn = builder.id ? 'update' : 'save';
      var attrs = {owner_type:$scope.ownerType, owner_id:$scope.ownerId};
      var keysFromGuiToSave=['id', 'assigned_to_id', 'delay', 'evergreen', 'label', 'role_id', 'task_type_id', 'template_id', 'skip_holidays_and_weekends'];
      jQuery.each(keysFromGuiToSave, function (index, key) {
        attrs[key] = builder[key];
      });
      TaskBuilder[fn]({crm_task_builder:attrs, id:builder.id},
        $scope.buildNgResourceSuccessCallback('Saved TaskBuilder', function (data) {
          if (!builder.id) {
            $scope.newBuilder = new TaskBuilder();
            $scope.taskBuilders.push(data);
            sort();
            builder = data;
          }
          else {
            $scope.toggle(builder, '_edit', false);
          }
          // kludge to apply template name to view
          if (builder.template_id) {
            for (var i = 0; i < $scope.templateOptions.length; i++) {
              if ($scope.templateOptions[i].id == builder.template_id) {
                builder.template_name = $scope.templateOptions[i].name;
                break;
              }
            }
          }
        }),
        $scope.buildNgResourceFailCallback('Failed to save TaskBuilder')
      );
    }
    $scope.destroyBuilder = function (builder) {
      if (builder.id)
        builder.$delete(
          $scope.buildNgResourceSuccessCallback('Destroyed TaskBuilder', function (data) {
            $scope.taskBuilders.remove(builder);
          }),
          $scope.buildNgResourceFailCallback('Failed to destroy TaskBuilder')
        );
      else
        $scope.taskBuilders.remove(builder);
    }
  }
])
.controller("MembershipLevelsCtrl", ['$scope', '$resource','$http', '$filter', 'bulletinService',
  function ($scope, $resource, $http, $filter, bulletinService) {
    $scope.orderedMembershipLevels=$filter('orderBy')(MEMBERSHIPS,['pricing','id']);

    $scope.getCounts=function(userId){
      var transactionId=$rootScope.randomNonZeroId();
      $rootScope.addBulletin({text:'Getting counts.', id:transactionId});
      $scope.totalRevenue=null;
      $scope.counts=$resource('/users/membership_levels.json',{user_id:userId}).get(
        function (data) {//success
          $scope.calcTotalRevenue();
          $rootScope.removeBulletinById(transactionId);
        },
        function () {//error
          $scope.addBulletin({text:'Failed to get counts.', klass:'danger'});
        }
      );
    }
    $scope.calcTotalRevenue=function(){
      var total=0;
      for(let key in $scope.counts){
        if( isNaN(key) ){ continue; }
        var priceForLevel=$filter('whereKeyMatchesValue')(MEMBERSHIPS,'id',key)[0].pricing,
            revForLevel=priceForLevel*$scope.counts[key];
        total+=revForLevel;
      }
      $scope.totalRevenue='$'+total;
    }

  }
])
.controller('PermissionCtrl', ['$scope', '$http', '$resource', 'UserModel', 'bulletinService',
  function ($scope, $http, $resource, UserModel, bulletinService) {

    $scope.init=function(){
      $scope.permissionCtrlVars={};
      //`userScopeVars` is defined in "User.PersonalCtrl", which should always be a parent scope for this controller.
      $scope.permissionCtrlVars.origUser=$scope.userScopeVars.agent;
      $scope.permissionCtrlVars.showForm=true;
      $scope.initOrResetForms();

      $scope.subscriptionLevels=[];
      //Add user's current subscription level to the menu.
      var id=$scope.permissionCtrlVars.origUser.membership_id;
      if( [-2,-1,5,0].indexOf( id )==-1 ){
        for(let i in MEMBERSHIPS){
          if(MEMBERSHIPS[i].id==id){
            $scope.subscriptionLevels.push(MEMBERSHIPS[i]);
          }
        }
      }
      $scope.permissionSets=[];
      for(let i in MEMBERSHIPS){
        if(MEMBERSHIPS[i].pricing==0){
          $scope.subscriptionLevels.push(MEMBERSHIPS[i]);
        }else{
          $scope.permissionSets.push(MEMBERSHIPS[i]);
        }
      }
    }

    $scope.initOrResetForms=function(){
      //Make a local copy so changes are not reflected outside this scope until they are saved.
      $scope.permissionCtrlVars.user=new UserModel( $.extend(true,{},$scope.permissionCtrlVars.origUser) );
      var u=$scope.permissionCtrlVars.user;
      for(let i in PERMISSIONS){
        u['can_strict_'+PERMISSIONS[i] ]=u.can(PERMISSIONS[i], null, true);
      }
    }

    $scope.confirmAndSetSubscription=function(){
      var msg="Are you sure you want to change this user's subscription level?",
          transactionId=$rootScope.randomNonZeroId(),
          params={user:{membership_id:$scope.permissionCtrlVars.user.membership_id}};

      if ( !$rootScope.can('edit_others_membership_id') || !confirm(msg) ){ return; }

      $http.put('/users/'+$scope.permissionCtrlVars.user.id+'.json', params )
      .success(function(data, status, headers, config){
        $scope.addBulletin({text:'Successfully changed subscription level.', klass:'success', id:transactionId});
        setTimeout(function(){ location.reload(); }, 750);
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to change subscription level.' + (data.errors||''), klass:'danger', id:transactionId});
      });
    }

    $scope.clearPermissions=function(){
      var u=$scope.permissionCtrlVars.user;
      for(let i in PERMISSIONS){
        u[ 'can_strict_'+PERMISSIONS[i] ]=false;
      }
    }
    // Fires when user changes 'Permission Set Level' selector
    $scope.setPermissionSwitches=function(){
      var u=$scope.permissionCtrlVars.user,
          newSetId = $scope.permissionCtrlVars.permissionSetToApply,
          oldSetId=$scope.permissionCtrlVars.appliedPermissionSet||u.membership_id;
      if (!newSetId) return;
      if(oldSetId){
        //Switch off permissions that are in the old set, but not in the new set.
        var newSet=PERMISSIONS_DEFAULTS[newSetId],
            oldSet=PERMISSIONS_DEFAULTS[oldSetId];
        for(let i in oldSet){
          if( newSet.indexOf( oldSet[i] )==-1 ){
            u['can_strict_'+oldSet[i] ]=false;
          }
        }
      }
      //Switch on permissions from the new set.
      for(let i in newSet){
        u['can_strict_'+newSet[i] ]=true;
      }
      $scope.permissionCtrlVars.appliedPermissionSet=newSetId;
    }

    $scope.savePermissions=function(){
      if($scope.permissionCtrlVars.miscChanged){
        var msg="An authorization code is needed to edit some of these permissions."+
                "Please enter it below or contact a System Admin for assistance.",
            password = prompt(msg);
        $scope.permissionCtrlVars.user.password=password;
      }

      var transactionId=$scope.randomNonZeroId(),
          params={user:{}};
      $rootScope.addBulletin({text:'Saving permissions.', id:transactionId});

      for(let key in $scope.permissionCtrlVars.user){
        if( key.match(/can_strict_\w+/) ){
          params.user[key]=$scope.permissionCtrlVars.user[key];
        }
      }
      if($scope.permissionCtrlVars.miscChanged){ params.password=$scope.permissionCtrlVars.user.password; }

      $http.put(('/users/'+$scope.permissionCtrlVars.user.id+'.json'), params)
      .success(function(data, status, headers, config){
          $scope.addBulletin({text:'Successfully saved permissions.', klass:'success', id:transactionId});
          setTimeout(function(){ location.reload(); }, 750);
      })
      .error(function(data, status, headers, config){
          $scope.addBulletin({text:'Failed to save permissions. '+(data.errors||data.error||''), klass:'danger', id:transactionId});
      });
    }

    $rootScope.getCurrentUser().$promise.then(function(){ $scope.init(); });

  }
])
.directive('pSwitch', [function(){

  return {
    restrict: 'E',
    scope: { permission: '=p'},
    transclude: true,
    template:function(element,attrs){
              var defaultNgDisabled='!$root.can(permission)',
                  defaultNgChange='';
              return "<label>\
                <input type=\"checkbox\"\
                  ng-model=\"$parent.permissionCtrlVars.user['can_strict_'+permission]\"\
                  ng-disabled=\""+( attrs.ngDisabledIf || defaultNgDisabled )+"\"\
                  ng-change=\""+( attrs.ngChangeCb || defaultNgChange )+"\">\
                <span ng-transclude></span>\
              </label><br>"
            },
    link: function (scope, element, attrs) {
      //
    }
  }
}])
.controller('smsModalCtrl', ['$scope', '$http', '$uibModalInstance', 'person', 'phone', 'bulletinService',
  function ($scope, $http, $uibModalInstance, person, phone, bulletinService) {
    $scope.smsModalScopeVars={};
    $scope.smsModalScopeVars.msg='';
    if(!$scope.person){ $scope.person=person; }
    $scope.phone=phone;

    $scope.charCountColor=function(){
      var l=$scope.smsModalScopeVars.msg.length,
          color=(l>0   && l<=140 && 'green') ||
                (l>140 && l<=160 && 'goldenrod') ||
                ('red');
      return color;
    }
    $scope.messageCount=function(){
      //Messages over 160 chars get divided into separate messages of 153 chars each.
      return Math.ceil($scope.smsModalScopeVars.msg.length/153);
    }
    $scope.isCharCountOutOfRange=function(){
      var l=$scope.smsModalScopeVars.msg.length;
      return l<1 || l>918;
    }
    $scope.send=function () {
      var errorMsg='',
          transactionId=$rootScope.randomNonZeroId(),
          params={ message: $scope.smsModalScopeVars.msg, phone_id: $scope.phone.id };

      if( $scope.isCharCountOutOfRange() ){
        errorMsg+='Your message is too long or too short to send';
      }
      if( !$scope.smsModalScopeVars.tcpaWarning ){
        errorMsg+='You must agree to follow TCPA Requirements in order to send.';
      }
      if(errorMsg.length){
        alert(errorMsg)
        return;
      }

      $http.post('/contacts/'+$scope.person.id+'/send_sms.json',params)
      .success(function(data){
        $rootScope.addBulletin({text:'Successfully sent SMS message.', klass:'success', id:transactionId});
        $uibModalInstance.close();
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to send SMS message. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
  }
])
.controller("SubmissionOptionsCtrl", ['$scope','$resource','$http','$filter','UserModel','bulletinService',
  function ($scope, $resource, $http, $filter, UserModel, bulletinService) {
  $scope.init=function(){
    $scope.getUserStaffAssignment();
    $scope.person.$submitBoth=($scope.person.submit_application && $scope.person.submit_referral);
  }

  $scope.getUserStaffAssignment=function(){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading staff assignment.', id:transactionId});
    $http.get('/usage/staff_assignments.json?owner_id='+$scope.person.id+'&owner_type=User')
    .success(function(data, status, headers, config){
      $scope.sA = data;
      $rootScope.removeBulletinById(transactionId);
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to load staff assignment. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.saveStaffAssignment=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        sA           =$scope.sA,
        sACopy       ={},
        httpMethod   =sA.id ? 'put' : 'post',
        url          =sA.id ? '/usage/staff_assignments/'+sA.id+'.json' : '/usage/staff_assignments.json';
    for(let k in sA){
      if( !k.match(/_name$/) ){//Exclude name fields.
        //Setting empty id keys to `null` instead of `undefined`
        //will allow a user to clear a role through the form if they choose.
         sACopy[k]=sA[k]||null;
      }
    }

    $rootScope.addBulletin({text:'Saving staff assignment.', id:transactionId});
    $http[httpMethod](url,{usage_staff_assignment:sACopy})
    .success(function(data, status, headers, config){
      $scope.getUserStaffAssignment();
      $rootScope.addBulletin({text:'Successfully saved staff assignment.', klass:'success', id:transactionId});
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save staff assignment. '+(data.errors||''), klass:'danger', id:transactionId});
    });
  }

  $scope.updateAOR=function(newValue){
    var successCb=function(){
          $scope.person.agent_of_record_id=$scope.person.$agentOfRecord.id;
          $scope.person.agent_of_record_name=$scope.person.$agentOfRecord.full_name;
        },
        failCb=function(){
          $rootScope.addBulletin({text:'Failed to update agent of record. '+(data.errors||''), klass:'danger'});
        };
    $scope.person.$updateWithCustomParams({key:'agent_of_record_id',value:newValue}, successCb, failCb);
  }

  $scope.init();
  }
])
.controller('RecruitmentCtrl', ['$scope', '$http', '$resource', 'bulletinService',
  function ($scope, $http, $resource, bulletinService){
    $scope.init=function(){
      $scope.recruitmentVars={};
      $scope.recruitmentVars.recipientEmail='';
      $scope.recruitmentVars.recipientName='';
    }
    $scope.sendInvitationEmail=function(){
      var transactionId= $rootScope.randomNonZeroId(),
          params={
            recipient_email:$scope.recruitmentVars.recipientEmail,
            recipient_name: $scope.recruitmentVars.recipientName
          };

      $rootScope.addBulletin({text:'Sending invitation email.', id:transactionId});

      $http.post('/usage/recruitment/user_invitation.json',params)
      .success(function(data, status, headers, config){
        $scope.addBulletin({text:'Successfully sent invitation email.', klass:'success', id:transactionId});
        $scope.recruitmentVars.recipientEmail='';
        $scope.recruitmentVars.recipientName='';
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to send invitation email. ' + (data.errors||''), klass:'danger', id:transactionId});
      });
    }

  }
])
.controller('MarketingTagFormCtrl', ['$scope', '$http', '$resource', 'bulletinService',
  function ($scope, $http, $resource, bulletinService){
    function isTagEqual(a, b) {
      return a.trim().toLowerCase() == b.trim().toLowerCase();
    }

    $scope.init=function(){
      if( window.location.pathname.match(/\/users\/common_tags/) ){
        //The object in question is current user's common tags.
        $scope.person=$scope.currentUser;
        $scope.tagUrl='/users/'+$scope.person.id+'/add_common_tags.json';
        if (!$scope.person.common_tags) $scope.person.common_tags = [];
        $scope.tagObj=$scope.person.common_tags;
      }else{
        //It is assumed that this controller always inherits from a scope that defines `person`.
        $scope.tagUrl =$scope.person.$personType=='Consumer' ? '/consumers/' : '/users/';
        $scope.tagUrl+=$scope.person.id+'/tag.json';
        if (!$scope.person.tags) $scope.person.tags = [];
        $scope.tagObj=$scope.person.tags;
      }
      $scope.tagSaveInProgress=false;
      $scope.tagRemoveInProgress=false;
    }

    $scope.removeTag=function(value,index){
      if(!$scope.person.id){
        $scope.tagObj.splice(index, 1);
        return;
      }

      if($scope.tagRemoveInProgress){ return; }

      $scope.tagRemoveInProgress=true;
      $http.post($scope.tagUrl,{delete_tag:value})
      .success(function(data, status, headers, config){
        $scope.tagObj.splice(index, 1);
      })
      .error(function(data, status, headers, config){
        $scope.addBulletin({text:'Failed to remove tag. ' + (data.errors||''), klass:'danger'});
      })
      .finally(function(){
        $scope.tagRemoveInProgress=false;
      })
    }

    $scope.addTags = function (tagValues) {
      if($scope.tagSaveInProgress){ return; }
      $scope.tagSaveInProgress=true;
      /* Skip tags that are already in the set */
      var alreadyInSet = tagValues.filter(function (value) { return $scope.tagObj.indexOf(value) >= 0 });
      tagValues = tagValues.filter(function (value) { return $scope.tagObj.indexOf(value) < 0 });
      if (alreadyInSet.length)
        $scope.addBulletin({text:'Skipping tag(s) already in set' + alreadyInSet.join(', '), klass:'warn'}, true);
      if (!tagValues.length) {
        $scope.tagSaveInProgress=false;
        return;
      }

      if ($scope.person.id) {
        return $http.post($scope.tagUrl, {tags: tagValues})
        .success(function(data, status, headers, config){
          $scope.addBulletin({text:'saved', klass:'success'});
          tagValues.forEach(function (value) { $scope.tagObj.push(value) });
        })
        .error(function(data, status, headers, config){
          $scope.addBulletin({text:'Failed to save tag(s) ' + tagValues.join(', ') + ' ' + (data.errors||''), klass:'danger'});
        })
        .finally(function(data, status, headers, config){
          $scope.tagSaveInProgress=false;
        });
      } else {
        $scope.tagSaveInProgress=false;
        tagValues.forEach(function (value) { $scope.tagObj.push(value) });
      }
    }

    $scope.addTag=function() {
      $scope.addTags([value]);
    }

    $scope.parseAndSaveNewTag=function(userHitEnterKey){
      var rawString=$scope.person.$newTag,
          isReadyToParse=rawString.includes(',')|| userHitEnterKey,
          newTags=rawString.split(',').filter(function(tagString){ return !!tagString.trim(); });
      if(!isReadyToParse || !newTags.length){ return; }
      $scope.person.$newTag = '';
      $scope.addTags(newTags);
    }

    $scope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller('MarketingLinksCtrl', ["$scope", '$rootScope', "$http", '$resource', '$location', 'bulletinService',
  function($scope, $rootScope, $http, $resource, $location, bulletinService) {
    $scope.init=function(){
      $scope.mktgLinksStateVars={};
      $scope.productLine=1;
      $scope.requestType=$rootScope.currentUser.can('submit_application') ? 'APPLICATION' : 'REFERRAL';
      $scope.brandId=$scope.currentUser.default_brand_id;
      $scope.brandForIdLink=null;
      $scope.getBrands();
    }

    $scope.getBrands=function (reload, params) {
      if (!$scope.brands || reload) {
        $scope.brands = $resource('/brands.json').query(
          $.extend({just_the_basics:true},params||{}),
          function(data, status, headers, config){//success
            $scope.brandForIdLink=data.filter(function(b){
              return b.id==$scope.currentUser.default_brand_id;
            })[0];
            $scope.updateIdLink();
          },
          function(data, status, headers, config){//error
            $scope.addBulletin({text:'Failed to load brands. '+(data.errors||''), klass:'danger'});
          }
        );
      }
      return $scope.brands;
    }

    //Note: ngChange directive fires before the model is changed,
    //so this function references `this.brandForIdLink` instead of `$scope.brandForIdLink`.
    $scope.updateIdLink=function(){
      if(!this.brandForIdLink){
        $scope.mktgLinksStateVars.idLink='';
        return;
      }
      var urlString='https://';
      urlString+=this.brandForIdLink.preferred_insurance_division_subdomain;
      urlString+='.insurancedivision.com/';
      urlString+=this.brandForIdLink.id+'/';
      urlString+=$scope.currentUser.insurance_division_id;//keep this logic internal to prevent tampering.

      $scope.mktgLinksStateVars.idLink=urlString;
    }

    $scope.getCurrentUser().$promise.then(function(){ $scope.init(); });
  }
])
.controller("FileMgmtCtrl", ['$scope', '$resource','$http', '$filter', '$uibModal', 'bulletinService',
  function ($scope, $resource, $http, $filter, $uibModal, bulletinService) {

    $scope.init=function(){
      var personType=$scope.person.$personType.replace('Recruit','User'),
          transactionId=$rootScope.randomNonZeroId();

      $scope.newAttachmentTemplate={
        person_type: personType,
        person_id:   $scope.person.id,
        case_id:null
      }

      $scope.targetPolicyOpts=[];

      $scope.addBulletin({text:'Loading attachments.', id:transactionId});
      $scope.attachmentsGroupedByTarget=$resource(
        '/attachments.json',
        {person_type:personType, person_id:$scope.person.id}
      ).get(
        function (data) {//success
          $scope.setUrlStrings(data);
          $scope.setTargetPolicyOptsForNewAttachment(data);
          $rootScope.removeBulletinById(transactionId)
        },
        function () {//error
          $scope.addBulletin({text:'Failed to load attachments.', klass:'danger', id:transactionId});
        }
      );
    }

    $scope.setUrlStrings=function(data){
      for(let targetName in data){
        let attachments=data[targetName];
        for(let i in attachments){
          let a=attachments[i];
          a.$urlString=a.is_recording ? 'recordings/'+a.id+'.'+a.extension : '/attachments/'+a.id+'.'+a.extension;
        }
      }
    }

    $scope.setTargetPolicyOptsForNewAttachment=function(){
      var targetNames=Object.keys($scope.attachmentsGroupedByTarget),
          targets=[];
      if(!targetNames.length){ return; }
      for(let i in targetNames){
        var tName=targetNames[i];
        if(tName=='Consumer'){ continue; }
        var numericIdMatchArray=tName.match(/Policy\s+\#\s+(\d+)\s+/),
            numericId=numericIdMatchArray ? numericIdMatchArray[1] : null;
        if(!numericId){ continue; }
        targets.push({id:numericId, name:tName});
      }
      targets.push({id:null,name:'Consumer'});
      $scope.targetPolicyOpts=targets;
    }

    $scope.toggleEditDesc=function(a){
      a.$editing=!a.$editing;
      a.$tmpDesc=a.description;//initialize/reset
    }

    $scope.updateDesc=function(a){
      if(a.$savingDesc){ return; }//one update at a time.
      a.$savingDesc=true;
      $http.put(a.$urlString.replace(a.extension,'json'),{attachment:{description:a.$tmpDesc}})
      .success(function(data, status, headers, config){
        a.$savingDesc=false;
        a.$editing=false;
        a.description=a.$tmpDesc;
      })
      .error(function(data, status, headers, config){
        a.$savingDesc=false;
        $rootScope.addBulletin({text:'Failed to save attachment. '+(data.errors||''), klass:'danger'});
      });
    }

    $scope.openNewAttachmentModal=function(){
      $uibModal.open({
        templateUrl: 'modal_for_new_attachment.html',//defined within `public/attachments/container.html`
        controller : 'newAttachmentModalCtrl',
        scope      : $scope
      });
    }

    $rootScope.getCurrentUser().$promise.then(function(){
      $scope.person.$promise.then(function(){
        $scope.init();
      });
    });
  }
])
.controller('newAttachmentModalCtrl', ['$scope', '$http', '$uibModalInstance', '$http', 'bulletinService',
  function ($scope, $http, $uibModalInstance, $http, bulletinService) {

    $scope.newAttachment=$.extend(true,{},$scope.newAttachmentTemplate);

    //From the input element, derive:
    //`$uploaderAsDataUrl` for previewing,
    //and `uploader` for uploading.
    $scope.readAndParseFile=function(){
      var a=$scope.newAttachment;
      a.uploader     =$('[ng-model="newAttachment.$uploader"]')[0].files[0];
      a.name         =a.uploader.name;
      a.size         =a.uploader.size;
      a.content_type =a.uploader.type;
      //define a fileReader object and callback.
      var fr = new FileReader();
      fr.onload = function (evt) {
        a.$uploaderAsDataUrl=fr.result;
        $scope.$apply();
      }
      //When finished, `readAsDataURL` will trigger the `onload` event,
      //which will execute the callback defined above.
      fr.readAsDataURL(a.$uploader[0]);
    }

    $scope.uploadNewAttachment=function(){
      var fd=new FormData(),
          transactionId=$rootScope.randomNonZeroId();
      fd.append('attachment[file_for_upload]',   $scope.newAttachment.uploader)
      fd.append('attachment[person_id]',  $scope.newAttachment.person_id);
      fd.append('attachment[person_type]',$scope.newAttachment.person_type);
      fd.append('attachment[case_id]',    $scope.newAttachment.case_id);


      $scope.addBulletin({text:'Saving attachment.', id:transactionId});
      $http.post('/attachments.json', fd, {
          transformRequest: angular.identity,//prevents transformation of data.
          headers: {'Content-Type': undefined}//allows browser to fill in this and the boundary parameter of the request.
      })
      .success(function(data, status, headers, config){
        var a                =$scope.newAttachment,
            targetGroup      =$scope.targetPolicyOpts.filter(function(o){ return o.id==a.case_id; })[0],
            nameOfTargetGroup=targetGroup.name;
        a.id=data.object.id;
        a.$urlString='/attachments/'+a.id;
        a.created_at='(Just Now)';
        $scope.person.attachments_count+=1;
        $scope.attachmentsGroupedByTarget[nameOfTargetGroup].push($scope.newAttachment);
        $uibModalInstance.close();
        $rootScope.addBulletin({text:'Successfully saved attachment.', klass:'success', id:transactionId});
      })
      .error(function(data, status, headers, config){
        $rootScope.addBulletin({text:'Failed to save attachment. '+(data.errors||''), klass:'danger', id:transactionId});
      });
    }
  }
])
;