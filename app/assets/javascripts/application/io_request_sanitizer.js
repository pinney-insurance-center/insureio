angular.module( 'ioRequestSanitizer', [] )
.provider('requestSanitizer', function requestSanitizerProvider () {
  this.$get = [ function requestSanitizerFactory () {
    return {
      // Remove nil-value attributes, empty arrays, & timestamps from object.
      // Any of these can cause an error in the back end.
      clean: function(object, blackListedAttrs, recursive, allowSpecialCharacters){
        if (!object || typeof object != "object") return;
        //If `blackListedAttrs` is defined as an array, including an empty one,
        //then it will not be changed.
        //Otherwise (if `null` or `undefined`), it will get these defaults.
        if(!blackListedAttrs){
          // remove timestamps by default
          blackListedAttrs=['created_at','updated_at']
        }
        // delete all blacklisted attributes
        for (let i in blackListedAttrs){
          if( blackListedAttrs[i] instanceof RegExp ){
            for (let key in object){//loop over object and delete matching keys
              if( key.match(blackListedAttrs[i]) ){
                delete object[key];
              }
            }
          }else{//delete exact matches
            delete object[blackListedAttrs[i]];
          }
        }
        for (let key in object){
          if(key=='tags' || key=='tags_attributes'){ continue; }//skip tags
          if( key.match(/^\$/) ){ delete object[key] }//remove an angular-provided key
          if( key.match(/s_count$/) ){
            //Remove counter cache fields, which all begin with the pluralized association names.
            //There are a few health condition fields that end in "count",
            //but those do not have a plural word before it, so they pass through ok.
            delete object[key];
          }
          //delete empty attribute hashes
          if(key.match(/_attributes$/) && (object[key]==null || !Object.keys(object[key]).length) )
            delete object[key];
          if( Array.isArray(object[key]) ){
            object[key]=object[key].filter(function( element ) {
              return element !== undefined;
            });
          }
          if( !allowSpecialCharacters && (typeof object[key]) == 'string' ){
            object[key]=object[key].replace(/[\u0250-\ue007<>]/g,'');//remove non-latin and <> characters.
          }
          if (object[key]!=null && typeof object[key] == 'object' && object[key].constructor != Date){
            if(Object.keys(object[key]).length == 0){// remove an empty array/object
              delete object[key];
            }else if(recursive){//recurse through a non-empty array/object
              this.clean(object[key], blackListedAttrs, true);
              if(Object.keys(object[key]).length == 0){// remove this array/object if it's empty after recursing
                delete object[key];
              }
            }
          }
        }
        return object;
      }
    };
  }]
});