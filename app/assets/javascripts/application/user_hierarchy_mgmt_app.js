angular.module('UserHierarchyMgmtApp', ['RestfulResource', 'ui.bootstrap'])
.factory("User", ['Resource', function ($resource) {
  //Angular will collapse the `/`s if no `actionName` or `id` is provided.
  return $resource('/users/:id/:actionName.json', {id:'@id'});
}])
.controller("UserHierarchyMgmtCtrl", ['$scope','$rootScope','$resource','$http','$uibModal','$filter','User','bulletinService',
function ($scope,$rootScope,$resource,$http,$uibModal,$filter,User,bulletinService) {
  $scope.init=function(){
    $scope.scopeVars={};
    $scope.scopeVars.newUserTemplate={
      addresses_attributes:[{address_type_id:3,$typeName:'Mailing'}],
      phones_attributes:[{phone_type_id:2}],
      emails_attributes:[{}],
      notes_attributes:[{}],
    };
    $scope.scopeVars.latestUserQuery=$scope.getUsers();

    $scope.getCurrentUser();
  }

  $scope.getUsers=function(parentUser) {
    if(!$scope.users){ $scope.users=[]; }
    var indexForInsertingUsers=parentUser ? $scope.users.indexOf(parentUser)+1 : 0,
        placeHolderUser={$isPlaceHolder:true,$parentUser:parentUser},
        transactionId=$rootScope.randomNonZeroId();

    $scope.users.splice(indexForInsertingUsers,0,placeHolderUser);

    $rootScope.addBulletin({text:'Loading users.',klass:'info',id:transactionId});

    var userSet=User.query(
      {id:(parentUser ? parentUser.id : null), actionName:'hierarchy_branch'},
      function(users){//success
        $rootScope.removeBulletinById(transactionId);

        var argsForSplice=[indexForInsertingUsers,1].concat(users);
        Array.prototype.splice.apply($scope.users,argsForSplice);//replace placeholder with actual users
        for(var i in users){
          if(parentUser){
            users[i].$parentUser=parentUser;
            users[i].$indent=(parentUser.$indent||0)+1;
          }
          users[i].$recentlyOnline=users[i].last_request_at && moment().diff( users[i].last_request_at, 'minutes')<30;
        }
        $scope.getCounts(users,parentUser);
      },
      function(response){//fail
        var msg='Failed to load users.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg,klass:'danger',id:transactionId});
      }
    );
    return userSet;
  }

  $scope.getCounts=function (users,parentUser){
    var transactionId=$rootScope.randomNonZeroId(),
        userIds=[];
    $rootScope.addBulletin({text:'Loading child and descendant counts.', id:transactionId});

    for(var i in users){
      userIds.push(users[i].id);
    }
    var counts=User.get(
      {actionName:'child_and_descendant_counts','user_ids[]':userIds},
      function(countsObject){//success
        $rootScope.removeBulletinById(transactionId);
        for(var i in users){
          users[i].immediateChildCount=countsObject.childCounts[users[i].id];
          users[i].descendantCount=countsObject.descendantCounts[users[i].id];
        }
      },
      function(response){//error
        var msg='Failed to load child and descendant counts.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
    //When multiple rows are toggled rapidly,
    //several requests may run concurrently.
    //Each user row will look at the `$parentUser` or `$scope`
    //to determine the status of the relevant server transaction.
    if(parentUser){
      parentUser.$counts=counts;
    }else{
      $scope.$counts=counts;
    }
  }
  $scope.fetchLineageBreadCrumb=function(user){
    var transactionId=$rootScope.randomNonZeroId();
    $rootScope.addBulletin({text:'Loading details for searched user.', id:transactionId});

    User.query(
      {actionName:'hierarchy_breadcrumb',id:user.id},
      function(data){//success
        $rootScope.removeBulletinById(transactionId);
        var moreUserDetails=data.pop(),
            breadcrumbArray=data,
            breadcrumbStr='';
        $.extend($scope.scopeVars.userForSearch, moreUserDetails );
        $scope.scopeVars.userForSearch.$isForSearch=true;
        $scope.scopeVars.userForSearch.name=$scope.scopeVars.userForSearch.full_name;
        for(var i=0; i<breadcrumbArray.length; i++){
          var user=breadcrumbArray[i];console.log('i='+i);
          breadcrumbStr+=user.name+' ('+user.id.toString(36).toUpperCase()+' / '+user.id+') > ';
        }
        $scope.scopeVars.userForSearch.$breadcrumb=breadcrumbStr;
        $scope.getCounts([$scope.scopeVars.userForSearch],$scope.scopeVars.userForSearch);
      },
      function(response){//error
        var msg='Failed to load details for searched user.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
  }

  $scope.isExpandable=function(user){
    return !user.$isForSearch && (user.can_strict_have_children || user.descendantCount);
  }
  $scope.highlightAscendantRows=function(user){
    user.$highlighted=true;
    if(user.$parentUser){
      $scope.highlightAscendantRows(user.$parentUser);
    }
  }
  $scope.unhighlightAscendantRows=function(user){
    user.$highlighted=false;
    if(user.$parentUser){
      $scope.unhighlightAscendantRows(user.$parentUser);
    }
  }
  $scope.toggleUser=function(user){
    if(!user.$expanded){
      $scope.getUsers(user);
    }else{//drop child users
      var indexOfUser=$scope.users.indexOf(user),
          users=$scope.users,
          usersToDrop=[];
      for(var i=indexOfUser+1;i<users.length;i++){
        if(users[i].$parentUser===user){
          if(users[i].$expanded){
            $scope.toggleUser(users[i]);//recursive call
          }
          usersToDrop.push(users[i]);
        }else{ break }
      }
      users.splice(indexOfUser+1,usersToDrop.length);
    }
    user.$expanded=!user.$expanded;
  }
  $scope.deactivateUser=function(user){
    var warningText='Deactivating this user account removes them from contact management, '+
      'lead distribution, and agent/staff assignment menus, immediately logs them out, and prevents them logging in. '+
      'It will NOT delete any descendant users, or brands/consumers/policies/tasks assigned to them. To continue, please '+
      'confirm that you have removed or reassigned all items assigned to this user.';

    if( !confirm(warningText) ){ return; }

    var transactionId=$rootScope.randomNonZeroId(),
        disabledMembershipId=$filter('whereKeyMatchesValue')( MEMBERSHIPS,'name','Disabled' )[0].id;
    $rootScope.addBulletin({text:'Deactivating user account.', id:transactionId});

    User.update({id:user.id},
      {
        user:{
          active:false,
          membership_id:disabledMembershipId,
        }
      },
      function(data){//success
        $rootScope.removeBulletinById(transactionId);
        user.active=false;
      },
      function(response){//error
        var msg='Failed to deactivate user account.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
  }
  $scope.restoreUser=function(user) {
    var warningText='Restoring this deactivated user account will make them visible in contact management, '+
      'lead distribution, and agent/staff assignment menus, and allow them to log in. It will set their '+
      'membership level to our most basic (unpaid) level until they choose a different level and reenter '+
      'their payment info. To continue, please confirm that you understand the consequences of this action.';

    if( !confirm(warningText) ){ return; }

    var transactionId=$rootScope.randomNonZeroId(),
        unpaidMembershipId=$filter('whereKeyMatchesValue')( MEMBERSHIPS,'name','EZLife' )[0].id;
    $rootScope.addBulletin({text:'Restoring basic functions to user account.', id:transactionId});

    User.update({id:user.id},
      {
        user:{
          active:true,
          membership_id:unpaidMembershipId
        }
      },
      function(data){//success
        $rootScope.removeBulletinById(transactionId);
        user.active=false;
      },
      function(response){//error
        var msg='Failed to restore basic functions to user account.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
  }

  $scope.openChangeLineageModal=function(user){
    $uibModal.open({
      resolve    : { user: function () { return user; } },
      templateUrl: '/users/change_lineage_modal.html',
      controller : 'changeLineageModalCtrl',
      scope      : $scope
    });
  }

  $scope.openNewUserModal=function(parentUser){
    $uibModal.open({
      resolve    : { parentUser: function () { return parentUser; } },
      templateUrl: '/users/new_user_modal.html',
      controller : 'newUserModalCtrl',
      windowClass: 'super-size',
      scope      : $scope
    });
  }

  $scope.init();
}])
.controller('newUserModalCtrl', ['$scope','$filter','$http','$uibModalInstance','parentUser','requestSanitizer','bulletinService',
  function ($scope, $filter, $http, $uibModalInstance, parentUser, requestSanitizer, bulletinService) {
  $scope.formHolder={};
  $scope.GLOBAL_ID=1;
  $scope.USER_ID=2;
  $scope.USERS_ID=3;
  $rootScope.getUserStatusTypes();
  $scope.userStatusTypeGroupName=function(item){
     return item.groupName;
  }   

  $scope.newUser=$.extend(true,{parent_id:parentUser.id,$parentUser:parentUser},$scope.scopeVars.newUserTemplate);
  $scope.resetNewUser=function(){
    $scope.newUser=$.extend(true,{parent_id:parentUser.id,$parentUser:parentUser},$scope.scopeVars.newUserTemplate);
    $scope.scopeVars.newUserForm.$setPristine();
  }
  $scope.saveNewUser=function(){
    var transactionId=$rootScope.randomNonZeroId(),
        newUserSanitizedCopy=$.extend(true,{},$scope.newUser);

    requestSanitizer.clean(newUserSanitizedCopy, null, true);

    $rootScope.addBulletin({text:'Saving new user.', id:transactionId});

    $http.post('/users.json',{user: newUserSanitizedCopy})
    .success(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Successfully saved new user with id '+data.object.id+'.', klass:'success', id:transactionId});
      if(!$scope.canCreateRecruits){//Not for Contacts view, only Hierarchy Mgmt. 
        if( !$rootScope.currentUser.can('super_edit') ){
          $rootScope.editableIds.push(data.object.id);
        }
        $scope.getCounts($scope.users);
        if(parentUser.$breadcrumb){
          parentUser.immediateChildCount++;
          parentUser.descendantCount++;
        }else{
          if(!parentUser.$expanded){
            $scope.toggleUser(parentUser);//expand
          }else{
            $scope.toggleUser(parentUser);//collapse
            $scope.toggleUser(parentUser);//reexpand
          }
        }
      }
      $uibModalInstance.close();
    })
    .error(function(data, status, headers, config){
      $rootScope.addBulletin({text:'Failed to save new user.'+(data.errors||''), klass:'danger', id:transactionId});
    });
  }
}])
.controller('changeLineageModalCtrl', ['$scope','$rootScope','$http','$uibModalInstance','User','user','bulletinService',
  function($scope, $rootScope, $http, $uibModalInstance, User, user, bulletinService){
  $scope.user=user;
  $scope.saveLineageChange=function(){
    var transactionId=$rootScope.randomNonZeroId();

    $rootScope.addBulletin({text:'Saving lineage change for user '+user.id+'.', id:transactionId});

    User.update({id:user.id},
      {user:{parent_id:user.$proposedParentUser.id} },
      function(response){//success
        $rootScope.addBulletin({text:'Successfully saved lineage change for user '+user.id+'.', klass:'success', id:transactionId});
        $uibModalInstance.close();
        if(user.$breadcrumb){
          $scope.fetchLineageBreadCrumb(user);
          return;
        }
        if(user.$parentUser){
          if(user.$parentUser.$expanded){
            $scope.toggleUser(user.$parentUser);//collapse old parent
          }
          delete user.$parentUser;//clear old parent linkage
        }else{
          var idx=$scope.users.indexOf(user);
          $scope.users.splice(idx,1);//take the user object out of the list
        }
        
        for(var i in $scope.users){
          if($scope.users[i].id==user.$proposedParentUser.id){//find new parent if in the view
            user.$parentUser=$scope.users[i];//set new parent
            delete user.$proposedParentUser;
            break;
          }
        }
        if(user.$parentUser){
          $scope.getCounts($scope.users);
          if(!user.$parentUser.$expanded){
            $scope.toggleUser(user.$parentUser);//expand
          }else{
            $scope.toggleUser(user.$parentUser);//collapse
            $scope.toggleUser(user.$parentUser);//reexpand
          }
        }
      },
      function(response){//error
        var msg='Failed to save lineage change for user '+user.id+'.'
        if(response.data){
          msg+=response.data.msg||response.data.error||response.data.errors;
        }
        $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
      }
    );
    
  }
}]);
