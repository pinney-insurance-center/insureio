$(function() {
  var collapse, iframe, link_show_btn, links, links_container;
  iframe = $("#links-iframe");
  links_container = $(".iframe-links");
  links = links_container.find("a[target!='_blank']");
  link_show_btn = $(".show-iframe-links");
  collapse = iframe.attr("data-iframe-collapses");
  links.on("click", function(e) {
    var src;
    if (e.isDefaultPrevented() || e.metaKey || e.ctrlKey || e.which === 2) {
      return;
    }
    if (collapse) {
      links_container.slideUp();
      link_show_btn.show();
    }
    src = $(this).attr("href");
    iframe.show();
    iframe.attr("src", src);
    e.preventDefault();
    return false;
  });
  link_show_btn.hide();
  return link_show_btn.on("click", function(e) {
    iframe.slideUp();
    link_show_btn.slideUp();
    links_container.slideDown();
    e.preventDefault();
    return false;
  });
});
