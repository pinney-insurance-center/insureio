insureioNg.controller('ContactMarketingCtrl', ["$scope", "$resource", "$http", "$uibModal", "bulletinService",
  function($scope, $resource, $http, $uibModal, bulletinService) {
    $scope.init=function(){
      $scope.personType=$scope.consumerMgmtVars ? 'Consumer' : 'User';
      //sets `groupedSubscriptions`, `campaigns`, and `nextQueueNumber` on the local scope
      $rootScope.getSubscriptions($scope.person.id, $scope.person.$personType, $scope);
      $scope.getSubsWTasks();
    }

    $scope.$on('refreshMarketingSection', function(e) {
      $scope.init();
    })

    $scope.saveSubscription=function(subscription,queueNum){
      var transactionId=$rootScope.randomNonZeroId();

      subscription.person_id=$scope.person.id;
      subscription.person_type=$scope.person.$personType;
      subscription.queue_num=queueNum|| $scope.nextQueueNumber;

      $rootScope.addBulletin({text: 'Saving campaign subscription.', id: transactionId});
      $http.post('/marketing/subscriptions.json',{marketing_subscription:subscription})
      .success(function (data, status, headers, config) {
        subscription.id=data.id;
        angular.extend(subscription,data.object);
        if(subscription.queue_num==$scope.nextQueueNumber){
          $scope.nextQueueNumber++;
        }
        $rootScope.addBulletin({text: 'Successfully saved campaign subscription.', klass:'success', id: transactionId});
        $rootScope.refreshCurrentTasks();
      })
      .error(function (data, status, headers, config) {
        $rootScope.addBulletin({text: 'Failed to save campaign subscriptions. ' + (data.errors || ''), klass: 'danger', id: transactionId});
      });
    }

    $scope.updateOrder=function(subscription,newOrderNum){
      //Needs to be re-implemented with Angular.
      //Will likely post to existing controller action:
      //'/marketing/subscriptions/update_order_for_queue'
    }

    $scope.getSubsWTasks=function(){
      $scope.subsWTasks=$resource('/marketing/subscriptions.json').get(
        {person_id: $scope.person.id, person_type: $scope.person.$personType.replace('Recruit','User'), with_completed_tasks: true},
        function (data, status, headers, config) {//success
          //
        },
        function (data) {//error
          $rootScope.addBulletin({text: 'Failed to retrieve past marketing communications. ' + (data.errors || ''), klass: 'danger'});
        }
      );
      return $scope.subsWTasks;
    }

    $scope.destroySubscription=function(index,queue,subscription){
      var successFn=function(){ $rootScope.refreshCurrentTasks(); $scope.init(); };
      $rootScope.destroyRecord(index,queue,'/marketing/subscriptions/'+subscription.id+'.json','active subscription',successFn);
    }

    $scope.init();
  }
]);
