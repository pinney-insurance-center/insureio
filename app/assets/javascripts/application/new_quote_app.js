/* Should eventually move these variables into QuoterModel for easier access.*/
var DEFAULT_INFLATION   = 0.025;
var DEFAULT_PATH = [
  {label: 'Basic Info', name: 'BASIC_INFO'},
  {label: 'Discovery', name: 'DISCOVERY'},
  {label: 'Underwriting', name: 'UNDERWRITING'},
  {label: 'Compare', name: 'COMPARE', viewRates: true},
  {label: 'Application', name: 'APPLICATION'},
];
var US_CITIZEN          = 1;
var DATE_REGEX          = new RegExp("^([0-9]{4})-([0-9]{2})-([0-9]{2})$");
var REFERRAL_REQUEST    = 'REFERRAL';
var APPLICATION_REQUEST = 'APPLICATION';
var TICKET_REQUEST      = 'TICKET';

/* This file was formerly using `jQuery.extend(true, {}, obj)` in several
/* places. While that is fine in most use cases, others were copying objects
/* which themselves contained arrays (as `cases_attributes`), and the `extend`
/* function copied the references instead of the data. While that should also
/* be okay under many circumstances, this script then proceeded to modify the
/* data, which forced changes on the original. */
function deepCopy (obj) {
  var clone = angular.copy(obj);
  for (var key in clone) {
    var val = clone[key];
    if (Array.isArray(obj)) clone[key] = deepCopy(val); /* Not automating for objects because of circular references. Build your specs. */
  }
  return clone;
}

// /#/?productLine=TLI&requestType=APPLICATION
angular.module("newQuoteApp", ['DrBulletins', 'ioConstants', 'io.nmbUtil'])
.config(["$httpProvider", function ($httpProvider) {
  $httpProvider.defaults.withCredentials = true;//ensures cookies get sent.
  $httpProvider.defaults.transformResponse.push(function(responseData){
    convertInputs(responseData);
    return responseData;
  });
}])
.provider('QuoterModel', function QuoterModelProvider() {
  this.$get = ['$resource','$location','resourceService','ioConstantsObject','requestSanitizer','NmbUtil',
  function ($resource, $location, resourceService, ioConstantsObject, requestSanitizer, NmbUtil) {
    function QuoterModel ($scope) {
      this.$scope = $scope;
      this.resourceService=resourceService;
      this.requestSanitizer=requestSanitizer;
      this.productLine = $location.search().productLine || 1;
      this.spouse           = {};
      this.childTemplate    = {};//gets altered based on answers in Discovery
      this.business         = {};
      this.AVG_CIGARETTES_OPTS = [ ['0',0], ['1-2',2], ['1/4 pack',5], ['1/2 pack',10], ['3/4 pack',15], ['1+ pack per day',20] ];
      /* Build `quoter.path` */
      this.path = DEFAULT_PATH.slice();
      this.path.forEach(step => { $scope[step.name] = step });
      /* Different QuoterModel configurations allow different DURATIONS, so use a local copy */
      this.durations = ioConstantsObject.DURATIONS.filter((d) => { return !!d.compulife_code });
    }

    Object.defineProperties(QuoterModel.prototype, {
      // Add Object to quoter.children
      addChild: {
        value: function () {
          if(!this.children){ this.children = []; }
          this.children.push( $.extend({},this.childTemplate) );
        }
      },
      calcCollegeCost: {
        value: function (child) {
          var total=0;
          if (child) {
            // calc cost for just one child
            if(!child.age){ child.age = 18; }
            var index0 = parseInt(child.collegeType);
            if (index0 == 1) index0 += parseInt(child.collegeResidence);
            if (isNaN(index0)) return 0;
            var index1 = parseInt(child.collegeFees);
            if (isNaN(index1)) return 0;
            var annualCost = this.collegeCosts[index0][index1];
            var yearsOfCollege = child.collegeType == 0 ? 2 : 4;
            var yearsTilCollege = child.age < 18 ? 18 - child.age : 0;
            for (var i = yearsTilCollege; i < yearsTilCollege + yearsOfCollege; i++) {
              total += annualCost * Math.pow(1+DEFAULT_INFLATION, i);
            }
            return total;
          } else {
            // calc total cost for all children
            if (!this.children) return 0;
            var quoter = this;
            total = this.children.reduce(function(previousVal, currentVal, index, array){
              return previousVal + quoter.calcCollegeCost(currentVal);
            }, 0);
            return Math.round(total, 0);
          }
        }
      },
      calcIncome: {
        value: function () {
          this.$scope.finance.annual_income = (this.$scope.finance.asset_earned_income||0) + (this.$scope.finance.asset_unearned_income||0);
        }
      },
      calcAssets: {
        value: function () {
          this.$scope.finance.assets = [
            this.$scope.finance.asset_checking,
            this.$scope.finance.asset_savings,
            this.$scope.finance.asset_pension,
            this.$scope.finance.asset_investments,
            this.$scope.finance.asset_home_equity,
            this.$scope.finance.asset_real_estate,
            this.$scope.finance.asset_life_insurance
          ].reduce(function(oldVal, curVal){
            return oldVal + (curVal || 0);
          }, 0);
        }
      },
      calcLiabilities: {
        value: function () {
          this.$scope.finance.liabilities = [
            this.$scope.finance.liability_final_expense,
            this.$scope.finance.liability_other,
            this.$scope.finance.liability_auto,
            this.$scope.finance.liability_credit,
            this.$scope.finance.liability_student_loans,
            this.$scope.finance.liability_personal_loans,
            this.$scope.finance.liability_mortgage_1,
            this.$scope.finance.liability_mortgage_2,
            this.$scope.finance.liability_heloc,
            this.$scope.finance.liability_education,
          ].reduce(function(oldVal, curVal){
            return oldVal + (curVal || 0);
          }, 0);
        }
      },
      copyAndCleanHealthForSave: {
        value: function () {
          let healthCopy = jQuery.extend(true, {}, this.$scope.health);
          this.sanitizeConsumerEtAl({ health_info_attributes: healthCopy });
          /* Remove keys that haven't changed s.t. POST is smaller and easier to
          /* read in logs. */
          for (let key in healthCopy) {
            if (healthCopy[key] == this.$scope.cleanHealthInfo[key])
              { delete healthCopy[key] }
          }
          return healthCopy;
        }
      },
      getDuration: {
        value: function () {
          for (let duration of this.durations) {
            if (duration.id === this.duration_id)
              return duration;
          }
        }
      },
      collegeCosts: {
        value: [
          [3131,  10550], // public 2yr
          [8655,  17860], // public 4yr in-state
          [21706, 30911], // public 4yr out-of-state
          [29056, 39518]  // private 4yr
        ]
      },
      fileUrl: {
        value: function (pane) {
          if(!pane) { return null; }
          return ('/quoter/'+pane.name.replace(/[_\s]/g, '-')+'.html').toLowerCase();
        }
      },
      hasCondition:{
        value: function (conditionName){
          var healthOrFinancial= (conditionName=='bankruptcy') ? 'finance' : 'health',
            objToTest=this.$scope[healthOrFinancial];
          if (!objToTest || !conditionName)
            return false;
          var conditionNameUnderScored=conditionName.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
          if(objToTest[conditionNameUnderScored])
            return true;
          var conditionFields=$.grep( Object.keys(objToTest), function(fieldName,idx){
            return fieldName.match( new RegExp('^'+conditionNameUnderScored+'_') );//filter them by prefix.
          });
          for (var i in conditionFields){
            var field=objToTest[conditionFields[i]];
              fieldIsAPrimitive=( field !== Object(field) );
            if( fieldIsAPrimitive ){
              if (field){
                return true;
              }
            }else{//for testing nested attributes.
              //Cannot just use `key in field` because angular often adds a ton of methods like `toJSON`.
              //`Object.keys` ensures that only the enumerable ones are looped over.
              for(let ii in Object.keys(field) ){
                var key=Object.keys(field)[ii],
                  value=field[key];
                if( key.match(/^\$/) ){ continue; }
                if(value){
                   return true;
                }
              }
            }

          }
          return false;
        }
      },
      isMoreThanFiveYearsAgo:{
        value: function(date){
          var today     =moment(),
              cutoffDate=today.subtract(5,'years');
          return moment(date).isBefore(cutoffDate);
        }
      },
      isLessThanOneYearAgo:{
        value: function(date){
          var today     =moment(),
              cutoffDate=today.subtract(1,'years');
          return moment(date).isAfter(cutoffDate);
        }
      },
      /* @param pane - {null|an object in `this.path`} */
      nextPane: {
        get: function (pane) {
          if (pane == null) { pane = this.currentPane }
          if (pane == this.$scope.COMPARE) { return }
          var idx = this.path.indexOf(pane);
          var next = this.path[idx+1]; /* May be undefined */
          return next;
        }
      },
      /* @param pane - {null|an object in `this.path`} */
      prevPane: {
        get: function (pane) {
          if (pane == null) { pane = this.currentPane }
          if (pane == this.$scope.COMPARE) { return }
          if (pane == this.$scope.BASIC_INFO) { return }
          var idx = this.path.indexOf(pane);
          var prev = this.path[idx-1]; /* May be undefined */
          return prev;
        }
      },
      minYearsOfIncome:{
        value: function () {
          var amount,
              dob,
              age;
          if( this.$scope.consumer ){
            dob=this.$scope.consumer.birth_or_trust_date;
          }
          if( dob ){
            age=moment().diff( dob, 'years');
            if(age>0 && age<65){
              amount=Math.max(65-age,1);
            }
          }
          return amount;
        }
      },
      calculatedNetWorth:{
        value: function(){
          var amount=(this.$scope.finance.assets || 0)-(this.$scope.finance.liabilities || 0);
          return amount;
        }
      },
      recommendedCoverageAmount: {
        value: function () {
          var total = 0;
          total += (parseInt(this.$scope.finance.annual_income) || 0) * (parseInt(this.$scope.finance.liability_years_income_needed) || 0);
          total-=( this.$scope.finance.net_worth_specified || this.calculatedNetWorth() );
          total=Math.max(total,0);
          return total;
        }
      },
      // Remove Object from quoter.children
      removeChild: {
        value: function (child) {
          this.children.splice(this.children.indexOf(child), 1);
        }
      },
      // @param pane - {an object in this.path | index into this.path | 'name' of object in this.path}
      setPane: {
        value: function (pane) {
          if (!this.path) return;
          if (typeof pane === 'string')
            this.currentPane = this.path.find(p => (p.name == pane));
          else if (pane == null || isNaN(pane))
            this.currentPane = (pane || this.path[0]);
          else
            this.currentPane = this.path[pane];
          this.currentPane.checked = true;
          window.scroll(0,0);
          $(".section-content").scrollTop(0);
        }
      },
      setRequestType: {
        value: function (typeString) {
          this.requestType = typeString;
          if (this.requestType == REFERRAL_REQUEST || this.requestType == TICKET_REQUEST)
            this.removePanesFromPath(['DISCOVERY', 'UNDERWRITING']);
          this.setPane();
        }
      },
      // Remove panes from this.path which match panes to be skipped
      // A String comparison is made against pane.name for pane in this.path.
      // @param panesToSkip can be string or array of names of pane (pane.name)
      removePanesFromPath: {
        value: function (panesToSkip) {
          if (!panesToSkip || !panesToSkip.keys) return;
          this.path = jQuery.grep(this.path, function (pane) {
            for (var i = 0; i < panesToSkip.length; i++)
              if (panesToSkip[i].indexOf(pane.name) >= 0)
                return false;
            return true;
          });
        }
      },
      existingPolicyTemplate: {
        value: {
          sales_stage_id:            ioConstantsObject.SALES_STAGE_ID_FOR_PLACED,
          is_a_preexisting_policy:   true,
          quoting_details_attributes:[{}]
        }
      },
      sanitizeConsumerEtAl:{
        // Remove nil-value attributes & empty arrays & timestamps from:
        // - consumer
        // - kase
        // This function is destructive, so it should always act on copies of objects.
        value: function  (consumerCopy, additionalBlackList) {
          var recursiveAttrBlacklist = [
            'created_at','updated_at','tags','brand_name','health_info','short_id','warnings',
            /eligible_for_processing_with_.+/,
            /date_processing_started_with_.+/,
            /reasons_for_ineligibility_with_.+/,
            /entered_stage_.+/,
            /exam_.+/,
            /marketech_.+/,
            'xrae_case_id', 'docusign_envelope_id', 'agency_works_id', 'equal_share_contingent_bens', 'equal_share_primary_bens', 'esign',
            'owner_id', 'owner_is_beneficiary', 'owner_is_premium_payer', 'quoting_details', 'next_task', 'status','last_app_page_snapshot', 'primary_contact_name',
            'family_diseases','financial_info', 'staff_assignment','annual_premium','semiannual_premium','quarterly_premium','monthly_premium','person_type','smm_status','statuses',
          ];
          var attrBlackList = [
            /.+_count$/,
          ];
          if( (typeof additionalBlackList)=='object' && additionalBlackList.length){
            recursiveAttrBlacklist = recursiveAttrBlacklist.concat(additionalBlackList);
          }
          this.requestSanitizer.clean(consumerCopy, attrBlackList);
          this.requestSanitizer.clean(consumerCopy, recursiveAttrBlacklist, true);
          if(!consumerCopy.id){
            //New records need not send nulls.
            //For older records, it's possible to overwrite a non-null with null tho.
            this.stripNulls(consumerCopy);
          }
        }
      },
      stripNulls:{
        // This function alters objects in place, so only use on copies.
        value: function (objOrArray) {
          var isArray=objOrArray instanceof Array;
          for(let key in objOrArray){
            if( key.match(/^(\$|_)/) ){
              //skip angular attributes
            }else if(objOrArray[key]==null && isArray){
              objOrArray.splice(key,1);
            }else if(objOrArray[key]==null && !isArray){
              delete objOrArray[key];
            }else if( (typeof objOrArray[key])=='object' ){
              this.stripNulls( objOrArray[key] );//recurse
            }
          }
          return objOrArray;
        }
      },
      consumerLoadedCallback:{
        value: function () {
          var $scope = this.$scope;
          const consumer = $scope.consumer;
          var queryString = $location.search();
          if ($scope.QuotePathCtrl) { // because places outside quote path still use contact object.
            $.each(['email','address','web'],function(cIndex,contactMethod){
              var pluralContactMethod=contactMethod[contactMethod.length-1]=='s' ? contactMethod+'es' : contactMethod+'s';

              if(!$scope.consumer[pluralContactMethod]){
                $scope.consumer[pluralContactMethod]=[{}];
              }else if(!$scope.consumer[pluralContactMethod].length){
                $scope.consumer[pluralContactMethod][0]={};
              }else{
                //This form will only use/set primary address/email,
                //so remove all non-primary records.
                var list=$scope.consumer[pluralContactMethod],
                    index=0;
                while(list.length>1){
                  if(list[index].id && list[index].id!=$scope.consumer['primary_'+contactMethod+'_id']){
                    list.splice(index,1);
                  }else{ index++; }
                }
              }
              $scope.consumer[pluralContactMethod+'_attributes']=$scope.consumer[pluralContactMethod];
              delete $scope.consumer[pluralContactMethod];
              var capitalizedContactMethod=contactMethod.charAt(0).toUpperCase() + contactMethod.substr(1);
              $scope.consumer['$primary'+capitalizedContactMethod+'Attributes']=$scope.consumer[pluralContactMethod+'_attributes'][0];
            });
            //This form collects multiple phones.
            delete $scope.consumer.primary_phone_attributes;
            $scope.consumer.phones_attributes=$scope.consumer.phones;
            delete $scope.consumer.phones;
            delete $scope.consumer.primary_phone_id;
            if ($scope.isNMB) {
              /* Update path for NMB */
              NmbUtil.modifyQuoterPath($scope.quoter, consumer);
              /* Update durations available for NMB */
              NmbUtil.modifyQuoterDurationOptions($scope.quoter);
              /* Update defaults for NMB */
              if (!$scope.consumer.preferred_contact_method_id)
                $scope.consumer.preferred_contact_method_id = 1; // Phone
            }
          }
          /* Support years_of_agent_relationship. (This is important. The
          /* previous implementation was manually setting one field from the
          /* other. These were not controlled, and when both hit the backend
          /* at the same time, data got mismanaged.) */
          Object.defineProperty($scope.consumer, 'years_of_agent_relationship', {
            get: function () {
              if (this.relationship_to_agent_start == null) return;
              let date = new Date(this.relationship_to_agent_start);
              /* Without `toFixed`, this leads to a stack overflow because of minute changes */
              return date && parseFloat(moment().diff(date, 'years', true).toFixed(2));
            },
            set: function (n) {
              if (n)
                this.relationship_to_agent_start = moment().subtract(parseInt(n), 'years').toDate();
              else
                this.relationship_to_agent_start = null;
            },
          });
          // Set brand
          if ($scope.consumer.brand_id){
            $scope.quoter.brandIdFrozen = true;
          }else{
            $scope.consumer.brand_id = queryString.brand_id;
          }
          // Set agent_id if provided in fragment params or logged in.
          if(!$scope.consumer.agent_id){ $scope.consumer.agent_id = queryString.agent_id; }
          if(!$scope.consumer.agent_id && $scope.isLoggedIn() ){ $scope.consumer.agent_id=$scope.currentUser.id; }
          if(!$scope.consumer.brand_id){ $scope.consumer.brand_id = queryString.brand_id; }
          // Set nested attribute models
          if (!consumer.health_info_attributes) consumer.health_info_attributes = consumer.health_info || consumer.health || {};
          if (!$scope.health.diseases) $scope.health.diseases = [];
          if (!$scope.health.relatives_diseases) $scope.health.relatives_diseases = [];
          if (!$scope.health.moving_violations) $scope.health.moving_violations = [];
          if (!$scope.health.crimes) $scope.health.crimes = [];
          $scope.cleanHealthInfo=$.extend(true,{},$scope.health);
          delete consumer.health_info;

          if(!consumer.financial_info_attributes) consumer.financial_info_attributes = consumer.financial_info || {};
          delete $scope.consumer.financial_info;

          if( [0,1,true,false].indexOf( $scope.consumer.gender )>-1 ){
            $scope.consumer.gender=$scope.consumer.gender ? '1' : '0';//menu values are strings
          }

          if(!$scope.health.tobacco_cigarettes_per_day){
            //Explicitly handle this case, since null<=0 is true, but undefined<=0 is false, and that ambiguity leads to problems.
            $scope.health.tobacco_cigarettes_per_day=0;
          }else{
            //Round the cigarettes per day to a value in the enumerated set.
            var opts=$scope.quoter.AVG_CIGARETTES_OPTS;
            for(let i in opts){
              if($scope.health.tobacco_cigarettes_per_day<=opts[i][1]||i==opts.length-1){
                $scope.health.tobacco_cigarettes_per_day=opts[i][1]; break;
              }
            }
          }

          $scope.consumer.existing_policies_attributes = this.resourceService.newArrayOrQuery($scope.consumer.id, '/consumers/:id/existing_coverage.json');
          if($scope.consumer.id){
            // Get value of existing insurance on client if client is persisted
            $scope.consumer.existing_policies_attributes.$promise.then(function () {
              var ePs=$scope.consumer.existing_policies_attributes;
              $scope.quoter.existingCoverageAmt = ePs.reduce(
                function (sum, kase) {
                  return sum + (kase.face_amount || 0);
                }, 0
              );
              if(!$scope.finance.asset_life_insurance){
                $scope.finance.asset_life_insurance = $scope.quoter.existingCoverageAmt;
              }
              $scope.quoter.hasExistingCoverage=true;

              for(let i in ePs){
                var k=ePs[i];
                k.quoting_details_attributes=[k.current_details];
                delete k.current_details;
              }
            });
          }
        }//End of `consumerLoadedCallback`
      },
    });

    return QuoterModel;
  }];
})
.controller('QuotePath.BaseCtrl', ['$scope','QuoterModel','NmbUtil', function ($scope, QuoterModel, NmbUtil) {
  Object.defineProperties($scope, {
    descendsFromQuotePathCtrl: {
      get: () => { return !!($scope.QuotePathCtrl && $scope.$parent.QuotePathCtrl) },
    },
    finance: {
      /* This getter is dynamic s.t. `$scope.finance` can be defined in a
      /* single place but support environments within and without the
      /* QuotePath. */
      get: () => { return $scope.consumer.financial_info_attributes },
      enumerable: true,
    },
    health: {
      /* This getter is dynamic s.t. `$scope.health` can be defined in a
      /* single place but support environments within and without the
      /* QuotePath. */
      get: () => { return $scope.consumer.health_info_attributes },
      enumerable: true,
    },
    relativesDiseaseTypes: {
      get: () => {
        if ($scope.isNMB) return window.DISEASE_TYPES;
        else return window.RELATIVES_DISEASE_TYPES_FOR_XRAE;
      },
    },
    isNMB: {
      get: () => { return NmbUtil.isNMB($scope.consumer) },
    },
    allowedHealthClassesForQuoting:{
      get: () => {
        if(!$scope.isNMB){ return $scope.HEALTH_CLASSES_TOP_4; }
        if($scope.quote.duration_id==17){//Lifetime, aka Whole Life
          return $scope.HEALTH_CLASSES_NMB_WHOLE_LIFE;
        }else if($scope.quote.face_amount<=(100*1000)){//see NMB_PRODUCTS in NmbUtil.
          return $scope.HEALTH_CLASSES_NMB_TERM_LTE100K;
        }
      },
    },
    allowedHealthClassesForManualEntry:{
      get: () => {
        if(!$scope.isNMB){ return $scope.HEALTH_CLASSES; }
        return allowedHealthClassesForQuoting;
      },
    },
  });
  if (!$scope.descendsFromQuotePathCtrl && !this.quoter) this.quoter = $scope.quoter = new QuoterModel($scope);
  if (!$scope.descendsFromQuotePathCtrl && !$scope.consumer && $scope.person)
    { $scope.consumer = $scope.person }
  if($scope.isNMB){ $scope.STATES=$scope.STATES_NMB; }
}])
.controller("QuotePathCtrl", ["$scope", "$resource", "$location", "$controller", "resourceService", "$http", "$q", "$uibModal", "bulletinService", "ioConstantsObject", "NmbUtil",
  function ($scope, $resource, $location, $controller, resourceService, $http, $q, $uibModal, bulletinService, ioConstantsObject, NmbUtil) {
    Object.defineProperty($scope, 'QuotePathCtrl', { value: true, writable: false });

    $scope.sliderVisibleBar = {
      value: 250000,
      options: {
        showSelectionBar: true,
        floor: 25000,
        ceil: 500000,
        step: 50000,
        hidePointerLabels: true,
        hideLimitLabels: true,
        translate: (value) => { return `$${value}` },
      }
    };

    // Link/set constants
    $scope.NAME = "QuotePathCtrl";
    $scope.REFERRAL_REQUEST    = REFERRAL_REQUEST;
    $scope.APPLICATION_REQUEST = APPLICATION_REQUEST;
    $scope.TICKET_REQUEST      = TICKET_REQUEST;
    $scope.WORK_PHONE_TYPE_ID = 2;
    $scope.PREMIUM_MODES      = [ioConstantsObject.PREMIUM_MODES[0], ioConstantsObject.PREMIUM_MODES[3]];
    $scope.RANGE              = new Array(256);
    $scope.modals             = {};
    $scope.app = {}; /* Holdling validation data from APPLICATION stage */

    //Start load/resolve of resources in parallel.
    //Then (for easier debugging and tracking) proceed only after all are loaded/resolved.
    $scope.getCurrentUser();
    $scope.consumer      = resourceService.newOrGet($location.search()['consumer_id']||$location.search()['io_consumer_id'], '/consumers/:id.json');
    $scope.kase          = resourceService.newOrGet($location.search()['kase_id'  ], '/crm/cases/:id.json');
    $scope.quotedDetails = resourceService.newOrGet($location.search()['quote_id' ], '/quoting/quotes/:id.json');
    $controller('QuotePath.BaseCtrl', { $scope: $scope });
    $scope.consumer.$promise.then((consumer) => { $scope.quoter.consumerLoadedCallback() });
    $scope.kase.$promise.then( kase => {
      if ($scope.isNMB) {
        if (undefined === kase.bind) kase.bind = false;
      }
    });

    var initQuotePathComplete = $q.defer();
    $scope.initQuotePathPromise = initQuotePathComplete.promise;
    $scope.initQuotePathCtrl=function(){
      if( $scope.isLoggedIn() ){//Consider user's permissions and preferences
        $scope.majorResourcesLoadedOrResolved=true;
        var referralAllowed = $scope.currentUser.can('submit_referral');
        var applicationAllowed = $scope.currentUser.can('submit_application');
        var referralPreferred = $scope.currentUser.submit_referral;
        var applicationPreferred = $scope.currentUser.submit_application;
        var tryToSetApplicationPane = $scope.consumer.id && $location.search().skip_to_apply_pane;

        /* Choose path: REFERRAL or APPLICATION */
        if (referralAllowed != applicationAllowed)
          $scope.quoter.setRequestType(referralAllowed ? REFERRAL_REQUEST : APPLICATION_REQUEST);
        else if ($location.search().requestType) /* Use URL params */
          $scope.quoter.setRequestType($location.search().requestType);
        else if ($scope.quotedDetails.id) /* This is a re-quote */
          $scope.quoter.setRequestType(APPLICATION_REQUEST);
        else if (referralPreferred != applicationPreferred)
          $scope.quoter.setRequestType(referralPreferred ? REFERRAL_REQUEST : APPLICATION_REQUEST);
        else if (applicationAllowed && tryToSetApplicationPane)
          $scope.quoter.setRequestType(APPLICATION_REQUEST);

        /* Choose visible pane: BASIC_INFO or APPLICATION_REQUEST */
        if ($scope.quoter.requestType) {
          if ($scope.quoter.requestType == APPLICATION_REQUEST && tryToSetApplicationPane)
            $scope.quoter.setPane($scope.APPLICATION);
          else
            $scope.quoter.setPane($scope.BASIC_INFO);
        }
        if ($location.search().pane) { $scope.quoter.setPane($location.search().pane) }
      }else{
        $scope.consumer.$promise.then(function(){
          if(!$scope.consumer.agent_id){
            $scope.agentSummary={error:"Invalid agent."};
            return;
          }
          $scope.agentSummary = $resource('/users/:id/agent_for_quote_path.json').get(
            {
              id:$scope.consumer.agent_id,
              key:$location.search()['key'],
              brand_id:$location.search()['brand'] || $scope.consumer.brand_id
            },
            function(){//success
              $scope.majorResourcesLoadedOrResolved=true;
            },
            function(data){//error
              $scope.agentSummary.error=data.error||'Invalid agent.'
             //The template will check whether `agentSummary` has key "error" and display accordingly.
            }
          );
        });
        //Default to application process for consumers.
        $scope.quoter.setRequestType($location.search().requestType||APPLICATION_REQUEST);
      }

      $scope.quote=$scope.quotedDetails;
      delete $scope.quote.$promise;
      delete $scope.quote.$resolved;
      if(!$scope.kase.id){
        delete $scope.quote.id;
      }
      $scope.consumer.cases_attributes=[$scope.kase];
      $scope.kase.quoting_details_attributes = [$scope.quote];
      $scope.quote.user_id = $scope.currentUser.id;
      if(!$scope.quote.premium_mode_id){ $scope.quote.premium_mode_id = 1; }
      // Read quote attrs from query string. Overwrite existing values.
      jQuery.each(['duration_id', 'face_amount', 'premium_mode_id', 'health_class_id'], function (i, key) {
        if ($location.search()[key]) $scope.quote[key] = parseInt($location.search()[key]);
      });


      // Set brand
      if ($scope.consumer.brand_id){
        $scope.quoter.brandIdFrozen = true;
      }

      //Set brand
      if( $scope.isLoggedIn() ){
        if(!$scope.consumer.brand_id){ $scope.consumer.brand_id = $scope.currentUser.default_brand_id; }
        if ($scope.consumer.brand_id) {
          $scope.brand = {id: $scope.consumer.brand_id, name: $scope.consumer.brand_name};
        } else {
          $scope.getBrands();
        }
      }else{
        if(!$scope.consumer.agent_id){ $scope.consumer.agent_id = $location.search()['agent_id']; }
        if( !$scope.consumer.brand_id ){
          $scope.consumer.brand_id=$location.search()['brand_id'];
        }
      }

      // Apply test data
      if ( $location.search().test || $location.search().dummy ){
        $scope.applyTestValues();
      }

      $scope.quoter.rightSidebarIsCollapsed=false;
      initQuotePathComplete.resolve('init ended');
    };//end of `$scope.initQuotePathCtrl`

    /* Don't call init until the resources have been loaded from the server */
    var resourcePromises = [
      $scope.currentUser.$promise,
      $scope.consumer.$promise,
      $scope.kase.$promise,
      $scope.quotedDetails.$promise,
    ];
    $q.all(resourcePromises).then(() => { $scope.initQuotePathCtrl() });

    $scope.getBrands = function (event, select) {
      $scope.brands = [];
      var transactionId = bulletinService.randomNonZeroId();
      bulletinService.add({text:'Loading brand options...', id:transactionId});
      $scope.brands = $resource('/brands/autocomplete.json').query();
      $scope.brands.$promise.then(function(){
        bulletinService.removeBulletinById(transactionId);
        $scope.brands.sort(function(a,b) { return a.name == b.name ? 0 : a.name < b.name ? -1 : 1; });
      }, function(){
        bulletinService.add({text:'Brand options failed to load.', klass:'danger', id:transactionId});
      });
    };

    $scope.generateRandomInRange=function(scaleFactor,offset,integerOnly){
      var seed=Math.random(),
          output=(seed*scaleFactor)+offset;
      if(integerOnly){
        output=Math.floor(output);
      }
      return output;
    }

    $scope.applyTestValues= function () {
      var c=$scope.consumer,
          h=$scope.health,
          gRIR=$scope.generateRandomInRange,
          randomYear         =gRIR( 30,1960,true),//year in range   [1960,1990]
          randomHeightFeet   =gRIR(  2,   4,true),//feet in range   [4,6]
          randomHeightInches =gRIR( 12,   0,true),//inches in range [0,12]
          randomWeight       =gRIR(100, 120,true);//weight in range [120,220]
      c.full_name='Test QuotePath'+moment().format('MM-DD');
      c.gender='1';
      c.$primaryAddressAttributes.state_id=13;
      c.$primaryEmailAttributes.value='TestQuotePath'+moment().format('MM-DD')+'@example.com';
      c.birth_or_trust_date = '11/11/'+randomYear;
      c.test_lead=true;
      h.feet = randomHeightFeet;
      h.inches = randomHeightInches;
      h.weight = randomWeight;
      h.tobacco=false;
      $scope.kase.is_test=true;
      $scope.quote.duration_id = $scope.quoter.duration_id = 1;
      $scope.quote.face_amount = $scope.quoter.face_amount = 500000;
      $scope.quote.health_class_id = 1;
      $scope.results = JSON.parse(localStorage.getItem('results'));
    };

    function viewRatesCallback (data, status, headers, config, transactionId) {
      // Handle error messages, if any.
      if (data.errors)
          bulletinService.add({text:data.errors, klass:'danger', id:transactionId});
      $scope.quoter.errors = data.errors;
      $scope.quoter.warnings = data.warnings;
      if (data.warnings && data.warnings.length && !$scope.isLoggedIn()){
        //Here, [\s\S] means all characters, including spaces and nonspaces.
        //It's the only way to include line breaks with a regex in JavaScript.
        var matches = data.warnings[0].match(/^Factor\(s\) skipped[^\n]+\n([\s\S]*)/);
        var factors = matches && matches[1];
        if(factors){ $scope.skippedFactorsWarning=factors; }
      }
      // Process results, if any
      if (Object.prototype.toString.call( data ) === '[object Array]')
        data = {results:data};
      $scope.results = data.results || [];
      if ($scope.results.length) {
        bulletinService.add({text:'Rates received', klass:'success', id:transactionId});

        // sort results
        var allowedModes=[null, 'annual_premium','semiannual_premium','quarterly_premium','monthly_premium'],
            selectedMode=allowedModes[$scope.quote.premium_mode_id],
          modalPremium = selectedMode || 'monthly_premium';
        for(let i in $scope.results){
          let cso=$scope.results[i].carrier.sprite_offset;
          $scope.results[i].$hasSprite=( angular.isDefined(cso) && angular.isNumber(cso) && cso>1);
        }
        $scope.results.sort(function(a, b){
          if (parseFloat(a[modalPremium]) < parseFloat(b[modalPremium]))
            return -1;
          else if (parseFloat(a[modalPremium]) > parseFloat(b[modalPremium]))
            return 1;
          else
            return 0;
        });
      }
      // set ids for any newly created records (which should have been returned with IDs in the HTTP response data)
      var majorModels = ['consumer', 'kase', ['health', 'health_info'], ['finance', 'financial_info'], ];
      for (let i in majorModels) {
        var model = majorModels[i];
        var dataKey = (model.keys ? model[1] : model)+"_id";
        if (model.keys) model = model[0];
        if(!$scope[model].id){ $scope[model].id = data[dataKey]; }
      }
    }

    $scope.addPhone = function (recipient) {
      if(!recipient.phones_attributes){ recipient.phones_attributes = []; }
      recipient.phones_attributes.push({});
    };

    $scope.categoryForId = function (id) {
      return !isNaN(id) && window.DURATIONS.find((d) => { return d.id == id });
    };

    // @param healthClassId is optional. It defaults to $scope.quote.health_class_id
    $scope.isTableRated = function (healthClassId) {
      if (!healthClassId)
        healthClassId = $scope.quote && $scope.quote.health_class_id;
      return !isNaN(healthClassId) && healthClassId >=7;
    };

    /* After $scope.consumer has already been defined, this can fetch data for
    /* a Consumer record and merge it into what's already been defined in
    /* scope (not overwriting existing values) */
    $scope.loadConsumer = (id, callback) => {
      if (id) {
        $http.get(`/consumers/${id}.json`)
        .success(data => {
          $scope.consumer = Object.assign(data, $scope.consumer); /* Keep local changes */
          $scope.quoter.consumerLoadedCallback();
          callback();
        }).error((data, status, headers, config) => {
          bulletinService.add({klass: 'danger', text: `Unable to retrieve Consumer record ${id} (${status})`});
          console.error(status, data);
        });
      }
    }

    //Prepares a deep copy of the consumer object for passing to the server.
    //Then sends it to the server.
    $scope.saveChanges = function (opts) {
      if (!opts) { opts = {} }
      if ($scope.saving && !opts.parallelOk) { return } /* Ignore call if currently executing */
      $scope.attemptedSave = true;
      if( !resourceService.consumerIsSaveable($scope) ){
        alert('Consumer full name and email are required to save.');
        return false;
      }
      var consumerCopy = deepCopy($scope.consumer),
          kaseCopy     = consumerCopy.cases_attributes[0],
          healthCopy   = $scope.quoter.copyAndCleanHealthForSave();
      /* Copy health changes to consumerCopy */
      consumerCopy.health_info_attributes = {};
      Object.keys(healthCopy).forEach(key => {
        let value = healthCopy[key];
        if (typeof value !== 'object' || Array.isArray(value)) // Object-type values represent only original states
          consumerCopy.health_info_attributes[key] = value;
      });

      $scope.setConsumerTrackingInfo(consumerCopy);
      if(!opts.includeCase){
        /* Don't save Case */
        delete consumerCopy.cases_attributes;
        /* Do save quote if persisted */
        if ($scope.quote.id)
          consumerCopy.quoting_details_attributes = [$scope.quote];
      }else{
        if(opts.submitReferOrSendNow){
          $scope.prepareToSubmitReferOrSend(consumerCopy);
        }
        $scope.setConsumerOwnershipForReferral(consumerCopy);
        $scope.splitReplacedFromOtherExistingPolicies(consumerCopy);

        let sRAs=kaseCopy.stakeholder_relationships_attributes||[];
        $.each(sRAs,function(i,sRA){
          if(sRA.stakeholder_id){
            delete sRA.stakeholder_attributes;
          }
        });
      }

      // Set Application page's note as a critical note
      if(
          $scope.quoter.criticalNote &&
          $scope.quoter.criticalNote.text &&
          !/^\s+$/.test($scope.quoter.criticalNote.text)
      ){
        if(!consumerCopy.notes_attributes) consumerCopy.notes_attributes = [];
        consumerCopy.notes_attributes.push({critical:true, text:$scope.quoter.criticalNote.text});
      }

      // Sanitize outgoing data
      $scope.quoter.sanitizeConsumerEtAl(consumerCopy);

      let defaultCallback = function (data) {
        if( opts.includeCase ){
          $scope.quoter.saved=true;
          $(window).scrollTop(0);
        }else{
          $scope.quoter.saved=false;
        }
        if(!$scope.consumer.id && data.consumer_id){
          $scope.consumer.id=data.consumer_id;
        }
        $scope.cleanHealthInfo=$scope.health;
        $scope.saveIllustration();
      };

      let successCallback = opts.success === undefined ? defaultCallback : opts.success;

      let saveOpts = {
        savingMsg: 'Saving form...',
        submit: !!opts.submitReferOrSendNow,
        successCallback: successCallback
      };

      return resourceService.save(
        $scope,
        {
          consumer:         consumerCopy,
          request_type:     $scope.quoter.requestType,
          agent_id:         $scope.consumer.agent_id,
          key:              $location.search()['key']
        },
        '/quoting/quotes/save_from_quote_path.json',
        saveOpts,
      );
    };

    $scope.saveChangesFromNavigation = (submitReferOrSendNow) => {
      $scope.attemptedSave = true;
      let isFromApplicationPane = $scope.isCurrentPane($scope.APPLICATION);
      if (isFromApplicationPane && !validateApplicationForm()) { return false }
      let opts = {
        noParallel: true,
        includeCase: isFromApplicationPane,
        submitReferOrSendNow: submitReferOrSendNow,
      };
      $scope.saveChanges(opts);
    }

    // If illustration is present, upload illustration file.
    $scope.saveIllustration=function(){
      var saveIllustrationPromise,
          transactionId = bulletinService.randomNonZeroId();
      if ($scope.quoter && $scope.quoter.illustration && $scope.quoter.illustration.length) {
        bulletinService.add({klass:'warn', id:transactionId, text:'Saving illustration. Please do not navigate away just yet.'});
        saveIllustrationPromise = $upload.upload({
          url: '/crm/cases/'+data.kase_id+'.json',
          method: 'PUT',
          file: $scope.quoter.illustration[0],
          fileFormDataName: 'crm_case[illustration_data]'
        }).success(function(){
            bulletinService.add({klass:'success', id:transactionId, text:'Illustration saved.'});
        }).error(function(data, status, headers, config){
            bulletinService.add({klass:'danger', id:transactionId,text:'Failed to save illustration.'});
        }).finally(function(){
          $scope.saving = false;
        });
      } else {
        saveIllustrationPromise = resourceService.makeResolvedPromise();
      }
    }

    $scope.searchDupConsumers = (evt, searchField) => {
      if (!$scope.isLoggedIn()) return;
      if ($scope.consumer.id) return;
      const searchTerm = evt.target.value.trim();
      if (!searchTerm || !searchField) return;
      const params = {
        'select[]': ['full_name', 'id', 'created_at', 'source'],
        person_type: 'Consumer',
        emails: true, /* include emails */
        exact: true,
        search_by: searchField,
        search_term: searchTerm,
      };
      $http.get('/contacts.json', { params: params })
      .success(data => {
        if (data.length) {
          /* Set primary email on each returned Consumer object */
          data.forEach(cons => {
            cons.$primaryEmail = cons.emails.find(r => r.id == cons.primary_email_id) || cons.emails[0];
          });
          /* Check if this operation's modal is already open */
          let modal = $scope.modals.quoteDupConsumers;
          const isModalOpen = modal && modal.opened.$$status && !modal.closed.$$status;
          /* Use the existing modal (if open) */
          if (isModalOpen) {
            /* Build an Object to avoid discarding or duplicating earlier matches
            in case of race condition (when both email and name triggers fire) */
            const consumersById = $scope.consumers.reduce((acc, cons) => acc[cons.id] = cons);
            data.forEach(cons => consumersById[cons.id] = cons);
            const childScope = $scope.modals.quoteDupConsumers.scope;
            childScope.consumers = Object.values(consumersById).sort((a,b) => b.id - a.id);
          }
          /* Open a modal */
          else {
            const childScope = $scope.$new();
            childScope.consumers = data;
            $scope.modals.quoteDupConsumers = $uibModal.open({
              windowClass: 'duplicate-consumer-check',
              templateUrl: '/quoter/modal_for_existing_consumer.html',
              size: 'lg',
              scope: childScope,
            });
            $scope.modals.quoteDupConsumers.scope = childScope;
          }
        }
      }).error((data, status, headers, config) => {
        console.error(status, data);
      });
    }

    $scope.prepareToSubmitReferOrSend=function(consumerCopy){
      //Add status_type_id.
      //If requoting an opportunity (persisted quote record, with no policy record),
      //prepare the updated details by removing the id and adding the proper sales_stage_id.
      let kaseCopy = consumerCopy.cases_attributes[0];
      let newQuoteCopy = kaseCopy.quoting_details_attributes[0];
      if ( !$scope.isLoggedIn() ) // consumer is running the quote
        { kaseCopy.status_type_id = CONSUMER_LINK_STATUS_TYPE_ID }
      else if ($scope.quoter.requestType == REFERRAL_REQUEST)
        { kaseCopy.status_type_id = REFERRAL_STATUS_TYPE_ID }
      else { /* Logged in && !REFERRAL_REQUEST */
        kaseCopy.status_type_id = SUBMIT_TO_APP_TEAM_STATUS_TYPE_ID;
        newQuoteCopy.sales_stage_id = 3;
      }
      if (newQuoteCopy.id) { // requoting
        kaseCopy.quoting_details_attributes.push({id: newQuoteCopy.id});
        delete newQuoteCopy.id;
      }
    }

    $scope.splitReplacedFromOtherExistingPolicies=function(consumerCopy){
      //The front end maintains a single list of existing policies.
      //Before saving them, they should be split into two lists,
      //so that those being replaced get their foreign key properly set.
      //As long as we are iterating from last to first,
      //we don't need to worry about the removal of one object altering the flow of the loop.
      var ePs      = consumerCopy.existing_policies_attributes,
          kaseCopy = consumerCopy.cases_attributes[0];
      for(let i=ePs.length-1; i>=0; i--){
        var eP=ePs[i];
        if(eP.replaced_by_id=='currentCaseId'){
          delete eP.replaced_by_id;
          if(kaseCopy.id || !eP.id){
            //Replacing policy record is already persisted to db or replaced policy is not.
            if(!kaseCopy.replaced_cases_attributes){
              kaseCopy.replaced_cases_attributes=[];
            }
            kaseCopy.replaced_cases_attributes.push(eP);
          }else if(!kaseCopy.id && eP.id){
            //Replacing policy is not persisted to db and replaced policy already is.
            //You can't pass nested attributes for an existing record within a new record.
            //But you can pass in the existing record's id to link the two.
            if(!kaseCopy.replaced_case_ids){
              kaseCopy.replaced_case_ids=[];
            }
            kaseCopy.replaced_case_ids.push(eP.id);
          }
          ePs.splice(i,1);
        }
      }
    }

    $scope.setConsumerTrackingInfo=function(consumerCopy){
      if(consumerCopy.id){
        //Consumer is already saved.
        //Tracking is meant to describe where a consumer record originated from.
        return;
      }
      var entryPoint=$scope.isLoggedIn() ? 'Manual Entry By User' : 'Referral Link';
      if($scope.quoter.requestType!=REFERRAL_REQUEST){//full app or app ticket process
        consumerCopy.source   ="IO Quote Path, started via "+entryPoint;
        consumerCopy.lead_type="IO Quote Path Full Application";
      }else{//referral process
        let loggedIn = $scope.isLoggedIn(),
            referringUserId   =(loggedIn ? $scope.currentUser.id : consumerCopy.agent_id),
            referringUserName =(loggedIn ? consumerCopy.brand_name   : $scope.agentSummary.brand_name),
            referringBrandId  =consumerCopy.brand_id,
            referringBrandName=(loggedIn ? consumerCopy.brand_name : $scope.agentSummary.brand_name);

        consumerCopy.referrer  =referringUserName+" (User "+referringUserId+")";
        consumerCopy.source    ="IO Quote Path Referral, started via "+entryPoint;
        consumerCopy.lead_type ="IO Quote Path Referral";
        consumerCopy.tags=consumerCopy.tags || {};
        consumerCopy.tags['referring_brand']=referringBrandName+" (Brand "+referringBrandId+")";
      }
    }

    $scope.setConsumerOwnershipForReferral=function(consumerCopy){
      if($scope.quoter.requestType!=REFERRAL_REQUEST){ return; }
      if(consumerCopy.id){ return; }
      let loggedIn = $scope.isLoggedIn(),
          referringUserId   =(loggedIn ? $scope.currentUser.id : consumerCopy.agent_id),
          referringBrandId  =consumerCopy.brand_id;
      consumerCopy.agent_id=null;
      consumerCopy.cases_attributes[0].agent_id=null;
      consumerCopy.cases_attributes[0].agent_of_record_id=referringUserId;
      consumerCopy.brand_id=$scope.REFERRAL_BRAND_ID;
    }

    $scope.canViewRates= function(){
      if(!$scope.health || !$scope.consumer){ return false; }
      var missingFieldsForRates=[],
          fieldsToCheck=[
            'consumer.gender',
            'health.feet',
            'health.inches',
            'health.weight',
            'consumer.birth_or_trust_date',
            'health.tobacco',
            'quote.face_amount',
            'quote.duration_id',
          ];
      if (!$scope.isNMB) fieldsToCheck.push('quote.health_class_id');
      for (let i in fieldsToCheck) {
        var fieldName = fieldsToCheck[i],
          field    =$scope.$eval(fieldName);
        if ((typeof field)=='undefined' || field==null) {
          var humanFieldName = fieldName.replace(/^(.*\.)*/,'').replace(/duration_id/,'coverage duration').replace(/_id$/,'').replace(/_/,' ');
          missingFieldsForRates.push(humanFieldName);
        }
      }
      if(missingFieldsForRates.length) {
        $scope.viewRatesErrorMsg = 'You must fill out the following fields to get rates:\n-' + missingFieldsForRates.join('\n-');
      } else {
        $scope.viewRatesErrorMsg = null;
      }
      return !$scope.viewRatesErrorMsg;
    }

    $scope.viewRates = function () {
      if ($scope.waitingOnRates) return;
      $scope.attemptedViewRates = true;
      var transactionId = bulletinService.randomNonZeroId();
      if( !$scope.canViewRates() ){
        bulletinService.add({text:$scope.viewRatesErrorMsg, klass:'danger', id:transactionId});
        return;
      }

      /* Move down NMB path instead of default path if this is an NMB case */
      if ($scope.isNMB) return loadAlq();

      $scope.waitingOnRates = true;

      // Set bulletin and viewpane
      bulletinService.add({text:'Requesting rates...', klass:'info', icon:'fa fa-refresh fa fa-spin', id:transactionId});

      $scope.initQuotePathPromise.then(function () {
        $scope.quoter.setPane($scope.COMPARE);
        $scope.results = null;
        $scope.quoter.errors = [];
        // Copy term and face amount to quoter so that those will not change
        // in the view for the following quotes, even if the user changes the
        // inputs
        $scope.quoter.duration_id = $scope.quote.duration_id;
        $scope.quoter.face_amount = $scope.quote.face_amount;
        // Sanitize payload and POST to back end
        var consumerCopy = deepCopy($scope.consumer),
            kaseCopy     = consumerCopy.cases_attributes[0];
        $scope.quoter.sanitizeConsumerEtAl(consumerCopy);
        if(!kaseCopy.id){
          delete kaseCopy.quoting_details_attributes[0].id;
          delete kaseCopy.quoting_details_attributes[0].case_id;
        }
        if( $scope.isLoggedIn() ){
          // Don't persist anything when agent is the party comparing quotes.
          // They know where the save buttons are and can save if they really
          // mean to do so.
          var keysToRemove=[
                'id','full_name','case_id','health_info_id','financial_info_id',
                'emails_attributes','phones_attributes','stakeholder_relationships_attributes',
              ];
          $scope.quoter.sanitizeConsumerEtAl(consumerCopy, keysToRemove);
        }

        $http.post('/quoting/get_quotes.json', {
          consumer:       consumerCopy,
          agent_id:       $scope.consumer.agent_id,
          key:            $location.search()['key'],
          request_type:   $scope.quoter.requestType,
        })
        .success(function(data, status, headers, config){
          viewRatesCallback(data, status, headers, config, transactionId);
        }).error(function(data, status, headers, config){
          bulletinService.add({text:'Failed to receive rates. '+data.errors, klass:'danger', id:transactionId});
          viewRatesCallback(data, status, headers, config);
        }).finally(function(){
          $scope.waitingOnRates = false;
        });
      });
    };

    $scope.isCurrentPane = function (pane) {
      return $scope.quoter.currentPane == pane;
    }
    $scope.showBtnSaveBasicInfo = () => {
      if (!$scope.isCurrentPane($scope.BASIC_INFO)) { return false }
      if (!$scope.isLoggedIn()) { return false }
      if (!$scope.currentUser.can('contacts_consumers')) { return false }
      return true;
    }

    $scope.showBtnSkipToQuotes = () => {
      if (!$scope.quoter.requestType == APPLICATION_REQUEST) { return false }
      if ($scope.quoter.nextPane == $scope.COMPARE) { return false }
      if (!$scope.isLoggedIn()) { return false }
      if (!$scope.currentUser.can('rapid_quote')) { return false }
      if ($scope.isNMB) { return false }
      var pane = $scope.quoter.currentPane;
      return (pane == $scope.BASIC_INFO || pane == $scope.DISCOVERY);
    }

    $scope.showBtnSkipToApplication = () => {
      if ($scope.quoter.requestType != APPLICATION_REQUEST) { return false }
      if (!$scope.isCurrentPane($scope.BASIC_INFO)) { return false }
      if (!$scope.isLoggedIn()) { return false }
      if (!$scope.currentUser.can('skip_quote')) { return false }
      if ($scope.isNMB) { return false }
      return true;
    }

    $scope.showBtnSaveConsumerAndCase = () => {
      if (!$scope.isLoggedIn()) { return false }
      if ($scope.quoter.currentPane == $scope.APPLICATION) {
        if ($scope.quoter.requestType != REFERRAL_REQUEST) { return true }
        return !!$scope.quote.id;
      }
      if ($scope.quoter.currentPane == $scope.UNDERWRITING) {
        return $scope.currentUser.can('contacts_consumers');
      }
      if ($scope.quoter.currentPane == $scope.DISCOVERY) {
        if (!$scope.consumer.id) { return false }
        return $scope.currentUser.can('contacts_consumers');
      }
      return false;
    }

    $scope.showBtnReferToSalesTeam = () => {
      if ($scope.quoter.requestType != REFERRAL_REQUEST) { return false }
      if (!$scope.isLoggedIn()) { return false }
      return $scope.quoter.currentPane == $scope.APPLICATION;
    }

    $scope.showBtnSubmitForProcessing = () => {
      if ($scope.quoter.requestType == REFERRAL_REQUEST) { return false }
      if (!$scope.isLoggedIn()) { return false }
      return $scope.quoter.currentPane == $scope.APPLICATION;
    }

    function loadAlq () {
      let opts = { useSandbox: $location.search().sandbox }; /* `useSandbox` option is available s.t. the NMB path can be tested while waiting on IXN to activate our ALQ. */
      /* Attempt save */
      $scope.saveChanges({includeCase: true, success: (data) => {
        $scope.consumer.id = data.consumer_id || $scope.consumer.id;
        $scope.kase.id = data.kase_id || $scope.kase.id;
        if(!$scope.quote.product_type_id){
          var isWholeLife=$scope.quote.duration_id==17;
          $scope.quote.product_type_id=isWholeLife ? 3 : 1;
        }
        /* Load ALQ*/
        if (NmbUtil.loadAlq($scope.consumer, $scope.kase, $scope.quote, opts))
          $scope.quoter.setPane('ALQ');
        else
          $scope.quoter.setPane(0);
      }}).then(() => {
        $scope.quoter.saved = false; /* Prevent the 'saved' page from showing. We want ALQ pane. */
      });
    }

    function validateApplicationForm () {
      if ($scope.saving) { return } /* Do not execute if already saving */
      /* If the try...catch block passes, then the validations have passed */
      try {
        if (!$scope.app.insuredInformation.$valid) { throw new Boolean(false) } // fail with generic message
        if (!$scope.consumer.birth_or_trust_date) { throw new Boolean(false) } // fail with generic message
        if (!$scope.app.contactInformation.$valid) { throw new String('Contact information fields missing.') }
        if ($scope.quoter.requestType == REFERRAL_REQUEST) { throw new Boolean(true) } // validations pass; stop checks
        if (!$scope.app.tempIns.$valid) { throw new Boolean(false) } // fail with generic message
        if ($scope.isLoggedIn() && !$scope.app.agentInfo.$valid) { throw new String('Agent information fields missing.') }
        if (!$scope.app.otherInformation.$valid) { throw new Boolean(false) } // fail with generic message
        if (!$scope.app.stakeholderRelationshipsForm.$valid) { throw new String('Stakeholder (e.g. beneficiary, owner, payer) fields missing.') }
        if (!$scope.app.attemptedSubmit) { throw new Boolean(true) } // validations pass; stop checks
        if ($scope.quoter.requestType != APPLICATION_REQUEST) { throw new Boolean(true) } // validations pass; stop checks
        if (!$scope.consumer.phones_attributes || !$scope.consumer.phones_attributes.length) { throw new String('Phone missing.') }
      } catch (ex) {
        if (ex instanceof Boolean && ex.valueOf()) { return true }
        if (ex instanceof String || ex instanceof Boolean) {
          var errorMessage = `${ex.valueOf() || 'Required fields missing.'} Please complete form.`
          //use both alert and bulletin to notify user that the form requires corrections
          bulletinService.add({text:errorMessage, klass:'danger'});
          alert(errorMessage);
          return false
        }
        /* If something other than a bool or string is caught, it's a real error. */
        throw ex;
      }
      return true;
    };
  }
])
.controller("QuotePath.DiscoveryCtrl", ["$scope", '$http', '$controller', "QuoterModel",
  function ($scope, $http, $controller, QuoterModel) {
    $controller('QuotePath.BaseCtrl', { $scope: $scope })
    if (!$scope.descendsFromQuotePathCtrl) {
      $scope.consumer.$promise.then(() => { $scope.quoter.consumerLoadedCallback() });
    }

    $scope.consumer.$promise.then(function(){
      $scope.quoter.isRecentlyApplied = $scope.consumer.last_applied_for_life_ins && (new Date() - $scope.consumer.last_applied_for_life_ins)/86400000 <= 200;
    });
  }
])
.controller("QuotePath.FinancialCtrl", ["$scope", "$controller", "$uibModal", 'resourceService', 'requestSanitizer',
  function ($scope, $controller, $uibModal, resourceService, requestSanitizer) {
    $controller('QuotePath.BaseCtrl', { $scope: $scope })

    $scope.openCollegeCostCalcModal=function(){
      $uibModal.open({
        templateUrl:'/quoter/modal_for_college_cost_calc.html',
        controller: 'QuotePath.CollegeCostCalcModalCtrl',
        size:       'lg',
        scope: $scope
      });
    }

    $scope.saveFinance = function () {
      var finance= jQuery.extend(true, {}, $scope.finance);
      requestSanitizer.clean(finance);
      if($scope.finance.id){
        resourceService.saveSingleModel($scope, '/crm/financial_infos/'+$scope.finance.id+'.json', {crm_financial_info:finance}, 'put');
      }else{
        resourceService.saveSingleModel($scope, '/crm/financial_infos.json', {crm_financial_info:finance}, 'post');   
      }
    }

    $scope.needsFinanceSaveButton = !$scope.descendsFromQuotePathCtrl;
  }
])
.controller("QuotePath.UnderwritingCtrl", ["$scope", "$controller", "resourceService",
  function ($scope, $controller, resourceService) {
    $controller('QuotePath.BaseCtrl', { $scope: $scope });
    // Build options for <select> elements
    $scope.SYSTOLIC_BP_OPTIONS = [[null, ''],[119, '< 120']];
    for (let i = 120; i < 250; i++) $scope.SYSTOLIC_BP_OPTIONS.push([i, i]);
    $scope.DIASTOLIC_BP_OPTIONS = [[null, ''],[79, '< 80']];
    for (let i = 80; i < 180; i++) $scope.DIASTOLIC_BP_OPTIONS.push([i, i]);
    $scope.COMPULIFE_DURATION_OPTIONS = [
      [null, 'n/a'],
      [moment().subtract(6, 'months').format('YYYY/MM/DD'), '6 Months or Less'],
      [moment().subtract(1, 'years').format('YYYY/MM/DD'), '1 Year or Less']
    ];
    for (let i = 2; i < 15; i++){
      $scope.COMPULIFE_DURATION_OPTIONS.push([moment().subtract(i, 'years').format('YYYY/MM/DD'), i+' Years or Less']);
    }
    $scope.COMPULIFE_DURATION_OPTIONS.push([moment().subtract(15, 'years').format('YYYY/MM/DD'), '15 Years or More']);
    /* If health info has bp_control_start, add an option to the UI for its current value */
    $scope.bpControlStartOptions = $scope.COMPULIFE_DURATION_OPTIONS.slice();
    $scope.consumer.$promise.then(() => {
      let bpControlStart = $scope.consumer.health_info_attributes.bp_control_start;
      if (bpControlStart) {
        let currentSelection = [bpControlStart, 'Since '+bpControlStart];
        $scope.bpControlStartOptions.unshift(currentSelection);
      }
    });
    /* If health info has cholesterol_control_start, add an option to the UI for its current value */
    $scope.cholesterolControlStartOptions = $scope.COMPULIFE_DURATION_OPTIONS.slice();
    /* Callback when consumer loaded */
    $scope.consumer.$promise.then(() => {
      let cholesterolControlStart = $scope.consumer.health_info_attributes.cholesterol_control_start;
      if (cholesterolControlStart) {
        let currentSelection = [cholesterolControlStart, 'Since '+cholesterolControlStart];
        $scope.cholesterolControlStartOptions.unshift(currentSelection);
      }
      /* Set UI config */
      if ($scope.consumer.id) {
        if ($scope.quoter.showFamilyHistory === undefined)
          $scope.quoter.showFamilyHistory = !!$scope.health.relatives_diseases.length;
        if ($scope.quoter.showDrivingHistory === undefined)
          $scope.quoter.showDrivingHistory = !!$scope.health.moving_violations.length || !!$scope.health.moving_violation_details;
        if ($scope.quoter.showCriminalHistory === undefined)
          $scope.quoter.showCriminalHistory = !!$scope.health.crimes.length || !!$scope.health.criminal_details;
      }
    });
    $scope.TOTAL_CHOLESTEROL_OPTIONS = [[null, '']];
    for (let i = 100; i < 300; i++) $scope.TOTAL_CHOLESTEROL_OPTIONS.push([i, i]);
    $scope.HDL_CHOLESTEROL_OPTIONS = [[null, '']];
    for (let i = 250; i < 1000; i += 10) {
      var val = i/100;
      $scope.HDL_CHOLESTEROL_OPTIONS.push([val, val.toString()]);
    }
    $scope.XRAE_CONDITION_DEGREES = [{id:'Mild', text:'Mild'},{id:'Moderate', text:'Moderate'},{id:'Severe', text:'Severe'}];

    /* Use dynamic accessors for form inputs which do not correspond to model values */
    Object.defineProperties($scope, {
      citizenship: {
        set: (value) => {
          $scope.consumer.citizenship_id = !!parseInt(value) ? 1 : 4;
        },
        get: () => { return $scope.consumer.citizenship_id == 1 ? '1' : null },
      },
      bankruptcy: {
        set: (value) => {
          $scope.finance.bankruptcy = !!parseInt(value);
        },
        get: () => {
          return ($scope.finance.bankruptcy || $scope.finance.bankruptcy_discharged) ? '1' : null
        },
      },
    });

    if (!$scope.descendsFromQuotePathCtrl) {
      $scope.cleanHealthInfo = {};
      $scope.consumer.$promise.then((consumer) => {
        $scope.cleanHealthInfo = $.extend(true, {}, $scope.health);
      });
    }

    $scope.saveUnderwriting = function () {
      let healthCopy = $scope.quoter.copyAndCleanHealthForSave();
      const financeAttrs = (({bankruptcy, bankruptcy_declared, bankruptcy_discharged}) => (
        { bankruptcy, bankruptcy_declared, bankruptcy_discharged }))($scope.finance);
      let params = { consumer: {
        citizenship_id: $scope.consumer,
        health_info_attributes: healthCopy,
        financial_info_attributes: financeAttrs,
      } };
      if ($scope.consumer.id)
        { resourceService.saveSingleModel($scope, '/consumers/'+$scope.consumer.id+'.json', params, 'put') }
      else
        { resourceService.saveSingleModel($scope, '/consumers.json', params, 'post') }
    }

    $scope.alertIfMultipleHealthConditionsPresent=function(){
      if ($scope.alreadyAltertedReMultipleHealthConds) return;
      var multipleHealthCondsMsg="Selecting two or more medical conditions will require evaluation"+
      " by our in-house underwriter or your brokerage director"+
      " to receive accurate health rating recommendations";
      var conditions=$scope.HEALTH_CONDITION_TYPES,
          conditionCount=0;
      for(let j in conditions){
        let cond=conditions[j];
        if($scope.health[cond]){
          conditionCount ++;
        }
        if(conditionCount >= 2){
          alert(multipleHealthCondsMsg);
          $scope.alreadyAltertedReMultipleHealthConds = true;
          break;
        }
      }
    }

    $scope.calcYearsSince=function(startDate){
      if(!startDate){ return null;}

      var monthsSince   =moment(new Date).diff(startDate,'month'),
          halfYearsSince=Math.ceil(monthsSince/6),
          yearsSince    =halfYearsSince/2;//need to get increments of half a year.
      if(yearsSince==0.5){
        yearsSince=0.5;
      }else if(yearsSince>14){
        yearsSince=15;
      }else{
        yearsSince=Math.ceil(yearsSince);//convert back to full years.
      }

      return yearsSince;
    }

    $scope.setDefaultBp=function(){
      $scope.health.bp_systolic=145;
      $scope.health.bp_diastolic=90;
    }

    $scope.setDefaultCholesterol=function(){
      $scope.health.cholesterol_level=150;
      $scope.health.cholesterol_hdl=4.0;
    }

  }
])
.controller("QuotePath.RelativesDiseasesCtrl", ["$scope", ($scope) => {
  Object.defineProperty($scope, 'relativesDiseases', {
    /* So far, `health` has been set in multiple places. I cleaned up some
    /* (possibly all) of them, but there may be multiple yet. This getter is
    /* dynamic so that if we still have multiple callbacks trying to set this,
    /* they will be honoured (as opposed to just setting the array statically
    /* and referencing it statically afterward). */
    get: () => {
      if (!$scope.health.relatives_diseases) { $scope.health.relatives_diseases = [] };
      return $scope.health.relatives_diseases;
    },
  });
}])
.controller("QuotePath.MovingViolationsCtrl", ["$scope", ($scope) => {
  Object.defineProperty($scope, 'movingViolations', {
    /* This is dynamic so that we don't have to set it in multiple places when upstream callbacks fire. */
    get: () => {
      if (!$scope.health.moving_violations) { $scope.health.moving_violations = [] };
      return $scope.health.moving_violations;
    },
  });
}])
.controller("QuotePath.CrimesCtrl", ["$scope", ($scope) => {
  Object.defineProperty($scope, 'crimes', {
    /* This is dynamic so that we don't have to set it in multiple places when upstream callbacks fire. */
    get: () => {
      if (!$scope.health.crimes) { $scope.health.crimes = [] };
      return $scope.health.crimes;
    },
  });
}])
.controller("QuotePath.CompareCtrl", ["$scope", "$http", "$uibModal", "resourceService", "QuoterModel",
  function ($scope, $http, $uibModal, resourceService, QuoterModel) {
    $scope.compareCtrlVars={};
    $scope.compareCtrlVars.selectedResults=[];

    $scope.applyQuote = function (result) {
      jQuery.extend($scope.quote, $scope.resultToQuotedDetails(result));
      $scope.quoter.readonly = true; // determines whether certain fields on Application page are readonly
      $scope.quoter.setPane($scope.APPLICATION);
    }

    $scope.openCoverageDetailsModal=function(result){
      $uibModal.open({
        templateUrl:'/quoter/modal_for_coverage_details.html',
        controller: 'QuotePath.CoverageDetailsModalCtrl',
        scope: $scope,
        resolve: {
          result: function () {
              return result;
            }
        }
      });
    }
    $scope.openComparisonModal = function () {
      var selectedResults =jQuery.grep($scope.results, function(result){
        return result.flaggedForComparison;
      }).slice(0,5);
      if (!selectedResults.length){
        alert('You must mark at least one checkbox.');
        return;
      }
      $scope.modals.comparisonModal = $uibModal.open({
        templateUrl:'/quoter/comparison_modal.html',
        controller: 'QuotePath.ComparisonModalCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
          selectedResults: function () {
            return selectedResults;
          }
        }
      });
    }
    $scope.openCarrierDetailsModal=function(result){
      $uibModal.open({
        templateUrl:'/quoter/carrier_details_modal.html',
        controller: 'QuotePath.CarrierDetailsModalCtrl',
        scope: $scope,
        resolve: {
          result: function () {
              return result;
            }
        }
      });
    }
    // Return an Object built from a quoter result. The returned object
    // is suitable for passing into rails within quoting_details_attributes,
    $scope.resultToQuotedDetails = function (result,keepAnnualAndMonthly) {
      var selectedPremium=null,
          quoteObj;

      if($scope.quote.premium_mode_id==1){//Annual
        selectedPremium=result.annual_premium;
      }else if($scope.quote.premium_mode_id==2){//Semi-annual
        selectedPremium=result.monthly_premium*6;
      }else if($scope.quote.premium_mode_id==3){//Quarterly
        selectedPremium=result.monthly_premium*3;
      }else if($scope.quote.premium_mode_id==4){//Monthly
        selectedPremium=result.monthly_premium;
      }

      quoteObj={
        carrier_id:            result.carrier.id,
        carrier_health_class:  result.health_category,
        plan_name:             result.product_name,
        planned_modal_premium: selectedPremium,
        user_id:               $scope.currentUser.id,
        consumer_id:           $scope.consumer.id,
        duration_id:           $scope.quote.duration_id,
        health_class_id:       $scope.quote.health_class_id,
        premium_mode_id:       $scope.quote.premium_mode_id,
        face_amount:           $scope.quote.face_amount,
        case_id:               $scope.kase.id,
        //Defaulting +product_type_id+ to 1 here only because the quote path currently does not capture this information.
        //This information should be captured in future, at which time this line may be removed.
        product_type_id:       (result.product_type_id||1),
      };
      if(keepAnnualAndMonthly){
        quoteObj['annual_premium']=result.annual_premium;
        quoteObj['monthly_premium']=result.monthly_premium;
      }
      return quoteObj;
    }
    // @param results should be array of quoter result objects
    $scope.saveQuotes = function (results) {
      $scope.consumer.cases_attributes = results.map(function(r){
        var qD=$scope.resultToQuotedDetails(r),
            savedQ={quoting_details_attributes:[qD]};
        savedQ.status_type_id=$scope.SAVED_QUOTE_STATUS_TYPE_ID;
        return savedQ;
      });
      if( !resourceService.consumerIsSaveable($scope) ){
        alert('Consumer full name and email are required to save.');
        return false;
      }
      $scope.saveChanges({ includeCase: true }).then(() => {
        //Ensure the opportunities are removed after saving so subsequent saves don't duplicate them.
        delete $scope.consumer.cases_attributes;
      });
    }
    $scope.saveQuote = function (result) {
      $scope.saveQuotes([result]);
    }
  }
])
.controller("QuotePath.ApplicationCtrl", ["$scope", "$resource", "resourceService", "bulletinService", "QuoterModel",
  function ($scope, $resource, resourceService, bulletinService, QuoterModel) {
    $scope.addExistingCoverage = function () {
      $scope.consumer.existing_policies_attributes.$promise.then(function(){
        var newExistingPolicy=$.extend(true,{},$scope.quoter.existingPolicyTemplate);
        $scope.consumer.existing_policies_attributes.push(newExistingPolicy);
      });
    }
    $scope.removeExistingCoverage = function (coverage) {
      var i = $scope.consumer.existing_policies_attributes.indexOf(coverage);
      if (i >= 0) $scope.consumer.existing_policies_attributes.splice(i, 1);
    }
    $scope.newStakeholderRelationshipTemplate={
      contingent:false,
      stakeholder_attributes:{
        agent_id:$scope.currentUser.id || $scope.consumer.agent_id,
        brand_id:$scope.consumer.brand_id,
        emails_attributes:[],
        phones_attributes:[],
        addresses_attributes:[],
        entity_type_id:1
      }
    };
    $scope.addstakeholderRelationship = function () {
      $scope.involvedParties.$promise.then(function(){
        var sR = $.extend(true,{},$scope.newStakeholderRelationshipTemplate);
        if(!$scope.kase.stakeholder_relationships_attributes){
          $scope.kase.stakeholder_relationships_attributes=[];
        }
        $scope.kase.stakeholder_relationships_attributes.push(sR);
      });
    }

    $scope.destroySR=function(sR, index){
      if(sR.id){
      sR._destroy=true
      }else{
      $scope.kase.stakeholder_relationships_attributes.splice(index,1);
      }
    }

    $scope.consumer.$promise.then(function(){
      // Fetch existing coverage
      $scope.consumer.existing_policies_attributes = resourceService.newArrayOrQuery($scope.consumer.id, '/consumers/:id/existing_coverage.json');
      $scope.consumer.existing_policies_attributes.$promise.then(
        function() {
          for(let i in $scope.consumer.existing_policies_attributes){
            var eP=$scope.consumer.existing_policies_attributes[i];
            eP.quoting_details_attributes=[eP.current_details];
            delete eP.current_details;
          }
        },
        function() {
          bulletinService.add({klass:'danger', text:'Failed to fetch existing coverage policies (if any)'});
        }
      );
    });
    $scope.kase.$promise.then(function(){
      // Get stakeholders (beneficiaries/owners/payers)
      $scope.involvedParties=resourceService.newOrGet($scope.kase.id, '/crm/cases/:id/involved_parties.json');
      $scope.involvedParties.$promise.then(function(){
        var iP=$scope.involvedParties;
        //If the kase is not persisted, these keys will not be present.
        if(iP.agent_id || iP.agent_of_record_id || iP.stakeholder_relationships_attributes){
          $scope.kase.agent_id=iP.agent_id;
          $scope.kase.agent_of_record_id=iP.agent_of_record_id;
          $scope.kase.stakeholder_relationships_attributes=iP.stakeholder_relationships;
          for(let i in $scope.kase.stakeholder_relationships_attributes){
            var sR=$scope.kase.stakeholder_relationships_attributes[i];
            sR.stakeholder_attributes=sR.stakeholder;
            delete sR.stakeholder;
            $.each(['emails','phones','addresses','webs'],function(cIndex,contact_method){
              //Convert keys like 'phones' to keys like 'phones_attributes'
              //so that submitting the beneficiary forms works smoothly with rails.
              sR.stakeholder_attributes[contact_method+'_attributes']=sR.stakeholder_attributes[contact_method]||[];
              delete sR.stakeholder_attributes[contact_method];
            });
          }
        }
      });
    });
  }
])
.controller('QuotePath.CollegeCostCalcModalCtrl', ['$scope', '$uibModalInstance', 'resourceService', "QuoterModel",
  function ($scope, $uibModalInstance, resourceService, QuoterModel) {
  }
])
.controller("QuotePath.CoverageDetailsModalCtrl", ['$scope', '$resource', '$uibModal', '$location',
  function($scope, $resource, $uibModal, $location) {
  }
])
.controller('QuotePath.ComparisonModalCtrl', ['$scope', '$http', 'selectedResults', 'resourceService', 'bulletinService', '$timeout',
  function ($scope, $http, selectedResults, resourceService, bulletinService, $timeout) {
    $scope.results = selectedResults;
    if(!$scope.comparisonModal){ $scope.comparisonModal = {}; }

    $scope.emailQuotes = function () {
      if( !resourceService.consumerIsSaveable($scope) ){
        alert('Consumer full name and email are required to save. A saved consumer record is required to email quotes.');
        return false;
      }
      var emailQuotesRequest=function(){
        //Don't show the saved overlay. They may want to keep working in quote path.
        $scope.quoter.saved=false;

        var quotes=$scope.results.map(function(r){
          var qD=$scope.resultToQuotedDetails(r,true);
          qD.status_type_id=$scope.SAVED_QUOTE_STATUS_TYPE_ID;
          return qD;
        });
        bulletinService.add({text:'Email request in process...'});
        $http.post('/quoting/quotes/email.json', {
          quotes:          quotes,
          to_address:      $scope.comparisonModal.email,
          brand_id:        $scope.consumer.brand_id,
          consumer_id:     $scope.consumer.id,
          user_id:         $scope.quote.user_id,
          request_type:    $scope.quoter.requestType,
          product_line:    $scope.quoter.productLine,
          premium_mode_id: $scope.quote.premium_mode_id,
          health_class_id: $scope.quote.health_class_id,
          face_amount:     $scope.quoter.face_amount,
          duration_id:     $scope.quoter.duration_id,
        }).success(function () {
          bulletinService.add({klass:'success', text:'Email sent.'});
        }).error(function (data) {
          var errorMsg = 'Email failed to send. ';
          if (data.errors) errorMsg += '\u00A0'+data.errors.join('; ');
          bulletinService.add({klass:'danger', text:errorMsg});
        });
      }
      if(!$scope.consumer.id){
        //In the rare but possible scenario where consumer is saveable but not yet saved,
        //save first, then email the quotes in a separate request.
        $scope.saveChanges({parallelOk: true}).then(function(){
          emailQuotesRequest();
        });
      }else{
        emailQuotesRequest();
      }
      
    }

    $scope.saveQuotes = function () {
      $scope.$parent.saveQuotes($scope.results); // here, results is just the (<=)5 selected results
      if($scope.modals.comparisonModal){
        $scope.modals.comparisonModal.close();
        delete $scope.modals.comparisonModal;
      }
    }

  }
])
.controller('QuotePath.CarrierDetailsModalCtrl', ['$scope', '$uibModalInstance', 'resourceService', 'result',
  function ($scope, $uibModalInstance, resourceService, result){
    $scope.result=result;
  }
])
.factory('resourceService', ['$http', '$window', '$resource', '$q', 'bulletinService', "$upload", "$location",
  function ($http, $window, $resource, $q, bulletinService, $upload, $location) {
    return {
      consumerIsSaveable: function($scope){
        var yayOrNay= $scope.consumer.id ||
                ($scope.consumer.full_name && $scope.consumer.full_name.length>0 ) ||
                ($scope.consumer.$primaryEmailAttributes.value && $scope.consumer.$primaryEmailAttributes.value.length>0 );
        return yayOrNay;
      },
      errorString: function (httpResponsePayload) {
        if (typeof(httpResponsePayload) === 'string')
          return httpResponsePayload;
        else if (typeof(httpResponsePayload.errors) === 'string')
          return httpResponsePayload.errors;
        else {
          var errors = httpResponsePayload.errors || httpResponsePayload;
          if (!Array.isArray(errors))
            errors = Object.keys(errors).map(function(val, key){
              return key.toString().replace(/\./g,' ')+' '+val;
            });
          return errors.join('; ');
        }
      },
      // Return a dummy promise that's already resolved to success
      makeResolvedPromise: function (value) {
        var deferred = $q.defer();
        deferred.resolve(value);
        return deferred.promise;
      },
      newOrGet: function (id, url) {
        var res;
        if (id) {
          res = $resource(url).get({id:id});
          res.$promise.then(null, function(responseObj){
            data=responseObj.data;
            bulletinService.add({klass:'danger', text:'Failed to fetch record from server at '+url+' .' + (data.errors || data.error || '') });
          });
        } else {
          res = new ($resource(url))();
          res.$promise = $q(function(resolve, reject){ resolve(res) });
          res.$resolved=true;
        }
        return res;
      },
      newArrayOrQuery: function (id, url) {
        if (id) {
          return $resource(url, {id:id}).query();
        } else {
          var array = [];
          array.$promise=this.makeResolvedPromise(array);
          return array;
        }
      },
      save: function ($scope, object, url, customOptions) {//returns promise object.
        if ($scope.saving && !customOptions.overrideSavingCheck) {
          bulletinService.add({klass:'danger', text:'Save already in progess. Please wait.'});
          return this.makeResolvedPromise();
        }
        $scope.saving = true;
        var self = this,
          defaultOptions={
            savingMsg  : 'Saving...',
            successMsg : 'Saved.',
            errorMsg   : 'Failed to save.',
            httpMethod : object.id ? 'put' : 'post',
          },
          opts=$.extend(true,defaultOptions,customOptions);
        // Add bulletin and send $http request
        let bulletinId = bulletinService.randomNonZeroId();
        bulletinService.add({text:opts.savingMsg, id:bulletinId});
        return $http[opts.httpMethod](url, object)
        .success(function(data, status, headers, config){
          if (data.errors && data.errors.length)
            bulletinService.add({klass:'danger', text:self.errorString(data), id:bulletinId});
          else
            bulletinService.add({klass:'success', text:opts.successMsg, id:bulletinId, ttl: 3000});
          if (opts.successCallback)
            opts.successCallback(data, status, headers, config);
          $scope.quoter.saved=true;
          if($scope.quoter.requestType!=REFERRAL_REQUEST && opts.submit==true ){
            $scope.quoter.submitted=true;
          }
          if(!$scope.consumer.id){ $scope.consumer.id=data.id; }
          /*$(window).scrollTop(0);*/
          $('.section-content').scroll(0,0);
        })
        .error(function(data, status, headers, config){
          var errorMsg = opts.errorMsg;
          if (data.errors) {
            opts.errorMsg += " "+self.errorString(data);
          }
          bulletinService.add({text:opts.errorMsg, klass:'danger', id:bulletinId});
          if (opts.errorCallback){
            opts.errorCallback(data, status, headers, config);
          }
          delete $scope.kase.status_type_id;
        })
        .finally(function(data, status, headers, config){
          if (opts.finallyCallback)
            opts.finallyCallback(data, status, headers, config);
          $scope.saving = false;
        });
      
      },
      // Save changes for a single model
      saveSingleModel: function  ($scope, url, payload, httpMethod) {
        var self = this;
        if( !self.consumerIsSaveable($scope) ){ return false; }
        return self.save($scope, payload, url, {
          savingMsg: 'Saving changes...',
          httpMethod: httpMethod,
          successCallback: function (data) {
            $scope.cleanHealthInfo=$scope.health;
          }
        });
      }
    };
  }
])
.directive('requiredMsg',[function(){
  return {
    restrict: 'E',
    template: '\
        <span class="alert alert-danger"\
        ng-show="field.$error.required && (field.$touched || saved)">\
        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>\
        Required Field\
        </span>',
    scope:{ field: '=', saved: '='},
    link: function (scope, elem, attrs) {
      //
    }
  }
}])
.directive('requiredToViewRatesMsg',[function(){
  return {
    restrict: 'E',
    template: '\
        <span class="alert alert-danger"\
        ng-show="showMsg()">\
        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>\
        Required to View Rates\
        </span>',
    scope:{ field: '=' },
    link: function (scope, elem, attrs) {
      
      scope.showMsg=function(){
      var isBlank=( !scope.field || (typeof scope.field.$viewValue)=='undefined' || scope.field.$viewValue==null),
        shouldBeHighlighted=isBlank && scope.$parent.attemptedViewRates || scope.$parent.attemptedSave;
      return shouldBeHighlighted;
      }
    }
  }
}])
.directive('drQuotePathPhones', [function () {
  return {
    scope: true,
    link: function (scope, element, attrs) {
  scope.ownerType = attrs.phoneOwner;
  scope.phoneForm = attrs.phoneForm;
      scope.phoneOwner = scope[attrs.phoneOwner];
      //If undefined/false, show required message when touched. otherwise wait until save attempted.
      scope.waitToValidate= !!attrs.waitToValidate;
      scope.saveAttemptIndicated=function(){
        var indicatorProvided=!!attrs.attemptedSaveIndicator;
        if(indicatorProvided){
          return scope[attrs.attemptedSaveIndicator];
        }else{
          return scope.attemptedSave;
        }
      };
    },
    templateUrl: '/quoter/phones.html'
  }
}])
.directive('drTobaccoInputs', [function () {
  return {
    restrict: 'A',
    transclude: true,
    templateUrl: '/quoter/tobacco-fieldset.html',
    link: function (scope, element, attrs) {
      scope.quoter=scope.$parent.quoter;
    },
    scope: {
      typeModel: '=',
      typeLabel: '@',
      currentModel: '=',
      lastUseModel: '=',
      averageUseModel: '=',
    }
  }
}])
;
