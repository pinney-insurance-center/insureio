// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require ./application/detect_chrome
//= require ./application/dln_validator
//= require ./application/constants
//= require ./application/dr_ng_directives
//= require ./application/dr_ng_filters
//= require ./application/DrBulletins-ng
//= require ./application/io_request_sanitizer

var insureioRttQuoteApp = angular.module('insureioRttQuoteApp', [
  //Dependencies
  'ui.select',
  'ui.mask',
  'ui.bootstrap',
  'xeditable',
  'ngSanitize',
  'ngResource',
  'rzModule',
  //App Modules
  'detectChrome',
  'ioConstants',
  'ioRequestSanitizer',
  'DrFilters',
  'dr.directives',
  'DrBulletins',
])
.config(['$locationProvider',
  function($locationProvider) {
      //Allows us to use standard url params (`/some/path?key=value`)
      //vs. Angular style (`/some/path#?key=value`).
      $locationProvider.html5Mode({
        enabled:true,
        rewriteLinks : false
      });
    }
])
.run(['$rootScope', '$compile', '$q', '$http', '$filter', 'editableOptions', 'editableThemes', 'bulletinService','ioConstantsObject',
  function ($rootScope, $compile, $q, $http, $filter, editableOptions, editableThemes, bulletinService, ioConstantsObject) {

    //Set options for xeditable.
    editableOptions.theme = 'default';
    //Overwrite submit and cancel button templates
    editableThemes['default'].submitTpl ='\
      <button type="submit" class="btn btn-primary">\
        <i class="fa fa-save"></i>\
      </button>\
    ';
    editableThemes['default'].cancelTpl ='\
      <button type="button" class="btn btn-warning" ng-click="$form.$cancel()">\
        <i class="fa fa-times"></i>\
      </button>\
    ';

    //Allows easy access to constants from templates.
    $.extend($rootScope, ioConstantsObject);

    window.$rootScope=$rootScope;
  }
])
.provider('RttSharedFunctionsModel', function RttSharedFunctionsProvider() {
  this.$get = ['$location','$resource','$http','$rootScope', 'bulletinService', function ($location, $resource, $http, $rootScope, bulletinService) {
    function RttSharedFunctionsModel ($scope) {
      this.$scope = $scope;
    }
    Object.defineProperties(RttSharedFunctionsModel.prototype, {
      getQueryData:{
        value: function(){
          var $scope=this.$scope;

          $scope.agentId   =$location.search()['agent_id'];
          $scope.key       =$location.search()['key'];
          $scope.brandId   =$location.search()['brand_id']||$location.search()['profile_id'];
          $scope.testEnv   =$location.search()['test']
          $scope.urlIsProduction=$location.protocol().match(/https/);

          //only present/needed for stakeholder signing or consumer resume links.
          $scope.rttGuid  =$location.search()['guid']||$location.search()['id'];
          $scope.personId =$location.search()['person_id'];
          $scope.resuming =$location.search()['resume_app'];
          if($scope.resuming){
            $scope.consumerId=$scope.personId;
          }
        }
      },
      getAgentSummary:{
        value: function(){
          var $scope=this.$scope;
          if(!$scope.agentId || !$scope.key || !$scope.brandId){
            $scope.agentSummary={error:'Invalid agent.'}
            return;
          }

          $scope.agentSummary = $resource('/rtt/agent_summary.json').get(
            {
              agent_id:  $scope.agentId,
              key:       $scope.key,
              brand_id:  $scope.brandId,
              guid:      $scope.rttGuid,//only present/needed for stakeholder signing or consumer resume links.
              person_id: $scope.personId//only present/needed for stakeholder signing or consumer resume links.
            },
            function(){//success
              if(!$scope.agentSummary.state_ids){ return; }
              $scope.rttAllowedStates=$scope.rttAllowedStates.filter(function(s) {
                  return $scope.agentSummary.state_ids.indexOf(s.id) !== -1;
              });
            },
            function(data){//error
              $scope.agentSummary.error=data.error||'Invalid agent.';
            }
          );
        },

      },
      getRoutingNumbersToBankNames:{
        value: function(){
          /*
          This list was gleaned from:
          https://www.random411.com/federal-reserve-bank-routing-numbers-bycity-usa-united-states-america/
          As unofficial info with no warranty is not necessarily 100% accurate or inclusive.
          There are about 9328 data rows in this file.
          */
          var $scope=this.$scope,
              transactionId=bulletinService.randomNonZeroId();
          $http.get('/rtt/routing_numbers_to_bank_names.csv')
          .success( function (data, status, headers, config){
            var rawData=data,
                parsedData=Papa.parse(rawData,{header:true});
            $scope.routingNumbersToBankNames=parsedData.data;
            if(parsedData.errors.length){
              console.error('Routing Number CSV parse errors:\n'+parsedData.errors.join("\n"));
            }
          }).error( function (data, status, headers, config){
            var failureMsg='Failed to load bank routing numbers.';
            console.log(failureMsg);
          });
        }
      },
      bankNameForRoutingNumber:{//called from the front end
        value:function(){
          var $scope=this.$scope;
          if(!$scope.consumer.eft_routing_num || $scope.consumer.eft_routing_num.length!=9){ return;}
          var name=null;
          for(var i in $scope.routingNumbersToBankNames){
            var x=$scope.routingNumbersToBankNames[i];
            if(x.routing_number==$scope.consumer.eft_routing_num){
              name=x.bank_name;
              break;
            }
          }
          if(name){
            $scope.consumer.eft_bank_name=name;
          }
          return name;
        }
      },
      verifyIdentity:{//called from the front end
        value:function(){
          var $scope=this.$scope,
              transactionId=bulletinService.randomNonZeroId();
          $http.put('/rtt/'+$scope.rttGuid+'/verify_stakeholder.json',{
            agent_id:   $scope.agentId,
            key:        $scope.key,
            person_id:  $scope.personId,
            zip:        $scope.person.zip,
            phone:      $scope.person.phone,
            birth:      $scope.person.birth,
          })
          .success(function(data, status, headers, config){
            if(data.verified){
              $scope.secureToken=data.secure_token;
              if( $scope.resuming ){
                var associationKeys=['addresses','phones','emails','cases','health_info'],
                    consumer=data.saved_consumer_data;
                for(var i in associationKeys){
                  var key=associationKeys[i];
                  consumer[key+'_attributes']=consumer[key]
                  delete consumer[key];
                }
                $.extend(true,$scope.consumer, consumer);
                $scope.scorHealthAnswers=$scope.consumer.scor_health_answers;
                delete $scope.consumer.scor_health_answers;
                $scope.setupAliasesForShorthand();
                $scope.calcAvailableRatesForConsumer();
              }
              $scope.nextSection();
            }else{
              var noMatchMsg='The data entered does not match what we have on file. '+
                'If you believe this to be in error, please contact your insurance agent or the primary insured.';
              $scope.addBulletin({text:noMatchMsg, id:transactionId, klass:'danger'})
            }
          })
          .error(function(data, status, headers, config){
            var failureMsg='Failed to verify identity. '+(data.error||data.errors);
            $scope.addBulletin({text:failureMsg, id:transactionId, klass:'danger'})
          });
        }
      },
      //
    });
    return RttSharedFunctionsModel;
  }];
})
.controller("MotoristsPathCtrl", [
  "$scope", "$rootScope", "$location", "$resource", '$http', '$log', '$templateRequest', '$timeout', '$interval', '$filter','$uibModal','bulletinService', 'requestSanitizer','RttSharedFunctionsModel',
  function ($scope, $rootScope, $location, $resource, $http, $log, $templateRequest, $timeout, $interval, $filter,$uibModal,bulletinService, requestSanitizer,RttSharedFunctionsModel) {
    $scope.initMotoristsQuotePath=function(){
      //Alias for views to be able to set variables at the controller scope level:
      $scope.$rttPathScope=$scope;
      $scope.devMode=false;
      console.log('Type `enterDevMode();` for convenient aliases and additional navigation buttons.');

      $scope.sharedFuncts = new RttSharedFunctionsModel($scope);
      $scope.sharedFuncts.getQueryData();

      //Once agent summary is loaded, this state list will be filtered for the agent in question.
      $scope.rttAllowedStates=$scope.STATES_RTT;

      $scope.sections=[
        {name:'Basic Info', viewed:true, subsections:['basic_info_01.html']},
        {name:'Instant Term Quotes', viewed:true,  subsections:['quotes_01.html']},
        {name:'Health Questions',    viewed:false, subsections:[]},
        {name:'Lifestyle Questions', viewed:false, subsections:[]},
        {name:'Application',         viewed:false, subsections:[
                                    'app_01_pi.html',
                                    'app_02_stakeholders.html',
                                    'app_03_rtt_ins_history.html',
                                    'app_04_payment_info.html',
                                    'app_05_verify_accuracy_and_esign.html',
                                    ]},
        {name:'Processing',         viewed:false, subsections:['scor_app_submit.html']},
        {name:'Confirmation',       viewed:false, subsections:['confirm_or_select_new_quote.html']},
        {name:'Congratulations',    viewed:false, subsections:['congratulations.html']}
      ];
      $scope.applicationLabels = [
                    'Proposed Insured',
                    'Beneficiary Information',
                    'Insurance History',
                    'Payment Information',
                    'Verify Accuracy and Consent To eSignature',
                    ];

      $scope.attemptedSave=false;
      $scope.autoSavePromises=[];
      $scope.reviewedTermsAndDisclosures=false;
      $scope.routingNumbersToBankNames=[];
      $scope.formHolder={};
      $scope.activeSectionIndex=0;
      $scope.activeSubsectionIndex=0;
      $scope.rttAnswers={};
      $scope.answers=$scope.scorHealthAnswers={};//The shorthand is used in the html templates.
      //Prepopulate answers to top level questions.
      for(var i=1; i<14; i++){
        var zeroPaddedNum=String(i).padStart(2,'0');
        $scope.scorHealthAnswers['RMM'+zeroPaddedNum]=null;
      }
      $scope.totalQuestions=0;
      $scope.activeQuestion=0;
      $scope.labelText=false;
      $scope.progressBarIndex=0;
      $scope.progressSections=[];
      $scope.sliderVisibleBar={
        value:275*1000,
        options:{
          floor:25*1000,
          ceil:500*1000,
          step:25*1000,
          showSelectionBar:true,
          hidePointerLabels:true,
          hideLimitLabels:true,
          //showTicks: true,
          translate: function(value){
            return '$'+value;
          },
          // ticksTooltip: function(value) {
          //    return (value).toLocaleString('en-US', { style: 'currency', currency: 'USD', }).slice(0,-3);
          // }
        }
      };

      $scope.PREMIUM_MODES=$scope.PREMIUM_MODES;

      $scope.consumerTemplate= {
        agent_id:   $scope.agentId,
        brand_id:   $scope.brandId,
        id:         $scope.consumerId,
        test_lead:  ($scope.testEnv && !$scope.consumerId),
        cases_attributes: [{
          scor_guid: $scope.rttGuid,
          is_test:($scope.testEnv && !$scope.consumerId),
          quoting_details_attributes:[{
            carrier_id:      $scope.CARRIER_ID_FOR_MOTORISTS,
            product_name:    'Real Time Term',
            product_type_id: $filter('whereKeyMatchesValue')( PRODUCT_TYPES,'name', 'Term')[0].id,
          }],
        }],
        health_info_attributes:{},
      };

      if( $scope.resuming ){
        var verifyIdSectionDescription={
          name:'Verify Your Identity',
          viewed:true,
          subsections:['stakeholder_verify_identity.html']
        };
        $scope.sections.unshift( verifyIdSectionDescription );
        $scope.consumer= $.extend(true,{},$scope.consumerTemplate);
        $scope.person=$scope.consumer;//for verification form
        $scope.setupAliasesForShorthand();//will run this again after identity is verified.
      }else{
        $scope.loadAndNormalizeLocallyStoredData();
      }

      if($scope.qda.face_amount){
        $scope.sliderVisibleBar.value=$scope.qda.face_amount;
      }

      $scope.sharedFuncts.getAgentSummary();

      $scope.getTemplates();
      $scope.getRateTables();
      $scope.getQuestions();
      $scope.sharedFuncts.getRoutingNumbersToBankNames();
      $scope.autoSaveTimer=$interval($scope.autoSaveAnswers, 60*1000);
    }

    window.enterDevMode=function(){
      $scope.devMode=true;
      $scope.$apply();//shows extra navigation for dev convenience
      window.$scope=$scope.$rttPathScope;
      $scope.$log = $log;//so events in the view can directly log to the console.
      $scope.$timeout=$timeout;
      $scope.$interval=$interval;
      $interval.cancel($scope.autoSaveTimer);
      console.log('Entered dev mode.');
    }

    $scope.loadAndNormalizeLocallyStoredData=function(){
      if( localStorage.InsureioConsumerData     =='undefined' ){ localStorage.InsureioConsumerData=''; }
      if( localStorage.InsureioRttAnswers       =='undefined' ){ localStorage.InsureioRttAnswers=''; }
      if( localStorage.InsureioScorHealthAnswers=='undefined' ){ localStorage.InsureioScorHealthAnswers=''; }

      $scope.consumer= JSON.parse( localStorage.InsureioConsumerData || '{}' );
      if(!$scope.consumer.agent_id || !$scope.consumer.brand_id){
        $.extend(true,$scope.consumer,$scope.consumerTemplate);
      }
      if($scope.consumer.rtt_answers){
        //Widget puts this data inside the consumer object.
        //This path keeps them separate.
        $.extend($scope.rttAnswers,$scope.consumer.rtt_answers);
        delete $scope.consumer.rtt_answers;
      }
      $.extend($scope.rttAnswers, JSON.parse(localStorage.InsureioRttAnswers || '{}' ) );
      $.extend($scope.scorHealthAnswers, JSON.parse(localStorage.InsureioScorHealthAnswers || '{}' ) );

      //Convert birth, tobacco, gender, state values from widget if present.
      if(!$scope.consumer.birth_or_trust_date){
        if($scope.consumer.birth){
          $scope.consumer.birth_or_trust_date=$scope.consumer.birth;
        }else if($scope.consumer.birth_month && $scope.consumer.birth_day && $scope.consumer.birth_year){
          $scope.consumer.birth_or_trust_date=$scope.consumer.birth_month+'/'+$scope.consumer.birth_day+'/'+$scope.consumer.birth_year;
        }
        delete $scope.consumer.birth;
        delete $scope.consumer.birth_month;
        delete $scope.consumer.birth_day;
        delete $scope.consumer.birth_year;
      }
      if( $scope.consumer.opportunities_attributes ){
        delete $scope.consumer.opportunities_attributes;
      }
      if(!$scope.consumer.health_info_attributes){
        $scope.consumer.health_info_attributes={};
      }
      if(!$scope.consumer.health_info_attributes.tobacco && (typeof $scope.consumer.tobacco)!='undefined' ){
        $scope.consumer.health_info_attributes.tobacco=$scope.consumer.tobacco=='true';
        delete $scope.consumer.tobacco;
      }
      if($scope.consumer.health_info_attributes.feet && (typeof $scope.consumer.health_info_attributes.feet)=='string' ){
        $scope.consumer.health_info_attributes.feet=parseInt($scope.consumer.health_info_attributes.feet)
      }
      if($scope.consumer.health_info_attributes.inches && (typeof $scope.consumer.health_info_attributes.inches)=='string' ){
        $scope.consumer.health_info_attributes.inches=parseInt($scope.consumer.health_info_attributes.inches)
      }
      if($scope.consumer.health_info_attributes.weight && (typeof $scope.consumer.health_info_attributes.weight)=='string' ){
        $scope.consumer.health_info_attributes.weight=parseInt($scope.consumer.health_info_attributes.weight)
      }
      if( (typeof $scope.consumer.gender) =='boolean' || (typeof $scope.consumer.gender) =='integer'){
        $scope.consumer.gender= $scope.consumer.gender==true ? 'm' : 'f';
      }
      if(!$scope.consumer.addresses_attributes){
        $scope.consumer.addresses_attributes=[{}]
      }
      if( (!$scope.consumer.addresses_attributes[0] || !$scope.consumer.addresses_attributes[0].state_id) && $scope.consumer.state){
        $scope.consumer.addresses_attributes[0].state_id=
          $filter('whereKeyMatchesValue')( STATES,'name', $scope.consumer.state)[0].id;
        delete $scope.consumer.state;
      }
      if( $scope.consumer.addresses_attributes[0].state_id && (typeof $scope.consumer.addresses_attributes[0].state_id)=='string' ){
        $scope.consumer.addresses_attributes[0].state_id=parseInt($scope.consumer.addresses_attributes[0].state_id);
      }
      if( (typeof $scope.rttAnswers.has_declined_substandard_or_pending_apps)!='undefined' &&
          !!$scope.rttAnswers.has_declined_substandard_or_pending_apps!=$scope.rttAnswers.has_declined_substandard_or_pending_apps ){
        $scope.rttAnswers.has_declined_substandard_or_pending_apps=$scope.rttAnswers.has_declined_substandard_or_pending_apps=='true';
      }
      if( (typeof $scope.consumer.gender)=='undefined' ||
        (typeof $scope.consumer.birth_or_trust_date)=='undefined' ||
        (typeof $scope.consumer.health_info_attributes.tobacco)=='undefined' ||
        (typeof $scope.rttAnswers.has_declined_substandard_or_pending_apps)=='undefined' ){
        //Insufficient data to provide quotes.
        //`calcAvailableRatesForConsumer` requires tobacco, age, gender, and past health designation.
        //Prepend another section to the path, for basic personal/contact info.
        //Start at 'Basic Info'
      }else{
        $scope.nextSection();
        $timeout($scope.autoSaveAnswers,1);
      }

      $scope.setupAliasesForShorthand();
      $scope.readOrGenerateGuid();
    }

    $scope.setupAliasesForShorthand=function(){
        $scope.policy=$scope.kase=$scope.consumer.cases_attributes[0];
        $scope.health=$scope.consumer.health_info_attributes;
        $scope.qda=$scope.policy.quoting_details_attributes[0];
    }

    $scope.preexistingPolicyTemplate={
      sales_stage_id:5,
      is_a_preexisting_policy:true,
      placed_details_attributes:{},
    };

    $scope.getTemplates=function(){
      for(var sN in $scope.sections){
        for(var ssN in $scope.sections[sN].subsections){
          var templateUrl=$scope.sections[sN].subsections[ssN];
          $templateRequest('/rtt/'+templateUrl);
        }
      }
    }

    $scope.ensureAtLeastOnePreexistingPolicy=function(){
      if(!$scope.rttAnswers.is_replacing){ return; }
      if($scope.consumer.cases_attributes.length<2){
        $scope.addPreexistingPolicy();
      }else{
        for(var i in _){
          var p=[i];
          if(!p._destroy){
            return;
          }
        }
        $scope.addPreexistingPolicy();
      }
    }

    $scope.addPreexistingPolicy=function(){
      var eP=$.extend(true,{},$scope.preexistingPolicyTemplate);
      $scope.consumer.cases_attributes.push(eP);
    }

    $scope.destroyPreexistingPolicy=function(eP,index){
      if(eP.id){
        eP._destroy=true
      }else{
        $scope.consumer.cases_attributes.splice(index+1,1);
      }
    }

    $scope.destroySR=function(sR, index){
      if(sR.id){
        sR._destroy=true
      }else{
        $scope.policy.stakeholder_relationships_attributes.splice(index,1);
      }
    }

    $scope.ensureAtLeastOneStakeholderRelationship= function(){
      if(!$scope.policy.stakeholder_relationships_attributes){
        $scope.policy.stakeholder_relationships_attributes=[]
      }
      if(!$scope.policy.stakeholder_relationships_attributes.length){
        $scope.addstakeholderRelationship();
      }
    }

    $scope.addstakeholderRelationship = function () {
      var sR=$.extend({},true,{
        contingent:false,
        is_beneficiary:true,
        stakeholder_attributes:{
          agent_id:$scope.consumer.agent_id,
          brand_id:$scope.consumer.brand_id,
          emails_attributes:[],
          phones_attributes:[],
          addresses_attributes:[],
          entity_type_id:1
        }
      });
      $scope.kase=$scope.consumer.cases_attributes[0];
      if(!$scope.kase.stakeholder_relationships_attributes){
        $scope.kase.stakeholder_relationships_attributes=[];
      }
      $scope.kase.stakeholder_relationships_attributes.push(sR);
    }

    $scope.ensureContactMethodsForOwnerPayer=function(sR){
      if(!sR.is_owner && !sR.is_payer){ return; }
      var cA=sR.stakeholder_attributes;
      if(!cA.phones_attributes.length){
        cA.phones_attributes.push({});
      }
      if(!cA.addresses_attributes.length){
        cA.addresses_attributes.push({});
      }
      if(!cA.emails_attributes.length){
        cA.emails_attributes.push({});
      }
    }

    $scope.ownerSR=function(){
      var sRAs=$scope.policy.stakeholder_relationships_attributes,
          owner=null;
      for(var i in sRAs){
        if(sRAs[i].is_owner && !sRAs[i]._destroy){
          owner=sRAs[i];
          break;
        }
      }
      return owner;
    }

    $scope.payerSR=function(){
      var sRAs=$scope.policy.stakeholder_relationships_attributes,
          payer=null;
      for(var i in sRAs){
        if(sRAs[i].is_payer && !sRAs[i]._destroy){
          payer=sRAs[i];
          break;
        }
      }
      return payer;
    }

    $scope.skipToApplication=function(){
      $scope.activeSectionIndex = 2;
      if( $scope.sections[0].name=='Basic Info' ){
        $scope.activeSectionIndex++;
      }
      $scope.activeSubsectionIndex = 0;
      $scope.setProgressBar();
      $(".section-content").scrollTop(0);
    }

    $scope.refreshSlider=function(){
      $scope.sliderVisibleBar.value=$scope.qda.face_amount;
      $timeout(function(){
        $scope.$broadcast('rzSliderForceRender');
      });
    }

    $scope.getModalPremium=function(r){
      var modalAdjustmentFactorOptions={1: 1, 2: .52, 3: .26, 4: .086},
          modalAdjustmentFactor=modalAdjustmentFactorOptions[$scope.qda.premium_mode_id],
          coverageAmt=$scope.sliderVisibleBar.value,
          policyFeeAmt=60;
      return ( ( parseFloat(r.premium_rate_per_thousand)*coverageAmt/1000) +policyFeeAmt)*modalAdjustmentFactor;
    }

    $scope.calcExpectedQuestions=function(){
      $scope.totalQuestions=0;
      for(x=0;x<$scope.sections.length-1;x++){
        $scope.totalQuestions+=$scope.sections[x].subsections.length;
      }
    }

    $scope.getProgressPercent=function(){
      return Math.floor(($scope.progressBarIndex/$scope.totalQuestions)*100)
    }

    $scope.getTotalProgress=function(){
      var total = 0;
      for (var x=0;x<$scope.progressSections.length;x++)
      {
        total += $scope.progressSections[x].value;
      }
      return Math.min(100,total);
    }

    $scope.getStackedBarValueByIndex=function(index){
      return $scope.progressSections[index].value;
    }

    $scope.getMaxProgressValue=function(){
      return 100/$scope.progressSections.length;
    }

    $scope.getSubsectionHeading=function(showApplicationAsHeading){
      var currentSection=$scope.sections[$scope.activeSectionIndex];
      if(currentSection.name=='Application'&&!showApplicationAsHeading){
        var currentHeading=$scope.applicationLabels[$scope.activeSubsectionIndex];
        return currentHeading||'Application';
      }
      if(currentSection.name=='Health Questions'){
        var currentHeading=$scope.scorHealthQuestionSets[$scope.activeSubsectionIndex].headingText;
        return currentHeading||'Health Questions';
      }else if(currentSection.name=='Lifestyle Questions'){
        var currentHeading=$scope.scorLifestyleQuestionSets[$scope.activeSubsectionIndex].headingText;
        return currentHeading||'Lifestyle Questions';
      }else{
        return currentSection.name;
      }
    }

    $scope.getProgressIndexByHeading=function(heading){
      for (var x=0;x<$scope.progressSections.length;x++){
        if ($scope.progressSections[x].heading == heading){
          return x;
        }
      }
      return -1;
    }

    $scope.setProgressBar=function(byOverallPercent){
      var percent_per_bar=$scope.getMaxProgressValue();
   
      for(var z=0;z<$scope.progressSections.length;z++){
        $scope.progressSections[z].value=0;
      }
      if (byOverallPercent){
        var i=0;
        var overall=$scope.getProgressPercent();
        while(overall>0){
          if(overall>percent_per_bar){
            $scope.progressSections[i].value=percent_per_bar;
            overall-=percent_per_bar;
          }else{
            $scope.progressSections[i].value=overall;
            overall=0;
          }
          i++;
        }
      }
      else{
        $scope.labelText = $scope.getSubsectionHeading(true);
        var progressIndex = $scope.getProgressIndexByHeading($scope.labelText);
        var currentSection=$scope.sections[$scope.activeSectionIndex];
        localStorage.InsureioRttSectionIndex=$scope.activeSectionIndex;

        while (progressIndex>=0){
          $scope.progressSections[progressIndex].value = percent_per_bar;
          progressIndex--;
        }

        if ($scope.activeSectionIndex==0){
          $scope.labelText = false;
        }
      }
    }

    $scope.buildProgressBars=function(){
      for (var i = 1;i<$scope.sections.length;i++)
      {
        var currentSection=$scope.sections[i];
        if(currentSection.name=='Health Questions'){
          for (var x = 0;x<$scope.scorHealthQuestionSets.length;x++){
            var question = $scope.scorHealthQuestionSets[x];
            if (!question.conditionalOnQuestion){
              $scope.progressSections.push({value:0,heading:question.headingText});
            }
          }
        }else if(currentSection.name=='Lifestyle Questions'){
          for (var x = 0;x<$scope.scorLifestyleQuestionSets.length;x++){
            var question = $scope.scorLifestyleQuestionSets[x];
            if (!question.conditionalOnQuestion){
              $scope.progressSections.push({value:0,heading:question.headingText});
            }
          }
        }
        else{
          $scope.progressSections.push({value:0,heading:currentSection.name});
        }
      }
    }

    $scope.generateGuid=function() {//taken from: https://stackoverflow.com/a/105074/1998680
      function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
    }
    //If coming from the rtt widget, this will already be set.
    $scope.readOrGenerateGuid=function(){
      if(localStorage.InsureioRttGuid){
        $scope.rttGuid=localStorage.InsureioRttGuid;
      }else{
        $scope.rttGuid=localStorage.InsureioRttGuid=$scope.generateGuid();
      }
      $scope.policy.scor_guid=$scope.rttGuid;
    }

    $scope.storeDataLocally=function(){
      if( (typeof Storage) == "undefined" ){ return; }
      localStorage.InsureioConsumerData     =JSON.stringify($scope.consumer);
      localStorage.InsureioScorHealthAnswers=JSON.stringify($scope.scorHealthAnswers);
      localStorage.InsureioRttAnswers       =JSON.stringify($scope.rttAnswers)
    }

    $scope.dataIsUpToDate=function(){
      var localIsUpToDate = (
                              localStorage.InsureioConsumerData     ==JSON.stringify($scope.consumer) &&
                              localStorage.InsureioRttAnswers       ==JSON.stringify($scope.rttAnswers) &&
                              localStorage.InsureioScorHealthAnswers==JSON.stringify($scope.scorHealthAnswers)
                            ),
          serverIsUpToDate= localStorage.InsureioServerIsUpToDate,
          isUpToDate      = localIsUpToDate && serverIsUpToDate;
      console.log('localStorage up to date:',localIsUpToDate,'\nserver up to date:',serverIsUpToDate);
      return isUpToDate;
    }

    $scope.sanityCheckScorConditionalQuestions=function(){
      for(var a in $scope.answers){
        for(var b in $scope.answers){
          var bIsConditionalOnA=b!=a && b.includes(a);
          if( bIsConditionalOnA && !$scope.answers[a] ){
            delete $scope.answers[b];
          }
        }
      }
    }

    //@returns undefined, true, or a promise for the save request.
    $scope.autoSaveAnswers=function(reportErrors){
      if( !$scope.subsectionIsValid() ){ return; }
      $scope.checkForPendingSaves();
      if( $scope.savesPending ){ return; }
      if( $scope.dataIsUpToDate() ){ return true; }
      $scope.sanityCheckScorConditionalQuestions();
      $scope.storeDataLocally();
      var consumerObj=$.extend(true,{},$scope.consumer),
          transactionId=bulletinService.randomNonZeroId(),
          policyIndicesToRemove=[];

      if(consumerObj.cases_attributes && consumerObj.cases_attributes.length>1){
        for(var i in consumerObj.cases_attributes){
          var e=consumerObj.cases_attributes[i];
          switch(e.$replacedOrFinanced){
            case 'replaced':
              //1 here is a a placeholder, which will be filled with the proper value by a rails model callback.
              e.replaced_by_id=$scope.policy.id||1;
              break;
            case 'financing':
              e.financing=true;
              break;
          }
          if(e.sales_stage_id==5 && !e.policy_number && !e.id && !reportErrors){
            //Don't actually autosave existing policies without a policy number.
            policyIndicesToRemove.push(i);
          }
        }
        for(var i=policyIndicesToRemove.length-1; i>-1; i--){
          consumerObj.cases_attributes.splice(policyIndicesToRemove[i],1);
        }
      }

      $scope.savesPending=true;
      requestSanitizer.clean(consumerObj, null, true);
      var autoSavePromise=$http.post('/rtt/save.json',{
        agent_id:   $scope.agentId,
        key:        $scope.key,
        brand_id:   $scope.brandId,
        consumer:   consumerObj,
        guid:                $scope.rttGuid,
        rtt_answers:         $scope.rttAnswers,
        scor_health_answers: $scope.scorHealthAnswers
      })
      .success(function(data, status, headers, config){
        console.log('Successfully autosaved.',new Date());
        $scope.consumer.id=data.id;
        $scope.policy.id=data.policy_id;
        $scope.policy.scor_guid=data.scor_guid;//corrects non-unique guid to avoid problems later on.
        $scope.rttGuid=data.scor_guid;
        $scope.qda.id=data.quoting_details_id;
        $scope.consumer.addresses_attributes[0].id=data.address_id
        localStorage.InsureioServerIsUpToDate='true';
        //Match returned ids to stakeholder rels by name.
        //This save should not happen unless required stakeholder fields are present,
        //which should together ensure proper matching of ids and no creation of duplicate stakeholder relationships.
        var sRAs=$scope.policy.stakeholder_relationships_attributes,
            savedSRAs=data.stakeholder_relationships;
        if(sRAs && sRAs.length){
          for(var i in sRAs){
            for(var j in savedSRAs){
              if(sRAs[i].stakeholder_attributes.full_name==savedSRAs[i].full_name){
                sRAs[i].id=savedSRAs[i].id;
                sRAs[i].stakeholder_attributes.id=savedSRAs[i].stakeholder_id;
              }
            }
          }
        }
        //Math the returned ids to existing policies by `policy_number`.
        var ePs=$scope.consumer.cases_attributes,
            savedEPs=data.existing_policies;
        if(ePs.length>1 && savedEPs.length){
          for(var i in ePs){
            for(var j in savedEPs){
              if(ePs[i].sales_stage_id==5 && ePs[i].policy_number && ePs[i].policy_number==savedEPs[j].policy_number){
                ePs[i].id=savedEPs[j].id;
              }
            }
          }
        }
        $scope.$rttPathScope.attemptedSave=false;
      })
      .error(function(data, status, headers, config){
        console.log('Failed to autosave.',(data.error||data.errors),new Date());
        if(reportErrors){
          var failureMsg='Failed to autosave. '+(data.error||data.errors)+' If this issue persists, please contact your insurance agent for assistance.'
          $scope.addBulletin({text:failureMsg, id:transactionId, klass:'danger'});
        }
        localStorage.InsureioServerIsUpToDate='';//Has to be a string. Luckily an empty string is falsey.
      })
      .finally(function() {
        //Running `checkForPendingSaves` within this callback seems to sometimes still show this request as pending.
        //A short timeout fixes that.
        $timeout(function(){ $scope.checkForPendingSaves(); }, 100 );
      });
      $scope.autoSavePromises.push(autoSavePromise);
      return autoSavePromise;
    }

    $scope.checkForPendingSaves=function(){
      if(!$scope.autoSavePromises.length){
        $scope.savesPending=false;
      }else{
        for(var i in $scope.autoSavePromises){
          var p=$scope.autoSavePromises[i];
          if(p.$$state.status!=1){
            $scope.savesPending=true;
            return;
          }
        }
        $scope.savesPending=false;
      }
    }

    $scope.saveAndSubmitForUnderwriting=function(){
      if( !$scope.subsectionIsValid(true) || $scope.submitting ){ return; }//If invalid, show error message and cancel submission.
      $scope.storeDataLocally();
      $scope.submitting=true;
      var consumerObj=$.extend(true,{},$scope.consumer);
      $scope.processingStatusText='Please be patient while we process your request.';
      $interval.cancel($scope.autoSaveTimer);

      requestSanitizer.clean(consumerObj, null, true);
      $http.post('/rtt/save_and_sign_for_consumer.json',{
        agent_id:            $scope.agentId,
        key:                 $scope.key,
        brand_id:            $scope.brandId,
        consumer:            consumerObj,
        guid:                $scope.rttGuid,
        rtt_answers:         $scope.rttAnswers,
        scor_health_answers: $scope.scorHealthAnswers
      })
      .success(function(data, status, headers, config){

        if($scope.rttAnswers.has_declined_substandard_or_pending_apps){
          $scope.processingStatusText='';
          $scope.hideLoadingIcon=true;
          $scope.nextSubsection();
          return;
        }
        $scope.processingStatusText=data.message;
        if( !$scope.ownerSR() && !$scope.payerSR() ){
          $scope.processingStatusText="Successfully sent for processing. Awaiting a decision."
          //Velogica business hours are Mon-Sat 7AM EST - 12AM EST.
          var currentESTTime=moment().tz('America/New_York'),
              withinUnderwritingBusinessHours=(currentESTTime.day()<=6 && currentESTTime.hour()>=7);

          if(withinUnderwritingBusinessHours){
            const pollingInitialDelay=15*1000;//15 seconds
            const pollingFrequency   = 3*1000;//3 seconds
            $timeout(function(){
              $scope.underwritingPollTimer=$interval($scope.pollForUnderwritingDecision, pollingFrequency);
            },pollingInitialDelay);
          }else{
            //Send only one polling request. If more info is needed for a decision,
            //it will need to be checked again during their business hours.
            $timeout(function(){ $scope.pollForUnderwritingDecision(); },15*1000);
          }
        }else{
          $scope.hideLoadingIcon=true;
          localStorage.clear();
        }
        $scope.submitting=false;
        $scope.sentForProcessing=true;
        localStorage.InsureioServerIsUpToDate='true';
      })
      .error(function(data, status, headers, config){
        $scope.processingStatusText='Failed to process. '+(data.error||data.errors)+
          '\nIf this issue persists, please contact your insurance agent for assistance.';
        $scope.submitting=false;
        $scope.hideLoadingIcon=true;
        localStorage.InsureioServerIsUpToDate='';//Has to be a string. Luckily an empty string is falsey.
      });
    }

    $scope.confirmQuotedPolicy=function(selectedQuote){
      var qda=$scope.qda;
      delete qda.id;
      qda.sales_stage_id=5;
      qda.face_amount=$scope.sliderVisibleBar.value;
      qda.modal_premium=$scope.getModalPremium(selectedQuote),
      qda.duration_id=$filter('whereKeyMatchesValue')( DURATIONS,'years', parseInt(selectedQuote.duration) )[0].id;
      qda.product_name=('Real Time Term '+selectedQuote.duration).replace(/\s+/,' ');
      $scope.policy.status_type_id=698;//set rtt in force status type.
      $scope.nextSubsection();//this logic includes saving.
      localStorage.clear();
      $interval.cancel($scope.autoSaveTimer);
      $timeout.cancel($scope.offerNotTakenTimer);
    }

    $scope.pollForUnderwritingDecision=function(){
      if($scope.pollInProgress){ return; }//Don't poll again if a poll is still in progress.
      $scope.pollInProgress=true;
      $http.get('/rtt/'+$scope.rttGuid+'/poll_for_underwriting_decision.json',{params:{
          agent_id:            $scope.agentId,
          key:                 $scope.key
        }
      })
      .success(function(data, status, headers, config){
        localStorage.InsureioServerIsUpToDate='true';
        $scope.hideLoadingIcon=true;
        $scope.pollInProgress=false;

        if(!data.system_available){
          $scope.processingStatusText="Automated underwriting is unavailable at this time."+
            "\nYour agent will contact you soon with an underwriting decision so you can finalize your application."
          $interval.cancel($scope.underwritingPollTimer);
          $scope.hideLoadingIcon=true;
          return;
        }
        if(!data.underwriting_completed){
          $scope.processingStatusText="Still waiting for a decision."
          if(data.decision=='REFERRED'){
            $scope.processingStatusText+="It looks like we may need more paperwork from you."+
            "\nYour agent will contact you soon regarding next steps in the process."
          }else if(data.decision=='INFORMATION_REQUESTED'){
            $scope.processingStatusText+="Your request was referred to an underwriter for a decision."
          }
          return;
        }

        if(data.decision=='ACCEPTED'){
          $scope.processingStatusText="Your request was approved!"
          $scope.consumer.health_info_attributes.tobacco=data.tobacco_class_name=='SM';
          $scope.qda.id=data.quoting_details_id;
          $scope.qda.carrier_health_class=data.risk_class_name=='PF' ? 'Premier ' : 'Preferred ';
          $scope.qda.carrier_health_class+=data.tobacco_class_name=='SM' ? 'Tobacco' : 'Non-Tobacco';
          $scope.storeDataLocally();
          $scope.progressPastProcessingSection();
          //40 minutes after receiving acceptance unless the offer is taken by clicking the purchase button.
          $scope.offerNotTakenTimer=$timeout($scope.setOfferNotTaken, 40*60*1000);
        }else if(data.decision=='DECLINED'){
          $scope.processingStatusText="Your request was declined."+
            "\nYour insurance agent will contact you soon to suggest some other options."
          $scope.progressPastProcessingSection();
          localStorage.clear();
        }else if(data.decision=='see status'){
          var decision=data.io_status.status_type_name.replace(/rtt/i, '').replace(/-|–/, '').toLowerCase();
          $scope.processingStatusText="Your request was "+decision+".";
          if( decision.match(/additional underwriting/i) ){
            $scope.processingStatusText+=" When the review is complete, your agent will notify you using the contact information you provided. You may close your browser now.";
            localStorage.clear();
          }
          if( decision.match(/approved/i) ){
            $scope.consumer.health_info_attributes.tobacco=data.tobacco_class_name=='SM';
            $scope.qda.id=data.quoting_details_id;
            $scope.qda.carrier_health_class=data.risk_class_name=='PF' ? 'Premier ' : 'Preferred ';
            $scope.qda.carrier_health_class+=data.tobacco_class_name=='SM' ? 'Tobacco' : 'Non-Tobacco';
            $scope.storeDataLocally();
            $scope.progressPastProcessingSection();
            //40 minutes after receiving acceptance unless the offer is taken by clicking the purchase button.
            $scope.offerNotTakenTimer=$timeout($scope.setOfferNotTaken, 40*60*1000);
          }
        }else{
          //Likely the decision is 'WITHDRAWN', indicating a problem with the app format.
          //Ideally, consumers will never encounter this, but if they do,
          //better to give them some coherent feedback that they can report to their agent.
          $scope.processingStatusText="Failed to fetch underwriting decision."+(warnings||'')+
            "\nYour insurance agent will contact you soon with an underwriting decision so you can finalize your application.";
        }
        $interval.cancel($scope.underwritingPollTimer);
      })
      .error(function(data, status, headers, config){
        $scope.processingStatusText='Failed to fetch underwriting decision. '+(data.errors||'')+' If this issue persists, please contact your insurance agent for assistance.';
        $scope.pollInProgress=false;
      });
    }

    //Because requests are asynchronous, two underwriting result polling requests may complete at the same time.
    //This function ensures that the section only progresses once.
    $scope.progressPastProcessingSection=function(){
      var currentSection=$scope.sections[$scope.activeSectionIndex];
      if(currentSection.name=='Processing'){
        $scope.calcAvailableRatesForConsumer();
        $scope.nextSubsection();
        $scope.refreshSlider();
      }
    }

    $scope.setOfferNotTaken=function(){
      $scope.policy.status_type_id=697;
      $scope.autoSaveAnswers(true);
      var timeoutWarningText="Too much time has elapsed since your approval, and you have not confirmed your coverage amount."+
            "\nContact your agent if you wish to proceed and they can assist in finalizing your application."
      $scope.addBulletin({text:timeoutWarningText, klass:'danger'})
      localStorage.clear();
      $interval.cancel($scope.autoSaveTimer);
    }

    $scope.getRateTables=function(){
      var transactionId=bulletinService.randomNonZeroId();
      $http.get('/rtt/premium_rate_per_thousand.csv')
      .success( function (data, status, headers, config){
        var rawRates=data,
            parsedRates=Papa.parse(rawRates,{header:true});
        $scope.rttRatesPerThousand=parsedRates.data;
        if(parsedRates.errors.length){
          console.error('Rate Table CSV parse errors:',parsedRates.errors);
        }
        if( !$scope.resuming ){
          $scope.calcAvailableRatesForConsumer();
        }
      }).error( function (data, status, headers, config){
        var failureMsg='Failed to load rate table.\n'+
          'If this issue persists, please contact your insurance agent for assistance.';
        $scope.addBulletin({
          text:failureMsg,
          id:transactionId,
          klass:'danger'
        });
      });
    }


    //Based on tobacco, age, gender, and past health designation.
    //The tobacco and past health designation combine into a health class.
    $scope.calcAvailableRatesForConsumer=function(){
      var matchingRates=[],
          tobacco=$scope.consumer.health_info_attributes.tobacco,
          substandard=$scope.rttAnswers.has_declined_substandard_or_pending_apps,
          bestRateClassId,
          age=moment().diff($scope.consumer.birth_or_trust_date,'years'),
          healthClassName=$scope.qda.carrier_health_class;
      //Health class ids refer to RttHealthClassOption / HEALTH_CLASSES_RTT.
      if(tobacco){
        bestRateClassId=3;
      }else if(substandard || (healthClassName && healthClassName.match(/Preferred/) ) ){
        bestRateClassId=2;
      }else{
        bestRateClassId=1;
      }
      for(var i in $scope.rttRatesPerThousand){
        var r=$scope.rttRatesPerThousand[i];
        if(r.age==age && r.gender==$scope.consumer.gender && bestRateClassId==parseInt(r.health_class_id) ){
          matchingRates.push(r);
        }
      }
      $scope.availableRatesForConsumer=matchingRates;
      if(!$scope.qda.premium_mode_id){
        $scope.qda.premium_mode_id=$filter('whereKeyMatchesValue')( PREMIUM_MODES,'name', 'Monthly')[0].id;
      }
      return $scope.availableRatesForConsumer;
    }


    $scope.getQuestions=function(){
      var transactionId=bulletinService.randomNonZeroId();
      $http.get('/rtt/scor_question_sets.json')
      .success( function (data, status, headers, config){
        var healthSection,
            lifestyleSection;
        for(var i in $scope.sections){
          if($scope.sections[i].name=='Health Questions'){
            healthSection=$scope.sections[i];
          }else if($scope.sections[i].name=='Lifestyle Questions'){
            lifestyleSection=$scope.sections[i];
          }
          if(healthSection && lifestyleSection){ break; }
        }
        $scope.scorHealthQuestionSets=data.healthQuestionSets;
        $scope.scorLifestyleQuestionSets=data.lifestyleQuestionSets;
        //These two loops ensure that `subsections.length` is correct
        //and that the correct helper text is shown at the top.
        for(var i=0; i<$scope.scorHealthQuestionSets.length; i++){
          healthSection.subsections.push('health_questions.html');
        }
        for(var i=0; i<$scope.scorLifestyleQuestionSets.length; i++){
          lifestyleSection.subsections.push('lifestyle_questions.html');
        }
        $scope.buildProgressBars();
        if( parseInt(localStorage.InsureioRttSectionIndex)>=0 ){
          $scope.activeSectionIndex=Math.min( parseInt(localStorage.InsureioRttSectionIndex), $scope.sections.length-1 );
          $scope.setProgressBar();
          var currentSection=$scope.sections[$scope.activeSectionIndex];
          if(currentSection.name=='Processing'){
            $scope.saveAndSubmitForUnderwriting();
          }else if(currentSection.name=='Instant Term Quotes'){
            $scope.openWelcomeModal();
          }
        }else{
          localStorage.InsureioRttSectionIndex=0;
        }
      }).error( function (data, status, headers, config){
        var failureMsg='Failed to load health and lifestyle questions. '+
          'If this issue persists, please contact your insurance agent for assistance.';
        $scope.addBulletin({
          text:failureMsg,
          id:transactionId,
          klass:'danger'
        });
      });
    }

    $scope.canShowSubsection=function(subsection){
      var currentSubsectionIsConditional=!!subsection.conditionalOnQuestion,
          age=moment().diff($scope.consumer.birth_or_trust_date,'years'),
          canShowCurrentSubsection= (subsection.headingText=='Last Physician Visit Questions' && !subsection.conditionalOnQuestion && age>=60) ||
            subsection.headingText!='Last Physician Visit Questions' &&
            !currentSubsectionIsConditional ||
            !!$scope.scorHealthAnswers[subsection.conditionalOnQuestion];
      return canShowCurrentSubsection;
    }

    $scope.nextSubsection=function(setProgress,skipValidation){
      if( (typeof setProgress)=='undefined' ){
        setProgress=true;
      }
      if( !$scope.subsectionIsValid(!skipValidation) && !skipValidation ){ return; }
      $scope.progressBarIndex++;
      var currentSection=$scope.sections[$scope.activeSectionIndex];
      if(currentSection.subsections.length>($scope.activeSubsectionIndex+1) ){
        $scope.activeSubsectionIndex+=1;
        var healthOrLifestyle=currentSection.name.match(/^(Health|Lifestyle)/);
        if( healthOrLifestyle ){
          healthOrLifestyle=healthOrLifestyle[0];
          var currentSubsection=$scope['scor'+healthOrLifestyle+'QuestionSets'][$scope.activeSubsectionIndex],
              canShowCurrentSubsection=$scope.canShowSubsection(currentSubsection);
          if (currentSubsection.conditionalOnQuestion) {
             setProgress=false;
          }
          if(!canShowCurrentSubsection){
            $scope.nextSubsection(true,true);
          }
        }else if(
          currentSection.name=='Application' &&
          currentSection.subsections[$scope.activeSubsectionIndex]=='app_04_payment_info.html' &&
          $scope.payerSR()
        ){
          $scope.nextSubsection(true,true);
        }
      }else if(currentSection.name=='Confirmation'){
        $timeout($scope.refreshSlider,100);
        $scope.nextSection(setProgress);
      }else{
        $scope.nextSection(setProgress);
      }
      $scope.scrollTop(0);
      if (setProgress){
        $scope.setProgressBar();
      }
      $scope.autoSaveAnswers(true);
    }

    $scope.prevSubsection=function(setProgress){
      if( (typeof setProgress)=='undefined' ){
        setProgress=true;
      }
      $scope.progressBarIndex--;
      var currentSection=$scope.sections[$scope.activeSectionIndex];
      if($scope.activeSubsectionIndex-1>=0){
        $scope.activeSubsectionIndex-=1;
        var healthOrLifestyle=currentSection.name.match(/^(Health|Lifestyle)/);
        if( healthOrLifestyle ){
          healthOrLifestyle=healthOrLifestyle[0];
          var currentSubsection=$scope['scor'+healthOrLifestyle+'QuestionSets'][$scope.activeSubsectionIndex],
              canShowCurrentSubsection=$scope.canShowSubsection(currentSubsection);
          // if (currentSubsection.conditionalOnQuestion) {
          //   setProgress=false;
          // }
          if(!canShowCurrentSubsection){
            setProgress=false;
            $scope.prevSubsection();
          }
        }else if(
          currentSection.name=='Application' &&
          currentSection.subsections[$scope.activeSubsectionIndex]=='app_04_payment_info.html' &&
          $scope.payerSR()
        ){
          $scope.prevSubsection();
        }
      }else{
        $scope.prevSection();
      }
      $scope.scrollTop(0);
      if (setProgress){
        $scope.setProgressBar();
      }
    }

    $scope.nextSection=function(){
      if($scope.sections.length>($scope.activeSectionIndex+1) ){
        $scope.activeSectionIndex+=1;
        $scope.activeSubsectionIndex=0;
        $scope.sections[$scope.activeSectionIndex].viewed=true;
      }
      $scope.scrollTop(0);
    }

    $scope.scrollTop=function(){
      //It appears when there are elements fixed to the window,
      //scrolling just the body does not work. Specifying both selectors works tho.
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }

    $scope.prevSection=function(){
      if($scope.activeSectionIndex-1<0){ return; }
      $scope.activeSectionIndex-=1;
      $scope.activeSubsectionIndex=Math.max(0,$scope.sections[$scope.activeSectionIndex].subsections.length-1);
      var currentSection=$scope.sections[$scope.activeSectionIndex],
          healthOrLifestyle=currentSection.name.match(/^(Health|Lifestyle)/),
          currentSubsection;
      if( healthOrLifestyle ){
        healthOrLifestyle=healthOrLifestyle[0];
        currentSubsection=$scope['scor'+healthOrLifestyle+'QuestionSets'][$scope.activeSubsectionIndex];
        var currentSubsectionIsConditional=!!currentSubsection.conditionalOnQuestion,
            age=moment().diff($scope.consumer.birth_or_trust_date,'years'),
            canShowCurrentSubsection=!currentSubsectionIsConditional ||
              !!$scope.scorHealthAnswers[currentSubsection.conditionalOnQuestion] ||
              (currentSubsection.headingText=='Last Physician Visit Questions' && age>=60);
        if(!canShowCurrentSubsection){
          $scope.prevSubsection();
        }
      }else if(
        currentSection.name=='Application' &&
        currentSection.subsections[$scope.activeSubsectionIndex]=='app_04_payment_info.html' &&
        $scope.payerSR()
      ){
        $scope.prevSubsection();
      }
      $scope.sections[$scope.activeSectionIndex].viewed=true;
      $scope.scrollTop(0);
    }

    $scope.setAnswerToRadioQuestion=function(qNum){
      var qNumSegmented=qNum.split('_'),
        prefixForQSet=qNumSegmented.slice(0,qNumSegmented.length-1).join('_');
      for(var key in $scope.scorHealthAnswers){
        if(key!=qNum && key.indexOf(prefixForQSet)==0 && key.length>prefixForQSet.length){
          $scope.scorHealthAnswers[key]=false;
        }
      }
      $scope.scorHealthAnswers[qNum]=true;
    }

    $scope.healthQuestionGroupIsValid=function(qG,errorsBlockNavigation){
      var qs=qG.questions||qG.questionGroup.questions,
        qGValid,
        qGSubsectionsValid;
      if(qG.conditionalOnQuestion && !$scope.scorHealthAnswers[qG.conditionalOnQuestion]){
        //This question Group is not visible, so needs no validation.
        qGValid=true;
        qGSubsectionsValid=true;
      }else{
        var affirmativeSelections=[];
        for(var i=0; i<qs.length; i++){
          var q=qs[i];
          if($scope.scorHealthAnswers[q.questionNumber]){
            affirmativeSelections.push(q.questionNumber);
            if(q.questionGroup && q.questionGroup.questions){
              //validate subsections with recursive call.
              var v=$scope.healthQuestionGroupIsValid(q.questionGroup,errorsBlockNavigation);
              if( ( (typeof qGSubsectionsValid)=='undefined' || qGSubsectionsValid==true) && v){
                qGSubsectionsValid=true;
              }else{
                qGSubsectionsValid=false;
              }
            }
          }
        }
        if(qG.validationStrategy=='DoNotRequireAnyAffirmativeSelection'){
          qGValid=true;
          for(var i=0; i<qs.length; i++){
            var q=qs[i];
            //When no affirmative selection is required,
            //all answers in the group are required to be explicitly true or false.
            $scope.scorHealthAnswers[q.questionNumber]=!!$scope.scorHealthAnswers[q.questionNumber];
          }
        }else if(qG.validationStrategy=='RequireExactlyOneSelection'){
          if(affirmativeSelections.length==0){
            qG.error="Selection required."
            if(errorsBlockNavigation){
              qG.isBlockingNavigation=true;
            }
            qGValid=false;
          }else if(affirmativeSelections.length==1){
            delete qG.error;
            delete qG.isBlockingNavigation;
            qGValid=true;
          }
          //These sections will use radios, so multiple selections are not possible.
        }else{//require one or more selections
          if(affirmativeSelections.length==0){
            qG.error="One or more selections required."
            if(errorsBlockNavigation){
              qG.isBlockingNavigation=true;
            }
            qGValid=false;
          }else if(affirmativeSelections.length>=1){
            delete qG.error;
            delete qG.isBlockingNavigation;
            qGValid=true;
          }
        }
      }
      return qGValid && (qGSubsectionsValid || (typeof qGSubsectionsValid)=='undefined');
    };


    //Identify and warn if obvious test name or email is provided in a non-test environment.
    $scope.testNameMatchPattern=RegExp('(test|example|[0-9])','i');
    $scope.testEmailMatchPattern=RegExp('(test|example)','i');

    $scope.identifyTestName=function(){
      return $scope.warnAboutTestName=!!(
        $scope.urlIsProduction &&
        !$scope.consumer.test_lead &&
        !$scope.consumer.id &&
        $scope.consumer.full_name &&
        $scope.consumer.full_name.match( $scope.testNameMatchPattern )
      );
    }
    $scope.identifyTestEmail=function(){
      return $scope.warnAboutTestEmail=!!(
        $scope.urlIsProduction &&
        !$scope.consumer.test_lead &&
        !$scope.consumer.id &&
        $scope.consumer.emails_attributes[0].value &&
        $scope.consumer.emails_attributes[0].value.match( $scope.testEmailMatchPattern )
      );
    }

    $scope.subsectionIsValid=function(errorsBlockNavigation){
      var currentSection=$scope.sections[$scope.activeSectionIndex],
          currentSubsection=currentSection.subsections[$scope.activeSubsectionIndex];
          isValid=true,
          healthOrLifestyle=currentSection.name.match(/^(Health|Lifestyle)/);

      if( healthOrLifestyle ){
        var currentSubsection=$scope['scor'+healthOrLifestyle[0]+'QuestionSets'][$scope.activeSubsectionIndex];
        isValid=$scope.healthQuestionGroupIsValid(currentSubsection,errorsBlockNavigation);
      }else{
        if(currentSection.name=='Basic Info'){
          var bII=$scope.formHolder.basicInsuredInfo,
              bQI=$scope.formHolder.basicQuotingInfo;
          isValid=(
                  bII && bII.$valid &&
                  bQI && bQI.$valid &&
                  $scope.consumer.birth_or_trust_date
                );
          $scope.touchErrorFields(errorsBlockNavigation,bII);
          $scope.touchErrorFields(errorsBlockNavigation,bQI);
        }else if(currentSection.name=='Application' && currentSubsection.match(/pi/) ){
          var form=$scope.formHolder.piInfo;
          $scope.validateDriversLicenseNumber();
          $scope.touchErrorFields(errorsBlockNavigation,form);
          isValid=form && form.$valid;
        }else if(currentSection.name=='Application' && currentSubsection.match(/stakeholders/) ){
          var form=$scope.formHolder.stakeholderRels,
              sRAs=$scope.policy.stakeholder_relationships_attributes,
              everySRIsBeneOrOwnerOrPayer=true,
              pBPTotal=$scope.benPercentageTotal(false),
              cBPTotal=$scope.benPercentageTotal(true),
              percentageTotalsAreSane=pBPTotal==100 && (cBPTotal==100 || cBPTotal==0);

          $scope.touchErrorFields(errorsBlockNavigation,form);

          for(var i in sRAs){
            if(!sRAs[i]._destroy && !sRAs[i].is_beneficiary && !sRAs[i].is_owner && !sRAs[i].is_payer){
              everySRIsBeneOrOwnerOrPayer=false;
              break;
            }
          }
          isValid=(form && form.$valid && percentageTotalsAreSane && everySRIsBeneOrOwnerOrPayer);
        }else if(currentSection.name=='Application' && currentSubsection.match(/payment_info/) ){
          var form=$scope.formHolder.paymentInfo;
          $scope.touchErrorFields(errorsBlockNavigation,form);
          isValid=(form && form.$valid && $scope.$rttPathScope.reviewedDisclosure1);
        }
        if(!isValid && errorsBlockNavigation){
          $scope.addBulletin({text:'Some fields on this page are missing or invalid.',klass:'danger'});
        }
      }
      return isValid;
    }

    $scope.validateDriversLicenseNumber=function(){
      if( !$scope.consumer.dl_state_id ){ return; }
      var stateAbbrev=$filter('whereKeyMatchesValue')( STATES, 'id', $scope.consumer.dl_state_id )[0].abbrev,
          validationObj=window.validateDriversLicenseNumber(stateAbbrev, $scope.consumer.dln, $scope.consumer),
          isValid=validationObj[0],
          errorMsg=validationObj[1]
          form=($scope.formHolder||{}).piInfo,//suppress errors when form has not yet rendered in DOM
          field=form.license;//angular uses the element's `name` attribute as a key within the form object.
      field.$setValidity('formatting', isValid);
      $scope.consumer.$dlnValidityError= isValid ? '' : errorMsg;
    }

    $scope.benPercentageTotal=function(contingent){
      var sR=$scope.policy.stakeholder_relationships_attributes,
          total=0;
      for(var i in sR){
        if(sR[i].is_beneficiary && !sR[i]._destroy && (!!sR[i].contingent)==(!!contingent) ){
          total+=sR[i].percentage;
        }
      }
      return total;
    }

    //This function marks invalid fields as touched, so they get the associated styles.
    $scope.touchErrorFields=function(errorsBlockNavigation,form){
      if(errorsBlockNavigation && !form.$valid){
        $.each(form.$error, function(errorName, controls){
          $.each(controls, function(idx,control) {
            control.$setDirty(); control.$setTouched();
          });
        });
      }
    }

    $scope.applyWithQuote=function(selectedQuote){
      var qda=$scope.qda;
      qda.face_amount=$scope.sliderVisibleBar.value;
      qda.modal_premium=$scope.getModalPremium(selectedQuote),
      qda.duration_id=$filter('whereKeyMatchesValue')( DURATIONS,'years', parseInt(selectedQuote.duration) )[0].id;
      qda.product_name=('Real Time Term '+selectedQuote.duration).replace(/\s+/,' ');
      $scope.nextSubsection();
    }

    $scope.openWelcomeModal=function(){
      $scope.openModalWithStubController('/rtt/welcome_modal.html');
    }

    $scope.openDisclosure1Modal=function(){
      $scope.openModalWithStubController('disclosure1_modal.html');
    }

    $scope.openDisclosure2Modal=function(){
      $scope.openModalWithStubController('disclosure2_modal.html');
    }

    $scope.openEftCheckImageModal=function(){
      $scope.openModalWithStubController('eft_check_image_modal.html');
    }

    $scope.openNeedHelpModal=function(){
      $scope.openModalWithStubController('/rtt/need_help_modal.html');
    }

    $scope.openModalWithStubController=function(templateUrl){
      $uibModal.open({
        templateUrl: templateUrl,
        controller : 'stubModalCtrl',
        scope      : $scope
      });
    }

    $scope.printElement=function (elementId) {
      var existingIframe=window.frames["print-frame"];
      if(!existingIframe){
        var iframe=document.createElement('iframe');
        iframe.width=0;
        iframe.height=0;
        iframe.frameborder=0;
        iframe.id='print-frame';
        document.body.appendChild(iframe);
      }
      window.frames["print-frame"].contentWindow.document.body.innerHTML=document.getElementById(elementId).innerHTML;
      window.frames["print-frame"].contentWindow.focus();
      window.frames["print-frame"].contentWindow.print();
    }

    $scope.printIframe=function(iframeId){
      window.frames[iframeId].contentWindow.focus();
      window.frames[iframeId].contentWindow.print();
    }

    $scope.initMotoristsQuotePath();
  }
])
.controller('stubModalCtrl', ['$scope', '$http', '$q', '$uibModal', '$uibModalInstance',
  function ($scope, $http, $q, $uibModal, $uibModalInstance) {
    //
  }
])
.controller('rttStakeholderRelationshipFormCtrl', function ($scope, $http) {
  $scope.init=function(){
    $scope.origStakeholderRelationship=($scope.stakeholderRelationship || $scope.newStakeholderRelationshipTemplate);
    $scope.primaryInsured=$scope.person;//preserve this value, before overriding it.
    $scope.setSDFormObj();
  }

  $scope.setSDFormObj=function(){
    if($scope.overwriteOrigSR){
      $scope.stakeholderRelationship=$scope.origStakeholderRelationship;
    }else{
      $scope.stakeholderRelationship=$.extend(true,{},$scope.origStakeholderRelationship);
    }
    $scope.person=$scope.stakeholderRelationship.stakeholder_attributes;
    $scope.contact=$scope.person;
  }

  $scope.init();
})
.controller("MotoristsStakeholderPathCtrl", [
  "$scope", "$rootScope", "$location", "$resource", '$http', '$timeout', '$interval', '$filter','$uibModal','bulletinService','requestSanitizer','RttSharedFunctionsModel',
  function ($scope, $rootScope, $location, $resource, $http, $timeout, $interval, $filter,$uibModal,bulletinService,requestSanitizer,RttSharedFunctionsModel) {
    $scope.initStakeholderPath=function(){
      //Alias for views to be able to set variables at the controller scope level:
      $scope.$rttPathScope=$scope;
      //Hook for developer/designer/testing team convenience:
      window.$scope=$scope.$rttPathScope;
      $scope.sections=[
        {name:'Verify Your Identity',     url:'stakeholder_verify_identity.html'},
        {name:'Payment Information',      url:'app_04_payment_info.html',             formName:'formHolder.paymentInfo'},
        {name:'Verify Accuracy And eSign',url:'app_05_verify_accuracy_and_esign.html'},
        {name:'Thank You',                url:'stakeholder_thank_you.html'}
      ];


      $scope.sharedFuncts = new RttSharedFunctionsModel($scope);
      $scope.sharedFuncts.getQueryData();

      $scope.policy   ={scor_guid:$scope.rttGuid};
      $scope.person   ={ id: $scope.personId }
      $scope.consumer =$scope.person;//This alias allows the payment info page to work without modification.

      $scope.reviewedTermsAndDisclosures=false;
      $scope.routingNumbersToBankNames=[];
      $scope.formHolder={};

      $scope.activeSectionIndex=0;
      $scope.progressBarIndex=0;
      $scope.progressSections=[];
      $scope.totalQuestions=$scope.sections.length;

      $scope.sharedFuncts.getAgentSummary();

      $scope.agentSummary.$promise.then(function(){
        $scope.person.$role=$scope.agentSummary.stakeholder_role;

        if( $scope.person.$role.match(/Payer/) ){
          $scope.sharedFuncts.getRoutingNumbersToBankNames();
        }else{
          $scope.sections.splice(1,1);//remove "Payment Information" section
        }
      })

    }

    $scope.nextSection=function(){
      if($scope.sections.length<=($scope.activeSectionIndex+1) ){ return; }
      $scope.activeSectionIndex+=1;
      $scope.progressBarIndex++;
      $scope.sections[$scope.activeSectionIndex].viewed=true;
      $scope.scrollTop(0);
    }

    $scope.prevSection=function(){
      if($scope.activeSectionIndex-1<0){ return; }
      $scope.activeSectionIndex-=1;
      $scope.progressBarIndex--;
      $scope.sections[$scope.activeSectionIndex].viewed=true;
      $scope.scrollTop(0);
    }

    $scope.getProgressPercent=function(){
      return Math.floor( ($scope.progressBarIndex/$scope.sections.length)*100 )
    }

    $scope.getTotalProgress=function(){
      var total = 0;
      for (var x=0;x<$scope.progressSections.length;x++)
      {
        total += $scope.progressSections[x].value;
      }
      return Math.min(100,total);
    }

    $scope.getStackedBarValueByIndex=function(index){
      return $scope.progressSections[index].value;
    }

    $scope.getMaxProgressValue=function(){
      return 100/$scope.progressSections.length;
    }


    $scope.setProgressBar=function(){
      var percent_per_bar=$scope.getMaxProgressValue();
    
      for(var z=0;z<$scope.progressSections.length;z++){
        $scope.progressSections[z].value=0;
      }

      $scope.labelText = '';
      var progressIndex = 0;
      var currentSection=$scope.sections[$scope.activeSectionIndex];

    }

    $scope.buildProgressBars=function(){
      for (var i = 1;i<$scope.sections.length;i++)
      {
        var currentSection=$scope.sections[i];
        $scope.progressSections.push({value:0,heading:currentSection.name});

      }
    }

    $scope.openDisclosure1Modal=function(){
      $scope.openModalWithStubController('disclosure1_modal.html');
    }

    $scope.openDisclosure2Modal=function(){
      $scope.openModalWithStubController('disclosure2_modal.html');
    }

    $scope.openEftCheckImageModal=function(){
      $scope.openModalWithStubController('eft_check_image_modal.html');
    }

    $scope.openNeedHelpModal=function(){
      $scope.openModalWithStubController('/rtt/need_help_modal.html');
    }

    $scope.openModalWithStubController=function(templateUrl){
      $uibModal.open({
        templateUrl: templateUrl,
        controller : 'stubModalCtrl',
        scope      : $scope
      });
    }

    $scope.subsectionIsValid=function(errorsBlockNavigation){
      var currentSection=$scope.sections[$scope.activeSectionIndex];
          isValid=true;

      if(currentSection.name=='Payment Information' ){
        isValid=($scope.formHolder.paymentInfo.$valid && !$scope.$rttPathScope.reviewedDisclosure1);
      }
      if(!isValid && errorsBlockNavigation){
        $scope.addBulletin({text:'Some fields on this page are missing or invalid.',klass:'danger'});
      }
      return isValid;
    }

    $scope.saveAcceptDecline=function(accepted){
      var url='/rtt/'+$scope.rttGuid+'/stakeholder_';
      url+= accepted ? 'sign' : 'decline';
      url+='.json';
      var transactionId=bulletinService.randomNonZeroId();
      $http.put(url,{
        agent_id:   $scope.agentId,
        key:        $scope.key,
        person_id:  $scope.personId,
        secure_token:$scope.secureToken,
      })
      .success(function(data, status, headers, config){
        $scope.nextSection();//next section is the thank you page.
      })
      .error(function(data, status, headers, config){
        var failureMsg='Failed to save. '+(data.error||data.errors);
        $scope.addBulletin({text:failureMsg, id:transactionId, klass:'danger'})
      });
    }

    $scope.initStakeholderPath();
  }
]);