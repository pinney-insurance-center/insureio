// This is a manifest file that'll be compiled into dependencies.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require bower_components/jquery/dist/jquery
//  * The next purge should definitely be all this jQueryUI.
//  * The functional parts of bootstrap depend on it (as described in http://getbootstrap.com/javascript/ ).
//  * So we would have to replace plain bootstrap accordions, modals, etc
//  * with their ui-bootstrap equivalents in order to remove jQueryUI.
//= require jquery.ui.all
//  * Order needs to be bootstrap-tooltip, bootstrap-popover, bootstrap-editable.
//= require javascripts/es6_object_assign_polyfill_for_ie
//= require jquery/bootstrap-tooltip
//= require jquery/bootstrap-popover
//= require jquery/bootstrap-editable
//= require jquery/jquery.iframe-transport
//= require bower_components/select2/select2.min
//= require bower_components/moment/min/moment.min
//= require bower_components/moment-timezone/builds/moment-timezone-with-data-2012-2022.min
//= require bower_components/tinymce/tinymce.min
//= require bower_components/tinymce/plugins/media/plugin.min
//= require bower_components/tinymce/plugins/image/plugin.min
//= require bower_components/tinymce/plugins/fullscreen/plugin.min
//= require bower_components/tinymce/plugins/preview/plugin.min
//= require bower_components/tinymce/plugins/code/plugin.min
//= require bower_components/tinymce/plugins/link/plugin.min
//= require bower_components/tinymce/themes/modern/theme.min
//  * Spectrum is a color picker.
//= require spectrum
//  * Eventually we should replace all plain bootstrap with ui-bootstrap
//  * so that we don't have to load redundant libraries.
//= require slimscroll
//= require bower_components/bootstrap/dist/js/bootstrap.min
//= require bower_components/angular/angular
//= require bower_components/angular-bootstrap/ui-bootstrap-tpls
//= require bower_components/angular-ui-bootstrap-datetimepicker/datetimepicker
//= require bower_components/angular-resource/angular-resource
//= require bower_components/angular-xeditable/dist/js/xeditable
//= require bower_components/angular-sanitize/angular-sanitize
//= require bower_components/angular-ui-select/dist/select.min
//= require bower_components/angular-ui-utils/mask.min
//= require bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min
//= require bower_components/angular-bootstrap-contextmenu/contextMenu
//= require bower_components/angular-ui-tinymce/dist/tinymce.min
//= require angular-file-upload.min
//= require image-resize-crop-canvas/component
//= require bower_components/easy-pie-chart/dist/angular.easypiechart.min
//= require bower_components/papaparse/papaparse.min
//= require bower_components/angularjs-slider/dist/rzslider.min
//  * angular-scroll,ez-ng,angular-hotkeys,hone,tether are all dependencies for `angular-ui-tour`.
//  * require bower_components/angular-scroll/angular-scroll
//  * require bower_components/ez-ng/dist/ez-ng
//  * require bower_components/angular-hotkeys/build/hotkeys
//  * require bower_components/hone/hone
//  * require bower_components/tether/dist/js/tether.min
//  * require bower_components/angular-ui-tour/dist/angular-ui-tour

// An ES7(2017) feature not natively supported in IE:
// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
}
// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      // 1. Let O be ? ToObject(this value).
      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        // c. Increase k by 1. 
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}
if (!String.prototype.includes) {
  String.prototype.includes = function(search, start) {
    'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }
    
    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}