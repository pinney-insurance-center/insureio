// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require insureio_ng
//= require_tree ./application

// This setting was moved here from `settings-jquery-ajax.js.coffee`.
// When the entire app is using AngularJS instead of jQuery, this can be removed.
$.ajaxSetup({
  dataType: 'script'
})
// END

$(document).ready(function() {
  $('#create-new-user').on('click', function(e){
    // validation code here
    if(!validate_signup()) {
      e.preventDefault();
    }
  });

  if ( $('#alert-box').length){
     $(".dr-tabs").css({"padding-top": '30px' }); 
  }

  $('button.close').on('click', function () {
     $(".dr-tabs").css({"padding-top": '0px' }); 
  })


  $('#signup-form input[class="required"], #user_phones_attributes_0_value').blur(function() {
    if($(this).val() != ''){
      $( this ).removeAttr('style');
    }
    else {
      $( this ).css( "border-color", 'red' );
    }
  });
});

function validate_signup() {
  var all_vaild = true
  $('#signup-form input[class="required"], #user_phones_attributes_0_value').each(function() {
    console.log($(this).attr('id')+ '  id  val = ' + $(this).val())
    if($(this).val() == '' || $(this).val() == undefined){
      $( this ).css( "border-color", 'red' );
      all_vaild = false
    }
    else {
      $( this ).removeAttr('style');
    }
  });
  return all_vaild;
}


function hide_role(drop_down_value) {
  if (drop_down_value == '') {
      $('#role').show();
      $('#auto-complete-template').hide();
  }
  else{
    $('#role').hide();
    $('#auto-complete-template').show();
  }
}

function fire_ajax(url, data, type)
{
  $.ajax({
    url: url,
    data: data,
    type: type
  });
}

$(document).on("click", "ul.dr-tabs li a", function(){
  var $this=$(this);
  $this.closest(".dr-tabs").find("li").removeClass("pending");
  $this.closest("li").addClass("pending");
});

function handleAjaxCompleteForTabs(containerSelector){
  var $container=$( containerSelector ),
      $tabBar;
  if( $container.find('ul.dr-tabs').length ){ $tabBar=$container.find('ul.dr-tabs') }else{ $tabBar=$container; }

  if ( $tabBar.find('.pending').length > 0 )
  {
    $tabBar.find("li").removeClass("active");
    $tabBar.find("li.pending").addClass("active").removeClass("pending");
  }
}

$(document).on("click", "a.disabled, a[disabled]", function(e){
  //Using this approach versus the css pointer-events property because we only want to disable the click action,
  //while preserving the hover action (which can include a title, explaining why the link is disabled).
  //This function is yet another backup, but cannot be relied upon, because we can't guaranteee it will run before rails's remote link callback.
  e.preventDefault();
});

if (typeof dR == "undefined"){
  window.dR = {};
}

/* start HELP VIDEOs JS */
$(document).on('click', '[data-video-src]', function(){
 var $helpModal =$('#help-video-modal'),
     $helpIframe=$helpModal.find('iframe'),
     $this      =$(this),
     videoSrc   =$this.attr("data-video-src")+"?autoplay=1",
     modalHeight=$this.attr("data-modal-height"),
     modalWidth =$this.attr("data-modal-width");

 $helpModal.css('height', modalHeight);
 $helpModal.css('width', modalWidth);
 $helpIframe.attr('src', videoSrc);
 $helpModal.find('button.close').click(function(){
   $helpIframe.attr('src', videoSrc);
 });
 $('.modal').click(function(){//add
   $helpIframe.attr('src', videoSrc);
 });
 $('#help-video-modal').on('hide',function(){
   $('.modal-body iframe').attr('src','');
 });
});
/* end HELP VIDEOs JS */