var insureioNg = angular.module('insureioNg', [
  //Dependencies
  "angularFileUpload",
  'ui.select',
  "ui.mask",
  "ui.bootstrap",
  'ui.bootstrap.datetimepicker',
  'ui.tinymce',
  'xeditable',
  'ngSanitize',
  //'bm.uiTour',
  //Global App Modules/Services/Etc.
  'detectChrome',
  'ioConstants',
  "ioRequestSanitizer",
  "DrModels",
  "DrFilters",
  "dr.directives",
  'DrBulletins',
  //App Modules
  "dr.controllers",
  'SocketClientApp',
  'ContactsMgmtApp',
  'UserHierarchyMgmtApp',
  "newQuoteApp",
  "WidgetApp",
  "drLeadDistribution",
  "dr.consumer",
  "dr.csvImport",
  "dr.marketing",
  "dr.users",
  "dr.brands",
  'DashboardApp',
  'noteApp',
  'ConsumerManagementApp',
  'PolicyMgmtApp',
  'SearchApp',
  'TasksApp',
  'tmpBulletin',
  'DrStatusTypeMgmtApp',
])
.constant("moment", moment)
.constant("DASHBOARD_WIDGETS", [
  {permission:'moderate_unassigned_tasks', label:'Unassigned Tasks',         src:'/dashboard/pending-tasks.html', url:'/dashboard/unassigned.json'},
  {permission:'dash_my_business_self',     label:'My Business',              src:'/dashboard/ezlife.html',        url:'/dashboard/ezl_cases.json'},
  {permission:'dash_opportunities_self',   label:'My Opportunities',         src:'/dashboard/opportunities.html', url:'/dashboard/opportunities.json'},
  {permission:'dash_pending_tasks_self',   label:'My Pending Tasks',         src:'/dashboard/pending-tasks.html', url:'/dashboard/tasks.json'},
  {permission:'dash_my_business_team',     label:'Team Business',            src:'/dashboard/ezlife.html',        url:'/dashboard/ezl_cases.json?team=1'},
  {permission:'dash_opportunities_team',   label:'Team Opportunities',       src:'/dashboard/opportunities.html', url:'/dashboard/opportunities.json?team=1'},
  {permission:'dash_pending_tasks_team',   label:'Team Pending Tasks',       src:'/dashboard/pending-tasks.html', url:'/dashboard/tasks.json?team=1'},
  {permission:'motorists_rtt_process_details',label:'RTT Business',          src:'/dashboard/rtt_business.html',  url:'/rtt/recent_apps.json?for_dashboard=true'},
])
.run(['$rootScope', 'UserModel', 'User', '$compile', '$q', '$http', '$timeout', '$filter', '$resource', '$location', '$uibModal', 'editableOptions', 'editableThemes', 'bulletinService',
  function ($rootScope, UserModel, User, $compile, $q, $http, $timeout, $filter, $resource, $location, $uibModal, editableOptions, editableThemes, bulletinService) {

  //Set options for xeditable.
  editableOptions.theme = 'bs3';
  //Overwrite submit and cancel button templates
  // editableThemes['bs3'].submitTpl ='\
  //   <button type="submit" class="btn btn-primary">\
  //     <i class="fa fa-save"></i>\
  //   </button>\
  // ';
  // editableThemes['bs3'].cancelTpl ='\
  //   <button type="button" class="btn btn-warning" ng-click="$form.$cancel()">\
  //     <i class="fa fa-times"></i>\
  //   </button>\
  // ';

	Object.defineProperties($rootScope, {
    // Set currentUser on $rootScope.
    // @return $rootScope.currentUser
    // @param reload is optional
    getCurrentUser: {
      value: function (reload) {
        if (!$rootScope.currentUser || reload) {
          $rootScope.currentUser = UserModel.get({id:0});
          $rootScope.currentUser.$promise.then(function(data){
            // A redirect gets treated as a success. Its data should be discarded
            // We don't simply assign $rootScope.currentUser to false b/c a lower scope
            // may have a reference to the original $rootScope.currentUser assignment.
            // Therefore, we must alter that Object and hold onto it.
            // Use $rootScope.isLoggedIn() to check whether a currentUser really exists.
            if (!$rootScope.currentUser.id){
              for (key in data){
                delete $rootScope[key];
              }
            }else{
              if($rootScope.currentUser.membership_id){//anything other than 0/undefined/null is truthy.
                $rootScope.getEditableIds();
              }
              $rootScope.templateCacheMonitor();
              $rootScope.getCurrentTabSet();
            }
          });
        }
        return $rootScope.currentUser;
      }
    },
    refreshCurrentUser: {
      value: function(){ return $rootScope.getCurrentUser(true); }
    },
    // @param email is a string holding an email address
    // @return boolean indicating whether @param email matches our regular expression for email addresses.
		isEmailValid: {
			value: function (email) {
				return email && email.match(/\S+@\S+\.\w+/);
			}
		},
    // @return boolean for whether current user exists and has an id
    isLoggedIn: {
      value: function () {
        return $rootScope.getCurrentUser() && !!$rootScope.getCurrentUser().id;
      }
    },
    isImpersonating: {
      value: function () {
        return $rootScope.getCurrentUser() &&
          $rootScope.getCurrentUser().$trueCurrentUserId!=$rootScope.getCurrentUser().id;
      }
    },
    impersonate: {
      value: function (id) {
        var url= !!id ? '/users/'+id+'/impersonate.json' : '/users/end_impersonate';
        $http.get(url)
        .success(function(data){
          //This will forward to the proper subdomain immediately.
          window.location.reload();
        });
      }
    },
    contactIconClass:{
      value: function(contact){
        switch(contact.person_type){
          case 'Consumer': return 'fa fa-folder-open';
          case 'Brand':    return 'fa fa-user';
          case 'User':     return 'fa fa-user-o';
          case 'Recruit':  return 'fa fa-black-tie';
        }
      }
    },
    searchContacts:{//populates `$rootScope.contactsForSearch`
      value: function(searchString){
        if(searchString.length < 3){ return; }//min length prevents overly broad queries.
        var paramsObj={
              search_term:searchString,
              search_by:'name_or_id',
              'select[]':['full_name', 'id', 'entity_type_id', 'company']
            };

        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text:'Loading agents.', id:transactionId});

        $rootScope.contactsForSearch=$resource('/contacts.json').query(paramsObj,
        function(data, status, headers, config){//success
          $rootScope.removeBulletinById(transactionId);
        },
        function(data, status, headers, config){//error
          $rootScope.addBulletin({text:'Failed to load agents. '+(data.errors||''), klass:'danger', id:transactionId});
        });
        return $rootScope.contactsForSearch;
      }
    },
    searchUsers:{//populates `$rootScope.usersForSearch`
      value: function(searchString,params){
        if(searchString.length < 3){ return; }//min length prevents overly broad queries.
        if(typeof(params)!='object'){ params={}; }
        if(!params['select[]']){ params['select[]']=['id','full_name','company']; }
        params.full_name=searchString;
        if(typeof(params.no_pagination)=='undefined'){ params.no_pagination=true; }//default to no pagination

        var transactionId=$rootScope.randomNonZeroId(),
            resourceMethod=params.no_pagination ? 'query' : 'get';
        $rootScope.addBulletin({text:'Loading agents.', id:transactionId});

        $rootScope.usersForSearch=$resource('/users.json')[resourceMethod](params,
          function(data, status, headers, config){//success
            $rootScope.removeBulletinById(transactionId);//just remove the loading message if successful.
          },
          function(data, status, headers, config){//error
            $rootScope.addBulletin({text:'Failed to load agents. '+(data.errors||''), klass:'danger', id:transactionId});
          }
        );
        return $rootScope.usersForSearch;
      }
    },
    getEditableIds:{
      value: function(){
        //Bypass the extra request for users with this permission.
        if($rootScope.currentUser.can('super_edit') ){ return; }

        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text:'Determining which users your account can edit.', id:transactionId});

        $rootScope.editableIds=User.query(//factory defined in `UserHierarchyMgmtApp`
          {actionName:'editable_user_ids'},
          function(ids){//success
            $rootScope.removeBulletinById(transactionId);
          },
          function(response){//error
            var msg='Failed to determine which users your account can edit.'
            if(response.data){
              msg+=response.data.msg||response.data.error||response.data.errors;
            }
            $rootScope.addBulletin({text:msg, klass:'danger', id:transactionId});
          }
        );
      }
    },
    canManageUser:{
      value: function(user){
        return $rootScope.currentUser.can('super_edit') ||
          user.is_editable;//is determined on the server as part of serialization
      }
    },
    // @return boolean for whether current user exists and has given permission
    // @throws error if given permission does not exist but current user does exist
    can: {
      value: function (permissionName, strict) {
        return $rootScope.isLoggedIn() && $rootScope.getCurrentUser().can(permissionName, null, strict);
      }
    },
    getCurrentTabSet:{
      value: function(forceRefresh){
        if($rootScope.activeSectionName && !forceRefresh){
          return $rootScope.SECTION_PAGES[$rootScope.activeSectionName]||[];
        }
        var currentPath=$location.$$url;
        for(var activeSectionName in $rootScope.SECTION_PAGES){
          var tabSet=$rootScope.SECTION_PAGES[activeSectionName];

          for(var i in tabSet){
            var tabName=tabSet[i][0],
                tabUrl  =tabSet[i][2];
            if( currentPath.indexOf(tabUrl)==0 ){/* url starts with specified path*/
              $rootScope.activeSectionName   =activeSectionName;
              $rootScope.activePageName=tabName;
              return tabSet;
            }
          }
        }
        return [];
      }
    },
    recUpdateSignal:{
      value: function(cacheTimeStamp=null){
        $rootScope.needsReload=true;
        $rootScope.templateCacheMonitor(parseInt(cacheTimeStamp),false);
      }
    },
    pageLastLoadedAt:{
      value: parseInt( Date.now().toString().slice(0,10) )
    },
    templateCacheMonitor:{
      // @see also: `recUpdateSignal`, `setUrl`, `getCurrentUser`,`pageLastLoadedAt`, and capistrano task `assets:update_template_version`
      // @param cacheTimeStamp (integer or null) allows passing a timestamp that may be more up to date than the page's meta tag
      // @param safeToReloadNow (boolean) specifies whether this context is safe to force a reload
      // (only gets set to true on async navigation between app sections)
      value: function(cacheTimeStamp=null,safeToReloadNow=false){
        var appLastUpdated = cacheTimeStamp || $rootScope.getCurrentUser().$templatesLastModified;
        $rootScope.needsReload= $rootScope.needsReload || ($rootScope.pageLastLoadedAt<appLastUpdated);

        if($rootScope.needsReload && safeToReloadNow){
          window.location.reload(true);
        }
      }
    },
    setUrl:{
      value: function(path){
        $('#main-container').html('<div class="loading"><div class="loading-things"></div></div>');
        //Ensure variables each have a string value before concatenation.
        $rootScope.activeSectionName='';
        $rootScope.activePageName='';
        $location.url(path);
        $rootScope.getCurrentTabSet(true);
        $rootScope.loadRailsView('#main-container',path,$rootScope.activePageName,$rootScope );
        $rootScope.templateCacheMonitor(null,true);
      }
    },
    setUrlToRootPath:{
      value: function(){
        $rootScope.setUrl('/');
      }
    },
    //used by 'layouts/application.js.erb' to allow jQuery ajax calls to modify the DOM in a way that AngularJS can still use.
    addToDom:{
      value: function(selector,markup,fn,skipApply,scope){
        if (!fn) fn = 'html';
        $(selector)[fn]($compile(markup)(scope||$rootScope));
        if(!scope){
          let warningMsg='You called `addToDom` without providing a scope.\n`$rootScope` will be used as a fallback.\n'+
            'Any non-global variables will not be available in the view that was loaded.';
          console.warn(warningMsg);
        }
        if(!skipApply) $rootScope.$apply();
      }
    },
    //Use this instead of jQuery Ajax and Rails Remote links.
    //It's a terrible but temporary solution until all ERB views can be angularized.
    loadRailsView:{
      value: function(selector,path,viewName,scope){
        if(!viewName){ viewName='view'}
        path= path.match(/\?/) ? path+'&no_layout=true' : path+'?no_layout=true';
        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text:'Loading '+viewName+'.', id:transactionId});
        $http({
          method:'GET',
          headers: {
            'Content-Type': 'text/html'
          },
          url:path
        })
        .success(function(data, status, headers, config){
          $rootScope.addToDom(selector,data,'html',true,scope);
          $rootScope.removeBulletinById(transactionId);
        })
        .error(function(data, status, headers, config){
          $rootScope.addBulletin({text:'Failed to load '+viewName+'. '+(data.errors||''), klass:'danger', id:transactionId});
        });
      }
    },
    toggle: {
      value: function (target, valueOrField, value) {
        var field;
        if (value == null) {
          value = valueOrField;
          field = '_toggled';
        }
        else
          field = valueOrField;
        if (typeof target === 'string')
          Object.defineProperty(this, target, {
            configurable:true, enumerable: false, value: (value == null ? !this[target] : value)
          });
        else
          Object.defineProperty(target, field, {
            configurable:true, enumerable: false, value: (value == null ? !target._toggled : value)
          });
      }
    },
    isToggled: {
      value: function (targetOrField, field) {
        if( (typeof targetOrField)=='string' ){
          return this[targetOrField];
        }else if( (typeof targetOrField)=='object' ){
          if( !field ){ field = '_toggled'; }
          return targetOrField[field];
        }
      }
    },
    // Return a dummy promise that's already resolved to success
    buildResolvedPromise: {
      value: function (value) {
        var deferred = $q.defer();
        deferred.resolve(value);
        return deferred.promise;
      },
    },
    buildNgResourceFailCallback: {
      value: function (fallbackMessage, chainedCallback) {
        return function (response) {
          var data = response.data;
          bulletinService.add({klass:'danger', text:(data.msg||fallbackMessage), data:response.data});
          if (chainedCallback) chainedCallback(response);
        }
      }
    },
    buildNgResourceSuccessCallback: {
      value: function (message, chainedCallback) {
        return function (data, headers) {
          bulletinService.add({klass:'success', text:message, data:data});
          if (chainedCallback) chainedCallback(data, headers);
        }
      }
    },
    DEFAULT_NG_RESOURCE_FAIL_CALLBACK: {
      value: function (response) {
        var message;
        try {
          var data = response.data;
          if (data.error) message = data.error;
          else if (data.errors) message = data.errors.join('. ');
          else if (data.msg) message = data.msg;
        } catch (ex) {
          message = response.statusText;
        }
        console.error('default ng resource fail cb', response);
        bulletinService.add({klass:'danger', text:message, data:data});
      },
      writable: false,
      configurable: false,
    },
    getEnum:{/* DEPRECATED. */
      value: function(enumObjName){
        console.warn('Deprecated function `getEnum` was called with args '+enumObjName+'. Please use `$rootScope.'+enumObjName+'` instead.');
        return $rootScope[enumObjName];
      }
    },
    /* !!! DEPRECATED !!! Please use filter `enumValue` */
    findEnumItem:{
      value: function(enumObjName,id){
        console.warn('Deprecated function `findEnumItem` was called with args '+enumObjName+','+id+'. Please use filter `enumValue` instead.');
        var enumObj=$rootScope[enumObjName];//because for whatever reason, outside variables aren't accessible within a directive
        if(!enumObj || !(enumObj instanceof Array) || id==null ) return null;
        var item=$.grep(enumObj,function(x){ return x.id==id || x.value==id; });
        return item[0];
      }
    },
    destroyRecord:{
      value: function(indexOrKey,parentObject,objectUrl,objectName,successFn){
        if(!confirm('Are you sure you want to permanently delete this '+objectName+'?')){return;}
        var transactionId =$rootScope.randomNonZeroId();

        $rootScope.addBulletin({text:'Deleting '+objectName+'.', id:transactionId});

        return $http.delete(objectUrl)
        .success(function(data, status, headers, config){
          if( Array.isArray(parentObject) ){
            parentObject.splice(indexOrKey,1);
          }else{
            delete parentObject[indexOrKey];
          }
          $rootScope.addBulletin({text:'Successfully deleted '+objectName+'.', klass:'success', id:transactionId});
          if( typeof(successFn)=='function' ){ successFn(); }
        })
        .error(function(data, status, headers, config){
          $rootScope.addBulletin({text:'Failed to delete '+objectName+'. '+(data.errors||[]).join('.\n'), klass:'danger', id:transactionId});
        });
      }
    },
    createRecord:{
      value: function(object,indexOrKey,parentObject,objectUrl,objectName,successFn){
        var transactionId =$rootScope.randomNonZeroId();

        $rootScope.addBulletin({text:'Creating '+objectName+'.', id:transactionId});

        return $http.post(objectUrl,object)
        .success(function(data, status, headers, config){
          var createdObject;
          if(data.object){
            createdObject=data.object;
          }else{
            createdObject=data;
          }

          if( Array.isArray(parentObject) && indexOrKey!=null ){
            parentObject.splice(indexOrKey,0,createdObject);
          }else if( Array.isArray(parentObject) ){
            parentObject.push(createdObject);
          }else{
            parentObject[indexOrKey]=createdObject;
          }
          if( typeof(successFn)=='function' ) successFn();
          $rootScope.addBulletin({text:'Successfully created '+objectName+'.', klass:'success', id:transactionId});
        })
        .error(function(data, status, headers, config){
          $rootScope.addBulletin({text:'Failed to create '+objectName+'.\n'+formatErrors(data), klass:'danger', id:transactionId});
        });
      }
    },
    //This method is intended to be used by angular-xeditable fields, called via the onbeforesave attribute.
    //The xeditable plugin updates the model upon success.
    updateRecordSingleField:{
      value: function(objectUrl,humanObjectName,realFieldName,newValue,successFn,failFn){
        var transactionId =$rootScope.randomNonZeroId(),
            humanFieldName=realFieldName.replace(/_id$/,'').replace(/_/g,' ');

        $rootScope.addBulletin({text:'Updating '+humanObjectName+' '+humanFieldName+'.', id:transactionId});

        return $http.put(objectUrl,{name:realFieldName,value:newValue})
        .success(function(data, status, headers, config){
          if( (typeof successFn)=='function' ) successFn(data);
          $rootScope.addBulletin({text:'Successfully updated '+humanObjectName+' '+humanFieldName+'.', klass:'success', id:transactionId});
          //If another instance of the `currentUser` object is modified make sure the global copy is reloaded.
          //In future this extra request may be avoided through a refactor of views that modify the `currentUser`.
          var urlMatches=objectUrl.match(/^\/users\/(\d+)\.json/),
              userId=!!urlMatches ? parseInt(urlMatches[1]) : null;
          if( userId==$rootScope.currentUser.id ){
            $rootScope.getCurrentUser(true);
          }
        })
        .error(function(data, status, headers, config){
          if( (typeof failFn)=='function' ) failFn(data);
          var errorMsg=(data && data.errors || data.error) ||'';
          $rootScope.addBulletin({text:'Failed to update '+humanObjectName+' '+humanFieldName+'. '+errorMsg, klass:'danger', id:transactionId});
        });
      }
    },
    setPointersToPrimaryContactMethods:{
      value: function(personObj){
        if(personObj.phones){
          personObj.$primaryPhone        =$filter('whereKeyMatchesValue')( personObj.phones,   'id', personObj.primary_phone_id )[0]   || personObj.phones[0];
        }
        if(personObj.emails){
          personObj.$primaryEmail        =$filter('whereKeyMatchesValue')( personObj.emails,   'id', personObj.primary_email_id )[0]   || personObj.emails[0];
        }
        if(personObj.addresses){
          personObj.$primaryAddress      =$filter('whereKeyMatchesValue')( personObj.addresses,'id', personObj.primary_address_id )[0] || personObj.addresses[0];
          personObj.$homeOrPrimaryAddress=$filter('whereKeyMatchesValue')( personObj.addresses,'address_type_id', 1 )[0] ||
                                          $filter('whereKeyMatchesValue')( personObj.addresses,'id', personObj.primary_address_id )[0];
        }
        if(personObj.webs){
          personObj.$website             =$filter('whereKeyMatchesValue')( personObj.webs,     'web_type_id', 1 )[0];
        }

      }
    },
    getTemplateOptions:{
      value: function (params) {
        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text: 'Loading templates.', id: transactionId});
        if(!params){
          var params={};
        }
        var r=$resource('/marketing/templates.json');
        if(params.paginate){
          $rootScope.templateOptions=r.get(params,
            function(){ $rootScope.removeBulletinById(transactionId); }
          );
        }else{
          params['select[]']=['id', 'name'];
          $rootScope.templateOptions=r.query(params,
            function(){ $rootScope.removeBulletinById(transactionId); }
          );
        }
        return $rootScope.templateOptions;
      }
    },
    getSubscriptions:{
      /*This method is defined here in `$rootScope` because it is used by both
        the marketing view and the task sidebars. */
      value: function(personId, personType, $scope, reload){
        if($scope.campaigns && $scope.subscriptions && !reload){ return; }

        var transactionId=$rootScope.randomNonZeroId();
        $rootScope.addBulletin({text: 'Loading campaign subscriptions.', id: transactionId});
        $scope.subscriptionsAndCampaignNames=$resource('/marketing/subscriptions.json').get(
          //returns an object with campaign names and subscriptions
          {person_id: personId, person_type: personType.replace('Recruit','User')},
          function (data, status, headers, config) {
            $scope.campaigns = data.campaignNames;
            $scope.subscriptions=data.subscriptions;
            $scope.groupedSubscriptions=[];
            var nextQueueNumber=1;
            //Maintain the flat list for use in task views,
            //and nested structure for marketing view.
            for(var i in data.subscriptions){
              var subscription=data.subscriptions[i],
                  queue=subscription.queue_num;
              if(!$scope.groupedSubscriptions[queue]){ $scope.groupedSubscriptions[queue]=[]; }
              $scope.groupedSubscriptions[queue].push(subscription);
              if(subscription.queue_num>=nextQueueNumber){
                nextQueueNumber=subscription.queue_num+1;
              }
            }
            $scope.nextQueueNumber=nextQueueNumber;
            $rootScope.removeBulletinById(transactionId);
          },
          function (data, status, headers, config) {
            $rootScope.addBulletin({text: 'Failed to load campaign subscriptions. ' + (data.errors || ''), klass: 'danger', id: transactionId});
          }
        );
        return $scope.subscriptions;
      }
    },
    //Force both lists to refresh the next time they are needed.
    //Is called after changing one's `exclude_global_status_types` setting.
    clearAllStatusTypes:{
      value: function(){
        delete $rootScope.userStatusTypes;
        delete $rootScope.policyStatusTypes;
      }
    },
    getPolicyStatusTypes:{
      value: function(forceRefresh,params){//Sets or refreshes `$rootScope.policyStatusTypes`.
        $rootScope.getStatusTypes(false,forceRefresh,params);
      }
    },
    getUserStatusTypes:{//Sets or refreshes `$rootScope.userStatusTypes`.
      value: function(forceRefresh,params){
        $rootScope.getStatusTypes(true,forceRefresh,params);
      }
    },
    /*
    Should only be accessed via `getPolicyStatusTypes`, or `getUserStatusTypes`
    or within `DrStatusTypeMgmtApp` for clarity of intent.
    Sets or refreshes either `$rootScope.userStatusTypes` or `$rootScope.policyStatusTypes`.
    */
    getStatusTypes:{
      value: function(forUsers,forceRefresh,params){
        var stKey=forUsers ? 'userStatusTypes' : 'policyStatusTypes';
        if($rootScope[stKey] && $rootScope[stKey].length && !forceRefresh && !params){
          return $rootScope[stKey];
        }
        if(!params){ params={}; }
        params.include_owner_name=true;
        if(forUsers){ params.for_users=true; }
        var transactionId = $rootScope.randomNonZeroId();

        $rootScope.addBulletin({text:'Loading status types.', id:transactionId});
        $rootScope[stKey]=$resource('/crm/status_types.json').query( params,
        function(data, status, headers, config){//success
          var structure={null:[]},
            cats=$filter('whereKeyMatchesValue')($rootScope.STATUS_CATEGORIES,'is_for_policies', !forUsers);
          //Set key `$groupName` for grouping in menus.
          for(var i in $rootScope[stKey]){
            var st=$rootScope[stKey][i];
               salesStage=$filter('whereKeyMatchesValue')($rootScope.SALES_STAGES,'id', st.sales_stage_id)[0],
               salesStageName=(salesStage ? salesStage.name : 'No Stage'),
               category=$filter('whereKeyMatchesValue')(cats,'id', st.status_type_category_id)[0],
               categoryName=(category ? category.name: 'No Category'),
               groupName=salesStageName+' - '+categoryName;
            st.$groupName=groupName;
          }
         $rootScope.removeBulletinById(transactionId);
        },
        function(data, status, headers, config){//error
          $rootScope.addBulletin({text:'Failed to load status types. '+(data.errors||''), klass:'danger', id:transactionId});
        });
        return $rootScope[stKey];
      }
    },
    statusTypeGroupName:{
      value:function(item){
        return item.$groupName;
      }
    },
    openSmsModal:{
      value: function(person,phone,scope){
        $uibModal.open({
          resolve: { person: function () { return person }, phone: function () { return phone } },
          templateUrl: '/contacts/modal_for_sms.html',
          controller : 'smsModalCtrl',//defined in dr-ng-controllers.js
          scope      : scope||$rootScope
        });
      }
    },
    openNewTaskModal:{
      // Called from $rootScope (lightning menu), Dashboard.contextMenu, User.PersonalCtrl, ConsumerManagementCtrl, PolicyCtrl.
      value:function(person,$scope){
        if( (typeof $scope)=='undefined' ){
          $scope=angular.element($('#main-container [ng-controller]')[0]).scope();
        }
        var p=$scope.person||person||{},
            pId=p.id,
            pType=p.person_type||p.$personType,
            pName=p.full_name;
        $uibModal.open({
          scope      : $scope,
          templateUrl: '/crm/cases/modal_for_new_task.html',
          controller : 'newTaskModalCtrl',
          resolve: {
            pId:    function () { return pId },
            pType:  function () { return pType },
            pName:  function () { return pName },
          }
        });
      }
    },
    refreshCurrentTasks:{
      value:function(){
        //broadcasts an event which is caught in Consumer.TasksCtrl
        $rootScope.$broadcast('refreshCurrentTasks');
      }
    },
  });

  function formatErrors (responseData) {
    if (responseData.error)
      return error;
    else if (responseData.errors) {
      if (Array.isArray(responseData.errors))
        return responseData.errors.join('\n');
      else {
        var strings = [];
        for (var key in responseData.errors) {
          var value = responseData.errors[key];
          if (Array.isArray(value))
            for (var i in value)
              strings.push(key + ' ' + value[i]);
          else
            strings.push(key + ' ' + value);
        }
        return strings.join('\n');
      }
    }
  }

  function onInitialCallToGetCurrentUser(){
    if( $rootScope.isLoggedIn() ){
      socketFn.loadSocketScript();
    }

    $timeout(function(){//after 250ms, show any rails flash messages
      if(window.highlightFlashErrorWithAJsDialog && window.railsFlashMsgs.error){
        alert(window.railsFlashMsgs.error);
      }
      if(window.railsFlashMsgs){
        for(var key in window.railsFlashMsgs){
          var msg=window.railsFlashMsgs[key],
              cssClassOpts={notice:'success',alert:'warning',error:'danger'}
              cssClass=cssClassOpts[key];
          $rootScope.addBulletin({text: msg, klass: cssClass});
        }
      }
    },250);
  }

  $rootScope.getCurrentUser().$promise.then(onInitialCallToGetCurrentUser);

  $rootScope.window=window;//So global variables (mostly constants) can be set on the window object and easily used inside and outside of AngularJS.

  //Massage JSON responses into the proper format.
  window.convertInputs=$rootScope.convertInputs=function(input) {
    if( (typeof input)!='object' || input==null ){ return; }
    for(let i in Object.keys(input) ){
      let key=Object.keys(input)[i];
      if( key.match(/^\$/) ){
        continue;
      }else if (typeof input[key] === "object"){
        $rootScope.convertInputs(input[key]);
      }else if (typeof input[key] === "string" && input[key].length){
        if( /^\d{4}-\d{2}-\d{2}$/.test(input[key]) ){
          //Dates and datetimes must be in exactly the same format they'll be displayed in,
          //or else ui-mask plugin will mangle them.
          input[key]=moment(input[key]).format('MM/DD/YYYY');
        }else if( /^\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}(-\d{2}:\d{2}|Z)$/.test(input[key]) ){
          //See above comment, re: ui-mask.
          input[key]=moment(input[key]).format('MM/DD/YYYY hh:mm A');
        }
      }
    }
  }

  /*
  This listener works in conjunction with `setUrl`.
  It fires when the user hits the back button, causing a page refresh.
  This behavior may be better handled with `ngRoute`,
  but I am postponing investigation into that until after
  this is converted to a fully AngularJS app (no lingering jQuery and ERB).
  */
  $(window).bind("popstate",function(e){
      location.reload();
  });

}])
.config(['$uibTooltipProvider', function($uibTooltipProvider){
  $uibTooltipProvider.options({
    placement: 'top',
    popupDelay: 500,/*units are 1/1000 seconds*/
    appendToBody: true,
  });
}]);
;

angular.module( 'RestfulResource', [ 'ngResource' ] )
.factory( 'Resource', [ '$resource', function( $resource ) {
    return function( url, params, methods ) {
    var defaults = {
       update: { method: 'put', isArray: false },
       create: { method: 'post' }
    };

  	methods = angular.extend( defaults, methods );

    var resource = $resource( url, params, methods );

    resource.prototype.$save = function(success, error) {
       if ( !this.id ) {
         return this.$create(success, error);
       }
       else {
         return this.$update(success, error);
       }
     };

     return resource;
   };
 }])
;

//To be used anywhere that non-AngularJS code needs to hook into AngularJS
$(function(){window.$rootScope=angular.element($('body')).scope()});

