// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED.
//
//= require bower_components/jquery/dist/jquery
//  * The next purge should definitely be all this jQueryUI.
//  * The functional parts of bootstrap depend on it (as described in http://getbootstrap.com/javascript/ ).
//  * So we would have to replace plain bootstrap accordions, modals, etc
//  * with their ui-bootstrap equivalents in order to remove jQueryUI.
//= require jquery.ui.all
//  * Order needs to be bootstrap-tooltip, bootstrap-popover, bootstrap-editable.
//= require jquery/bootstrap-tooltip
//= require jquery/bootstrap-popover
//= require jquery/bootstrap-editable
//= require bower_components/select2/select2.min
//= require bower_components/moment/min/moment.min
//  * Eventually we should replace all plain bootstrap with ui-bootstrap
//  * so that we don't have to load redundant libraries.
//= require bower_components/bootstrap/dist/js/bootstrap.min
//= require bower_components/angular/angular
//= require bower_components/angular-bootstrap/ui-bootstrap-tpls
//= require bower_components/angular-ui-bootstrap-datetimepicker/datetimepicker
//= require bower_components/angular-resource/angular-resource
//= require bower_components/angular-xeditable/dist/js/xeditable
//= require bower_components/angular-sanitize/angular-sanitize
//= require bower_components/angular-ui-select/dist/select.min
//= require bower_components/angular-ui-utils/mask.min
//= require bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min
//= require bower_components/angular-bootstrap-contextmenu/contextMenu
//= require bower_components/easy-pie-chart/dist/angular.easypiechart.min
//  * angular-scroll,ez-ng,angular-hotkeys,hone,tether are all dependencies for `angular-ui-tour`.
//= require bower_components/angular-scroll/angular-scroll
//= require bower_components/ez-ng/dist/ez-ng
//= require bower_components/angular-hotkeys/build/hotkeys
//= require bower_components/hone/hone
//= require bower_components/tether/dist/js/tether.min
//= require bower_components/angular-ui-tour/dist/angular-ui-tour
//= require application/detect_chrome
//= require application/DrBulletins-ng

//Dynamically load the correct tenant stylesheet.
var tenant=location.hostname.match(/(\w*)/)[0];
if(tenant!='pinney' && tenant!='app'){
  var cssTag = document.createElement('link'),
      head = document.head || document.getElementsByTagName('head')[0];
  cssTag.type = 'text/css';
  cssTag.rel = 'stylesheet';
  cssTag.href = '/tenants/'+tenant+'/stylesheets/branding-'+tenant+'.css';
  $().ready(function(){
    $('.logo-wrapper img').attr('src','/tenants/'+tenant+'/img/'+tenant+'-logo.png');
  });
  head.appendChild(cssTag);
}

var insureioNgMinimal = angular.module('insureioNgMinimal', [
  //Dependencies
  'ui.select',
  "ui.mask",
  "ui.bootstrap",
  'xeditable',
  'ngSanitize',
  //App Modules
  'detectChrome',
  'DrBulletins',
])
.config(['$locationProvider',
  function($locationProvider) {
      //Allows us to use standard url params (`/some/path?key=value`)
      //vs. Angular style (`/some/path#?key=value`).
      $locationProvider.html5Mode({
        enabled:true,
        rewriteLinks : false
      });
    }
])
.run(['$rootScope', '$compile', '$q', '$http', '$filter', 'editableOptions', 'editableThemes',
  function ($rootScope, $compile, $q, $http, $filter, editableOptions, editableThemes) {

    //Set options for xeditable.
    editableOptions.theme = 'default';
    //Overwrite submit and cancel button templates
    editableThemes['default'].submitTpl ='\
      <button type="submit" class="btn btn-primary">\
        <i class="fa fa-save"></i>\
      </button>\
    ';
    editableThemes['default'].cancelTpl ='\
      <button type="button" class="btn btn-warning" ng-click="$form.$cancel()">\
        <i class="fa fa-times"></i>\
      </button>\
    ';

    $rootScope.currentYear=moment().year()
    window.$rootScope=$rootScope;
  }
]);


//Login Form Validation
insureioNgMinimal.controller('formValidationCtrl', ["$scope", "$http", "$location", 'bulletinService', function($scope, $http, $location, bulletinService) {
  //Redirect immediately to root path (which will redirect to a correct subdomain if needed) if user is already logged in.
  //This must be initiated on the client side, since the page is static.
  $http.get('/users/0.json').then(
  function(requestObj){//success
    let data=requestObj.data;
    if(data && data.id){
      window.location='/';
    }
  });

  const err = $location.search().error;
  if (err) bulletinService.add({klass: 'danger', text: err});
  
  $scope.formData = {};
  $scope.submitForm = function(isValid) {
    $scope.submitAttempted = true;

    if (isValid) {
      $http({
        method  : 'POST',
        url     : '/user_sessions',
        data    : { usage_user_session:{login: $scope.userForm.username.$modelValue, password: $scope.userForm.password.$modelValue}},
        headers : { 'Content-Type': 'application/json' }  // set the headers so angular passing info as form data (not request payload)
      }).
      success(
        function(data, status, headers, config) {
          if (/localhost/.test(data.redirect))
            window.location = '/';
          else
            window.location = data.redirect || '/';
        }).
      error(
        function(data, status, headers, config) {
          bulletinService.add({text:(data||{}).error||'', klass:'danger', id:'/user_sessions'});
        });
    }
    else {
      return(false);
    }
  };
}]);