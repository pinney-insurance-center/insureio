class ContactBelongsTosController < ApplicationController
  before_filter :require_login

  respond_to :json

  WRAPPERS = %w[phone email email_address address web website].freeze

  before_filter :require_editable_person
  before_filter :remove_timestamps, only: :update

  def create
    @record = build_record
    @record.contactable_type ||= params[:person_type]
    @record.contactable_id ||= params[:person_id]
    if @record.save
      respond_with @record
    else
      respond_with @record
    end
  end

  def update
    attrs= params[self.class::MODEL_NAME].present? ? params[self.class::MODEL_NAME] : {}
    attrs[ params[:name] ]=params[:value] if params.has_key?('name') && params.has_key?('value')
    if get_record && attrs.present?
      if get_record.update_attributes(attrs)
        canned_success_response
      else
        canned_fail_response get_record
      end
    else
      canned_fail_response
    end
  end

  def destroy
    #we use destroy here to ensure important callbacks are executed
    get_record.destroy
    respond_to do |format|
      format.json {render json:get_record}
      format.js {render nothing: true}
    end
  end

  protected

  def build_record
    instance = self.class::MODEL.new
    Array(self.class::MODEL_NAME).each do |wrapper|
      instance.attributes = params[wrapper]
    end
    instance
  end

  def get_record
    @record ||= self.class::MODEL.find(params[:id]) if params[:id].present?
  end

  def remove_timestamps
    WRAPPERS.each do |wrapper|
      if params[wrapper].is_a? Hash
        params[wrapper].delete :created_at
        params[wrapper].delete :updated_at
      end
    end
  end

  def require_editable_person
    @person=
    if params[:person_id].present? && params[:person_type].present?
      params[:person_type].sub('Recruit','User').capitalize.constantize.find_by_id( params[:person_id] )
    else
     get_record
     @record.try(:contactable)
    end

    if !@person
      return permission_denied("No person record found", status: 404)
    elsif !@person.editable?(current_user)
      return permission_denied("You do not have permission to edit #{@person.person_type} #{@person.id}")
    else
      @person
    end
  end

end
