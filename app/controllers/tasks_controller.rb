class TasksController < ApplicationController

  before_filter :require_login

  respond_to :json

  def index
    # Require that a person is supplied and editable by current user
    return permission_denied('Both person id and person type are needed to load tasks.') unless params[:person_type] && params[:person_id]
    person=params[:person_type].constantize.find(params[:person_id])
    return permission_denied() unless person.editable?(current_user)
    @tasks = person.tasks
    @tasks = @tasks.where(active:params[:active]) unless params[:active].nil?
    render json: @tasks.as_json
  end

  def show
    render json: Task.find(params[:id])
  end


  def create
    @task = Task.new(params[:crm_task])
    return permission_denied unless @task.person.try(:editable?, current_user)
    @task.created_by_id = current_user.id
    @task.sequenceable = @task.origin.try(:statusable) if @task.sequenceable_id.nil?
    @task.save
    respond_with @task
  end

  def update
    @task = Task.find params[:id]
    return permission_denied unless @task.person.try(:editable?, current_user)
    attrs = params['crm_task'] || params['editable']
    attrs ||= {params[:name] => params[:value]}
    attrs.select!{|key, val| @task.respond_to?("#{key}=") }
    attrs.delete "lock_version"
    @task.attributes = attrs
    if @task.completed_at_changed?
      @task.completed_by = @task.completed_at.nil? ? nil : current_user
    end
    if @task.save
      respond_with @task
    else
      canned_fail_response @task
    end
  rescue ActiveRecord::StaleObjectError => ex
    respond_to do |format|
     format.js { flash.now[:error]='Edit conflicts with newer edit. Please try again.';render nothing:true, layout:true, status:422 }
      format.json {render json:{error:ex.to_json}}
    end
  end

  #Lists names of follow ups. Used in dashboard "Follow Ups" column to filter tasks.
  def follow_up_names
    if params[:q].present?
      user_ids = [current_user.id]
      user_ids += current_user.descendant_ids if current_user.can?(:have_children)
      names = Task.where('label LIKE ?',"%#{params[:q]}%").where(assigned_to_id:user_ids).select('label').map(&:label).uniq
      render(json: names.to_json)
    end
  end

  def inactive_task
   if params[:id].present?
     @task = Task.find(params[:id]).update_attributes(active: 0)
     canned_success_response
   end
  end

private

  def _sanitize_sort_column
    join_condition, sort_column=case params[:sort_column]
    when "status_type"  then [nil, "tasks.status_type_name"]
    when "created"      then [nil, "tasks.created_at"]
    when "due"          then [nil, "tasks.due_at"]
    when "follow_up"    then [nil, "tasks.label"]
    when "agent_name"   then [nil, "users.full_name"]
    else#sort by person name
      already_joins_person= params[:resource]=='unassigned'
      [ %Q(
          #{already_joins_person ? '#' : ''}LEFT JOIN users     AS persons ON tasks.person_type = "User"     AND usage_users.id =tasks.person_id
          #{already_joins_person ? '#' : ''}LEFT JOIN consumers AS persons ON tasks.person_type = "Consumer" AND consumers.id   =tasks.person_id
        ),
       "persons.full_name"]
    end
    #time zone is not in this switch because i really don't know how to deal with it. may revisit later if sorting by time zone is required
    return join_condition, sort_column
  end

end
