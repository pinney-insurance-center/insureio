class DashboardController < ApplicationController
  require 'will_paginate/array'

  before_filter :require_login
  before_filter -> { require_permission :moderate_unassigned_tasks }, only: :unassigned
  before_filter -> { require_permission :super_view }, only: [
                :background_jobs,
                :view_slow_queries,
                :kill_slow_queries,
                :view_recent_server_logs,
                :view_recent_request_stats,
                :view_recent_automated_job_stats
              ]
  before_filter :get_user_ids, only: [:tasks, :opportunities, :unassigned]

  DEFAULT_PER_PAGE = 15
  respond_to :html, :json

  def index
    return render 'public/dashboard/container.html', layout:!params[:no_layout]
  end

  #This action is intended as a quick reference for admins and devs.
  #In depth analysis still requires the console until we can update to the newest version with Web UI.
  #The full Sidekiq API is documented at https://github.com/mperham/sidekiq/wiki/API#stats
  def background_jobs
    stats=Sidekiq::Stats.new
    stats_this_week=Sidekiq::Stats::History.new(7)
    workers=Sidekiq::Workers.new#live updating, so count and iteration may not match
    schedule_set=Sidekiq::ScheduledSet.new
    grouped_schedule_set=schedule_set.group_by{|j| j['queue'] }
    grouped_schedule_set.each{|k,v| grouped_schedule_set[k]=v.length }
    outstanding_tasks=Task.handled_by_system.active.where(suspended:false, failed_attempts:0, is_executing:false)
    outstanding_tasks_count =outstanding_tasks.count
    past_due_tasks_count    =outstanding_tasks.due.count
    output=HashWithIndifferentAccess.new({
        processed_since_last_reset:stats.processed,
        failed_since_last_reset:stats.failed,
        processed_this_week:stats_this_week.processed,
        failed_this_week:stats_this_week.failed,
        queued:stats.queues,
        scheduled: grouped_schedule_set,
        open_system_tasks_in_db: outstanding_tasks_count,
        open_past_due_system_tasks_in_db: past_due_tasks_count,
        number_of_worker_currently_running: workers.size,
      })
    if params[:detailed]
      stats.queues.each do |q_name,count|
        next unless count>0
        output['jobs_in_'+q_name+'_queue']=[]
        jobs=Sidekiq::Queue.new(q_name)
        jobs.each do |j|
          h=j.item.as_json
          h['enqueued_at']=DateTime.strptime(h['enqueued_at'].to_s,'%s')
          output['jobs_in_'+q_name+'_queue'] << h
        end
      end
      output['workers_currently_running']=[]
      output['number_of_worker_currently_running']={}
      workers.entries.each do |w|
        w_output=w[1]['payload']
        w_output['queue']=w[1]['queue']
        w_output['process_id']=w[0]
        w_output['timestamp']=w[2]
        enqueued_at=DateTime.strptime( w_output['enqueued_at'].to_s, '%s') rescue nil
        w_output['enqueued_at']=enqueued_at||w_output['enqueued_at']
        run_at=Date.strptime( w[1]['run_at'].to_s, '%s') rescue nil
        w_output['run_at']=run_at||w_output['run_at']
        output['workers_currently_running'] << w_output

        output['number_of_worker_currently_running'][ w[1]['queue'] ]||=0
        output['number_of_worker_currently_running'][ w[1]['queue'] ]+=1
      end
    end
    render json: output
  end

  def view_slow_queries
    seconds= params[:seconds].present? ? params[:seconds].to_i : 200
    queries=ActiveRecord::Base.connection.execute("SELECT ID, HOST, COMMAND, TIME, STATE, INFO FROM INFORMATION_SCHEMA.PROCESSLIST WHERE TIME>#{seconds};").to_a
    keys=[:id, :host, :command, :time_in_seconds, :current_state, :query, :progress]
    queries.map!{|q| keys.zip(q) }
    queries=Hash.new queries
    render json: queries
  end

  def kill_slow_queries
    seconds= params[:seconds].present? ? params[:seconds].to_i : 200
    queries=ActiveRecord::Base.connection.execute("SELECT ID, HOST, COMMAND, TIME, STATE, INFO FROM INFORMATION_SCHEMA.PROCESSLIST WHERE TIME>#{seconds};").to_a

    kill_command=''
    queries.each{|q| kill_command+="kill #{q[0]};" }
    ActiveRecord::Base.connection.execute(kill_command);

    keys=[:id, :host, :command, :time_in_seconds, :current_state, :query, :progress]
    queries.map!{|q| keys.zip(q) }
    queries=Hash.new queries
    render json: {time:Time.now, killed_queries: queries}
  end

  # This action outputs logs for super users to search or review.
  # Custom user stylesheets or browser plugins like JSONView are very helpful for this.
  # This action performs a partial file read, rather than loading the entire thing,
  # since it can be many megabytes in size.
  # This action also accommodates searching the logs.
  # @param:
  #   -lines_to_show: integer, optional
  #   -search_string: string, optional
  def view_recent_server_logs
    default_lines_to_show=200
    specified_lines_to_show=params[:lines_to_show].to_i
    lines_to_show=specified_lines_to_show > 0 ? specified_lines_to_show : default_lines_to_show
    search_string=params[:search_string]
    start_time_match_pattern=Regexp.new '\A\# Logfile created on (\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} -\d{4}) by logger\.rb\/?\d*$'

    log_file_path="log/#{Rails.env}_via_lograge.log"
    log_exists=File.exists?(log_file_path)
    return render(json:[]) unless log_exists

    if search_string.present?
      log_lines=`grep --no-filename -r "#{search_string}" #{log_file_path}*`.split("\n") rescue []
      line_count=log_lines.length
      log_lines=log_lines[0..(lines_to_show-1)]
    else
      line_count=`wc -l #{log_file_path}`.match(/^(\d+)/)[0].to_i rescue 0
      log_lines=IO.foreach(log_file_path).take(lines_to_show)#yeilds an array of strings

    end

    if log_lines[0] && log_lines[0].match(start_time_match_pattern)
      entries=log_lines[1..-1]
    else
      entries=log_lines
    end

    entries_string=entries.join(",\n")
    #wrap entire log in an object, and log entries in an array.
    wrapped_log_lines="{\"number_of_lines_being_shown\":#{[lines_to_show,line_count].min},\n"+
      "\"number_of_lines_counted_in_#{search_string.present? ? 'results' : 'file'}\":#{line_count},"+
      "\n\"entries\":[#{ entries_string }]}"

    json_log_content=JSON.parse(wrapped_log_lines) rescue nil
    if json_log_content
      render json: json_log_content
    else
      render text: wrapped_log_lines
    end
  end

  #This endpoint aims to provide system admins with aggregate data on
  #where most requests are going within the system
  #and whether they are succeeding or failing.
  def view_recent_request_stats
    require 'oj'
    keys_to_collect=%w[method path status error duration db current_user_id current_api_user_id]
    default_reqs_to_show=200
    specified_reqs_to_show=params[:reqs_to_show].to_i
    reqs_to_show=specified_reqs_to_show > 0 ? specified_reqs_to_show : default_reqs_to_show

    log_file_path="log/#{Rails.env}_via_lograge.log"
    log_exists=File.exists?(log_file_path)
    return render(json:[]) unless log_exists

    stats={
      requests:0,
      uniq_users_count:0,
      uniq_api_users_count:0,
      sum_duration_in_ms:0,
      sum_db_time_in_ms:0,
      avg_duration_in_ms:0,
      avg_db_time_in_ms:0,

      user_reqs_by_method_and_path:{},
      user_reqs_by_status:{},
      user_reqs_by_error:{},
      user_reqs_by_user:{},

      api_reqs_by_method_and_path:{},
      api_reqs_by_status:{},
      api_reqs_by_error:{},
      api_reqs_by_user:{},

      unauth_reqs_by_method_and_path:{},
      unauth_reqs_by_status:{},
      unauth_reqs_by_error:{},

    }.stringify_keys!

    log_lines=IO.foreach(log_file_path).take(reqs_to_show)#yeilds an array of strings
    log_lines.map do |l|
      req_hash=Oj.load(l) rescue nil
      next unless req_hash
      req_hash=req_hash.select{|k,v| keys_to_collect.include?(k) }
      req_hash['duration']=req_hash['duration'].to_f
      req_hash['db']=req_hash['db'].to_f

      stats['requests']+=1
      stats['sum_duration_in_ms']+=req_hash['duration']
      stats['sum_db_time_in_ms']+=req_hash['db']

      if req_hash['current_user_id'].present?
        auth_prefix='user'
      elsif req_hash['current_api_user_id'].present?
        auth_prefix='api'
      else
        auth_prefix='unauth'
      end
      user=req_hash['current_user_id']||req_hash['current_api_user_id']
      method_and_path= req_hash['method']+' '+req_hash['path']

      m_and_p_obj=stats[auth_prefix+'_reqs_by_method_and_path'][method_and_path]||={}
      m_and_p_obj['count']||=0
      m_and_p_obj['count'] +=1
      m_and_p_obj['sum_duration_in_ms']||=0
      m_and_p_obj['sum_duration_in_ms'] +=req_hash['duration']
      m_and_p_obj['sum_db_time_in_ms']||=0
      m_and_p_obj['sum_db_time_in_ms'] +=req_hash['db']
      if req_hash['status']!=200
        m_and_p_obj['failure_count']||=0
        m_and_p_obj['failure_count'] +=1
      end

      status_obj=stats[auth_prefix+'_reqs_by_status'][ req_hash['status'] ]||={}
      status_obj['count']||=0
      status_obj['count'] +=1
      status_obj['sum_duration_in_ms']||=0
      status_obj['sum_duration_in_ms'] +=req_hash['duration']
      status_obj['sum_db_time_in_ms']||=0
      status_obj['sum_db_time_in_ms'] +=req_hash['db']

      if req_hash['error']
        error_obj=stats[auth_prefix+'_reqs_by_error'][ req_hash['error'] ]||={}
        error_obj['paths_effected']||=[]
        error_obj['paths_effected'] << method_and_path
        error_obj['count']||=0
        error_obj['count'] +=1
        error_obj['sum_duration_in_ms']||=0
        error_obj['sum_duration_in_ms'] +=req_hash['duration']
        error_obj['sum_db_time_in_ms']||=0
        error_obj['sum_db_time_in_ms'] +=req_hash['db']
      end
      if auth_prefix!='unauth'
        user_obj=stats[auth_prefix+'_reqs_by_user'][ user ]||={}
        user_obj['count']||=0
        user_obj['count'] +=1
        user_obj['sum_duration_in_ms']||=0
        user_obj['sum_duration_in_ms'] +=req_hash['duration']
        user_obj['sum_db_time_in_ms']||=0
        user_obj['sum_db_time_in_ms'] +=req_hash['db']
        if req_hash['status']!=200
          user_obj['failure_count']||=0
          user_obj['failure_count'] +=1
        end
      end
    end

    stats.each do |unit_key,unit|
      next unless unit.is_a?(Hash)
      unit.each do |sub_unit_key,sub_unit|
        sub_unit['avg_duration_in_ms']=(sub_unit['sum_duration_in_ms']/sub_unit['count']).round(2)
        sub_unit['avg_db_time_in_ms']=(sub_unit['sum_db_time_in_ms']/sub_unit['count']).round(2)
        sub_unit.delete 'sum_duration_in_ms'
        sub_unit.delete 'sum_db_time_in_ms'
        sub_unit['by_users']=sub_unit['by_users'].uniq! if sub_unit.has_key?('by_users')
      end
    end
    stats['uniq_users_count']=stats['user_reqs_by_user'].keys.length
    stats['uniq_api_users_count']=stats['api_reqs_by_user'].keys.length
    stats['avg_duration_in_ms']=(stats['sum_duration_in_ms']/stats['requests']).round(2)
    stats.delete 'sum_duration_in_ms'
    stats['avg_db_time_in_ms']=(stats['sum_db_time_in_ms']/stats['requests']).round(2)
    stats.delete 'sum_db_time_in_ms'

    render json: stats
  end

  #
  def view_recent_automated_job_stats
    render json:{ error: 'Not yet implemented.' }
  end

  # Return JSON containing case, consumer, agent, status, quote data
 def ezl_cases
   agent_ids = get_user_ids
   return if performed?
   direction = params.fetch(:order_direction, 'DESC')
   @scope = Crm::Case.where(agent_id:agent_ids).open.where(is_a_preexisting_policy:false)
   @scope = case params[:order_column]
   when 'current_status'
     @scope.joins_current_status.order("status_type_name #{direction}")
   when 'submission_type'
     @scope.joins(:consumer).order("lead_type #{direction}")
   when 'source'
     @scope.joins(:consumer).order("source #{direction}")
   when 'premium' #fixme after deploy
    @temp = @scope.includes(:quoted_details).sort_by{|data| data.annualized_premium || 0}
    @temp = @temp.reverse! if direction.eql?("DESC")
    array_of_ids = @temp.map(&:id)
    if array_of_ids.present?
      Crm::Case.where(id: array_of_ids).order("field(id, #{array_of_ids.join(',')})")
    else
      Crm::Case.none
    end
   when 'assigned_to'
     @scope.joins(:agent)
     .order("users.full_name #{direction}")
   else
     @scope.order("crm_cases.updated_at #{direction}")
   end

    @scope = @scope.preload(consumer:[:brand])
    @scope = @scope.preload(:consumer)
    @scope = @scope.preload(:statuses)
    @scope = @scope.preload(:quoted_details)
    @scope = @scope.preload(:agent)


   # Scope for filter by name
   @scope = @scope.person_name_like(params[:person_name]) if params[:person_name].present?

   # Scope for filter by status
   @scope = @scope.status_type_id(params[:status_type_id]) if params[:status_type_id].present?

   # Scope for created_at
   @scope = @scope.where('crm_cases.updated_at >= ?', params[:updated_at_gte].to_date) if (params[:updated_at_gte].present? && (params[:updated_at_gte].to_date rescue nil))
   @scope = @scope.where('crm_cases.updated_at <= ?', params[:updated_at_lte].to_date) if (params[:updated_at_lte].present? && (params[:updated_at_lte].to_date rescue nil))

   @cases = @scope.paginate(page: params[:page], per_page: DEFAULT_PER_PAGE)

   @errors=''
   @cases_hash = @cases.map do |kase|
     begin
       {
         id: kase.id,
         client_name: kase.consumer.try(:full_name),
         last_updated: kase.updated_at,
         current_status: kase.status.try(:status_type_name),
         submission_type: kase.consumer.try(:lead_type),
         source: kase.consumer.try(:source),
         premium: kase.quoted_details.try(:annualized_premium),
         assigned_to: kase.agent.try(:full_name),
         consumer_id: kase.try(:consumer_id),
         agent_id: kase.agent_id,
         brand_name: kase.consumer.try(:brand_name),
         brand_id: kase.consumer.try(:brand_id),
         status_id: kase.status.try(:id),
       }
     rescue => ex
       Rails.logger.error [ex.message, kase.as_json].join("\n")
       @errors+="For policy #{kase.id}: #{ex.message}.\n"
       nil
     end
   end.compact
   output_obj={
     cases:      @cases_hash,
     page:       @cases.current_page,
     page_count: @cases.total_pages,
     count:      @cases.count,
     errors:     @errors,
   }
   render json: output_obj, status: @errors.present? ? 422 : 200
 end
    def tasks
      @scope = Task
      @scope = @scope.for_users(@user_ids)
      build_scope
      paginate_and_render
    end

    def opportunities
      @scope=Task.
        joining_origin.
        where('tasks.origin_type="Crm::Status" AND crm_statuses.sales_stage_id=1')#this will include all opportunities, and new cases that come in from api
      @scope = @scope.for_users(@user_ids).readonly(false)
      build_scope
      paginate_and_render
    end

    def unassigned
      @scope = Task
      @scope = current_user.super? ? @scope.unassigned_for_users(@user_ids) : @scope.unassigned
      build_scope
      paginate_and_render
    end

private

  def get_user_ids
    user = params[:user_id] ? User.find(params[:user_id]) : current_user
    return permission_denied unless current_user == user || user.editable?(current_user)
    
    if params[:team]
      team_permissions=[:view_descendants_resources,:edit_descendants_resources,:view_siblings_resources,:edit_siblings_resources,:view_nephews_resources,:edit_nephews_resources]
      unless current_user.can_any?(*team_permissions)
        return permission_denied('You need permission to view team resources to access dashboard team tables.')
      end
      @user_ids=[]
      @user_ids+=user.descendant_ids          if current_user.can_any?(*team_permissions[0..1])
      @user_ids+=user.siblings.pluck(:id)     if current_user.can_any?(*team_permissions[2..3])
      @user_ids+=user.nephews_only.pluck(:id) if current_user.can_any?(*team_permissions[4..5])
      return canned_fail_response("You do not appear to have any team members yet.") if @user_ids.empty?
    else
      @user_ids=[user.id]
    end
    @user_ids
  end

  def build_scope
    @scope = @scope.for_users([params[:assigned_to_id].to_i]) if params[:assigned_to_id].present?
    # Scope for person
    @scope = @scope.where(person_type:params[:person_type],person_id:params[:person_id]) if params[:person_type].present? && params[:person_id].present?
    # Scope for label
    @scope = @scope.label_like(params[:label]) if params[:label].present?
    # Scope for status_type
    @scope = @scope.status_type_id(params[:status_type_id]) if params[:status_type_id].present?
    # Scope for created_at
    @scope = @scope.created_in_range(params[:created_at_gte], params[:created_at_lte])
    # Scope for due_at
    @scope = @scope.due_in_range(params[:due_at_gte], params[:due_at_lte])
    # Scope for due
    @scope = @scope.where('due_at <= ?', 1.month.from_now) if params[:due]  == 'Current'
    if params[:due]  == 'New Lead Only'
      @scope = @scope.where(
        'due_at <= ? AND crm_statuses.status_type_category_id=?',
        1.month.from_now, Enum::StatusTypeCategory.id('New Lead')
      )
    end
    # Order by
    direction = params[:order_direction] || 'ASC'
    if params[:order_column].eql?('branding')
      @scope = @scope.sort_by{|data| data.try(:brand_name_of_consumer).present? ? data.try(:brand_name_of_consumer).downcase : '' }
      @scope = @scope.reverse! if direction.eql?("DESC")
    elsif params[:order_column].eql?('premium')
      @scope = @scope.sort_by{|data| data.annualized_premium || 0}
      @scope = @scope.reverse! if direction.eql?("DESC")
    else
      @scope = @scope.order("#{params[:order_column] || 'due_at'} #{direction}")
    end
  end

  def paginate_and_render json_opts={}
    @tasks = @scope.paginate(page: params[:page], per_page: DEFAULT_PER_PAGE)
    # Paginage and render
    tasks_as_json=@tasks.as_json({
      include: {
        status:{},
        person:{include:[:phones,:emails]}
      },
      methods: [:annualized_premium, :brand_name_of_consumer]
    })
    render json:{
      tasks:      tasks_as_json,
      count:      @tasks.count,
      page:       @tasks.current_page,
      page_count: @tasks.total_pages
    }
  end
end
