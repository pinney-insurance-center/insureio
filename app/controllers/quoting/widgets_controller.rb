# Widgets controller does not implement rails CRUD
# Instead we expose a rest API to work with angular
# Rails CRUD when trying to make a SPI is godawful
# so instead we use angularjs for client side MVC and expose a REST API
# Separation of logic and view ftw.

class Quoting::WidgetsController < ApplicationController
  WIDGET_ACTIONS = [:pre_flight_response, :get_quotes, :show, :track_widget_view, :legacy_ezl_widget, :tunnel_for_local_storage]

  skip_before_filter :require_referrer_host_match
  skip_before_filter :verify_authenticity_token, only: [:pre_flight_response,:track_widget_view]
  before_filter :require_login, except: WIDGET_ACTIONS
  prepend_before_filter ->{ @additional_auth_keys=[:quoter_key] }, only: WIDGET_ACTIONS
  before_filter :set_any_http_origin, only:   [:get_quotes, :track_widget_view,:tunnel_for_local_storage]
  before_filter -> {require_permission :quoter_widget}, only:[:index, :create, :destroy, :update]

  after_filter :set_csrf_cookie_for_ng
  after_filter :allow_cross_site_request, only:[:show, :tunnel_for_local_storage]

	def index
    if request.format.html?
      return render 'public/widgets/container.html', layout:!params[:no_layout]
    end

    render json: current_user.quoting_widgets.as_json(methods:[:user_name,:brand_name])
	end

	#we expose both html and json endpoints for browsing and rest access
	def show
		@widget = Quoting::Widget.find(params[:id])

		respond_to do |f|
			f.html{ render :layout => false}
			f.js{ render :layout => false}
			f.json {render :json => @widget.as_json}
			f.css { }
		end
	end

  #These 4 actions are called from the edit modal.
  #In the consumer-facing form, the widget renders these as partials within `container.html.erb`.
  def form_for_tli
    wt_id=Enum::QuotingWidgetType.id('Term Life Insurance')
    @widget=Quoting::Widget.new(widget_type_id: wt_id, user: current_user)
    render layout: false
  end

  def form_for_ltc
    wt_id=Enum::QuotingWidgetType.id('Long Term Care')
    @widget=Quoting::Widget.new(widget_type_id: wt_id, user: current_user)
    render layout: false
  end

  def form_for_rtt
    wt_id=Enum::QuotingWidgetType.id('Motorists Real Time Term')
    @widget=Quoting::Widget.new(widget_type_id: wt_id, user: current_user)
    render layout: false
  end

  def form_for_cap
    wt_id=Enum::QuotingWidgetType.id('Lead Capture')
    @widget=Quoting::Widget.new(widget_type_id: wt_id, user: current_user)
    render layout: false
  end

  def edit_modal
    render 'edit_modal', layout:false
  end

  def tunnel_for_local_storage
    render layout:false
  end

  def legacy_ezl_widget
    agent=User.where(quoter_key:params[:code]).first
    return render(nothing:true, status:401) unless agent
    widget=agent.quoting_widgets.first
    return render(nothing:true, status:401) unless widget
    redirect_to 'action'=>'show', id:widget.id, format:'js'
  end

  #This action is called by the widget when it is first made visible.
  #The intention is to track widget views independently of page views.
  #So if a page loads several widgets in hidden modals, and the consumer only ever views one widget, only one widget view event will be generated.
  def track_widget_view
    @widget = Quoting::Widget.find(params[:id])

    # Don't track requests coming from DataRaptor. Those will just be users viewing the widget preview.
    unless request.referrer && request.referrer=~/^\w+\:\/\/(\w+[-\w^\/]+\.)+insureio\.com/
      log_event_with_ahoy "Viewed #{@widget.widget_type.name|| 'Quoter'} Widget \"#{@widget.name}\"", owner_id:@widget.user_id, brand_id:params[:brand_id]||@widget.brand_id, participant_type:params[:consumer_id].present? ? 'Consumer' : nil, participant_id:params[:consumer_id]
    end
    render nothing:true
  end

	#we *don't expose html for update because updates should only be done via RESTful calls
	def update
    params=HashWithIndifferentAccess.new request.params.select{|p| Quoting::Widget.new.respond_to?(p) }
    unless @widget = current_user.quoting_widgets.find_by_id(params[:id])
		  return render json: {errors:"Could not find a widget with that id for the current user."}, status:406
    end
		respond_to do |f|
      @widget.attributes = params
      if @widget.save
				f.json { render json:@widget.as_json }
			else
				f.json { render json: {errors: @widget.errors.full_messages.join('. ')}, status: 400 }
			end
		end
	end

	#we *don't expose html for create because creation should only be done via RESTful calls
	def create
    unless current_user.can?(:quoter_widget_multiple) || current_user.quoting_widgets.count == 0
      return permission_denied("You have at least one widget saved already and don't have permission to create more. Have you considered editing your existing widget(s)?")
    end
    params=request.params
    params=params.select{|p| Quoting::Widget.new.respond_to?(p) }
		@widget = current_user.quoting_widgets.build(params)
		respond_to do |f|
			if @widget.save
				f.json { render :json => @widget.as_json}
			else
				f.json { render json:{msg:"Failed to create widget.\n#{@widget.errors.full_messages.join(".\n")}"}, status:400 }
			end
		end
	end

	def destroy
		current_user.quoting_widgets.destroy(params[:id])
		render :json => nil, :status => 200
	end

	# Additional rest api endpoints outside of CRUD

  def pre_flight_response
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, POST, GET, OPTIONS'
    response.headers['Access-Control-Max-Age'] = '1000'
    response.headers['Access-Control-Allow-Headers'] = 'Accept,Accept-Language,Content-Language,Content-Type,x-csrf-token,x-xsrf-token'
    render nothing:true
  end

private

  # By default rails sends 'X-Frame-Options' headers, restricting cross-site iframe use.
  def allow_cross_site_request
    response.headers.delete_if{|key| key=='X-Frame-Options' }
    response.headers['Set-Cookie'] = [response.headers['set-cookie'], 'Secure;SameSite=None'].compact.join(';')
  end

end
