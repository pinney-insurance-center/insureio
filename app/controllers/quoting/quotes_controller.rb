class Quoting::QuotesController < ApplicationController
  respond_to :html, :js, :json
  append_view_path 'app/views/quoting/quotes'

  # These actions do not require the same strictures for `current_user` as `require_login` does
  CONSUMER_FACING_ACTIONS = [:health_class_info, :new, :legacy_ezl_link, :get_quotes, :save_from_quote_path]

  skip_before_filter :verify_authenticity_token
  skip_before_filter :require_referrer_host_match
  prepend_before_filter -> { @ltd_auth_enabled = true }, only: CONSUMER_FACING_ACTIONS
  before_filter :require_login_or_quoter_key, only: CONSUMER_FACING_ACTIONS
  before_filter :require_login, except: CONSUMER_FACING_ACTIONS
  before_filter :_scrub_params, only:[:create, :update]
  before_filter :set_any_http_origin, only:[:get_quotes]

  # Render the quote form.
  def new
    render 'new', format: :html#in the event that #edit somehow gets called without a consumer id or an agent id
  end

  alias_method :edit, :new

  #This action saves either just consumer or consumer with main policy and pre-existing policies.
  #Most data manipulation is occurring on the front end.
  def save_from_quote_path
    consumer_attrs=params[:consumer]
    minimal_fields_present=(
      consumer_attrs.present? &&
      consumer_attrs[:full_name].present? &&
      consumer_attrs.dig(:emails_attributes,0,:value).present?
    )
    return canned_fail_response('Consumer name and email are required to save') unless minimal_fields_present
    prevent_blank_assoc_errors Consumer, consumer_attrs
    @consumer = consumer_attrs[:id] ? Consumer.find(consumer_attrs[:id]) : Consumer.new
    @consumer.agent ||= current_user
    return canned_fail_response(status: 403) unless @consumer.editable?(current_user)
    # The quoter_key grants a client-user access to ALL the agent's resources. Disallow overwrites
    return canned_fail_response(status: 403) if @consumer.persisted? && :quoter_key == Thread.current[:current_user_auth]
    @consumer.attributes=consumer_attrs
    kase = @consumer.cases.find &:new_record?
    if @consumer.save
      render json: { url: "/consumers/#{@consumer.id}", consumer_id: @consumer.id, kase_id: kase&.id, errors: controller_errors }
    else
      render json:{errors:@consumer.errors.full_messages}, status:422
    end
  end

  def update
    @quote=Quoting::Quote.find(params[:id])

    attrs= params[:quoting_quote]||{}
    if params[:key].present? && params[:value].present?
      attrs[ params[:key] ]=params[:value]
    end
    return canned_fail_response(status: 403) if @quote.crm_case && ! @quote.crm_case.editable?(current_user)
    return canned_fail_response(status: 403) if @quote.consumer && ! @quote.consumer.editable?(current_user)
    if @quote.update_attributes(attrs)
      obj, opts=@quote.crm_case ? [@quote.crm_case, {jopts: json_opts}] : [@quote, {}]
      canned_success_response obj, opts
    else
      canned_fail_response @quote
    end
  end

  def show
    @quote=Quoting::Quote.find(params[:id])
    render json: @quote
  end

  def health_class_info
    render format: :html, layout: 'pop_up_window'
  end

  #Requires login.
  #Sends an html email with a quote path link and the selected list of quotes.
  #Saves the quotes as opportunity records.
  def email
    required_params=[
      :brand_id, :user_id, :consumer_id, :to_address, :quotes,
      :request_type, :product_line, :premium_mode_id, :health_class_id, :face_amount, :duration_id
    ]
    missing_rps=required_params.select{|k| !params.has_key?(k) }
    return canned_fail_response("Missing required parameters: #{missing_rps.join(', ')}") if missing_rps.present?

    brand = Brand.find_by_id params[:brand_id]
    smtp_config = current_user.branded_smtp_config(brand)

    raise Net::SMTPAuthenticationError.new unless smtp_config.present?

    @quote_url = APP_CONFIG['base_url']+'/quoting/quotes/new?'
    query_params = params.slice(
      :requestType, :productLine, :duration_id, :face_amount, :premium_mode_id, :health_class_id, :consumer_id,
    )
    query_params.merge! ltd_access_code: current_user.access_code(Consumer.find_by id: params[:consumer_id])
    @quote_url += query_params.to_query
    #set global vars for rendering message
    @quoter=params
    @quotes=params[:quotes]

    consumer=Consumer.find(params[:consumer_id])

    html = render_to_string(
      file:'/quoting/quotes/email',
      layout:false, formats:[:html],
      locals: {
        :@brand=>brand,
        :@recipient=>consumer,
        :@agent=>consumer.agent
      }
    )
    mail = Mail.new(
      to:           params[:to_address],
      from:         smtp_config.from_addr,
      subject:      'Emailed quotes from Insureio',
      content_type: "text/html",
      body:         html
    )
    mail.delivery_method :smtp, smtp_config.to_delivery_method
    mail.deliver!

    params[:quotes].each do |q|
      q.delete(:annual_premium)
      q.delete(:monthly_premium)
    end

    save_succeeded=consumer.update_attributes(opportunities_attributes:params[:quotes])
    if save_succeeded
      canned_success_response
    else
      error_msg="Successfully emailed quotes, but failed to save them. #{consumer.errors.full_messages}"
      render json:{errors:error_msg}, status:400
    end
  rescue Net::SMTPAuthenticationError => ex
    @errors = ["Email failed to send based on a SMTP Configuration Error. Please check your email settings under the \"My Account\" tab to verify proper set-up."]
    @status = 500
  rescue Exception => ex
    @errors = ["#{ex.message} <#{ex.backtrace[0]}>"]
    @status = 500
  ensure
    return if performed?
    render json:{errors:@errors}, status:@status||200
  end

  def get_quotes
    return permission_denied unless params.has_key?(:consumer)

    instantiate_or_persist_consumer
    c_attrs=params[:consumer]
    if c_attrs[:cases_attributes].present?
      @case ||= @consumer.cases.last #`@consumer` is defined in `instantiate_or_persist_consumer`
    elsif c_attrs[:opportunities_attributes].present?
      opp_attrs=c_attrs[:opportunities_attributes]
      details_hash = opp_attrs[0]||opp_attrs['0']
      @opportunity ||= @consumer.opportunities.last||Quoting::Quote.new(details_hash)
    end
    @qd=@case.try(:current_details)||@opportunity

    unless data_is_sufficient_for_quoting?
      @controller_warnings+=@consumer.errors.full_messages+@case.errors.full_messages
      return render json:{
        kase:@case,
        errors:@missing_data_msg,#set in `data_is_sufficient_for_quoting?`
        warnings:@controller_warnings
      }, status:422 
    end

    xrae_disease_factors=@consumer.health_info.try(:disease_factors_incompatible_w_compulife)||[]
    use_xrae=@qd.health_class_id.to_i > 6 || xrae_disease_factors.present?
    @results = use_xrae ? xrae_quoter_results : compulife_results

    @controller_warnings+=@consumer.errors.full_messages

    factors_requiring_underwriting=[]
    Consumer::FACTORS_REQUIRING_UNDERWRITER.each do |key, human_text|
      factors_requiring_underwriting << human_text if @consumer.send(key)
    end
    if factors_requiring_underwriting.present?
      warning_str ="Factor(s) skipped for now that will require manual underwriting:\n"
      warning_str+=factors_requiring_underwriting.join("\n")
      @controller_warnings<< warning_str
    end
    if xrae_disease_factors.length>=2#multiple xrae diseases require underwriter.
      factors_requiring_underwriting+=xrae_disease_factors
      warning_str ="Cannot accurately quote for multiple diseases.\n"
      warning_str+=xrae_disease_factors.map(&:to_s).map(&:humanize).join("\n")
      @controller_warnings<< warning_str
    end
    if use_xrae && @qd.duration && !(@qd.duration.years>0)
      xrae_and_table_text='The quoter lacks functionality for UL quotes when either '+
        'a) the client\'s health is table rated or b) an impairment is flagged in the client\'s health history. '+
        'To run quotes for this client, please choose a term coverage duration (i.e. nothing in the pattern "To Age X"). '+
        'Or contact the underwriter, Mike Woods.'
      @controller_warnings<< xrae_and_table_text
    end

    if @results.is_a?(Array)
      if params.has_key?(:widget_id)
        w=Quoting::Widget.find_by_id( params[:widget_id] )
        if w
          bcs=w.blacklisted_carriers
          wcs=w.whitelisted_carriers
          @results.delete_if do |r|
            #Filter based on whitelist for widget type (if any),
            #user-provided blacklist for widget (if any),
            #and bsb list.`
            ( bcs.include?(r.carrier) ) ||
            ( !wcs.include?(r.carrier) ) ||
            ( request.subdomain=='bsb' && !r.carrier.try(:works_for_bsb) )
          end
        end
      end

      render json:{
        results:          @results,
        kase_id:          @case.try(:id),
        consumer_id:      @consumer.id,
        address_id:       @consumer.addresses.last.try(:id),
        health_info_id:   @consumer.health_info_id,
        financial_info_id:@consumer.financial_info_id,
        quoted_details_id:@qd.try(:id),
        health_info:      @consumer.health_info,
        warnings:         @controller_warnings
      }
    elsif @results.is_a?(Hash) && @results[:error].present?
      message=@results[:error]+'. '
      render json:{errors:message}, status:422
    else
      @errors||=@results.errors.try(:full_messages)
      render json:{errors:@errors}, status:422
    end
  rescue Errno::ETIMEDOUT => ex
    ExceptionNotifier.notify_exception(ex)
    render status:408, json:ex.backtrace.to_json
  rescue Enum::NotFound => ex
    render json: {errors: [ex.message]}, status: 422
  end

private

  #This method returns options for generating the JSON necessary to create/update the AngularJS policies GUI.
  def json_opts
    all_contact_methods={include:[:phones, :emails, :addresses],except:[:encrypted_ssn],methods:[:ssn]}
    {
      include:{
        quoting_details:{}, notes:{}, attachments:{}, replaced_by_id:{}, statuses:{},
         stakeholder_relationships:{stakeholders: all_contact_methods},
      }
    }
  end

  # Provide a string which can be included in URL query data and checked for validity in controller.
  # This is just a bit of obfuscation to prevent just anyone from setting data.
  def consumer_params_key consumer_id
    Base64.encode64(Digest::MD5.digest("PEPPER#{consumer_id}")).gsub(/[\s=]+/, '')
  end

  def controller_errors
    @errors ||= ActiveModel::Errors.new(self)
  end

  #This method is needed for `controller_errors.full_messages` to work without throwing method missing errors.
  def self.human_attribute_name(attribute, options={})
    attribute
  end

  def prevent_blank_assoc_errors object_class, attr_hash
    # Prevent error of backend parsing empty array (from JSON request)
    # as nil and then trying to create an instance
    # with nil supplied as value for a has_many association.
    object_class.reflections.select{|k,v| v.macro == :has_many}.each do |name,reflection|
      next if name == :opportunities # `opportunities` is a deprecated association
      if object_class.instance_methods.include?("#{name}_attributes=".to_sym)
        attr_hash["#{name}_attributes"] ||= []
      end
    end
  end

  def instantiate_or_persist_consumer
    attr_hash=params[:consumer]

    #Quote path front end requires this info, but first step of quoter may or may not, so best to check here as well.
    name_and_contact_method_present=lambda{
      attr_hash[:full_name].present? &&
      ( attr_hash[:phones_attributes].present? ||
        attr_hash[:emails_attributes].present? )
    }
    just_an_agent_comparing_quotes=request.referrer && request.referrer.match(/\/quoting\/quotes\/new/) && current_user

    if attr_hash[:id].present? && @consumer=Consumer.find_by_id(attr_hash[:id])
      @consumer.update_attributes(attr_hash)
    elsif !just_an_agent_comparing_quotes && name_and_contact_method_present.call()
      @consumer=Consumer.update_existing_or_create_new(attr_hash)
    else
      @consumer=Consumer.new attr_hash
    end
    @consumer
  end

  def data_is_sufficient_for_quoting?
    fields_for_c =[:state, :birth_or_trust_date, :feet, :inches, :weight, :gender_string]
    missing_fields=[]
    fields_for_c.each do |f|
      missing_fields.push(f) unless @consumer[f].present?
    end
    missing_fields.push(:face_amount) unless @qd && @qd.face_amount.present? && @qd.face_amount>0
    missing_fields.push(:duration) unless @qd && @qd.duration.present?

    if missing_fields.present?
      @missing_data_msg="Missing fields required for quoting: #{missing_fields.join(', ')}"
    end
  end

  def compulife_results
    #Should return an object with either key "results", or key "errors":
    term_quoter = Compulife.new @consumer, @qd
    term_quoter.get_quotes
  end

  def xrae_quoter_results
    case_for_xrae=@case||@opportunity
    xrae_quoter = Quoting::Xrae::Client.new(case_for_xrae)
    xrae_quoter.get_quotes

    if xrae_quoter.errors.present?
      {error: "Message from Quoting Engine: #{xrae_quoter.errors.full_messages.join(' ')}" }
    else
      xrae_quoter.quotes
    end
  end

  # Gets rid of certain params unless certain flag params are set
  def _scrub_params
    if params[:consumer] 
      #remove dollar signs and commas from financial info fields
      if params[:consumer][:financial_info_attributes]
        params[:consumer][:financial_info_attributes].each do |k,v|
          params[:consumer][:financial_info_attributes][k]=v.gsub(/[$,]/,'') if v.is_a?(String)
        end
      end
      #temporary measure, until i hear back from Ryan as to what the "Replacing a Policy" section should do or store
      if params[:consumer][:cases_attributes]
        params[:consumer][:cases_attributes].delete(:replacing)
        params[:consumer][:cases_attributes].delete(:adding)
      end
      # destroy phones that have blank value
      if params[:consumer][:phones_attributes]
        params[:consumer][:phones_attributes].each do |phone_attrs|
          phone_attrs[:_destroy] = true if phone_attrs[:value].blank?
        end
      end
    end
  end

  REFERRAL_REQUEST = 'REFERRAL'

end