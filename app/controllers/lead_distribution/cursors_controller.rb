class LeadDistribution::CursorsController < ApplicationController
  before_filter :require_login
  respond_to :json

  def create
    lead_type = Crm::LeadType.find_or_create_by_text(params[:lead_type_text])
    cursor = LeadDistribution::Cursor.create lead_type:lead_type, brand_id:params[:brand_id]
    respond_with cursor
  end

  def reset_counts
    cursor = LeadDistribution::Cursor.find(params[:id])
    cursor.reset_counts
    render json:{}
  end

  #This action returns info about the linkages between between users, brands, and lead types.
  #It will also contain distribution cursor records, which consist only of lead_type_id, brand_id, rule_id.
  def join_tables
    c=ActiveRecord::Base.connection
    #Fastest approach here is to make simple queries,
    #and filter results as needed within rails app logic rather than via MySQL.
    lead_types_brands=c.execute("SELECT lead_type_id, brand_id FROM crm_lead_types_brands").to_a
    brands_users=c.execute("SELECT brand_id, user_id FROM brands_users").to_a
    cursors=LeadDistribution::Cursor.select([:lead_type_id,:brand_id]).all
    if current_user.super?
      brands=Brand.not_private.name_and_id_only.order('full_name ASC')
      lead_types=Crm::LeadType.all.order('text ASC')
    else
      user_ids=User.excluding_recruits.editable_by(current_user).pluck(:id)
      brands=Brand.viewable_by(current_user).name_and_id_only.preload(:lead_types).order('full_name ASC')
      lead_types=brands.map(&:lead_types).flatten
      lead_types_brands=lead_types_brands.select{|j| brands.map(&:id).include?(j[1]) }
      brands_users=brands_users.select{|j| user_ids.include?(j[1]) }
      cursors=cursors.select{|c| brands.map(&:id).include?(c.brand_id) }
    end
    render json:{
      brands: brands,
      lead_types: lead_types,
      lead_types_brands: lead_types_brands,
      brands_users: brands_users,
      cursors: cursors,
    }
  end
end
