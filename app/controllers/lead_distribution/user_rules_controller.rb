class LeadDistribution::UserRulesController < ApplicationController
  before_filter :require_login
  before_filter :authenticate_agency_manager
  respond_to :json

  def index
    if request.format.html?
      return render 'public/lead_distribution/user_rules/gui.html', layout:!params[:no_layout]
    end
    cursors = scope_for_current_user(LeadDistribution::Cursor)
    cursors = cursors.where(brand_id: params[:brand_id] ) if params[:brand_id].present?
    cursors = cursors.where(lead_type_id:params[:lead_type_id]) if params[:lead_type_id].present?
    cursors = cursors.preload(all_rules:{},brand:[:members])
    cursors.each(&:create_missing_rules)

    methods_to_output=[:lead_type_id, :brand_id]
    rules=LeadDistribution::UserRule.where( cursor_id:cursors.map(&:id) ).preload(:cursor)
    rules = rules.where(user_id:params[:agent_id]) if params[:agent_id].present?
    if params[:include_user_name]
      rules = rules.preload(:user)
      methods_to_output << :user_name
    end
    if params[:include_permission_flag]
      rules = rules.preload(:user)
      methods_to_output << :lacks_permission
    end

    rules=rules.as_json(methods:methods_to_output)

    render json:rules
  end

  def update_multiple #update multiple records
    return permission_denied('Parameters must contain an array of existing rules with ids.', status: 422) unless params[:rules].is_a?(Array)

    ids = params[:rules].map{|hash| hash[:id] }
    @rules = scope_for_current_user(LeadDistribution::UserRule)
    .joins(:cursor).readonly(false).where(id:ids)
    @failures = []

    # Identify users who have been deactivated. (Historically, lead-dist rules
    # have not consistently been removed when a user is rmoved.)
    active_user_ids = Set.new User.where(id: @rules.map(&:user_id)).select(:id).map(&:id)
    @updatables, @deletables = @rules.partition { |rule| active_user_ids.include? rule.user_id }
    LeadDistribution::UserRule.where(id: @deletables.map(&:id)).destroy_all

    # Try to update remaining records
    remaining_attrs = Hash[params[:rules].map { |attrs| [attrs[:id], attrs] }] # Use a hash for ease of tracking 404s
    @updatables.each do |record|
      attrs = remaining_attrs.delete(record.id).slice(*LeadDistribution::UserRule::USER_MODIFIABLE_ATTRS)
      if attrs.present? and not record.update(attrs)
        @failures << attrs.merge(errors: record.errors.full_messages)
      end
    end

    # Mark params hashes for which lead-dist-rule records were not found
    remaining_attrs.values.each do |attrs|
      attrs[:errors] = { base: ['Record not found'] }
    end
    @failures += remaining_attrs.values

    # Render
    output_obj = {failures: @failures.as_json, deleted: @deletables.as_json}
    status = @failures.empty? ? 200 : 422
    render json: output_obj, status: status
  end

  private

  def scope_for_current_user model
    if current_user.super?
      model.all
    else
      model.where 'lead_distribution_cursors.brand_id': Brand.viewable_by(current_user).pluck(:id)
    end
  end
end
