class TagsController < ApplicationController
  before_filter :require_login

  respond_to :json

  MIN_SEARCH_LENGTH = 3

  def index
    unless params[:q].present? && params[:q].length >= MIN_SEARCH_LENGTH
      return canned_fail_response object: { errors: "A parameter \"q\" of length #{MIN_SEARCH_LENGTH} chars or greater is required" }
    end
    @tags = Tag.viewable_by current_user
    @tags = @tags.where('`value` LIKE ?', "%#{params[:q]}%") if params[:q].present?
    @tags = @tags.group(:value).limit(25).pluck(:value)
    # if this is a request from jQuery-tags-manager, format response differently
    if params[:tags_manager]
      @tags = {tags:@tags.map{|t| {tag:t.to_s} }}
    else
      @tags=@tags.map{|t| {id:t.to_s,text:t.to_s} }
    end
    respond_with @tags
  end
end
