class Usage::StaffAssignmentsController < ApplicationController
  before_filter :require_login
  respond_to :json

  #Users never need to see more than one staff assignment at one time,
  #so this action just returns the first record matching the given criteria,
  def index
    return permission_denied('Invalid parameters', status: 400) unless params[:owner_type].present? and params[:owner_id].present?
    @staff_assignment  = Usage::StaffAssignment.
      where(owner_type:params[:owner_type], owner_id:params[:owner_id]).
      preload([
        :administrative_assistant,
        :case_manager,
        :manager,
        :policy_specialist,
        :application_specialist,
        :underwriter,
        :insurance_coordinator
      ]).first
    render json:@staff_assignment.as_json(methods:[
        :administrative_assistant_name,
        :case_manager_name,
        :manager_name,
        :policy_specialist_name,
        :application_specialist_name,
        :underwriter_name,
        :insurance_coordinator_name,
      ])
  end

  def show
    @staff_assignment  = Usage::StaffAssignment.find_by_id(params[:id])
    @staff_assignment||= Usage::StaffAssignment.new(agent_id: params[:user_id])
    @user=@staff_assignment.agent
  end

  def create
    @staff_assignment = Usage::StaffAssignment.new(params[:usage_staff_assignment])
    return unless require_permission(current_user_is_owner? ? :edit_staff_assignment_self : :edit_staff_assignment)
    if @staff_assignment.save
      respond_with @staff_assignment
    else
      canned_fail_response @staff_assignment
    end
  end

  def update
    @staff_assignment = Usage::StaffAssignment.find(params[:id])
    return unless require_permission(current_user_is_owner? ? :edit_staff_assignment_self : :edit_staff_assignment)
    @staff_assignment.attributes = params[:usage_staff_assignment]
    if @staff_assignment.save
      canned_success_response
    else
      canned_fail_response @staff_assignment
    end
  end

private

  def current_user_is_owner?
    @staff_assignment && @staff_assignment.owner_id == current_user.id && @staff_assignment.owner_type == current_user.class.name
  end

end
