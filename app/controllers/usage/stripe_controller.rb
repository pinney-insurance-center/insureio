# This controller contains an API endpoint for requests from Stripe. This
# requires you to set up Webhooks (https://stripe.com/docs/webhooks) in your
# Stripe dashboard.

class Usage::StripeController < ApplicationController
  protect_from_forgery with: :exception, except: :create_event

  # API endpoint to which Stripe sends events
  def create_event
    @evt = Usage::StripeEvent.build(params, request.raw_post)
    @evt.save!
    @evt.process if @evt.processable?
    AuditLogger['stripe'].info "#{@evt.processable?} -- #{@evt.try(:user).try(:user).try(:payment_due)}"
    render nothing:true
  end

  def create_customer
    unless params[:membership_id].present? && params[:stripeToken].present?
      return permission_denied "Missing a required parameter. Plan and stripe token are required"
    end
    unless params[:membership_id].to_i > 0
      return permission_denied "Invalid Payment Plan selected. Got #{params[:membership_id]}"
    end
    current_user.save!
    s_plan=Enum::Membership.find(params[:membership_id]).stripe_plan_name
    if current_user.stripe_find_or_create_customer(params[:stripeToken], s_plan, false, params[:coupon_code])
      current_user.update_attributes(membership_id:params[:membership_id], payment_due: 1.month.from_now)
      canned_success_response
    else
      SystemMailer.warning("Failed to subscribe user at Stripe.\n\n#{current_user.errors.inspect}\n\nUser #{current_user.inspect}\n\nParams #{params.inspect}")
      canned_fail_response current_user
    end
  end

  def update_plan
    unless params[:membership_id].present?
      return permission_denied "Missing a required parameter. Plan is required"
    end
    unless params[:membership_id].to_i > 0
      return permission_denied "Invalid Payment Plan selected. Got #{params[:membership_id]}"
    end
    s_plan=Enum::Membership.find(params[:membership_id]).stripe_plan_name
    if current_user.stripe_update_plan(s_plan,params[:coupon_code])
      current_user.update_attributes(membership_id:params[:membership_id])
      canned_success_response
    else
      canned_fail_response current_user
    end
  end

  def update_payment_source
    unless params[:stripeToken].present?
      return permission_denied "Missing a required parameter. Stripe token is required"
    end
    unless current_user
      return permission_denied "You can only update payment source after you have chosen a payment plan."
    end
    if current_user.stripe_update_payment_source(params[:stripeToken])
      canned_success_response
    else
      canned_fail_response current_user
    end
  end

  def unsubscribe
    return permission_denied 'This user account does not appear to be subscribed to a payment plan.' if !current_user.stripe_subscribed

    if current_user.stripe_unsubscribe!
      canned_success_response
    else
      canned_fail_response current_user
    end
  end

end
