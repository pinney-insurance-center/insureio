class Usage::RecruitmentController < ApplicationController

  before_filter :require_login, :only => [:index, :user_invitation]
  before_filter :restrict_agency_management_for_agent
  
  def index
    respond_to do |format|
      format.js { }
      format.html { }
    end
  end

  def user_invitation
    return permission_denied('A valid email is required to send.') unless valid_email?(params[:recipient_email])
    if current_user.send_recruitment_email(params[:recipient_email],params[:recipient_name])
      canned_success_response
      else
      canned_fail_response current_user
      end
  end

  private

  def valid_email?(email)
    email.present? && (email =~ /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i)
  end

end