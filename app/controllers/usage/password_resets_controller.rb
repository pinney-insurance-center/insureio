class Usage::PasswordResetsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def new
  end

  def successfully_sent
    @body_id = "password_resetsnew"
  end

  def link_expired
    @body_id = "password_resetsnew"
  end

  def create
    @user=User.excluding_recruits.where(login:params[:login]).first
    unless params[:login].present? && @user.present?
      flash[:error]='There is no user account on file with this login.'
      redirect_to(new_usage_password_reset_path) && return
    end

    email  =@user.emails.where(value:params[:email]).first if params[:email].present?
    email||=@user.primary_email

    unless email.present?
      error_msg='There is no email address on file for this login. Please contact support for assistance.'
      redirect_to("/login?error=#{error_msg}") && return
    end

    if @user.send_password_reset(email)
      flash[:notice]="Email sent with password reset instructions."
      redirect_to(successfully_sent_usage_password_resets_path) && return
    else
      error_msg='Something went wrong. Please contact support for assistance.'
      redirect_to("/login?error=#{error_msg}") && return
    end
  end

  def edit
    @user = User.find_using_perishable_token(params[:id], 1.hour)
    unless @user.present?
      redirect_to(link_expired_usage_password_resets_path) && return
    end
  end

  def update
    @user = User.find_using_perishable_token(params[:id], 1.hour)
    unless @user.present?
      redirect_to(link_expired_usage_password_resets_path) && return
    end
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]
    if  @user.save
      msg="Password updated successfully."
      redirect_to "/login?notice=#{msg}"
    else
      flash.now[:error] = @user.errors.full_messages
      render :edit
    end
  end


end
