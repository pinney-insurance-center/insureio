class Usage::ContractsController < Usage::UsageBaseController
  
  before_filter :require_editable_user_or_current_user
  before_filter :require_login
  before_filter -> { require_permission(:l_and_c) }
  before_filter -> { require_permission(:my_account_view_contracts) }, only:[:index, :show], if: -> {@user == current_user}
  before_filter -> { require_permission(:my_account_edit_contracts) }, only:[:create, :update, :destroy], if: -> {@user == current_user}

  def index
    contracts = @user.contracts
    render json: contracts.as_json(methods:[:state_ids])
  end

  def create
    @contract = Usage::Contract.new(params[:usage_contract])

    if @contract.save
      canned_success_response
    else
      canned_fail_response @contract
    end
  end
  
  def update
    @contract = @user.contracts.where(id: params[:id]).first

    if @contract.update_attributes(params[:usage_contract])
      canned_success_response
    else
      canned_fail_response @contract
    end
  end

  def destroy
    @contract = @user.contracts.where(id: params[:id]).first
    if @contract && @contract.destroy
      canned_success_response
    else
      canned_fail_response @contract
    end
  end

  private

end
