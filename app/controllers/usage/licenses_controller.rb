class Usage::LicensesController < Usage::UsageBaseController

  before_filter :require_editable_user_or_current_user
  before_filter :require_login
  before_filter -> { require_permission(:l_and_c) }
  before_filter -> { require_permission(:my_account_view_licenses) }, only:[:index, :show], if: -> {@user == current_user}
  before_filter -> { require_permission(:my_account_edit_licenses) }, only:[:create, :update, :update_usage_license, :destroy, :edit, :new], if: -> {@user == current_user}

  def index
    licenses = @user.licenses
    render json: licenses
  end

  def create
    @user = User.where(id: params[:usage_license][:user_id]).try(:first)
    @license = Usage::License.new(params[:usage_license])
    
    if @license.save
      canned_success_response
    else
      canned_fail_response @license
    end
  end

  def update
    license = @user.licenses.where(id: params[:id]).first

    attrs= params[:usage_license]||{}
    if params.has_key?('name') && params.has_key?('value')
      attrs[ params[:name] ]=params[:value]
    elsif params.has_key?('key') && params.has_key?('value')
      attrs[ params[:key]  ]=params[:value]
    end

    if license.update_attributes(attrs)
      canned_success_response
    else
      canned_fail_response license
    end
  end

  def destroy
    license = @user.licenses.where(id: params[:id]).first
    if license && license.destroy
      canned_success_response
    else
      canned_fail_response license
    end
  end

  #DEPRECATED. Use `#update`.
  def update_usage_license
    SystemMailer.deprecated_method_warning
    return update
  end

end