class Usage::UsageBaseController < ApplicationController

  before_filter :require_login

private

  def get_user
    params[:user_id] = params[:user_id] if params[:user_id]
    params[:user_id] = params[:usage_license][:user_id] if params[:usage_license] && params[:usage_license][:user_id]
    params[:user_id] = params[:usage_contract][:user_id] if params[:usage_contract] && params[:usage_contract][:user_id]
    @user ||= User.where(id: params[:user_id]).first
  end

  def require_editable_user
    permission_denied unless get_user && current_user.can_edit_user?(@user)
  end
  def require_editable_user_or_current_user
    if current_user.nil?
      permission_denied
    elsif get_user
      permission_denied unless current_user.can_edit_user?(@user) || current_user == @user
    else
      @user = current_user
    end
  end

end
