class UserSessionsController < ApplicationController
  require 'onelogin/ruby-saml'
  include AuditLogger::Helpers

  before_filter :require_no_login, only: %w[new create]
  skip_before_filter :verify_authenticity_token
  skip_before_filter :time_zone_for_current_user, only: [:create]
  skip_before_filter :require_tenant_match, only:[:create]

  respond_to :js,:html,:json

  #Typically, users will go directly to static page '/login', not through this action.
  #Therefor, redirection for already logged in users must be initiated on the client side.
  def new
    redirect_to '/login', layout:false
  end

  def sso
    audit log:'saml_sso'
    #Handle SAML requests from Identity Provider (IdP).
    return redirect_to '/login?error=Single sign on requires the using parameter.' unless params[:using].present? && self.send('saml_settings_for_'+params[:using])
    if !params[:SAMLResponse]#1st request (meta request) from IdP
      return saml_init
    else#2nd request from IdP, carrying actual user data
      return saml_consume
    end
  end

  def saml_metadata
    settings = saml_settings#Account.get_saml_settings
    meta = OneLogin::RubySaml::Metadata.new
    render xml: meta.generate(settings), content_type: "application/samlmetadata+xml"
  end

  #This action is only accessed via json from the login page.
  def create
    creds=params.require(:usage_user_session).permit!
    @user_session = UserSession.new(creds.to_h)
    if @user_session.save
      if cookies[:remember_ip] == "0"
        cookies.delete :remember_ip
      end
      flash[:notice] = "Successfully logged in."
      out = {}
      if request.subdomains.join('.') != @user_session.user.tenant.name
        redirect = URI '/'
        redirect.host = [@user_session.user.tenant.name, request.domain].join('.')
        out[:redirect] = redirect.to_s
      end
      render json: out
    else
      error_msg = 'Incorrect username or password.'
      if @user_session.errors.present?
        error_list=@user_session.errors.full_messages.
        reject{|m| m.match("Password is not valid") || m.match("Login is not valid")}

        error_msg+= ' '+error_list.join('. ') if error_list.present?
        error_msg+='.' if error_list.present?
      end
      render json:{error:error_msg}, status: 401
    end
  end

  def destroy
    @user_session = UserSession.find
    if session[:ostensible_user_id]
      custom_header(:error, 'Impersonation killed.')
      redirect_to controller:'/users', action:'end_impersonate'
    else
      @user_session.try(:destroy)
      msg="Successfully logged out."
      redirect_to "/login?msg=#{msg}"
    end
  end

  # Fires when a browser page unloads. Carry out clean-up duties
  def unload
    # create response
    render js:";"
  end

  private

  def require_no_login
    unless current_user.nil?
      redirect_to root_path, flash: {error: "You are attempting to login, but you are already logged in."}
    end
  end

  #For the time being, this method assumes the Identity Provider to be Motorist Group.
  #It can later be abstracted to allow other IdPs.
  def saml_settings
    settings_for_idp=self.send('saml_settings_for_'+params[:using])

    settings = OneLogin::RubySaml::Settings.new
    settings_for_idp.each do |k,v|
      settings.send "#{k}=", v
    end

    settings
  end

  def saml_settings_for_motorist
    settings_hash={
      assertion_consumer_service_url:     "#{request.protocol}#{request.host}#{Rails.env.production? ? '' : ':'+request.port}/sso?using=motorist",
      issuer:                             "#{request.protocol}#{request.host}#{Rails.env.production? ? '' : ':'+request.port}/saml_metadata",
      name_identifier_format:             "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress",#optional. See http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf
      idp_cert:                           File.open("config/data_sensitive_or_machine_specific/motorist_sso_cert.crt",'r').read
    }

    env_specific_hash=if Rails.env=='production'
      {idp_sso_target_url: "https://secure.motoristsgroup.com/isrvp/migsaml/PinneyTransferInProgress.jsp",
      certificate:         File.open("/var/www/ssl/insureio-cert.crt",'r').read,
      private_key:         File.open("/var/www/ssl/insureio.key",'r').read}
    elsif Rails.env=='release'
      {idp_sso_target_url: "https://secure.motoristsgroup.com/isrvps/migsaml/PinneyTransferInProgress.jsp",
      certificate:         File.open("#{ENV['HOME']}/.ssh/id_rsa.pub",'r').read,
      private_key:         File.open("#{ENV['HOME']}/.ssh/id_rsa",'r').read}
    else
      {idp_sso_target_url: "https://testsecure.motoristsgroup.com/isrvt/migsaml/PinneyTransferInProgress.jsp",
      certificate:         File.open("#{ENV['HOME']}/.ssh/id_rsa.pub",'r').read,
      private_key:         File.open("#{ENV['HOME']}/.ssh/id_rsa",'r').read}
    end
    settings_hash.merge(env_specific_hash)
  end

  def saml_init
    audit level:'info', body: "Requesting SAML identity info from: #{params[:using]}"
    request = OneLogin::RubySaml::Authrequest.new
    redirect_to(request.create(saml_settings))
  end

  #For the time being, this method assumes the Identity Provider (IdP) to be Motorist Group.
  #It can later be abstracted to allow other IdPs.
  def saml_consume
    saml_response=params[:SAMLResponse]
    audit level:'info', body: "Checking SAML identity info provided by: #{request.try(:referrer)}"
    response=OneLogin::RubySaml::Response.new(saml_response, settings: saml_settings) rescue nil
    app_data=response.attributes.attributes.dig('ApplicationData',0) rescue nil
    if response.present?
      audit level:'info', body: "Decoded response body:\n#{response.attributes.attributes}"
    else
      audit level:'info', body: "Raw response body:\n#{saml_response}"
    end
    audit level:'info', body: "Is SAMLResponse valid? #{response.is_valid? || response.errors}"
    begin
      if !response.is_valid?
        #Manually check the certificate after ignoring the gem's errors.
        #Fail for real if this check fails.
        xml_response =Nokogiri::XML( app_data )
        response_cert=xml_response.remove_namespaces!.css('X509Certificate').text.gsub(/\t/,'')
        valid_enough =response.settings.idp_cert.include?(response_cert)
        unless valid_enough
          error_string="After SAMLResponse failed the `ruby-saml` gem's validation, "+
          "a manual comparison of the certificate sent and the certificate we have on file "+
          "showed that they do not match. Please contact the identity provider promptly."
          raise error_string
        end
      end
      app_data=Hash.from_xml(app_data)['UserData']
      a_d_w_underscored_keys={}
      #This loop is needed because Rails doesn't automatically convert keys
      #from xml's default camelCase form to its own default underscored form.
      app_data.each{|k,v| a_d_w_underscored_keys[k.underscore]=v }
      app_data=a_d_w_underscored_keys

      #Since a password is required to create a new user account, a random password is generated.
      #Users can reset their passwords as needed once they've added an email to their account.
      pw=SecureRandom.base64(10)

      motorist_id=app_data.delete('id')

      motorist_user=User.find_by_motorist_id(motorist_id)
      motorist_user||=User.create(
        app_data.
        merge({
          login:        motorist_id,
          motorist_id:  motorist_id,
          tenant_id:    Enum::Tenant.id('bsb'),
          membership_id:Enum::Membership.id('Free'),
          parent_id:    User::PARENT_ID_FOR_MOTORIST_GROUP,
          password:pw,password_confirmation:pw,
          tos: true
        })
      )
      motorist_user.can!(:motorists_rtt_process,true)
      motorist_user.save
      UserSession.create(motorist_user)
      respond_to do |format|
        format.html {redirect_to '/dashboard'}
        format.all  {render js:"window.location='#{request.protocol}#{request.host}:#{request.port}/"}
      end
    rescue =>ex
      audit level:'error', body:{exception_class:ex.class.name, message:ex.message, backtrace:ex.backtrace}
      ExceptionNotifier.notify_exception(ex)
      error_msg="Authorization using Motorist Group didn't seem to work. Either login to their site first, or enter your Insureio credentials below."
      return redirect_to "/login?error=#{error_msg}"
    end
  end

end
