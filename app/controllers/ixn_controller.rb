class IxnController < ApplicationController
  before_filter :require_login, except: [:alq_save]
  before_filter :require_editable_case, only: [:esp, :generate_pdf]
  before_filter :require_viewable_case, only: [:xml, :pdf, :download]
  respond_to :json

  # Receive 'tickets' posted from IXN
  def create
    consumer = Ixn::IxnToPic.new(params).run current_user
    kase = consumer.cases.first
    consumer.save if consumer.valid? && Esp::Validation.new(kase).valid?
    respond_with consumer
  rescue Esp::XmlBuilder::ESPValidationError, Ixn::IxnToPic::DateError => ex
    canned_fail_response errors: ex.message
  rescue ActiveRecord::RecordNotFound => ex
    msg = ex.message.sub /\s*\[.*$/, ''
    canned_fail_response status: 404, errors: msg
  end

  # Receive POST from IXN's ALQ
  def alq_save
    agent = current_user || User.find(Enum::Tenant.find(10).default_parent_id)
    quote = Ixn::AlqToPic.new(params).run agent
    if quote.consumer.save
      status = quote.persisted? ? 200 : 201
      status = 422 unless quote.save
      respond_with quote, status: status
    else
      respond_with quote.consumer
    end
  rescue Ixn::AlqToPic::PermissionDeniedException
    permission_denied
  rescue ActiveRecord::RecordNotFound => ex
    msg = ex.message.sub /\s*\[.*$/, ''
    canned_fail_response status: 404, errors: msg
  end

  # Run download through this application instead of straight from AWS so that
  # we can apply content-disposition header to indicate 'download' behaviour
  def download
    type = params[:type] || 'pdf'
    signed_url = Esp::Aws.new(@kase).get_signed_url type
    bin = Net::HTTP.get URI.parse(signed_url)
    send_data bin, disposition: 'attachment', filename: "Insureio case #{@kase.id}.#{type}"
  end

  # Send case to ESP for underwriting
  def esp
    if Esp::Validation.new(@kase).valid?
      Esp.push_ticket @kase
      canned_success_response
    else
      canned_fail_response @kase.consumer
    end
  rescue Esp::XmlBuilder::ESPValidationError => ex
    canned_fail_response errors: ex.message
  rescue ActiveRecord::RecordInvalid => ex
    canned_fail_response ex
  end

  #Generate, upload, and provide aws url for pdf without sending to ESP.
  #Requires the policy to have a whole life duration.
  def generate_pdf
    if Esp::Validation.new(@kase).valid?
      Esp.generate_pdf_only @kase
      get_aws_url 'pdf'
    else
      canned_fail_response @kase.consumer
    end
  rescue Esp::XmlBuilder::ESPValidationError => ex
    canned_fail_response errors: ex.message
  rescue ActiveRecord::RecordInvalid => ex
    canned_fail_response ex
  end

  # Provide URL of PDF that was sent to ESP
  def pdf; get_aws_url 'pdf'; end

  # Provide URL of XML that was sent to ESP
  def xml; get_aws_url 'xml'; end

  private

  def get_aws_url format
    # `@kase` should be set by `require_viewable_case`
    url = Esp::Aws.new(@kase).get_signed_url format
    render json: {url: url}
  end
end
