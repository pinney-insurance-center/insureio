class Crm::TaskBuildersController < ApplicationController
  before_filter :require_login
  before_filter :require_view_access, only:[]
  respond_to :html, :js, :json

  def index
    owner_type =params[:owner_type]
    owner_id   =params[:owner_id]
    if params[:status_type_id]
      owner_type ='Crm::StatusType'
      owner_id   =params[:status_type_id]
    end
    return permission_denied unless owner_id.present? && owner_type.present?

    @task_builders = Crm::TaskBuilder.all
    @task_builders = @task_builders.where(owner_id: owner_id, owner_type: owner_type)
    @task_builders = @task_builders.where("label like ?","%#{params[:label]}%") if params[:label]
    if params[:includes]
      @task_builders = @task_builders.preload([:template,:assigned_to])
      @json_methods = [:assigned_to_name, :template_name]
      return render json:@task_builders.as_json(methods: @json_methods)
    end

    respond_with @task_builders
  end

  def assignee_candidates
    candidates = User.
      brand_id(params[:brand_id]).
      search_full_name(params[:term]).name_and_id_only
    render json: _json_for_autocomplete(candidates, :full_name)
  end

  def create
    @task_builder = Crm::TaskBuilder.new(params[:crm_task_builder])
    return permission_denied unless @task_builder.owner.try(:editable?, current_user)
    if @task_builder.save
      if request.format.json?
        respond_with @task_builder
      elsif @task_builder.owner_type == "Marketing::Campaign"
        redirect_to controller:'/marketing/campaigns', action:'task_builders', id:@task_builder.owner_id
      else
        status_type = @task_builder.status_type
        params[:status_type_id] ||= status_type.id
        @auto_system_task_rules = status_type.task_builders
        redirect_to params.merge({action:'index', status: 303})
      end
    elsif request.format.json?
      respond_with @task_builder
    else
      flash.now[:error] = (@task_builder.errors.full_messages.join('.<br/>')+'.<br/>').html_safe
      render nothing:true, layout:true, status: 422
    end
  end

  def destroy
    task_builder = Crm::TaskBuilder.find_by_id(params[:id])
    status_type_id = task_builder.status_type_id
    task_builder.destroy
    if request.format.json?
      respond_with task_builder
    else
      redirect_to action:'index', status: 303, status_type_id:status_type_id
    end
  end

  def update
    @task_builder = Crm::TaskBuilder.find(params[:id])
    attrs = params[:crm_task_builder] || params[:editable] || {params[:name] => params[:value]}
    if @task_builder.update_attributes(attrs)
      flash.now[:notice] = 'Auto system task rule was successfully updated.'
      status=200
    else
      flash.now[:error] = (@task_builder.errors.full_messages.join('.<br/>')+'.<br/>').html_safe
      status=422
    end
    if request.format.json?
      respond_with @task_builder
    else
      render nothing:true, layout:true, status: status
    end
  end

  private

  # This method mimics Rails3JQueryAutocomplete::Autocomplete.json_for_autocomplete
  def _json_for_autocomplete items, method, extra_data=[]
    items.collect do |item|
        hash = {"id" => item.id.to_s, "label" => item.send(method), "value" => item.send(method)}
        extra_data.each do |datum|
          hash[datum] = item.send(datum)
        end if extra_data
        # TODO: Come back to remove this if clause when test suite is better
        hash
      end
  end
end
