class Crm::CasesController < Crm::CrmBaseController
  respond_to :html, :js, :json

  prepend_before_filter -> { @ltd_auth_enabled = true }, only: [:show, :involved_parties]

  before_filter :require_viewable_case, :only => [
    :details, :exam, :health,
    :show,
    :status_history,
    :follow_up,
    :docusign,
    :admin,
  ]
  before_filter :require_editable_case, only: [:exam_update, :update]
  before_filter :require_consumer,      only: [:create, :index, :exam]
  before_filter :require_login

  def new
    @crm_case = Crm::Case.new()
  end

  def create
    @crm_case = @consumer.cases.build(params[:crm_case])
    @crm_case.agent_id=current_user.id
    @crm_case.active = true
    if @crm_case.save
      canned_success_response @crm_case, jopts: json_opts
    else
      canned_fail_response @crm_case
    end
  end

  def update
    # Normalise args for Crm::Case#update
    new_attrs = params[:crm_case].presence || {}.with_indifferent_access
    if key = params[:key] || params[:name]
      new_attrs[key] = params[:value]
    end
    # Get name of former agent if agent is to change
    if new_attrs[:agent_id] && @kase.agent_id != new_attrs[:agent_id]
      @prev_agent_name = @kase.agent.try(:name) || '(n/a)' # want this to be non-nil so that it indicates whether a Note should be created
    end
    if new_attrs[:agent_of_record_id] && @kase.agent_of_record_id != new_attrs[:agent_of_record_id]
      @prev_aor_name = @kase.agent_of_record.try(:name) || '(n/a)'
    end

    sanitize_params @kase, new_attrs

    return canned_fail_response('Your request must include which case attributes you wish to update.') unless new_attrs.present?
    return canned_fail_response(@kase) unless @kase.update_attributes(new_attrs)

    # Create note for agent change (if any)
    @kase.create_note_for_agent_change(current_user, @prev_agent_name) if @prev_agent_name
    # Create note for aor change (if any)
    @kase.create_note_for_aor_change(current_user, @prev_aor_name) if @prev_aor_name

    canned_success_response @kase.as_json, jopts: json_opts
  end

  def destroy
    crm_cases_ids = params[:case_ids].to_s.split(",") # Why is this not getting passed as an Array?
    crm_cases_ids.each do |crm_case_id|
      crm_case = Crm::Case.find_by_id(crm_case_id)
      if crm_case.editable?(current_user)
        crm_case.update_attributes(active: false)
      end
    end
    respond_to do |format|
      flash.now[:notice]='Successfully deleted'
      format.js { render nothing: true, layout: true }
      format.html {}
    end
  end

  def exam
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def exam_update
    if @kase && params.has_key?(:crm_case)
      if @kase.update_attributes params[:crm_case]
        flash[:notice] = 'Updated'
      else
        flash[:error] = 'Failed to Update'
      end
    elsif params.has_key?(:name) && params.has_key?(:value)
      if @kase.update_attributes(params['name'] => params['value'])
        flash[:notice] = 'Updated'
      else
        flash[:error] = 'Failed to Update'
      end
    else
      flash[:error] = 'Failed to Update'
    end
    redirect_to action:'exam', id:params[:id], container:params[:container]||'#inner-container'
  end

  def requirements
    @case = Crm::Case.find(params[:id]||params[:case_id])
    render json: @case.requirements
  end

  def index
    if request.format.html?
      return render 'public/crm/cases/container.html', layout:!params[:no_layout]
    end

    #Yes, this is a lot of models to preload. In future, most processing data should be moved to the cases table.
    models_to_preload=[:statuses,:quoting_details,:next_task,:docusign_case_data,:marketech_dataset]

    cases=@consumer.cases.order('created_at DESC').preload(models_to_preload)
    opportunities=@consumer.opportunities.includes(statuses:{}).order('created_at DESC').preload(:next_task)
    opps=opportunities.as_json({
      include:{ statuses:{}, next_task:{except:[:recipient]} },
      except:[:annual_premium,:monthly_premium,:quarterly_premium,:semiannual_premium]
    })
    render json:{
      data: opps+( cases.as_json(json_opts) ),
      opportunities_count:   opps.length,
      pending_policies_count:cases.select{|c| c['active'] && c['sales_stage_id']< Enum::SalesStage.id('Placed') }.length,
      placed_policies_count: cases.select{|c| c['active'] && c['sales_stage_id']>=Enum::SalesStage.id('Placed') }.length,
    }
  end

  def involved_parties
    kase = Crm::Case.preload({
      agent_name_only:[],
      agent_of_record_name_only:[],
      stakeholder_relationships:{stakeholder: [:phones,:emails,:addresses] }
    }).find_by(id:params[:id])
    return canned_fail_response(status: 404) unless kase
    return canned_fail_response(status: 403) unless kase.viewable?(current_user)

    all_contact_methods={
      include:[:phones, :emails, :addresses],
      except:[:encrypted_ssn,:encrypted_dln],
      methods:[:ssn,:has_opps_or_policies]
    }

    render json: kase.as_json({
      only:[:agent_id,:agent_of_record_id],
      methods:[:agent_name,:agent_of_record_name],
      include:{
        stakeholder_relationships:{include:{stakeholder:all_contact_methods}},
      }
    })
  end

  #!!DEPRECATED!!
  # Returns all case notes and client notes in a single array named "crm-notes"
  def notes
    logger.warn "DEPRECATED! Use NotesController\#index instead."
    return render_404 unless @case = Crm::Case.find_by_id(params[:id])
    @notes = @case.notes + @case.consumer.notes
    if @notable.try(:agency_works_id)
      @agency_works_notes = Processing::AgencyWorks::CommentsService.new(@case).request
      @notes += @agency_works_notes
    end
    @notes = @notes.sort_by{|e| e[:created_at] || Time.zone.now}.reverse
    respond_to do |format|
      format.xml {render xml:@notes}
      format.json { render json:@notes.as_json(methods: :creator_name) }
    end
  end

  def follow_up
    system_task = Task.get_earliest_task(@crm_case).first
    follow_up_value = system_task.try(:label) ? system_task.try(:label) :
                      system_task.try(:status).try(:status_type).try(:name)
    render text: follow_up_value, layout: true
  end

  def admin
    return permission_denied unless current_user.can?(:sales_support)
    #prepare the staff assignment partial
    if(@crm_case.staff_assignment)
      @staff_assignment=@crm_case.staff_assignment
    else
      @staff_assignment=@crm_case.create_staff_assignment(agent:@crm_case.agent)
      @crm_case.save
    end
    render 'admin', layout: (params[:no_layout] ? false : true)
  end

  def show
    return permission_denied unless @crm_case.viewable?(current_user)
    render json:@crm_case.as_json(json_opts)
  end

private

  def scope_by_client_search_criteria
    return unless params[:search_term] and params[:search_by]
    @crm_cases = 
    case params[:search_by]
    when "id"
      @crm_cases.where ["consumers.id = ?", Consumer.id_from_short_id(params[:search_term])]
    when "name"
      @crm_cases.merge Consumer.search_full_name params[:search_term]
    when "phone"
      @crm_cases.joins(consumer:[:phones]).where ["CONCAT( phones.value, 'ext.', phones.ext) LIKE ?", params[:search_term].gsub(/^|[\s\-\.\,[a-z]]+|$/,'%')]
    when "email"
      @crm_cases.joins(consumer:[:emails]).where ["email_addresses.value LIKE ?", params[:search_term].gsub(/^|$/,'%')]
    else
      @crm_cases
    end
  end

  #This method returns options for generating the JSON necessary to create/update the AngularJS policies GUI.
  def json_opts
    {
      include:{
        quoting_details:{except:[:annual_premium,:monthly_premium,:quarterly_premium,:semiannual_premium]},
        statuses:{}, next_task:{}, last_app_page_snapshot:{}, exam_one_case_data:{}, smm_status:{except:[:desc]}
      },
      methods:[
        :attachments_count,
        :app_page_snapshot_count,
        :eligible_for_processing_with_docusign,
        :eligible_for_processing_with_agency_works,
        :eligible_for_processing_with_igo,
        :eligible_for_processing_with_marketech,
        :eligible_for_processing_with_apps,
        :eligible_for_processing_with_apps_121,
        :eligible_for_processing_with_exam_one,
        :date_processing_started_with_docusign,
        :date_processing_started_with_agency_works,
        :date_processing_started_with_igo,
        :date_processing_started_with_marketech,
        :date_processing_started_with_apps,
        :date_processing_started_with_apps_121,
        :reasons_for_ineligibility_with_docusign,
        :reasons_for_ineligibility_with_agency_works,
        :reasons_for_ineligibility_with_igo,
        :reasons_for_ineligibility_with_marketech,
        :reasons_for_ineligibility_with_apps,
        :reasons_for_ineligibility_with_apps_121,
        :reasons_for_ineligibility_with_exam_one,
        :marketech_email_ready,
        :marketech_editable,
        :marketech_loggable,
        :marketech_esign_url,
        :marketech_edit_url,
        :marketech_pdf_url,
        :marketech_log_url,
        :docusign_envelope_id,
        :agency_works_id,
      ]
    }
  end

end
