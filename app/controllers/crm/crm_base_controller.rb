class Crm::CrmBaseController < ApplicationController
  include AuditLogger::Helpers

  before_filter :require_login

private

  def require_consumer
    require_login # current_user must be set before Consumer.find in case it has CROSS_TENANCY_PERMISSIONS
    return if performed?
    @consumer ||=
    if @crm_case || params[:case_id].present?
      @crm_case ||= Crm::Case.find_by(id: params[:case_id])
      @crm_case.try(:consumer)
    else
      Consumer.find_by(id: params[:consumer_id] || params[:id] || params[:client_id])
    end
    unless @consumer&.viewable?(current_user)
      permission_denied "You lack permission to view this resource #{@consumer&.id}"
    end
  end
end
