class Crm::LeadTypesController < ApplicationController
  before_filter :require_login

  respond_to :json

  def index
    @lead_types = current_user.can?(:super_view) ? Crm::LeadType.all : current_user.lead_types;
    @lead_types = @lead_types.joins(:brands).where('brands.id = ?', params[:brand_id]) if params[:brand_id]
    params[:q] ||= params[:text] || params[:name]
    @lead_types = @lead_types.where('text LIKE ?', '%'+params[:q]+'%') if params[:q].present?
    @lead_types = @lead_types.select(params[:select]) if params[:select].present?
    respond_with @lead_types.as_json(methods:[:brand_ids])
  end
end