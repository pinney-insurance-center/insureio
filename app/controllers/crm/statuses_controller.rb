class Crm::StatusesController < Crm::CrmBaseController
  #This controller needs refactoring wherever it is assumed that a status has a case, versus a user or an opportunity.

  before_filter :require_editable_crm_status, :only => [:update, :partial]
  before_filter :require_statusable, :only => [:show, :create]
  skip_before_filter :require_login, only:[:show,:index]

  respond_to :html, :js, :json

  def index
    unless params[:statusable_type].present? && params[:statusable_id].present?
      return permission_denied('Invalid request parameters. Please specify both the type and id of the statusable record.', status: 400)
    end
    @statuses=Crm::Status.includes(:status_type).where(statusable_type:params[:statusable_type], statusable_id:params[:statusable_id]).order('id DESC')
    render json: @statuses.as_json(include:{status_type:{include:[:status_type_category]}})
  end

  def create
    Rails.logger.level=0 #debug level. show everything!
    if @statusable.update_attributes(status_type_id:params[:status_type_id].to_i)
      @status=@statusable.status
    else
      return canned_fail_response @statusable
    end
    Task.find(params[:task_id]).update_column(:active, false) if params[:task_id].present?
    render json: @status.to_json
  end

  def show
    the_layout='application'

    if @crm_status
      return render(json:@crm_status) if request.format.json?
      @crm_system_tasks = @crm_status && @crm_status.tasks.order("due_at ASC").includes(:completed_by) || []
      @crm_system_tasks+= Task.evergreen.where('origin_type=? AND origin_id IN (?)',@crm_status.class.name,@crm_status.statusable.statuses)
      @crm_system_tasks = @crm_system_tasks.uniq

      @old_statuses =     @crm_status.statusable.statuses.includes(:status_type).reject{|x| x.id == @crm_status.id }
    end

    respond_to do |format|
      format.js {
        render 'show', layout:the_layout
      }
      format.html {
        render 'show', layout:the_layout
      }
    end
  end

  def partial
    @tasks = @crm_status && @crm_status.tasks.order("due_at DESC") || []
    render partial:'partial'
  end

  def update
    if params[:crm_status].present? && @crm_status.update_attributes(params[:crm_status])
      canned_success_response
    elsif params[:key].present? && params[:value].present? && @crm_status.update_attributes(params[:key]=>params[:value])
      canned_success_response
    else
      canned_fail_response
    end
  end

private

  def require_editable_crm_status
    @crm_status=Crm::Status.find_by_id(params[:id])
    @statusable=@crm_status.try(:statusable)
    return permission_denied('Cannot find status.', status: 404) unless @crm_status.present?
    return permission_denied("Permission denied for Status #{@crm_status}") unless @crm_status.try(:editable?, current_user)
  end

  def require_statusable
    type =params[:statusable_type]
    id   =params[:statusable_id]

    type, id='Crm::Case',  params[:case_id] if params[:case_id]
    type, id='User',       params[:user_id] if params[:user_id]

      @statusable =type.constantize.send(:find_by_id, id)
      @crm_status =@statusable.try(:status)
    return permission_denied('Cannot find statusable record.', status: 404) unless @statusable.present?
  end
end
