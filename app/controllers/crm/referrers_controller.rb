class Crm::ReferrersController < ApplicationController
  before_filter :require_login

  respond_to :json

  def index
    @referrers = current_user.referrers
    params[:q] ||= params[:text] || params[:name]
    @referrers = @referrers.where('text LIKE ?', '%'+params[:q]+'%') if params[:q].present?
    @referrers = @referrers.select(params[:select]) if params[:select].present?
    respond_with @referrers
  end
end
