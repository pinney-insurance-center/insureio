class Crm::FinancialInfosController < ApplicationController

  before_filter :_scrub_params, only:[:create, :update]
  respond_to :json

  #This controller renders no views, because financial info is only managed in the context of
  #the quote path or the consumer mgmt view, both of which use `public/quoter/finance.html`.

  # GET /crm/financial_infos/1.json
  def show
    @crm_financial_info = Crm::FinancialInfo.find(params[:id])

    render json: @crm_financial_info
  end

  # POST /crm/financial_infos.json
  def create
    @crm_financial_info = Crm::FinancialInfo.new(params[:crm_financial_info])
    if @crm_financial_info.save
      render json: @crm_financial_info, status: :created
    else
      render json: {errors: @crm_financial_info.errors}, status: :unprocessable_entity
    end
  end

  # PUT /crm/financial_infos/1.json
  def update
    @crm_financial_info = Crm::FinancialInfo.find(params[:id])
    @crm_financial_info.update_attributes(params[:crm_financial_info])
    if @crm_financial_info.errors.blank?
      canned_success_response @crm_financial_info
    else
      canned_fail_response @crm_financial_info
    end
  end

  private

  def _scrub_params
    #remove dollar signs and commas from financial info fields
    if params[:consumer] and params[:consumer][:financial_info_attributes]
      params[:consumer][:financial_info_attributes].each do |k,v|
        params[:consumer][:financial_info_attributes][k]=v.gsub(/[$,]/,'')
      end
    end
  end

end
