class Crm::StatusTypesController < Crm::CrmBaseController
  respond_to :html, :json

  before_filter -> { require_permission :create_status_types }, only:[:create]

  def index
    if request.format.html?
      return render 'public/crm/status_types/gui.html', layout: !params[:no_layout]
    end
    return permission_denied :user_status_rules_gui if params[:for_users] && !current_user.can?(:user_status_rules_gui)
    @status_types = Crm::StatusType::viewable_by(current_user)

    @status_types=@status_types.where(is_for_policies:!params[:for_users])

    if params[:status_type_category_id]
      @status_types=@status_types.where(status_type_category_id:params[:status_type_category_id])
    end
    @status_types=@status_types.preload(:owner_name_only) if params[:include_owner_name]

    params[:q]  ||= params[:name]
    @status_types = @status_types.where('LOWER(name) LIKE ?',"%#{params[:q].downcase}%") if params[:q].present?
    @status_types = @status_types.sorted

    @status_types = @status_types.excluding_global if current_user.exclude_global_status_types

    methods_to_include=[:note_to_users]
    methods_to_include<<:owner_name if params[:include_owner_name]
    render json: @status_types.as_json(methods:methods_to_include)
  end

  def new
    @status_types = Crm::StatusType.new
    @ownerships = Enum::Ownership.all
    respond_to do | format |
      format.js {}
    end
  end

  def create
    return permission_denied("Improperly formed parameters.", status: 400) unless params[:crm_status_type].present?
    attrs= _protect_owner_and_ownership
    if status=Crm::StatusType.create(params[:crm_status_type])
      canned_success_response status, nil, 'status rule'
    else
      canned_fail_response status_type
    end
  end

  def update
    status_type = Crm::StatusType.editable_by(current_user).where(id: params[:id]).first
    # Use name=value pair if supplied (as x-editable is wont to do)
    if params[:name].present? && params[:value].present? && !params[:crm_status_type]
      params[:crm_status_type] = {params[:name] => params[:value]}
    end
    return permission_denied("Improperly formed parameters.", status: 400) unless params[:crm_status_type].present?
    return permission_denied("Status rule does not exist or you lack access to edit it.") unless status_type.present?

    attrs= _protect_owner_and_ownership

    if status_type.update_attributes(attrs)
      canned_success_response
    else
      canned_fail_response status_type
    end
  end

  def destroy
    Crm::StatusType.find(params[:id]).destroy
    redirect_to action: "index", status: 303
  end

  def adjust_sort_order
    status_type = Crm::StatusType.editables(current_user).where(id:params[:id]).first
    return permission_denied unless status_type.present?
    if status_type.adjust_sort_order(params[:direction])
      canned_success_response
    else
      canned_fail_response status_type
    end
  end

  private

  def _protect_owner_and_ownership
    attrs=params[:crm_status_type]
    #Lambdas allow these checks to be separated out, named,
    #and still benefit from short-circuit evaluation.
    allowed_to_set_fields    =->{ current_user.can?(:edit_status_type_access) }
    attempts_to_set_owner    =->{ attrs[:owner_id].present? }
    attempts_to_set_ownership=->{ attrs[:ownership_id].present? }
    owner_is_not_current_user=->{ attrs[:owner_id]!=current_user.id }
    new_owner_is_editable    =->{ User.find(attrs[:owner_id]).editable?(current_user) }

    #When disallowed values are set, just overwrite them with the defaults.
    unless allowed_to_set_fields.call && attempts_to_set_ownership.call
      attrs[:owner_id]=Enum::Ownership.id('user and descendants')
    end
    unless allowed_to_set_fields.call && (attempts_to_set_owner.call && owner_is_not_current_user.call && new_owner_is_editable.call)
      attrs[:owner_id]=current_user.id
    end
    attrs
  end
end
