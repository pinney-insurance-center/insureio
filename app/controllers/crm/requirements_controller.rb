class Crm::RequirementsController < ApplicationController
  respond_to :html, :json

  before_filter :require_login
  before_filter -> { require_permission :edit_requirements }

  def create
    unless params[:requirement]
      return permission_denied "Missing parameters required to create a custom policy requirement."
    end
    requirement=Crm::CaseRequirement.new params[:requirement]
    if requirement.save
      canned_success_response requirement
    else
      canned_fail_response requirement
    end
  end

end