class Crm::SourcesController < ApplicationController
  before_filter :require_login

  respond_to :json

  def index
    @sources = current_user.lead_sources
    params[:q] ||= params[:text] || params[:name]
    @sources = @sources.where('text LIKE ?', '%'+params[:q]+'%') if params[:q].present?
    @sources = @sources.select(params[:select]) if params[:select].present?
    respond_with @sources
  end
end