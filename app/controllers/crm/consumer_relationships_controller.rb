class Crm::ConsumerRelationshipsController < ApplicationController
  respond_to :json

  def create
    if @relationship=Crm::ConsumerRelationship.create(params[:consumer_relationship])
      canned_success_response
    else
      canned_fail_response @relationship
    end
  end

  def update
    @relationship=Crm::ConsumerRelationship.find_by_id params[:id]||params[:pk]
    attrs= params[:consumer_relationship] || {}
    attrs[ params[:key]  ]=params[:value] if params[:key]  && params[:value]
    attrs[ params[:name] ]=params[:value] if params[:name] && params[:value]

    if @relationship.update_attributes params[:consumer_relationship]
      canned_success_response
    else
      canned_fail_response @relationship
    end
  end

  def destroy
    if @relationship=Crm::ConsumerRelationship.find_by_id(params[:id]).delete
      canned_success_response
    else
      canned_fail_response @relationship
    end
  end

private

  def canned_success_response *args, **opts
    if request.format.json?
      respond_with @relationship
    else
      super
    end
  end

  def canned_fail_response *args, **opts
    if request.format.json?
      respond_with @relationship
    else
      super
    end
  end

end
