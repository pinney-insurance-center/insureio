class SocketsController < ApplicationController
  before_filter :require_login
  before_filter :require_recipients
  before_filter -> {require_permission :socket_comm}

  def publish
    allowed_modes=%w[alert bulletin js]
    unless allowed_modes.include?(params[:mode])
      return render permission_denied('Mode parameter not in allowed list.', status: 400)
    end

    data=params[:mode]=='bulletin' ? {text:params[:content]} : params[:content]
    redis params[:mode], data
    render json:{}
  end

private

  def recipients
    @recipients ||= begin
      params[:recipients] ||=[]
      params[:recipients].compact!
      if current_user.can?(:super_view)
        params[:recipients]
      else
        users = User.where(id:params[:recipients])
        users.select!{ |u| u.id == current_user.id || u.viewable?(current_user) }
        users.map &:id
      end
    end
  end

  def redis mode, data
    $redis.publish(APP_CONFIG['socket']['redis_channel_for_socket'], {
      type: mode,
      recipients: recipients.map(&:to_i),
      data: data
    }.to_json)
  end

  def require_recipients
    unless current_user.can?(:super_view) || recipients.present?
      render json:{errors:'No valid recipients specified'}, status:406
      return false
    end
  end

end
