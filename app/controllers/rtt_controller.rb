class RttController < ApplicationController
  include AuditLogger::Helpers
  audit_logger_name 'rtt_process'

  respond_to :html, :js, :json
  layout:false

  # Actions for which authentication with :quoter_key should be permitted
  QUOTER_KEY_ACTIONS = [
    :agent_summary,
    :app_forms_only,
    :msg_resume_app,
    :msg_signature_required_owner,
    :msg_signature_required_owner_payer,
    :msg_signature_required_payer,
    :poll_for_underwriting_decision,
    :save,
    :save_and_sign_for_consumer,
    :stakeholder_decline,
    :stakeholder_sign,
    :verify_stakeholder,
  ]

  skip_before_filter :verify_authenticity_token
  skip_before_filter :require_referrer_host_match
  prepend_before_filter :require_policy, except: [
    :agent_summary,
    :index,
    :recent_apps,
    :save,
    :save_and_sign_for_consumer,
  ]
  prepend_before_filter :require_login, except: [ :index ] + QUOTER_KEY_ACTIONS
  prepend_before_filter :require_login_or_quoter_key, only: QUOTER_KEY_ACTIONS
  # The API for RTT specifies :agent_id, but it gets used as :id for
  # authentication on the following actions
  prepend_before_filter -> { @credentials = params.values_at(:agent_id, :key) }, only: QUOTER_KEY_ACTIONS
  around_filter :audit_input_and_output

  def index
    redirect_to "/rtt/?#{request.params.except('action','controller').to_query}", status: :moved_permanently
  end

  # Returns just enough data for the customer-facing 'agent summary' at the top of the quote path
  # Is protected by the requirement that the quoter_key match, since this action does not require login.
  def agent_summary
    user = current_user
    return permission_denied("Invalid agent.") unless user && user.ok_for_rtt_process?
    brand  = Brand.find_by_id(params[:brand_id])
    brand||= user.default_brand
    email = brand.primary_email || user.primary_email
    phone = brand.primary_phone || user.primary_phone
    output_obj={
      name:user.name,
      brand_name:brand.try(:name),
      thumb:brand.logo_url,
      phone:phone,
      email:email,
      license_disclaimer:user.license_info,
      state_ids:user.rtt_allowed_state_ids,
    }
    #If policy and person are provided, also provide info on that person's role in the policy.
    if params[:person_id] && params[:guid]
      @policy=Crm::Case.viewable_by(user).where(scor_guid:params[:guid]).first
      output_obj[:stakeholder_role]=get_stakeholder_role_names @policy, params[:person_id]
    end
    render json: output_obj
  end

  #This action is called by the autosave function of the rtt quoter,
  #As well as by the rtt widget.
  def save
    attrs=params[:consumer]
    c=attempt_save attrs
    if c.persisted?
      policy=c.cases.where(scor_guid:params[:guid]).first
      note=c.notes.select{|n| n.text.include?(Processing::Rtt::RTT_NOTE_PREFIX) }.last
      render json:{
        message:'Successfully saved data.',
        id:c.id,
        scor_guid:policy.scor_guid,
        policy_id:policy.id,
        #make sure the front end is always modifying only the latest details record, not the earliest.
        quoting_details_id:policy.quoting_details.last.id,
        address_id: c.primary_address_id,
        stakeholder_relationships: policy.stakeholder_relationships.as_json(only:[:id,:stakeholder_id],methods:[:full_name]),
        existing_policies: c.cases.where(is_a_preexisting_policy:true).as_json(only:[:id,:policy_number])
      }
    else
      canned_fail_response c
    end
  end

  def verify_stakeholder
    policy=@policy
    person=Consumer.find_by_id(params[:person_id])
    specified_person_is_the_consumer= person==policy.consumer
    verified=(
      person.present? &&
      (specified_person_is_the_consumer || policy.stakeholders.include?(person)) &&
      #2/3 values must be present, and those that are present must match.
      %w[birth zip phone].select{|k| params[k].present? }.length>=2 &&
      ( params[:birth].blank? || person.birth_or_trust_date.to_s==params[:birth] ) &&
      ( params[:zip].blank?   || person.addresses.map(&:zip).include?(params[:zip]) ) &&
      ( params[:phone].blank? || person.phones.map(&:value).include?( params[:phone].gsub(/\D/,'') ) )
    )
    if verified && policy.date_signed_by_consumer.present?
      secure_token=generate_secure_token policy
    end
    output_obj={ verified:verified, secure_token:secure_token||'' }
    if verified && specified_person_is_the_consumer
      output_obj[:saved_consumer_data]=Processing::Rtt.serialize_saved_data_for_rtt_consumer_resume_link policy
    end
    render json: output_obj
  end

  def stakeholder_sign
    policy=@policy
    session_msg='Your session appears to have expired. '+
      'Please use your secure link to verify your identity and start again.'
    return permission_denied(session_msg) unless params[:secure_token]==generate_secure_token(policy)

    person_id=params.require(:person_id)
    person=Consumer.find_by_id(person_id)
    if !policy.stakeholders.include?(:person)
      return canned_fail_response
    end
    attrs={}
    @policy.stakeholder_relationships.each do |sr|
      if sr.stakeholder_id==person_id
        attrs[:date_signed_by_owner]=Time.now if sr.is_owner
        attrs[:date_signed_by_payer]=Time.now if sr.is_payer
      end
    end
    if policy.update_attributes(attrs)
      msg='Your signature has been recorded.'
      if policy.eligible_for_processing_with_scor
        service_obj=Processing::Rtt::SubmitService.new policy
        service_obj.send_to_scor
        if service_obj.errors.empty?
          msg+='Submitted application for underwriting. '+ service_obj.warnings.join("\n")
        else
          msg+="Unable to start underwriting. #{service_obj.errors.full_messages.join('. ')}"
          #transfer despite the problem with underwriting
          service_obj.transfer_xml_file
        end
      else
        msg+="Still waiting for the following before we can start underwriting. #{policy.reasons_for_ineligibility_with_scor}"
      end
      canned_success_response
    else
      canned_fail_response
    end
  end

  def stakeholder_decline
    policy=@policy
    session_msg='Your session appears to have expired. '+
      'Please use your secure link to verify your identity and start again.'
    return permission_denied(session_msg) unless params[:secure_token]==generate_secure_token(policy)
    @policy.update_attributes(status_type_id: Crm::StatusType::PRESETS[:rtt_signer_declined][:id])
    role_names=get_stakeholder_role_names @policy, params[:person_id]
    note_text="Person #{params[:person_id]}, acting as #{role_names} for policy #{@policy.id}, declined to sign the application\."
    @policy.consumer.notes.create(text:note_text)
  end

  #This action
  #1)  Saves consumer signature,
  #2A) Sends messages to owner and payer if present.
  #2B) Otherwise, checks if auto-declined/disqualified from RTT process.
  #2C) Otherwise sends to SCOR for underwriting.
  #3A) If sending fails, then `transfer_xml_file` is called.
  #3B) Otherwise, later polls for underwriting result will call `transfer_xml_file`.
  def save_and_sign_for_consumer
    attrs=params[:consumer]
    attrs[:cases_attributes][0][:date_signed_by_consumer]=Time.now
    c=attempt_save attrs
    if !c.persisted?
      return canned_fail_response c
    end
    policy=c.cases.where(scor_guid:params[:guid]).first
    if params[:rtt_answers][:has_declined_substandard_or_pending_apps]
      #Do not send to SCOR when we know they are disqualified.
      policy.update_attributes(status_type_id:Crm::StatusType::PRESETS[:rtt_scor_auto_declined][:id])
      return render json:{message:'Successfully signed.'}
    end
    unless policy.eligible_for_processing_with_scor(true)
      return permission_denied "Cannot sign and submit for underwriting yet. #{policy.reasons_for_ineligibility_with_scor}"
    end
    if policy.owner.present? || policy.payer.present?
      policy.update_attributes(status_type_id: Crm::StatusType::PRESETS[:rtt_pending_signatures][:id])
      Processing::Rtt::deliver_stakeholder_emails policy
      render json:{message:'Successfully signed. Awaiting signatures from other parties.'}
    else
      service_obj=Processing::Rtt::SubmitService.new policy
      service_obj.send_to_scor
      if service_obj.errors.empty?
        render json:{message:'Successfully sent for processing '+ service_obj.warnings.join("\n")+' Awaiting a decision.'}
      else
        #transfer despite the problem with underwriting
        service_obj.transfer_xml_file
        canned_fail_response service_obj.as_json, 'submit application', 'underwriting'
      end
    end
  end

  #This action requires authentication, RTT admin permission, and a policy guid.
  #This action is not part of the normal process.
  #It is a manual action which may be taken by the user.
  def force_send_for_underwriting
    return permission_denied unless current_user.can?(:motorists_rtt_process_details)
    service_obj=Processing::Rtt::SubmitService.new @policy
    service_obj.send_to_scor
    if service_obj.errors.present?
      canned_fail_response service_obj.as_json, 'force send', 'policy'
    else
      canned_success_response service_obj.as_json, 'force send', 'policy'
    end
  end

  #This action requires authentication, RTT admin permission, and a policy guid.
  #This action is not part of the normal process.
  #It is a manual action which may be taken by the user.
  def force_transfer_xml
    return permission_denied unless current_user.can?(:motorists_rtt_process_details)
    service_obj=Processing::Rtt::SubmitService.new @policy
    service_obj.transfer_xml_file
    if service_obj.errors.present?
      canned_fail_response service_obj.as_json, 'transfer xml', 'policy'
    else
      canned_success_response service_obj.as_json, 'transfer xml', 'policy'
    end
  end

  #This action requires authentication, RTT admin permission, and a policy guid.
  #This action is not part of the normal process.
  #It is a manual action which may be taken by the user.
  def deliver_resume_app_email
    return permission_denied unless current_user.can?(:motorists_rtt_process_details)
    delivered=Processing::Rtt.deliver_resume_app_email @policy
    if delivered
      canned_success_response
    else
      render json: { errors: @policy.errors.full_messages }, status: 400
    end
  end

  #Requires basic auth and a policy id.
  #Polls SCOR if no decision is already indicated.
  #Returns a policy status and case notes or an error message.
  def poll_for_underwriting_decision
    policy=@policy

    rtt_decision_statuses=policy.statuses.select do |s|
      Crm::StatusType::RTT_SCOR_DECISION_STATUS_TYPE_IDS.include? s.status_type_id
    end
    decision_status=rtt_decision_statuses.last
    if decision_status.present?
      #Policy status already indicates approved/declined.
      #No need to contact SCOR again.
      #Polling service sets `carrier_health_class`, so use that to determine tobacco/risk classes.
      last_qda=policy.quoting_details.order('sales_stage_id ASC').last
      obj_to_return={
        io_status:decision_status,
        system_available:true,
        underwriting_completed:true,
        decision:'see status',
        tobacco_class_name:last_qda.carrier_health_class.match(/non-tobacco/i) ? 'NS' : 'SM',
        risk_class_name:last_qda.carrier_health_class.match(/standard/i) ? 'ST' : 'PF',
        quoting_details_id:last_qda.id
      }
      render json: obj_to_return
    else
      #Otherwise, poll SCOR, which will likely update the status.
      service_obj=Processing::Rtt::PollingService.new policy
      service_obj.check_underwriting_status
      if !service_obj.errors.present?
        if service_obj.underwriting_completed && service_obj.decision
          submit_service_obj=Processing::Rtt::SubmitService.new policy
          submit_service_obj.transfer_xml_file
        end
        obj_to_return=service_obj.as_json
        obj_to_return[:io_status]=policy.status;
        obj_to_return[:quoting_details_id]=policy.quoting_details.order('sales_stage_id ASC').last.id
        render json: obj_to_return
      else
        canned_fail_response service_obj.as_json, 'retrieve', 'underwriting decision'
      end
    end
  end

  #Requires basic auth.
  #Allows optional parameters min_time and max_time, both in ISO8601 format.
  #By default, chooses records created in the past 24 hours.
  #Returns a json object with keys for each status type name, pointing to arrays of policy ids.
  def recent_apps
    return permission_denied unless current_user.can?(:motorists_rtt_process_details)
    params[:min_time]=params[:created_at_gte] if params[:created_at_gte].present?
    params[:max_time]=params[:created_at_lte] if params[:created_at_lte].present?

    if params[:min_time].present?
      begin
        params[:min_time]=Time.parse(params[:min_time])
      rescue => e
        return permission_denied('Min time, if provided, must be a valid ISO8601 format datetime.', status: 400)
      end
    end
    if params[:max_time].present?
      begin
        params[:max_time]=Time.parse(params[:max_time])
      rescue => e
        return permission_denied('Max time, if provided, must be a valid ISO8601 format datetime.', status: 400)
      end
    end
    policies=Crm::Case.where('scor_guid IS NOT NULL')

    #handle filters
    if params[:min_time].present?
      policies=policies.where('crm_cases.created_at>=?',params[:min_time])
    end
    if params[:max_time].present?
      policies=policies.where('crm_cases.created_at<=?',params[:max_time])
    end
    if params[:include_tests]=='Exclude Tests'
      policies=policies.where(is_test:false)
    elsif params[:include_tests]=='Tests Only'
      policies=policies.where(is_test:true)
    end
    #handle filter by status created range
    #handle filter by current status id
    #handle filter by agent id

    #handle order_column and order_direction params...
    direction = params.fetch(:order_direction, 'DESC')
    column=params.fetch(:order_column, 'id')
    policies=case column
    when 'consumer_name'
      column='consumers.full_name'
      policies.joins(:consumer)
    when 'status_created'
      column='status.created_at'
      policies.joins_current_status
    when 'status_name'
      column='status.status_type_name'
      policies.joins_current_status
    when 'agent_id'
      column='crm_cases.agent_id'
      policies
    else
      policies
    end
    policies=policies.order("#{column} #{direction}")
    policies=policies.preload(:statuses, :consumer, :agent_name_only )
    policies=policies.as_json(
      only:[:id,:scor_guid,:consumer_id,:agent_id,:created_at,:sales_stage_id,:entered_stage_submitted_to_carrier,:is_test],
      methods:[:agent_name,:consumer_name],
      include:{
        consumer:{ only:[:test_lead]},
        statuses:{only:[:created_at],methods:[:status_type_name]}
      }
    )
    policies.map{|p| p[:status]=(p[:statuses]||[]).last }
    render json: {cases:policies, count:policies.length, page:1, page_count:1}

  end

  #Requires basic auth and a policy id.
  #Returns an XML document with embedded base-64 encoded pdf document.
  def app
    policy=@policy

    unless policy.eligible_for_processing_with_scor
      return permission_denied "Cannot generate an xml document yet. #{policy.reasons_for_ineligibility_with_scor}"
    end
    #need to generate a hash from case here...
    xml=Processing::Rtt::SubmitService.new(policy).prepare_output_for_motorists
    render xml:xml, layout:false
  end

  #Requires basic auth and a policy id.
  #Returns a PDF or HTML document.
  def app_forms_only
    policy=@policy

    unless policy.eligible_for_processing_with_scor(true)
      request.format= :json
      return permission_denied "Cannot generate a pdf document yet. #{policy.reasons_for_ineligibility_with_scor}"
    end

    service_obj=Processing::Rtt::SubmitService.new policy
    respond_to do |format|
      format.pdf{
        #need to generate a hash from case here...
        pdf=service_obj.send(:compose_rtt_pdf)
        opts={type: 'application/pdf', disposition:'inline', status:200}
        send_data pdf, opts
      }
      format.html {
        @policy             =policy
        @consumer           =policy.consumer
        @scor_health_answers=service_obj.scor_health_answers
        @rtt_answers        =service_obj.rtt_answers
        @license_number     =policy.agent.licenses.in_effect.where(state_id: @consumer.primary_address.state_id ).first.try(:number)
        @contract_number    =policy.agent.contracts.where(carrier_id: Carrier::ID_FOR_MOTORISTS ).first.try(:carrier_contract_id)
        @agent_signature, @consumer_signature, @consumer_full_signature, @owner_signature, @payer_signature = service_obj.send(:compose_signatures)
        render 'forms_wrapper.html'
      }
    end
  end

  #These actions are so that the message can be viewed in browser
  #if stakeholder has trouble viewing html messages through their email client.
  def msg_signature_required_owner
    @stakeholder=@policy.owner
    @msg_view_url=""
    query_params={
      agent_id:  @policy.agent_id,
      key:       @policy.agent.quoter_key,
      brand_id:  @policy.consumer.brand_id,
      guid:      @policy.scor_guid,
      person_id: @stakeholder.id
    }
    query_str=query_params.to_query
    @link_url=Processing::Rtt::STAKEHOLDER_SIGN_LINK_PATH+query_str
  end

  def msg_signature_required_owner_payer
    @stakeholder=@policy.owner
    @msg_view_url=""
    query_params={
      agent_id:  @policy.agent_id,
      key:       @policy.agent.quoter_key,
      brand_id:  @policy.consumer.brand_id,
      guid:      @policy.scor_guid,
      person_id: @stakeholder.id
    }
    query_str=query_params.to_query
    @link_url=Processing::Rtt::STAKEHOLDER_SIGN_LINK_PATH+query_str
  end

  def msg_signature_required_payer
    @stakeholder=@policy.payer
    @msg_view_url=""
    query_params={
      agent_id:  @policy.agent_id,
      key:       @policy.agent.quoter_key,
      brand_id:  @policy.consumer.brand_id,
      guid:      @policy.scor_guid,
      person_id: @stakeholder.id
    }
    query_str=query_params.to_query
    @link_url=Processing::Rtt::STAKEHOLDER_SIGN_LINK_PATH+query_str
  end

  def msg_resume_app
    @msg_view_url=""
    query_params={
      agent_id:  @policy.agent_id,
      key:       @policy.agent.quoter_key,
      brand_id:  @policy.consumer.brand_id,
      guid:      @policy.scor_guid,
      person_id: @policy.consumer.id,
      resume_app:true
    }
    query_str=query_params.to_query
    @link_url=Processing::Rtt::CONSUMER_RESUME_LINK_PATH+'?'+query_str
  end

  #This is for the agent's reference, in case they want to go beyond
  #viewing rate class and decision, since SCOR sometimes provides more in-depth info.
  def full_scor_underwriting_response
    service_obj=Processing::Rtt::PollingService.new @policy
    service_obj.check_underwriting_status
    if !service_obj.errors.present?
      render json: service_obj.response_hash
    else
      canned_fail_response service_obj.as_json, 'retrieve', 'underwriting response'
    end
  end

  private

  #This method makes logging the policy id simpler.
  def id
    params[:id] || params[:guid]
  end

  #This method is called via an after filter on all requests to this controller.
  #Collect minimal data for success and exception responses.
  #(Service objects are expected to collect data on any exceptions.)
  #Collect more verbose data on error (4xx) responses.
  def audit_input_and_output
    begin
      yield#run the action
    rescue => ex
      @exception="#{ex.class.name} - #{ex.message}\n#{ex.backtrace}"
      ExceptionNotifier.notify_exception(ex)
      #After notifying devs, do what would have been done otherwise.
      raise ex
    ensure
      data_of_interest={action: params[:action], path: request.path, response_status: response.status}
      params=request.params.except(:controller,:action,:format)
      if params[:id] || params[:guid]
        data_of_interest[:guid]=params[:id] || params[:guid]
      end
      data_of_interest[:exception]=@exception if @exception
      if response.status>=400 && response.status<500#4xx statuses only
        data_of_interest[:params]=request.params
        rb= JSON.parse(response.body) rescue response.body
        data_of_interest[:response_body]=rb 
      end
      audit level:(response.status>=400 ? 'error' : 'info'), body: data_of_interest
    end
  end

  #This token will ensure that the process cannot be
  #reverse engineered to manipulate or view data without authorization.
  #Stakeholders will get a secure token upon verifying their identity.
  #It will be stored on the front end during their session,
  #and will be checked for validity when they attempt to
  #save payment info or sign/decline.
  def generate_secure_token policy
    initialization_vector=policy.date_signed_by_consumer.iso8601
    string="#{request.remote_ip} #{Date.today.iso8601} #{params[:id]}"
    scor_env=(Rails.env=='production' && !policy.is_test) ? 'production' : 'test'
    encrypted_value=Encryptor.encrypt(
      algorithm: 'aes-256-gcm',
      value: string,
      key: APP_CONFIG['scor'][scor_env]['signature_crypt_key'],
      iv: initialization_vector
    )
    Base64.strict_encode64(encrypted_value)
  end

  def get_stakeholder_role_names policy, stakeholder_id
    return nil unless policy.present? && policy.stakeholder_ids.include?(params[:person_id].to_i)
    roles=[]
    policy.stakeholder_relationships.each do |sr|
      if sr.stakeholder_id==params[:person_id].to_i
        roles<< 'Owner' if sr.is_owner
        roles<< 'Payer' if sr.is_payer
      end
    end
    role_names=roles.uniq.join(' and ')
  end

  def require_policy
    msg="It does not appear you are using a valid link."
    agent = current_user
    unless agent && agent.ok_for_rtt_process?
      request.format= :json
      return permission_denied(msg)
    end
    @policy=Crm::Case.where(scor_guid:params[:id]).first
    unless @policy.present?
      logger.warn "No valid policy with that guid."
      request.format= :json
      return permission_denied(msg)
    end
    unless agent.can?(:motorists_rtt_process_details) || @policy.viewable?(agent)
      logger.warn "No valid policy owned by agent."
      request.format= :json
      return permission_denied(msg)
    end
  end

  def replace_state_name_w_id attrs
    if attrs[:state].present?
      s_id=Enum::State.id(attrs[:state])
      attrs.delete(:state)
      addr_list=attrs[:addresses_attributes]||=[{}]
      addr_obj=addr_list[0] || addr_list.is_a?(Hash) && addr_list['0']
      if addr_obj.blank?
        addr_obj=addr_list[0]||={}
      end
      addr_obj[:state_id]=s_id
    end
    attrs
  end

  #Called from actions `save` and `save_and_sign_for_consumer`.
  #Attempts a recursive call if `scor_guid` is not unique.
  #Generates and delivers xml file to Motorists (now Encova) if
  #this save will set the status type to be in force.
  def attempt_save c_attrs
    note_text=Processing::Rtt::RTT_NOTE_PREFIX+
      JSON.pretty_generate({
        scor_health_answers: params[:scor_health_answers],
        rtt_answers:         params[:rtt_answers]
      })
    #Just in case these fields get through front end filter.
    #Not sure how they would but it was observed in testing.
    %w[birth_day birth_month birth_year].each{|key| c_attrs.delete key }
    c_attrs[:ip_address]||=request.remote_ip
    c_attrs=replace_state_name_w_id c_attrs
    ps=c_attrs[:cases_attributes]
    p_attrs=(ps[0]||ps['0'])
    p_attrs[:scor_guid]||=params[:guid]
    if c_attrs[:id]
      c=Consumer.find(c_attrs[:id])
      note_id=c.notes.select{|n| n.text.include?(Processing::Rtt::RTT_NOTE_PREFIX) }.last.try(:id)
      c_attrs[:notes_attributes]=[{text:note_text, id:note_id}] if note_id
      qdas=p_attrs[:quoting_details_attributes]
      qda=qdas[0]||qdas['0']
      if qda[:modal_premium] && !p_attrs[:date_signed_by_consumer] && !p_attrs[:status_type_id]
        p_attrs[:status_type_id]= Crm::StatusType::PRESETS[:rtt_app_quoted][:id]
      end
    else
      c_attrs[:notes_attributes]=[{text:note_text}]
      p_attrs[:status_type_id]= Crm::StatusType::PRESETS[:rtt_app_started][:id]
      qdas=p_attrs[:quoting_details_attributes]
      qda=qdas[0]||qdas['0']
      qda[:health_class_id]=Enum::TliHealthClassOption.id('Preferred')
      qda[:carrier_health_class]='Preferred'
      c=Consumer.new
    end
    #Remove any attrs hashes for already destroyed records.
    #Sometimes an array in params can be turned into a hash with string keys like "0","1".
    if c_attrs[:cases_attributes].is_a? Array
      c_attrs[:cases_attributes].reject do |v|
        v[:_destroy] && v[:id].present? && !Crm::Case.find_by_id( v[:id] )
      end
    else
      c_attrs[:cases_attributes].reject do |k,v|
        v[:_destroy] && v[:id].present? && !Crm::Case.find_by_id( v[:id] )
      end
    end
    c.assign_attributes c_attrs
    c.save
    if c.errors.full_messages.length==1 && c.errors.full_messages[0]=="Cases scor guid has already been taken"
      p_attrs[:scor_guid]=params[:guid]=SecureRandom.uuid
      attempt_save c_attrs
    end
    if p_attrs[:status_type_id]==Crm::StatusType::PRESETS[:rtt_in_force][:id]
      policy=c.cases.where(scor_guid:p_attrs[:scor_guid]).first
      service_obj=Processing::Rtt::SubmitService.new policy
      service_obj.transfer_xml_file
    end
    c
  end

end