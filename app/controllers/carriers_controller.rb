class CarriersController < ApplicationController
  respond_to :json
  before_filter ->{ require_permission :super_edit }, except:[:show,:index]
  
  def show
    @carrier = Carrier.find params[:id]
    respond_with @carrier
  end

  def index
    respond_with Carrier.all
  end

  def create
    return permission_denied("Required parameters are missing.") unless params[:carrier]
    c=Carrier.new( params[:carrier] )
    if c.save
      canned_success_response
    else
      canned_fail_response c
    end
  end

  def update
    return permission_denied("Required parameters are missing.") unless params[:carrier]
    c=Carrier.find_by_id params[:id]
    return permission_denied("Could not find a record with the given id in order to update.", status: 404) unless c
    if c.update_attributes(params[:carrier])
      canned_success_response
    else
      canned_fail_response c
    end
  end

end
