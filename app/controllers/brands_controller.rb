class BrandsController < ApplicationController

  before_filter :require_login
  before_filter -> {return permission_denied unless current_user.can_any?(:user_gui_brands,:brand)}, only:[:create, :new, :update, :destroy, :index]
  before_filter -> {require_permission :brand_multiple}, only:[:create, :new, :destroy]
  before_filter -> {require_permission :brand_assignments}, only:[:update_assignments, :assignments]
  before_filter :owner, only:[:new]
  before_filter :require_editable, only:[:destroy, :edit, :update]
  before_filter :require_editable_owner, only:[:destroy, :edit, :update]

  respond_to :html, :js, :json
  DEFAULT_PER_PAGE = 15

  def agents
    @agents  = brand.members.agents
    # Handle special case of being called by lead distribution view
    if params[:lead_types] and request.format.json?
      render :json => @agents.as_json({lead_types:params[:lead_types]})
    else
      respond_with @agents
    end
  end

  def lead_type_ids
    render json:brand.lead_type_ids
  end

  def member_ids
    render json:brand.member_ids
  end

  def create
    attrs=params[:brand]
    attrs[:owner_id] = owner.id
    @brand = Brand.new()
    @brand.attributes=params[:brand]
    if @brand.save
      canned_success_response
    else
      canned_fail_response @brand
    end
  end

  def update
    attrs=params[:brand] || {params[:name]=>params[:value]}
    if @brand && @brand.update_attributes( attrs )
      canned_success_response
    else
      canned_fail_response @brand
    end
  end

  def assignments
    return render 'public/brands/assignments_gui.html', layout:!params[:no_layout]
  end

  def autocomplete
    @brands = current_user.indexable_brands.select(['brands.id', :ownership_id,:full_name]).search_full_name(params[:name])
    @brands = @brands.joins(:members).where('users.id = ?', params[:member_id]) if params[:member_id].present?
    hashes = @brands.all.map{|p| {id:p.id, name:p.full_name, ownership_id:p.ownership_id}}
    render json:hashes
  end

  def show
    if request.format.html?
      return redirect_to action: :index, brand_id:params[:id]
    end
    @brand = current_user.indexable_brands.find(params[:id])
    render json:@brand.as_json(include:[:phones, :emails, :addresses,:webs],methods:[:logo_url,:header_url])
  end

  def edit
    redirect_to action: :index, brand_id:params[:id]
  end

  def index
    return render 'public/brands/gui.html', layout:!params[:no_layout] if request.format.html?

    owner.default_brand#finds the explicitly specified default, or finds the first available brand, or creates a brand
    @brands = owner.indexable_brands.includes([:phones])
    if params[:paginate]
      @brands = @brands.paginate(page: params[:page], per_page: DEFAULT_PER_PAGE).
        preload(:addresses,:emails,:phones,:owner_name_only,:initial_status_type,:logo_file,:header_file)
      return  render json:{
          brands:     @brands.as_json(
                          methods:[:logo_url,:header_url],
                          include:[:phones, :emails, :addresses,:webs,{ initial_status_type:{only:[:id,:name]} } ],
                        ),
          page:         @brands.current_page,
          page_count:   @brands.total_pages
        }
    elsif params[:just_the_basics]
      @brands = @brands.preload(:phones,:logo_file,:header_file)
      return render json: @brands.as_json({
        only:[:id,:owner_id],
        methods:[:name,:logo_url,:header_url,:preferred_insurance_division_subdomain],
        include:[:primary_phone]
      })
    elsif params[:name]
      @brands=@brands.name_like(params[:name]).name_and_id_only.all
      @brands=@brands.map{|p| {id:p.id, name: p.name} } if params[:select] && params[:select].include?('name')
      render json: @brands.to_json
    end
  end

  def destroy
    if brand.destroy
      flash[:notice] = "Deleted Successfully."
    else
      flash[:error] = "Failed to delete Branding."
    end
    return render(json:{}) if request.format.json?
    redirect_to :action => :index
  end

  def update_assignments
    logger.info "The parameters:\n#{params.pretty_inspect}"
    add_list   =[]
    remove_list=[]

    if params[:joins].present?
      params[:joins].each do |u_id, p_id,value|
        if value.to_i==1
          add_list    << "(#{u_id},#{p_id})"#[u_id,p_id]#add_membership u, p
        else
          remove_list << "(user_id=#{u_id} AND brand_id=#{p_id})"
        end
      end
      #direct sql query cuts page load time significantly
      if remove_list.present?
        ActiveRecord::Base.connection.execute("DELETE FROM `brands_users` WHERE #{remove_list.join(' OR ')}")
      end
      if add_list.present?
        ActiveRecord::Base.connection.execute("INSERT IGNORE INTO `brands_users` (`user_id`, `brand_id`) VALUES #{add_list.join(',')}")
      end
    end

    render json:{msg:"Successfully updated"}
  end

private

  def owner(no_default_to_current_user=false)
    return @owner if @owner
    @owner ||= User.find(params[:owner_id]) if (params[:owner_id] ||= params[:user_id]).present?
    @owner ||= current_user unless no_default_to_current_user
    @owner
  end

  def brand
    @brand ||=  Brand.find(params[:id])
  end

  def require_editable
    permission_denied("Not allowed to edit given brand") unless brand.editable?(current_user)
  end

  def require_editable_owner
    permission_denied("You cannot edit a brand belonging to a user whom you cannot edit") unless owner == current_user || owner.editable?(current_user)
  end

end
