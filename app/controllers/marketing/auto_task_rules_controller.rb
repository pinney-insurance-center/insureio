class Marketing::AutoTaskRulesController < ApplicationController
  
  before_filter :require_login

  def index
    @marketing_task_builders = Marketing::TaskBuilder.order("id DESC")

    respond_to do |format|
      format.js {render :layout => false} 
    end
  end

  def new
    @marketing_task_builder = Marketing::TaskBuilder.new
    respond_to do |format|
      format.js
    end
  end

  def edit
    @marketing_task_builder = Marketing::TaskBuilder.find(params[:id])
    
    respond_to do | format |  
      format.js {render :layout => false}  
    end
  end

  def create
    @marketing_task_builder = Marketing::TaskBuilder.new(params[:marketing_task_builder])

    respond_to do |format|
      if @marketing_task_builder.save
         @marketing_task_builders = Marketing::TaskBuilder.order("id DESC")
        format.js {render action: "index" } 
      else
        format.js { render action: "new" }
      end
    end
  end

  def update
    @marketing_task_builder = Marketing::TaskBuilder.find(params[:id])

    respond_to do |format|
      if @marketing_task_builder.update_attributes(params[:marketing_task_builder])
         @marketing_task_builders = Marketing::TaskBuilder.order("id DESC")
        format.js {render :layout => false}
      else
        format.js { render action: "edit" }
      end
    end
  end

  def destroy
    @marketing_task_builder = Marketing::TaskBuilder.find(params[:id])
    @marketing_task_builder.destroy
    @marketing_task_builders = Marketing::TaskBuilder.order("id DESC")

    respond_to do |format|
      format.js {render :layout => false}
    end
  end
end
