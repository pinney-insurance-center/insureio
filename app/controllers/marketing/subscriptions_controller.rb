class Marketing::SubscriptionsController < ApplicationController
  before_filter :require_login
  before_filter :require_person, except:[:create, :destroy,:show]

  def index
    #needs to return the list of subscriptions for a person, grouped by queue_num and ordered by order_in_queue
    subscriptions=@person.subscriptions
    if params[:with_completed_tasks]
      #This includes any subscriptions with completed tasks,
      #including ones that completed, ones currently running, and ones that were started but then moved backward in the queue.
      subscriptions=subscriptions.preload(:tasks).
      reject{|s| s.tasks.complete.count<1 }.
      as_json(methods:[:campaign_name], include:{tasks:{methods:[:assigned_to_name,:completed_by_name]} })
      subscriptions.each{|s| !s[:tasks] || s[:tasks].reject{|t| t[:completed_at].blank? } }
    else
      subscriptions=subscriptions.where('queue_num IS NOT NULL').
      select('id, campaign_id, queue_num, order_in_queue, active').
      order('queue_num ASC, order_in_queue ASC').as_json
    end
    campaign_names=Marketing::Campaign.select('id, name').order(:name).as_json(only:[:id,:name])
    render json: {subscriptions: subscriptions, campaignNames: campaign_names}
  end

  def create
    params[:marketing_subscription][:person_type] = 'User' if params.dig(:marketing_subscription, :person_type) == 'Recruit'
    if s=Marketing::Subscription.create(params[:marketing_subscription])
      canned_success_response s
    else
      canned_fail_response s
    end
  end

  def update_order_for_queue
    Marketing::Subscription.update_order_for_queue @person, params[:queue_num].to_i, params[:new_order]

    flash.now[:notice]="Subscription queue successfully reordered."
    render nothing: true, layout: true
  rescue
    flash.now[:error]="Could not reorder the subscription queue.  Please contact support for assistance."
    render nothing: true, layout: true, status: 500
  end

  def destroy
    s=Marketing::Subscription.find_by_id(params[:id])
    if s.try(:destroy)
      render json: {}
    else
      m="Could not destroy subscription. #{s.try(:errors).try(:full_messages)}"
      render json: {error: m}
    end
  end

  #displays completed tasks for a subscription
  def show
    s=Marketing::Subscription.find_by_id(params[:id])
    canned_fail_response("Could not find the subscription specified.", status: 404) unless s.present?
    @tasks=s.tasks.complete
    render
  end

  private

  def require_person
    return canned_fail_response("A person id and type are required for that action.") unless params[:person_type].present? && params[:person_id].present?

    effective_person_type=params[:person_type].to_s.sub('Recruit','User')
    @person=effective_person_type.constantize.find_by_id(params[:person_id])

    return canned_fail_response("Could not find the person specified.", status: 404) unless @person.present?
    return permission_denied unless @person.editable?(current_user)
  end

end