class Marketing::CampaignsController < ApplicationController
  before_filter :require_login
  respond_to :html, :js, :json

  def index
    if request.format.html?
      return render 'public/sequences/index.html', layout:!params[:no_layout]
    end

    fields_to_select=params[:select]
    scope = Marketing::Campaign.all.order(:name)
    if fields_to_select.present?
      fields_to_select+=[:id,:owner_id]
      scope= scope.select(fields_to_select)
    end
    scope = scope.viewable_by(current_user)
    scope = scope.where(params[:campaigns])                                 if params[:campaigns]
    scope = scope.where(product_category_id:params[:product_category_id])   if params[:product_category_id].present?
    scope = scope.where(planning_category_id:params[:planning_category_id]) if params[:planning_category_id].present?
    scope = scope.where('name LIKE ?', "%#{params[:name]}%")                if params[:name].present?
    scope = scope.where('id=?', params[:id])                                if params[:id].present?
    # Limit scope according to User's permissions
    scope = scope.where('NOT (ownership_id=?)', Enum::Ownership.id('global')) unless current_user.can?(:marketing_campaigns_canned)
    scope = scope.where('NOT (ownership_id!=?)', Enum::Ownership.id('global')) unless current_user.can?(:marketing_campaigns_custom)
    if params[:for_autocomplete]
      return render json: scope.select('id, name AS text').as_json
    end
    campaigns = scope.paginate page:params[:page], per_page:(params[:per_page] || 20)
    render json:{
      campaigns: campaigns.as_json(methods:[:owner_name]),
      total_pages: campaigns.total_pages,
      current_page: campaigns.current_page
    }
  end

  def new
    @marketing_campaign = Marketing::Campaign.new
    @ownerships = Enum::Ownership.all
    respond_with @marketing_campaign
  end

  def create
    marketing_campaign = Marketing::Campaign.create(params[:marketing_campaign].merge(owner_id: current_user.id))
    if request.format.json?
      render json:marketing_campaign.as_json(methods:[:owner_name])
    else
      redirect_to action:'index', status: 303
    end
  end

  def update
    @marketing_campaign = Marketing::Campaign.find(params[:id])
    attrs = params[:marketing_campaign] || params[:editable] || {params[:name] => params[:value]}
    if @marketing_campaign.update_attributes(attrs)
      if request.format.json?
        respond_with @marketing_campaign
      else
        canned_success_response
      end
    elsif request.format.json?
      render json:@marketing_campaign.as_json(methods:[:owner_name])
    else
      canned_fail_response @marketing_campaign
    end
   end

  def destroy
    @campaign = Marketing::Campaign.find(params[:id]).destroy
    if request.format.json?
      respond_with @campaign
    else
      redirect_to action:'index', status: 303
    end
  end

  # Just used for returning a partial to the Campaigns index page
  def task_builders
    @campaign = Marketing::Campaign.find params[:id]
    @task_builders = @campaign.task_builders
    append_view_path 'views/crm/task_builders'
    def self._prefixes; super << 'crm/task_builders'; end # Hack to include a partials path
    render 'task_builders', locals:{owner:@campaign}, layout:false
  end

  def subscribe_consumers
    unless params[:filtered_consumer_ids].is_a?(Array) && params[:filtered_consumer_ids].present?
      return permission_denied('No consumers were specified. Did not enroll anyone.', status: 400)
    end
    unless params[:id] && @campaign = Marketing::Campaign.find_by_id(params[:id])
      return permission_denied('There is no campaign specified. Did not enroll anyone.', status: 400)
    end
    enroll_summary = {}
    @campaign = Marketing::Campaign.find params[:id]
    enroll_summary = @campaign.associate_consumers(params[:filtered_consumer_ids], params[:created_via_report_id]) if @campaign.valid?
    respond_to { |format|
      format.json { render :json => enroll_summary.to_json }
    }
  end

end
