class Marketing::MessagesController < ApplicationController
  skip_before_filter :require_login, only: [:show]
  before_filter :cross_reference_identity_for_message_links, only: [:show]

  respond_to :html, :json

  def show
    render text: @message.body.to_s.html_safe, format: :html, layout:false
  end
end
