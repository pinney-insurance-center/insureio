class Marketing::Email::MessagesController < ApplicationController
  before_filter      :require_login
  skip_before_filter :require_login,     only: [:tracking_pixel, :log_and_redirect]

  def tracking_pixel
    # Don't track requests coming from users logged in to DataRaptor. Those will be users viewing sent emails.
    # Consumers viewing the message in browser after clicking the link from their email client
    # will still not be logged in, and therefor will still be tracked.
    unless current_user
      message=Marketing::Email::Message.find(params[:id])
      log_email_event_with_ahoy "Opened Email", message
      #Increment viewed count. Doing this here because rails :counter_cache does not allow conditions,
      #and because this is the only action which should update that count,
      #so a before_save callback on Ahoy::Event or conditional block in Ahoy::Store#track_event would seem harder to follow.
      message.update_attributes(viewed:message.viewed+1)
      message.person.update_column(:last_contacted,Time.now) if message.person
    end
    the_file=open("#{Rails.root}/public/images/1x1_transparent.gif").read 
    send_data the_file, filename: 'tracking_pixel.gif', type: 'image/gif', disposition: 'inline'
  end

  # Called from links within emails.
  def log_and_redirect
    # Don't track requests coming from users logged in to DataRaptor. Those will be users viewing sent emails.
    # Consumers viewing the message in browser after clicking the link from their email client
    # will still not be logged in, and therefor will still be tracked.
    unless current_user
      message=Marketing::Email::Message.find(params[:id])
      log_email_event_with_ahoy "Followed External Link", message
    end
    if params[:url].blank?
      return render text:'Something went wrong. Please contact your insurance agent or Insureio support for help.'
    end
    redirect_to params[:url]
  end

  private

end
