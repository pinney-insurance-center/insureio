class Marketing::TemplatesController < ApplicationController
  before_filter -> {require_permission :marketing}, except: [:tracking_pixel, :log_and_redirect, :show]

  respond_to :json, :js, :html

  @model = Marketing::Template

  def index
    if request.format.html?
      return render 'public/marketing/templates/container.html', layout:!params[:no_layout]
    end
    _prepare_index
  end

  def _prepare_index
    scope = model.all.order(:name)
    scope = scope.select(params[:select]) if params[:select]
    scope = scope.where(template_purpose_id:params[:purpose_id])            if params[:purpose_id].present?
    scope = scope.where(product_category_id:params[:product_category_id])   if params[:product_category_id].present?
    scope = scope.where(planning_category_id:params[:planning_category_id]) if params[:planning_category_id].present?
    scope = scope.where(['name LIKE ?', "%#{params[:name]}%"])              if params[:name].present?
    scope = scope.viewable_by(current_user)

    unless current_user.super?
      Enum::MarketingTemplatePurpose.all.each do |p|
        unless current_user.can?(p.permission)
          scope = scope.where("NOT (template_purpose_id=?)", p.id)
        end
      end

      # scope = scope.where('NOT (status=0 OR status IS NULL)') unless current_user.can?(:comm_template_marketing)
      scope = scope.where('NOT (type=? AND ownership_id=?)',  "Marketing::Email::Template",        Enum::Ownership.id('global')) unless current_user.can?(:marketing_canned_email)
      scope = scope.where('NOT (type=? AND ownership_id!=?)', "Marketing::Email::Template",        Enum::Ownership.id('global')) unless current_user.can?(:marketing_custom_email)
      scope = scope.where('NOT (type=? AND ownership_id=?)',  "Marketing::SnailMail::Template",    Enum::Ownership.id('global')) unless current_user.can?(:marketing_canned_print)
      scope = scope.where('NOT (type=? AND ownership_id!=?)', "Marketing::SnailMail::Template",    Enum::Ownership.id('global')) unless current_user.can?(:marketing_custom_print)
      scope = scope.where('NOT (type=? AND ownership_id=?)',  "Marketing::MessageMedia::Template", Enum::Ownership.id('global')) unless current_user.can?(:marketing_canned_sms)
      scope = scope.where('NOT (type=? AND ownership_id!=?)', "Marketing::MessageMedia::Template", Enum::Ownership.id('global')) unless current_user.can?(:marketing_custom_sms)
    end

    if !params.has_key?(:select) || params[:select].include?(:type) && params[:select].include?(:body)
      json_opts={methods:[:medium,:fields_in_body]}
    else
      json_opts={}
    end

    # Paginate if indicated
    if params[:paginate]
      @templates=scope.paginate page:params[:page], per_page:(params[:per_page] || 10)
      @templates_hash=@templates.as_json(json_opts)
      render json: {
       templates:  @templates_hash,
       page:       @templates.current_page,
       page_count: @templates.total_pages,
     }
    else
      render json: scope.as_json(json_opts)
    end
  end

  def edit
    redirect_to :index
  end

  def new
    redirect_to :index
  end

  def create
    permission_denied unless params[:marketing_template].present?
    params[:marketing_template].delete 'fields_in_body'
    @template = model.new params[:marketing_template]
    @template.owner_id ||= current_user.id
    if @template.save
      canned_success_response
    else
      canned_fail_response @template
    end
  end

  def update
    @template = model.find(params[:id])
    permission_denied unless params[:marketing_template].present?
    params[:marketing_template].delete 'fields_in_body'
    if @template.editable?(current_user) && @template.update_attributes(params[:marketing_template])
      canned_success_response
    else
      canned_fail_response @template
    end
  end

  def show
    @template = model.find(params[:id])
    respond_to do |format|
      format.html {render text: @template.body, layout:false }
      format.json { render json:@template }
    end
  end

  def destroy
    @template = model.find(params[:id])
    if @template.update_attributes(:enabled => false)
      canned_success_response
    else
      canned_fail_response @template
    end
  end

  def clone_template
    @template = Marketing::Template.find(params[:existing_template_id]).dup
    respond_with @template
  end

  # Called by "Send Email" button on Templates index
  def deliver
    @template = model.find params[:id]
    if(params[:medium].blank? || params[:medium]=='Email')
      #Allow the front end form to provide temporary quote link options for this delivery.
      @template.tmp_quoter_url_type             =params[:quoter_url_type].to_i
      @template.tmp_custom_quoter_url           =params[:custom_quoter_url]
      @template.tmp_insurance_division_url_page =params[:insurance_division_url_page]
    end
    @msg=''
    @status=200
    case params[:sending_option]
    when 'self'
      message_opts={template:@template, sender:current_user, target:current_user}
      if params[:medium]=='SMS'
        message=Marketing::MessageMedia::Message.new message_opts
      else
        message=Marketing::Email::Message.new message_opts
      end
      if message.deliver
        @recipient = current_user.primary_email.try(:value)
        @msg="Dispatched single email to #{@recipient}"
      else
        @msg=message.errors.full_messages.join('. ')
        @status=400
      end
    when 'blast', 'custom'
      attrs={user_id:current_user.id, template: @template}
      attrs= params[:sending_option]=='custom' ? attrs.merge(recipients: params[:recipients]) : attrs.merge(search_id: params[:search_id])
      blast = Marketing::Email::Blast.create(attrs)
      blast_response = blast.valid? && blast.deliver
      @msg+=blast_response if blast_response
      if blast.warnings.present?
        @msg+=blast.warnings.join(".\n")
      end
      if blast.errors.present?
        @msg+=blast.errors.full_messages.join(".\n")
        @status=400
      end
      #Saving calls `valid?` again and clears previous errors,
      #so we call save only after attempting sending and composing response.
      blast.save
    end
    render json:{msg:@msg}, status:@status
  end

  #Since displaying a confirmation dialog would require a second request anyway,
  #I'm making this its own action for clarity.
  def gauge_size_of_delivery
    case params[:sending_option]
    when 'self'
      return deliver
    when 'blast'
      size = Reporting::Search.find(params[:search_id]).get_matching_record_ids_for_user(current_user)[0].length
      max_size_without_confirmation=500
      return deliver unless size >max_size_without_confirmation
      user_has_bulk_send_capable_account=current_user.smtp_servers.any?{|s| s.bulk_send_capable }
      render json:{requires_confirmation:true, size:size, user_has_bulk_send_capable_account:user_has_bulk_send_capable_account}
    when 'custom'
      size=params[:recipients].to_s.split(/\s|;|,/).compact.uniq.reject(&:blank?).length
      max_size_for_custom=100
      return deliver unless size >max_size_for_custom
      render json:{errors:"Recipient list should not exceed #{max_size_for_custom} addresses. #{size} addresses were provided."}, status:400
    end
  end

private

  def task_type_id
    @task_type_id ||=
    begin
      if params[:task_type_id]
        params[:task_type_id].try(:to_i)
      elsif params[:task_builder_id]
        Crm::TaskBuilder.find(params[:task_builder_id]).try(:task_type_id)
      end
    end
  end

  # Return the model (subclass of Marketing::Template) on which we should be operating
  def model
    @model ||=
    begin
      if params[:medium].present?
        case params[:medium]
        when 'Email'
          Marketing::Email::Template
        when 'SMS'
          Marketing::MessageMedia::Template
        when 'Print'
          Marketing::SnailMail::Template
        else
          Marketing::Template
        end
      elsif task_type_id
        Marketing::Template.template_type_for_task_type_id task_type_id
      else
        self.class.instance_variable_get '@model'
      end
    rescue
      self.class.instance_variable_get '@model'
    end
  end

end