=begin

  Controller for receiving requests from 3CX Phone Server.

  The 3CX server POSTs data here when the availability of the Live Transfer
  queue changes. (Or when its cached value for same expires.)

  Dataraptor can alternatively send a GET request for this information if
  updates from the 3CX server are slow in coming (i.e. if Dataraptor's cache
  for said data expires).

=end

class PhoneRecordingsController < ApplicationController
  skip_before_filter :require_login, only:[:recording_post]
  # Requests to #update are allowed only from 3CX server
  before_filter -> { request.ip == '206.169.203.190' }, only:[:recording_post] if Rails.env.production?
  # Requests to get/index require user permissions
  before_filter -> { permission_denied unless current_user.can?(:super_view) }, only:[:recording_get, :recordings_index]


  #This action hides the secure AWS link,
  #and enforces login and access restrictions.
  def show
    r=PhoneRecording.find_by_id(params[:id])
    return canned_fail_response(status: 404) unless r.present?

    unless current_user.super? || (r.person && r.person.viewable?(current_user) )
      return permission_denied
    end

    the_file=r.binary_content
    opts={type: r.content_type, disposition:'inline', status:200}
    send_data the_file, opts
  end

  # Return list of Recordings pertaining to given person
  def index
    SystemMailer.deprecated_method_warning rescue nil
    permission_denied "Phone recordings are now served along with other file attachments."
    #@recordings = PhoneRecording.where(person_id:params[:person_id], person_type:params[:person_type])
  end

  # Create record for object in AWS S3
  # API endpoint accessed only by 3CX Server
  def create
    if %w[Party1 Party2 Timestamp S3FilePath].any?{|sym| params[sym].blank? }
      return render status:406, text:"Missing required attribute"
    end      
    records = PhoneRecording.create_records params
    status = records.blank? ? 200 : 201
    render status:status, json:records.as_json
  end

end
