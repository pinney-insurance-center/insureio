class MarketingController < ApplicationController
  #These pages are only rendered through rails in order to ensure that no one accesses without proper permissions.
  before_filter :require_login
  before_filter -> {require_permission :marketing}
  before_filter -> {require_permission :marketing_links}, only:'links'

  def links
    return render 'public/marketing/links_gui.html', layout:!params[:no_layout]
  end

end
