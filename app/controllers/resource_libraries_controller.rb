class ResourceLibrariesController < ApplicationController
  #These pages are only rendered through rails in order to ensure that no one accesses without proper permissions.

  before_filter :require_login
  before_filter -> { require_permission :quoting_product_tools }, only:'quoters'
  before_filter -> { require_permission :advanced_planning_tools }, only:[:calculators, :planning]
  before_filter -> { require_permission :professional_development }, only:'training'
  before_filter :require_login
  before_filter lambda { |controller|
   if controller.request.format.html?
      params.delete :layouts
 	end
      }
   before_filter -> {require_permission :resource_library}
   before_filter -> {require_permission :quoting_product_tools}, only:'xrae'
   before_filter -> {require_permission :underwriting_resources}, only:['guides', 'table_shave', 'ask', 'questionnaires']
end