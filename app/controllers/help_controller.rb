class HelpController < ApplicationController
  before_filter :require_login, only:[:suggestions, :post_suggestion]

	def post_suggestion
		SystemMailer.suggestion_email( params[:suggestion] ).deliver

		canned_success_response
	end

end
