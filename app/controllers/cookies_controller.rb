class CookiesController < ActionController::Base

  def delete
    reset_session
    cookies.clear
    redirect_to :root
  end

end
