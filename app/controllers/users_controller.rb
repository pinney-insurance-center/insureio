class UsersController < Usage::UsageBaseController
  respond_to :html, :js, :json

  prepend_before_filter -> { @ltd_auth_enabled = true }, only: [:agent_for_quote_path]

  before_filter :require_user, :only => ['lead_distribution_rules', 'tag',:l_and_c]
  before_filter :require_login, :except => [ 'add_security_question', 'set_security_questions', 'signup', 'process_signup', 'end_impersonate', 'create', 'quote_path_agent',:show]
  before_filter :set_any_http_origin, :only => [:create]

  before_filter :restrict_agency_management_for_agent, :only => [:index]
  before_filter :require_editable_user_or_current_user, :only => [:update_aml_vendor]
  skip_before_filter :check_security_questions_answered, only: [:set_security_questions, :add_security_question, :answer_security_question, :confirm_security_question]
  skip_before_filter :require_login, only: [:tos, :put_tos, :agent_for_quote_path,:show_my_api_key]

  # Returns just enough data for the customer-facing 'agent summary' at the
  # top of the quote path. Two methods of access are currently available: (1)
  # a login session (using a ltd_access_code) and (2) an id+quoter_key match
  # with no login requirement
  def agent_for_quote_path
    if current_user && User.ltd_access_sess?
      return canned_fail_response(status: 403) if params[:id].to_i != current_user.id
      @user = current_user
    elsif params[:key].present?
      @user = User.able_to_log_in.find_by(id:params[:id], quoter_key:params[:key])
    end
    return canned_fail_response("Invalid agent", status: 403) unless @user&.can_log_in?
    @brand  = Brand.find_by_id(params[:brand_id])
    @brand||= @user.default_brand
    @email = @brand.primary_email || @user.primary_email
    @phone = @brand.primary_phone || @user.primary_phone
    render json:{
      name:@user.name,
      brand_name:@brand.try(:name),
      thumb:@brand.logo_url,
      phone:@phone,
      email:@email,
      license_disclaimer:@user.license_info(@brand.id)
    }
  end

  def agents
    scope = User.viewable_by(current_user).
      excluding_recruits.
      can(:lead_distribution).
      select('users.id, users.full_name, users.temporary_suspension')
    scope = scope.search_full_name(params[:name]) if params[:name].present?
    scope = scope.joins(:brands).where('brands.id = ?', params[:brand_id]) if params[:brand_id].present?
    hashes = scope.all.map{|u| {id:u.id, name:u.full_name, temporary_suspension:u.temporary_suspension}}
    render json:hashes
  end

  def brand_ids
    @user = User.find(params[:id])
    render json:@user.brand_ids
  end

  #Accepts a list of user ids provided via POST.
  #Makes 3 queries and collates the results with a few loops.
  #Renders a hash of arrays of brands, grouped by user id.
  def member_brand_names
    unless params[:ids].present? && params[:ids].is_a?(Array) && params[:ids].all?{|x| x.is_a?(Integer)}
      return permission_denied('This action requires a list of valid user ids.')
    end
    id_pairs=ActiveRecord::Base.connection.
      execute("SELECT user_id,brand_id
      FROM brands_users
      WHERE user_id IN (#{params[:ids].join(',')})").to_a
    p_ids=id_pairs.map{|x| x[1]}

    p_names=Brand.
      where(id:p_ids).name_and_id_only

    brands_per_user={}
    id_pairs.each do |u_id,p_id|
      brands_per_user[u_id]||=[]
      #In rare instances, there may be an entry in the join table
      #for a brand that no longer exists. Skip it.
      p_name =p_names.select{|x| x.id==p_id }.first.name rescue next
      brands_per_user[u_id]<<{id:p_id,name:p_name}
    end

    render json: brands_per_user
  end

  #Handles posts from the signup page.
  def process_signup
    attrs=params[:user]
    if !attrs || !params[:pay_agree] || !params[:tos]
      return permission_denied "It looks like required fields are missing.", status: 400
    end
    @user = User.new

    p_id=attrs[:parent_id]
    p=p_id ? User.find_by_id(p_id) : nil
    r_id=attrs[:recruit_id]
    r=r_id ? User.where(is_recruit,id:r_id).first : nil

    parent_can_recruit = p && p.can?(:recruit)
    recruit_owner_can_recruit = r && r.parent && r.parent.can?(:recruit)
    if p_id  && !parent_can_recruit
      attrs.delete :parent_id
    end
    if r_id  && !recruit_owner_can_recruit
      attrs.delete :recruit_id
    elsif recruit_owner_can_recruit
      @user.recruits<<r
      attrs.delete :recruit_id
    end

    @user.assign_attributes attrs

    if @user.save
      UserSession.create(@user)
      canned_success_response
    else
      canned_fail_response @user, 'signup', 'new user'
    end
  end

  def show
    if request.format.html?
      return render 'public/users/container.html', layout: !params[:no_layout]
    end

    #When not logged in, rather than fail, return an empty object.
    #This is important, because the quote path is accessible to both consumers and users.
    return render json:'{}' if current_user.nil?

    @user = (params[:id] && params[:id].to_i != 0) ? User.find(params[:id]) : current_user
    return permission_denied unless @user == current_user || @user.viewable?(current_user)

    commission_level_id = (@user.recruit?) ? @user.user_account.try(:commission_level_id) : @user.commission_level_id
    login = (@user.recruit? && @user.user_account) ? @user.user_account.login : @user.login
    output_obj=@user.as_json({
      include:[:phones, :emails, :addresses,:webs],
      methods:[
        :insurance_division_id,
        :website,
        :ssn,
        :stripe_subscribed,
        :lapsed,
        :parent_name,
        :ssn,:tin,
        :photo_thumb_url,
        :status,
        :license_types,
        :aml_vendor_name,
        :aml_completed_at,
        :agent_of_record_id,
        :agent_of_record_name,
        :submit_application,
        :submit_referral,
        :marketech_signature,
        :signature_url,
        :rtt_allowed_state_ids,
      ],
      except:User::FIELDS_TO_HIDE
    })
    output_obj=output_obj.merge({
      :user_login => login,
      :user_commission_level_id => commission_level_id
    })
    if @user==current_user
      output_obj=output_obj.merge({
        '$trueCurrentUserId'.to_sym=>true_current_user.id,
        '$templatesLastModified'.to_sym=>(IO.readlines("#{Rails.root}/config/template_version.txt")[0].to_i rescue 0)||0,
      })
    end
    render json: output_obj
  end

  def lead_types
    @user = User.find(params[:id])
    respond_with @user.lead_distribution_rule_values.sort
  end

  def common_tags
    render 'public/users/common_tags_gui.html', layout:!params[:no_layout]
  end

  def add_common_tags
    tags_to_add= params[:tags] || Array(params[:tag])
    tags_to_add << params[:tag_name] if params[:tag_name]
    tags_to_add= tags_to_add.map{|t| t.split('=')}
    current_user.common_tags.push(*tags_to_add) if tags_to_add.all?{|t| t.is_a?(Array) && (t.length==1 || t.length==2) }

    tags_to_delete= params[:delete_tag].try(:split,'=')
    current_user.common_tags.delete(tags_to_delete) if tags_to_delete.present?

    if current_user.save
      canned_success_response nil, 'update', 'common tag list'
    else
      canned_fail_response current_user, 'update', 'tag list'
    end
  end

  def create
    @parent_user = User.find_by_id(params["user"]["parent_id"])
    can_create=( current_user==@parent_user || current_user.can_edit_user?(@parent_user) ) && current_user.try(:can?, :have_children)
    return permission_denied('You do not have permission to create user') unless can_create
    @user = User.new()

    #remove addresses where all fields are blank (rather than attempting to save them and throwing validation errors)
    if params[:user][:addresses_attributes].present?
      blank_addresses=[]
      params[:user][:addresses_attributes].each do |k,v|
        blank_addresses << v if k[:value].blank? && k[:street].blank? && k[:city].blank? && k[:state_id].blank? && k[:zip].blank?
      end
      blank_addresses.each{|i| params[:user][:addresses_attributes].delete(i)}
    end

    @user.attributes=params[:user]
    @user.skip_notification_email = true
    if @user.save
      unless request.format.json?
        flash[:notice] = "Account created."
        respond_to do |format|
          format.html {
            redirect_to action: :personal, id: @user.id
          }
        end
      else
        canned_success_response @user, jopts: {only:[:id]}
      end
    else
      # This call to load_errors_from_all_associations is here and not in a
      # validation callback because calling this on every validation could be
      # expensive. (In the current implementation, it actually is expensive
      # because of fields like :parent_name_only and :owner_name_only.) We may
      # revisit this later, when a hotfix is not due, and find a better way to
      # perform this check.
      @user.load_errors_from_all_associations if @user.valid?
      canned_fail_response @user
    end
  end

  def recruit_for_user
    user=User.find_by_id params["id"]
    pinney          =current_user.pinney?
    recruit         =pinney ? user.recruits.where('pinney_record IS TRUE OR parent_id=?',current_user.id).first : user.recruits.where(parent_id:current_user.id).first
    recruit       ||=user.recruits.create(pinney_record: pinney, parent_id:current_user.id, user_account:user)
    render json: recruit
  end

  def update
    @user = User.find(params[:id])

    attrs=params[:user]||{}.with_indifferent_access

    attrs[ params[:name] ]=params[:value] if params.has_key?('name') && params.has_key?('value')
    attrs[ params[:key]  ]=params[:value] if params.has_key?('key') && params.has_key?('value')

    return canned_fail_response(@user) unless attrs.present?

    return permission_denied unless current_user.can_edit_user?( @user, attrs )

    ### SET ATTRIBUTES W/OUT SAVING ###
    @user.attributes = attrs
    ### CHECK PERMISSIONS FOR INDIVIDUAL CHANGES ###
    # Check for permission to change lineage (parent_id)
    if !current_user.can?(:edit_user_parent_id) && @user.parent_id_changed?
      return permission_denied('You lack permission to change the lineage of this User')
    end
    # Check for permission to edit permissions (i.e. current_user is not editing self)
    # This will not effect permission changes triggered by a membership level change, as that change is made in a `before_update` filter.
    if !current_user.can?(:super_edit) && current_user == @user && (0..4).any?{|i| @user.send("permissions_#{i}_changed?") }
      return permission_denied('Silly rabbit! You can\'t edit your own permissions!')
    end
    # Check for permission to edit membership_id
    if !current_user.can?(:edit_others_membership_id) && @user.membership_id_changed? && current_user != @user
      return permission_denied('You don\'t have permission to change payment info on this user')
    end

    password_verification_required=@user.misc_permissions_changed? || attrs.has_key?(:password) || @user.trigger_reset_quoter_key || @user.trigger_reset_api_key
    if password_verification_required
      #Note that the password for verification is located outside the `user` node of `params` object.
      provided_pw=params[:password] && User.crypto_provider.encrypt(params[:password], current_user.password_salt)
      provided_pw_matches_saved_pw=provided_pw && current_user.crypted_password == provided_pw
      unless provided_pw_matches_saved_pw
        return permission_denied('Password rejected. Please contact a System Admin for assistance.')
      end
    end

    ### ATTEMPT SAVE ###
    if @user.save
      commission_level_id = (@user.recruit?) ? @user.user_account.try(:commission_level_id) : @user.commission_level_id
      login = (@user.recruit?) ? @user.user_account.try(:login) : @user.login
       render json: @user.as_json({
         include:[:phones, :emails, :addresses,:webs],
         methods:[:website,:license_types,:lapsed,:parent_name,:ssn,:tin,:photo_thumb_url, :status],
         except:User::FIELDS_TO_HIDE
       }).merge({:user_login => login, :user_commission_level_id => commission_level_id})
    else
      canned_fail_response @user
    end
  end

  #Allows a user to check their own key.
  #Requires login, and @param 'password'.
  def show_my_api_key
    provided_pw = params.permit(:password) && User.crypto_provider.encrypt(params[:password], current_user.password_salt)
    provided_pw_matches_saved_pw = provided_pw && current_user.crypted_password == provided_pw
    unless provided_pw_matches_saved_pw
      return permission_denied('Password rejected. Please contact a System Admin for assistance.')
    end
    render json: {api_key:current_user.api_key}, status: 200
  end

  def get_autocomplete_items(parameters)
    super(parameters)
    items = User.search_full_name(params[:term])
  end

  def destroy
    @user = User.find_by_id(params[:id])
    return permission_denied unless @user.editable?(current_user)
    @user.delete
    flash[:notice]="Successfully deleted user."
    render :index
  end

  def index
    # This json response is used for auto-complete inputs (using select2 js library)
    if request.format.json?
      index_json
    end
  end

  #This action is used for autocompletion in agent assignment menu.
  def names
    render(json:{error:"You have to supply a search term and a brand id"}, status:422) unless params[:full_name] && params[:brand_id]
    #using 2 queries here b/c rails is incapable of using includes and select together.
    user_ids=User.joins('LEFT JOIN `brands_users` ON `brands_users`.`user_id` = `users`.`id`').
    where('brands_users.brand_id=?', params[:brand_id]).pluck(:id)

    @names = User.where(id:user_ids).
      name_like( params[:full_name] ).name_and_id_only.order(:full_name)
    render json:@names
  end

  #This page shows the membership levels for the specified user's downline.
  #Will either render the markup, or if provided a user id, will render json for populating the table.
  def membership_levels
    return permission_denied unless current_user.can?(:super_edit)

    if request.format.html?
      return render 'public/users/membership_levels_gui.html', layout: !params[:no_layout]
    end

    return permission_denied('Must specify user.') unless params[:user_id].present?
    @user=User.find_by_id(params[:user_id])
    return permission_denied('Cannot find specified user.') unless @user.present?

    base_scope=@user.descendants.excluding_recruits.
      where('active_or_enabled IS TRUE OR membership_id=0')
    counts=base_scope.select(:membership_id).group(:membership_id).count
    counts[-1]||=0
    counts.each do |k,v|
      #If membership id is invalid, lump it into free column.
      if Enum::Membership.find(k).nil?
        counts[-1]+=v && counts.delete(k)
      end
    end
    #This type of inconsistent data should not exist, but if it does, we still want to count it.
    num_who_cant_login_but_indicate_membership=@user.descendants.
      excluding_recruits.
      where(active_or_enabled:false).where('membership_id!=0').count
    counts[0]||=0
    counts[0]+=num_who_cant_login_but_indicate_membership
    total=0
    counts.map{|k,v| total+=v}
    counts[:total]=total
    #detect incorrectly setup or lapsed accounts:
    lack_subscription=base_scope.
      where(membership_id:[1,2,3,4], stripe_subscription_id:nil).
      select([:id,:membership_id,:full_name])
    payment_overdue=base_scope.
      where(membership_id:[1,2,3,4]).
      where('payment_due IS NULL OR payment_due < ?',Date.today).
      select([:id,:membership_id,:full_name])
    should_be_paying=lack_subscription+payment_overdue
    if should_be_paying.length>0
      should_be_paying.map!{|u| {id:u.id,membership_id:u.membership_id,name:u.full_name} }
      counts[:should_be_paying]=should_be_paying
    end
    render json: counts
  end

  def payment_info
    if params[:no_layout].present?
      render 'public/users/payment_info_gui.html', layout:false
    else
      render 'public/users/payment_info_gui.html', layout:true
    end
  end

  def tos
    @user = current_user
    @redirect_path = params[:redirect_path]
  end

  def put_tos
    @user = current_user
    @user.update_attributes(tos:params[:user][:tos])
    render nothing:true, layout:true
  end

  def lead_distribution_rules
    @user = User.find(params[:id])
    @lead_distribution_rules = @user.lead_distribution_rules.enabled
    respond_with @lead_distribution_rules.sort_by { |object| object.lead_type }
  end

  def update_aml_vendor
    if params["name"] == "name"
      @aml_vendor = Usage::AmlVendor.find(params[:pk])
      @aml_vendor.name = params[:value]
      @aml_vendor.save
    else
      @user = User.find_by_id(params[:pk])
      @aml = @user.aml.update_attributes(completion: params[:value])
    end
    respond_to do |format|
      flash.now[:notice]='Successfully updated.'
      format.js { render nothing: true, layout: true }
      format.html {}
    end
  end

  def impersonate
    target_user = User.find params[:id]
    if current_user.impersonation && current_user.can_edit_user?(target_user)
      flash[:notice] = "successfully impersonating user #{target_user.full_name}"
      session[:ostensible_user_id] = params[:id]
      custom_header(:ostensible_user_id, session[:ostensible_user_id])
      @status = 200
    else
      flash[:error] = "You do not have permission to access it"
      @status = 401
    end
    if request.format.json?
      render json:{name:target_user.name, id:target_user.id}, status:@status
    else
      redirect_to :root
    end
  end

  def end_impersonate
    session.delete(:ostensible_user_id)
    if request.format.json?
      render json:{name:current_user.name, id:current_user.id}
    else
      redirect_to root_path
    end
  end

  def get_autocomplete_items(parameters)
    unless params[:user_id].blank?
      user = User.find(params[:user_id])
      items = super(parameters)
      unless user.try(:primogen_id).blank?
        items = items.where(primogen_id: user.try(:primogen_id))
      else
        return []
      end
    else
      items = super(parameters)
    end
  end

  def set_security_questions
    @security_answer = Usage::SecurityAnswer.new
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def add_security_question
    @security_answer = current_user.build_security_answer(params[:usage_security_answer])
    respond_to do |format|
      if @security_answer.save
        current_user.update_attributes(security_questions_answered: true)
        format.js { redirect_to root_path }
        format.html { redirect_to root_path }
      else
        format.js {}
        format.html {
          render action: 'set_security_questions'
        }
      end
    end
  end

  def confirm_security_question
    answered_correctly = false
    Usage::SecurityAnswer::QUE_ARRAY.each do |que_attr|
      unless params[:usage_security_answer][que_attr].blank?
        if current_user.security_answer.try(Usage::SecurityAnswer::QUE_ANS_HASH[que_attr]).casecmp( params[:usage_security_answer][Usage::SecurityAnswer::QUE_ANS_HASH[que_attr]] )
          if  params[:remember_me] == "yes"
            cookies[:remember_ip] = "1"
          else
            cookies[:remember_ip] = "0"
          end
          answered_correctly = true
          break
        end
      end
    end
    if answered_correctly
      flash[:notice]="Successfully logged in."
      redirect_to root_path
    else
      flash[:error]="Security question answers do not match the answers on file."
      @user_session=UserSession.new
      @contact=User.new
      2.times { @contact.emails.build }
      3.times { @contact.phones.build }
      render '/user_sessions/new'#:action => "/user_sessions/new"
    end
  end

  def answer_security_question
    @security_answer = Usage::SecurityAnswer.new
    respond_to do |format|
      format.js {}
      format.html {
      }
    end
  end

  def mass_update
    raise 'not implemented yet'
  end

  def staff_assignment
    @user = params[:id] ? User.find(params[:id]) : current_user
    redirect_to params.merge controller:'/usage/staff_assignments', action:'show', id:@user.staff_assignment.id
  end

  # +params[:tags]+ should be an Array of String formatted "key=value" OR "key"
  def tag
    params[:tags] ||= Array(params[:tag])
    params[:tags] << params[:tag_name] if params[:tag_name]
    params[:tags] = params[:tags].map(&:strip).reject{|t| t.blank? }
    @user.tags.push *params[:tags]

    if params[:delete_tag].present?
      @user.tags.delete( params[:delete_tag] )
      tag_w_out_white_space=params[:delete_tag].strip
      @user.tags.delete( tag_w_out_white_space )
    end

    if @user.save
      canned_success_response nil, 'update', 'tag list'
    else
      canned_fail_response @consumer, 'update', 'tag list'
    end
  end

  #used by select2 for filtering tasks in dashboard
  def descendant_names
    @user  = params[:id] ? User.find(params[:id]) : current_user
    @users = @user.descendants.name_like( params[:q] ).name_and_id_only
    render json: @users.map{|u| {id:u.id, text:u.name} }
  end

  def editable_user_ids
    ids=User.editable_by(current_user).pluck(:id)
    render json: ids
  end

  def hierarchy_mgmt
    render 'public/users/hierarchy_gui.html', layout: !params[:no_layout]
  end

  #Return a user and their ascendants ordered by ascendancy.
  def hierarchy_breadcrumb
    return permission_denied unless current_user.can_any?(:super_view,:super_edit)
    return permission_denied unless params[:id]

    user=User.where(id:params[:id]).first

    list=user.ascendants
      .select([:id, :parent_id,:full_name,:membership_id,:active_or_enabled])
      .as_json(only:[:id,:parent_id],methods:[:name,:active])
    list << user.as_json(only:[:id,:parent_id,:last_request_at,:membership_id],methods:[:can_strict_have_children,:active])
    #Through some quirk of rails, the above produces a hash with some string keys and some symbol keys.
    #Indifferent access allows us to ignore that.
    list.map!{|u| HashWithIndifferentAccess.new u}
    sorted_list=[]
    while sorted_list.length<list.length
      list.each do |a|
        if sorted_list.include?(a)
          next
        elsif a[:parent_id].nil?
          sorted_list << a#has no parent, so it is the first ancestor in the line.
        elsif parent=sorted_list.find{|b| b[:id]==a[:parent_id] }
          sorted_list << a#parent is in sorted_list already. add after it.
        elsif !parent=list.find{|b| b[:id]==a[:parent_id] }
          sorted_list << {id:a[:parent_id],name:'(Missing/Deleted User)'}#parent is missing. child erroneously referrs to a deleted user.
        else
          next#parent is in list but not in the sorted_list yet. wait.
        end
      end
    end

    render json: sorted_list
  end

  #May be called with or without an id.
  #Without an id will show the current user the top level of the hierarchy that they are allowed to see.
  def hierarchy_branch
    return permission_denied unless current_user.can?(:user_tree)
    if params[:id]
      branch_parent=User
        .viewable_by(current_user)
        .excluding_recruits
        .where(id:params[:id])
        .select('id')
        .first
      return permission_denied unless branch_parent
      branch_parent_id=params[:id]
    elsif current_user.can?(:super_view)
      branch_parent_id=nil
    else
      #Even if a user cannot see their siblings, it is useful for them to see themselves,
      #so they can track their immediate child and descendant counts.
      branch_parent_id=current_user.parent_id#can be nil
    end

    users=User.viewable_by(current_user)
      .excluding_recruits
      .where(parent_id: branch_parent_id )

    users=users.uniq
    output=users.as_json(only:[:id,:last_request_at,:membership_id],methods:[:name,:active,:can_strict_have_children,:is_editable])
    render json: output
  end

  def child_and_descendant_counts
    unless params[:user_ids] && params[:user_ids].is_a?(Array)
      return permission_denied('You must provide a list of users for which to get the counts.')
    end

    child_counts=User.where(parent_id:params[:user_ids]).excluding_recruits.group(:parent_id).count

    descendant_counts=User
      .excluding_recruits
      .joins('INNER JOIN usage_ascendants_descendants
        ON users.id = usage_ascendants_descendants.descendant_id')
      .where('usage_ascendants_descendants.ascendant_id IN (?)',params[:user_ids])
      .group('usage_ascendants_descendants.ascendant_id').count

    render json:{childCounts:child_counts, descendantCounts: descendant_counts}
  end

  private

  # used for auto-complete inputs
  def index_json
    scope = User.all.viewable_by(current_user).excluding_recruits
    if params[:user_id]
      scope = scope.descendants_and_self(params[:user_id])
    elsif params[:brand_id].present?
      col, bit = Usage::Permissions.column_and_bit(:sales_support)
      #this is for the task reassignment menu
      scope=scope.joins('LEFT JOIN `brands_users` ON `brands_users`.`user_id` = `users`.`id`').
        where(%Q(
        active_or_enabled IS TRUE AND
        (brands_users.brand_id=? OR users.membership_id=? OR users.#{col} & (1<<#{bit}) )),
        params[:brand_id], Enum::Membership.id('Pinney') )
    end
    if params[:full_name]
      scope = scope.search_full_name(params[:full_name])
    end
    params[:select].push('users.id') if params[:select] && params[:select].delete('id')
    users = scope.select(params[:select]).uniq
    users = users.paginate(page: params[:page]||1, per_page: params[:per_page]||9).to_a unless params[:no_pagination]
    users<< User.where(id:current_user.id).select(params[:select]).first
    # include pseudo_attribute 'full_name' in the results
    return render json: users.map { |user|
      attrs = user.attributes.select{|a| !User::FIELDS_TO_HIDE.include?(a) }
      attrs[:full_name] = user.full_name if attrs.has_key?('middle_name')
      attrs
    }
  end

  def require_user
    if params[:user_id].present?
      @user = User.find_by_id(params[:user_id])
    elsif params[:id].present?
      @user = User.find_by_id(params[:id])
    else
      @user=current_user
    end

    return permission_denied('Cannot find user with the specified id.',400) unless @user.present?
    return permission_denied unless current_user.try(:can_view_user?, @user)
  end

  def security_answer_given
    if current_user.security_questions_answered
      redirect_to root_path
    end
  end

end
