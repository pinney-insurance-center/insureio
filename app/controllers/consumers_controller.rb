class ConsumersController < Crm::CrmBaseController

  skip_before_filter :require_login, only: [:create]
  skip_before_filter :require_referrer_host_match, only: [:create]
  skip_before_filter :verify_authenticity_token, only: [:create]

  prepend_before_filter -> { @ltd_auth_enabled = true }, only: [
    :show, :update, :existing_coverage,
  ]

  before_filter :set_any_http_origin, only: [:create, :show, :policy_and_opportunity_ids]
  before_filter :require_widget_and_user, only: [:create]
  before_filter :require_consumer, except:[
        :csv,
        :batch_import,
        :names,
        :get_autocomplete_items,
        :create, :new, :index,
      ]

  respond_to :html, :json

  #Will list and allow addition/editing/removal of connection relationships
  def related
    return redirect_to '/consumers/related/container.html' if request.format.html?
    list= @consumer.all_relationships_w_details_as_json
    respond_with(list)
  end

  def suggested_relations
    render json: @consumer.suggested_relations
  end

  def show
    if request.format.html?
      return render 'public/consumers/show.html', layout:!params[:no_layout]
    end
    render json: @consumer.default_serialization
  end

  #DEPRECATED. USE `TasksController#index`.
  def tasks
    SystemMailer.deprecated_method_warning
    render json: @consumer.tasks
  end

  # +params[:tags]+ should be an Array of String formatted "key=value" OR "key"
  def tag
    params[:tags] ||= Array(params[:tag])
    params[:tags] << params[:tag_name] if params[:tag_name]
    params[:tags] = params[:tags].map(&:strip).reject{|t| t.blank? }
    @consumer.tags.push params[:tags]

    if params[:delete_tag].present?
      @consumer.tags.delete( params[:delete_tag] )
      tag_w_out_white_space=params[:delete_tag].strip
      @consumer.tags.delete( tag_w_out_white_space )
    end

    if @consumer.save
      canned_success_response nil, 'update', 'tag list'
    else
      canned_fail_response @consumer, 'update', 'tag list'
    end
  end

  def create
    @controller_warnings=[]
    if !params.has_key?(:consumer) && params.keys.any?{|k| k.match(/^consumer\.\w+/) }
      params[:consumer]=expand_dot_notation( params, [:crm_connection,:connection,:response], :consumer)
    end

    #This deprecation check should be temporary.
    #Should set a schedule for removal with Ryan.
    if params[:consumer].blank? && params.has_key?(:crm_connection)
      _handle_key_crm_connection
    end
    return json_406 'Lacking consumer object' if params[:consumer].blank?

    mark_all_params_as_permitted params[:consumer]

    attrs=params[:consumer]

    # Detect if this request originated from outside the app (the API),
    referring_host=request.referrer.nil? ? nil : URI.parse(URI.encode(request.referrer)).host
    is_api_call = !referring_host || !referring_host.match(/\w+\.(insureio\.\w+)/)
    if is_api_call
      Rails.logger.info "Request originated from API. Remote IP:#{request.remote_ip}"
    end

    if attrs.to_s.include?('status_type_id')
      Rails.logger.level=0 #debug level. show everything!
    end

    #This endpoint gets more fake data than others, so requires additional, early detection.
    if !name_sounds_real_enough
      @controller_warnings||=[]
      @controller_warnings<< @name_error
    end

    if !nested_attrs_have_correct_types
      return render json:{errors: @attrs_type_error }, status: 400
    end

    @consumer=Consumer.update_existing_or_create_new(attrs)
    if @consumer.errors.blank?
      log_event_with_ahoy "Submitted To Lead Import", participant_id:@consumer.id, participant_type:'Consumer', brand_id:@consumer.brand_id, owner_id: @consumer.agent_id

      response.headers["Access-Control-Allow-Origin"] = "*"
      response.headers["Access-Control-Allow-Methods"] = "POST"
      output = if params[:debug]
        @consumer.as_json_debug
      elsif params[:response] == 'empty'
        {}
      elsif params[:response] == 'short'
        @consumer.short_serialization
      elsif params[:response] == 'medium'
        @consumer.medium_serialization
      else#the most detailed response, and the default.
        @consumer.detailed_serialization
      end
      if @controller_warnings.present?
        output[:warnings]||=[]
        output[:warnings]+=@controller_warnings
      end
      render json:output
    else
      output ={errors: @consumer.errors.full_messages}
      output[:warnings]=@controller_warnings
      output[:warnings]+=(@consumer.warnings||[])
      render json:output, status:422
    end
  rescue ActiveRecord::UnknownAttributeError, Enum::NotFound => ex
    render json: {errors: [ex.message]}, status: 422
  end

  #DEPRECATED
  #Reverts a set of changes made via the API.
  #Determines old values from the most recent update note.
  #Retains the note for documentation, but marks it as reverted.
  def revert
    SystemMailer.deprecated_method_warning
    update_note=@consumer.notes.of_type('api-triggered consumer update').order('created_at DESC').first
    update_text=update_note.text
    updated_obj=case update_text
                when /\AConsumer/
                  @consumer
                when /\AHealth Info/
                  @consumer.health_info
                when /\AQuoted Details/
                  return canned_fail_response unless ids=update_text.match(/\AQuoted Details for Policy id\:(\d+)/).try(:captures)
                  @consumer.cases.where(id:ids[0]).quoted_details
                end
    update_json=update_text.match(/Old Values:\s*(\{.+)\s+New Values:/m)[1]
    update_hash=JSON.parse(update_json)

    updated_obj.assign_attributes(update_hash)

    #so that tags get added/modified, rather than replacing the list, as would normally happen with mass-assignment.
    @consumer.tags.push( params[:consumer].delete(:tags) ) if update_hash.has_key?(:tags)

    if updated_obj.save
      update_note.update_attributes(note_type_id:Enum::NoteType.id('reverted consumer update'), text:"(Reverted on #{Date.today.to_s})\n#{update_text}" )
      canned_success_response
    else
      canned_fail_response updated_obj, status: 500
    end
  rescue Enum::NotFound => ex
    render json: {errors: [ex.message]}, status: 422
  end

  def destroy
    @consumer.destroy
    respond_to do |format|
      format.js { canned_success_response }
      format.html {}
    end
  end

  def edit
    render :layout => false
  end

  def update
    attrs=params[:consumer]||{}.with_indifferent_access

    attrs[ params[:name] ]=params[:value] if params.has_key?('name') && params.has_key?('value')
    attrs[ params[:key]  ]=params[:value] if params.has_key?('key') && params.has_key?('value')
    return canned_fail_response(status: 403) unless @consumer.editable?(current_user)
    return canned_fail_response(status: 422) unless attrs.present?
    if attrs[:opportunities_attributes]
      saved_quote_num=@consumer.saved_quotes.length
    end

    @consumer.attributes = attrs
    if @consumer.save
      if saved_quote_num && @consumer.saved_quotes.length!=saved_quote_num#similar to model +changed?+ method, but only considers length
        flash.now[:notice]="There are now #{@consumer.saved_quotes.length} quotes saved for this consumer."
      end
      canned_success_response
    else
      canned_fail_response @consumer
    end
  end

  def log_contact_and_call
    @consumer.update_column(:last_contacted,Time.now)
    redirect_to "tel:#{params[:phone]}"
  end

  def new
  end

  def primary_contact
    respond_with @consumer.primary_contact.try(:full_name)
  end

  def spouse
    render json: @consumer.spouse.as_json(methods:[:name])
  end

  def get_autocomplete_items(parameters)
    super(parameters)
    items = Consumer.search_full_name(params[:term])
  end

  def financial
    SystemMailer.deprecated_method_warning
    @consumer.financial_info
    render json:@consumer.financial_info.as_json
  end

  def tracking
    SystemMailer.deprecated_method_warning
    @tags = @consumer.tags
  end

  def csv
    return render 'public/consumers/csv_gui.html', layout:!params[:no_layout]
  end

  def batch_import
    return permission_denied unless params[:rows].present? && params[:offset].present?
    successes =[]
    failures  =[]
    params[:rows].each_with_index do |row,i|
      #handle missing agent/brand
      if row[:agent_id].blank? && row[:brand_id].blank?
        row[:agent_id]=current_user.id && row[:brand_id]=current_user.default_brand_id
      elsif row[:agent_id].present? && row[:brand_id].blank?
        row[:brand_id]=User.find(row[:agent_id]).try(:default_brand_id)
      end#if only agent_id is missing, lead distribution will set it.

      #Ensure that batch imported opportunities and policies that
      #do not explicitly specify an initial status type
      #get set to a status type that will not generate an avalanche of tasks.
      if row[:opportunities_attributes].present?
        row[:opportunities_attributes].each do |opp|
          if !opp[:status_type_id]
            opp[:status_type_id]=Crm::StatusType::PRESETS[:prospect]
          end
        end
      end

      if row[:cases_attributes].present?
        row[:cases_attributes].each do |c|
          if !c[:status_type_id] && c[:sales_stage_id].to_i!=Enum::SalesStage.id('Placed')
            c[:status_type_id]=Crm::StatusType::PRESETS[:prospect]
          end
        end
      end
      consumer = Consumer.new
      begin
        consumer.assign_attributes row
        consumer.save
      rescue =>ex
        ExceptionNotifier.notify_exception(ex)
        consumer.errors.add(:base,'A non-recoverable error occurred while saving this row. Please contact support.')
      end
      if consumer && consumer.persisted? && consumer.errors.empty?
        successes << {row_number:i+params[:offset].to_i, id:consumer.id, created_at:consumer.created_at, updated_at:consumer.updated_at}
      else
        failures << {row_number:i+params[:offset].to_i, errors:consumer.errors.full_messages}
      end
    end
    render json: {successes:successes, failures: failures}
  end

  def existing_coverage
    render json:@consumer.cases.existing_coverage.as_json( include:[:current_details])
  end


  def names
    permission_denied("You have to supply a search term", status: 400) unless params[:q]
    @client_ids = Consumer.viewable_by(current_user)
      .where('full_name LIKE ?', params[:q].gsub(/\s|^|$/,'%'))
      .name_and_id_only
      .order(:full_name).map(&:full_name).uniq
    render json:@names
  end

  #This is an API endpoint for authenticated users.
  #It gives them the data needed to make future requests for status history or current status.
  def policy_and_opportunity_ids
    render json: {
      policy_ids: @consumer.case_ids,
      opportunity_ids: @consumer.opportunity_ids
    }
  end

  private

  def _handle_key_crm_connection
    SystemMailer.deprecated_method_warning params
    params[:consumer]=params[:crm_connection]
    c_deprecation_msg='The key `crm_connection` will no longer be supported in the future.. Please use key `consumer` going forward.'
    @controller_warnings<<c_deprecation_msg
  end

  # Render 406 response with error message
  def json_406 message
    render json:{errors:message}, status:406
  end

  def name_sounds_real_enough
    return if params[:consumer][:id]
    return if params[:consumer][:test_lead]

    fname=params[:consumer][:full_name]
    contains_tipoff_words= lambda {|str| str.split(/\s/).any?{|word| word.match(/\A(name|test|example|placeholder)\z/i) } }
    looks_like_junk= lambda {|str| Dejunk.is_junk?(str) }

    if fname.blank?
      @name_error="Missing required field: full name."
    elsif contains_tipoff_words.call(fname)
      @name_error="Placeholder words detected. Must provide a real full name."
    elsif looks_like_junk.call(fname)
      @name_error="#{looks_like_junk.call(fname).to_s.humanize.capitalize} detected. Must provide a real full name."
    end
    return @name_error.blank?
  end

  #This function is meant to prevent `ArgumentError`s for the public API endpoint.
  #`ArgumentError`s sadly do not mention the key/value pair that caused them,
  #so in order to collect and  report this specific info to the user,
  #it is better to prevent than to rescue such errors.
  def nested_attrs_have_correct_types
    attrs=params[:consumer]
    @attrs_type_error=''
    attrs.each do |key, value|
      is_assoc_key, assoc_name=key.match(/(.+)_attributes\z/).try(:to_a)
      next unless is_assoc_key
      specified_assoc=Consumer.reflect_on_association(assoc_name.to_sym)
      next unless specified_assoc

      if specified_assoc.collection? && !value.is_a?(Array)
        @attrs_type_error+=" Key #{key} requires an array, but instead received a #{value.class.name}."
      elsif specified_assoc.collection? && !value.all?{|member| member.is_a?(Hash) }
        @attrs_type_error+=" Key #{key} requires an array of objects."
      elsif !specified_assoc.collection? && !value.is_a?(Hash)
        @attrs_type_error+=" Key #{key} requires an object, but instead received a #{value.class.name}."
      end
    end
    @attrs_type_error=@attrs_type_error.sub('ActiveSupport::HashWithIndifferentAccess','object')
    @attrs_type_error.empty?
  end
end
