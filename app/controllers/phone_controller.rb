class PhoneController < ContactBelongsTosController
  respond_to :json

  MODEL = Phone
  MODEL_NAME = 'phone' # signals to look in params[:phone] for CRUD

end
