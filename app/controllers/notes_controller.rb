class NotesController < ApplicationController
  before_filter :require_login
  before_filter :require_notable,       only: [:index, :create]
  after_filter  :set_csrf_cookie_for_ng

  #JSON requests retrieve actual note lists.
  #HTML/JS requests retrieve the view.
  def index
    return render '/public/notes/list.html' if request.format.html?

    @notes=Note.regarding(@notable, current_user).
      includes(creator:[:photo_file]).
      order "critical DESC, updated_at DESC"
    @notes=params[:note_type_name].present? ? @notes.of_type(params[:note_type_name]) : @notes.except_restricted_types
    @notes=params[:note_filter].present? && params[:note_filter]!='all' ? @notes.where(params[:note_filter].to_sym => true) : @notes

    if @notable.is_a?(Crm::Case) && @notable.try(:agency_works_id)
      agency_notes = Processing::AgencyWorks::CommentsService.new(@notable).request
      agency_notes.reject!(&:confidential) unless current_user.can?(:sales_support)
      @notes += agency_notes
      @notes.select!{|n| n.is_a?(Note)}
      @notes = @notes.sort_by{|e| e[:created_at] || Time.zone.now}.reverse
    end

    json_opts={
      except:[:updated_at,:note_type_id],
      methods:[:creator_name,:creator_photo_thumb_url]
    }
    render json:@notes.as_json(json_opts)
  end

  def create
    return permission_denied "You lack permission to edit resource #{@notable.try(:id)}" unless @notable.editable?(current_user)
    attrs=params[:note]
    attrs[:text]=ActionController::Base.helpers.strip_tags attrs[:text]
    attrs[:creator_id]||=current_user.id
    @new_note = Note.new(attrs)

    if @new_note.save
      canned_success_response @new_note, jopts: {only:[:id]}
    else
      canned_fail_response @new_note
    end
  end

  def destroy
    @note = Note.find(params[:id])
    @note.delete
    render json:{}
  end

  def update
    attrs=params[:note]
    attrs[:text]=ActionController::Base.helpers.strip_tags attrs[:text]
    attrs.delete(:text) if attrs[:text].blank?

    @note = Note.find(attrs[:id])
    if @note.update_attributes(attrs)
      canned_success_response
    else
      canned_fail_response @note
    end
  end

  private

  def require_notable
    attrs =params[:note]||params
    id    =attrs[:notable_id]
    type  =attrs[:notable_type]

    if id.present? && id.to_i>0 && type.present?
      begin
        @notable=type.constantize.send(:find,id.to_i)
      rescue
        return permission_denied "Could not find the object to which you are trying to associate this note"
      end
    end
    if !@notable && params[:case_id].present?
      @notable=Crm::Case.find_by_id(params[:case_id])
    end
    if !@notable && params[:consumer_id].present?
      @notable=Consumer.find_by_id(params[:consumer_id])
    end
    if !@notable && params[:user_id].present?
      @notable=User.find_by_id(params[:user_id])
    end
    if !@notable && params[:notable_type].present? && params[:notable_id].present?
      @notable=params[:notable_type].constantize.find_by_id(params[:notable_id])
    end

    return permission_denied "A notable entity is required to load this view"            unless @notable.present?
    return permission_denied "You lack permission to view resource #{@notable.try(:id)}" unless @notable.viewable?(current_user)
  end

end
