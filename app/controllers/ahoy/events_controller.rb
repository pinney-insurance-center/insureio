class Ahoy::EventsController < ApplicationController
  respond_to :json, :js, :html
  before_filter :require_login, except: [:publish]

  def index
    @events=Ahoy::Event.viewable_by(current_user).includes(:visit,message:[:template,:task,:blast]).newest_first
    if params[:participant_id] && params[:participant_type]
      @events=@events.where(user_id: params[:participant_id], user_type: params[:participant_type])
    else
      @events=@events.identifiable.limit(20)
    end
    render json:@events.to_json()
  end

  #This action allows InsuranceDivision to notify DataRaptor immediately upon saving an event,
  #so users see immediate feedback in their dashboards.
  def publish
    error_msg=""
    status=200
    if @event=Ahoy::Event.where(id:params[:id]).first
      @event.publish
      error_msg=@event.errors.full_messages.join('\n') if @event.errors.present?
      status=500 if @event.errors.present?
    else
      error_msg="Could not find event with id #{params[:id]}."
      status=404
    end
    if error_msg.present?
      AuditLogger["event_publishing"].error error_msg+"\nRequest originated from IP #{request.remote_ip}."
      render json:{errors:error_msg}, status:status
    else
      render json:{}, status:status
    end
  end
end