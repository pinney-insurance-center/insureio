class SmtpServersController < ApplicationController

  respond_to :html, :json
  before_filter :require_login
  before_filter -> {require_permission :email}

  def init
  end

  def new
  end

  def edit
  end

  def index
    if request.format.html?#Use the static html template.
      return redirect_to '/smtp_servers/container.html'
    end
    if params[:id] && @user = User.find(params[:id])
      return permission_denied unless @user.viewable?(current_user)
    else
      @user = current_user
    end
    @smtp_servers = @user.smtp_servers
    render(json: @smtp_servers.as_json(except:[:crypted_password]) )
  end

  def test_connection
    @smtp_server = Marketing::Email::SmtpServer.new(params[:marketing_email_smtp_server])
    if @smtp_server.test_connection
      canned_success_response @smtp_server
    else
      canned_fail_response @smtp_server
    end
  end

  def create
    params[:marketing_email_smtp_server][:owner_id] = current_user.id
    @smtp_server = Marketing::Email::SmtpServer.new(params[:marketing_email_smtp_server])
    unless @smtp_server.owner.can?(:smtp_multiple) || @smtp_server.owner.smtp_servers.count == 0
      return permission_denied('Given user has already met his/her limit for Email Accounts')
    end
    if @smtp_server.host.empty?
      @smtp_server.host = @smtp_server.username.partition("@").last
    end
    if @smtp_server.save
      canned_success_response @smtp_server
    else
      canned_fail_response @smtp_server
    end
  end

  def update
    @smtp_server = Marketing::Email::SmtpServer.find(params[:id])
    return permission_denied unless @smtp_server.owner_id == current_user.id
    params[:marketing_email_smtp_server].delete(:password) if params[:marketing_email_smtp_server][:password].blank?
    if @smtp_server.update_attributes(params[:marketing_email_smtp_server])
      canned_success_response @smtp_server
    else
      canned_fail_response @smtp_server
    end
  end

  def destroy
    @smtp_server = Marketing::Email::SmtpServer.find(params[:id])
    return permission_denied unless current_user.smtp_servers.include?(@smtp_server)
    if @smtp_server.destroy
      canned_success_response @smtp_server
    else
      canned_fail_response @smtp_server
    end
  end

end
