class ContactsController < ApplicationController
  respond_to :json

  UNSUBSCRIBE_ACTIONS = [:unsubscribe_from_mktg_step_one, :unsubscribe_from_mktg_step_two, :unsubscribe_from_status_step_one, :unsubscribe_from_status_step_two]

  before_filter :require_login, except: UNSUBSCRIBE_ACTIONS
  before_filter :cross_reference_identity_for_message_links, only: UNSUBSCRIBE_ACTIONS

  PER_PAGE=15

  def index
    if request.format.html?
      return render 'public/contacts_gui.html', layout: !params[:no_layout]
    end
    # DEPRECATED param handler
    if params[:name_or_id].present?
      ActiveSupport::Deprecation.warn "name_or_id param received from #{request.referrer}"
      params[:search_term] = params[:name_or_id]
      params[:search_by] = 'name_or_id'
    end
    # Build a scope
    search = ContactSearch.new current_user, params
    return canned_fail_response(search.errors.join ' ') if search.errors.present?
    # Add select statements
    select_cols = [
      :id,
      :full_name,
      :tenant_id,
      :agent_id, :owner_id, :parent_id,
      :brand_id, :default_brand_id, :recruitment_brand_id,
      :primary_address_id, :primary_phone_id, :primary_email_id,
      :company,
      :is_recruit,
    ] + Array(params[:select]).select { |f| f.present? }
    # ^ This behaviour which adds params[:select] to the defaults appears
    # wrong. I looked at every usage in the front-end, and they're all
    # specifying fields are redundant with the default columns. I think every
    # usage intends params[:select] to override the default cols list instead
    # of augmenting it.
    search.select! select_cols
    search.scopes.each do |scope|
      [:addresses, :emails, :phones].each do |assc|
        scope.preload!(assc) if params[assc]
      end
    end
    json_includes = [:addresses, :emails, :phones].select { |assc| params[assc] }
    if params[:paginate]
      return canned_fail_response('Pagination not supported on multi-model searches') if search.scopes.length != 1
      scope = search.scopes.first # We only support paginated search on a single model
      scope.preload!(:owner_name_only)
      scope.preload!(:brand_name_only) if scope.model == Consumer
      scope.preload!(:default_brand_name_only, :recruitment_brand_name_only) if scope.model == User
      records = scope.paginate page: params[:page], per_page: PER_PAGE
      json_methods = [:person_type, :owner_name, :brand_name, :default_brand_name, :recruitment_brand_name]
      records_hashes = records.map { |r|
        r.as_json(methods: json_methods, include: json_includes)
        .tap { |obj| obj[:is_editable] = r.editable?(current_user) }
      }
      # Return an Object
      out = { records: records_hashes, page: records.current_page, page_count: records.total_pages }
    else
      records = search.scopes.map! { |s| s.limit(PER_PAGE) }.flatten(1)
      # Return an Array
      out = records.map { |r|
        r.as_json(methods: [:person_type], include: json_includes)
        .tap { |obj| obj[:is_editable] = r.editable?(current_user) }
      }
    end
    respond_with out
  rescue ContactSearch::ArgumentError => ex
    canned_fail_response ex.message
  end

  #This endpoint is specifically for 3cx phone system.
  #It will return one or no records, with a pre-established set of fields.
  #3cx phone system supports basic auth, so will be passing a basic auth header with the request.
  def search_by_phone
    number=params[:number]
    return render json:{error:'Missing required parameter "number".'}, status:422 unless number.present?
    contact_record=nil
    try_count=0
    while contact_record.nil? && try_count<10 do
      matching_phone=Phone.where("value LIKE '%#{number}%'").first
      contact_record=matching_phone&.contactable#in case phone is associated with a deactivated person
      try_count+=1
    end
    if contact_record
      return_obj=contact_record.as_json(only:[:id,:full_name,:company])
      return_obj.delete 'warnings'
      return_obj[:primary_email]=contact_record.primary_email&.value
      return_obj[:home_phone   ]=contact_record.phones.select{|p| p.phone_type&.name=='home'}.first&.value
      return_obj[:home_phone2  ]=contact_record.phones.select{|p| p.phone_type&.name=='home'}.second&.value
      return_obj[:work_phone   ]=contact_record.phones.select{|p| p.phone_type&.name=='work'}.first&.value
      return_obj[:work_phone2  ]=contact_record.phones.select{|p| p.phone_type&.name=='work'}.second&.value
      return_obj[:mobile_phone ]=contact_record.phones.select{|p| p.phone_type&.name=='mobile'}.first&.value
      return_obj[:mobile_phone2]=contact_record.phones.select{|p| p.phone_type&.name=='mobile'}.second&.value
      #3cx only recognizes the response of the matched number is included, so if not included already, put it here.
      return_obj[:other_phone  ]=return_obj.values.any?{|v| v.to_s.include?(number.to_s) } ? nil : number
      return_obj[:url          ]="#{root_url}#{contact_record.class.name.downcase}s/#{contact_record.id}"
      return_obj[:person_type  ]=contact_record.class.name
      return_obj={contact:return_obj}
      render json: return_obj
    else
      render json: {}, status: 404
    end
  end

  #Renders an xml file with the current user's subdomain.
  #Users should be instructed to upload this file to their 3cx server
  #and then enter their Insureio user id and api_key into 3cx in the fields labelled "username" and "password".
  def crm_settings_for_3cx
  end

  def send_sms
    return permission_denied unless current_user.can?(:sms)
    return permission_denied unless [:message, :phone_id, :id].all?{|k| params.has_key?(k) }
    phone=Phone.where(id:params[:phone_id], contactable_id:params[:id]).where('carrier_id IS NOT NULL').first
    return permission_denied unless phone.present? && phone.carrier_provided_email
    person=phone.contactable
    return permission_denied unless person && person.viewable?(current_user)
    recipient=phone.carrier_provided_email

    from_addr=person.is_a?(Consumer) ? APP_CONFIG['email']['consumer_facing_sender'] : APP_CONFIG['email']['sender']
    brand=if person.is_a?(Consumer)
      person.brand
    elsif person.recruit?
      person.recruitment_brand
    else
      person.default_brand
    end

    message_attrs={
      body:      params[:message],
      sender:    current_user,
      from_addr: from_addr,
      target:    person,
      recipient: recipient,
      brand:   brand,
    }
    message = Marketing::MessageMedia::Message.new( message_attrs )
    if message.deliver
      canned_success_response
    else
      canned_fail_response message
    end
  end

  #This is the action linked to from marketing emails.
  #Renders a button to confirm the unsubscription.
  def unsubscribe_from_mktg_step_one
    log_email_event_with_ahoy("Initiated Marketing Unsubscribe", @message) if @message
  end

  #Makes the actual change and renders the response (confirmation/error).
  def unsubscribe_from_mktg_step_two
    log_email_event_with_ahoy("Completed Marketing Unsubscribe", @message) if @message

    if @person
      if @person.update_attributes marketing_unsubscribe: true
        @msg=("You will receive no further marketing communications.<br />\n"+
          "You may still receive updates on the progress of your application for insurance.<br />\n"+
          "If you wish to start receiving marketing communications again in the future, "+
          "you may contact your sales agent #{@person.owner.try(:name)} and have them reinstate your subscription.").html_safe
        @status=200
      else
        @msg="Something went wrong while unsubscribing you.<br />\nPlease contact your assigned sales agent.<br />\nThey can unsubscribe you from further marketing.".html_safe
        @status=500
      end
    else
      @status=200
      @msg="The sender has been notified to remove you from their marketing list."
      SystemMailer.unsubscribe_request @message, 'marketing'
    end
    render 'unsubscribe_step_two', layout:true, status: @status
  end

  def unsubscribe_from_status_step_one
    log_email_event_with_ahoy("Initiated Status Unsubscribe", @message) if @message
  end

  def unsubscribe_from_status_step_two
    @success = @person.respond_to?(:status_unsubscribe) && @person.update_attributes(status_unsubscribe: true)
    log_email_event_with_ahoy("Completed Status Unsubscribe", @message) if @success
    render 'unsubscribe_step_two', layout: true, status: (@success ? 200 : 422)
  end
end
