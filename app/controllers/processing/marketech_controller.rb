class Processing::MarketechController < ApplicationController
  before_filter :require_login, except:[:update, :direct_update, :email]
  skip_before_filter :verify_authenticity_token, only:[:update,:direct_update]


  # Send email to agent and client
  def email
    m_case_data=Processing::Marketech::Dataset.find(params[:id])

    to   = m_case_data.kase.consumer.primary_email.value
    from = m_case_data.kase.agent

    esign_template=Marketing::Email::Template.find_by_name('App Sent - eSign')
    esign_message=Marketing::Email::Message.new(template:esign_template, sender:from, recipient:to, cc:from.primary_email.value, target: m_case_data.kase)
    esign_message.deliver
    marketing:true,
      template:self,
      sender:sender,
      recipient:recipient
    flash.now[:notice]='Message sent successfully.'
    render nothing:true, format: :js, layout:'application.js'
    rescue Exception => ex
      flash.now[:error]="Could not send message. #{'<br>'+ex.message}".html_safe
      render nothing:true, format: :js, layout:'application.js', status:500
  end

  #API endpoint. Receives updates directly from PSG/Marketech.
  #Does not require any type of auth, but expects a correctly formatted payload.
  def direct_update
    record = Processing::Marketech::Update.build params
    AuditLogger['marketech_update'].info "Marketech::Update: #{record.inspect}"
    missing_msg="Parameter case_id or caseID is required, but was missing. Please try your request again."
    return render(json: {error:missing_msg}, status:400) unless record.case_id.present?
    if record.dataset
      #Bypass scoping by tenant when querying for the consumer and assignee records.
      Thread.current[:current_user_acts_across_tenancies]=true
      record.save
    else
      AuditLogger['marketech_update'].info "No Marketech::Dataset in db for given Marketech::Update: #{record.inspect}"
    end
    render json: {message:'Successfully updated case.'}
  end

  #DEPRECATED. Will remove when they start sending to direct_update.
  # API endpoint: Receive Marketech Update from MarketechRouter application
  alias_method :update, :direct_update

  def export
    csv=Processing::Marketech::Carrier.export_csv
    render text: csv, format: :csv, layout:false
  end

  def import
    csv=params[:csv].read
    import=Processing::Marketech::Carrier.import_csv(csv)
    if import[:failures].present?
      flash.now[:error]="Failed to import Marketech carrier rules.<br>#{pluralize(import[:failures].count,'Row')} missing carrier id.".html_safe
      respond_to do |format|
        format.js   { render nothing: true, layout: true }
        format.html { render nothing: true, layout: true }
        format.json { render json: {errors: flash.now[:error_msgs]} }
      end
    else
      canned_success_response
    end
  end

end
