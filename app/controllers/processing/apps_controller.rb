class Processing::AppsController < ApplicationController
  before_filter :require_login, except:[:status]
  skip_before_filter :verify_authenticity_token, only:[:status]

  # Receive an XML document from APPS
  def status
    AuditLogger['app-right-status'].info request.raw_post
    render nothing:true
  end

end
