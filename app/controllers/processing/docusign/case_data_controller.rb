class Processing::Docusign::CaseDataController < ApplicationController
  def create
    @case_datum = Processing::Docusign::CaseDatum.create params[:processing_docusign_case_datum]
    redirect_to docusign_crm_case_path(@case_datum.case_id, format: :js, container:'#inner-container')
  end
end