class Reporting::SearchesController < ApplicationController
    before_filter :require_login
    before_filter -> {permission_denied unless current_user.can_any?(:reporting_custom, :reporting_canned)}, :only => [:show]
    before_filter -> {require_permission :reporting_custom}, :only => [:new, :create, :edit, :destroy, :update]
    before_filter :get_search, except:[:index, :destroy, :fields_to_show_whitelist, :criteria_fields_whitelist]

  respond_to :html, :js, :json

  def index
    if request.format.html?
      return render 'public/reporting/searches/container.html', layout:!params[:no_layout]
    end

    prepare_index
    respond_with @searches.as_json(methods:[:owner_name])
  end

  def create
    @search.owner ||= current_user
    @search.ownership_id ||= Enum::Ownership.id('user and descendants')

    @search.attributes = params[:reporting_search]

    if @search.save
      canned_success_response @search
    else
      canned_fail_response @search
    end
  end

  alias_method :update, :create

  def destroy
    if Reporting::Search.find_by_id(params[:id]).destroy
      canned_success_response
    else
      canned_fail_response
    end
  end

  def results
    #Run validations independently, versus through `valid?`,
    #because a name is necessary to save, but not to get results.
    @search.specifies_criteria
    @search.specifies_fields_to_show
    @search.criteria_are_sane
    @search.fields_to_show_match_whitelist
    if @search.errors.present?
      return render json:{errors:@search.errors.full_messages.join(".\n")},status:400
    end
    #When issue #581 is implemented, the following 3 method calls
    #will be executed in sequence via 3 requests to distinct controller actions.
    @record_ids = @search.get_matching_record_ids_for_user(current_user)
    if (@record_ids[1].length+@record_ids[2].length)>0
      @results_summary=@search.get_policies_summary @record_ids[1], @record_ids[2]
      @headings,@records=@search.get_fields_for_display @record_ids[1], @record_ids[2]
    end
    respond_to do |format|
      format.json{
        render json:{
                  recordIds:     @record_ids,
                  resultsSummary:@results_summary,
                  records:       @records||[],
                  headings:      @headings
                }
      }
    end
  end

  def get_consumers_for_subscription
    result = {}
    result[:consumer_data]={}
    result[:consumer_ids], result[:filtered_consumer_ids], result[:skipped_ids] = @search.consumers_summary(current_user)

    #get consumer data for necessary information for bulk enrollment
    raw_consumer_data = Consumer.where(id: result[:consumer_ids])
    raw_consumer_data.each {|consumer| result[:consumer_data][consumer.id] =  {name: consumer.name}};

    respond_to do |format|
      format.json { render :json => result.to_json }
    end
  end

  alias_method :show, :results

  def fields_to_show_whitelist
    render json: Enum::ReportingByCaseShowableFieldGroups.as_json
  end

  def criteria_fields_whitelist
    render json: Enum::ReportingByCaseCriteriaFields.as_json
  end

private

  def consumers_summary
    consumer_ids, filtered_consumer_ids, skipped_ids = @search.consumers_summary(current_user)
    {skipped_ids: skipped_ids, consumer_ids: consumer_ids, filtered_consumer_ids: filtered_consumer_ids}
  end

  def prepare_index
    @searches = Reporting::Search.viewable_by(current_user)
    @searches = @searches.where(owner_id:params[:owner_id]) if params[:owner_id]
    @searches = @searches.where(ownership_id:params[:ownership_id]) if params[:ownership_id]
    @searches = @searches.where('name LIKE ?', "%#{params[:name]}%") if params[:name] # Used in JSON response (for select2)
    @searches = @searches.order('name ASC')
    @searches = @searches.preload(:owner_name_only)
  end

  def get_search
    @search = params[:id] ? Reporting::Search.find(params[:id]) : Reporting::Search.new
    @search.attributes = params[:reporting_search]
    @search
  end

end

