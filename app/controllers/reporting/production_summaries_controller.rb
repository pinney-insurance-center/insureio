require 'will_paginate/array'
class Reporting::ProductionSummariesController < ApplicationController
  before_filter :require_login
  respond_to :json, :html

  DEFAULT_PER_PAGE = 10

  def index
    params.permit(:user_id,:page,:order_column,:descending)
    render_open_cases(sort_and_paginate)
  end

  private

  def cases_obj data
    return data.map do |open_case|
    begin
    {
      id: open_case.id,
      consumer_id:        open_case.consumer_id,
      consumer_name:      open_case.consumer.try(:full_name),
      last_updated:       open_case.updated_at,
      current_status:     open_case.status.try(:status_type_name),
      annualized_premium: open_case.current_details.try(:annualized_premium).to_f,
      carrier_name:       open_case.current_details.carrier.try(:name),
      product_name:       open_case.current_details.try(:product_name),
      brand_name:         open_case.consumer.try(:brand_name)
    }
    rescue => ex
      Rails.logger.error [ex.message, open_case.as_json].join("\n")
      {}
      end
    end.compact
  end

  def sort_and_paginate
    open_cases = Crm::Case.where(agent_id:params[:user_id]).open
    sql_paginate = lambda { |scope| scope.limit(DEFAULT_PER_PAGE).offset(DEFAULT_PER_PAGE*(params[:page].to_i-1)) }
    descending = params[:descending] == "true" ? true : false
    sort_and_paginate_array = lambda {|array, key|
          array.sort_by!{ |k| k[key] ? k[key] : "-" }
          array.reverse! if !descending
          array.paginate(page: params[:page], per_page: DEFAULT_PER_PAGE)
    }

    case params[:order_column]
    when "last_updated"
      return cases_obj(sql_paginate.call(open_cases.order("updated_at #{descending ? 'DESC' : 'ASC'}")))
    when "client_name"
      return cases_obj(sql_paginate.call(open_cases.joins(:consumer).order("consumers.full_name #{descending ? 'DESC' : 'ASC'}")))
    when "current_status"
      return cases_obj(sql_paginate.call(open_cases.joins(:status).order("crm_statuses.status_type_name #{descending ? 'DESC' : 'ASC'}")))
    when "carrier"
      return sort_and_paginate_array.call(cases_obj(open_cases), :carrier)
    when "product_name"
      return sort_and_paginate_array.call(cases_obj(open_cases), :product_name)
    when "premium"
      return sort_and_paginate_array.call(cases_obj(open_cases), :premium)
    when "brand_name"
      return sort_and_paginate_array.call(cases_obj(open_cases), :brand)
    else
      return []
    end
  end

  def render_open_cases cases, json_opts={}
    cases_count = Crm::Case.where(agent_id:params[:user_id]).open.count
    page_count = (cases_count.to_f / DEFAULT_PER_PAGE).ceil

    render json: {
        open_cases: cases,
        cases_count: cases_count,
        page_count: page_count
    }
  end
end

