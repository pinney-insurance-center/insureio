
class ProcessingController < ApplicationController

  before_filter :get_case_and_consumer

  # Send a 103 (new business submission) XML document to APPS
  # Referred to on the front end as AppRight process.
  def submit_to_apps
    unless @case.eligible_for_processing_with_apps
      error_msg="Case is not ready to send to AppRight\n#{@case.errors[:apps].join("\n")}"
      return render(json:{errors:error_msg}, status:400)
    end

    if !Processing::Apps::UploadService.new(@case).submit_new_business
      render json:{errors:'Failed transmission to AppRight'}, status:400
    else
      render json:{msg:'Uploaded to AppRight'}, status: 200
    end
  end

  # Send a 121 (General Requirement Order Request) XML document to APPS
  # Referred to on the front end as APPS process.
  def submit_to_apps_121
    unless @case.eligible_for_processing_with_apps_121
      error_msg="Case is not ready to send to APPS\n#{@case.errors[:apps].join("\n")}"
      return render(json:{errors:error_msg}, status:400)
    end

    if !Processing::Apps::UploadService.new(@case).order_requirements
      render json:{errors:'Failed transmission to APPS'}, status:400
    else
      render json:{msg:'Uploaded to APPS'}, status: 200
    end
  end

  def submit_to_docusign
    envelope = Processing::Docusign.send_to_docusign(@case)
    @case_data = @case.build_docusign_case_data(envelope_id: envelope.id)
    render json:{envelope_id:@case.docusign_envelope_id}
  end

  def submit_to_igo
    saml_response =Processing::Igo.get_saml(@case, @consumer)
    @case.update_attributes sent_to_igo: Date.today
    form=%Q(
      <form id="igo-hidden-form" method="post" action="#{APP_CONFIG['igo']['post_url']}">
        <input type="hidden" name="SAMLResponse" value="#{saml_response}" />
        <input type="hidden" name="TARGET"       value="#{APP_CONFIG['igo']['post_target']}" />
      </form>
      )
    #open the popup window, then make the browser perform the post 
    render json:{markup:form}
  end

  def submit_to_agency_works
    @response=@case.send_to_agency_works
    if @response.match(/error/i)
      s_code=400
      body={errors:@response}
    else
      s_code=200
      body={msg:@response, agency_works_id:@case.agency_works_id}
    end
    render json: body, status: s_code
  end

  # Send case to Marketech (see also: CLU.PoliciesController#upload_app)
  def submit_to_marketech
    @case.marketech_dataset || @case.build_marketech_dataset
    # attempt upload
    if @case.eligible_for_processing_with_marketech
      if !Processing::Marketech::UploadService.new(@case.marketech_dataset).upload(current_user)
        @errors = 'Unable to upload to Marketech'
      end
    else
      @errors = @case.errors.full_messages.join("\n")
    end
    # render or redirect
    if @errors
      render json: {errors:@errors}, status:400
    else
      render json:{object:@case.as_json(methods:[
          :marketech_email_ready,
          :marketech_editable,
          :marketech_loggable,
          :marketech_esign_url,
          :marketech_edit_url,
          :marketech_pdf_url,
          :marketech_log_url]
        )}
    end
  end

  # Create a CaseData record. Return the url for the ExamOne scheduler.
  # This obscures (nominally) how the get params are composed for the schedule url
  # (since it gets done in the model versus the front end).
  def schedule_with_exam_one
    #This table currently only stores its association to a case, and (in only 16% of current records) the info_sent indicator.
    @case.exam_one_case_data.create params[:case_datum]
    schedule_url=Processing::ExamOne::schedule_url(@case)
    if @case.errors[:exam_one].present?
      render(json:{errors:@case.errors[:exam_one][0]}, status:400)
    else
      render json: {url:schedule_url,exam_one_case_data:@case.exam_one_case_data}
    end
  end

  # Send OR01 to ExamOne, which if successful, also updates the CaseData record.
  def submit_to_exam_one
    if Processing::ExamOne::build_and_send_or01(@case)
      msg = 'ExamOne OR01 sent'
      render json: {msg: msg}
    else
      msg = [ 'Failed to send OR01 to ExamOne', *@case.errors.full_messages ].join ".\n"
      render json: {errors:msg}, status:400
    end
  end

  def exam_one_control_codes
    control_codes=Processing::ExamOne::ControlCode.like(@case)
    render json: control_codes.as_json(only:[:id,:name])
  end

  def submit_to_smm
    @case_status=@case.smm_status
    @case_status||=Processing::Smm::Status.create(case_id:@case.id)#this table currently only stores its association to a case
    @smm_response=Processing::Smm.new.schedule_url(@case, @consumer)
    render json: {smm_status:@case_status.attributes,url:@smm_response}
  end

  #This action is meant to obscure the url, which contains the password in plain text.
  #It puts the page within an iframe, rather than getting and rendering the page content,
  #because the page contains lots of relatively linked stylesheets, scripts, etc.,
  #and the average user will not think to inspect an iframe for its source url.
  def submit_to_app_assist
    url="https://www.lgaappassist.com/cgi-bin/rlipi.exe?ulogin=X300000_PT&password=1nwwk2kkiuwidy&fid=log&fnd=0"
    render inline:"<iframe src=\"#{url}\" style=\"height:100%;width:100%;\"></iframe>"
  end

private

  def get_case_and_consumer
    @case=Crm::Case.find_by_id( params[:case_id] || params[:id] )
    return(permission_denied) unless @case.present? && @case.editable?(current_user)
    @consumer=@case.consumer
  end

end

