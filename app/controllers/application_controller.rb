class ApplicationController < ActionController::Base
  include AuthenticationHelpers
  include ControllerHelpers

  #If authenticated via api instead of a cookie session, assume no xsrf (this app doesn't store anything in cookies aside from the session).
  protect_from_forgery with: :exception, unless: ->{ key_auth }

  rescue_from ActionController::ParameterMissing, with: lambda { |ex| canned_fail_response ex.message, status: 422 }

  helper_method :current_user
  helper_method :true_current_user

  before_filter -> { @controller_warnings||=[] }
  before_filter -> { Time.zone = 'UTC' } # This may be overridden in `current_user`
  before_filter(#:check_security_questions_answered,#disabled for now, so it will not ask the security questions
                :require_ssl,
                :require_referrer_host_match,
                :mark_all_params_as_permitted,
                :annotate_tenant_id_based_on_subdomain
                )
  after_filter :set_csrf_cookie_for_ng
  layout :layout_based_on_requested_format

  #This augments the payload for the lograge logging gem.
  #The gem's log action happens at the end of the request.
  def append_info_to_payload(payload)
    super
    uninteresting_headers=["HTTP_VERSION", "HTTP_HOST", "HTTP_REFERER", "HTTP_ACCEPT", "HTTP_X_FORWARDED_FOR", "HTTP_COOKIE","HTTP_X_XSRF_TOKEN"]
    data_to_append={
      app_server_name:          (`hostname`||'').sub("\n",''),
      remote_ip:                request.remote_ip,
      true_current_user_id:     Thread.current[:true_current_user_id],
      current_user_id:          Thread.current[:current_user_id],
      current_user_auth_method: Thread.current[:current_user_auth],
      tenant_id:                Thread.current[:tenant_id],
      current_user_acts_across_tenancies: Thread.current[:current_user_acts_across_tenancies],
      referrer:                 request.referrer,
      headers:                  request.headers.select{|k,v| k=~/^HTTP_/ && !uninteresting_headers.include?(k) },
    }
    payload.merge!(data_to_append)

    payload[:level]=(#set log level based on status.
      case payload[:status]
      when 200 then "INFO"
      when 302 then "WARN"
      else "ERROR"
      end
    )
  end

  #without this method, requests for js that find an html partial
  #will render those partials within layouts/application.html.erb
  def layout_based_on_requested_format
    return false if params[:no_layout]
    if request.format.html? || request.format=="*/*"
      "application.html"
    else
      false
    end
  end

  # override redirect_to to default to 303 instead of 302
  def redirect_to options={}, response_status={}
    super options, response_status
    self.status = 303 if status == 302
  end

  #Summary of CSRF (cross-site forgery) protection scheme:
  #Any domain sending requests to this one via a browser will *include* any stored cookies for this domain.
  #But only pages/scripts served by this domain, and this server itself, can *read* the content of those cookies.
  #So cookie-based session can't be the only form of auth for put/post/patch.
  #Railsy front end forms pass the xsrf token as a hidden field in the params.
  #AngularJS front end passes the xsrf token (read from the cookie) in a header.
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  #Direct rails to look for xsrf token in the headers in addition to the params.
  #AngularJS front end app passes the token in the headers.
  def verify_authenticity_token
    form_authenticity_token == request.headers['X-XSRF-TOKEN'] || super
  end

  #This is a temporary measure until all controller actions can be reorganized.
  #Where we call `permission_denied` unless some parameter is required, those should
  #eventually be replaced with the new `require` method.
  #Where we specifically delete undesired param keys, those should
  #eventually be replaced with logic permitting all except for those keys.
  def mark_all_params_as_permitted param_obj=params
    Rails.logger.info "\nCalled `mark_all_params_as_permitted`.\n" if param_obj==params
    param_obj.permit!
    param_obj.each do |k,v|
      mark_all_params_as_permitted(v) if v.respond_to?(:permit!)
    end
    param_obj
  end

private

  def allow_action_for_lapsed_user?
    if value = FREE_ACTIONS[self.class.name]
      return true if :all == value or value.include?(action_name)
    end
  end

  def log_email_event_with_ahoy event_name, message
    return true#This will be revisited when issue #531 is addressed.
    options={
      participant_id:   message.person.try(:id),
      participant_type: message.person.try(:class).try(:name),
      owner_id:         message.sender_id,
      brand_id:         message.brand_id
    }
    log_event_with_ahoy event_name, options.merge(message_id:message.id, owner_id:message.sender_id)
  end

  def log_event_with_ahoy event_name, options={}
    return true#This will be revisited when issue #531 is addressed.
    ahoy.track_visit(options) unless current_visit
    ahoy.track event_name, request.filtered_parameters, options
  end

  def set_any_http_origin
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

  def admin?
    current_user&.super?
  end

  def permission_denied message=nil, opts={}
    status = opts.fetch :status, 403
    dialog = opts.fetch :dialog, false
    resource = opts.fetch :resource, 'this resource'
    if !message || message.blank?
      message = t(:permission_denied, resource: resource)
      dialog=true
    end

    @highlight_flash_error_with_a_js_dialog=dialog
    logger.error "Responded with permission denied: #{message}."
    if opts[:redirect] && request.format.html?
      flash[:error] = message
      return redirect_to opts[:redirect]
    elsif request.format.html? || request.format.js?
      flash.now[:error]=message
      formats = request.format.js? ? [:js] : [:html]
      return render nothing:true, layout:layout_based_on_requested_format, status:status, formats:formats
    else
      render json:{error:message}, status:status
    end
  end

  def render_404
    respond_to do |format|
      format.html {render 'public/404.html', status: 404}
      format.all {render nothing: true, status: 404}
    end
  end

  def authenticate_agency_manager
    require_permission :agency_mgmt, "You must be an agency manager to access this endpoint or resource"
  end

  def require_authenticated
    if current_user.nil?
      msg = "You need to login first"
      custom_header :error, "current_user() is nil"
      permission_denied msg, status: 401, redirect: "/login?error=#{msg}"
    end
    !performed?
  end

  def require_enabled
    unless current_user.enabled
      custom_header :error, "User #{current_user.id} is not enabled"
      UserSession.find&.destroy
      session.destroy
      msg = "Your account must be enabled by the vendor before you can continue"
      permission_denied msg, status: 401, redirect: "/login?error=#{msg}" 
    end
    !performed?
  end

  def require_paid
    return true unless current_user.lapsed?(true)
    if value = FREE_ACTIONS[self.class.name]
      return true if :all == value or value.include?(action_name)
    end
    custom_header :error, "User #{current_user.id}'s payments have lapsed"
    if action_name == 'payment_info'
      flash[:alert] = t('flash.lapsed')
    else
      msg = t('errors.lapsed')
      permission_denied msg, redirect: '/users/payment_info.html'
    end
    !performed?
  end

  def require_tos
    unless current_user.tos
      custom_header :error, "User #{current_user.id} has not signed TOS"
      unless params[:controller] == 'users'
        permission_denied "You must accept the TOS first", redirect: tos_path(:redirect_path => request.original_url)
      end
    end
    !performed?
  end

  def require_login
    Rails.logger.info "Current User id: #{current_user.try(:id)}"
    require_authenticated or return
    require_enabled or return
    require_paid or return
    require_tos or return
    require_tenant_match or return
  end

  # Quoter key is included in self-quote urls and in photo/signature urls sent to consumers.
  # It should not be used to secure personally identifiable informtion, because it is distributed so widely.
  def require_login_or_quoter_key
    @additional_auth_keys = :quoter_key
    current_user
    require_login unless :quoter_key == Thread.current[:current_user_auth]
  end

  # The Consumers#create action is used both by api users with API credentials,
  # and by the last step of the quoting widget, with widget credentials.
  def require_widget_and_user
    @additional_auth_keys = :quoter_key
    return canned_fail_response(status: 401) unless current_user
    widget = Quoting::Widget.find_by(id: params[:widget_id]) if params[:widget_id]
    if :quoter_key == Thread.current[:current_user_auth]
      permission_denied(nil, resource: 'quoter widgets') unless current_user.can?(:quoter_widget)
      canned_fail_response(status: 404, errors: "No widget for id") unless widget.present?
    else
      require_login
    end
  end

  def check_security_questions_answered
    if true_current_user
      unless true_current_user.security_questions_answered
        redirect_to set_security_questions_users_path
      else
        redirect_to answer_security_question_users_path if cookies[:remember_ip].blank?
      end
    end
  end

  def require_permission permission_name, err_msg=nil
    require_login
    if performed?
      return false
    elsif current_user.can?(permission_name)
      return true
    else
      err_msg ||= "You need #{permission_name} to access #{controller_name}##{action_name}"
      respond_to do |format|
        format.html { redirect_to root_path, flash:{error:err_msg} }
        format.json { render json:{error:err_msg}, status:403 }
        format.js   { render text:err_msg, status:403 }
      end
      return false
    end
  end

  # Prevent session hijacking via XSS
  # Ignore subdomain, so pinney.insureio.com can make requests to ebi.insureio.com.
  # But pinney.insureio.qadev can NOT make requests to ebi.insureio.com.
  def require_referrer_host_match
    if !request.format || !request.format.html? || request['HTTP_X_REQUESTED_WITH'].present? # Detect AJAX request
      unless request.referrer.nil?
        referring_host=URI.parse(URI.encode(request.referrer)).host
        referring_domain=referring_host.try(:match,/\w+\.(insureio\.\w+)/).try(:[],1)
        request_domain=request.host.try(:match, /\w+\.(insureio\.\w+)/).try(:[],1)
        unless referring_domain==request_domain
          render nothing:true, status:403
        end
      end
    end
  end

  def require_super
    require_permission(:super_edit, "You need admin privileges to continue")
  end

  def restrict_agency_management_for_agent
  end

  # redirect to https
  def require_ssl
    unless request.ssl? or Clu2::Application::config.consider_all_requests_local or request.local?
      redirect_to( url_for(params.merge({:protocol => "https://"})), :status => 301 )
    end
  end

  #because so many controllers repeat the same generic message, and nested if/else trees get very cluttered
  def canned_success_response *args, **opts
    object, action, resource, json_opts = args
    object ||= opts[:object]
    action ||= opts[:action] || action_name.gsub(/_/,' ')
    resource ||= opts[:resource] || translate(controller_name.humanize.singularize)
    json_opts ||= opts[:jopts] || {}
    if opts.except(:object, :action, :resource, :jopts).present?
      raise KeyError.new("Unrecognized key(s) in #{__callee__} opts: #{opts.inspect}")
    end

    flash.now[:notice]||=""
    flash.now[:notice]="Successfully #{past_tense(action)} #{resource}.#{flash.now[:notice].length>0 ? "<br />#{flash.now[:notice]}" : nil}".html_safe

    if request.format.html?
      render nothing: true, layout: true
    else#respond with json
      response={message: flash.now[:notice]}
      if object.present?
        object=object.as_json(json_opts)
        response[:object]=object.select{|k| !k.match(/crypted/)}#scrub ecnrypted fields
      end
      if @controller_warnings.present?
        response[:warnings]||=[]
        response[:warnings]+=@controller_warnings
      end
      render json: response
    end
  end

  def canned_fail_response *args, **opts
    obj_or_msg, action, resource, status = args
    object = obj_or_msg unless obj_or_msg.is_a?(String)
    object ||= opts[:object] || {}
    action ||= opts[:action] || action_name
    resource ||= opts[:resource]
    status ||= opts[:status] || 422
    resource ||= translate object.class.name.underscore.humanize if object.present?
    resource ||= translate controller_name.humanize.singularize
    if opts.except(:object, :action, :resource, :status, :errors).present?
      raise KeyError.new("Unrecognized key(s) in #{__callee__} opts: #{opts.inspect}")
    end

    error_msgs = obj_or_msg if obj_or_msg.is_a?(String)
    error_msgs ||= object.delete(:errors) if object.is_a? Hash
    error_msgs ||= opts[:errors] # A newline-delimited String
    error_msgs ||= if object.present? && object.respond_to?(:errors) && object.errors.present?
      if object.is_a? ActiveRecord::Base
        object.errors.full_messages.join(".\n")
      elsif object.respond_to? :errors
        object.errors.messages.values.flatten.join(".\n")
      end
    end
    error_msgs ||= object

    logger.error "Responded with error: #{error_msgs}."
    flash.now[:error] = "Failed to #{action} #{resource}".html_safe
    flash.now[:error] =+ "\n#{error_msgs}".html_safe if error_msgs.present?
    if request.format.html?
      render text: error_msgs, status: status
    else#respond with json
      if object.as_json.is_a?(Hash)
        output = object.as_json.reject { |k| k =~ /crypted/ }
      else
        output = {}
      end
      output.merge! errors: error_msgs.presence || "Failed to #{action} #{resource}."
      if @controller_warnings.present?
        output[:warnings] ||= []
        output[:warnings] += @controller_warnings
      end
      render json: output, status: status
    end
  end

  def past_tense verb
    verb_orig=verb
    matches={
      'create'=>'created',
      'update'=>'updated',
      'destroy'=>'destroyed'
    }
    matches.each { |present, past| verb=past and break if verb==present}
    if verb==verb_orig
      "completed #{verb} for"
    else
      verb
    end
  end

  def translate resource_name
    matches={
      'connection'=>'consumer',
      'user'=>'agent'
    }
    matches.each { |present, past| resource_name=past and break if resource_name==present}
    resource_name
  end


  # If request url is not a match for user's tenant, redirect to user's tenant.
  # Only works corectly if called after user has been authenticated.
  def require_tenant_match
    if current_user && current_user.tenant_name != request.subdomain
      if redirect_uri = Enum::Tenant.subdomain_uri(request.url, current_user.tenant_name) # returns nil if output would be the same as input
        # POSTs just get redirected to '/'. GETs get redirected to same path & params but different host.
        redirect_uri.path = '/' if request.post? && redirect_uri.path!='/login'
        redirect_to redirect_uri.to_s
      end
    end
    !performed?
  end

  def annotate_tenant_id_based_on_subdomain
    Thread.current[:tenant_id] = tenant&.id
  end

  def custom_header key, value, delimiter=';'
    key = 'DR-' + key.to_s
    response.headers[key] ||= '' # This operation is in place in case custom_header gets called multiple times for the same key so that the values shall be joined together by delimiter
    response.headers[key] += "#{value}#{delimiter}"
  end

  #The goal of this method is to minimize the need for front end massaging of data.
  def sanitize_params object, new_attrs
    new_attrs.keys.each do |k|
      #Remove keys for which there is no setter method.
      if !object.respond_to?("#{k}=")
        # We don't want to have to indefinitely maintain a lot of superfluous input keys. Clean up the API!
        ActiveSupport::Deprecation.warn("Do not use #{k} for #{controller_name}##{action_name}")
        new_attrs.delete(k)
        next
      end
      #Remove keys holding datetimes that haven't changed substantially (more than just the seconds being truncated).
      if new_attrs[k] && object.respond_to?(k) && object.send(k).is_a?(Time)
        existing_value=object.send(k).utc.to_i+object.send(k).utc_offset
        new_attrs.delete(k) if new_attrs[k].to_time.to_i/120==existing_value/120
      end
      #Modify keys that are the names of an association.
      if object.class.reflect_on_all_associations.map(&:name).include?(k.to_sym)
        assoc_data=new_attrs.delete(k)
        accepts_attrs_for_assoc=object.class.reflect_on_association(k.to_sym).options[:autosave]

        if assoc_data.present? && accepts_attrs_for_assoc
          # We don't want to have to indefinitely maintain a lot of superfluous input keys. Clean up the API!
          ActiveSupport::Deprecation.warn("Do not use #{k} for #{controller_name}##{action_name}")
          new_attrs["#{k}_attributes".to_sym]=assoc_data
          k="#{k}_attributes"
        else
          next
        end
      end
      #Recurse through nested attributes.
      if k.match(/_attributes\z/)
        assoc_data=new_attrs.delete(k)
        assoc_name=k.match(/(.*)_attributes\z/)[1]
        assoc=object.class.reflect_on_association(assoc_name.to_sym)
        accepts_attrs_for_assoc=assoc.options[:autosave]
        if assoc_data.present? && accepts_attrs_for_assoc
          class_name=assoc.options[:class_name]#sometimes class_name is not specified.
          (new_attrs[k.to_sym]=assoc_data && next) unless class_name.present?#can't build a sample object without knowing the class
          assoc_object=eval(class_name).new
          if !assoc.collection?
            new_attrs[k.to_sym]=sanitize_params assoc_object, assoc_data
          else
            new_attrs[k.to_sym]=assoc_data.map{|ad| sanitize_params(assoc_object, ad) }
          end
        end
      end
    end
    new_attrs
  end

  # This is not secure against brute-force attacks. Please use `ltd_auth` in the future.
  # When a client receives an email, he has the quoter_key and can infer that message ids and person_ids are
  # auto-incrementing integers, so he can run through a reasonable range of both to access data pertaining
  # to the agent's other users.
  # Set @message and @person using one of the following combinations to establish identity:
  # - message id, person_type, person_id, key
  # - message id, recipient
  # - message id, ltd_access_code
  #This provides a small level of security, so no one can tamper with the links sent in emails.
  def cross_reference_identity_for_message_links
    message_id = request.path.match(/messages\//) ? params[:id] : params[:message_id]
    @message = Marketing::Message.find_by id: message_id
    return canned_fail_response("Message not found", status: 404) unless @message
    if params[:person_id].present? && ['User','Consumer'].include?(params[:person_type])
      @person = params[:person_type].constantize.find_by id: params[:person_id]
    elsif params[:recipient] == @message.recipient && @message.recipient.present?
      return @person = @message.person
    elsif params[:ltd_access_code].present?
      _, coded_record = User.from_access_code(params[:ltd_access_code])
      return @person = coded_record if coded_record == @message.person
    end
    return if current_user&.super?
    return if current_user && [@message.user_id, @message.sender_id].include?(current_user.id)
    return if @message.person == @person && @message.person.editable?(current_user)
    if params[:key].present? && @message.person == @person
      key_holder = @person.is_a?(Consumer) ? @person.agent : @person.parent
      return if params[:key] == key_holder&.quoter_key
    end
    return canned_fail_response("Message not found", status: 404)
  end

  # Set this Thread's tz to the current_user's (or ostensible current user's)
  def use_user_time_zone user
    Time.zone = ActiveSupport::TimeZone.new(user.time_zone) if user&.time_zone
  end

  # A Hash of [controller_name => [action_name, action_name...] for actions
  # accessible to users without accounts or whose accounts are lapsed. To
  # allow all actions for a given controller, make the value `:all`
  FREE_ACTIONS = {
    'UserSessionsController' => :all,
    'UsersController' => ['update','payment_info'].freeze,
    'Usage::PaymentInfosController' => :all,
    'Usage::StripeController' => :all,
  }.freeze

  protected

  #AngularJS does not send authenticity token in params, so we need to direct rails to look for it in the headers.
  #Otherwise, the session would be reset and impersonation would end.
  def verified_request?
    super || form_authenticity_token == request.headers['X_XSRF_TOKEN']
  end

  #Converts a flat object like `{"consumer.address1.state_id":1}`
  #to a nested object like `{consumer:{addresses_attributes:[{state_id:1}]}}`.
  def expand_dot_notation flat_object, blacklisted_keys=[], root_object_key
    nested_obj=HashWithIndifferentAccess.new
    blacklisted_keys+=[:action,:controller,:format,:tab_set]
    o=flat_object.except(*blacklisted_keys)
    o.each do |k,v|
      method_chain=extract_method_chain_from_string k
      follow_method_chain_to_set_key nested_obj, method_chain, v
    end
    if root_object_key
      nested_obj[root_object_key.to_s+'_attributes']
    else
      nested_obj
    end
  end

  def extract_method_chain_from_string key
    tmp_chain=key.to_s.gsub(/[^\w\.]/, '').split('.')
    method_chain=[]
    tmp_chain.each do |m|
      matches=m.match(/(\D+)(\d+)/);
      if matches
        method_chain+=[ matches[1], matches[2].to_i-1 ]
      else
        method_chain.push m
      end
    end
    method_chain
  end

  def follow_method_chain_to_set_key obj, method_chain, v
    next_obj=nil
    if method_chain.length>1
      if method_chain[0].is_a?(Integer)#key is an index
        if(!obj[ method_chain[0] ])
          obj[ method_chain[0] ]=HashWithIndifferentAccess.new
        end
        next_obj=obj[ method_chain[0] ]
      else#key is a key
        if method_chain[1].is_a?(Integer)#value is an array
          pluralized=''
          case method_chain[0].last
            when 's'
              pluralized=method_chain[0].gsub(/s$/,'ses')
            when 'y'
              pluralized=method_chain[0].gsub(/y$/,'ies')
            else
              pluralized=method_chain[0]+'s'
          end
          if method_chain[0].match(/_details$/)
            #These are the only associated model names that are already plural
            #and therefor don't conform to the above pluralization rules.
            #This list will need reevaluating if/when future models and associations are added.
            pluralized=method_chain[0]
          end
          if !obj[ pluralized+"_attributes" ]
            obj[ pluralized+"_attributes" ]=[]
          end
          next_obj=obj[ pluralized+"_attributes" ]
        else#value is an object
          if !obj[ method_chain[0]+"_attributes" ]
            obj[ method_chain[0]+"_attributes" ]=HashWithIndifferentAccess.new
          end
          next_obj=obj[ method_chain[0]+"_attributes" ]
        end
      end
      method_chain.shift()#like `pop`, but from beginning of array.
      follow_method_chain_to_set_key next_obj, method_chain, v
    else#key is an attribute
      if v.is_a?(String) && v.match(/^[\d,\.]+$/)#value is a string representing a number
        v=v.gsub(/,/,'');#remove commas so rails doesn't choke
      elsif v==''#value is a blank string
        return obj#return without setting this key
      end
      obj[ method_chain[0] ]=v
    end
    obj
  end

  def tenant
    @tenant ||= request.subdomain.present? ? Enum::Tenant.find_by_name(request.subdomain) : Enum::Tenant.first
  end
end
