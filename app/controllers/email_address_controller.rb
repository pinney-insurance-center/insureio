class EmailAddressController < ContactBelongsTosController
  respond_to :js, :html, :json

  MODEL = EmailAddress
  MODEL_NAME = ['email', 'email_address']  # signals to look in params[:email] for CRUD

end
