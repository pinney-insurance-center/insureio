class AttachmentsController < ApplicationController
  before_filter :require_login, except:[:show]
  skip_before_filter :require_referrer_host_match, only:[:show]

  def index
    if request.format.html?
      return render 'public/attachments/container.html', layout:!params[:no_layout]
    end
    unless params[:person_type].present? && params[:person_id].present?
      return canned_fail_response status: 404
    end

    params.permit(:person_type,:person_id)
    person_type=params[:person_type]
    person_id=params[:person_id]

    person  =person_type.constantize.
      where(id: person_id).
      preload(attachments:[:created_by,:updated_by], recordings:[:created_by,:updated_by]).first
    output_obj={}
    output_obj[ person_type ]=person.attachments.where(case_id:nil).map(&:default_serialization)
    if current_user.can?(:super_view)
      #Though new phone recordings are no longer being created,
      #we are retaining existing records in the existing table structure for now,
      #hence this separate query.
      phone_recordings=person.recordings.map(&:default_serialization)
      phone_recordings.each{|r| r[:is_recording]=true }
      output_obj[ person_type ]+=phone_recordings
    end
    include_attachments_for_policies=person.respond_to?(:cases)
    if include_attachments_for_policies
      policies=person.cases.preload(quoting_details:[], attachments:[:created_by,:updated_by])
      policies.map do |p|
        identifier="Policy \# #{p[:id]} #{p.current_details.try(:carrier_name)} - #{p.current_details.try(:plan_name)}"
        output_obj[identifier]=p.attachments.map(&:default_serialization)
      end
    end
    render json: output_obj
  end

  #This action hides the secure AWS link,
  #and enforces login and access restrictions.
  def show
    a=Attachment.find_by_id(params[:id])

    return canned_fail_response(status: 404) unless a.present?

    valid_user=nil

    #Define conditions which may allow an attachment to be viewable.
    is_consumer_request  = ->{ current_user.nil? }
    has_valid_user       = ->{ params[:key].present? && valid_user=User.find_by_quoter_key(params[:key]) }
    is_user_photo        = ->{ valid_user.photo_file==a || a.person==valid_user }
    is_user_signature    = ->{ valid_user.signature_file==a || a.person==valid_user }
    is_brand_logo        = ->{ a.person_type=='Brand' && a[:file_name].match(/(brand|profile)_\d+_logo/) && a.person && a.person.owner_id==valid_user.id }
    is_brand_header      = ->{ a.person_type=='Brand' && a[:file_name].match(/(brand|profile)_\d+_header/) && a.person && a.person.owner_id==valid_user.id }
    is_consumer_viewable = ->{ is_user_photo.call || is_user_signature.call || is_brand_logo.call || is_brand_header.call }
    is_for_kase          = ->{ a.kase && a.kase.viewable?(current_user) }
    is_for_person        = ->{ a.person && a.person.viewable?(current_user) }

    #Call the appropriate condition procs to compose the final verdict.
    allowed_to_view_attachment= (is_consumer_request.call && has_valid_user.call && is_consumer_viewable.call ) ||
      (!is_consumer_request.call && (is_for_kase.call || is_for_person.call) )

    unless allowed_to_view_attachment
      #Do not give a more specific error code like permission denied.
      #Improper access to an attachment is highly likely to be malicious
      #and should be given as little info as possible.
      return canned_fail_response(status: 404)
    end

    the_file=a.binary_content rescue nil
    if the_file
      opts={type: a.content_type, disposition:'inline', status:200}
      send_data the_file, opts
    else
      render nothing:true, status: 422
    end
  end

  def create
    required_attrs=[:file_for_upload, :person_id, :person_type]
    attrs=params.require(:attachment).permit(required_attrs+[:case_id,:file_name,:description])
    required_attrs.each{|a| attrs.require(a) }

    if attrs.has_key?(:case_id) && attrs[:case_id].to_i==0
      attrs.delete(:case_id)
    end
    if !attrs[:file_for_upload].is_a?(ActionDispatch::Http::UploadedFile)
      return canned_fail_response 'Parameter `file_for_upload` must be a file.', status: 422
    end
    attrs[:file_name]||=attrs[:file_for_upload].original_filename
    a=Attachment.new(attrs)
    if a.save
      canned_success_response a, jopts: {only:[:id]}
    else
      canned_fail_response a
    end
  end

  def update
    attrs=params.require(:attachment).permit(:description,:updated_by_id)
    a=Attachment.find_by_id(params[:id])
    return permission_denied(nil, status: 404) if !a

    a.assign_attributes(attrs)
    if a.save
      canned_success_response a
    else
      canned_fail_response a
    end
  end

  def destroy
    a=Attachment.find_by_id(params[:id])
    if a.try(:destroy)
      canned_success_response
    else
      canned_fail_response a, status: 405
    end
  end

end
